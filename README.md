# Notes

[![Three Column Design](sidebar_version.jpg)](https://lawrencechin.gitlab.io/Notes_Export/)

[![Single Pane Design](single_pane_version.jpg)](https://lawrencechin.gitlab.io/Notes_Export/)

A static site generator/script using **Pandoc** to convert `.md` files to `.html`.

Notes are stored in a *Notes* folder categorised in subfolders. An index is generated with simple navigation and search facilities. 
