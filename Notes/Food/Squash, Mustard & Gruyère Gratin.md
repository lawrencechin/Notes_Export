# [Squash, Mustard & Gruyère Gratin](https://www.bbcgoodfood.com/recipes/squash-mustard-gruyere-gratin)

Layer up squash or pumpkin with a rich, creamy sauce and bubbling melted cheese to make this indulgent side dish - serve with a Sunday roast or sausages.

## Ingredients

* small knob of butter
* 1 tbsp olive oil
* 2 onions, halved and thinly sliced
* 2 garlic cloves, peeled and squashed
* 8 sage leaves
* 300ml pot double cream
* 200ml milk
* 2 tbsp wholegrain mustard
* 1 squash or pumpkin, peeled, seeds removed, quartered and thinly sliced (about 950g/2lb 2oz prepared weight)
* 100g Gruyère (or vegetarian alternative), grated

## Method

1. Heat the butter and oil in a large frying pan. Add the onions and cook slowly over a low-medium heat, stirring every now and then, for 10-15 mins until golden and soft.
2. Meanwhile, put the garlic and half the sage in a saucepan, add the cream and milk, and heat gently, not allowing the mixture to boil, for 5 mins. Remove from the heat and set aside to cool for 10 mins, then fish out the sage and garlic. Stir in the mustard and season well.
3. If cooking straight away, heat oven to 180C/160C fan/gas 4. Layer the pumpkin slices, onions, most of the cheese and the infused cream into a large baking dish, finishing with a layer of cream. Once you’ve used the ingredients up, scatter with the remaining cheese and put the remaining sage leaves on top. Cover the dish with foil and bake for 45 mins.
4. Uncover the dish and increase the heat to 200C/180C fan/gas 6. Cook for a further 20-30 mins until golden brown and tender all the way through. Leave to cool for 10 mins before serving.
