# [Savoury Danish Pastries](https://www.bbcgoodfood.com/recipes/savoury-danish-pastries)

Give morning pastries a bakeover by swapping the sweet for a savoury butternut squash, blue cheese, bacon, onion and beetroot filling.

## Ingredients

* 200g block of butter
* 500g bag white bread mix
* 1 egg, beaten, to glaze
* cornichons, to serve

### For the squash, diced

* 6 rashers streaky bacon, cooked until crispy and roughly chopped
* 200g Danish blue cheese
* 2 tsp caraway or fennel seeds
* 50g walnut, roughly chopped
* 1 small red onion, thinly sliced
* 2 cooked beetroot, not in vinegar, diced
* 25g spinach leaves, chopped filling

## Method

1. Wrap the butter in foil and freeze for 45 mins.
2. Tip the bread mix into a bowl and, using the foil to hold it, coarsely grate in the butter – keep dipping the end in the flour to stop it sticking to the grater too much. Use a cutlery knife to stir together, then add 300ml cold water and stir to a dough.
3. Put the squash in a microwave-proof bowl with a little water, cover with cling film, poke one hole in, and microwave on High, at 2-min increments, until just cooked. Drain any liquid.
4. Heat oven to 200C/180C fan/gas 6. Roll out the dough as thinly as you can, to a rectangle with longest side measuring 25cm. Scatter over all the filling ingredients, spreading as evenly as you can, leaving a small gap along one of the longest sides, brush this edge with a little beaten egg. Season, then roll up tightly from the opposite side and pinch to seal. Use a sharp, floured knife to cut roll into 8 slices. Dust a baking sheet with more flour and place slices, cut-side up, with gaps in between. Brush tops with more egg and bake for 15-20 mins until golden and risen. Best eaten warm with cornichons.
