# Okonomiyaki

* 1 cup all-purpose flour
* 1 tablespoon corn starch
* ½ tsp baking powder
* 2/3 cup stock (dashi, and if dashi isn’t available use any broth)
* 1 egg
* 2 cups finely shredded green cabbage
* ½ lb medium shrimp (51-60 size)
* 3 green onions finely sliced
* 1 tablespoon canola or peanut oil
* Garnishes:
* Mayonnaise and Tonkatsu sauce (see below)
* Finely minced green onions
* Japanese Furikake seasoning (optional but very good-worth seeking out)

1. Combine the flour, cornstarch, and baking powder in a medium mixing bowl. Whisk the broth and egg together and add to the flour mixture to make a medium-thick pancake batter.
2. Add all of the cabbage, shrimp and green onions (will seem like a lot of cabbage, but this is how it should be). Preheat the oil in a non-stick sauté pan over medium high heat. Add the all of the pancake mix and flatten to the depth of the shrimp with a spatula.
3. Lower the heat to medium and cook for about 15 minutes, flipping several times, until golden brown on both sides.
4. Serve right away with the mayonnaise and Tonkatsu sauce drizzled over the top.

Makes one large or two medium pancakes and will serve two people.

## Sauce
* ½ cup ketchup
* ¼ cup Worcestershire sauce
* ¼ cup sake or dry sherry
* ¼ cup dark soy sauce
* ½ tsp sugar

Stir all of the ingredients until the sugar is dissolved, Serve on the side in dipping bowls or drizzled over top of the pancake.