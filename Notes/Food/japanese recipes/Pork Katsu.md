# Pork Katsu With Pickled Cucumbers and Shiso Recipe

## Ingredients

* ½ pound small Kirby cucumbers, sliced into 1/4-inch-thick rounds
* 1 teaspoon coarse kosher salt, more for seasoning
* 1 ¼ teaspoons sugar
* 8 thin slices boneless pork medallions or center-cut pork chops (about 1 1/2 pounds)
* 2 eggs, lightly beaten
* 2 tablespoons Worcestershire sauce
* 1 tablespoon tomato paste
* 2 cups panko crumbs
* ½ cup flour
* Black pepper
* Peanut or vegetable oil, for frying
* 2 tablespoons sliced scallions
* 2 teaspoons rice wine vinegar
* 1 tablespoon minced shiso or basil
* 1 teaspoon soy sauce
* 1 teaspoon toasted Asian sesame oil

## Preparation

1. Place the cucumbers in a colander set over a bowl. Toss them with 1 teaspoon salt and 3/4 teaspoon sugar.
2. Place each piece of pork between sheets of waxed paper. Pound meat to 1/8-inch thickness.
3. Place eggs in a large shallow bowl; whisk in the Worcestershire and tomato paste. Place the panko and flour in two separate shallow bowls.
4. Season cutlets with salt and pepper. Dip each cutlet in the flour (tap off excess), the egg mixture (ditto), then dredge in panko crumbs.
5. Heat a large pan, pour in 1/8 inch of oil and heat for 30 seconds. Working in batches, put cutlets in the pan. Immediately shake and tilt it so the oil rolls over the pork in waves (this will give it a lighter, crisper crust). Shake the pan occasionally, until cutlets are golden on the bottom, about 3 minutes. Flip them and shake again. Cook 2 to 3 minutes longer. Transfer pork to a paper-towel-lined platter to drain.
6. Pat the cucumbers dry with paper towels. Toss with scallions, vinegar, shiso, soy sauce, sesame oil and 1/2 teaspoon sugar. Serve cutlets with pickled cucumbers on the side.
