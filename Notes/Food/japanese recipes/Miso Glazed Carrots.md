# Miso Glazed Carrots

Maggie Zhu | [Link](https://omnivorescookbook.com/glazed-carrots/) 

![Miso Glazed Carrots](./@imgs/recipes/miso_carrots.jpg)

This miso glaze is slightly sweet, buttery, and slightly charred to release its umami, which makes the carrots more irresistible than candy.

Course | Cuisine | Prep Time | Cook Time | Servings | Calories
-- | -- | -- | -- | -- | --
Side | Asian | 5 minutes | 55 minutes | 4 | 161kcal

## Ingredients

- 1 and 1/2 to 2 pounds (700 to 900 grams) carrots , peeled (\*Footnote 1)
- 2 tablespoons olive oil
- 1/4 teaspoon fine sea salt
- 1 tablespoon chopped chive for garnish (Optional)

### Glaze

- 1 tablespoon olive oil
- 3 cloves garlic , minced
- 2 tablespoons white miso paste (\*Footnote 2)
- 1 tablespoon maple syrup
- 1/2 teaspoon cayenne pepper (or paprika for less spiciness)

## Instructions

1. Preheat oven to 175 degrees C (350 F). Line a baking sheet with aluminum foil for easy clean up, if needed.
1. Dry carrots thoroughly and place onto the baking tray, drizzle with olive oil, sprinkle with salt. Toss with a pair of tongs to evenly coat carrots with oil.
1. Bake for 20 to 30 minutes, until the carrots just start to turn tender but are still chewy inside. (\*Footnote 3)
1. Mix all the ingredients for the glaze while roasting.
1. Brush the miso sauce onto both sides of the carrots. You might have a bit of leftover sauce depending on how many carrots you used.
1. Return the baking sheet to the oven and roast for a further 20 to 30 minutes, until the carrots turns tender while piercing with a fork, and the surface is lightly caramelized.
1. Serve warm as a side.

## Notes

1. I used large carrots and it took me 55 minutes in total to roast. If you’re using smaller carrots, especially new carrots, you might halve the time.
1. White miso generates the best result, but red and mixed miso work too.
1. If you’re using much smaller carrots and found out they have turned quite tender before applying miso glaze, you can turn up oven temperature to 400 F to caramelize the glaze without overcooking the carrots.
