# [Ginger Pork Recipe](http://www.japanfoodaddict.com/pork/ginger-pork/)  

Ginger Pork is called "Shogayaki" in Japanese. It means to grill with ginger. It is a standard meal to cook in the home. In Japan you can find shogayaki in a Teishoku-ya (similar to an American diner). The typical way to make Shogayaki is to marinate the pork in the spices before cooking, but a famous TV host named 'Tamori' started a trend of not marinating the pork and now many Japanese cook it in this fashion.                                                         

## Ingredients                                                           
- 1/2 lb sliced pork                                                
- 1/2 medium onion (sliced)                                         
- 1 1/2 cup sliced cabbage                                          
- 1/8 cup flour                                                     

### Spices                                                                
- 3 tsp sake                                                        
- 3 tsp soy sauce                                                   
- 3 tsp mirin                                                       
- 2 tsp grated ginger                                               

## Preparation                                                           
1. Mix spices together in small bowl
1. Sprinkle flour on sliced pork
1. Heat oil in frying pan and fry pork (about 4 minutes)
1. Add onion and sautee for 5 minutes
1. Add spices and cook for about 2 minutes
1. Serve with cabbage and your favorite salad dressing               
