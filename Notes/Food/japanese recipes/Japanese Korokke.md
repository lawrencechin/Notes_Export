# Japanese Korokke
## [Japan Center](https://www.japancentre.com/en/recipes/26-japanese-korokke)

![Korokke](https://japancentre-images.global.ssl.fastly.net/recipes/pics/26/main/photo_Japanese-Korokke.jpg?1469572845)

Crispy and crunchy on the outside, soft and succulent on the inside, these Japanese korokke need to be tried to be believed. Adapted from French croquettes, korokke are mixtures of meat, potato and vegetables that have been moulded into small round patties, covered in panko breadcrumbs, and fried until golden and crispy. Enjoy with rice and salad as a main meal, or make a featured dish in a bento lunch.

### Ingredients

* panko breadcrumbs
* potato
* ground beef or pork
* onion
* flour
* egg
* tonkatsu sauce
* japanese worcestershire sauce
* chuno sauce
* kewpie mayonnaise
* pumpkin
* canned tuna (*optional*)

### How To Prepare

1. The first part of this recipe is preparing the main patty. Start by cooking some roughly chopped potatoes in boiling water until they have cooked. Put them aside and mash them until they are smooth.
2. In a separate frying pan (you can do this while boiling the potatoes), sauté a whole chopped-up onion and then add your ground beef or pork until they have both cooked.
3. Add the meat and onion to your mashed potato and mix them all together in a large bowl. It is a good idea to try and remove some of the oil from the meat before you mix with the potato to avoid making a greasy korokke.
4. Season your mix with salt and pepper and then make round patties about 10cm wide and 2-3cm high.
5. Fill three separate bowls each with a generous amount of flour, beaten eggs and panko Japanese breadcrumbs. Dip each of the korokke patties into the flour first, then the beaten eggs and finally the panko. Make sure you get plenty of panko on each korokke to make sure that they become nice and crispy when you cook them.
6. Once you have prepared all the korokke you need, heat up cooking oil in a pan and fry the korokke until they are golden brown and crispy.
7. Serve with a massive squeeze of sauce or mayonnaise (or both) and add some sliced cabbage for an authentic way of serving this great dish. You can use many different types of Japanese sauce for korokke, but the most common varieties are tonkatsu sauce, Japanese worcester sauce and chuno sauce (they are all very similar in flavour so are interchangeable with each other).

### Tips and Information

* Prefer a vegetarian option? If so of course you can simply remove the meat and make with potato only. Alternatively, using Japanese pumpkin called kabocha makes a delicious, slightly sweet vegetarian korokke that is very popular in Japan.
* Beef, pork or pumpkin not your thing? No problem, try using canned tuna mixed with potato for a fish based korokke instead.
* Already had potato for lunch today? There is still a choice for you… Rice korokke is made by mixing rice with a little ketchup before dipping into the flour, egg and panko and then frying.