# The Easy Japanese Cookbook: 250 Traditional And Modern Recipes With Step-By-Step Guidelines
Ayami Uchiumi

## Introduction
### Japanese cuisine

The chef from Japan is focused and exact. He frequently makes the same meal year after year, which has led to the overall improvement and perfection of Japanese cuisine. Japanese culture has developed a rich culinary legacy through the years. In 2013, UNESCO recognized this custom and added it to the organization's list of intangible ("living") cultural assets. Additionally, Japanese eateries are highly specialized. In contrast to what we are accustomed to in the West, most Japanese restaurants don't provide a wide variety of various cuisines. If you're in the want for okonomiyaki, visit an okonomiyaki-ya; if you're in the mood for sushi, visit a sushiya or a kaitenzushi, where the sushi is served on a conveyor belt. The selection is only somewhat wider in teishokuya, where they serve fixed meals, and izakayas, which are more casual eating and drinking establishments. However, the menu in Japanese restaurants in the West typically offers a variety of items.

### Outside influences

Japan was a "closed country" (sakoku) for many years, isolated from outside influences. Japanese citizens were not permitted to travel overseas on a casual basis, and neither were foreigners permitted entry. Only a few port cities allowed contact. Before the Japanese came into touch with Portuguese and Dutch traders, for instance, they did not eat any meat, therefore Japanese food remained pure and traditional for a very long time. The Japanese eventually adopted numerous Western foods and cooking techniques when the government gave the go-ahead, including deep-frying, the use of batter and breadcrumbs for dishes like tempura and tonkatsu, noodles like yakisoba, and noodle soup like ramen.

### Typical ingredients

Without three crucial ingredients, Japanese cuisine would not be Japanese cuisine: rice, soybeans, and liquid spices.

#### Rice
In Japan, rice is the foundation of every meal. It is adored in Japan and consumed in a variety of forms, including onigiri and mochi (rice balls and elastic dough balls). Short-grain rice, often known as Japanese rice (japonica rice or Japanese hakumai), is firm and slightly sticky after cooking. It must be thoroughly rinsed several times before being wet. It's ideal to take both of these processes carefully because chefs don't always agree on which is the most crucial. Less than 15 minutes must pass after cooking the rice for the water to evaporate. Cook the rice in dashi (broth) to make shari (sushi rice). The rice is eagerly tossed into the cool air while the sushi sushizu (sushi vinegar) is pushed through while the rice is still heated. The rice will become more shiny as it cools and the vinegar is absorbed. When serving the sushi-shaped rice, it should be lukewarm. It shouldn't be abruptly cooled down. That lesson has been lost in our Western stores, where the sushi is pre-packaged and kept in the refrigerator.

#### Soybeans
Soybeans are utilized in Japanese cuisine in a variety of ways. edamame, for instance, which is completely unadulterated, young, and straight from the plant, or fermented foods like natto, shoyu (soy sauce), or miso. Curdled soy milk is used to make tofu, which is subsequently prepared by grilling (atsuage) or deep-frying and covering (aburaage). Even the skin that forms when soy milk is boiled is consumed.

#### Liquid flavourings
Japanese cuisine does not really include sauces as we know them but uses a lot of liquid flavourings. The most popular is shoyu (soy sauce). This sauce, which is created from high-quality soybeans, roasted crushed wheat, sea salt, and spring water, is used in moderation to provide umami---never too much would be overpowering. In addition to being used to make dipping sauces, dressings, and preserving liquids, rice vinegar is a necessary ingredient in the preparation of sushi rice.

Mirin is sweeter than sake, although both are rice wines. Both wines come in various qualities; some can be consumed straight up, while others are employed in cooking.

Sushizu is a concoction of rice vinegar, sugar, salt, mirin, or sake. It is also referred to as awasezu.

Japanese dashi is broth. It serves as the foundation for numerous cuisines, including takoyaki and miso soup. Kombu (kelp), katsuobushi (bonito flakes), and occasionally niboshi (dried sardines) are the main ingredients in dashi. The outcome is an umami-rich broth that provides a flavorful foundation for soups, sauces, rice, and batter.

### Japanese Dining Etiquette

Japanese folks typically eat at low tables while sitting on floor cushions. In a formal situation, one eats while kneeling, while in a casual one, one sits cross-legged (for men) or with both legs crossed
(for women). The kamiza, or place of honor, is where the honored guest or family head is seated and is located the farthest from the entryway. You will receive a sizzling hot towel to clean your hands in eateries. Do not wipe your face with the cloth. It is customary for the host, family patriarch, or waiter to signal the start of the meal with the term "itadakimasu". Since practically everything in Japan is bite-sized, it is intended that you eat everything at once rather than cutting a piece of sushi in half. Even soup is consumed with chopsticks. You consume all of the "solid" ingredients with chopsticks and slurp (or drink) the remaining ingredients. Slurping is beneficial because the food tastes better when air is mixed with it (at least, that's the story...

## Easy Japanese Recipes for Quick Weeknight Dinners

### Beef Sukiyaki

![](./@imgs/recipes/tejc_imgs/00001.jpeg)

Prep Time: 30 mins | Cook Time: 15 mins | Total Time: 45 mins | Servings: 4

#### Ingredients

- 1 ½ cups prepared dashi stock
- ¾ cup soy sauce
- ¾ cup mirin
- ¼ cup white sugar
- 8 ounces shirataki noodles
- 3 tbsps canola oil, divided
- 1 pound beef top sirloin, thinly sliced
- 1 medium onion, thinly sliced
- 2 medium carrots, thinly sliced
- 2 stalks celery, thinly sliced
- 4 ounces sliced fresh mushrooms (button, shiitake, or
  enoki)
- 5 green onions, cut into 2-inch pieces
- 1 (14 ounce) package firm tofu, cut into cubes

#### Directions

1. In a bowl, mix the dashi, soy sauce, mirin, and sugar.
1. Noodles should be boiled for one minute. Drain once more after rinsing under cold water.
1. 2 tbsps of oil are heated in a pot on medium heat. Add the meat, and cook and stir for 2 to 3 minutes, or until no longer pink. Place the steak on a platter.
1. Over medium heat, add the remaining 1 tbsp oil to the pot. Add the onion, carrots, celery, and mushrooms; simmer and stir for 4 minutes, or until the vegetables are tender. Bring to a simmer the dashi sauce, noodles, meat, tofu, and green onions.
1. After taking it off the fire, divide the hot sukiyaki among four bowls.

### Japanese-Style Crispy Fried Pork Bowl (Tonkatsu Donburi)

![](./@imgs/recipes/tejc_imgs/00002.jpeg)

Prep Time: 25 mins | Cook Time: 7 mins | Total Time: 32 mins | Servings: 2

#### Ingredients

- ⅔ cup prepared dashi stock
- 2 tsps white sugar
- 2 tsps mirin (Japanese sweet wine)
- 2 tsps soy sauce
- ½ tsp salt
- 1 tbsp vegetable oil
- 1 small onion, chopped
- 2 (3 ounce) fried breaded pork chops, sliced into long
  strips
- 2 eggs, beaten
- 1 ½ cups cooked short-grain rice

#### Directions

1. In a small bowl, combine the dashi, sugar, mirin, soy sauce, and salt.
1. In a small skillet over medium heat, warm the vegetable oil. Add the onion; stir-fry and simmer for 5 minutes, or until transparent. Add the dashi mixture and stir. On top of the onions, carefully arrange slices of pork. Spread eggs all over the pieces. Cook eggs in a covered skillet for 2 minutes or until they are set.
1. Give each bowl of rice a portion. Add half of the onions, eggs, and pork slices to each plate.

#### Cook's Note:

If preferred, swap out the dashi for chicken stock.

### Soba Noodle Veggie Bowl

![](./@imgs/recipes/tejc_imgs/00003.jpeg)

Prep Time: 15 mins | Cook Time: 10 mins | Total Time: 25 mins | Servings: 1

#### Ingredients

- 3 ounces dried soba noodles
- 2 tbsps soy sauce
- 2 tbsps oyster sauce
- 1 tbsp mirin (Japanese sweet wine)
- 1 tbsp olive oil
- ½ green bell pepper, chopped
- ¼ onion, sliced
- ½ cup fresh green beans, trimmed and cut into 1 1/2-inch
  pieces
- 1 cup baby spinach leaves

#### Directions

1. Lightly salted water should be added to a big saucepan and brought to a rolling boil. Add soba noodles and bring to a boil once more. Sauté uncovered for about 7 minutes, stirring periodically, until fork-tender but still firm. Drain.
1. In the meantime, mix mirin, oyster sauce, and soy sauce in a small bowl. Set aside.
1. In a skillet, heat the olive oil over medium-high heat. Add the bell pepper, onion, and green beans and cook for 2 to 3 minutes, or until softened but still crisp. Toss in the soba noodles and the cooked veggies. Add the soy sauce mixture and continue cooking for another 1--2 minutes. For 2 to 3 minutes, add baby spinach and cook until desired doneness is achieved. Dish it out in a bowl.

### Chicken Katsu

![](./@imgs/recipes/tejc_imgs/00004.jpeg)

Prep Time: 15 mins | Cook Time: 10 mins | Total Time: 25 mins | Servings: 4

#### Ingredients

- 4 skinless, boneless chicken breast halves - pounded to 1/2 inch thickness
- salt and pepper to taste
- 2 tbsps all-purpose flour
- 1 egg, beaten
- 1 cup panko bread crumbs
- 1 cup oil for frying, or as needed

#### Directions

1. Chicken breasts should be salt and pepper-seasoned on both sides. Put the panko crumbs, beaten egg, and flour into three separate shallow bowls. Shake off any excess flour from the chicken breasts before coating with flour, dipping in eggs, and pressing into panko crumbs to thoroughly coat both sides.
1. In a large skillet, heat the oil over medium-high heat. Put the chicken in the hot oil and fry for 3 to 4 minutes on each side, or until golden brown. Move to a plate covered with paper towels to drain.
1. a tonkatsu-topped fried chicken cutlet with salad in the backdrop

#### Tips

Consider dipping this in some Japanese salad dressing.

### Tonkatsu (Japanese-Style Crispy Fried Pork Cutlets)

![](./@imgs/recipes/tejc_imgs/00005.jpeg)

Prep Time: 20 mins | Cook Time: 4 mins | Total Time: 24 mins | Servings: 4

#### Ingredients

- 2 boneless pork chops, trimmed of excess fat
- ½ tsp salt
- ¼ tsp ground black pepper
- 1 egg
- ½ tsp soy sauce
- ¼ cup all-purpose flour
- ½ cup panko bread crumbs
- oil for frying

#### Directions

1. To remove any extra moisture, place the pork chops on a piece of paper towel. Add salt and pepper to taste.
1. In a small bowl, mix the egg and soy sauce.
1. Put panko on one tiny plate and flour on the other. A pork chop should be well covered in flour by being dredged in it and pressing it in with your fingertips. Flip to evenly coat all sides.
1. Coat the pork completely by dipping it into the egg mixture. Add to the panko bowl right away, pressing down to coat evenly. Follow the same procedure for the second pork chop.
1. Using medium-high heat, bring the oil in a large skillet or wok to 350 degrees F (175 degrees C). A pork chop is lowered into the oil. Fry for 2 to 3 minutes, or until the bottom side is golden. 2 to 3 minutes more after turning; cook until the pork is slightly pink in the center. In the center, an instant-read thermometer should register at least 145 degrees Fahrenheit (63 degrees Celsius). To allow the oil to drip off, pick up the cutlet and hold it on its side for a short period of time. Drain on a piece of paper. Continue by using the second pork chop.

#### Cook's Note:

Most grocery stores have panko, a type of big bread crumb that is Japanese in origin.

### Miso-Glazed Black Cod

![](./@imgs/recipes/tejc_imgs/00006.jpeg)

Prep Time: 10 mins | Cook Time: 15 mins | Additional Time: 20 mins | Total Time: 45 mins | Servings: 2

#### Ingredients

- cooking spray
- 3 tbsps white miso paste
- 2 tbsps water
- 2 tbsps mirin (Japanese sweet wine)
- 2 tbsps sake
- 1 tbsp brown sugar
- 2 (7 ounce) black cod fillets

#### Directions

1. Preheat the oven's broiler to high and place a rack about 6 inches from the heat source. Cooking spray should be used to lightly lubricate the aluminum foil used to line the baking sheet.
1. In a small skillet over medium heat, combine miso, water, mirin, sake, and brown sugar. Stir the mixture until it simmers and slightly thickens, about 1 to 3 minutes. After 3 to 5 minutes, turn off the heat and allow the food to cool fully.
1. Cod fillets should be put on the prepared baking sheet. Fillets should be evenly coated in the cooled miso mixture. 15 to 20 minutes at room temperature for marinating
1. For five minutes, broil in the preheated oven. Flip the baking sheet 180 degrees, then continue broiling the salmon for an additional 5 minutes or until it flaked readily with a fork. Before serving, remove the pin bones.

### Japanese Shrimp Fried Rice with Yum Yum Sauce

![](./@imgs/recipes/tejc_imgs/00007.jpeg)

Prep Time: 15 mins | Cook Time: 35 mins | Total Time: 50 mins | Servings: 6

#### Ingredients

Rice:

- 2 cups uncooked jasmine rice
- 3 cups water
- 3 tbsps vegetable oil, divided
- 1 sweet onion, chopped
- 2 cloves garlic, crushed and minced
- 1 (16 ounce) package frozen peas and carrots
- 4 tbsps butter, divided
- 2 eggs
- 4 tbsps oyster sauce
- 3 tbsps soy sauce
- 1 lemon, juiced, divided
- salt and pepper to taste
- 1 pound uncooked medium shrimp, peeled and deveined

Yum Yum Sauce:

- 1 cup mayonnaise
- 3 tbsps water
- 2 tbsps paprika
- 1 tsp ginger paste
- 1 tsp white sugar
- ½ tsp garlic powder
- salt and pepper to taste

#### Directions

1. In a saucepan, bring 3 cups of water and the rice to a boil. Lower the heat to medium-low, cover, and simmer for 20 to 25 minutes, or until the rice is soft and the water has been absorbed. Cool off and set aside.
1. In a big, deep skillet over medium-high heat, warm 2 tbsps of vegetable oil. Add the onion and simmer for about 5 minutes, or until tender and transparent. Add the garlic, stir, and cook for approximately a minute, until fragrant. Add the cooked jasmine rice and the frozen pea-carrot mixture, and stir-fry for 5 minutes, or until the rice starts to turn golden. Stir in 2 tbsps of butter after adding it. Add the eggs and heat until they are set. Stir in the oyster sauce, soy sauce, and half of the lemon juice. Add salt and pepper to taste.
1. In another skillet, heat the final tbsp of vegetable oil over medium-high heat. Add the shrimp and cook for 2 to 3 minutes, or until the outside is a vibrant pink color and the meat is opaque. Lemon juice and the final 2 tbsps of butter are combined. With the fried rice mixture, combine.
1. You make the yum yum sauce by combining mayonnaise, water, paprika, ginger paste, white sugar, garlic powder, salt, and pepper in a bowl. Stir thoroughly. Serve the fried rice with it.

### Japanese Country-Style Miso and Tofu (Hiya Shiru)

![](./@imgs/recipes/tejc_imgs/00008.jpeg)

Prep Time: 15 mins | Cook Time: 2 mins | Total Time: 17 mins | Servings: 6

#### Ingredients

- 2 tbsps sesame seeds
- ½ cup dried Asian-style whole sardines
- 2 ½ tbsps red miso paste
- ½ cup boiling water
- 1 (16 ounce) package silken tofu, cubed
- 4 green onions, thinly sliced
- 1 pinch crushed red pepper flakes

#### Directions

1. Stir the sesame seeds and dried sardines for about two minutes over medium heat, until they release their aroma but don't burn. In the work bowl of a small food processor, combine the sesame seeds and dried sardines. Pulse several times to create a fine powder.
1. To form a very thick paste, combine the dry sardine mixture with the miso in a big bowl. Cubed tofu, green onions, and red pepper flakes are gently stirred into the smooth, creamy mixture after adding boiling water and mixing until that consistency is reached.

### Omelet for Leftovers (Omurice)

![](./@imgs/recipes/tejc_imgs/00009.jpeg)

Prep Time: 25 mins | Cook Time: 10 mins | Total Time: 35 mins | Servings: 1

#### Ingredients

- 1 ½ tsps butter
- 1 small tomato, sliced, or more to taste
- ½ red bell pepper, sliced
- ¼ onion, sliced
- ¼ zucchini, sliced
- 1 ounce sliced mushrooms
- ½ cup warm cooked rice
- 1 tbsp ketchup, or to taste
- 1 slice cooked bacon, chopped, or more to taste
- ½ tsp paprika
- salt and ground black pepper to taste
- 2 eggs, lightly beaten

#### Directions

1. In a skillet set over medium heat, melt the butter. Add the tomato, red bell pepper, onion, zucchini, and mushrooms; simmer and stir for 5 minutes, or until the vegetables are soft. Stir together the rice, ketchup, bacon, paprika, salt, and black pepper. Place on serving plate.
1. A nonstick skillet should be heated to medium. Eggs should be added in a thin layer and cooked for 5 minutes or until set. Over the rice mixture, place eggs.

### Sesame Chicken and Soba Noodles

![](./@imgs/recipes/tejc_imgs/00010.jpeg)

Prep Time: 10 mins | Cook Time: 15 mins | Total Time: 25 mins | Servings: 4

#### Ingredients

- 1 (8 ounce) package dried soba noodles
- 3 tbsps soy sauce
- 3 tbsps brown sugar
- 1 pound boneless, skinless chicken breasts, cubed
- 2 tbsps sesame seeds
- ½ cup olive oil
- 3 tbsps rice vinegar
- 1 tbsp finely grated ginger
- 1 (7 ounce) package mixed salad greens

#### Directions

1. Lightly salted water should be added to a big saucepan and brought to a rolling boil. Bring the water back to a boil before adding the soba noodles. Noodles should be soft yet firm to the bite after cooking uncovered for 5 to 7 minutes. Drain, and then run a cold water rinse.
1. Brown sugar and soy sauce are heated in a large skillet over medium heat. Add the chicken, stir, and cook for 5 to 7 minutes, or until the chicken is no longer pink in the center and the juices are clear. Put in a bowl and coat with sesame seeds after being transferred there.
1. In a bowl, combine the vinaigrette ingredients: olive oil, vinegar, and ginger. Place greens on each of four plates, then add chicken and soba noodles. With vinaigrette, serve.

### Japanese Curry

![](./@imgs/recipes/tejc_imgs/00028.jpeg)

Prep Time: 30 mins | Cook Time: 1 hrs 10 mins | Total Time: 1 hrs 40 mins | Servings: 8

#### Ingredients

- 1 tbsp vegetable oil, or more as needed
- 1 ¾ pounds beef chuck, cut into 2-inch cubes
- 3 onions, quartered
- 1 tbsp ketchup
- 1 ½ tsps Worcestershire sauce
- 1 pinch cayenne pepper, or to taste (Optional)
- water to cover
- 4 carrots, cut into 2-inch pieces
- 1 cube chicken bouillon (Optional)
- 3 medium potatoes, cut into 3-inch chunks
- 1 ½ (3.5 ounce) containers Japanese curry roux, or more to
  taste

#### Directions

1. In a 6-quart pot, heat the oil over medium-high heat. For 5 to 7 minutes, add the beef and cook until brown. Around 3 minutes after adding the onions, they should start to soften. Worcestershire and ketchup should be added. To coat, stir. Cayenne pepper is added. Add water until the mixture is covered by 1 to 2 inches. Include bouillon and carrots.
1. Simmer for 30 minutes, scraping off as much fat as necessary from the broth's surface. Put potatoes in. Add the remaining curry as needed to reach the desired thickness after stirring in 1 packet of the curry roux and letting it dissolve. For another 30 minutes or so, boil the beef and vegetables until they are soft.

#### Cook's Notes:

Use your preferred beef stew meat. If preferred, swap out the Worcestershire with soy sauce. In the international section of an international grocery store, curry roux is available in boxes. Several manufacturers offer a variety of heat intensities. S&B(R) and Vermont Curry appeal to me. Normally, I combine the two brands. The sauce thickens as you add additional roux. The longer it simmers, similar to most stews, the better it will taste. You can freeze this without the potatoes as well.

### Authentic Miso Soup

![](./@imgs/recipes/tejc_imgs/00012.jpeg)

Prep Time: 15 mins | Cook Time: 15 mins | Total Time: 30 mins | Servings: 4

#### Ingredients

- 4 cups water
- 1 (4 inch) piece dashi kombu (dried kelp)
- ½ cup bonito flakes
- ½ (12 ounce) package tofu, cut into chunks
- 1 tsp dried wakame
- 3 tbsps miso paste
- ¼ cup chopped green onions

#### Directions

1. On low heat, warm up the water in a big pot. When the liquid barely starts to simmer, add the kombu. Add the bonito flakes and mix well. After turning off the heat, let the dashi sit for five minutes with the lid off. Put it aside after straining.
1. In a pot, warm 3 1/2 cups of dashi over medium heat. Stir in the tofu and wakame after adding them. Whisk 1 cup of the warmed dashi with the miso paste in a small bowl. Refill the pot with the miso mixture and leftover dashi. Stir until thoroughly hot. Green onions, chopped, to serve as a garnish.

### Sesame Seared Tuna and Sushi Bar Spinach Salad

![](./@imgs/recipes/tejc_imgs/00013.jpeg)

Prep Time: 15 mins | Cook Time: 5 mins | Total Time: 20 mins | Servings: 2

#### Ingredients

Spinach Salad:

- ½ pound baby spinach leaves
- 3 tbsps white sesame seeds
- 1 tbsp white sugar
- 1 tbsp soy sauce, or to taste
- ½ tsp mirin

Miso Mayo Sauce:

- ¼ cup mayonnaise
- 2 tsps white miso paste
- 1 tbsp seasoned rice vinegar

Seared Tuna:

- 2 (5 ounce) sushi-grade ahi tuna steaks
- salt to taste
- 2 tbsps black sesame seeds
- 2 tsps vegetable oil
- 1 tbsp prepared ponzu sauce

#### Directions

1. In a dry skillet over medium-high heat, add the spinach and cook, stirring occasionally, for 1 to 2 minutes, or until it just starts to wilt. To cool, transfer to a strainer.
1. White sesame seeds can be lightly browned while spinach cools by toasting them in a dry pan over medium heat. With some seeds still whole, transfer to a mortar and pestle and grind into an extremely coarse pulp. Add white sugar, mirin, and soy sauce. Combine ingredients with a wooden spoon, then set aside.
1. Spinach should be transferred to a towel and any extra liquid squeezed out. Roughly chop, then add to a mixing bowl. Mix thoroughly after adding the dressing. Before serving, cover and fully chill.
1. To make miso mayo sauce, combine mayonnaise, miso paste, and rice vinegar. Put in the fridge until required.
1. Tuna steaks should be heavily salted and then covered with as many sesame seeds as you want and lightly pressed into the surface.
1. Place a nonstick pan over medium heat after brushing it with oil. Tuna steaks should be seared in a hot pan for 30 to 45 seconds on each side and around the edges.
1. Tuna should be sliced and laid over the miso sauce. Serve tuna with a side of spinach salad and ponzu sauce.

### Tofu Hiyayakko

![](./@imgs/recipes/tejc_imgs/00014.jpeg)

Prep Time: 10 mins | Total Time: 10 mins | Servings: 1

#### Ingredients

- 1 tbsp soy sauce
- 1 tsp white sugar
- ½ tsp dashi granules
- ½ tsp water
- ¼ (12 ounce) package silken tofu
- 1 ½ tsps grated fresh ginger root
- ¼ tsp thinly sliced green onion
- 1 pinch bonito shavings (dry fish flakes)
- 1 pinch toasted sesame seeds

#### Directions

1. Together in a small bowl, stir the soy sauce, sugar, dashi grains, and water until the sugar dissolves. On a small plate, arrange the tofu and garnish with the ginger, green onion, and bonito shavings. Add sesame seeds and drizzle the soy mixture on top.

#### Cook's Note

This soy-dashi dressing (Tsuyu) can be kept in the refrigerator for approximately a week if you prepare a lot of it. It can be used for anything, including salad, steamed salmon, boiled chicken, and boiled spinach.

### Nikujaga (Japanese-style meat and potatoes)
 
![](./@imgs/recipes/tejc_imgs/00029.jpeg)

Prep Time: 15 mins | Cook Time: 35 mins | Total Time: 50 mins | Servings: 4

#### Ingredients

- 8 snow peas
- 1 tbsp vegetable oil
- ¼ pound sirloin steak, thinly sliced
- 4 potatoes, cut into bite sized pieces
- 2 cups dashi soup
- ¼ cup soy sauce
- ¼ cup sake
- 1 tbsp white sugar
- 1 onion, chopped

#### Directions

1. Together in a small saucepan, cover the snow peas with water, bring to a boil, and then immediately remove from the heat. Drain, then set apart.
1. Large skillet with oil heated over medium heat; add steak and cook until browned. Cook and toss the potatoes for 5 to 7 minutes, or until they are tender. Pour in the sake, sugar, soy sauce, and dashi soup before simmering for 10 minutes.
1. Turn down the heat to low and sprinkle the chopped onion over the mixture. Continue to simmer for another 15 minutes or more, or until the liquid has almost totally evaporated. To serve, add the snow peas on top of the mixture.

### Nasu Dengaku

![](./@imgs/recipes/tejc_imgs/00016.jpeg)

Prep Time: 10 mins | Cook Time: 10 mins | Total Time: 20 mins | Servings: 4

#### Ingredients

- 4 Japanese eggplants, cut in half lengthwise and stems removed
- 1 tbsp apple juice
- 1 tbsp cranberry juice
- 2 tbsps rice vinegar
- ¼ cup white miso paste
- 3 tbsps agave nectar
- 1 tsp sesame oil
- 2 tsps sesame seeds
- 1 green onion, sliced

#### Directions

1. Set the oven rack about 6 inches away from the heat source and preheat the oven's broiler.
1. In a small saucepan over medium heat, warm the apple juice, cranberry juice, and vinegar. Miso paste and agave nectar should be thoroughly mixed in. Turn the heat down to low and stir occasionally to maintain warmth. Sesame oil should be brushed over the cut side of the eggplants. Put the eggplants in a shallow baking dish with the cut side up.
1. For three minutes while the oven is preheated, broil, monitoring occasionally to make sure they don't burn. Broil eggplants for three minutes on the other side, or until they are golden brown and soft. Each eggplant should have miso sauce drizzled over it. Broil each eggplant for 1 minute, or until the miso starts to bubble. Sesame seeds and green onions are used as garnish. Serve warm.

### Chicken Yakisoba

![](./@imgs/recipes/tejc_imgs/00017.jpeg)

Prep Time:20 mins | Cook Time: 15 mins | Total Time: 35 mins | Servings: 4

#### Ingredients

- 2 tbsps canola oil
- 1 tbsp sesame oil
- 2 skinless, boneless chicken breast halves - cut into bite-size pieces
- 2 cloves garlic, minced
- 2 tbsps Asian-style chile paste
- ½ cup soy sauce
- 1 tbsp canola oil
- ½ medium head cabbage, thinly sliced
- 1 onion, sliced
- 2 carrots, cut into matchsticks
- 1 tbsp salt
- 2 pounds cooked yakisoba noodles
- 2 tbsps pickled ginger, or to taste (Optional)

#### Directions

1. Sesame oil and 2 tbsps of canola oil are heated over medium-high heat in a large skillet. In hot oil, stir-fry chicken and garlic for approximately a minute, or until aromatic. Add the chile paste to the chicken mixture and stir while cooking for 3 to 4 minutes, or until the chicken is thoroughly browned. Stir in the soy sauce and cook for two minutes. In a bowl, add the chicken and the sauce.
1. Cook and stir the cabbage, onion, carrots, and salt in hot oil until the cabbage is wilted, 3 to 4 minutes, in a skillet with 1 tbsp canola oil heated to medium-high heat.
1. Combine the cabbage mixture with the chicken mixture. Add the noodles; cook and toss for 3 to 4 minutes, or until the noodles are heated and the chicken is no longer pink inside. Ginger pickles are a nice garnish.

### Sesame-Seared Tuna

![](./@imgs/recipes/tejc_imgs/00018.jpeg)

Prep Time: 10 mins | Cook Time: 1 mins | Total Time: 11 mins | Servings: 4

#### Ingredients

- ¼ cup soy sauce
- 2 tbsps sesame oil
- 1 tbsp mirin (Japanese sweet wine)
- 1 tbsp honey
- 1 tbsp rice wine vinegar
- ½ cup sesame seeds
- 4 (6 ounce) tuna steaks
- 1 tbsp olive oil
- wasabi paste

#### Directions

1. In a small bowl, combine the soy sauce, sesame oil, mirin, and honey. As a dipping sauce, pour half into a separate, little bowl, combine with rice wine vinegar, and set aside.
1. On a plate, spread out the sesame seeds. Apply the remaining soy sauce mixture to the tuna steaks, then press into the sesame seeds to coat.
1. Cast iron skillet filled with hot olive oil that has been heated on high heat. Steaks should be seared for about 30 seconds on each side in the pan. Serve with wasabi paste and dipping sauce.

### Stir-Fried Japanese Ginger Pork

![](./@imgs/recipes/tejc_imgs/00019.jpeg)

Prep Time: 25 mins | Cook Time: 11 mins | Additional Time: 5 mins | Total Time: 41 mins | Servings: 4

#### Ingredients

- 1 daikon radish, peeled and cut into 2-inch sticks
- 2 tbsps soy sauce
- 2 tbsps sake (Japanese rice wine)
- 1 ½ tsps sesame oil
- 1 tbsp fresh grated ginger
- 2 cloves garlic, minced
- 1 tsp mirin (Japanese sweet rice wine)
- 11 ounces pork fillet, thinly sliced
- 1 tbsp vegetable oil
- 1 onion, thinly sliced
- salt and ground black pepper to taste

#### Directions

1. Together in a sizable bowl, combine the daikon radish, soy sauce, sake, sesame oil, ginger, garlic, and mirin. Add the sliced pork and let it sit for five minutes.
1. In a wok or sizable skillet, heat vegetable oil over medium heat. Add the onion and simmer and stir for 3 to 5 minutes, or until tender. Pork should be taken out of the marinade and added to the wok. Cook and stir for 3 to 5 minutes, or until uniformly browned. Add the remaining marinade; cover and cook for 5 minutes, or until bubbling. Add salt and black pepper to taste.

### Yakitori Chicken

![](./@imgs/recipes/tejc_imgs/00020.jpeg)

Prep Time: 25 mins | Cook Time: 20 mins | Total Time: 45 mins | Servings: 6

#### Ingredients

- ½ cup sake
- ½ cup soy sauce
- 1 tbsp sugar
- 1 clove garlic, crushed
- 1 (2 inch) piece fresh ginger root, grated
- 1 pound skinless, boneless chicken breast meat - cubed
- 3 leeks, white part only, cut into 1/2 inch pieces

#### Directions

1. Combine the sake, soy sauce, sugar, garlic, and ginger in a medium dish. Add the chicken and give it 15 minutes to marinade.
1. Activate the broiler in your oven. Six metal skewers should be greased before alternately threading three pieces of chicken and two pieces of leek. Brush the marinade over the meat before placing it on a baking sheet or broiling pan.
1. The chicken should be cooked through after around 5 minutes of broiling, after which it should receive another 5 minutes of basting. Throw away any leftover marinade.

## Japanese Beef Recipes for Simple Comforting Dinners
### Easy Japanese Roast

![](./@imgs/recipes/tejc_imgs/00021.jpeg)

Prep Time: 15 mins | Cook Time: 12 hrs | Total Time: 12 hrs 15 mins | Servings: 8

#### Ingredients

- 2 onions, finely chopped
- 5 cloves garlic, minced
- 1 (3 1/2) pound beef roast
- 1 (14.5 ounce) can onion-flavored beef broth
- 1 ⅓ cups rice wine
- ⅔ cup soy sauce
- 4 slices fresh ginger
- 2 tbsps white sugar
- ground black pepper to taste

#### Directions

1. In a 5-quart slow cooker, layer the bottom with the onions and garlic. On top, place the roast.
1. In a different bowl, combine rice wine, soy sauce, and beef broth. Add slices of ginger and pour over the roast. Roast should be dusted with sugar and black pepper.
1. Roast should be covered and cooked on low for 12 to 18 hours, turning it two or three times.

### Beef Kushiyaki

![](./@imgs/recipes/tejc_imgs/00022.jpeg)

Prep Time: 15 mins | Cook Time: 10 mins | Additional Time: 30 mins | Total Time: 55 mins | Servings: 8

#### Ingredients

- 2 ½ tbsps mirin (Japanese sweet wine)
- 1 clove garlic, minced
- ⅓ cup soy sauce
- ⅛ tsp monosodium glutamate (such as Aji-No-Moto®)
- ⅓ cup white sugar
- 1 pound beef sirloin steak, cut paper-thin
- 3 green onions, cut into 2-inch slices
- bamboo skewers

#### Directions

1. Stir the sugar in a bowl after adding the mirin, garlic, soy sauce, and monosodium glutamate. Stir the marinade into the beef and green onions, then set the mixture aside for 30 minutes to marinate. Water-soaked bamboo skewers are used.
1. Set the oven rack about 6 inches away from the heat source and preheat the oven's broiler.
1. Using two bamboo skewers spaced about 1/2 inch apart, skewer a thin slice of beef around a piece of green onion. Put three meat buns on the two skewers twice more. Continue until all of the beef has been folded up, using the remaining beef, green onion, and skewers. Skewers should be put on a broiling pan. Delete the used marinade.
1. 3 to 3 1/2 minutes per side under the broiler, or until both sides are browned.

### Traditional Beef Sukiyaki

![](./@imgs/recipes/tejc_imgs/00023.jpeg)

Prep Time: 25 mins | Cook Time: 11 mins | Total Time: 36 mins | Servings: 4

#### Ingredients

Broth:

- 1 ½ cups water
- ⅔ cup soy sauce
- ⅔ cup white sugar
- ⅓ cup sake

Sukiyaki Ingredients:

- 1 pound thinly sliced beef
- 1 (12 ounce) package firm tofu, drained and cut into bite-size
  pieces
- ½ head Chinese cabbage, cut into bite-size pieces
- 1 (7 ounce) package yam noodles (shirataki), drained
- 7 shiitake mushrooms, sliced
- 1 enoki mustrooms, roots removed
- 1 green onion (negi), sliced
- 1 tbsp vegetable oil
- 4 eggs

#### Directions

1. To prepare broth, mix water, soy sauce, sugar, and sake in a bowl.
1. Put separate plates of beef, tofu, Chinese cabbage, yam noodles, shiitake, enoki, and green onion on the table.
1. Oil should be heated in a big skillet at the table or in an electric skillet. Add the meat pieces, and cook and stir for approximately a minute, or until browned. Bring to a boil after adding some broth. Add tofu, cabbage, noodles, shiitake, enoki, and green onion; simmer for about 5 minutes or until softened.
1. Pour the cooked sukiyaki mixture into dishes for serving. Refill the skillet's broth.
1. Each egg should be cracked into a small basin and lightly beat. Eggs should be served with sukiyaki for dipping.

#### Cook's Note:

Whether using dried shiitake mushrooms, rehydrate according to the instructions on the package.

### Instant Pot Japanese Curry

![](./@imgs/recipes/tejc_imgs/00024.jpeg)

Prep Time: 30 mins | Cook Time: 35 mins | Additional Time: 10 mins | Total Time: 1 hrs 15 mins | Servings: 6

#### Ingredients

- 1 ½ tbsps canola oil
- 1 onion, chopped
- 1 ½ pounds beef, cut into 1/2-inch or 1-inch cubes
- 2 cloves garlic, minced
- 1 tsp grated fresh ginger
- 2 medium potatoes, peeled and cubed
- 2 carrots, peeled and chopped into 1/2-inch pieces
- ¼ tsp salt
- ¼ tsp ground black pepper
- 2 ½ cups beef broth
- 1 small apple, grated
- 1 tbsp ketchup
- 1 tbsp soy sauce
- 1 (3.5 ounce) container Japanese curry roux

#### Directions

1. Choose the sauté function on a multipurpose pressure cooker (such as an Instant Pot®). In a pot, warm the oil. Add the onion and cook for three minutes, or until almost transparent. Add the steak, garlic, and ginger and cook for an additional 3 minutes, or until the beef starts to brown. Add the carrots and potatoes. Cook for two minutes while stirring constantly. Add salt and pepper to taste.
1. Fill the pot with the beef broth, apple, ketchup, and soy sauce. Blocks of the curry roux should be broken up and added on top of the beef mixture. Keep the blocks out of the soup.
1. Offset the sauté function. Lock the lid by closing it. shutoff valve. Set the timer for 15 minutes while choosing high pressure in accordance with the manufacturer's recommendations. Give the pressure 10 to 15 minutes to build.
1. In accordance with the manufacturer's recommendations, relieve pressure naturally after 10 to 40 minutes. Lock released; lid removed. The curry roux blocks should have melted on top and been mixed into the broth.

#### Cook's Notes:

Using a curry roux, which you're able to find at any grocery store, even those that aren't Asian stores, is the simplest method to get the creamy, spicy, and ideal flavor. If preferred, use tonkatsu sauce in place of ketchup.

### Japanese Beef Rolls

![](./@imgs/recipes/tejc_imgs/00025.jpeg)

Prep Time: 30 mins | Cook Time: 10 mins | Total Time: 40 mins | Servings: 8

#### Ingredients

- 1 tbsp vegetable oil
- 12 shiitake mushrooms, sliced
- 24 spears fresh asparagus, trimmed
- 8 thin-cut top round steaks
- ¼ cup soy sauce
- 1 bunch green onions, green parts only

#### Directions

1. Together, in a skillet over medium heat, warm the oil. The mushrooms should be added, covered, and sweated over low heat until tender. Keep them from browning. While you're waiting, heat up some water in a big pot or skillet. Asparagus can be blanched by lowering it into boiling water in a strainer, cooking it for just 30 seconds or until it turns brilliant green, and then transferring it to icy water to stop the cooking process. Set aside.
1. Heat up the oven's broiler while positioning the oven rack about 6 inches from the heat source. Oil a broiler pan.  1. Lay the steaks down flat and start assembling the buns. If your steaks are thick, pound them to a thickness of about 1/4 inch. Place a few mushrooms, a couple of green onions, and three stalks of asparagus at the end of each steak after brushing soy sauce over the surface. Put a toothpick through the center of each bundle, then roll it up toward the other end to seal it. On the broiling pan, arrange the rolls seam side down.
1. For about 3 minutes, roast under the preheated broiler until the top is browned. After 2 to 3 minutes, flip the rolls over and brown the other side. Avoid overcooking since this may cause the steaks to burn or the meat to become tough.

### Japanese Minced Beef

![](./@imgs/recipes/tejc_imgs/00026.jpeg)

Prep Time: 10 mins | Cook Time: 6 mins | Total Time: 16 mins | Servings: 4

#### Ingredients

- ¾ pound ground beef
- 2 tbsps freshly grated ginger
- 3 tbsps soy sauce
- 3 tbsps sake (Japanese rice wine)
- 2 tbsps mirin (Japanese sweet wine)
- 1 tbsp white sugar, or more to taste

#### Directions

1. Cook and stir the beef in a medium-hot skillet for 5 to 7 minutes, or until it is thoroughly cooked. Stir the ginger well. The beef combination has been mixed with soy sauce, sake, mirin, and sugar. Bring to a boil and cook for an additional minute.

### Gyudon Japanese Beef Bowl

![](./@imgs/recipes/tejc_imgs/00030.jpeg)

Prep Time: 15 mins | Cook Time: 7 mins | Total Time: 22 mins | Servings: 2

#### Ingredients

- 1 large white onion
- 1 tsp vegetable oil
- ½ cup water
- 2 tbsps soy sauce
- 1 tbsp brown sugar
- 1 tbsp mirin
- 1 tbsp sake
- ½ pound beef ribeye steak, thinly sliced
- 1 tbsp sesame seeds, or to taste (Optional)
- 2 green onions, thinly sliced, or to taste (Optional)
- 2 tsps pickled ginger (beni shoga), or to taste (Optional)
- 1 sheet dried seaweed, cut into strips, or to taste (Optional)

#### Directions

1. Cut the onion in half, then remove the core. Slice up the halves into little pieces.
1. On high heat, warm oil in a sizable skillet or wok. Add the onion and stir while cooking for 30 seconds or until it begins to brown.  Lower the heat to medium-low, add the water, soy sauce, brown sugar, mirin, and sake, and simmer for about 3 minutes, or until the flavors are well combined.
1. Add the beef to the skillet and stir. Cook the beef, covered, for 3 to 5 minutes or until it is well done. Garnish with sesame, green onions, ginger, and seaweed strips after dividing between serving bowls.

#### Cook's Note:

If you'd rather, you can use another type of rice wine in place of the sake.

### Japanese Wafu Burger

![](./@imgs/recipes/tejc_imgs/00031.jpeg)

Prep Time: 25 mins | Cook Time: 20 mins | Total Time: 45 mins | Servings: 6

#### Ingredients

- 1 (14 ounce) package firm tofu
- 1 pound ground beef
- ½ cup sliced shiitake mushrooms
- 2 tbsps miso paste
- 1 egg, lightly beaten
- 1 tsp salt
- 1 tsp ground black pepper
- ¼ tsp ground nutmeg
- ¼ cup mirin (Japanese sweet wine)
- 2 tbsps soy sauce
- 1 tsp garlic paste
- ¼ tsp minced fresh ginger root
- 1 tbsp vegetable oil

#### Directions

1. Put a plate on top of the tofu block, then another plate. Add a 3--5-pound weight at the top (a container filled with water works well). After pressing the tofu for 10 to 15 minutes, drain the liquid that has gathered and throw it away. Tofu should be cut into 1/2-inch chunks.
1. In a large bowl, mix the tofu, meat, shiitake mushrooms, miso paste, egg, salt, pepper, and nutmeg. Flatten into pies after dividing into six balls.
1. In a small bowl, combine the mirin, soy sauce, ginger, and garlic paste. Set aside.
1. In a sizable skillet over medium-high heat, warm the vegetable oil.  Burgers should be browned for two minutes on each side. Turn the heat down to low, cover the pan, and cook for about 5 minutes, or until the juices flow clearly. Excess grease should be drained and thrown away.
1. Fill the pan with the soy sauce mixture. Move the skillet frequently to prevent the sauce from burning, and turn the burgers over to coat both sides with the sauce. The sauce will thicken and leave the burgers with a glossy coating. The burgers are prepared once all of the sauce has been consumed.

### Beef Yakitori

![](./@imgs/recipes/tejc_imgs/00032.jpeg)

Prep Time: 15 mins | Cook Time: 15 mins | Additional Time: 4 hrs | Total Time: 4 hrs 30 mins | Servings: 4

#### Ingredients

- ½ cup soy sauce
- 2 tbsps vegetable oil
- 2 tbsps lemon juice
- 1 tbsp sesame seeds
- 2 tbsps white sugar
- 2 green onions, thinly sliced
- 1 clove garlic, minced
- ½ tsp ground ginger
- 1 pound sirloin steak, cubed

#### Directions

1. Mix the soy sauce, oil, lemon juice, sesame seeds, sugar, green onions, garlic, and ginger in a glass or plastic bowl.
1. The meat is threaded onto skewers. (Soak wooden skewers for 30 minutes before using.) Pour the marinade over the meat, rotating the skewers to thoroughly coat them, and place them in a glass or plastic dish just big enough to hold them. For at least 4 hours, cover and chill.
1. Place the grate 5 inches from the fire and preheat the grill to high heat.
1. After oiling the grate, set the skewers on the grill. Grill the kabobs for 10 to 15 minutes, flipping them once or twice to ensure equal cooking.

## The Best Japanese Dessert Recipes
### Green Tea (Matcha) Tiramisu

![](./@imgs/recipes/tejc_imgs/00033.jpeg)

Prep Time: 15 mins | Additional Time: 4 hrs 20 mins | Total Time: 4 hrs 35 mins | Servings: 6

#### Ingredients

- 1 ⅛ cups finely crushed graham cracker crumbs
- 3 ½ tbsps unsalted butter, melted
- 7 ounces cream cheese, at room temperature
- 6 tbsps white sugar
- 1 tsp white sugar
- 7 ounces mascarpone cheese
- 3 tbsps matcha green tea powder, plus more for dusting
- 1 cup heavy whipping cream
- 1 tbsp unflavored gelatin
- 3 tbsps warm water
- 1 tsp confectioners' sugar for dusting (Optional)
- 3 strawberries, sliced

#### Directions

1. Graham cracker crumbs and melted butter are combined in a bowl and stirred until moistened. The batter is then pressed evenly into a 7-inch round springform pan. Put the pan in the refrigerator and leave it there for 20 minutes to cool.
1. In a bowl, use an electric mixer to thoroughly blend the cream cheese, 6 tbsps plus 1 tsp of sugar, and mascarpone cheese. Mixture of cheese and three tsps of green tea powder.
1. Using an electric mixer, whip cream in a chilled glass or metal bowl until soft peaks form. Whip the cream cheese mixture, then fold it in.
1. In a small bowl, mix the gelatin until it is completely dissolved.  Pour the mixture over the chilled Graham cracker crust after stirring the gelatin mixture into the cream cheese mixture.  Refrigerate for at least 4 hours or until firm.
1. Sprinkle confectioners' sugar and green tea powder on the tiramisu's top before adding strawberry slices for decoration.

### Japanese Banana Rice Pudding
 
![](./@imgs/recipes/tejc_imgs/00034.jpeg)

Prep Time: 10 mins | Cook Time: 24 mins | Additional Time: 15 mins | Total Time: 49 mins | Servings: 4

#### Ingredients

- 2 bananas
- 1 tbsp white sugar
- 1 cup milk
- 1 cup cooked short-grain white rice
- 2 egg yolks
- 3 tbsps white sugar
- 2 tbsps butter, melted
- 1 pinch salt
- ½ cup whipped cream

#### Directions

1. Slice half a banana and top with one tbsp of sugar.
1. The remaining bananas should be chopped and added to a skillet over medium heat. Stir and mash the bananas for 5 to 10 minutes, or until they are heated through, have a light brown color, and can be rolled into a ball. Get rid of the heat.
1. In a saucepan over low heat, combine milk, rice, egg yolks, 3 tbsps sugar, butter, and salt. Cook the mixture for 2 to 3 minutes, or until it starts to thicken and steam. After thoroughly blending and heating up, add the mashed bananas. After being taken off the heat, the rice pudding should be chilled for at least 15 minutes.
1. Rice pudding and whipped cream should be well combined.
1. Slices of banana covered in sugar should be cooked and stirred in a hot skillet for 2 to 4 minutes, or until the sugar melts and turns caramelized. Pour rice pudding on top.

### Japanese Fruit Cake

![](./@imgs/recipes/tejc_imgs/00035.jpeg)

Prep Time: 1 hrs | Cook Time: 40 mins | Additional Time: 10 mins | Total Time: 1 hrs 50 mins | Servings: 48

#### Ingredients

- 2 cups raisins
- 2 cups flaked coconut
- 1 cup chopped pecans
- 3 cups all-purpose flour, divided
- 2 cups white sugar
- 1 cup butter
- 6 large egg yolks
- 4 tsps baking powder
- 2 tsps ground cinnamon
- 1 tsp nutmeg
- 1 tsp ground cloves
- 1 cup milk
- 6 large egg whites

Filling:

- 2 cups white sugar
- ¼ cup all-purpose flour
- 1 ½ cups water
- 2 oranges, peeled and seeded
- 2 lemons, peeled and seeded
- 2 cups flaked coconut

#### Directions

1. Set the oven to 350 degrees Fahrenheit (175 degrees Celsius). Cake pans should be greased and dusted with flour.
1. In a medium bowl, combine raisins, coconut, and pecans. Add 1 cup of flour and toss to combine. Set aside.
1. Using an electric mixer, cream the butter and sugar in a large bowl until frothy. Egg yolks should be added one at a time, beating vigorously after each addition. In another bowl, sift the remaining 2 cups of flour, baking powder, cinnamon, nutmeg, and cloves. Beat the egg mixture momentarily after each addition before adding the flour mixture in portions, alternating with the milk. The mixture of raisins should be folded in well.
1. In a different, clean bowl, whip the egg whites to firm peaks. Once there are no longer any streaks, fold the batter. Into the prepared cake pans, divide the batter.
1. For about 30 minutes, or until a toothpick inserted in the center comes out clean, bake in the preheated oven. Cakes should cool for ten minutes in their pans before being taken out and put on wire racks to cool more.
1. Produce the filling: In a saucepan, combine the sugar, flour, and water. Add the water, and stir over medium heat until the sugar dissolves. Oranges and lemons should be chopped into small pieces and added to the water mixture in the pan. Bring to a boil and simmer for 10 minutes or until thick. Add coconut and stir; then, set aside to chill.
1. Put the filling in the middle of the layers of cooled cake before assembling the cake.

### Manju (Japanese Sweet Bean Paste Cookies)

![](./@imgs/recipes/tejc_imgs/00036.jpeg)

Prep Time: 45 mins | Cook Time: 15 mins | Total Time: 1 hrs | Servings: 50

#### Ingredients

- 2 cups white sugar
- 1 cup butter, softened
- 4 eggs
- 1 tsp vanilla extract
- 5 cups all-purpose flour
- 2 tbsps baking powder
- 1 (18 ounce) can koshi an (sweetened red bean paste)
- ¼ cup evaporated milk, or as needed

#### Directions

1. Set the oven to 375 degrees Fahrenheit (190 degrees Celsius).  Butter two baking trays.
1. In a large bowl, use an electric mixer to thoroughly blend and cream the butter and sugar. One at a time, mix eggs into creamed butter until smooth. Then, beat in vanilla essence.
1. In a bowl, sift the flour and baking powder together. Once the dough is smooth, gradually stir the flour mixture into the butter mixture, mixing thoroughly after each addition.
1. Flour your hands liberally. On a floured board, roll the dough into walnut-sized balls and press into 4-inch circles, making the circles larger in the center and thinner on the edges. Each dough circle should have 1 1/2 tsps of koshi in the center of it. Then, tightly compress the dough around the filling to seal it. Bring the edges together. Pinched-side down, arrange the dough balls on the preheated baking pans about 2 inches apart. Dried milk is brushed onto dough balls.
1. Bake for about 15 minutes in the preheated oven, or until the tops are gently browned.

#### Cook's Note:

The manju can be filled more easily with a cookie press that runs on batteries and has a piping tip. Squeeze the koshi into the pastry circles by simply loading it into the press's barrel.

### Azuki Ice Cream (Japanese Red Beans Ice Cream)

![](./@imgs/recipes/tejc_imgs/00037.jpeg)

Prep Time: 9 hrs | Cook Time: 30 mins | Total Time: 9 hrs 30 mins | Servings: 8

#### Ingredients

- 1 cup dry adzuki beans
- ⅓ cup white sugar
- 2 tsps lemon juice
- 3 ½ cups water
- 1 cup milk
- 1 cup heavy cream
- 4 egg yolks
- ⅔ cup white sugar
- 1 tsp vanilla extract

#### Directions

1. The azuki beans, water, lemon juice, and 1/3 cup of sugar should all be combined in a pot. Bring to a boil and cook for 3 minutes without covering. The beans should be quite soft after 2 1/2 to 3 hours of simmering on low heat. When finished, the liquid and beans should equal 3 cups. If not, increase the water amount accordingly.
1. After putting the bean mixture through a sieve with a wooden paddle, throw away the bean skins. Place in the fridge for around two hours, or until cool.
1. The milk and cream should be combined in a pan. Over medium heat, bring to a boil. The egg yolks and 2/3 cup of sugar should be whisked together in a medium bowl as you wait for that to boil. Pour about 1/4 cup of the heated mixture into the bowl containing the egg yolks after the cream and milk have reached a rolling boil. Whisk the mixture until it is smooth. When the yolk mixture is sufficiently thick to coat the back of a metal spoon, pour it into the pan with the cream. It ought to take five minutes or so. Cooking for too long will result in lumps. Add the vanilla after turning the heat off. Cool in the refrigerator.
1. Stir the two mixtures together once they have both cooled. Pour into an ice cream maker and freeze as the manufacturer instructs. 

### 3-Ingredient Cheesecake

![](./@imgs/recipes/tejc_imgs/00038.jpeg)

Prep Time: 15 mins | Cook Time: 30 mins | Additional Time: 4 hrs 15 mins | Total Time: 5 hrs | Servings: 8

#### Ingredients

- 1 (4 ounce) bar white chocolate, chopped
- ½ cup cream cheese, softened
- 3 eggs, separated

#### Directions

1. Set the oven to 350 degrees Fahrenheit (175 degrees Celsius). Using parchment paper, line the bottom of a 9-inch springform pan.
1. Over simmering water, place the white chocolate on top of a double boiler. For about 5 minutes, while stirring continuously and scraping the sides with a rubber spatula to prevent burning, the chocolate will melt. Remove it and give it a little time to cool.  Add the egg yolks and cream cheese.
1. In a sizable glass, metal, or ceramic bowl, beat the egg whites with an electric mixer until stiff peaks form. Add the white chocolate mixture after it has cooled. Pour into the springform pan you just made.
1. Bake for 15 minutes in the preheated oven. Bake for an additional 15 minutes at 300 degrees F (150 degrees C) in the oven. As soon as the cheesecake has been in the oven for 15 minutes, turn it off.
1. Take the cheesecake out of the oven and let it cool to room temperature for about an hour. 3 hours or more should pass before serving.

### Kasutera (Castella), the Japanese Traditional Honey Cake

![](./@imgs/recipes/tejc_imgs/00039.jpeg)

Prep Time: 30 mins | Cook Time: 50 mins | Additional Time: 10 mins | Total Time: 1 hrs 30 mins | Servings: 6

#### Ingredients

- 1 tsp butter, softened, or as needed
- 1 ⅓ cups superfine sugar
- 5 tbsps honey, divided
- 3 ⅓ fluid ounces milk
- 8 eggs
- 1 ⅓ cups superfine sugar (¿más?)
- 1 ⅝ cups all-purpose flour, sifted twice
- 1 cup sweet bean paste

#### Directions

1. Set the oven to 375 degrees Fahrenheit (190 degrees Celsius). Use parchment paper to line a cake pan. Sprinkle one tsp of superfine sugar over the paper after buttering it.
1. Water in a huge bowl after it has been brought to a boil in a saucepan
1. Milk and 1/4 cup of honey are combined in a bowl and whisked until smooth.
1. Eggs are stirred with an electric mixer in a bowl over boiling water. Add 1 1/3 cups of superfine sugar gradually while beating the mixture until it is warm and smooth. After about 5 minutes, remove the egg mixture from the hot water basin and let it cool to room temperature. Place the egg mixture's bowl back in the hot water and keep pounding. Up till the batter is frothy and thick, repeat this process several times.
1. Using a spatula, combine the milk mixture with the egg mixture.  Once the batter is thoroughly combined, toss in the sifted flour.  Fill the prepared cake pan with the batter.
1. For about 50 minutes, or until a toothpick inserted in the center of the cake comes out clean, bake in the preheated oven. Over the cake's top, drizzle the final tbsp of honey. If preferred, serve slices with ice cream and sweet bean paste.

#### Editor's Note:

You can make your own sweet red bean paste using this recipe if you
can't find canned sweet red - paste.

### Microwave Mochi

![](./@imgs/recipes/tejc_imgs/00040.jpeg)

Prep Time: 5 mins | Cook Time: 10 mins | Total Time: 15 mins | Servings: 25

#### Ingredients

- 1 ½ cups mochiko (glutinous rice flour)
- 1 ½ cups water
- 1 ¼ cups white sugar, divided
- 2 drops distilled white vinegar
- ½ cup potato starch
- ¼ tsp salt

#### Directions

1. Mochiko, water, and 1 cup sugar should all be thoroughly combined in a medium bowl. Vinegar can be added to soften. Spoon into a dish that can be used in the microwave, then loosely wrap in plastic. 8 to 10 minutes on high in the microwave After it is removed, allow it to cool until it is safe to handle.
1. In a separate bowl, combine the potato starch, salt, and the final 1/4 cup of sugar. Use a plastic or wooden knife to turn the mochi out onto the plastic wrap and cut it into 25 pieces (metal knives tend to stick too much). In the potato starch mixture, roll the pieces.

### Spongy Japanese Cheesecake

![](./@imgs/recipes/tejc_imgs/00041.jpeg)

Prep Time: 20 mins | Cook Time: 1 hrs 15 mins | Additional Time: 1 hrs 35 mins | Total Time: 3 hrs 10 mins | Servings: 8

#### Ingredients

- 1 (8 ounce) package cream cheese, cubed
- ½ cup milk
- 3 tbsps unsalted butter
- 10 tbsps cake flour
- 2 tbsps cornstarch
- 6 egg yolks
- 6 egg whites
- 1 tbsp fresh lemon juice
- ¼ tsp cream of tartar
- ⅛ tsp salt
- 10 tbsps superfine sugar

#### Directions

1. Set the oven to 325 degrees Fahrenheit (165 degrees Celsius). An 8-inch round cake pan should be lightly greased and lined with parchment paper.
1. Put the cream cheese and milk in a bowl and let it sit for 20 minutes.  1. Cream cheese, milk, and butter are heated in a double boiler over simmering water. The mixture is heated for about 5 minutes, while being stirred regularly, until melted and smooth. After removing it from the heat, allow it to cool for at least 15 minutes.
1. Sieve cornstarch and cake flour into a bowl. Re-sift into the cream cheese mixture and thoroughly combine. Mix thoroughly after adding the egg yolks and lemon juice.
1. In another dish, combine the egg whites, cream of tartar, and salt.  With an electric mixer, whip the mixture until frothy. Add the sugar, 2 tbsps at a time, mixing well after each addition. Beat rapidly on and on until soft peaks appear.
1. After mixing the cream cheese mixture and the egg white mixture well, pour it into the pan that has been set up. Put the cake pan into a bigger baking dish, and then pour water into the larger baking dish until it is halfway up the sides of the cake pan.
1. Bake in the preheated oven for 1 hour and 10 minutes, or until the cheesecake is set and the top is golden brown. Bake the cake for one hour after the oven is turned off and the door is slightly ajar.  Move to a wire rack to thoroughly cool.

### Green Tea Layer Cake

![](./@imgs/recipes/tejc_imgs/00042.jpeg)

Prep Time: 30 mins | Cook Time: 40 mins | Additional Time: 30 mins | Total Time: 1 hrs 40 mins | Servings: 12

#### Ingredients

- 1 cup all-purpose flour
- 1 cup cake flour
- 1 tsp baking soda
- 1 tsp salt
- 4 tsps powdered green tea
- 1 ¼ cups white sugar
- 1 cup vegetable oil
- 3 eggs
- 1 cup plain yogurt
- 1 ½ tsps vanilla extract
- 1 ¼ cups confectioners' sugar
- 2 tsps powdered green tea
- 2 tbsps butter, softened
- 1 (3 ounce) package cream cheese, softened
- ½ tsp vanilla extract
- 1 ½ tsps milk

#### Directions

1. Set the oven to 350 degrees Fahrenheit (175 degrees Celsius).  Butter and flour two 8-inch round baking pans. All-purpose flour, cake flour, baking soda, salt, and green tea powder should be sifted together and set aside.
1. Beat sugar, oil, and eggs in a sizable bowl until combined and smooth. Add 1 1/2 tsps vanilla and stir. Yogurt and the flour mixture should be beaten in alternately, just until combined. Fill the pans with the batter.
1. A toothpick inserted into the center of the cake should come out clean after 30 to 40 minutes of baking in the preheated oven. Before turning them out of the pans, let them cool on a wire rack for 30 minutes.
1. Green tea frosting is created by: Green tea powder and confectioners' sugar are combined by sifting. Combine the tea mixture with the butter, cream cheese, vanilla, and milk in a medium bowl. Till smooth, beat with an electric mixer.
1. The cakes are assembled by placing one layer on a flat serving dish after they have completely cooled. Frost it with a little coating of icing. Spread icing over the top and sides of the cake before adding the second layer. Sprinkle, if desired, with green tea powder. at room temperature or chilly

#### Cook's Note:

Optional: Before the batter is added to the pans, 1 cup of adzuki beans (little red beans) can be boiled until tender and incorporated into the batter. Before using, pat the surface dry and allow it to cool.

### Marshmallow Cake

![](./@imgs/recipes/tejc_imgs/00043.jpeg)

Prep Time: 15 mins | Cook Time: 5 mins | Additional Time: 3 hrs | Total Time: 3 hrs 20 mins | Servings: 8

#### Ingredients

- ½ (10.5 ounce) package marshmallows
- ½ cup milk
- 1 (8 ounce) package cream cheese, softened
- 1 large egg yolk
- ½ lemon, juiced
- 1 cup heavy cream
- 1 (9 inch) prepared graham cracker crust

#### Directions

1. On low heat, combine milk and marshmallows in a saucepan. Boil, occasionally stirring, until the mixture begins to boil and the marshmallows are melted. Get rid of the heat.
1. Cream cheese should be smoothed out in a big dish. Add the egg yolk and stir. Stir in a third of the melted marshmallow mixture, then quickly incorporate the rest of it until there are no streaks left.  Add lemon juice and stir.
1. In a medium bowl, beat the cream until soft peaks form. Until there are no more streaks, fold in the cream cheese and marshmallow mixture. Before serving, spread into the graham crust and chill for three hours in the fridge.

### Green Tea Cheesecake

![](./@imgs/recipes/tejc_imgs/00044.jpeg)

Prep Time: 15 mins | Cook Time: 25 mins | Additional Time: 1 hrs | Total Time: 1 hrs 40 mins | Servings: 12

#### Ingredients

- 2 (8 ounce) containers fat-free cream cheese, softened
- 2 eggs, beaten
- ¾ cup white sugar
- 1 tbsp green tea powder
- 2 tsps vanilla extract
- 1 (9 inch) prepared graham cracker pie crust

#### Directions

1. Set the oven to 350 degrees Fahrenheit (175 degrees Celsius).
1. Cream cheese and sugar should be combined in a sizable bowl and beaten until smooth. Pour into the prepared crust after softly and smoothly blending in the green tea granules, eggs, and vanilla essence.
1. Bake for 25 minutes in a preheated oven, or until the middle jiggles evenly when the cake is gently shook. Before serving, chill for one hour.

### Coffee Jelly

![](./@imgs/recipes/tejc_imgs/00045.jpeg)

Prep Time: 5 mins | Cook Time: 5 mins | Additional Time: 6 hrs | Total Time: 6 hrs 10 mins | Servings: 4

#### Ingredients

- 2 tbsps hot water
- 1 (.25 ounce) package unflavored gelatin
- 2 cups fresh brewed coffee
- 3 tbsps white sugar

#### Directions

1. Gelatin and boiling water are combined in a small bowl and stirred until the gelatin is completely dissolved. Coffee and sugar are stirred in before being heated to a boil.
1. Fill a shallow 9-inch square or 7-by-11-inch baking dish with the coffee mixture. Refrigerate for 6 to 7 hours or until the mixture has set.
1. To serve, cut the coffee jelly into cubes.
1. a glass of coffee, jelly, and cream

#### Tips

Pour the heated coffee mixture into 4 individual cups as opposed to one big baking dish for individual servings, and refrigerate as instructed in Step 2 afterward.

### Green Tea Mousse Cheesecake

![](./@imgs/recipes/tejc_imgs/00046.jpeg)

Prep Time: 20 mins | Cook Time: 1 mins | Additional Time: 7 hrs | Total Time: 7 hrs 21 mins | Servings: 12

#### Ingredients

- 1 (4.8 ounce) package graham crackers, crushed
- 2 tbsps white sugar
- 3 tbsps unsalted butter, melted
- 2 tbsps green tea powder (matcha)
- ½ cup warm water
- 2 tbsps unflavored gelatin
- ½ cup cold water
- 2 cups whipping cream
- 2 (8 ounce) packages cream cheese, at room temperature
- ½ cup white sugar
- 1 tsp vanilla extract
- ¼ cup honey
- 2 eggs

#### Directions

1. In a mixing bowl, combine the Graham cracker crumbs and 2 tbsps of sugar. Add the melted butter and stir until the mixture is thoroughly moistened. Set aside. Push into the bottom of a 9-inch springform pan that has been lined with wax paper.
1. Warm water and tea powder are combined and set aside. Set aside after adding the gelatin to the cold water.
1. Cream should be whipped until firm peaks form. In a fresh mixing bowl, combine the cream cheese, 1/2 cup sugar, vanilla, and honey.  One at a time, beat in the eggs until well combined. Microwave the gelatin mixture for about 45 seconds, or until melted. The cream cheese mixture is blended with gelatin and tea before the whipped cream is added and folded in smoothly. Fill the springform pan with liquid. Before removing the mold and serving, refrigerate for seven to eight hours.

## Top-Rated Japanese Tofu Recipes
### Crispy Teriyaki Tofu
 
![](./@imgs/recipes/tejc_imgs/00047.jpeg)

Prep Time: 10 mins | Cook Time: 15 mins | Total Time: 25 mins | Servings: 4

#### Ingredients

- 3 tbsps vegetarian teriyaki sauce
- 2 green onions, chopped
- 1 tbsp sherry
- 1 tsp minced garlic
- 1 tsp Sriracha sauce (Optional)
- ½ tsp ginger paste
- 5 tbsps cornstarch
- 1 (14 ounce) package extra-firm tofu, pressed and cut into slices
- 2 tbsps olive oil

#### Directions

1. Green onion, sherry, garlic, Sriracha sauce, and ginger paste should all be combined until smooth. Set aside.
1. Put cornstarch in a zip-top bag measuring 1 gallon. Slices of tofu should be placed inside the bag, sealed, and gently shaken until coated.
1. Over medium-high heat, warm up the olive oil in a big skillet.  5-minute tofu cooking time. Using tongs, flip the food and cook for a further 5 minutes or until the other side is browned. Wipe the skillet clean after transferring the tofu to a plate.
1. Cook the sauce you saved in the pan for two minutes, or until it starts to get a little bit thicker. Get rid of the heat. Toss the tofu back into the skillet to mix.
1. Just before serving, place the tofu on serving dishes and cover with sauce.

### Tofu with Pork and Miso
 
![](./@imgs/recipes/tejc_imgs/00048.jpeg)

Prep Time: 10 mins | Cook Time: 10 mins | Total Time: 20 mins | Servings: 3

#### Ingredients

- ⅓ cup miso paste
- ½ cup water
- 1 tbsp sesame oil
- ¼ cup chopped green onion, white and green parts
  separated
- ⅓ cup ground pork
- 1 (14 ounce) package tofu, cubed

#### Directions

1. Miso and water are blended until completely dissolved; discard. In a frying pan or wok, warm the sesame oil over medium-high heat.  Sauté the white portions of the green onion for 30 to 1 minute, or until they are aromatic and lightly browned. The ground meat should be added and cooked for a few minutes, until almost done.
1. Add the miso paste and tofu slowly while stirring. Warm the tofu by bringing it to a simmer and cooking it for a little while. Before serving, top with the green parts of the green onions.

### The Best Ever Vegan Sushi

![](./@imgs/recipes/tejc_imgs/00049.jpeg)

Prep Time: 30 mins | Cook Time: 38 mins | Additional Time: 15 mins | Total Time: 1 hrs 23 mins | Servings: 6

#### Ingredients

- 1 cup short-grain sushi rice
- 2 cups water
- 1 pinch salt
- 1 ½ tsps vegetable oil
- ¼ cup rice vinegar
- 2 tbsps white sugar
- ⅛ tsp salt
- 1 (16 ounce) package extra-firm tofu
- 1 tbsp olive oil, or more as needed
- ¼ small onion, minced (Optional)
- 1 tsp garlic, minced (Optional)
- ¼ cup vegan mayonnaise (such as Follow Your Heart®
  Vegenaise®)
- 2 tbsps sriracha sauce, or to taste
- 2 sheets nori, or as needed
- ½ avocado - peeled, pitted, and sliced
- ½ cup matchstick-sliced Savoy cabbage
- ¼ cup matchstick-cut carrots
- ¼ cup matchstick-cut seeded cucumber

#### Directions

1. Rice, water, and a little salt are combined in a saucepan and brought to a boil. Use a small wooden spoon or a bamboo rice spatula to stir once. Lower the heat and cover. Cook for 20 minutes or until all the water has been absorbed and the rice is soft. Cool down.
1. Rice vinegar, sugar, and 1/8 tsp salt are added to a small pot of hot vegetable oil. The mixture should be heated until all the sugar has dissolved and the liquid is simmering. Remove it from the heat and allow it to cool for at least 10 minutes before handling. You might not need all of the cooled liquid; instead, add little amounts carefully until the mixture is moist and sticky without becoming goopy.
1. With a paper towel, press the tofu to remove any extra liquid.  Create tofu strips.
1. In a small skillet over medium heat, warm the olive oil. Add the tofu strips, onion, and garlic; cook and stir for about 4 minutes per side, or until the tofu is golden brown.
1. Sriracha and vegan mayonnaise should be combined in a small bowl.
1. Put a nori sheet on a sushi mat with the rough side facing up.  Apply a thick, even layer of prepared rice to the nori, covering it all with damp fingers. Along the bottom border of the sheet, arrange the tofu strips, avocado, cabbage, carrots, and cucumber in a straight line.
1. Over the filling, roll the nori and the sushi mat. Take the mat off, then wrap the roll in plastic wrap, tightly twisting the ends to compress the roll. For five to ten minutes, refrigerate until set. Repeat with the remaining filling and nori.
1. Take the sushi roll out of the plastic wrap, cut it into pieces, and spread Sriracha mayo on top.

#### Cook's Notes:

If preferred, jasmine rice can be used in place of the sushi rice.  Like shrimp tempura, fried tofu has a nice crunch. Pan-seared tofu, on the other hand, is a bit creamier. The driest tofu you can get, like the pre-marinated variety packaged in a vacuum-sealed package, is what you want for fried tofu (instead of tofu in liquid). The tofu should be well dried with a paper towel before being cut into 1/4-inch strips, cornstarched, and cooked in a skillet with a few tsps of vegetable oil. Fry for 1 to 2 minutes on each side, or until golden brown.

### Pumpkin and Tofu Miso Soup

![](./@imgs/recipes/tejc_imgs/00050.jpeg)

Prep Time: 15 mins | Cook Time: 15 mins | Total Time: 30 mins | Servings: 2

#### Ingredients

- ¼ small pumpkin - peeled, seeded, and cubed
- 1 (2 inch) piece fresh ginger, cut into matchsticks
- 2 tbsps soy sauce
- 2 ounces buckwheat noodles
- 3 ½ ounces firm tofu, cubed
- 2 tsps miso paste
- 2 tsps sesame oil
- 2 green onions, finely chopped on the diagonal
- 2 large red chile peppers, sliced on the diagonal
- 2 tbsps toasted sesame seeds
- 2 tbsps chopped fresh cilantro, or more to taste
- 2 tbsps pickled ginger

#### Directions

1. Water in a big saucepan is brought to a boil, then pumpkin, ginger, and soy sauce are added. For around three minutes, cook the pumpkin mixture. The noodles should be cooked for around 4 minutes after being added to the pumpkin sauce.
1. Add tofu to the pumpkin-noodle mixture and simmer for an additional 5 to 10 minutes, or until the pumpkin and noodles are almost soft.
1. Put a tsp of miso paste in each bowl for serving. Cooking water should be added to each bowl and whisked until the miso is dissolved.
1. As a garnish, add 1 tsp sesame oil, 1 chopped green onion, 1 diced red chile pepper, 1 tbsp sesame seed, 1 tbsp cilantro, and 1 tbsp pickled ginger to each serving dish of the pumpkin-tofu soup.

### Tofu Chanpuru

![](./@imgs/recipes/tejc_imgs/00051.jpeg)

Prep Time: 20 mins | Cook Time: 30 mins | Additional Time: 5 mins | Total Time: 55 mins | Servings: 6

#### Ingredients

- 1 (12 ounce) package extra-firm tofu, drained
- 1 (12 ounce) can fully cooked luncheon meat (such as SPAM®), cubed
- 4 cloves garlic, chopped
- 1 tbsp minced fresh ginger, or to taste
- ½ cup sake
- ½ cup miso paste
- ¼ cup soy sauce
- 2 tbsps white sugar
- 2 tsps vegetable oil, or as needed
- 4 eggs, slightly beaten
- 2 tsps vegetable oil, or as needed
- 1 large onion, chopped
- 1 head cabbage, cored and chopped
- 2 carrots, grated
- 8 mushrooms, sliced
- 1 tbsp chopped green onion, or more to taste

#### Directions

1. Tofu should be wrapped in a paper towel and microwaved for one minute. Wrap the tofu with a dry paper towel after pressing out any remaining water. Let it sit for five minutes to let the excess water drain. Cut the tofu into cubes and remove the paper towel.
1. In a bowl, mix the ginger, garlic, and luncheon meat. In another bowl, combine the sake, miso paste, soy sauce, and sugar.
1. Scramble the eggs for about 5 minutes, stirring often, in a pan with 2 tsps of vegetable oil on medium heat. Place the eggs on a platter. Tofu should be cooked and stirred in the same skillet for 5 to 10 minutes, until browned on both sides. Add tofu and eggs to a plate. In the same skillet, heat and stir the luncheon meat combination for about 5 minutes, or until the meat is fully cooked and the garlic is gently browned.
1. In another skillet, heat 2 tbsps of vegetable oil over medium heat.  Cook and toss the onion, cabbage, carrots, and mushrooms for 10 to 12 minutes, or until the onions are translucent and the cabbage has softened. Stir in the eggs, tofu, luncheon meat, and sake mixture after adding the vegetable mixture. Sauté for approximately a minute, or until well heated. Green onions are the garnish.

### Traditional Beef Sukiyaki

![](./@imgs/recipes/tejc_imgs/00001.jpeg)

Prep Time: 25 mins | Cook Time: 11 mins | Total Time: 36 mins | Servings: 4

#### Ingredients

Broth:
- 1 ½ cups water
- ⅔ cup soy sauce
- ⅔ cup white sugar
- ⅓ cup sake

Sukiyaki Ingredients:
- 1 pound thinly sliced beef
- 1 (12 ounce) package firm tofu, drained and cut into bite-size
  pieces
- ½ head Chinese cabbage, cut into bite-size pieces
- 1 (7 ounce) package yam noodles (shirataki), drained
- 7 shiitake mushrooms, sliced
- 1 enoki mustrooms, roots removed
- 1 green onion (negi), sliced
- 1 tbsp vegetable oil
- 4 eggs

#### Directions

1. To prepare broth, mix water, soy sauce, sugar, and sake in a bowl.
1. Put separate plates of beef, tofu, Chinese cabbage, yam noodles, shiitake, enoki, and green onion on the table.
1. Oil should be heated in a big skillet at the table or in an electric skillet. Add the meat pieces, and cook and stir for approximately a minute, or until browned. Bring to a boil after adding some broth. Add tofu, cabbage, noodles, shiitake, enoki, and green onion; simmer for about 5 minutes or until softened.
1. Pour the cooked sukiyaki mixture into dishes for serving. Refill the skillet's broth.
1. Each egg should be cracked into a small basin and lightly beat.  Eggs should be served with sukiyaki for dipping.

#### Cook's Note:

If using dried shiitake mushrooms, rehydrate according to the instructions on the package.

#### Editor's Note:

Eggs are uncooked in this recipe. Pregnant women, small children, the elderly, and people with physical limitations shouldn't swallow raw eggs. Check out our article on how to make your eggs safe to learn more about egg safety.

### Homemade Miso Soup

![](./@imgs/recipes/tejc_imgs/00052.jpeg)

Prep Time: 10 mins | Cook Time: 10 mins | Additional Time: 10 mins | Total Time: 30 mins | Servings: 4

#### Ingredients

- 1 tbsp finely chopped wakame
- 4 cups water
- 2 tsps dashi granules
- 3 tbsps miso paste
- 4 ounces silken tofu, cubed
- 2 green onions, sliced on the bias

#### Directions

1. Put the wakame in a fine-mesh sieve and soak it for 10 minutes in cold water.
1. Dashi granules and 4 cups of water are combined in a pot and heated to a rolling boil over medium-low heat. Miso paste can be added; whisk to incorporate. Add the wakame, then boil for three minutes.
1. Place the tofu in four serving bowls. Add miso soup and green onions for decoration.

### Japanese Agedashi Tofu

![](./@imgs/recipes/tejc_imgs/00053.jpeg)

Prep Time: 10 mins | Cook Time: 5 mins | Additional Time: 15 mins | Total Time: 30 mins | Servings: 4

#### Ingredients

- 1 (10.5 ounce) package firm silken tofu
- 2 cups water
- 2 tbsps light soy sauce
- 2 tbsps mirin
- 1 tsp dashi granules
- ¼ cup all-purpose flour, or as needed
- vegetable oil for frying
- 2 green onions, chopped

#### Directions

1. Put a paper towel between the tofu block and the towel. Place a plate there and weigh it down with a 3- to 5-pound object. After 15 minutes of pressing, drain the liquid and toss it away.
1. In the meantime, put water, soy sauce, mirin, and dashi in a pot.  Over medium heat, bring to a boil. The heat has been removed and set aside.
1. Remove the plate, paper towels, and the weight. Make 3/4-inch chunks of tofu. Tofu is given a thin dusting of flour in a bowl.
1. Tofu should be fried in hot oil for 4 to 6 minutes, or until golden brown. If necessary, fry in batches to prevent crowding the pot. Add sauce to a serving dish with the fried tofu. Add some green onions on top.
1. A view of Japanese agedashi up close Green onions on a chopping board; tofu with green onions in a bowl; and broth in a bowl.

### Tofu Hiyayakko

![](./@imgs/recipes/tejc_imgs/00054.jpeg)

Prep Time: 10 mins | Total Time: 10 mins | Servings: 1

#### Ingredients

- 1 tbsp soy sauce
- 1 tsp white sugar
- ½ tsp dashi granules
- ½ tsp water
- ¼ (12 ounce) package silken tofu
- 1 ½ tsps grated fresh ginger root
- ¼ tsp thinly sliced green onion
- 1 pinch bonito shavings (dry fish flakes)
- 1 pinch toasted sesame seeds

#### Directions

1. In a small bowl, stir the miso, sugar, dashi granules, and water until the sugar dissolves. On a small plate, arrange the tofu and garnish with the ginger, green onion, and mackerel shavings. Add sesame seeds and drizzle the soy mixture on top.

#### Cook's Note

This soy-dashi dressing (Tsuyu) can be kept in the refrigerator for approximately a week if you prepare a lot of it. It can be used for anything, including salad, steamed salmon, boiled chicken, and boiled spinach.

### Japanese Tofu Salad

![](./@imgs/recipes/tejc_imgs/00055.jpeg)

Prep Time: 35 mins | Cook Time: 5 mins | Additional Time: 1 hrs | Total Time: 1 hrs 40 mins | Servings: 4

#### Ingredients

- 1 (14 ounce) package firm tofu, drained
- 3 tbsps soy sauce
- 1 tbsp mirin (sweetened rice wine)
- 1 tbsp rice vinegar
- 2 tsps sesame oil, or to taste
- 2 tbsps vegetable oil
- 2 cloves garlic, minced
- 1 tsp minced fresh ginger
- 1 large tomato, seeded and chopped
- 1 small red onion, thinly sliced
- ¼ cup chopped cilantro
- 1 tbsp sesame seeds

#### Directions

1. Between two plates, divide the tofu. Pour away the released liquid every 20 minutes while letting the tofu drain for roughly an hour while supporting it with a thick book.
1. In a small bowl, combine the soy sauce, mirin, rice vinegar, and sesame oil.
1. In a small pan, heat the vegetable oil over medium heat. Add the garlic and ginger, gently whisk, and simmer for 1 to 2 minutes, or until the garlic is just beginning to turn golden. Add to the soy sauce amalgamation.
1. Bite-sized pieces of pressed tofu should be placed in a bowl with tomato, onion, and cilantro. Dressing is added, then tossed onto the coat. Add sesame seeds as a garnish.

### Japanese Nabeyaki Udon Soup

![](./@imgs/recipes/tejc_imgs/00056.jpeg)

Prep Time: 15 mins | Cook Time: 25 mins | Total Time: 40 mins | Servings: 4

#### Ingredients

- 6 cups prepared dashi stock
- ¼ pound chicken, cut into chunks
- 2 carrots, diced
- ⅓ cup soy sauce
- 3 tbsps mirin
- ½ tsp white sugar
- ⅓ tsp salt
- 2 (12 ounce) packages firm tofu, cubed
- ⅓ pound shiitake mushrooms, sliced
- 5 ribs and leaves of bok choy, chopped
- 1 (9 ounce) package fresh udon noodles
- 4 eggs
- 2 leeks, diced

#### Directions

1. In a pot over medium heat, warm the dashi stock, chicken, carrots, soy sauce, mirin, sugar, and salt. Cook for 5 to 7 minutes, or until the core of the chicken is no longer pink. When the veggies are soft, add the tofu, mushrooms, and bok choy and stir for an additional 5 minutes.
1. Udon noodles should be added to the broth after it has simmered for three to four minutes. Add leeks and cracked eggs to the soup; simmer for an additional 5 minutes or until the eggs are just starting to firm up.

## Our Top 10 Japanese Izakaya Recipes
### Authentic Yakisoba

![](./@imgs/recipes/tejc_imgs/00057.jpeg)

Prep Time: 30 mins | Cook Time: 25 mins | Total Time: 55 mins | Servings: 8

#### Ingredients

- 4 (8 ounce) packages buckwheat soba noodles
- 2 tbsps vegetable oil
- 1 pound pork tenderloin, cut against the grain in thin strips
- 2 cups carrots, cut into julienne strips
- 1 medium onion, cut into julienne strips
- 1 tbsp freshly grated ginger
- 3 cloves garlic, minced
- ½ head napa cabbage, shredded
- ⅔ cup yakisoba sauce
- salt and ground black pepper to taste
- 4 tbsps pickled red ginger (beni shoga), or to taste
- 4 tbsps kizami nori (dried flaked aonori seaweed), or to taste

#### Directions

1. Bring water in a big pot to a boil. Soba should be cooked in boiling water for 5 to 8 minutes, stirring occasionally, until the noodles are soft but still firm to the bite. Rinse the noodles with cold water after draining. Set aside.
1. Oil in a wok is heated to medium heat. Add the pork and stir-fry with a pinch of salt and pepper for about 5 minutes. While saving the oil in the wok, transfer the meat to a platter. Stir-fry the carrots, onion, ginger, and garlic for 3 to 4 minutes in the wok.
1. Add the cabbage to the wok and stir-fry for a minute or so. Put the drained soba noodles in. Pour in half the yakisoba sauce and stir-fry for about 3 minutes, or until the noodles and vegetables are coated in sauce. Pork is returned to the wok. As needed, increase the sauce amount. Get rid of the heat.
1. Add kizami nori and a small mound of beni shoga to the yakisoba right before you serve it. 

#### Cook's Note:

The chicken breast can be used in place of the pork, but it must be extremely thinly cut or else the texture will be off.

### Pork Gyoza

![](./@imgs/recipes/tejc_imgs/00059.jpeg)

Prep Time: 1 hrs | Cook Time: 8 mins | Total Time: 1 hrs 8 mins | Servings: 6

#### Ingredients

- 12 ounces ground pork
- ¼ head cabbage, shredded
- 1 egg
- 2 spring onions, sliced
- 1 tbsp soy sauce
- 2 tsps sake
- 2 tsps mirin
- 2 tsps minced fresh ginger root
- 40 gyoza wrappers, or as needed
- 2 tbsps vegetable oil
- ½ cup water
- Sauce:
- ¼ cup rice wine vinegar
- ¼ cup soy sauce

#### Directions

1. In a sizable bowl, mix together the ground pork, cabbage, egg, red onion, 1 tbsp soy sauce, sake, mirin, and ginger.
1. Fill the middle of each gyoza wrapper with 1 to 2 tsps of the pork mixture. The edges of each wrapper should be rubbed with wet fingers. Over the filling, fold the wrappers in half to form a semicircle. To seal the two sides of the wrapper together, take one side and crimp the edges in a decorative pattern (similar to the pleats on a skirt). Make sure the dumpling doesn't have a lot of extra air inside it. Continue until all of the pork mixture has been used.
1. Using a large skillet over medium-high heat, warm the vegetable oil. Gyoza should be arranged in a single layer in the skillet and fried for 3 to 5 minutes, or until the bottom is browned. Reduce heat after adding water to the skillet. Gyoza should steam with the lid on for about five minutes, or until all the water has evaporated. Continue by using the remaining gyoza.
1. Serve the gyoza with a dipping sauce that you made by combining rice vinegar and soy sauce. 

### Easy Chicken Yakitori

![](./@imgs/recipes/tejc_imgs/00060.jpeg)

Prep Time: 10 mins | Cook Time: 15 mins | Additional Time: 15 mins | Total Time: 40 mins | Servings: 4

#### Ingredients

- 10 wooden skewers
- 4 skinless, boneless chicken thighs, cut into 1-inch cubes
- 4 scallions, sliced into 1-inch pieces
- ½ cup sake
- ½ cup soy sauce
- 3 tbsps mirin
- 2 tbsps white sugar
- 1 tsp vegetable oil, or to taste

#### Directions

1. 15 minutes of cold water soaking for 10 wooden skewers
1. Scallions and bits of chicken are alternately threaded onto the moistened skewers.
1. Bring sake, soy sauce, mirin, and sugar to a boil in a small saucepan. Lower the heat, then simmer for five minutes. Set half the sauce aside for dipping.
1. A grill pan should be heated to a high temperature and lightly oiled. Add the skewers and cook for 7 to 10 minutes per side, basting periodically with half the sauce, until the chicken is no longer pink in the center.

#### Cook's Note:

For optimal flavor, remember to baste regularly!

### Chicken Karaage (Japanese Fried Chicken)

![](./@imgs/recipes/tejc_imgs/00061.jpeg)

Prep Time: 15 mins | Cook Time: 10 mins | Additional Time: 30 mins | Total Time: 55 mins | Servings: 6

#### Ingredients

- 2 tbsps soy sauce
- 1 tbsp sake (Japanese rice wine)
- 2 tsps grated fresh ginger
- 1 ½ pounds boneless, skinless chicken breasts, cut into bite-size pieces
- 2 cups vegetable oil for frying
- ¾ cup cornstarch

#### Directions

1. In a large bowl, mix the soy sauce, sake, and ginger. Next, turn the chicken into a coat. 30 minutes of marinating in the refrigerator under a plastic wrap cover.
1. a deep fryer or large pot with 350 degrees Fahrenheit (175 degrees C) of oil.
1. Put cornstarch in a sizable plastic bag that can be sealed. Add the chicken, close the bag, and shake until the chicken is well cornstarched.
1. Cook the chicken in batches for 2 to 3 minutes, or until golden brown and the juices flow clear. On a wire rack or with paper towels, drain.

#### Tips

For this dish, make sure to use standard rice wine (sake). It shouldn't be confused with rice vinegar or sweet rice wine (mirin).

### Okonomiyaki (Japanese Pancake)

![](./@imgs/recipes/tejc_imgs/00062.jpeg)

Prep Time: 25 mins | Cook Time: 15 mins | Total Time: 40 mins | Servings: 4

#### Ingredients

Pancake:

- 1 cup all-purpose flour
- ⅔ cup water
- 4 cups chopped cabbage
- 6 strips cooked bacon, crumbled
- 2 eggs
- 1 sausage, diced, or more to taste (Optional)
- ½ cup chopped green onions
- ¼ cup cooked shrimp (Optional)
- ¼ cup shredded cheese (Optional)
- ¼ cup tenkasu (tempura pearls)
- 1 tbsp vegetable oil, or to taste

Sauce:

- 2 tbsps soy sauce
- 1 tbsp ketchup
- 1 tsp white vinegar

 Garnish:

- 1 tbsp panko bread crumbs, or to taste
- 1 tsp mayonnaise, or to taste

#### Directions

1. To make a smooth paste, combine flour and water in a bowl. Add the cheese, tenkasu, green onions, shrimp, bacon, eggs, and sausage.
1. Oil a griddle and raise the temperature to 400 degrees F (200 degrees C).
1. Spoon a quarter of the batter onto the hot griddle. Sauté for 6 minutes on each side or until golden brown. Onto a serving platter after transfer. To make a total of 4 pancakes, repeat.
1. Create sauce: In a small bowl, combine the vinegar, ketchup, and soy sauce. Pour over the pancakes.
1. Mayonnaise and panko are used as garnishes.

#### Tips

Add cayenne pepper to the okonomiyaki sauce for extra heat. If preferred, swap out the panko bread crumbs for crumbled saltine
crackers.

### Simple Roasted Edamame

Prep Time: 5 mins | Cook Time: 20 mins | Total Time: 25 mins | Servings: 6


#### Ingredients

- 1 (12 ounce) package frozen edamame (soybeans) in their
  pods
- 2 tbsps extra-virgin olive oil
- 2 cloves garlic, minced
- 1 tsp sea salt
- ½ tsp ground black pepper

#### Directions

1. Set the oven to 375 degrees Fahrenheit (190 degrees Celsius).
1. Edamame, olive oil, garlic, sea salt, and black pepper should all be thoroughly combined in a big bowl. On a baking sheet, spread out in a single layer.
1. In the preheated oven, roast the edamame shells for about 20 minutes, tossing halfway through. Serve beans that can be eaten whole and shelled.

### Tamagoyaki (Japanese Rolled Omelette)

![](./@imgs/recipes/tejc_imgs/00063.jpeg)

Prep Time: 10 mins | Cook Time: 10 mins | Additional Time: 5 mins | Total Time: 25 mins | Servings: 2

#### Ingredients

- 4 eggs
- 4 tbsps prepared dashi stock
- 1 tbsp white sugar
- 1 tsp mirin (Japanese sweet wine)
- ½ tsp soy sauce
- ½ tbsp vegetable oil

#### Directions

1. In a bowl, combine the eggs, dashi stock, sugar, mirin, and soy sauce.
1. In a sizable nonstick skillet set over medium-high heat, heat 1/3 of the oil. A third of the egg mixture should be added, and you should swiftly swirl the pan to coat the bottom evenly. As soon as the omelette is done, begin rolling it up from one side to the other.
1. Place the roll to the side and then add another third of the egg and oil to the skillet. Stir the pan to make sure the initial roll's surrounding area and underside are completely covered. Cook to the desired consistency. Roll toward the center from the side containing the first roll.
1. The remaining oil and egg should be used in the same manner. On a bamboo rolling mat, transfer the rolled omelette. Let it cool for a few minutes after securely rolling it up.
1. Slice the unwrapped omelette into six pieces. cold or warm serving.

#### Cook's Note:

Serious Japanese home cooks can buy a tamagoyaki frying pan, which is a rectangular nonstick frying pan used only for making this omelette. The pan can be purchased at Japanese markets or online.

## Vegan Japanese Recipes to Make at Home
### Vegan Japanese Winter Squash and Leek Soup

![](./@imgs/recipes/tejc_imgs/00064.jpeg)

Prep Time: 20 mins | Cook Time: 1 hrs 55 mins | Total Time: 2 hrs 15 mins | Servings: 8

#### Ingredients

- 1 (2 pound) Japanese winter squash (kabocha), halved and seeded
- 3 tbsps canola oil, or more as needed
- 1 large sweet onion, chopped
- 3 large leeks - white part cut horizontally and green parts cut lengthwise
- 2 cups water, or more as needed
- 3 small russet potatoes, chopped
- 3 large carrots, cut into rounds
- 4 cups vegetable stock, or more if needed
- 1 pinch garlic salt, or more to taste
- ground black pepper to taste

#### Directions

1. Set the oven to 350 degrees Fahrenheit (175 degrees Celsius). Place the squash on a baking sheet with the cut side up.
1. Bake in the preheated oven for 30 to 40 minutes or until the squash is soft; let cool slightly. Cut the squash's flesh after removing the skin.
1. Onion and the white sections of the leeks should be cooked and stirred in canola oil for 10 to 15 minutes, until they are golden brown.
1. To the onion mixture, add water, the green sections of the leeks, potatoes, roasted squash, and carrots. Bring to a boil. Lower the heat, cover the pan, and simmer the veggies for approximately an hour, stirring every ten minutes and adding more water as necessary.
1. Add vegetable stock and salt and pepper to the vegetable combination. Increase the heat to medium and cook for 15 to 20 minutes, or until boiling.

#### Cook's Notes:

Leeks are excellent at retaining grit, as you'll see when you cut them longways, so beware!  If preferred, vegan margarine can be used in place of the oil.  The squash can also be pierced several times and microwaved on the "baked potato" option. Next, split it in half, remove the seeds and fibers, and repeat the process until the flesh is tender.

### Gomasio (Sesame Salt)

![](./@imgs/recipes/tejc_imgs/00065.jpeg)

Prep Time: 5 mins | Cook Time: 5 mins | Additional Time: 5 mins | Total Time: 15 mins | Servings: 48

#### Ingredients

- 2 tsps coarse sea salt
- ½ cup white sesame seeds
- ½ cup black sesame seeds

#### Directions

1. Over medium heat, place a cast-iron or heavy-bottomed stainless steel pan. Put salt in. About a minute of stirring is required to fully heat the mixture. Salt is added to a mortar. Sesame seeds are added to the pan. Stirring continuously is necessary until the seeds are aromatic and beginning to crack. Using the back of a stainless steel spoon, check the seeds to determine if they are ready; they should be dry, not moist.
1. The toasted seeds should be added to the mortar. Let it gently cool. Ideally, while seated, place the mortar at hip level. Use a pestle to crush the salt and open the seeds; the end product should resemble coarse sand.

#### Cook's Note:

When toasting the sesame seeds and salt, stay away from nonstick pans.

### Avocado Sushi with Brown Rice

![](./@imgs/recipes/tejc_imgs/00066.jpeg)

Prep Time: 30 mins | Cook Time: 45 mins | Total Time: 1 hrs 15 mins | Servings: 4

#### Ingredients

- 1 cup uncooked short grain brown rice
- 2 cups water
- 1 pinch sea salt
- 1 tbsp brown rice vinegar
- 1 avocado - peeled, pitted, and thinly sliced
- ¼ red bell pepper, cut into matchsticks
- ¼ cup alfalfa sprouts, or to taste
- 4 sheets nori (dry seaweed)

#### Directions

1. Brown rice should be rinsed and drained before being added to water in a pot set over medium heat. To allow the rice to absorb the water, add sea salt, then boil and simmer for 45 minutes. Stir in brown rice vinegar after letting the rice cool until it is warm.  Rice will have a slightly sticky texture.
1. Wrap a bamboo sushi rolling mat with plastic before beginning to roll the sushi. Place a nori sheet on the plastic wrap with the rough side facing up. Apply a thick, even coating of brown rice to the nori using damp fingers, leaving the top edge with about a 1/2-inch depth of rice uncovered. At the bottom edge of the sheet, arrange 1 or 2 avocado slices, a few red bell pepper pieces, and a few alfalfa sprouts.
1. The sushi is tightly rolled into a thick cylinder by picking up the edge of the bamboo rolling sheet and folding the bottom edge of the sheet up to enclose the veggies. Roll up the nori after moistening the exposed edge with a damp finger. Sushi should be rolled, then wrapped in a mat and gently squeezed to compact it. Before cutting each roll into six pieces for serving, let the rolls rest for a few minutes.

### Japanese Zucchini and Onions
 
![](./@imgs/recipes/tejc_imgs/00067.jpeg)

Prep Time: 10 mins | Cook Time: 10 mins | Total Time: 20 mins | Servings: 4

#### Ingredients

- 2 tbsps vegetable oil
- 1 medium onion, thinly sliced
- 2 medium zucchinis, cut into thin strips
- 2 tbsps teriyaki sauce
- 1 tbsp soy sauce
- 1 tbsp toasted sesame seeds
- ground black pepper

#### Directions

1. In a large skillet over medium heat, warm the oil. Add the onions and simmer for five minutes. For approximately a minute, while stirring, add the zucchini. Add sesame seeds, soy sauce, and teriyaki sauce. Sauté for about 5 minutes, or until the zucchini are soft. Add peppercorns, stir, and serve right away.

### Konbu Dashi

![](./@imgs/recipes/tejc_imgs/00068.jpeg)

Prep Time: 5 mins | Cook Time: 20 mins | Additional Time: 5 mins | Total Time: 30 mins | Servings: 4

#### Ingredients

- 1 (4 inch) piece dashi kombu (dried kelp)
- 4 cups water

#### Directions

1. To clean kombu, use a moist cloth. Put in a saucepan with water after being cut into 1-inch pieces. Bring to a boil, then simmer for 20 minutes on medium-low heat. After taking it off the heat, let it stand for a while. Before use, pass it through a mesh strainer.

#### Tips

Dashi ought to smell like the sea and have a light gold tint. When not in use, store daikon in a covered, chilled container. Dashi can last up to 14 days. When the stock has gone bad, it will smell sour.  Konbu can be chilled by soaking it in water for a couple of hours.

### Onigiri (Japanese Rice Balls)

![](./@imgs/recipes/tejc_imgs/00069.jpeg)

Prep Time: 20 mins | Cook Time: 25 mins | Additional Time: 10 mins | Total Time: 55 mins | Servings: 6

#### Ingredients

- 2 cups water
- 1 cup jasmine rice
- salt
- 1 sheet nori (dry seaweed), cut into 1-inch strips, or as desired (Optional)

#### Directions

1. Rice, 2 cups of water, and a dash of salt are all combined in a saucepan. Heat to a rolling boil, then turn heat down to a simmer and cover. Cook the rice for 20 to 25 minutes, stirring regularly to keep it from sticking, or until it is soft and the liquid has been absorbed. When the rice is cold enough to handle comfortably, fluff with a fork.
1. Water should be put in a small bowl. Add 2 tsps of salt to a different little bowl.
1. Rub salt on your hands after dipping them in water. Take a handful of warm rice and roll it into a ball. Next, gently shape the ball into an "L" or "C" shape using your hands. Finally, softly press the sides together to make a triangle.
1. A strip of nori should be wrapped around the triangle; if necessary, a little water can be used to make it adhere.

#### Cook's Notes:

If preferred, sushi rice can be substituted for the jasmine variety.
Garlic salt, seasoned salt, or salt combined with Chinese five-spice powder can all be used in place of salt.  If you want to add a filling, gently press down on the rice ball in step 3 to make a little pocket for it.  Rice should be shaped while still heated to ensure that it sticks well.  Don't lose heart if you don't get it right the first time; this takes a lot of practice and patience.  Sushi frequently uses nori, which is available in most supermarkets and Asian shops. I typically use scissors to cut into strips. Although nori has a very fishy flavor and aroma, I wouldn't recommend adding it if you don't enjoy fish or sushi.

### Vegan Japanese Spinach Salad

![](./@imgs/recipes/tejc_imgs/00071.jpeg)

Prep Time: 5 mins | Cook Time: 5 mins | Total Time: 10 mins | Servings: 2

#### Ingredients

- 2 tbsps toasted sesame seeds, divided
- 2 tbsps water
- 1 ½ tbsps soy sauce
- ½ tsp white sugar
- ½ (8 ounce) package fresh spinach
- 1 pinch salt

#### Directions

1. Put 1 1/2 tbsps of sesame seeds on a plate and crush them with the bottom of a saucepan or large measuring cup. Place in a basin. Stir the dressing after adding the water, soy sauce, and sugar.
1. Salt the water as it comes to a boil in a small pot. Cook the spinach for no longer than one minute after adding it. To stop the cooking process, place the spinach in a colander and rinse with cool water. To eliminate moisture, squeeze the spinach with your hands.  Strips should be cut.
1. Put cooked spinach in dishes for serving. Add remaining sesame seeds as a garnish, and drizzle dressing over top.

### Japanese-Style Sesame Green Beans

Prep Time: 5 mins | Cook Time: 15 mins | Total Time: 20 mins | Servings: 4

#### Ingredients

- 1 tbsp canola oil
- 1 ½ tsps sesame oil
- 1 pound fresh green beans, washed
- 1 tbsp soy sauce
- 1 tbsp toasted sesame seeds

#### Directions

1. A big skillet or wok should be warmed up. When the skillet is heated, add the canola and sesame oils before adding the whole green beans. The oil should be mixed into the beans. About 10 minutes of cooking time will result in brilliant green beans with some minor browning. Add soy sauce after taking the pan off the heat; cover and let settle for about five minutes. Add toasted sesame seeds after transferring to a serving plate.

### Japanese Tofu Salad

![](./@imgs/recipes/tejc_imgs/00072.jpeg)

Prep Time: 35 mins | Cook Time: 5 mins | Additional Time: 1 hrs | Total Time: 1 hrs 40 mins | Servings: 4

#### Ingredients

- 1 (14 ounce) package firm tofu, drained
- 3 tbsps soy sauce
- 1 tbsp mirin (sweetened rice wine)
- 1 tbsp rice vinegar
- 2 tsps sesame oil, or to taste
- 2 tbsps vegetable oil
- 2 cloves garlic, minced
- 1 tsp minced fresh ginger
- 1 large tomato, seeded and chopped
- 1 small red onion, thinly sliced
- ¼ cup chopped cilantro
- 1 tbsp sesame seeds

#### Directions

1. Between two plates, divide the tofu. Pour away the released liquid every 20 minutes while letting the tofu drain for roughly an hour while supporting it with a thick book.
1. In a small bowl, combine the soy sauce, mirin, rice vinegar, and sesame oil.
1. In a small pan, heat the vegetable oil over medium heat. Add the garlic and ginger, gently whisk, and simmer for 1 to 2 minutes, or until the garlic is just beginning to turn golden. Add to the soy sauce amalgamation.
1. Bite-sized pieces of pressed tofu should be placed in a bowl with tomato, onion, and cilantro. Dressing is added, then tossed onto the coat. Add sesame seeds as a garnish.

### Soba with Toasted Sesame Seed Sauce

Servings: 4

#### Ingredients

- ½ cup sesame seeds
- 8 ounces dried soba noodles
- 2 tbsps balsamic vinegar
- 1 tbsp white sugar
- 2 ½ tbsps soy sauce
- 1 clove garlic, minced
- 1 tsp dark sesame oil
- 5 green onions, chopped
- 3 cups broccoli florets

#### Directions

1. Set the oven to 375 degrees Fahrenheit (190 degrees C).
1. On a baking sheet with a rim, scatter the sesame seeds. The seeds should be baked for 10 to 12 minutes, or until the edges are a deep brown.
1. In the interim, heat a sizable pot of salted water to a boil. The noodles should be just soft after 5 to 6 minutes of cooking after being added. Drain them, give them a thorough cold water rinse, and then drain them once again.
1. In a large bowl, mix together the vinegar, sugar, soy sauce, garlic, sesame oil, and green onions. The noodles and the toasted sesame seeds should be added. Stir the broccoli in after a thorough tossing. Before serving, let the dish rest at room temperature for 30 minutes.

### Cucumber and Avocado Sushi

![](./@imgs/recipes/tejc_imgs/00073.jpeg)

Prep Time: 35 mins | Cook Time: 25 mins | Additional Time: 5 mins | Total Time: 1 hrs 5 mins | Servings: 6

#### Ingredients

- 1 ¼ cups water
- 1 cup uncooked glutinous white rice (sushi rice)
- 3 tbsps rice vinegar
- 1 pinch salt
- 4 sheets nori (dry seaweed)
- ½ medium cucumber, sliced into thin strips
- 1 medium avocado - peeled, pitted and sliced

#### Directions

1. Rice and water should be combined in a pan and heated until boiling. For about 20 minutes or until the rice is soft and the water has been absorbed, cover the pot, lower the heat, and simmer.  Add salt and vinegar after turning off the heat, and then leave the mixture to cool for at least five minutes.
1. Wrap a bamboo sushi mat in plastic so that the rice doesn't stick to it. One nori sheet should be laid over the plastic. Leave about 1/2 inch of the bottom unfilled after uniformly spreading the rice onto the nori sheet. Place the avocado, cucumber, and rice in a crosshatch pattern. Roll the avocado and cucumber over once on the mat before pressing down. To produce a long roll, unroll first, then roll again in the direction of the uncovered end of the nori. If required, seal the roll with a little water. To make additional rolls, repeat.
1. Each roll should be divided into six pieces using a sharp, moist
  knife.

#### Tips

To keep the sushi rolls vegetarian, you can either add imitation crab or smoked salmon if you'd like.

### Japanese Cucumber Sunomono

![](./@imgs/recipes/tejc_imgs/00074.jpeg)

Prep Time: 15 mins | Additional Time: 1 hrs 5 mins | Total Time: 1 hrs 20 mins | Servings: 4

#### Ingredients

- 2 large cucumbers
- 1 tsp salt
- 4 tsps white sugar
- 1 ½ tsps minced fresh ginger root
- 5 tbsps rice vinegar
- 1 tbsp soy sauce
- 2 tsps sesame seeds, or to taste

#### Directions

1. With a vegetable peeler, cut each cucumber into four strips, leaving some peel on for a striped appearance. Cucumbers should be split lengthwise in half after trimming the ends.
1. Use a mandoline or a knife to slice cucumbers into extremely thin slices. Add salt to the item and place it in a bowl. Mix thoroughly, then let stand for five minutes.
1. In the meantime, combine the sugar, ginger, vinegar, and soy sauce in a bowl.
1. Cucumbers can be squeezed dry with your hands or a fresh tea towel.  Mix the dressing with the cucumbers. Before serving, give the mixture a good stir and refrigerate in the fridge for an hour.
1. Sprinkle sesame seeds over each plate just before serving.  Enjoy!

### Homemade Pickled Ginger (Gari)

![](./@imgs/recipes/tejc_imgs/00075.jpeg)

Prep Time: 40 mins | Cook Time: 5 mins | Total Time: 45 mins | Servings: 32

#### Ingredients

- 8 ounces fresh young ginger root, peeled
- 1 ½ tsps sea salt
- 1 cup rice vinegar
- ⅓ cup white sugar

#### Directions

1. Ginger should be chopped. Add to a bowl, season with sea salt, and toss to combine. After around 30 minutes, transfer to a fresh jar with a cover.
1. Once the sugar has dissolved, stir the rice vinegar and sugar in the pan. Pour the boiling liquid over the pieces of ginger root in the container after bringing it to a boil.
1. After letting the liquid cool, tightly cap the container. Don't panic if the liquid's color slowly shifts to pink after a few minutes. Keep chilled for no less than a week.
1. For serving, slice the ginger into paper-thin pieces.

#### Tips

You could observe that your liquid and/or ginger are beginning to take on a little pink hue, as stated in the recipe. This isn't a big deal and is caused by the ginger and rice wine vinegar reacting with each other. Red coloring is often added to pickled ginger sold in stores to give it the pink color that makes it easy to spot.  Talk to your local extension office about safe ways to can and store food in your area that take into account your altitude. 

## Flavorful Vegetarian Japanese Recipes
### Cucumber and Avocado Sushi

Prep Time: 35 mins | Cook Time: 25 mins | Additional Time: 5 mins | Total Time: 1 hrs 5 mins | Servings: 6

#### Ingredients

- 1 ¼ cups water
- 1 cup uncooked glutinous white rice (sushi rice)
- 3 tbsps rice vinegar
- 1 pinch salt
- 4 sheets nori (dry seaweed)
- ½ medium cucumber, sliced into thin strips
- 1 medium avocado - peeled, pitted and sliced

#### Directions

1. Rice and water should be combined in a pan and heated until boiling. For about 20 minutes or until the rice is soft and the water has been absorbed, cover the pot, lower the heat, and simmer.  Add salt and vinegar after turning off the heat, and then leave the mixture to cool for at least five minutes.
1. Wrap a bamboo sushi mat in plastic so that the rice doesn't stick to it. One nori sheet should be laid over the plastic. Leave about 1/2 inch of the bottom unfilled after uniformly spreading the rice onto the nori sheet. Place the avocado, cucumber, and rice in a crosshatch pattern. Roll the avocado and cucumber over once on the mat before pressing down. To produce a long roll, unroll first, then roll again in the direction of the uncovered end of the nori. If required, seal the roll with a little water. To make additional rolls, repeat.
1. Each roll should be divided into six pieces using a sharp, moist knife.

#### Tips

To keep the sushi rolls vegetarian, you can either add imitation crab or smoked salmon if you'd like.

### Japanese Zucchini and Onions

Prep Time: 10 mins | Cook Time: 10 mins | Total Time: 20 mins | Servings: 4

#### Ingredients

- 2 tbsps vegetable oil
- 1 medium onion, thinly sliced
- 2 medium zucchinis, cut into thin strips
- 2 tbsps teriyaki sauce
- 1 tbsp soy sauce
- 1 tbsp toasted sesame seeds
- ground black pepper

#### Directions

1. Together, in a big skillet over medium heat, warm the oil. Add the onions and simmer for five minutes. For approximately a minute, while stirring, add the zucchini. Add sesame seeds, soy sauce, and teriyaki sauce. Sauté for about 5 minutes, or until the zucchini are soft. Add peppercorns, stir, and serve right away.

### Japanese Tofu Salad

Prep Time: 35 mins | Cook Time: 5 mins | Additional Time: 1 hrs | Total Time: 1 hrs 40 mins | Servings: 4

#### Ingredients

- 1 (14 ounce) package firm tofu, drained
- 3 tbsps soy sauce
- 1 tbsp mirin (sweetened rice wine)
- 1 tbsp rice vinegar
- 2 tsps sesame oil, or to taste
- 2 tbsps vegetable oil
- 2 cloves garlic, minced
- 1 tsp minced fresh ginger
- 1 large tomato, seeded and chopped
- 1 small red onion, thinly sliced
- ¼ cup chopped cilantro
- 1 tbsp sesame seeds

#### Directions

1. Between two plates, divide the tofu. Pour away the released liquid every 20 minutes while letting the tofu drain for roughly an hour while supporting it with a thick book.
1. In a small bowl, combine the soy sauce, mirin, rice vinegar, and sesame oil.
1. In a small pan, heat the vegetable oil over medium heat. Add the garlic and ginger, gently whisk, and simmer for 1 to 2 minutes, or until the garlic is just beginning to turn golden. Add to the soy sauce amalgamation.
1. Bite-sized pieces of pressed tofu should be placed in a bowl with tomato, onion, and cilantro. Dressing is added, then tossed onto the coat. Add sesame seeds as a garnish.

### Vegan Japanese Spinach Salad
 
Prep Time: 5 mins | Cook Time: 5 mins | Total Time: 10 mins | Servings: 2

#### Ingredients

- 2 tbsps toasted sesame seeds, divided
- 2 tbsps water
- 1 ½ tbsps soy sauce
- ½ tsp white sugar
- ½ (8 ounce) package fresh spinach
- 1 pinch salt

#### Directions

1. Put 1 1/2 tbsps of sesame seeds on a plate and crush them with the bottom of a saucepan or large measuring cup. Place in a basin. Stir the dressing after adding the water, soy sauce, and sugar.
1. Salt the water as it comes to a boil in a small pot. Cook the spinach for no longer than one minute after adding it. To stop the cooking process, place the spinach in a colander and rinse with cool water. To eliminate moisture, squeeze the spinach with your hands.  Strips should be cut.
1. Put cooked spinach in dishes for serving. Add remaining sesame seeds as a garnish, and drizzle dressing over top.

### Japanese Restaurant Cucumber Salad

Prep Time: 15 mins | Additional Time: 30 mins | Total Time: 45 mins | Servings: 4

#### Ingredients

- 2 tbsps white sugar
- 2 tbsps rice vinegar
- 1 tsp Asian (toasted) sesame oil
- 1 tsp chili paste (sambal oelek)
- salt to taste
- 2 large cucumbers - peeled, seeded, and cut into 1/4-inch slices

#### Directions

1. In a bowl, combine salt, sugar, rice vinegar, sesame oil, and chile paste. Cucumbers are added; swirl to coat. Before serving the salad at room temperature, let it marinade for 30 minutes.

### Tamagoyaki with Mushroom and Mozzarella Cheese

Prep Time: 15 mins | Cook Time: 10 mins | Additional Time: 5 mins | Total Time: 30 mins | Servings: 2

#### Ingredients

- 2 tsps olive oil
- 6 button mushrooms, sliced very thin
- 3 eggs
- 2 ½ tbsps white sugar
- 1 pinch salt and ground black pepper to taste
- ¼ tsp red chile powder, or to taste
- 2 tbsps vegetable oil, divided
- 1 ounce mozzarella cheese, sliced very thin

#### Directions

1. A big skillet with medium heat is used to heat the olive oil. Add the mushrooms, and simmer and stir for 5 minutes, or until the mushrooms are browned and have released their moisture. Turn off the heat, drain, and pat yourself dry.
1. In a bowl, combine the eggs, sugar, salt, pepper, and red chile powder.
1. Set the skillet over medium heat after coating it with vegetable oil. Spoon a portion of the beaten eggs into the pan and tilt it to spread them out into a thin layer. Cook for 1 to 2 minutes, or until almost set. To release the edges, slide a heat-resistant rubber spatula along them. Add some mushrooms on top, roll up the egg, and place it on the skillet's side.
1. Add more vegetable oil to the skillet to grease it. To make a second layer, add more eggs. Get the raw egg underneath the first roll-up by lifting it. Top with mozzarella cheese, and then roll the second layer over the top.
1. Using the remaining oil, eggs, mushrooms, and mozzarella cheese, layer and roll once more. Cook finished tamagoyaki for 30 seconds on each side, or until only lightly browned.
1. Tamagoyaki should be moved to a cutting board. Before slicing, allow it to cool for 5 to 10 minutes.

### Vegetarian Nori Rolls

Prep Time: 30 mins | Cook Time: 30 mins | Additional Time: 1 hrs | Total Time: 2 hrs | Servings: 5

#### Ingredients

- 2 cups uncooked short-grain white rice
- 2 ¼ cups water
- ¼ cup soy sauce
- 2 tsps honey
- 1 tsp minced garlic
- 3 ounces firm tofu, cut into 1/2 inch strips
- 2 tbsps rice vinegar
- 4 sheets nori seaweed sheets
- ½ cucumber, julienned
- ½ avocado, julienned
- 1 small carrot, julienned

#### Directions

1. Rice should be covered with water in a big pot and left to stand for 30 minutes.
1. Combine soy sauce, honey, and garlic on a small plate. Tofu should be marinated in this mixture for at least 30 minutes.
1. Rice should be cooked in a pot with water until it is thick and sticky, about 20 minutes after bringing it to a boil. Cooked rice and rice vinegar should be combined in a sizable glass bowl.
1. Lay a bamboo mat on top of a sheet of nori. Spread 1/4 of the rice evenly over the nori using damp fingertips, leaving about 1/2 inch on the top edge of the nori. Put 2 marinated tofu strips end to end, about 1 inch from the base. Place 2 pieces of cucumber, an avocado, and a carrot next to the tofu.
1. Using the mat, tightly roll the nori, starting at the bottom. By drizzling water over the top 1/2 inch, seal Add the remaining ingredients and repeat. Slice into 1-inch-thick slices with a serrated knife.

#### Tips

The whole list of marinade ingredients is included in the nutrition facts for this recipe. The amount of marinade actually ingested will vary according to the ingredients, cooking method, and marinating period.

### Sweet Potato Tempura

Prep Time: 20 mins | Cook Time: 15 mins | Total Time: 35 mins | Servings: 4

#### Ingredients

Tempura:

- 2 large eggs
- ¾ cup ice water
- 3 tbsps ice water
- ¾ cup all-purpose flour
- 1 tbsp all-purpose flour
- ½ tsp salt
- 2 cups oil for frying
- 1 large sweet potato, scrubbed and sliced into 1/8-inch slices

Dipping Sauce:

- ¼ cup rice wine
- ¼ cup soy sauce

#### Directions

1. Creating the tempura: In a large bowl, whisk eggs until foamy. The batter should still be quite lumpy after adding 3/4 cup plus 3 tbsps of ice water, 3/4 cup plus 1 tbsp of flour, and salt.
1. a deep fryer or large pot with 350 degrees Fahrenheit (175 degrees C) of oil. Over many layers of paper towels, place a wire rack.
1. Using paper towels, pat dry slices of sweet potatoes. Three slices are dipped into the batter, with the extra batter dripping back into the bowl. Cook in the hot oil for about two minutes on each side, or until golden brown. Transfer the sweet potatoes to the wire rack using a slotted spoon. Continue dipping and frying the remaining sweet potatoes.
1. Creating the dipping sauce Soy sauce and rice wine are combined in a small bowl. accompanying tempura.

### Japanese-Style Sesame Green Beans

Prep Time: 5 mins | Cook Time: 15 mins | Total Time: 20 mins | Servings: 4

#### Ingredients

- 1 tbsp canola oil
- 1 ½ tsps sesame oil
- 1 pound fresh green beans, washed
- 1 tbsp soy sauce
- 1 tbsp toasted sesame seeds

#### Directions

1. A big skillet or wok should be warmed up. When the skillet is heated, add the canola and sesame oils before adding the whole green beans. The oil should be mixed into the beans. About 10 minutes of cooking time will result in brilliant green beans with some minor browning. Add soy sauce after taking the pan off the heat; cover and let settle for about five minutes. Add toasted sesame seeds after transferring to a serving plate.

### Cucumber Sunomono

Prep Time: 15 mins | Additional Time: 1 hrs | Total Time: 1 hrs 15 mins | Servings: 5

#### Ingredients

- 2 large cucumbers, peeled
- ⅓ cup rice vinegar
- 4 tsps white sugar
- 1 tsp salt
- 1 ½ tsps minced fresh ginger root

#### Directions

1. Cucumbers should be split lengthwise, and any large seeds should be removed. Cut into very thin slices by cutting across.
1. Combine the vinegar, sugar, salt, and ginger in a small bowl. Mix thoroughly. Put the cucumbers in the basin and whisk to coat them with the mixture. Place the dish of cucumbers in the refrigerator for at least an hour before serving.

### Soba with Toasted Sesame Seed Sauce

Servings: 4

#### Ingredients

- ½ cup sesame seeds
- 8 ounces dried soba noodles
- 2 tbsps balsamic vinegar
- 1 tbsp white sugar
- 2 ½ tbsps soy sauce
- 1 clove garlic, minced
- 1 tsp dark sesame oil
- 5 green onions, chopped
- 3 cups broccoli florets

#### Directions

1. Set the oven to 375 degrees Fahrenheit (190 degrees C).
1. On a baking sheet with a rim, scatter the sesame seeds. The seeds should be baked for 10 to 12 minutes, or until the edges are a deep brown.
1. In the interim, heat a sizable pot of salted water to a boil. The noodles should be just soft after 5 to 6 minutes of cooking after being added. Drain them, give them a thorough cold water rinse, and then drain them once again.
1. In a large bowl, mix together the vinegar, sugar, soy sauce, garlic, sesame oil, and green onions. The noodles and the sesame seeds should be added. Stir the broccoli in after a thorough tossing. Before serving, let the dish rest at room temperature for 30 minutes.

## Unbeatable Udon Noodle Recipes
### Chicken Yaki Udon

Prep Time: 15 mins | Cook Time: 15 mins | Total Time: 30 mins | Servings: 4

#### Ingredients

- 6 ounces frozen udon noodles
- 1 tbsp olive oil
- ½ pound boneless chicken breasts, cut into thin strips
- ½ onion, sliced
- ½ red bell pepper, sliced
- ½ cup shredded cabbage
- ½ cup carrot matchsticks
- 1 tsp minced garlic
- 2 tbsps soy sauce, or more to taste
- 1 tbsp gochujang (Korean chile paste)
- 1 tbsp ketchup
- salt and ground black pepper to taste

#### Directions

1. A large saucepan of lightly salted water should be brought to a boil. Udon should be cooked in boiling water for 10 to 12 minutes, stirring occasionally, until the noodles are soft but still firm to the bite. Rinse the noodles with cold water after draining.
1. Over medium heat, warm the olive oil in a big pan or wok. Add the chicken and simmer for 3 to 4 minutes, or until done. Garlic, cabbage, carrots, onion, and bell pepper should also be added.  Vegetables should be cooked for 3 to 4 minutes to retain the majority of their crispness.
1. Add soy sauce to the chicken and vegetables, and cook for an additional 1 to 2 minutes. Toss thoroughly after adding the udon noodles. Add the ketchup and gochujang, mixing well to evenly distribute the flavors. Add salt and pepper to taste.

#### Tips

If preferred, boneless chicken thighs may be substituted for boneless chicken breasts.  The red bell pepper can be swapped out for orange or yellow if you'd prefer.

### Japanese Nabeyaki Udon Soup

Prep Time: 15 mins | Cook Time: 25 mins | Total Time: 40 mins | Servings: 4

#### Ingredients

- 6 cups prepared dashi stock
- ¼ pound chicken, cut into chunks
- 2 carrots, diced
- ⅓ cup soy sauce
- 3 tbsps mirin
- ½ tsp white sugar
- ⅓ tsp salt
- 2 (12 ounce) packages firm tofu, cubed
- ⅓ pound shiitake mushrooms, sliced
- 5 ribs and leaves of bok choy, chopped
- 1 (9 ounce) package fresh udon noodles
- 4 eggs
- 2 leeks, diced

#### Directions

1. In a pot over medium heat, warm the dashi stock, chicken, carrots, soy sauce, mirin, sugar, and salt. Cook for 5 to 7 minutes, or until the core of the chicken is no longer pink. When the veggies are soft, add the tofu, mushrooms, and bok choy and stir for an additional 5 minutes.
1. Udon noodles should be added to the broth after it has simmered for three to four minutes. Add leeks and cracked eggs to the soup; simmer for an additional 5 minutes or until the eggs are just starting to firm up.

### Kimchi Udon Noodle Stir-Fry

Prep Time: 15 mins | Cook Time: 15 mins | Total Time: 30 mins | Servings: 4

#### Ingredients

- 2 (7 ounce) packages fresh udon noodles
- 1 ½ tbsps honey
- 1 tbsp soy sauce
- 1 tbsp sriracha sauce
- ½ tbsp rice vinegar
- 2 strips bacon, chopped
- 1 ½ cups chopped kimchi
- 1 clove garlic, minced
- 1 tsp toasted sesame oil
- 4 scallions, diagonally sliced

#### Directions

1. A large saucepan of lightly salted water should be brought to a boil. Udon should be cooked in boiling water for 3 to 5 minutes, stirring occasionally, until the noodles are soft but still firm to the bite. Drain.
1. In a bowl, combine the honey, rice vinegar, soy sauce, and sriracha sauce. Discard the sauce.
1. Bacon should be cooked in a sizable pan over high heat for two to three minutes, or until the fat has been rendered but the bacon is not yet crispy. Cook the kimchi and garlic for one minute. Stir thoroughly after adding the sauce, udon noodles, and sesame oil.  Noodles are taken off the fire and placed on a plate. Add scallions on top.

#### Cook's Note:

If necessary, substitute 6 to 7 ounces of dried noodles. Cook for 8 minutes in boiling water.

### Asian Steak and Noodle Bowl

Prep Time: 15 mins | Cook Time: 44 mins | Additional Time: 4 hrs | Total Time: 4 hrs 59 mins | Servings: 4

#### Ingredients

- ½ cup low-sodium soy sauce
- ⅓ cup vegetable oil
- ⅓ cup brown sugar
- 1 tbsp minced ginger
- ½ tsp garlic powder
- 2 pounds flank steak
- 1 (10 ounce) package dried Japanese udon noodles
- 6 ounces snow peas
- 1 cup broccoli florets
- 1 tbsp mirin (Japanese sweet wine)

#### Directions

1. In a large bowl, combine the soy sauce, vegetable oil, brown sugar, ginger, and garlic powder. Using a large fork, prick the flank steak numerous times. Put the item in the bowl and wrap it with plastic.  Let marinate for at least four hours and up to overnight in the refrigerator.
1. A large saucepan of salted water should be brought to a boil. Udon noodles should be cooked in boiling water, stirring occasionally, for 13 to 14 minutes, or until soft but still firm to the bite.  Drain.
1. A sizable skillet is heated over high heat. Take the steak out of the marinade and cook it for two minutes on each side, or until well-browned. Save the marinade.
1. Lightly oil the grill grate and preheat the grill to medium heat.  Cook steak on the grill, basting with half the reserved marinade, for at least 10 minutes on each side or until the internal temperature has reached 140 degrees F (60 degrees C) for medium or 150 degrees F (65 degrees C) for medium-well. Slice the meat thinly the other way around.
1. Mix the snow peas, broccoli florets, and mirin with the marinade that was left over in the skillet. About 2 minutes of cooking and stirring over medium-high heat should result in soft snow peas. Add the drained noodles and blend thoroughly.
1. The noodle mixture should be divided among big dishes. Add steak slices on top.

#### Cook's Note:

The ideal way to prepare flank steak is medium-rare or medium-well. A medium-rare steak will be too chewy.

### Quick Sesame Chicken Noodle Bowl

Prep Time: 10 mins | Cook Time: 15 mins | Total Time: 25 mins | Servings: 6

#### Ingredients

- 28 ounces fresh udon noodles
- 1 pound chicken tenderloins
- 2 tbsps oil for cooking
- 2 stalks green onions - chopped, green and white parts separated, divided
- 1 ½ tbsps grated fresh ginger root
- 5 cloves garlic, minced
- 1 (12 ounce) package frozen stir-fry vegetables
- Sauce:
- 4 tbsps oyster sauce
- 2 tbsps sweet soy sauce (kecap manis)
- 2 tbsps soy sauce
- 1 tbsp sesame oil
- 3 tbsps toasted sesame seeds

#### Directions

1. To separate the udon noodles, soak them in warm water.
1. During this time, use a sharp knife to cut the tenderloins' tendon. Tenderloins should be added to a food processor and pulsed.  Not in large bits or as ground chicken, but rather in finely chopped form.
1. In a wok, heat the oil until it is shimmering. Include the white pieces of the ginger, garlic, and green onions. For 30 seconds, cook. Cook and stir the chicken for 7 to 10 minutes, or until the middle is no longer pink. Add vegetables to the stir-fry. Sauté for 3 minutes or until thoroughly cooked.
1. Noodles should be drained before being added to the stir-fry ingredients.
1. Oyster sauce, spicy soy sauce, soy sauce, and sesame oil should all be combined in one bowl. To prevent breaking the noodles, gently combine the stir-fry mixture with the sauce. Add sesame seeds and the green onion ends as garnish.

### Thai Cucumber Salad with Udon Noodles

Prep Time: 20 mins | Cook Time: 15 mins | Additional Time: 25 mins | Total Time: 1 hrs | Servings: 4

#### Ingredients

- ½ cup white sugar
- ¼ cup water
- 1 tbsp soy sauce
- 1 tsp ground ginger
- 1 clove garlic, crushed
- ½ tsp salt
- ½ cup rice vinegar
- 8 ounces udon noodles, or more to taste
- 2 English cucumbers, sliced
- 1 large shallot, thinly sliced
- ½ red bell pepper, thinly sliced
- ½ small Thai chile pepper, minced

#### Directions

1. In a small saucepan, combine salt, sugar, water, soy sauce, ginger, and garlic. Around 5 minutes of boiling and stirring are required to completely dissolve the sugar and salt. Remove it from the heat and allow it to cool for around five minutes. To prepare the dressing, stir in the rice vinegar.
1. Bring water in a big pot to a boil. Udon noodles should be cooked in boiling water, stirring occasionally, for 10 to 12 minutes, or until they are soft but still firm to the bite. Drain. Drain after rinsing with cold water.
1. Udon should be moved to a big bowl. Red bell pepper, shallot, cucumber, and Thai chili pepper should all be included. Pour the dressing into the bowl and toss to coat the salad completely. Before serving, allow the meat to marinate for at least 20 minutes.

#### Cook's Notes:

For those who like less spice, you can delete the Thai chile pepper or replace it with a milder pepper. It can also be used as a garnish. You can use any hue of bell pepper.

### Peanut Butter Noodles

Prep Time: 15 mins | Cook Time: 10 mins | Total Time: 25 mins | Servings: 4

#### Ingredients

- 8 ounces Udon noodles
- ½ cup chicken broth
- 3 tbsps soy sauce
- 3 tbsps peanut butter
- 1 ½ tbsps minced fresh ginger root
- 1 ½ tbsps honey
- 3 cloves garlic, minced
- 2 tsps hot chile paste (Optional)
- ¼ cup chopped green onions
- ¼ cup chopped peanuts

#### Directions

1. A large saucepan of lightly salted water should be brought to a boil. Udon should be cooked in boiling water for 10 to 12 minutes, stirring occasionally, until the noodles are soft but still firm to the bite. Drain.
1. In the meantime, put in a medium pot: chicken broth, soy sauce, peanut butter, ginger, honey, garlic, and chili paste. Stirring often, cook over medium heat until peanut butter dissolves and sauce is well cooked.
1. Drained noodles should be added to the peanut butter sauce and coated. Add peanuts and green onions as a garnish.

### Sesame Udon Noodles

Prep Time: 20 mins | Cook Time: 5 mins | Total Time: 25 mins | Servings: 4

#### Ingredients

- 2 cloves garlic, minced
- 1 tbsp minced fresh ginger root
- ¼ cup soy sauce
- 3 tbsps rice vinegar
- ¼ cup peanut oil
- 3 tbsps sesame oil
- 1 dash hot pepper sauce
- ½ green bell pepper, julienned
- ½ red bell pepper, julienned
- ½ yellow bell pepper, julienned
- 4 green onions, minced
- 2 cups diagonally sliced snap peas
- 2 tbsps sesame seeds, toasted
- 1 (7 ounce) package fresh udon noodles

#### Directions

1. Garlic, ginger, soy sauce, rice vinegar, peanut oil, sesame oil, and hot pepper sauce should all be combined in a container with a tight-fitting lid. Shake vigorously to combine after tightly fitting the lid. To allow the flavors to meld, set it aside.
1. Bring water in a big pot to a boil. Udon noodles should be added and cooked for 3 minutes, or until soft. Place in a serving bowl after draining.
1. Bell peppers, green onions, and peas should all be put in a bowl that can go in the microwave. Heat the food in the microwave until it is warm but still crunchy. Add the noodles to the bowl, then drizzle the dressing over everything. After tossing to evenly distribute the dressing, top with toasted sesame seeds.

Toasting sesame seeds

1. Together, in a dry skillet over medium heat, toast sesame seeds.  For about 3 minutes, shake the pan occasionally or stir the contents.

### Pick-Me-Up Egg Drop Soup

Prep Time: 15 mins | Cook Time: 15 mins | Total Time: 30 mins | Servings: 2

#### Ingredients

- 1 (16 ounce) can chicken broth
- ½ tsp soy sauce
- ½ tsp sesame oil, or to taste
- ¼ cup finely broken udon noodles
- 2 tsps water
- 1 tsp cornstarch (Optional)
- 2 eggs, beaten
- sea salt to taste
- freshly ground black pepper to taste

#### Directions

1. Together in small saucepan, combine the sesame oil, soy sauce, and chicken broth; bring to a boil. Pieces of udon should be put into the broth and let it simmer for 10 to 12 minutes, while being stirred every now and then to make sure it cooks evenly.
1. In a bowl, combine cornstarch and water. The broth mixture should be smooth and slightly thickened after adding the cornstarch mixture. Pour eggs in gradually while stirring softly and often for 2 to 3 minutes, or until eggs are wispy and cooked through. Black pepper and sea salt are used to season food.

#### Cook's Note:

To prevent the egg from clumping up, make sure to stir the soup constantly as you add it. If you're concerned about this, stir quickly.  Understirring is a bigger issue than overstirring. Wheat toast goes well with this because it isn't too heavy.

### Easy Spicy Udon Cold Salad

Prep Time: 25 mins | Cook Time: 2 mins | Additional Time: 4 hrs | Total Time: 4 hrs 27 mins | Servings: 4

#### Ingredients

- 1 (7 ounce) package fresh udon noodles (such as Ka-Me®)
- ½ (14.5 ounce) can bean sprouts, drained
- ½ onion, thinly sliced
- 1 carrot, grated
- ½ (4.5 ounce) can mushroom pieces, drained
- 1 bunch fresh cilantro, chopped, or to taste
- 2 tsps sesame seeds
- 3 tbsps rice vinegar
- 3 tbsps soy sauce
- 1 tbsp olive oil
- 1 tbsp sesame oil
- 1 tbsp white sugar, or to taste
- 1 tbsp diced ginger
- 1 tsp minced garlic
- 1 splash lime juice
- 1 splash white wine
- 1 dash sriracha sauce
- 1 pinch red pepper flakes, or to taste

#### Directions

1. Remove the outer bag of the udon noodles. Put the inner pouch on a microwave-safe dish after multiple punctures. For around 90 seconds, microwave the food until it is soft.
1. Place the noodles in a big bowl. Add the bean sprouts, sesame seeds, cilantro, onion, carrot, and mushrooms.
1. Stir together the following ingredients in a small bowl: rice vinegar, miso, olive oil, sesame oil, sugar, ginger, onion, lime juice, white wine, fajita seasoning, and red pepper flakes. Pour the dressing over the bowl of noodles and stir to incorporate.  Refrigerate for at least 4 hours to allow flavors to meld.

#### Editor's Note:

If you're using dried udon noodles, you may cook them in a sizable pot of boiling water for 10 to 12 minutes, stirring occasionally, until they are soft to the bite. Drain, then continue the recipe.

### Kitsune Udon

Prep Time: 15 mins | Cook Time: 30 mins | Additional Time: 5 mins | Total Time: 50 mins | Servings: 2

#### Ingredients

Dashi:

- 1 ounce kombu (Japanese dried kelp)
- 5 cups cold water
- ¾ cup bonito flakes

Kitsune Udon:

- 2 ounces aburaage (frozen, defrosted fried tofu cakes)
- 2 ½ tbsps soy sauce, divided
- 2 tbsps mirin, divided
- 1 tbsp white sugar
- 1 tsp kosher salt
- 1 pound frozen udon noodles
- 2 tbsps thinly sliced scallions

#### Directions

1. 2 cups of loosely packed 2-inch pieces of kombu should result from this. Over medium heat, gently bring water and kombu to a boil. Continue to cook for 5 minutes. Remove and discard the kombu after removing it from the heat. Add the bonito flakes, then simmer for 2 minutes over medium heat. Remove from heat when simmering, filter, and then throw away solids. Refill the pot with dashi and leave it aside.
1. Make a medium bowl and add the avocado. For about five minutes, cover with hot water and soak until fluffy. After straining, squeeze the absorbent material with your hands to remove any extra water and let it cool momentarily until it is safe to handle.
1. In a medium saucepan over medium-high heat, bring 1 cup dashi, 1 1/2 tbsps soy sauce, 1 tbsp mirin, and sugar to a boil. Add the soaked agave and reduce the heat to a simmer. Cook for about 5 minutes with the lid on, or until the majority of the liquid has been absorbed. Halfway through, flip the aburage.
1. Over medium heat, gently bring the remaining 4 cups of dashi to a boil. Add salt, the final tbsp of soy sauce, and the final tbsp of mirin. Add the frozen udon noodles and toss occasionally over medium heat for 2 minutes, or until the noodles have separated and are soft to the bite.
1. Pour equal amounts of noodles and broth into two dishes. Add seasoned arugula and thinly sliced scallions on top.

#### Cook's Notes:

Deep-fried tofu pouches manufactured from soybeans are known as abuse. This recipe's cooking technique converted the ingredient into an infringement. Instead, you may use 2 ounces of fried bean curd. Kombu and bonito flakes are used to create traditional dashi. Furthermore, dashi tea bags soaked in hot water can be used to make it.

### Curry Udon

Prep Time: 15 mins | Cook Time: 25 mins | Total Time: 40 mins | Servings: 4

#### Ingredients

- 3 carrots, cut into bite-size pieces
- 1 small onion, cut into bite-size pieces
- 3 tbsps water
- ¼ cup vegetable oil
- ½ cup all-purpose flour
- 2 tbsps all-purpose flour
- 2 tbsps red curry powder
- 5 cups hot vegetable stock
- ¼ cup soy sauce
- 2 tsps maple syrup
- 8 ounces udon noodles, or more to taste

#### Directions

1. Put the carrots, onion, and water in a bowl that can go in the microwave. For roughly 4 minutes on high, cover and microwave the food until soft.
1. Oil in a big pot is heated at medium heat. For a paste, whisk in 1/2 cup plus 2 tbsps of flour for 1 to 2 minutes. Add curry powder and stir. For about 3 minutes, slowly whisk in hot vegetable stock until it's integrated and smooth. Soy sauce, maple syrup, and cooked onions and carrots are added to the curry soup.
1. Bring water in a big pot to a boil. Udon noodles should be cooked in boiling water, stirring occasionally, for 10 to 12 minutes, or until they are soft but still firm to the bite. Drain. To the curry broth, add noodles.

#### Cook's Note:

If you'd like, swap out the maple syrup for another liquid sweetener.

### Japanese Pan Noodles
 
Prep Time: 25 mins | Cook Time: 25 mins | Total Time: 50 mins | Servings: 4

#### Ingredients

- 1 (10 ounce) package fresh udon noodles
- ½ tsp sesame oil, divided, or to taste
- 2 cups chopped broccoli
- ½ green bell pepper, cut into matchsticks
- 2 small carrots, cut into matchsticks, or to taste
- ½ zucchini, thinly sliced
- 2 tbsps soy sauce
- 2 tbsps mirin (Japanese sweet wine)
- 1 tbsp chili-garlic sauce
- ¾ tsp minced ginger

#### Directions

1. A large saucepan of lightly salted water should be brought to a boil. Udon should be cooked in boiling water for 10 to 12 minutes, stirring occasionally, until the noodles are soft but still firm to the bite. Rinse with cold water after draining. Add a few drops of sesame oil and stir.
1. Over medium heat, warm the remaining sesame oil in a large skillet. Cook the broccoli for about 5 minutes, or until it is brilliant green and still crisp. Cook and toss the carrots and green bell pepper for about two minutes, or until they start to soften. Add the zucchini and simmer for another 2 minutes, or until slightly softened. Stir in the ginger, chili-garlic sauce, soy sauce, and mirin. Add the noodles and stir-fry for an additional 1 to 2 minutes, allowing the noodles to absorb some of the sauce.

#### Cook's Note:

Any type of vegetable is acceptable. I enjoy including bok choy, onions, bamboo shoots, etc. You can include any type of protein.

### Chicken Udon Noodle Soup

Prep Time: 20 mins | Cook Time: 15 mins | Total Time: 35 mins | Servings: 4

#### Ingredients

- 1 ½ (32 fluid ounce) containers chicken stock
- 1 tbsp minced fresh ginger root
- 1 tsp chili powder
- 1 clove garlic, minced
- 1 large cooked skinless, boneless chicken breast,
  chopped
- 1 head bok choy, chopped
- ¼ cup dried shiitake mushrooms
- 2 (7 ounce) packages dried udon noodles
- ½ cup mung bean sprouts
- 1 green onion, sliced diagonally
- 2 tbsps dried minced onion
- 1 tbsp chopped fresh cilantro

#### Directions

1. Over medium-high heat, bring chicken stock, ginger, chili powder, and garlic to a boil. Add the mushrooms, bok choy, and chicken; turn down the heat, and simmer for three minutes. For 4 minutes, add the noodles.
1. Pour the soup into bowls using a ladle. Add bean sprouts, cilantro, dried onions, and green onions as garnish.

### Prawns and Vegetables Over Pan-Fried Udon Noodles

Prep Time: 20 mins | Cook Time: 20 mins | Total Time: 40 mins | Servings: 2

#### Ingredients

- Sauce:
- ½ cup soy sauce
- ½ cup water
- ⅓ cup brown sugar
- ½ tbsp ground ginger
- Noodle Bowls:
- 1 ½ cups water, divided
- 4 stalks celery, halved lengthwise and cut into 2-inch sticks
- ½ red bell pepper, sliced into thin strips
- ½ yellow bell pepper, sliced into thin strips
- 10 prawns, peeled, or more to taste
- 1 ½ tbsps lemon juice
- 2 (7 ounce) packages fresh udon noodles

#### Directions

1. Together in a small saucepan over low heat, combine the soy sauce, water, brown sugar, and ginger. Sauce should be simmered, occasionally stirring, until ready to use.
1. Celery, bell peppers, and 1/2 cup water are combined in another small pot over medium heat while the sauce simmers. For about 10 minutes, cook under a cover without stirring. Drain, then keep protected.
1. Udon noodles and the final cup of water are combined in a big pot over medium heat. For 4 to 5 minutes, cook. Return the drained noodles to the pot and heat them up. Add a third of the sauce to the noodles and cook for an additional 3 to 4 minutes, or until cooked through. Add prawns after moving the noodles to the side of the pan. Sauté for 3 to 5 minutes, or until completely opaque.
1. Equally divide the noodles between two bowls. While the prawns are still in the saucepan, add the lemon juice and swirl to coat. Vegetables and prawns should be distributed equally on top of the noodles. As desired, spoon additional sauce on top.
