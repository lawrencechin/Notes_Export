# Karaage Japanese Fried Chicken
## [Japan Center](https://www.japancentre.com/en/recipes/37-karaage-japanese-fried-chicken)
![Karaage Japanese Fried Chicken](https://japancentre-images.global.ssl.fastly.net/recipes/pics/37/main/photo_shoryu_karaage.jpg?1469572853)

With karaage chicken by your side, you'll never want to eat takeaway chicken again. Inspired by Chinese fried chicken recipes, karaage is a delicious izakaya or bento box favourite, comprised of crunchy, deep fried pieces of marinated chicken served with a liberal squeeze of lemon juice or a side pot of kewpie mayonnaise. Perfect eaten hot or cold, this simple recipe is quick to make and produces fantastic results. Great for parties, new year's, or any get together you may have. 

### Ingredients

* 300g boneless skin-on chicken thighs

#### marinade:
* 3 tbsp soy sauce
* 1 tbsp cooking sake
* 5g fresh ginger, grated
* 2 cloves garlic, crushed

#### to fry:
* 1/4 cup katakuriko potato starch, to coat
* cooking oil, for frying

#### to serve:
* japanese mayonnaise
* lemon wedges

### How To Prepare

1. Cut chicken into bite-size pieces, leaving a little skin/fat on for extra crispiness.
2. Add the ginger, garlic, soy sauce and cooking sake to a bowl and mix until combined. Add the chicken, coat well, and allow to marinade for 30 minutes.
3. Drain any excess liquid from the chicken and add your katakuriko potato starch. Mix until the pieces are fully coated.
4. Heat some cooking oil in a pan to around 180°C and test the temperature by dropping in some flour (if it sizzles on impact, the oil is hot enough). Fry 3-4 pieces at a time for 3-4 minutes until they are deep golden brown colour, then remove and allow to drain on a wire rack or kitchen roll.
5. Serve hot or cold with some lemon wedges and a squeeze of Japanese mayonnaise.

### Tips and Information

* If you want to make karaage without the fuss, try using a ready made flour variety which includes all the marinade ingredients in one pack.
* Although chicken breast can be used for this recipe, thighs have more flavour and are generally less dry. Chicken wings are also a great option.
* Karaage tastes just as good when it is cold so why not try in a bento or at a summer picnic.