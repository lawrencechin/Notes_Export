# How to Make Calpis

Calpis is a Japanese fizzy drink of fermented milk and sugar to sup under the blossoming boughs

Henry Dimbleby | 6th April 2013 | [Link](https://www.theguardian.com/lifeandstyle/2013/apr/06/how-to-make-calpis)

![Calpis in a glass](./@imgs/calpis-drink.jpg)

## Ingredients

- 450g yoghurt
- 400g sugar
- 140ml lemon juice or 2 tsp citric acid
- 1 tsp vanilla essence

## Method

1. Place the yoghurt and sugar in a bowl over simmering water and whisk until smooth.
1. Continue whisking for 15 minutes or until it reaches 70-80C. Do not let it boil.
1. Remove from the heat and cool to room temperature. Whisk in the lemon juice (or citric acid) and vanilla essence to make the base syrup, which keeps well in the fridge.
1. To mix the drink, dilute a quantity of the syrup with four times the volume of water or soda water or sparkling water. Serve over ice.

## Preamble

I never found Japan as alien a place as people make out. Yes, you notice strangeness everywhere when you first arrive: taxi doors that open automatically, the drivers wearing white gloves; beer vending machines on street corners; perfect melons sold in gift-wrapped boxes for £80; everyone getting uproariously drunk and dancing down the street in their underpants with the boss, then turning up for work the next day as if nothing had happened.

But these are cosmetic differences. I lived in Tokyo for a year, and the longer you stay the more you realise life goes on in the same way everywhere: it's the basic stuff – love, friends and family, obligations and little freedoms – just dressed in different clothes.

I miss all the quirks now, particularly the gastronomic ones. I miss the Japanese breakfast of rice porridge, fish and pickles. I miss the fact that most restaurants in Japan specialise in just one form of cookery (it could be a robata grill, soba noodles or sashimi) and, as a result, get very, very good at it. And I miss calpis, the ubiquitous fermented milk drink that gets drunk by the gallon during hanami, the annual cherry blossom festival.

This syrup of fermented milk and sugar was introduced to Japan just after the first world war by Kaiun Mishima, an entrepreneur who had come across it in Mongolia and was taken by its gut-improving qualities. The fermentation introduces lactic acid, which gives it a sharp taste. This recipe uses lemon juice for the tang, although you can use citric acid if you have some in the back of your cupboard. For me, it is best very cold and without too much sugar.

If you fancy, you can shake two parts syrup with one part vodka and a little egg white over ice to make a calpis sour. As the Japanese would say: Oishi desu (It's delicious).