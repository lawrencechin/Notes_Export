# Wagamama Katsu Curry

![Katsu Curry](./@imgs/recipes/katsu_curry.jpg)

## Ingredients

### For the sauce (serves two)

- 2-3 tablespoons of vegetable oil
- 1 onion, finely chopped
- 1 garlic clove, crushed
- 2.5cm piece of ginger, peeled and grated
- 1 teaspoon of turmeric
- 2 heaped tablespoons of mild curry powder
- 1 tablespoon of plain flour
- 300ml chicken or vegetable stock
- 100ml coconut milk
- 1 teaspoon of light soy sauce
- 1 teaspoon of sugar, to taste

### For the dish (serves two)

- 120g rice (any type of rice you would like)
- katsu curry sauce, made from ingredients above
- 2 skinless chicken breasts
- 50g plain flour
- 2 eggs, lightly beaten
- 100g panko breadcrumbs
- 75ml vegetable oil, for deep-frying
- 40g mixed salad leaves

## Method

1. To start making the katsu curry sauce, place the onions, garlic and ginger in a pan on heat on the hob and stir them as they soften.
1. Next add the curry mix, before then adding the turmeric and continue to stir as the strong flavours are released.
1. Allow the mixture to sit on a low to medium heat for a minute or so.
1. Then add the flour, which will help to thicken the sauce, continuing to mix for a minute as it combines with the spices.
1. After watering down your chicken or vegetable stock, start slowly adding it to the mixture. Add a little bit at a time, stirring as you do so.
1. Once the chicken or vegetable stock as been added and stirred in, you can start to add the coconut milk. Although the recipe says to use 100ml, it is up to you how much you would like to use. The more you add, the creamier it will be. Just as with the stock, add a little bit at a time as you stir.
1. Next, add a little bit of a sugar and small amount of soy sauce to finish off your sauce.
1. Moving on to the rest of the dish, split your chicken fillet in half before turning it over in a bowl of flour, then in a bowl of lightly beaten eggs, and lastly in a bowl of panko breadcrumbs.
1. Once the chicken fillet has been coated in breadcrumbs, you need to deep fry it in vegetable oil, turning it over with tongs to achieve a golden colour. Executive chef Mr Mangleshot recommends being very cautious at this stage to make sure you do not burn yourself. 
1. Before serving your dish, strain the curry sauce to ensure it is as smooth as possible.
1. Cook the rice, which can be any type you would like, and pour it on the serving plate.
1. Once your chicken is cooked, remove it from the pan with your tongs, slice it diagonally in a Wagamama-esque fashion and place it on the plate next to the rice before adding mixed leaves as well.
1. Finally, as Mr Mangelshot puts it, “drench” your dish in the famous katsu curry sauce for the finishing touch.
