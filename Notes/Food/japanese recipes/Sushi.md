# Sushi

![Aah… Sushi](./@imgs/recipes/sushi.jpg)

## Sushi Rice
### Ingredients
#### For the Rice

- 300g sushi rice
- 100ml rice wine vinegar
- 2 tbsp golden caster sugar

#### For the Japanese Mayonnaise

- 3 tbsp mayonnaise
- 1 tbsp rice wine vinegar
- 1 tsp soy sauce

### Method

1. Place rice in saucepan with cold water - enough to cover the rice with a bit extra on top.
1. Leave rice in cold water for around 20-30 minutes.
1. Meanwhile, mix the rice wine vinegar and sugar until sugar dissolves.
1. Turn on heat until water boils then reduce to simmer with lid on.
1. Rice should be done when water has absorbed into rice.
1. Take rice off the heat and gently fold the seasoning trough the rice.
1. *Optional* - add some toasted sesame seeds to the rice
1. Allow to cool at room temperature before using however you see fit.

Via [BBC GoodFood](https://www.bbcgoodfood.com/recipes/simple-sushi)

## Tamagoyaki

![Tamago](./@imgs/recipes/tamagoyaki.jpg)

### Ingredients

- 4 eggs
- 1 tbsp soy sauce
- 1 tbsp mirin
- 1 tbsp sugar
- pinch of salt
- cooking oil as needed

### Method

1. First, beat your eggs well in a bowl using either a fork, or chopsticks if you are an expert chopstick user.
1. Add one tablespoon each of soy sauce, mirin and sugar and a little salt to your mix.
1. Put a small amount of cooking oil in your pan and bring it up to medium heat. Keep some kitchen roll handy to help keep the pan oiled during cooking.
1. Add a small amount of your egg mix into the heated pan. Once the egg has cooked slightly so that the top is still slightly uncooked, push it over to the side of your pan.
1. Add a little more oil to the pan using the kitchen roll and add another small amount of the egg mix to your pan. Again, wait for this to cook a little, but before it sets on top. You can then begin to roll the first bit of egg over the mix you just put in the pan until you have a small roll of egg.
1. Continue adding a small amount of egg while oiling the pan each time in between. As you add more egg and roll it up each time, your egg roll will start getting larger and easier to add new layers. Keep adding the egg in new layers until you have used it all up.
1. Your tamagoyaki is now finished so remove from the pan and wait to cool before slicing it up into thin pieces with a sharp knife.

Via [Japan Centre](https://www.japancentre.com/en/recipes/20-tamagoyaki-japanese-omelette)

## Teriyaki Sauce

### Ingredients

- 85g light brown soft sugar
- 70ml light soy sauce
- 1 large garlic clove, crushed
- 4cm piece ginger, peeled and finely grated
- 1 tbsp cornflour
- 1 tbsp rice wine vinegar

#### To serve

- sesame seeds (*optional*)
- 2 spring onions, finely sliced (*optional*)

### Method

1. Pour 350ml water into a small saucepan with the sugar, soy sauce, garlic and ginger. Slowly bring to a simmer, stirring occasionally until the sugar has dissolved. Cook for 5 mins more or until glossy and slightly thickened. Combine the cornflour with 1 tbsp water and quickly whisk through the sauce. Whisk through the rice wine vinegar. If it's still too thick, add a splash more water. Pour into a clean jar and leave to cool at room temperature.
1. Once cooled, will keep in the fridge for up to 1 week. Sprinkle with sesame seeds and spring onions just before serving, if you like.

Via [BBC GoodFood](https://www.bbcgoodfood.com/recipes/teriyaki-sauce)
