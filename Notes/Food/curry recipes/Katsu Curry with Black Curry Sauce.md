# Katsu Curry with Black Curry Sauce

## for curry

* 2 tablespoons oil
* 1 pound pork butt cut into 1/2″ cubes
* 4 medium onions sliced thin
* 1 1/2 C red wine
* 2 1/2 C water or stock
* 2 carrots cut 1/2″ cubes
* 2 large yukon gold potatoes cut into 1/2″ cubes
* 2 tsp kosher salt (use less if you use regular salt)
* 1 Tbs tonkatsu sauce
* 1 Tbs tomato paste
* 1/4 cup apple puree
* 1 black cardamom pod
* 1 whole star anise pod
* 1 teaspoon Mayu (black garlic oil)
* 1/2 cup peas
* 2 ounces good quality bittersweet chocolate

## for roux

* 3 tablespoons butter
* 1/3 cup flour
* 2 tablespoons garam masala (or curry powder)
* 1-2 teaspoons freshly ground black pepper (depending on how spicy you want it)
* 1 batch of tonkatsu or grilled veggies

## Directions

* Heat the oil in a large chef’s pan over medium high heat. Salt and pepper the pork then add them to the pan in a single layer. Let them meat brown on one side, then use tongs or chopsticks to flip them over and brown the other side. Transfer the pork to a bowl and turn the heat down to medium low. Add the onions and cover with a lid for 10 minutes. Remove the lid and continue caramelizing the onions until they are dark brown and glossy (about 1 hour).
* Add the browned pork, wine, water, carrots potatoes, salt, tonkatsu sauce, tomatoe poast, apple puree, cardamom and mayu then bring to a boil over high heat. Turn the heat down to medium low and simmer partially covered until the carrots are tender (about 45 minutes).
* Meanwhile, make the roux by melting the butter over medium low heat. Add the flour cook while stirring until the mixtures turns a golden brown. Add the garam masala and black pepper and stir to incorporate (it will turn into a paste). Remove from the heat and set aside until the carrots are tender.
* Make the tonkatsu, or grill some vegetables to serve the curry sauce on.
* Finish the curry by ladling some of the liquid the meat and veggies have been cooking in into the roux and whisk until there are no lumps. Pour this mixture back into the other pot and gently stir until the curry is thickened. Taste for salt and adjust as needed. When you’re happy with it, add the peas and chocolate and stir until the chocolate is melted and incorporated.
* Slice the tonkatsu and plate with some cooked rice. Pour the sauce all over the tonkatsu and part of the rice.
