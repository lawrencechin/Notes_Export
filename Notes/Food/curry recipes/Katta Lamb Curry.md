# Katta Lamb Curry

![Katta lamb curry](./@imgs/recipes/katta_lamb_curry.jpeg)

## Ingredients

Serves 4

- 2tbsp mustard oil
- <sup>1/2</sup> a cinnamon stick
- 1<sup>1/2</sup>tsp cumin seeds
- 3 cloves
- 5 green cardamom pods, smashed
- 3 onions, finely chopped
- 2tbsp garlic and ginger paste
- 1tsp each of ground turmeric and garam masala
- <sup>1/2</sup>tsp Kashmiri chilli powder
- 500g lean lamb, cut into bite-sized pieces
- 3 heaped tsp amchoor (dried mango powder)
- 1<sup>1/2</sup>tsp fennel seeds, lightly crushed
- 2tsp dried fenugreek leaves
- 3 green chillies, cut lengthways
- Salt, to taste
- Chopped coriander, to garnish
- Cooked rice

## Method

1. Heat the mustard oil in a saupepan or wok over a medium-high heat until visibly hot.
1. Add the cinnamon, cumin seeds, cloves and cardamom pods. Remember that if you don't like biting into whole spices, count them in and then count them out before serving.
1. Temper the spices in the oil for about 30 seconds, then add the chopped onions and fry for about 5 minutes, or until soft, translucent and lightly browned.
1. Stir in the garlic and ginger paste and fry for a further minute, then add the turmeric, garam masala, chilli powder and lamb. Stir well to combine. 
1. Add about 300ml of water, cover the pan with a lid and simmer on a medium heat for about 1 hour, or until the meat is really tender.
1. Stir in the amchoor, fennel seeds, dried fenugreek and green chillies. Simmer until you are happy with the consistency.
1. Season with salt, garnish with coriander and serve with the rice.
