# Keralan Prawn Curry

![Keralan Prawn Curry](./@imgs/recipes/keralan_prawn_curry.jpeg)

## Ingredients

Serves 4

- 1tsp Kashmiri chilli powder
- <sup>1/2</sup>tsp ground turmeric
- 1tbsp ground coriander
- <sup>1/2</sup> freshly ground black pepper
- 2.5cm piece of ginger, peeled and finely minced
- 5 green chillies, sliced down the middle
- 3 kokums or 2tsp tamarind
- 500g medium raw prawns, shelled and cleaned
- 3 garlic cloves, minced
- 1-1<sup>1/2</sup>tbsp rapeseedd or coconut oil
- 1tsp black mustard seeds
- 1tsp cumin seeds
- 20 fresh or frozen curry leaves
- Salt

## Method

1. Pour 500ml of water in a saucepan and bring to the boil.
1. Stir in the Kashmiri chilli powder, turmeric, coriander, black pepper, ginger, green chillies and kokums or tamarind paste, and simmer of 15 minutes.
1. Add the prawns, cover with a lid and simmer for 5 minutes.
1. Uncover the pan and stir the garlic in well to combine.
1. Heat the oil in a small frying pan over a medium-high heat until visibility hot, then add the mustard seeds.
1. When they start to crackle (after about 30 seconds), add the cumin seeds and curry leaves.
1. Allow to infuse into the oil for about 30 seconds, then pour over the cooked prawns. Season the curry with salt and serve hot.
