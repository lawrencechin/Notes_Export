# Onion Bhajis

## Ingredients

Makes 20

- 3 large onions
- 1tsp fine sea salt
- 1tsp cumin seeds
- 2.5cm piece of ginger, peeled and julienned
- <sup>1/4</sup>tsp ground turmeric
- <sup>3/4</sup>tsp ground coriander
- 3tbsp finely chopped coriander
- 2 green bird's eye chillies, finely chopped
- <sup>1/4</sup>tsp Kashmiri chilli powder
- 100g chickpea flour
- 1<sup>1/2</sup>tbsp rapeseed oil

## Method

1. Peel and finely slice the onions. Cut each slice into 3cm pieces. 
1. Place the onions in a bowl and sprinkle with the salt, mixing very well with your hands. Allow to sit for at least 30 minutes or up to 3 hours.
1. When ready to form the bhajis, squeeze the onions with your hands to release the water into the bowl.
1. Add the remaining ingredients up to and including the Kashmiri chilli powder to the bowl and give everything a good stir.
1. Sift the chickpea flour over the onion mixture and mix well. There should be enough water released from the salted onions to form a batter.
