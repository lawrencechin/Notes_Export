# Fish Curry

![Fish Curry](./@imgs/recipes/fish_curry.jpeg)

## Ingredients

- 3 kokums or 2tsp tamarind paste
- 1<sup>1/2</sup> rapeseed or coconut oil
- <sup>1/2</sup>tsp fenugreek seeds
- 20 fresh or frozen curry leaves plus extra (*optional*)
- 2 red onions, finely chopped
- 7 garlic cloves, thinly sliced
- 2.5cm piece of ginger, peeled and minced
- 3 green bird's eye chillies, sliced lengthways
- 1tbsp Kashmiri chilli powder
- 1tbsp sweet paprika
- 1 generous tbsp ground coriander
- <sup>1/2</sup> ground turmeric
- 200g chopped tomatoes
- 500g cod, cut into bite-sized pieces
- Salt, to taste
- Chopped fresh coriander, to garnish

## Method

1. If using kokums, wash them and then soak in 400ml of water until needed.
1. Heat the oil in a large saucepan over medium-high heat until visibly hot. Add the fenugreek seeds and curry leaves and let these infuse into the oil for about 30 seconds.
1. Add the chopped red onions and fry for about 5 minutes until lightly browned.
1. Stir in the garlic, ginger and bird's eye chillies and fry for a further minute, then add the Kashmiri chilli powder, paprika, ground coriander and ground turmeric. Stir well.
1. Add the tomatoes, the kokums with the water they were soaked in or 2tsp tamarind paste, and 400ml of water. Bring to the boil and simmer for 5 minutes.
1. Add the cod and cook, covered with a lid, over a medium heat for 10-15 minutes until the fish is just cooked.
1. You can add a little more water if you prefer the sauce to be thinner. 
1. Stir in a handful more curry leaves, if using, then season with salt and garnish with coriander and serve.
