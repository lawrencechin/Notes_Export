# Blanche Vaughan potatoes cooked in milk

One method, which is a classic, is potatoes cooked in milk. A great way to enliven potatoes which have been stored since last year's harvest. The milk gives them a creamier texture and a little more body and richness.

Serves 4 as a side dish

* 500g firm waxy potatoes
* 500ml milk
* A few sprigs of thyme, leaves picked from the stalk
* Bay leaves
* Salt and pepper
* Nutmeg
* A few pieces of butter
* Peel the potatoes and slice them into pieces at least 1cm thick.

1. Put them into a saucepan and cover with the milk. Add the thyme and bay, salt and pepper. Simmer gently for about 15 minutes, being careful not to boil the milk, until the potatoes are just cooked through.
2. Heat the oven to 200°C.
3. Remove the bay leaves and lift the potatoes into an oven proof dish which fits them snugly so that there are a couple of layers.
4. Grate over a little nutmeg, season with a bit more salt and pepper, a few spoonfuls of the milk and put some pieces of butter on top.
5. Cook for another 10 minutes or so, or until the top has started to brown and the milk has soaked into the potatoes.

Excellent served with roast chicken.
