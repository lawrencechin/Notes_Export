# Pissaladière Pasta

## Ingredients

* 1 tablespoon olive oil, plus 1 tablespoon
* 1 tablespoon butter, plus 1 tablespoon
* 3 yellow onions, sliced into paper-thin half moons
* 4 anchovy fillets
* 3 cloves garlic, sliced
* 4 stems thyme
* 1 bay leaf
* 1 pound multigrain spaghetti (recommended: Barilla Plus)
* 1/4 cup Niçoise olives, finely chopped

## Directions

1. Bring a large pot of water to a boil, and salt it liberally.
2. Meanwhile, heat 1 tablespoon each olive oil and butter in a sauté pan over medium heat. Add the onions, anchovy, garlic, thyme, and bay leaf. Season lightly with salt, and cook over medium heat until the onions are caramelized, 15 to 20 minutes, stirring often. Remove the thyme stems and bay leaf and discard.
3. Meanwhile, cook the pasta until it is al dente. Reserve the pasta water.
4. Toss the pasta with the remaining butter and olive oil, and the onion mixture, and moisten with pasta water as needed. Top with the chopped olives, lemon zest, and some fresh thyme leaves.
