# Tagliatelle with tomatoes and prawns

## Blanche Vaughan's perfect tagliatelle with tomatoes, prawns, summer salad

Serves 4

* 1 packet of tagliatelle ( I used squid ink because I love the way it looked with the other colours, but of course plain is delicious, too)
* Olive oil 
* 500g of sustainable prawns
* 1 small shallot, diced
* 1 red chilli, seeds removed and diced
* 500g ripe tomatoes
* 1 tbsp fresh fennel (if you can't find flowers, use finely sliced bulb and fronds)
* 1 tbsp basil, chopped
* Good salt and pepper
* Good olive oil to finish

1. Heat the oil in a heavy bottomed pan and quickly fry the prawns so that they start to turn pink. Sprinkle with a little salt and remove from the heat into a bowl.
2. Using the same pan, heat some more oil and gently fry the shallot and chilli until it is soft.
3. Peel the tomatoes by pouring over boiling water and leaving them for about 30seconds before refreshing under cold water. The skins should then slip off. Break them open and pull out the core and seeds and chop roughly.
4. Add the tomatoes to the cooked shallot and season well. Then add the basil and fennel and prawns and cook together briefly.
5. Cook the pasta in plenty of boiling, salted water and when it is al dente, drain and add to the sauce. Stir together, season well and douse with some good olive oil.

## Mixed leaf salad with shaved white radish

This barely needs a recipe, just the suggestion that what makes it taste good, is a good combination of salad textures and flavours and to slice the radish really thin so that it is crisp but not a mouthful in the salad.

* 1 handful of rocket
* 1 handful of mustard leaves
* 1 handful of mache
* 1 head of fennel flowers
* 1 white radish, finely sliced
* A good squeeze of lemon
* Very good olive oil and salt and pepper

Put the leaves into a bowl and slice over the radish. When you are ready to serve, add the lemon and oil and season well.

