# Mary Berry's Hearty Lasagne

Original [link](https://www.mirror.co.uk/news/weird-news/mary-berrys-hearty-lasagne-speedy-31189516)

## Ingredients

- Butter
- 6 large dried lasagne sheets
- 75g mature cheddar cheese, grated

#### Pork and Spinach Sauce

- 450g sausage meat
- 1 tbsp oil
- 1 red chilli, finely chopped
- 2 garlic cloves (crushed)
- 250g chestnut mushrooms (sliced)
- 200 ml crème fraîche
- 1 tbsp plain flour
- 100g spinach

#### Tomato Sauce

- 1 tbsp sage
- 1 tbsp thyme
- 2 tbsp sun-dried tomato paste
- 1 tsp muscovado sugar
- 500g passata

## Method

1. First things first, you'll need to grease your ovenproof dish with butter and put your oven on to preheat to 200C/180C Fan/Gas 6. Then boil some water and let it cool very slightly before soaking your six lasagne sheets in it to help it soften - it should stay soaking while you make both of the sauces.
1. Heat some oil in your frying pan, then add your sausage meat until "every little bit [has turned into] a lovely golden brown colour," says Berry. This should take between 5-10 minutes. While you're browning the meat, break it up with two wooden spoons. Then, Berry advises, "Add two tablespoons of flour, two cloves of crushed garlic and one chopped chilli".
1. While Berry admits she supposes "it's a bit unusual to put chopped chilli in, my lot seem to like chilli in things more and more and more, so it's up to you whether you put it in or not." You should add your mushrooms at this stage too, and fry it all for about five minutes, before stirring in the crème fraîche and spinach.
1. Then bring it to a boil for a couple of minutes and season the pan with salt and pepper to your taste, before setting it aside. Then you move on to the tomato sauce: all you have to do is mix all your ingredients for this together in a jug and add salt and pepper for flavour.
1. Drain your soaking lasagne sheets and put about a third of your sausage and spinach sauce into the bottom of your greased ovenproof dish. Then "dribble informally" a third of the tomato sauce on top of this, and arrange three of your lasagne sheets over this, before repeating the process. At the end put your grated cheese at the very top, which "will all melt and become crispy on top".
1. Cook the final product for 20-30 minutes or "until the pasta is soft and the top is golden". Berry recommends it's "best served the moment it comes out of the oven" so tuck in!