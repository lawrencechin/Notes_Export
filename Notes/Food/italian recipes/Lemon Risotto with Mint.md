# Lemon Risotto with Mint
Serves 6

* 6 cups chicken stock
* 2 tablespoons unsalted butter, divided
* 1 tablespoon olive oil
* 1 small yellow onion or 2 spring onions, white and pale green parts only, finely chopped
* 2 cups arborio rice
* 1/2 cup dry white wine
* 1/2 cup grated Parmigiano-Reggiano cheese, plus extra for garnish
* 2 tablespoons freshly squeezed lemon juice
* 2 teaspoons lemon zest, plus extra for garnish
* 2 tablespoons chopped fresh mint leaves, plus extra for garnish

## Directions

* Bring stock to a simmer. Reduce heat to lowest setting and keep warm.
* Heat 1 tablespoon butter and oil in a deep skillet or pot over medium heat. 
* Add onion and sauté until softened, 2 minutes. 
* Add rice and stir to coat. 
* Add wine. Cook, stirring, until the wine evaporates. 
* Add stock 1 cup at a time, stirring until nearly all of the liquid is absorbed before adding the next cup. Continue until the rice is tender but not mushy. 
* Remove pan from the heat. Stir in 1 tablespoon butter, 1/2 cup parmesan cheese, lemon juice and lemon zest. 
* Add mint leaves and serve immediately in bowls garnished with extra cheese, lemon zest and mint.
