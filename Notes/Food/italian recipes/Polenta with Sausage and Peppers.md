# Polenta with Sausage and Peppers
February 22, 2008 Uncategorized Lindsey Johnson | Cafe Johnsonia

To say I love polenta is an understatement. I hadn’t even tried it until I was dating my husband. We ate it in cubes under homemade chilli.
When I want to treat my husband, I make him some. And I don’t fool around with that stuff you buy in the supermarket–the tube polenta. It’s just not the same.
Yes, there’s a little extra effort involved. And constant attention to the bubbling pot on the stove. Oh, and possibly a few burns. But, it’s worth the extra effort. Very worth it.

## Ingredients:
### For Polenta:
* 6 1/2 cups water
* 1 1/2 cups coarse, stone-ground cornmeal (I like Goya brand)
* 1 1/2 tsp. salt
* 2 Tbsp. butter
* 1/4 cup Parmesan cheese

### For Sausage and Peppers: 
* 1 to 1 1/2 lbs. Italian sausage (can use pork or chicken)
* 1 of each: red, yellow, green bell pepper, cut lengthwise into strips
* 1 large onion, cut lengthwise into strips
* 2 garlic cloves, thinly sliced
* 1, 28 oz. can diced tomatoes with juice
* 1 small can tomato sauce
* 2-3 Tbsp. olive oil
* salt and pepper to taste

## Instructions:
### For Polenta:
1. Prepare an 8″ by 11″ or 9″ by 13″ baking dish by either buttering or spraying with non-stick cooking spray.
Bring water to a hard boil in a large pot–I use a stockpot. Add salt.
2. While whisking, slowly pour cornmeal into the boiling water. Continue whisking until it comes to a rolling boil. Switch to a wooden spoon and stir continually. The mixture will slowly thicken to a porridge, but will be done when the bubbles in the center of the pot are very large and the polenta starts to come away from the sides of the pan. This takes about 20 to 30 minutes.
3. Add butter and Parmesan cheese and stir well. Pour into prepared pan and set aside until the polenta sets up. (You can serve it soft, which is also delicious.) Slice the polenta into squares and serve.
 
### For Sausage and Peppers: 
1. Remove casing from sausage. Heat a large skillet over medium-high heat. Break the sausage up as it cooks into large chunks–roughly the size of meatballs. Cook sausage until it is no longer pink and has started to brown. Drain off the rendered fat. Add the tomatoes and tomato sauce and set aside.
2. Spray a 3 quart baking dish with nonstick cooking spray. Add the peppers, onion, and garlic. Drizzle with olive oil and toss to coat. Place in a 400 degree oven for about 20 minutes, or until the veggies start to soften. Stir in the sausage-tomato sauce and place back in the oven for another 20-30 minutes.
3. Serve hot over slices of polenta. Serves 6.


### Notes about polenta:
1. Make sure you stir constantly. This will prevent lumps.
2. Most important–Be very, very careful while you stir. Those bubbles get pretty big and when they burst, the hot polenta can splash and really burn you. I have quite a few scars. Wear really good and long oven mitts to protect yourself.
3. Leftovers are great sliced and fried in a little butter or oil and drizzled with maple syrup.
