# [Baked Conchiglioni with Sausage, Sage & Butternut Squash](https://www.bbcgoodfood.com/recipes/baked-conchiglioni-sausage-sage-butternut-squash)

## Ingredients

* 1 small butternut squash, peeled, seeds discarded, flesh chopped into small cubes
* 2 tsp olive oil
* small pack sage, leaves picked and roughly chopped
* 8 good-quality sausages
* good grating nutmeg, about ¼ of a whole one
* 350g conchiglioni, see below
* 50g butter
* 50g plain flour
* 850ml milk
* 50g parmesan, grated, plus extra for sprinkling
* 100g fontina, grated, or 125g ball mozzarella, chopped
* drizzle of truffle oil

## Method
1. Place the squash in a microwavable bowl with 1 tbsp water. Cook in the microwave on High for 10 mins or until soft. Meanwhile, heat the oil in a frying pan, add the sage leaves and sizzle for 1 min. Remove the sausages from their skins and divide into small balls about the size of unshelled hazelnuts. Add to the pan and fry until browned. Add half the squash and set aside. Mash the remaining squash with any liquid from the bowl, the nutmeg and seasoning.
2. Heat oven to 220C/200C fan/gas 7. Cook the pasta following pack instructions. Meanwhile, melt the butter in a saucepan, then add the flour and stir to a paste. Cook for 1-2 mins, stirring the whole time, then gradually mix in the milk until you have a smooth sauce. Add the mashed squash, Parmesan and plenty of seasoning. Add 3-4 tbsp of the sauce to the sausage mix to moisten it a little.
3. Spread the remaining squash sauce over the base of a large, shallow baking dish. Drain the pasta, but keep a little liquid in the pan to stop the shells from sticking together. Fish any broken bits of pasta out of the pan and scatter these over the sauce. Line up the shells on top, pushing them down into the sauce so only the tops of the shells are poking out. Fill each shell with the sausage mixture. Scatter with fontina or mozzarella, grate over some extra Parmesan and drizzle with a little truffle oil, if you like. Bake for 30 mins or until golden brown.
