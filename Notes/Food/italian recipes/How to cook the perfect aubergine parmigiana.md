# How to cook the perfect aubergine parmigiana

Aubergine parmigiana (aka melanzane alla parmigiana, or parmesan aubergine, for the sake of linguistic consistency) is that rare and glorious thing: Mediterranean stodge. Despite the oft-repeated assumption that it is, as Jamie Oliver puts it, "a classic northern Italian recipe", parmigiana probably, as Jane Grigson observes in her masterful Vegetable Book, hails from the south, where it is popular fare in the rosticcerias, or roast-meat shops, of Naples and the surrounding area. True, it's almost certainly named for the northern cheese, but the aubergines, the tomatoes, and the mozzarella are all traditionally southern ingredients – perhaps the very fact the parmesan is singled out for mention suggests its exoticism.

Whatever the truth of it, however, this is a dish which works as well on a bleak British February evening as it does on a balmy Naples night in May, the bold flavours supplying a welcome taste of warmer climes, while the copious amounts of oil and cheese provide the real heating power.

## Aubergines

These days, as we all know, there's no real need to salt aubergines, because the bitterness has long been bred out of the poor things. Nevertheless, almost every recipe I come across, bar Oliver's, call for it – mostly for a period of about an hour, although Soho's Mele e Pere trattoria goes for a more moderate 30 minutes.

As expected, I don't find any significant difference in the amount of oil they soak up, but I do think salting improves the flavour: the slightly saline taste the aubergines retain, even after rinsing, improves the dish immeasurably. Half an hour is quite sufficient for this.
Mele e Pere peel the aubergines before use, which I don't approve of: the skins provide a welcome textural contrast in a dish that can otherwise tend towards the sloppy. (They're also, I'd observe, surprisingly difficult to remove.)

Cutting them lengthways, as the Silver Spoon and Anna del Conte's Gastronomy of Italy suggest, makes it much easier to line the baking dish, and gives the whole thing a more coherent structure: it should stand up on the plate, rather than collapsing in a heap on serving.

## Cooking

Traditionally, the aubergines are fried in copious amounts of oil before layering. This makes them meltingly soft, and gives the dish a quite outrageous richness – oil actually spills out of Anna del Conte's parmigiana as I cut into it (which does not, I must admit, lose it many fans). Grigson suggests countering this by frying only half to a third of the slices, and blanching the remainder in boiling water. Although I'm initially sceptical, once baked, the dish is as flavourful as ever, without being queasily greasy.
Jamie Oliver chargrills his aubergine slices on a smoking hot griddle pan, which looks very pretty, but gives a disappointingly dry result. Better is Ed Schneider's method, in which he drizzles the slices with oil and bakes them until softer. (His reasoning will chime with anyone who's ever fried an aubergine: "This way, you don't need to stand over a frying pan worrying about how much oil the eggplant is soaking up and surreptitiously adding more as it disappears.") I still prefer the fried and blanched combination, however – the slices may be oilier, but they're distinctly more tender than the baked variety.
Grigson and Angela Hartnett both caution against browning the aubergines, while the Silver Spoon, Mele e Pere and Conte insist on a golden brown colour. I prefer the slightly caramelised flavour of the latter – they look more attractive too.

## Breading
￼
The aubergine slices are often floured or breaded before frying. Mele e Pere flour them, so they crisp up beautifully in the pan but as Schneider observes, this gives the finished dish a slightly "gummy" texture. Although he admits he prefers them au naturel, he gives a "nice variation" in which he bakes the slices with a light coating of breadcrumbs: "because they were thoroughly toasted and because they did not form a dense layer that would be prone to sogginess, the sparse crumbs stayed crunchy, which was fun". The additional texture is popular with my testers, but we all agree we'd prefer the breadcrumbs on top, as Oliver suggests, where they remain properly crisp.

## Sauce

Ideally, of course, it would be midsummer, and the markets would be bursting with fresh, robustly flavoured tomatoes crying out to be made into a sauce, but it's not, and the one fresh tomato sauce I attempt, from the Silver Spoon recipe, requires an awful lot of reducing to achieve the same intensity of flavour as those using the tinned variety. The price of half-decent tomatoes at this time of year rules that one out.

The other sauces range from a pared-down passata, oregano and seasoning number from Mele e Pere which is surprisingly successful for something so quick, to a slow-simmered, wine-spiked sauce studded with carrots, onion and garlic from Grigson. I love the richness the wine brings (Oliver's wine vinegar effort seems thin in comparison), but I'm not sure this particular dish demands carrots or onions, so I'm going to streamline her sauce to make it more like Anna del Conte's simpler version, but with added wine, and then puree it to give the aubergines a smooth coating.
Basil and oregano are the herbs of choice here. There are some objections from the testers that dried oregano makes the Mele e Pere parmigiana taste too much like a "1980s pizza" but I like it – basil, I think, works better torn on to the dish at the last minute, rather than added to the sauce, where it has a tendency to turn slimy and brown.
I notice many of the recipes are very light on sauce – this should be quite a solid dish, but I find the Silver Spoon version in particular too dense; the only liquid is the oil in the aubergines. It may not be authentic, but I'm going to be a little more generous with mine.

## Cheese and other things
￼
Parmesan is, as Grigson says, "the soul" of this dish, so there's no dispute there. Oliver even goes so far as to leave out the mozzarella altogether, but I think that's a shame: its milky blandness works brilliantly with the other flavours, and pulling an elastic string of the stuff out of the dish is a joy that never grows old. There's no point in using good buffalo mozzarella here though: it's too wet, and doesn't melt quite the same way. I'm with Mele e Pere: the firm cheese used as a pizza topping works much better.

Del Conte layers slices of hard-boiled egg along with the mozzarella, which is apparently quite common in Italy. As a foil to the other flavours, it works well, but breadcrumbs, we decide, would serve the same purpose, and actually soak them up much better. The Silver Spoon, meanwhile, adds beaten egg to each layer. This is, frankly, bizarre, giving the dish the dense firmness of a frittata, while the combination of egg and aubergine puts us all in mind of moussaka. This particular parmigiana will be an egg-free zone.

One last thing: some of my testers would disagree, but the key to this dish, as Ed Schneider informs me "as to many other Italian things, is not to serve it hot, though it's hard to wait sometimes". I'm not mad keen on it cold, as the Silver Spoon suggests, but it's definitely better warm than piping hot. Even in this climate.

## Aubergine parmigiana

* Serves 4-6 (main course/side dish)
* 1.5kg aubergines
* Fine salt
* 2 tbsp olive oil
* 3 cloves of garlic, crushed
* 800g good tinned tomatoes
* 150ml red wine
* Pinch of sugar
* 1/2 tsp dried oregano
* Oil, to fry
* 200g mozzarella, thinly sliced
* 125g Parmesan, grated
* 50g breadcrumbs
* Handful of basil leaves

1. Cut the aubergines lengthways into 5mm slices, sprinkle with salt and leave in a colander to drain for half an hour.
2. Meanwhile, heat the olive oil in a medium pan over a medium-high heat and add the garlic. Fry for a minute, then tip in the tomatoes and wine. Bring to the boil, mashing the tomatoes, stir and then turn down the heat slightly. Add a pinch of sugar, a little seasoning and the oregano, and simmer gently for 45 minutes, stirring occasionally. Puree until smooth.
3. Preheat the oven to 180C. Put a large pan of water on to boil if you're feeling healthy. Rinse the aubergines well, and dry thoroughly with kitchen paper. Pour enough oil into a frying pan to coat the bottom well, and put on a high heat. Fry half (healthy) or all (not), of the aubergine slices until golden brown on both sides, working in batches. Put the cooked slices on paper towel to drain. Blanch the other half, if necessary, in the boiling water for 2 minutes, then drain well.
4. Lightly grease a baking dish and spread with a thin layer of tomato sauce, followed by a layer of aubergines (packing them tightly), mozzarella, Parmesan and seasoning.
5. Add another layer of aubergines, followed by tomato sauce, the cheeses and seasoning. Repeat this order until you have used up all the aubergine, finishing with a layer of sauce (you may not need all the sauce) – keep a little Parmesan back for the top.
6. Toss the breadcrumbs with a little olive oil and Parmesan and sprinkle on top. Bake for about 30 minutes, until bubbling and browned, then allow to cool slightly and sprinkle with torn basil before serving.

Aubergine parmigiana: the perfect warming dish for those is it?/isn't it? spring days, or one best left until aubergines and tomatoes are at their summery peak? Warm or cold, fried or baked – how do you like yours, and what other examples are there of sunny dishes that work well in not-so-sunny climes?