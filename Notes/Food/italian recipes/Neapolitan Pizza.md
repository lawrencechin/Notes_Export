# Neapolitan Pizza 

![Neapolitan Pizza](./@imgs/recipes/nea_pizza.jpg)

By Ciro Salvo of 50 Kalò | [Link](http://www.bbc.com/travel/story/20200415-how-to-make-pizza-like-a-neapolitan-master)

## Dough recipe
### Ingredients

- 450g flour (ideally 00, but can use 0 or 1)
- 300ml cold tap water
- 3g fresh brewer's yeast
- 9g salt

### Method

1. In a large bowl, dissolve the yeast in the cold tap water, and then mix in about two-thirds of the flour with a big spoon until a creamy consistency is formed. Mix in the salt, and then the remaining flour a little bit at a time. 1. Continue to mix until all the flour has been absorbed.
1. Knead energetically with your hands by folding the dough and pushing it inwards. When the dough is smooth and no longer has lumps, let it rest for 10-15 minutes.
1. Dust the dough with flour, place it on a table and then give it a few folds, forming it into a spherical shape until it’s firm and elastic.
1. Place dough inside an oiled baking tin and cover, and then allow it to rest and rise for 7-8 hours at room temperature.
1. Place dough onto a round sheet pan that’s been lightly greased with olive oil, and lightly press it with your fingertips until the classical, flat-and-round pizza shape is formed (or into a square if using a square-shaped pan). It should be no more than 3mm thick. Cover and let rest for another 3 hours.
1. Add tomatoes (or tomato puree) as desired onto the flattened dough, and put pizza on the base of the oven, baking at 250-280°C for 5-6 minutes. Move pizza to the top rack of the oven and bake for another 6-8 minutes, adding drained mozzarella (if using) only in the last 3-4 minutes. 1. Other ingredients such as extra virgin olive oil and basil should be added at the end of cooking.


## Pizza marinara recipe 

### Ingredients

- pizza dough (see recipe above)
- 50g Corbara tomatoes, drained
- 70g escarole, blanched
- 30g Caiazzo black olives, pitted
- pinch of Salina capers
- garlic clove, thinly sliced
- pinch of oregano
- extra virgin olive oil

### Method

1. Place already-prepared dough onto a baking sheet and lightly press flat. It should be no more than 3mm thick.
1. Top with tomatoes and bake at 250-280°C for 5-6 minutes. 1. Add escarole, black olives, capers, garlic and bake in the oven at 250-300°C for another 6-8 minutes. Add oregano and a trickle of olive oil.


(Credit: Ciro Salvo, adapted for BBC Travel)
