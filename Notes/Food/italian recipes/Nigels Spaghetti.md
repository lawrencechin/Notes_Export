# Nigel’s Spaghetti

## Ingredients
* 50g/1¾ oz butter
* 70g/2½ oz pancetta
* 1 onion, finely chopped
* 2 cloves garlic, finely chopped
* 1 carrot, finely chopped
* 2 celery stalks, finely chopped
* 2 large mushrooms, finely chopped
* 2 bay leaves
* 400g/14oz minced beef or lamb
* 200ml/7fl oz whole milk
* 220ml/7¾fl oz crushed tomatoes or passata
* 200ml/7fl oz red wine
* 200ml/7fl oz vegetable stock
* pinch freshly ground nutmeg
* spaghetti or tagliatelle, cooked according to packet instructions
* grated parmesan cheese, to serve

## Preparation method
1. Melt the butter in a heavy-based pan, then stir in the pancetta and let it cook for 4-5 minutes, without colouring much. Add the onion, garlic, carrot, celery, mushrooms and bay leaves, stirring well. Leave to cook for 8-10 minutes over a medium heat, stirring frequently, until the vegetables are tender.
2. Turn up the heat and tip in the minced beef or lamb, breaking it up well with a wooden spoon. Leave to cook without stirring for 3-4 minutes, or until the bottom is browned, then stir again and cook for a further 3-4 minutes, or until golden-brown all over and cooked through.
3. Pour in the milk a bit at a time, then mix in the tomatoes, red wine, vegetable stock, nutmeg and season with salt and freshly ground black pepper freshly ground black pepper. Bring to the boil, then reduce the heat to a gentle simmer. Partially cover the pan with a lid and leave to simmer for 1 hour 30 minutes, stirring occasionally.
4. To serve, season to taste with salt and freshly ground black pepper, then stir in the cooked pasta and pile onto serving plates.
5. Sprinkle over the grated parmesan and serve.
