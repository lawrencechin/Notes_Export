# Italian Rice Recipe

Serves 8-10

### Crust
* 1 1/2 cups all-purpose flour
* 1/4 cup sugar
* 1/2 teaspoon salt
* 1/2 teaspoon baking powder
* 3/4 stick unsalted butter (chilled)
* 1 extra large egg or 2 small eggs
* 1 to 2 tablespoons ice water, or as much as needed

1.	Combine flour, sugar, salt and baking powder in the work bowl of a food processor fitted with a metal blade. Pulse several times to combine. Add the butter and pulse about 10 times until the dough becomes pebbly in texture. Add the eggs and pulse repeatedly until the dough begins to stick together. Slowly add the ice water by the tablespoonful, while using a few long pulses. Add more drops of ice water as necessary, until the dough holds together well. Invert the dough onto a floured work surface. Form the dough into a ball, flatten into a disc, wrap in plastic wrap, and refrigerate while preparing the filling. (Dough can be refrigerated for up to 2 days before continuing.)
2.	If you don't have a processor, then combine the dry ingredients in a bowl. Add chunks of chilled butter, and using a pastry blender or two forks, chop the butter until it resembles little pebbles. At this point, add the eggs and ice water, and stir with a spoon until the dough begins to form. Using your hands and working the dough as little as possible, transfer it to a lightly floured surface. Knead until the dough holds together. Form the dough into a ball, flatten into a disc, wrap in plastic and refrigerate while preparing the filling. (Dough can be refrigerated for up to 2 days before continuing.)

### Rice Filling

* 1/2 cup uncooked Arborio rice
* 4 cups of water OR whole milk
* 7 large eggs
* 1 cup sugar
* 2 teaspoons lemon extract (or the zest and juice of 1 small lemon, preferably Meyer)
* 2 teaspoons pure vanilla extract
* 1 pound ricotta cheese, drained (minimum of 2 hours or preferably overnight)

1.	Place the rice and water OR whole milk in medium heavy-bottom saucepan and bring to a boil. Reduce the heat to low and cook the rice, uncovered, stirring occasionally for about 15 to 20 minutes, or until the water is absorbed and the rice is sticky. The rice should still be firm as it will finish cooking in the oven. Remove from heat and set aside.

2. Add the eggs and sugar to a large bowl. Using a hand-mixer, beat until well combined. Add the lemon extract (or zest and juice) and vanilla, and beat on low for about 10 seconds. Add the drained ricotta, and beat on low for a few seconds until just combined. Stir in the cooked rice. Mix with a rubber spatula until well combined, making sure there are no clumps of rice. Place in the refrigerator.
3.	Place a rack in the lower third of the oven and preheat to 375 degrees. Coat a 10 1/2-inch pie plate with cooking spray. Turn out the dough onto a lightly floured surface and roll into an 11 1/2-inch circle. Transfer the dough to the prepared pie plate, gently pressing it into the bottom and sides. No fluted crust is necessary. At this point, set the crust in the freezer for about 10 to 15 minutes to get it really chilled, which will make for a flakier crust.
4. Remove the chilled crust from the freezer and pour the filling to about 1/4 of an inch below the top of the crust, as it will puff up slightly when baking. Bake for 1 hour or until the filling puffs up, turns golden, and is "set," meaning it should be firm, not jiggly when you gently move the pie plate. Remove from the oven and let cool on a rack. Serve at room temperature or chilled.
5.	Note: If you have some extra filling left over, you can pour it into a small baking dish or ramekins for a crustless version, and follow the same baking instructions. Leftover rice pie can be stored in an air-tight container in the refrigerator for 3 to 4 days.
