# Frying-Pan Pizza Recipe

Felicity Cloake | [Link](https://www.theguardian.com/food/2019/sep/11/how-to-make-perfect-pan-fried-pizza-felicity-cloake-recipe)

![Pizza on a chopping board](./@imgs/recipes/Fried_Pizza_1.jpg)

Rise | Prep | Cook | Makes
-- | -- | -- | --
3-5 days | 5 mins | 35 mins | 4  

## Ingredients

* 500g Italian 00 flour
* 1<sup>1/2</sup> tsp dried active yeast
* 1 tsp fine salt
* 1 tsp sugar (*optional*)
* 1 400g tin chopped plum tomatoes
* 1 ball buffalo mozzarella 
* 1 bunch basil or a sprinkle of dried oregano
* Extra-virgin olive oil, to drizzle

Mix the flour, yeast, salt and sugar then stir in 325ml water to a smooth dough. Cover and put in the fridge for three to five days.

Tip on to a lightly floured surface. Divide into four roughly equal portions, shape into balls, cover and leave for two hours. Get the cheese out of the fridge.

![Mixing bowl with dough mix](./@imgs/recipes/Fried_Pizza_2.jpg)

Put the tomatoes in a saucepan over a medium heat and simmer for about 30 minutes, until you have a thick sauce. Season to taste with salt and a pinch of sugar, if necessary.

![Dough balls on tray](./@imgs/recipes/Fried_Pizza_3.jpg)

Heat a frying pan or skillet (preferably cast-iron) over a medium-high flame until an experimental drop of water dances across the surface. Heat the grill to medium-high.

Stretch the dough with lightly floured hands into a rough round, keeping the edges thicker. Cook on the skillet until the base has begun to char, adding a spoonful of tomato sauce and some torn mozzarella once the top starts to dry out.

![Pan Pizza](./@imgs/recipes/Fried_Pizza_4.jpg)

Once the bottom looks done, grill until the cheese is bubbling and the edges are brown. Finish with a few leaves of basil or a sprinkle of dried oregano, and a drizzle of extra-virgin olive oil.
