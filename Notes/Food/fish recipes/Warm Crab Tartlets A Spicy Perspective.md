# Warm Crab Tartlets - A Spicy Perspective

**YIELD**: 10 servings
**COOK TIME**: 20 minutes

## Ingredients:

* 1 ¾ cup flour
* ½ salt salt
* ¾ cup unsalted butter, cold and cut into cubes
* 4-6 Tbs. ice cold water
* 2/3 cup heavy cream
* 2 eggs
* 8 oz. cooked, drained crabmeat
* 1 ½ Tb. fresh chopped Tarragon
* 1 small shallot, grated
* ¼ tsp. salt
* ¼ tsp. ground nutmeg
* ¼ tsp. cayenne pepper

## Directions:

* For the crust: Combine the flour and salt in a food processor. Pulse a few times.
* Add the cold butter and pulse until the bits of butter are the size of peas. Add the cold water 1 tablespoon at a time, pulsing in between, until the dough comes together. It will be a bit lumpy.
* Shape the dough into a rectangle, then wrap it in plastic and refrigerate for at least 30 minutes.
* Preheat the oven to 400 degrees F. Place the dough on a floured work surface and roll out to a 8 X 20 inch rectangle.
* Trim the rough edges. Then cut down the center lengthwise. Cut each strip into 5 pieces creating 10- 4 inch squares of dough.Fold each square and place them into muffin tins. Press the centers down to the bottom and press the folds into the sides. Prick the bottoms of the shells several times with a fork.
* Bake the shells for 15 minutes. Then cool a little.
* In the meantime, combine the cream and eggs in a medium sized bowl. Whisk until smooth. Then stir in the rest of the ingredients.
* Scoop the crab mixture into each shell evenly.
