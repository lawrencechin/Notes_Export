# [Souffléd Crab & Asparagus Tart](https://www.bbcgoodfood.com/recipes/472633/souffld-crab-and-asparagus-tart)

## Ingredients

* 300g ready-made shortcrust pastry
* 25g butter
* 25g plain flour
* 300ml milk
* freshly grated nutmeg
* 2 good pinches chilli powder
* 200g white crabmeat, fresh or frozen and defrosted
* 140g asparagus tips
* 3 eggs, separated
* 3 tbsp finely grated Parmesan

## Method

1. Heat oven to 200C/180C fan/gas 6. Roll out pastry and line a 25cm tart tin. Line the case with greaseproof paper and fill with baking beans. Bake for 15 mins, then remove paper and beans and cook for 5 mins more. Trim off excess pastry. Reduce oven to 190C/170C fan/gas 5.
1. Melt the butter in a non-stick pan, stir in the flour and cook for 1 min. Gradually add the milk, stirring all the time until the sauce is smooth and glossy. Add nutmeg, chilli and salt and leave to cool slightly.
1. Flake up the crabmeat and add to the sauce. Blanch the asparagus for 3 mins, drain well and cool under cold running water. Pat dry with kitchen paper and scatter over the pastry case.
1. Stir the egg yolks into the sauce. Whisk the whites until stiff, then fold into the sauce in 3 batches. Spoon into the tart case and sprinkle with Parmesan. Bake for 30-35 mins until puffed and golden.
