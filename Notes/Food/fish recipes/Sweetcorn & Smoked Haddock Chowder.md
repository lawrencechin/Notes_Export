# [Sweetcorn & Smoked Haddock Chowder](https://www.bbcgoodfood.com/recipes/7417/sweetcorn-and-smoked-haddock-chowder)

## Ingredients

* knob of butter
* 2 rashers of streaky bacon, copped
* 1 onion, finely chopped
* 500ml milk
* 350g potato (about 2 medium) cut into small cubes
* 300g frozen smoked haddock fillets (about 2)
* 140g frozen sweetcorn
* chopped parsley, to serve

## Method

1. Heat the butter in a large saucepan. Tip in the bacon, then cook until starting to brown. Add the onion, cook until soft, then pour over the milk and stir through the potatoes. Bring to the boil, then simmer for 5 mins.
2. Add the haddock, then leave to gently cook for another 10 mins. By now the fish should have defrosted so you can break it into large chunks. Stir through the sweetcorn, then cook for another few mins until the fish is cooked through and the sweetcorn has defrosted. Scatter over parsley, if using. Serve with plenty of crusty bread.
