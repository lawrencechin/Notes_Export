# Halibut with Creamed Peas, Bacon and Scallions

The largest of the coldwater flatfish, weighing up to an admittedly rare 300kg, halibut has a firm, almost meaty flesh. It can have a tendency to dry out if cooked on too high a heat or for too long, but gently heated for no more than 8 to 9 minutes, as here, it makes for a superior dish. Plus, it is fabulous for fish and chips.

![Halibut with creamed peas](../@imgs/recipes/halibut.jpg)

Serves 4

## Ingredients

- Extra virgin olive oil (for frying)
- 4 x 180g halibut fillets (skinned)
- Salt and freshly ground
- 1 lemon

### For the Creamed Peas

- 200g Peas (shelled or frozen)
- 60g Unsalted butter
- 1 small Shallot (finely chopped)
- 50g smoked bacon (chopped into ½cm dice)
- 50ml vegetable stock
- 120ml double cream
- Salt and freshly ground black pepper
- ½ bunch, finely sliced at an angle scallions (spring onions)
- Pea shoots or little gem lettuce (handful, roughly shredded)

## Method

1. Bring a saucepan of salted water to the boil, add the peas and 20g butter and cook for 5 to 6 minutes until tender (they may take more or less time, depending on size and freshness). Drain in a colander.
1. Meanwhile, in a larger saucepan, melt 20g butter and gently soften the shallot with the bacon, without allowing them to colour. Add the vegetable stock (see page 70, but a good quality cube is fine) and simmer until reduced by three quarters.
1. Add the cream, season with freshly ground black pepper and simmer until reduced by half. Add peas, scallions and pea shoots or lettuce and continue to simmer until the sauce is just thick enough to coat the peas. Add the remaining butter and season if necessary. Set aside and keep warm.
1. Meanwhile, heat the olive oil in a non-stick frying pan. Season the halibut and gently cook for 5 minutes. Turn over and cook for a further 4 minutes.
1. To serve, arrange the halibut on a dish, drizzle with olive oil and garnish with lemon wedges. Place the creamed peas, bacon and scallions into a separate dish and serve with the fish, or you can serve individually with the halibut resting on a bed of the peas.
