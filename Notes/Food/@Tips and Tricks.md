# Tips and Tricks
## Fry pizza, add Marmite to roast potatoes: 27 eye-opening and invaluable tips from top chefs

Whether you want to thicken a stew, tenderise meat or get your scallops squeaky-clean, the pros have a trick for it

Stuart Heritage | Mon 13 Feb 2023 10.00 GMT

If you have ever tried to re-create a restaurant dish at home, you will almost certainly have been perplexed by the experience. No matter how much care and attention you give to your version, something seems to be missing when you tuck in. What are chefs doing differently? Here, 27 professionals divulge their favourite kitchen trick – the one unexpected riff that keeps them ahead of the pack.

### Potatoes

*Simon Shaw, chef patron and creative director at El Gato Negro, Canto and Habas, Manchester*

"A little tip for roast potatoes is to put a bit of Marmite in the water when you’re parboiling the potatoes. It gives them a really nice colour and will present you with deep-golden roasties every time."

*Giovann Attard, executive head chef at Norma, London*

"Adding white wine vinegar to water while boiling potatoes keeps them firm on the outside and soft on the inside. The acid in the vinegar prevents the pectin from breaking down too quickly in the outer part of the potato and it will remain compact and firm. This is great for making potato salad – or anything where you want to mix the potato with other ingredients."

### Sauces

*Lisa Marley, chef trainer for ProVeg UK*

"To thicken curries, soups and stews, add in chopped, boiled sweet potato. It dissolves into the dish, adds sweetness and creates a thick and tasty sauce."

*Jeremy Pang, chef and founder of School of Wok*

"I hate to waste food and I love this little trick to elevate your Chinese or any south-east Asian cuisine: using vegetable skins or ends to flavour your oils. Garlic skin, spring onion ends (washed and dried on a tea towel), clean onion skins and ginger peel work a treat if cooked low and slow (for 30 minutes or so) in a pool of vegetable oil. Then use it to cook stir-fries or anything else. It adds a real depth of flavour, but your guests will have no idea where the deliciousness came from." 

*Mike Reid, executive chef at M Restaurants, London and Gaucho, nationwide*

"There’s an old trick called velveting that works for tenderising cheaper cuts of meat. Mix a teaspoon of baking powder in 250ml water. Pour over your meat (up to about 280g for these measurements) and leave it to marinade for no longer than 15 minutes. Then wash it off and pat dry. This process raises the pH level on the meat’s surface, which will keep it tender and soft when you cook it." 

*Judy Joo, founder of Seoul Bird, London*

"You can do a lot with soy sauce. Brush a whole chicken with it before roasting, to add flavour and help crisp up the skin with gorgeous colour. I also like to add it to gravy, for extra umami flavour." 

*Ahmed Abdalla, head chef at Legacy, York*

"An older chef, some time ago, urged me to add a dollop of mayonnaise to a hollandaise sauce. This helps the hollandaise keep its consistency for many hours and stops it from splitting. It also allows the sauce to hold its creamy nature during reheating." 

*Sohan Bhandari, head chef at Colonel Saab, London*

"Spoon out excess fat from stocks, stews, and sauces by skimming a few ice cubes (wrapped in a paper towel or cheese cloth) along the surface of the liquid. The ice helps the fat solidify, making it easier to remove with a spoon (or even a piece of toast)." 

### Meat & Fish

*Kerth Gumbs, head chef at Fenchurch restaurant, London*

"For juicy, tender meat, blend fresh celery and add it to your marinades. This helps to tenderise the meat before cooking, giving you the perfect flavour." 

*Sam Idione, head chef, Roots + Seeds Kitchen Garden, Cirencester Park*

"If you’re cooking burgers on the barbecue or on a grill in your kitchen, push down on the centre of your patty to form a sort of doughnut. This little trick will allow more airflow around the burger, allowing it to cook evenly without the centre swelling up." 

*Tom De Keyser, head chef at the Hand & Flowers, Marlow*

"If cooking for two, the easiest thing to do is to buy a steak big enough for both of you, rather than two separate steaks. Sear it in the pan, then put it into a pre-heated oven at 200C (gas mark 6) and immediately turn the oven off. This will cook and rest the bigger cut at the same time, giving you time to pop out for a drink. My timings for each steak are as follows if you live a couple of minutes’ walk from the pub: a generous fillet is time for one pint, a generous sirloin is time for two pints, and a generous côte de boeuf is time for three pints. You will return to a beautifully cooked steak." 

*Adebola Adeshina, chef patron at Chubby Castor, Cambridgeshire*

"Rinse scallops in sparkling water instead of still, as the bubbles will remove any hidden grit." 

### Vegetables

*Matt Fletcher, chef de cuisine at Great Central Pub by Matt Fletcher at the Landmark London Hotel*

"When preparing sturdier green vegetables, such as green beans or asparagus, they are ordinarily very dry. I soak them in cold water for at least one hour before cooking in boiling, heavily salted water. Once al dente, I drop them into iced water. The whole process really brings out the colour and ensures a lush, green, crisp vegetable – ideal for adding to salads." 

*Kevin Dalgleish, chef patron at Amuse, Aberdeen*

"At the restaurant, we make an emulsion that is perfect for glazing or seasoning blanched vegetables. In a pan, whisk together 20ml of cream with a chicken (or veg) stock cube and bring to a simmer. Add in your vegetables of choice, season with salt, pepper and sugar, and reduce until the veg is glazed. It works perfectly with spinach, cavolo nero and carrots, giving them more of a rich and meaty flavour." 

*Sam Grainger, owner and chef at Belzan and Madre, Liverpool*

"Don’t cook vegetables in plain water. Instead, treat it as a cooking step to increase flavour. Cook any greens in a seaweed broth (use shimaya konbu stock). Always cook carrots in carrot or orange juice where possible, because it really heightens the flavour and brings out the sweetness." 

*Kirk Westaway, executive chef at Jaan, Singapore*

"Try cooking vegetables whole. Roast a whole carrot, barbecue a whole leek, bake a whole beetroot, blanch a whole courgette, simmer whole cabbages. I always do this in the restaurant and at home because you get the maximum flavour out of your ingredient instead of washing the taste and nutrients out into the boiling water and then down the sink. Once cooked, you can then trim and portion." 

*Aktar Islam, chef patron at Opheem, Birmingham*

"I always cook greens towards the end of a roast dinner. I love to sauté them in a pan with leftover chicken-fat reduction. It’s a great byproduct and makes them super-tasty. Simply collect what comes off the tray from your roast chicken and pass through a strainer. Reduce it down to a stock and it’s ready for your greens." 

### Herbs & Spices

*Michael Carr, executive chef at the Old Vol and the Reindeer, Nottingham*

"Here’s how to refresh dying or drying herbs: put ice-cold water and potato trimmings in a cup or glass, then add bunches of coriander, parsley or tarragon that are past their best. They come back to life within 30 minutes of standing in the starchy water." 

*Nidhi Verma, chef at the Cookaway*

"I recommend keeping small quantities of dry powdered spices in jars for immediate use, with the rest stored in ziplock bags, which keep them fresher for longer. That’s how I store my pomegranate powder that I bring back from Punjab." 

*Daniel Heffy, chef at Nord, Liverpool*

"Roasted garlic oil is a fantastic base for all cooking. I roast peeled garlic with oil at about 140C until golden brown, which takes about an hour. Once it has cooled, you can separate the oil from the garlic, make a paste from the garlic and put the oil in a bottle. These will keep in the fridge and add flavour to pretty much anything you are cooking." 

### Rice

*Amy Poon, founder of Poon’s, London*

"People are always asking me for rice to water ratios. I seldom measure anything so I can’t tell you in cups, but here is a simple trick for measuring the right amount. Stick your index finger perpendicular to the surface of the rice in the pan and add water until it reaches the first joint of your finger. No idea why it works given different sizes of hands and pans, but it does." 

*Manuel Prota, head chef at Jacuzzi, London*

"Toast the rice when making risotto. The key is to add the rice to the pan first, without any oil or butter as this could burn it. You can tell when it’s done when the rice is dry and hot to touch. Doing this ensures the grain is sealed, and doesn’t become mushy as you add the stock." 

### Pizza

*Vittorio Meli, head chef at Zoom East, London*

"To make your homemade pizza equal to that of Italian pizzerias, try double cooking: first a few minutes in a large frying pan and then in the oven at a very high temperature. This allows the pizza to brown evenly on the surface." 

*Paul Foster, chef patron at Salt, Stratford-upon-Avon*

"If you’re making pizza, stretch out your sourdough as normal, then put two or three ice cubes on it before you place it in a pizza oven. The ice melts down to a starchy sauce. Pull it out, add cheese, pop back in and you get this lovely gooey cheesy sauce on the top. It’s quite a traditional technique – the end result is a little like cacio e pepe (cheese and pepper pasta) – but not a lot of people have heard of it and it’s very rarely done." 

*Dipna Anand, chef patron at Dipna at Somerset House, London*

"When I make chana masala, I create the distinctive colour and flavour by adding a teabag to the curry. Chickpeas tend to dry out when you cook them and a teabag also helps to retain moisture. Just be careful to take it out before serving!" 

### Desserts

*Adeline Vining, baker at Heirloom Cafe, Edlesborough*

"A lot of cake recipes tell you to swap some of the flour for cocoa powder if you want to make a chocolate version, but that makes for a dry cake. I always recommend using cooled hot chocolate as well to keep it moist. Pour two or three tablespoons over each layer and wait until it has soaked in before frosting."

*Jude Sam, head chef at Apothecary East, London*

"I always add a pinch of salt when I create a sorbet. It really helps bring out the flavour of the fruit and enhances acidity." 

## Marmite, 7Up, smoked butter … chefs on how to improve your cooking for £5

Looking for a cheap way to perk up what you eat? Top chefs from Yotam Ottolenghi to Tom Kerridge reveal what they would buy for under a fiver  

Tony Naylor | Wed 5 Feb 2020 13.38 GMT

It is cold. It is dark. It is winter. And post-Christmas, we all feel skint. How do you lift spirits and put some sizzle in your cooking without spending much? We asked some of Britain's best chefs: if you had a fiver or less to spend – what would you buy??

### Kikkoman dark soy sauce
*Stephen Harris, chef-owner, the Sportsman, Seasalter, Kent*

The Dutch East India company listed soy sauce in 1737. It has a long history in Europe, but I don't think British people understand how powerful it is. It's liquid umami for a couple of quid and, used judiciously, it's amazing in bolognese, chilli, stews and marinades. If you're frying mushrooms and want a real flavour boost, bung a tablespoon in and reduce it down.

### Chilli paste 
*Yotam Ottolenghi, Ottolenghi restaurants, London*

Hat tip on this to Tara Wigley, who I write with. Buy as many green and red chillies as you can, finely slice and salt them (one tablespoon per 250g). Refrigerate in a sealed container for three days, then blitz with cider vinegar (3 tbsp) and lemon juice (1 tbsp). In a new container, seal the paste with olive oil and keep in the fridge. Spoon over and into anything savoury to ramp up its flavour.

### Too Good To Go food bag
*Sally White, chef-owner, Salcooks, Birmingham*

The [Too Good to Go](https://www.theguardian.com/environment/2019/jul/06/food-waste-how-to-get-cheap-grub-and-help-save-the-planet) [app helps fight food waste](https://www.theguardian.com/environment/2019/jul/06/food-waste-how-to-get-cheap-grub-and-help-save-the-planet). Customers buy a magic bag from a listed cafe or restaurant – maybe a chain like Yo\! Sushi or an indie like us – filled with food that would have been binned. You can't be too fussy. You might get meat, veg or a bag of cakes, and sometimes the bags are cancelled and refunded if places get busy. But you do get excellent deals. Our bags cost £3 for £9- or £10-worth of food. If you need inspiration for the contents, you can pick up some brilliant cookbooks second-hand for under a fiver. For example, Merry White's Cooking for Crowds, Claudia Roden's A New Book of Middle Eastern Food or Hugh Fearnley-Whittingstall's reliable River Cottage Veg Every Day.

### 7 Up  
*Lee Johnson, chef and co-owner, Bong Bong's Manila Kanteen, London*

I was born in Manila, and Filipino cooking is a real combination of sweet and sour, often in the form of vinegar-based dipping sauces. At Bong Bong we use 7 Up (some Filipinos think it should be Sprite) in a sweet 24-hour pork-rib marinade. I presume this came from Americans in the Philippines during the second world war, who used Coke in BBQ marinades and that kind of thing. Ours is half-and-half 7 Up and soy sauce, plus garlic, ginger, chilli, sugar and fresh lemongrass, which creates good caramelisation and sweet, citrusy, fragrant ribs.

### Kiwi knives 
*Shaun Hurrell, Barrio Comida, Durham*

I used expensive Japanese knives for years before an Aussie chef put me on to the Thai-brand Kiwi knives. They're my go-to knives for daily prep – ultra-sharp, super-lightweight and with a very thin blade you can quickly put an edge on. You can pick them up for about £5.

### Smoked butter
*Tom Kerridge, Hand & Flowers, Marlow*

Buy 200g, divide into 10 cubes and freeze them individually. As needed, stir one into a casserole, chilli, puy lentils or even a curry to give it a massive enhancement. At 40p a pop, it's an incredibly cost-effective way to drive flavour into dishes.

### Lemon
*Miguel Barclay, chef and author of One Pound Meals*

A squeeze of lemon will transform almost any food you can think of; you need that balance of acidic tartness, even in a bolognese. A lemon is what, 30p? That's the kind of cooking that real chefs do. You'll notice the difference.

### Maldon smoked sea salt
*Grace Regan, chef-owner, SpiceBox, London*

I'm salt-obsessed, and this salt takes things to the next level. I add it to salads, roasted veg and cooked grains. Sprinkled over cauliflower roasted in curry powder, its smokiness makes the soft, charred cauliflower almost meaty-tasting.

### Flat-leaf parsley
*Esther Miglio, chef and co-owner, OWT, Leeds*

I'm French and both my grandmas use parsley all the time. It's about 70p a bunch on Kirkgate Market and you can do amazing stuff with it. We use a lot of parsley sauce made with garlic, vinegar, mustard, oil, salt and pepper. You can put this vibrant green sauce on eggs, sandwiches, pasta or with fish. It keeps for five days in the fridge.

### Belazu rose harissa
*Helen Graham, head chef, Bubala, London*

I use this to jazz up pasta sauces. It's pretty punchy. Blend a tin of chopped tomatoes with one heaped tablespoon of harissa until smooth, cook for 30 minutes on a medium heat, season with salt, add pasta, parsley and feta and you've got a delicious dinner.

### Wok spatula
*Andrew Chongsathien, Brother Thai, Cardiff*

If you have a decent carbon-steel wok, ditch that wooden spoon and get a proper 14in plastic-handled stainless steel wok spatula, also known as a wok chuan or turner. Chinese supermarkets have them for about a fiver and because this shovel-like utensil is designed to fit the wok's curves, it's like having your own hand in there. It's much easier to move things around and char them properly.

### Lentils and pulses
*Ravinder Bhogal, chef-founder, Jikoni, London*

I stock up on every type of lentil and pulse I can to make dhal. LSD, life-saving dhal, as the great Madhur Jaffrey calls it. I never get bored of it. Hodmedods' British lentils and pulses have become a cheap, reliable store-cupboard essential for me. I love to fry onion, garlic and dried mint, then braise the lentils with a punctured dried Iranian lime and veg stock. Add tons of parsley and dill at the end. It's such a nourishing, flavourful meal with bread or rice.

### Toban djan sauce
*Larkin Cen, chef-owner, Woky Ko restaurants, Bristol*

My secret weapon. A salty, savoury sauce made from chillies and fermented soy beans, which I use instead of salt in a lot of food. I put it on everything. It tastes delicious stirred into cooked rice.

### Gnocchi paddle
*Masha Rener, head chef, Lina Stores, London*

I would recommend this to any home cook. After cutting freshly made gnocchi, you roll it softly across the board to score it, not just to make it pretty but also to create a better surface area for your pasta sauce to coat. This little board can also be used for different pastas as such garganelli and conchiglie.

### Marmite
*Tom Cenci, chef-owner, Loyal Tavern, London*

Marmite is essential in my cooking. Controversially, perhaps, I always add a teaspoon to the chicken broth in which I cook my goat meatballs. A dollop of this pungent, salty sauce in any meat or veggie dish, stock or stew adds a flavoursome punch.

### £1 veg bowls
*Pierre Koffmann, Koffmann's Foods, London*

I buy my vegetables at Church Street Market, near Edgware Road, London. The traders sell bowls of veg for £1. You might get five aubergines in one or a couple of white cabbages, which I use to make sauerkraut in the winter. For £5 you can get a grade-A French black-legged chicken. On Fridays, there is a chap who sells squabs for £2.50. It's first class.

### Ikea Kavalkad 24cm frying pan
*Simon Hulstone, chef-owner, the Elephant, Torquay*

Most non-stick pans last three months if you're lucky and might cost £35. We buy 50 Ikea ones at a time for £2.75 each. The non-stick only lasts weeks in a professional kitchen, but then we just use them as normal pans until they die. I've been doing this over 10 years. They used to be 79p\!

### Anchovy paste
*Paul Ainsworth, chef-owner, No 6 and Rojano's, Padstow*

Just a teaspoon of Gentleman's Relish can elevate a simple tomato sauce into something really special. Anchovy paste is a great way to add serious depth to your cooking.
