# Ham in Coke - Gastronomy Domine

Several years ago, I stumbled on a Usenet post waxing lyrical about the savoury potential of Coca Cola when combined with pork. That same Coca Cola that your teachers spent years warning you about in the very darkest terms; at my school they used a can to dissolve a volunteer’s recently shed milk tooth away to nothing, and demonstrated its unholy ability to clean pennies with rotten-incisored glee.

I have a caffeine-addicted husband and a yen to flout the outdated authority of my Home Economics teacher. I have spent several years perfecting a ham in cola recipe, and am more than mildly irritated to find that these days, Nigella Lawson is publishing a version of ham in Coke in every book she writes. No matter. Mine’s better. Ham needs something sweet and spicy to counter its savoury saltiness – it happens that cola is the perfect foil. I can’t think of another way I’d prefer to cook ham now – this may sound a perverse thing to do to a nice chunk of pork, but trust me; it’s fabulous.

### You’ll need:

* 1kg smoked gammon
* 1-2 large bottles cola (more or less depending on the size of your pan)
* 1 red onion
* 1 bulb garlic
* 1 stick cinnamon
* 1 tablespoon coriander seeds
* 2 dried chilis
* 20 cloves (give or take a few)
* 1 teaspoon ground chipotle chili
* 1 teaspoon ground cinnamon
* 1 teaspoon ground mustard
* 4 tablespoons maple syrup

1. Place the gammon in a close-fitting, thick-bottomed pan (important, this thick bottom; you need to avoid singing the bottom of your ham) with the onion, halved, the bulb of garlic, cut in halves, the cinnamon stick, coriander seeds and whole chilis. Pour over Coke to cover (I’m afraid it has to be the full-fat version; Diet Coke won’t caramelise properly) and put on a medium heat until it reaches a simmer. Lower the heat enough to keep a gentle simmer, and put the lid on for 2 1/2 hours.
2. After your kitchen timer has gone, preheat the oven to 200c and lift the whole ham carefully from the liquid (Hang onto that liquid if you want to make Boston baked beans). Leave the ham to cool enough to handle. With a sharp knife, remove the rind, without removing the fat.
3. You’ll be left with a joint of meat with a glistening covering of fat. Use your sharp knife to score the top in diamonds, and stick a clove in each corner of each diamond. Make a paste from the ground cinnamon, ground chipotles, mustard powder and maple syrup, and brush it all over the ham, concentrating on the fatty surface. The sweet mixture will caramelise onto the crisping fat; this is pretty much 90% bad for you, but, unfortunately, it tastes approximately 100% good. I really should talk a friendly social statistician somewhere into working out just how bad for you things have to be to start tasting good; I’m sure there’s an interesting graph in that somewhere.
4. Put the whole ham in the oven, uncovered, for twenty minutes, remove and check that the fatty surface has formed a crust. (If you prefer more crust, put the ham under a high grill for two minutes.)
5. If you have made a large ham, you can make several good meals from it. Eat it like this, freshly cooked, with some sautéed potatoes; eat it in Pasta alla Medici; use it to flavour Boston baked beans.
6. If you’re having people round for dinner and feel like cheating, feel free not to mention the cola. And if you enjoyed this as much as I do, you’ll probably want to check out the sticky chicken pieces in coke too.
