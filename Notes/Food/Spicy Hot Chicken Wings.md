# Spicy Chicken Wings

![spicy chicken wings](./@imgs/recipes/spicy_chicken_wings.jpeg)

## Ingredients

Serves 4

- 1tbsp rapeseed oil
- 1tbsp garlic and ginger paste
- <sup>1/2</sup>tbsp curry powder
- <sup>1/2</sup>tbsp garam masala
- 1tsp Carolina chilli powder
- 1tbsp tomato purée
- 1tbsp sugar or honey
- 100ml base curry sauce
- 12 chicken wings, skin removed
- Juice of 1 lime
- A pinch of dried fenugreek leaves
- Salt
- 1tbsp finely chopped coriander
- Raita and lime wedges to serve

## Method

1. Heat the oil in a frying pan over a medium-high heat until really hot. 
1. Add the garlic and ginger paste and fry for 30 seconds. 
1. Stir in the curry powder, garam masala, chilli powder, tomato purée and sugar or honey.
1. Give it all a good stir, then stir the base sauce in and bring to the boil.
1. Reduce the heat a little, add the chicken wings and allow to cook in the simmering sauce for 7 minutes, or until cooked through. If you need to add a little more base sauce or water to cook the chicken through, do it.
1. To finish, squeeze the lime juice over the top and sprinkle with the dried fenugreek leaves. 
1. Check the seasoning and adjust if needed. Sprinkle with the chopped coriander and serve with raita and lime wedges.
