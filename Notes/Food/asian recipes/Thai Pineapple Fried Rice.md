# Pineapple Fried Rice

![Pineapple Fried Rice](./@imgs/recipes/pineapple_fried_rice.jpg)

Darlene Schmidt | [Link](https://www.thespruceeats.com/special-fried-rice-with-pineapple-prawns-3217416)

Total | Prep | Cook | Yield
-- | -- | -- | --
30 mins | 15 mins | 15 mins | 4 to 6 servings

## Ingredients

* 3 to 4 cups cooked rice (preferably several days old)
* 1 small can pineapple chunks (drained) or 1 1/2 cups fresh pineapple chunks
* Handful raw prawns or shrimp (shells removed or tail-on)
* 1/4 cup shallots (finely chopped)
* 4 cloves garlic (finely chopped)
* 1 red or green chili (thinly sliced) or 1/4 to 3/4 teaspoon dried crushed chili flakes
* *Optional*: 1 to 1 1/2 cups green or red bell pepper (diced)
* 1/4 cup chicken stock
* 1 egg
* 1/2 cup frozen peas
* 1/4 cup currants or raisins
* 1/2 cup roasted unsalted cashews
* 2 to 3 spring onions (finely sliced)
* 3 tablespoons fish sauce
* 1 tablespoon soy sauce
* 2 teaspoon Thai curry powder or regular curry powder
* 1/2 teaspoon sugar

## Instructions

1. If using old rice, oil your fingers with up to 1 tablespoon vegetable oil, then work through the rice with your hands, separating any chunks back into grains. Set aside.
1. Combine stir-fry sauce ingredients (fish sauce, soy sauce, curry powder, and sugar) together in a cup, stirring to dissolve. Set aside. Heat a wok or large frying pan over medium-high heat. Add 2 tablespoons vegetable oil and swirl around, then add shallots, garlic, and chili, stir-frying 1 minute.
1. Add bell pepper, if using, plus 1 to 2 tablespoons chicken stock. Stir-fry 1 to 2 minutes.
1. Add the shrimp plus chicken stock, 1 to 2 tablespoons. at a time, enough to keep ingredients sizzling. Stir-fry until shrimp turn pink and plump (2 to 3 minutes).
1. Push ingredients aside and crack the egg into the pan, stirring quickly to cook (like making scrambled eggs).
1. Now add the rice, pineapple chunks, peas, and currants/raisins.
1. Drizzle stir-fry sauce mixture over and gently stir-fry to combine over medium-high to high heat. You want the rice to "dance" (make popping sounds) as it fries (5 to 10 minutes), or until rice has achieved desired lightness.
1. Toward the end of this cooking time, add the cashews.
1. Remove from heat.
1. Taste-test for salt and flavor, adding more fish sauce until desired taste is achieved.
1. To serve, scoop rice onto a serving platter or in a carved-out pineapple boat if serving at a party. Top with spring onions and enjoy.

## Tips

Always scrape from the bottom of the pan as you fry the rice. Avoid adding any more stock at this point, or your rice will become heavy; you want the pan hot and dry. However, you can push ingredients aside and add a little more oil to the pan or wok (this provides that special 'shine' you see in restaurant fried rice).
