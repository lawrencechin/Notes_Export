# Crispy Chinese roast pork

I am pathetically proud of having successfully cooked a strip of Chinese roast belly pork (siew yoke or siew yuk, depending on how you transliterate it) at home. This pork, with its bubbly, crisp skin and moist flesh is a speciality of many Cantonese restaurants. An even, glassy crispness is hard to achieve if you’re making it at home, but I think I’ve cracked it; with this method, you should be able to prepare it at home too.

You’ll need a strip of belly pork weighing about two pounds. Here in the UK you may have trouble finding a belly in one piece (for some reason, belly pork is often sold in thick but narrow straps of meat); look for a rolled belly which you can unroll and lay flat, make friends with a pliant butcher or shop at a Chinese butcher (you’ll find one in most Chinatowns). Look for a piece of meat with a good layer of fat immediately beneath the skin. The belly will have alternating layers of meat and fat. Try to find one with as many alternating strips as possible.

To serve three or four (depending on greed) with rice, you’ll need:

* 2lb piece fat belly pork
* 1 teaspoon sugar
* 1 teaspoon salt
* 1 teaspoon five-spice powder
* ½ teaspoon cinnamon
* 1 tablespoon Mei Gui Lu jiu (a rose-scented Chinese liqueur – it’s readily available at Chinese grocers, but if you can’t find any, just leave it out)
* 3 cloves garlic, crushed
* 2oo ml water
* 2 tablespoons Chinese white vinegar

1. Bring the water and vinegar to the boil in a wok, and holding the meat side of your pork with your fingers, dip the rind in the boiling mixture carefully so it blanches. Remove the meat to a shallow tray and dry it well. Rub the sugar, salt, five-spice powder, cinnamon, Mei Gui Lu jiu and garlic well into the bottom and sides of the meat, leaving the rind completely dry. Place the joint rind side up in your dish.
2. Use a very sharp craft knife to score the surface of the rind. If your rind came pre-scored, you still need to work on it a bit – for an ideal crackling, you should be scoring lines about half a centimetre apart as in this photo, then scoring another set of lines at ninety degrees to the original ones, creating tiny diamonds in the rind. Rub a teaspoon of salt into the rind. Place the dish of pork, uncovered (this is extremely important – leaving the meat uncovered will help the rind dry out even further while the flavours penetrate the meat) for 24 hours in the fridge.
3. Heat the oven to 200° C (450° F). Rub the pork rind with about half a teaspoon of oil and place the joint on a rack over some tin foil. Roast for twenty minutes. Turn the grill section of your oven on high and put the pork about 20cm below the element. Grill the meat with the door cracked open for twenty minutes, checking frequently to make sure that the skin doesn’t burn (once the crackling has gone bubbly you need to watch very closely for burning). The whole skin should rise and brown to a crisp. This can take up to half an hour, so don’t worry if the whole thing hasn’t crackled after twenty minutes – just leave it under the grill and keep an eye on it.
4. Remove the meat from the heat and leave it on its rack to rest for fifteen minutes. Cut the pork into pieces as in the picture at the top of the page. Serve with steamed rice, with some soya sauce and chillies for dipping. A small bowl of caster sugar is also traditional, and these salty, crisp pork morsels are curiously delicious when dipped gingerly into it.
