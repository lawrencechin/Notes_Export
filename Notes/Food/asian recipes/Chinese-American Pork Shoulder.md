# [Chinese-American Pork Shoulder](https://food52.com/recipes/3902-chinese-american-pork-roast)

This pork shoulder has lots of layers: earthy mushrooms, salty soy sauce, sweetness from the sugar. The sauce at the end is a great combination of flavours.

Serves 8

## Ingredients

* 1/4 cup vegetable oil
* 4 ginger slices
* 3	 tablespoons brown sugar
* 4 1/2 pounds pork shoulder (*boneless, trimmed of skin and fat*)
* 1 onion, sliced thinly
* 1 shallot, sliced thinly
* 2 green onions
* 4 garlic cloves, chopped
* 6-8 shiitake mushrooms (*other types can be substituted*)
* 1/4 cup sweet rice wine (*shao-xing wine, dry sherry or sake can be substituted*)
* 1	 cup soy sauce

## Method

1. An important first step is to tie the pork shoulder roast. This will help keep the meat from falling apart. You can ask your butcher to do this for you. 
2. Using a long knife, insert knife into pork shoulder in several places all around roast. This will help the marinade penetrate the meat. 
3. In a large Dutch oven (*or heavy oven-proof pot with a lid*) heat the cooking oil. Add ginger and brown sugar and sauté for 1-2 minutes. This flavours the oil and melts the sugar. 
4. Add the pork shoulder and brown it on all sides. 
5. Remove pork from pot. 
6. Add onion and mushrooms to pot. Sauté until onions are soft and slightly browned. 
7. Add garlic and sauté briefly, then wine and soy sauce. Heat and stir to mix. 
8. Return the browned pork shoulder to the pot. Turn pork over to coat well with marinade. Pile mushrooms and onions on top. You can continue straight to roasting here, but letting the pork sit in the refrigerator 3 hours to overnight will help the flavours penetrate the meant. 
9. Turn occasionally if you can. Let refrigerated pork roast sit at room temperature for 1 hour. 
10. Preheat oven to 300 degrees. Cover top of pot with foil and cover with lid. Roast for 3-3 ½ hours. 
11. Check the roast once every hour and turn it over. Baste the roast with juices that accumulate in pot. 
12. For a crustier outside layer, remove pot from oven and turn up the oven to 400 degrees. 
13. Transfer roast fat side up to sheet pan lined with foil. Take 3 Tbsp of juices from pot and combine with 2 Tbsp brown sugar. Spread all over top of roast and return to oven for 15 minutes until sugar has caramelised, but not burned. 
14. Let roast rest out of the oven for 15 minutes before serving. Decant sauce to fat separator or smaller bowl to remove fat, if desired. 
15. Remove roast to serving platter, cut and remove strings, then cut meat into thick slices across the grain of the meat. 
16. Drizzle roast pork with sauce and serve extra sauce along side. Serve with white rice.
