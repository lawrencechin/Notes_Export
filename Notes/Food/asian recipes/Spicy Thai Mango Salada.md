# [Spicy Thai Mango Apple Salad](https://pickledplum.com/thai-apple-salad-recipe/)

![Spicy Thai Mango Apple Salad](./@imgs/recipes/thai_mango_apple_salad.jpg)

Prep Time | Total Time | Yield | Cuisine
-- | -- | -- | --
15 minutes | 15 minutes | 2 people | Thai

A delicious and refreshing Thai inspired mango apple salad.

## Ingredients

- 1 green apple (peeled and sliced into strips (julienned))
- 1 small mango (peeled and sliced into strips (julienned))
- 2 small Thai chilis (finely chopped)
- 1 clove garlic (finely chopped)
- 3 tablespoons cashews (salted or unsalted)
- 1 shallot (finely chopped)
- 2 tablespoons cilantro (roughly chopped, *optional*)
- freshly ground black pepper

### For the dressing

- 2 tablespoons fresh lemon juice
- 1/4 teaspoon kosher salt
- 1/2 teaspoon fish sauce

## Instructions

1. Put all the ingredients into a bowl and toss well until all the fruit is coated.
1. Season with freshly ground pepper and serve cold.
