# Quick Stir Fry—Peppery Pork Cucumber Stir-fry

Serves 4

## Ingredients

* 1 pound ground pork
* 2 tablespoons soy sauce
* 1 tablespoon cornstarch
* canola oil
* 1 tablespoon chopped tender part of lemongrass
* 4 cloves garlic, minced
* 2 teaspoons fresh ginger (about 1/3 inch), peeled, smashed and minced
* 1 bell pepper, slivered
* 1 English cucumber (about 7 or 8 ounces), sliced into coins
* 8 scallions, finely chopped crosswise (about 1/2 cup)
* 1 tablespoon isot pepper, divided

## For the sauce:

* 1/2 cup water
* 1/3 cup soy sauce
* 2 teaspoons sugar
* 1 tablespoon cornstarch
* cooked rice

1. Put the pork in a bowl and add 2 tablespoons soy sauce and 1 tablespoon cornstarch. Mix it together well with your hands, then let it sit for 10 or 15 minutes.
2. In a heavy-bottomed skillet or a wok, heat 2 tablespoons canola oil. Prepare a plate to drain the cooked pork, with two or three layers of paper towel or a lint-free cloth. When the oil is hot, add the pork all at once, breaking it up into small chunks as you stir and cook. Cook it thoroughly, then scoop out of the pan (I used the Joseph Joseph scoop colander, which sees a lot of action at our house) and drain on the towel.
3. Discard any fat in the pan and wipe it clean. Add 1 tablespoon oil and heat it on a medium flame. Set a timer for 5 minutes.
4. When the oil is hot, add the lemongrass, garlic and ginger, and stir for 30 seconds. Then add the bell pepper; stir and cook for another minute. Add the cucumber coins; stir and cook everything for one more minute. Then add the scallions, cook and stir for another half minute, and then add in the pork and mix everything together.
4. Give the sauce ingredients a quick stir and pour into the pan. 5. Stir everything together—the cornstarch will cook very quickly and there will be just enough sauce to coat everything lightly. At the last moment, stir in 2 teaspoons of isot pepper.
6. Serve over cooked white rice. When you plate this, to finish, scatter more isot pepper over everything with a generous hand, and serve.
