# Shrimp Fried Rice Recipe
**Prep time**: 15 minutes
**Cook time**: 15 minutes
**Yield**: Serves 4.

Make sure to use leftover, day old rice when making fried rice. Freshly made rice will make a fried rice that's mushy.

## INGREDIENTS
* 8 ounces small raw shrimp, shelled and deveined
* 1/2 teaspoon kosher salt
* freshly ground black pepper
* 1/2 teaspoon cornstarch
* 2 tablespoons high smoke point oil such as canola oil or rice bran oil
* 3 eggs, beaten
* 2 stalks green onion, minced
* 4 cups leftover rice, grains separated well
* 3/4 cup frozen peas and carrots, defrosted
* 1 tablespoon soy sauce (use gluten-free soy sauce if you are making a gluten-free version)
* 1 teaspoon sesame oil
		
## METHOD
1. In a medium bowl, sprinkle the shrimp with salt, pepper, and cornstarch, and toss to coat. Set aside to sit for ten minutes at room temperature.
2. Heat a large sauté pan or wok (a seasoned cast iron pan or hard anodized aluminum works well, they're relatively stick free and can take the heat) on high heat. When the pan is very hot (a drop of water instantly sizzles when it hits the pan), swirl in a tablespoon of the oil to coat the pan.
3. Add the shrimp to the pan, spreading them out quickly in a single layer on the pan. Let them fry in the pan without moving them, for 30 seconds. Flip them over and let them fry on the other side for another 30 seconds or until they are mostly cooked through. Use a slotted spoon to scoop the shrimp out of the pan to a plate, leaving as much oil in the pan as possible.
4. Return the pan to the burner and lower the heat to medium. Add the beaten eggs and stir them quickly to scramble them while they cook. When the eggs are almost cooked through, still a bit runny, remove them from the pan to the plate with the cooked shrimp.
5. Clean out the pan or wok with paper towels and return it to the burner. Heat the pan on high and when it is hot, swirl in the remaining tablespoon of oil. When the oil is shimmering hot (almost smoking), add the green onions and sauté for 15 seconds. Then add the leftover cooked rice to the pan and stir with the green onions to mix well. Spread the rice onion mixture over the surface of the pan and let it fry, without moving it, until you hear the rice sizzle, about 1 to 2 minutes. Use a spatula to toss the rice, and spread it over the pan again.
6. Sprinkle soy sauce around the rice and toss to combine. Add the carrots, peas, shrimp, eggs, and sesame oil, stirring to combine well. Let everything heat up again until sizzling hot. Add more soy sauce to taste.

