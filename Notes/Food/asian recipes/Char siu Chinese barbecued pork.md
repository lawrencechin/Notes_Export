# Char siu – Chinese barbecued pork

Char siu is a brilliantly versatile thing. Even if you’re not familiar with it by name, you’ve almost certainly tasted it before; it’s the reddish pork that appears in little pieces in every Special Fried Rice in every Chinese restaurant and takeaway in the country, in those wonderful fluffy buns you get as dim sum (my recipe for those buns is here), on its own over rice as a roast meat, and sliced thickly in a million different noodle dishes. It’s a sweetly glazed, aromatically spiced, perfectly delicious piece of meat, and one of my very favourite things to do with pork.

This recipe makes a single fillet of charsiu. I’d recommend you at least double it – you’re going to need a whole fillet of the stuff for Monday’s recipe, and you’ll probably want to eat at least some as soon as it comes out of the oven. Charsiu freezes well too, so you don’t need to worry about cooking too much.

A note on the glaze and colour. The strips of charsiu you’ll see in Chinese shops are usually glazed with maltose, a sugary by-product of the brewing industry. It does achieve a really gorgeous, crackly sheen, but it’s not got a lot of flavour or sweetness, and I find it’s not as tasty as glazing with a honey/soy mixture, thinned with a little vegetable oil to help the sugar catch and caramelise. Shop-bought charsiu is normally very red, because a little food colouring is used in the marinade. Feel free to add half a teaspoon to yours if you like – I find I’m happy with the less shocking colour the meat gets from the hoi sin sauce in its marinade.

To make one strip of charsiu (enough for three as a roast meat on rice) you’ll need:

1 pork fillet

### Marinade
* 5 tablespoons light soya sauce
* 3 tablespoons dark soya sauce
* 5 tablespoons runny honey
* 3 tablespoons sugar
* 1 teaspoon five spice powder
* ½ glass Chinese rice wine (sherry will do if you can’t find any)
* 3 tablespoons Hoisin sauce (I like Lee Kum Kee)
* 1 thumb-sized piece of ginger, crushed
* 4 fat cloves of garlic, crushed

### Glaze
* 2 tablespoons runny honey
* 1 tablespoon dark soya sauce
* 1 tablespoon vegetable oil

1. Mix all the marinade ingredients together and warm through in a saucepan until the sugar has all dissolved. Pour the warm marinade over the pork, and leave for at least eight hours in the fridge.
2. To cook the charsiu, heat the oven to 210° C and place the meat, basted with some of its marinade, on a rack over a roasting tin with a couple of centimetres of water in it. Roast for 20 minutes, then baste again on both sides, turn the meat over and reduce the heat to 180° C. Roast for another ten minutes, then baste and turn again, and roast for a final ten minutes.
2. Transfer the meat to a plate, empty the tin of water and line it with foil. Place the meat and rack back on the tin, then brush it liberally with the glaze and put it under the grill for about five minutes, until the glaze is glossy and starting to catch at the edges. Turn the meat, glaze again and put back under the grill until the other side is also glossy and starting to caramelise.