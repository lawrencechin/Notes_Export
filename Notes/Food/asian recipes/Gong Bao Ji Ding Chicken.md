# Gong Bao Ji Ding Chicken
Serves 3~4

### Tenderizing the Meat

* 2 chicken thighs, deboned and cut into 1/2-inch cubes (if yours are tiny, you may want to throw in 1-2 more)
* 1/2 teaspoon beaten egg
* 2 teaspoons cornstarch
* 1 pinch salt
* 1 teaspoon chinese cooking wine

### Stir-Frying

* 2 teaspoons dark soy sauce
* 2 teaspoons brown sugar
* 1 tablespoon chinese dark vinegar
* 1 tablespoon cornstarch
* 6 tablespoons of water or stock
* 1 generous handfuls of peanuts
* 2 green onions, chopped into 1-inch lengths
* 4 garlic cloves, skin removed, smashed and chopped
* 6 slices of ginger
* 8 red dried chiles, chopped
* 4 teaspoons Sichuan peppercorns
* 1/2 cup vegetable oil

### Directions
1. Mix together the marination with the meat; set aside while preparing the rest. *Can put in fridge for the day.
2. Mix the liquid ingredients, brown sugar and corn starch and set aside to use as the sauce for stir-frying. Heat up wok with vegetable oil until shimmering and hot, about 120 F.
3. Dip half of the meat into the oil and move around until half-cooked, around 2 minutes; remove with slotted spoon and drain from oil. Repeat for the other half.
4. Drain off all but 2 tbsp of oil in heated wok, throw in chiles, peppercorns, garlic, ginger and spring onion; stir-fry until fragrant, about 2 minutes; add peanuts and stir-fry for another 1~2 min.
5. Add chicken cubes, stir-fry for about 3 minutes, or until chicken is cooked.
6. Pour on reserved sauce and simmer until the dish thickens, about 3 minutes.
7. Garnish with ground Sichuan peppercorn; serve with rice.
