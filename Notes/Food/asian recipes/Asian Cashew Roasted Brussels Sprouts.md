# Asian Cashew Roasted Brussel Sprouts

Author: Jeanie and Lulu's Kitchen | [link](https://jeanieandluluskitchen.com/asian-cashew-roasted-brussels-sprouts/)

![Asian Cashew Roasted Brussels Sprouts](./@imgs/recipes/asian_sprouts.jpg)

Prep Time | Cook Time | Servings | Calories
-- | -- | -- | --
10 mins | 20 mins | 4 | 256 kcal
 
Roasted brussel sprouts are taken to a whole new flavor level! The Asian inspired mixture and chopped cashews they roast in make them such an amazing side.

## Ingredients

- 16 whole brussels sprouts halved
- 1/4 cup toasted sesame oil
- 2 tablespoons soy sauce
- 1 dash Sriracha
- 1 dash worcestershire sauce
- 1/2 cup cashew nuts roughly chopped

## Instructions

1. Pre-heat the oven to 375 degrees. Get out a large baking dish and spread the brussels sprout halves evenly in it.
1. Mix the sesame oil, soy sauce, sriracha and Worcestershire together thoroughly in a bowl.
1. Pour that liquid all over the brussels sprouts and sprinkle the cashews all over too. Give everything a quick stir in the baking dish to make sure the brussels sprouts are coated.
1. Then roast it all for 20 minutes. When it's done, serve immediately and enjoy! Easy!
