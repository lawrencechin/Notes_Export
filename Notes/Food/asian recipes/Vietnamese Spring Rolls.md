# Vietnamese Spring Rolls

## Ingredients

* 1 lb ground pork
* 1 (4 ounce) packagerice vermicelli, soaked in cold water for 15 minutes,drained and chopped
* 1 egg
* 1 green onion, minced
* 1 small onion, minced
* 2 cloves garlic, mashed
* 1⁄4 cup finely grated carrot
* 1⁄4 cup finely grated cabbage (optional)
* 1⁄2 cup bean sprouts
* 3 tablespoons cilantro, chopped
* 2 tablespoons white wine
* salt
* white pepper
* 20 spring roll wrappers (I like the frozen ones, though the dry rice wraps are good too, just more difficult to work with)
* oil (for frying)


## Directions

1. Combine all ingredients except wraps and oil in a bowl.
On a damp tea towel, place a wrap, and fill with a couple of tablespoons of the pork mixture in a sausage like shape at one end of the wrap.
2. Fold sides in, then begin rolling spring roll at the mixture side.
3. Seal roll by moistening edge with water.
4. Heat oil over medium-high heat.
5. Fry rolls, turning frequently until cooked through and golden, about 5-7 minutes.
6. Drain on paper towels, and serve with Spring Roll Sauce (Nuoc Mam).
7. Keep warm in heated oven.

**Note**: The spring rolls may be made ahead, and frozen, and then fried when thawed.
Spring rolls may be kept in fridge until ready to fry too, up to a day or two in advance.
Ground chicken may be substituted for the pork, and chopped shrimp may also be added for a special flavour combination.
