# Singapore Fried Rice Noodles
## Recipe: Sing Chow Mai Fun (Singapore Fried Rice Noodles/星洲炒米粉)

Adapted from Fine Cooking Magazine, Nov 2005, pages 64*65

### Ingredients:

* 2 cloves garlic, minced
* 1 tablespoon minced ginger
* 8 dried or fresh shiitake mushrooms
* 12 ounces of fine dry rice vermicelli (Wai Wai brand recommended)
* 2 stalks celery, sliced thin
* 1 medium yellow onion, sliced thin
* 4 jalapeño peppers, seeded and sliced thin
* 1 cup bean sprouts, rinsed and drained
* 8 green onions, root end trimmed, cut into 2*inch pieces
* 1 pound shrimp, peeled and deveined
* 1 pound charsiu (Chinese barbecued pork), cut into matchsticks
* 4 tablespoons vegetable oil
* 3 tablespoons oyster sauce
* For sauce:
* 3 tablespoons Madras (hot) curry powder
* 2 cloves garlic, minced
* 1 tablespoon minced ginger
* 1 cup chicken broth
* 4 tablespoons soy sauce
* 4 teaspoons granulated sugar
* 2 teaspoons hot chilli paste
* 2 tablespoons vegetable oil

Method:

1. If using dried shiitake mushrooms, soak them in hot water for half an hour. Drain, then cut off the stems. Slice the mushrooms thinly.
2. Put the rice vermicelli in a large bowl and soak in enough hot water to cover, until the noodles are soft (about 8 to 10 minutes). Drain noodles and set aside.
3. Start by heating up 2 tablespoons of oil in a small pan over medium heat. Add the curry powder, the ginger, and the minced garlic, and sauté until fragrant. Add the chicken broth, soy sauce, sugar, and chilli paste. Stir to combine and then cover and cook for 5 minutes. Remove pan from heat and set aside.
4. Heat 2 tablespoons of oil in a large wok over high heat. Add in the remaining garlic and ginger, and stir-fry until the garlic starts to become golden. Add in the celery, onion, pepper, sprouts, green onions, and mushrooms. Stir-fry for 3 minutes, until the vegetables start to soften. Set the vegetables aside in a bowl.
5. Heat the last 2 tablespoons of oil in the wok over high heat. Add in the shrimp and stir-fry until they start to turn pink on both sides. Add the charsiu and toss to combine.
6. Add in the noodles and the vegetables. Pour on the sauce and also add the oyster sauce. Mix the ingredients thoroughly to coat all the noodles and incorporate all the vegetables.
7. Serve hot.
