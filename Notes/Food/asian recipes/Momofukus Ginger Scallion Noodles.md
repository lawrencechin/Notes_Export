# Momofuku’s Ginger Scallion Noodles

## Ingredients

* 2 1/2 cups thinly sliced or shredded scallions (about 1 to 2 large bunches)
* 1/2 cup finely minced peeled fresh ginger
* 1/2 cup grapeseed or other neutral oil, divided
* 2 tablespoons soy sauce, preferably usukuchi (light soy sauce), divided
* 3/4 teaspoon sherry vinegar
* 3/4 teaspoon kosher salt, or more to taste
* 1 pound firm tofu, cut into 1-inch wide planks
* 1 pound Asian wheat noodles
* Sriracha hot sauce for serving (optional)

## Directions

1. In a medium bowl, mix together the scallions, ginger, 1/4 cup of the oil, 1 1/2 teaspoons of the soy sauce, vinegar, and salt. Cover the tofu with the remaining soy sauce and set aside. Allow to sit at room temperature for 15 minutes.
2. Bring a large pot of water to a boil. Heat remaining 2 tablespoons oil in a 12-inch skillet over medium until shimmering. Add the tofu and cook until golden, about 4 minutes total, turning the pieces once. Transfer tofu to a paper towel-lined plate, then cook noodles according to package directions.
3. Drain the noodles and toss them with the ginger scallion sauce in a large bowl. Transfer to individual bowls and top with the tofu. 

Serve with sriracha, if desired.
