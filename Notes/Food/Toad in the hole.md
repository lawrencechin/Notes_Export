# [Sam's Toad in the hole](https://www.bbcgoodfood.com/recipes/1572643/sams-toad-in-the-hole)

## Ingredients

* Sausages
* 1 tbsp sunflower oil
* 140g plain flour
* 1/2 tsp salt
* 2 eggs
* 175ml semi-skimmed milk
* 1 tbsp wholegrain mustard (*optional*) 

## Method

1. Heat over to 200C fan. Put the sausages in a 20 x 30cm roasting tinth the oil, then bake for 15 minutes until browned.
2. Meanwhile, make up the batter mix. Tip the flour into a bowl with the salt, make a well in the middle and crack both eggs into it. Use an electric whisk to mix it together, then slowly add the milk, whisking all the time. Leave to stand until the sausages are nice and brown.
3. Carefully remove the sausages from the oven - watch because the fat will be sizzling hot - but if it isn't, put the tin on the hob for a few minutes until it is. Pour in the batter mix, transfer to the top shelf of the oven, then cook for 25-30 minutes, until risen and golden. Serve with gravy and Sam's favourite vegetable - broccoli. 
