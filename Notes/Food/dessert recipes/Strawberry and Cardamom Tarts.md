# Strawberry and Cardamom Tarts
## Blanche Vaughan's perfect strawberry and cardamom tarts

Serves 4

### For the pastry:

* 120g plain flour
* A good pinch of salt
* 70g cold, unsalted butter, diced
* 40g sugar
* 4-6 cardamon pods
* 1 egg yolk
* A little cold water
* For the filling:
* 1 punnet of strawberries
* 1 vanilla pod, halved with the seeds scraped out
* 1 tbsp sugar
* ½ lemon, juiced

1. Make the pastry by sifting the flour into a bowl with the salt and stirring in the chopped butter. You can now either blitz it in a Magimix until it is like small breadcrumbs, or lightly rub it with your finger tips to reach the same consistency.
2. Add the sugar and mix well. In a pestle and mortar, lightly bash the cardamom pods and open their green cases to allow the black seeds to be released.
3. Discard the outer case and grind the black seeds until they are a powder. Add this to the flour mixture.
4. Add the egg yolk and mix well and as much water as you need to bring the dough together into a ball.
5. Wrap in cling film and refrigerate to rest for at least 20 minutes.
6. Heat the oven to 160°C.
7. Hull the strawberries and slice. Put them in a bowl with the vanilla, sugar and lemon juice and allow to macerate while the pastry rests.
8. Grate the chilled pastry and press it into a tart shell or for individual cakes, use a cup cake tray.
9. Bake the pastry in the oven for 15 minutes or so or until it is just starting to brown. Remove from the oven and fill with the strawberries then continue to cook for another 15-20 minutes.
10. Remove and allow to cool slightly before serving.

Delicious with a dollop of cool yoghurt.
