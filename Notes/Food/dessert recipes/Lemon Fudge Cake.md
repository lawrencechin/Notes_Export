# Lemon Fudge Cake Recipe

Mary Berry  

![Lemon fudge cake](./@imgs/recipes/lemon_fudge_cake.jpg)

## Ingredients
### For the base

* 7 plain digestive biscuits, crushed
* 50g butter, melted
* 25g demerara sugar

### For the filling
* 2 eggs
* 150g caster sugar
* 300ml double cream
* 50g plain flour
* Grated zest and juice of 2 lemons

### For the topping
* 150g Greek-style yoghurt
* 2 tbsp lemon curd

**Serves 6-8 (or more as it is very rich!)**  

## Instructions

1. Preheat oven to 180°C/160°C fan
1. Line the base of a 20cm round springform tin with baking parchment. In a bowl, mix the crushed biscuits with the melted butter and sugar.
1. Stir until all the biscuit is coated, then press into the base of the prepared tin.
1. Make the filling: whisk the eggs and sugar together until blended. Pour in the cream, sift in the flour and whisk until blended. Stir in the lemon juice and zest, and whisk again until combined.
1. Pour this into the tin on top of the biscuit base and bake in the preheated oven for about 30-40 minutes, until the filling has set and is pale golden brown on top. Remove from the oven and allow to cool in the tin.
1. This can be made and baked up to 3 days ahead, but ice it on the day of serving. It also freezes well un-iced.
1. Mix the topping ingredients together and spread over the cold cake. Decorate with the blueberries.
