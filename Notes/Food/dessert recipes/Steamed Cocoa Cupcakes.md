# Steamed Cocoa Cupcakes

![Steamed Cocoa Cupcakes](./@imgs/recipes/Steamed_Cocoa_Cupcakes.jpg)

## Ingredients

- 85g hot chocolate powder
- 80ml milk
- 30g honey
- 1/2 tsp salt
- 1 tsb baking powder
- 100g cake flour
- 40g melted butter
- 2 eggs

## Instructions

1. Add hot chocolate powder and milk into mixing bowl and combine until smooth
1. Put the honey, salt, baking powder into mixture and whisk together
1. Sift in flour, introduce butter and eggs then mix until smooth and all lumps are gone
1. Put 4 cupcake cases into 4 foil cases, pour equal amounts of mixture into each 
1. Prepare steamer, carefully place cupcakes into steamer and cover. Steam for 15 minutes on a medium-low heat
1. Enjoy
