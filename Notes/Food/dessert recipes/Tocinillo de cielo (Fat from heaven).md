# [How to make Tocinillo de cielo (*Fat from heaven*)](http://www.independent.co.uk/life-style/food-and-drink/how-to-make-tocinillo-de-cielo-fat-from-heaven-a7889931.html)

Serves 4
Preparation time: 10 minutes
Cooking time: 30 minutes, plus chilling

## Ingredients

* 130g sugar
* 50ml water
* Strip of lemon zest
* 4 egg yolks
* 1 egg

### For the caramel

* 2 tbsps sugar
* 2 tbsps water

 

## Method

1. Find a small ovenproof dish for this dessert – it’s very sweet so you only make it in small quantities.
2. Put the **sugar** and **water** for the **caramel** into the dish and place over a **medium heat** until it turns to **caramel** and is a lovely golden colour. 
3. Swirl the **caramel** all over the bottom of the dish and a little up the sides.
4. Set aside.
5. Put the **sugar**, **water** and **lemon zest** into a small pan over a **medium heat**.  When it comes to the boil, let it bubble for a further **3 minutes**.  Take off the heat and remove and discard the lemon zest. 
6. Set aside for **5 minutes**.
7. Very gently whisk together the **egg yolks with the whole egg** – you do not want to create any froth. 
8. Pour the warm syrup into the egg mixture in a thin, steady stream, whisking as you do so.  Keep whisking until it is completely incorporated. 
9. Pour this mixture into the coated caramel–coated dish and place the dish inside a deep-sided roasting tin.  Pour in a couple of fingers’ depth of cold water (*to create a bain marie*) and put the whole thing in a cold oven. 
10. Turn the temperature to **160°C/gas mark 3** and cook for **25 minutes** or until set.  Take care not to overcook – it should still have a slight wobble.
11. Chill in the fridge for a couple of hours before serving.  Run the point of a knife around the edge of the dish and then turn out on to a plate.  Do not eat too much of this as it is extremely calorific, but it is a real treat.
