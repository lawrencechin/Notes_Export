# Milk Chocolate Ganache Tart

## Crust:

* 2 cups coarse pretzel crumbs
* 1 stick very soft butter
* ½ cup powdered sugar

## Filling:

* 12 oz. good quality milk chocolate
* 8 oz heavy cream
* ½ stick (4 tablespoons) butter

## Directions

1. To make the crust combine the pretzel crumbs, soft butter and powdered sugar. Using finger tips, rub together until combined and the mixture forms large, coarse crumbs. Place in a tart or pie pan and with the back of a small glass, firmly press the crumbs evenly in the bottom and up the sides of the pan.
Bake for about 10 minutes in a 350 degree oven until the edge of the crust just begins to brown. Remove from the oven and set aside while preparing the filling.
2. To make the filling, coarsely chop the chocolate and place in a medium bowl with the butter. In a small pan bring the cream just to a boil and pour over the chocolate and butter. Let sit for about 5 minutes and gently stir with a spatula until the chocolate has completely melted and the mixture is smooth. Do not use a mixer or a whisk to minimise creating bubbles or beating excess air into the mixture.
3. Pour into the cooled crust and refrigerate overnight. To serve, slice into small wedges.

Serves 8-10
