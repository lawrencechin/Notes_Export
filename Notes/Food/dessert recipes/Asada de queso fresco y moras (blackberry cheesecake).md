# [How to make Tarta asada de queso fresco y moras (*Blackberry cheesecake*)](http://www.independent.co.uk/life-style/food-and-drink/how-to-make-blackberry-cheesecake-a7879096.html)

Serves | Preparation Time | Cooking Time
--|--|--
6 | 5 Minutes | 30 Minutes

## Ingredients

* Butter, for greasing
* 300g cream cheese
* 3 eggs
* 200ml double cream
* 200g caster sugar
* 100g blackberries
* 20g icing sugar (optional)

## Method

1. You can make this blackberry cheesecake using pretty much any soft cheese; cream cheese, cottage cheese, quark or curd will all give great results.
3. Preheat the oven to 180°C/gas mark 4 and lightly grease a 20 cm cake tin with a little butter.
4. Put the cream cheese, eggs, cream and sugar into a bowl and use an electric whisk or hand blender to mix together for no longer than 40 seconds.
5. Tip the mixture into your prepared dish and spread the blackberries on top.  
6. Bake in the preheated oven for about 30 minutes, or until a knife inserted into the middle comes out clean.  
7. Remove from the oven and allow to cool.  Dust with icing sugar before serving.
