# Microwave Cheese Cake (mejor que es suene)

![Microwave Cheese Cake](./@imgs/recipes/Microwave_Cheese_Cake.jpg)

## Ingredients

- 2 eggs (yolks)
- 3 tbsp sugar
- 200g cream cheese
- 20g cake flour
- 2-3 drops of vanilla essence
- a little butter or oil

## Instructions

1. Place two egg yolks in a bowl with all the sugar and mix together until the sugar has dissolved
1. Add all the cream cheese and combine with eggs/sugar until it is smooth 
1. Sift in the flour and stir through until the mixture is smooth
1. Use oil or butter to grease a microwavable bowl
1. Pour mixture into bowl and smooth the top before banging the bowl on surface one or twice
1. Put a plate atop the bowl and into the microwave for four minutes (700w)
1. Turn the plate upside down to remove the cake and let it cool in the fridge
