# Almond Loaf

![Almond Loaf](./@imgs/recipes/Almond_Loaf.jpg)

## Ingredients

- 3 eggs
- 80g sugar
- 1.5g salt
- 2g baking powder
- 60g melted butter
- 120g cake flour
- 30g almond powder
- 20g cooking oil
- Almond slices

## Instructions

1. Whisk eggs
2. Add sugar, salt and combine with eggs
3. Add baking powder and melted butter and whisk with batter
4. Sift in flour, almond powder and oil and whisk
5. Once batter is smooth transfer to loaf tray, smooth out mixture and tap the tray a few times
6. Place almond slices on top
7. Bake at 170°C for 40 minutes
