# [Magic Custard Cake](http://www.thecooksroom.com.au/magic-custard-cake/)

![Magic Custard Cake](./@imgs/recipes/magic_custard_cake.jpg)

Maybe you've already heard of this popular Custard Cake, but it was new to me.

The 'magic' of the cake is that you make only **one** custard-like batter, which then separates into **three layers** while it bakes. So you get a cake layer on top, a custard layer in the middle and another dense layer on the bottom. It has a wonderful vanilla/orange flavour and simply melts in the mouth. Absolutely yummy!

One of the keys to this recipe is to have all the ingredients at right temperature. Make sure the eggs are at room temperature, the butter is melted and cooled, and the milk is lukewarm. Also make sure you beat the egg yolk mixture well, and also the egg whites, before you combine the two.

> I do think that this cake is at its best on the day it's made. Make it into dessert with a dollop of whipped cream and some mixed berries.

## Ingredients

Serves: 10 slices
Cooking Time: 45-60 minutes

- 125g butter, melted & cooled
- 500ml milk
- 1 tsp vanilla extract or vanilla paste
- 130g pure icing sugar, sifted
- 4 x 70g eggs, at room temperature
- pinch salt
- 1 tbs caster sugar
- 115g plain flour
- finely grated zest of 1 orange (optional)
- extra icing sugar, for dusting

## Instructions

> If you've forgotten to get the eggs to room temperature, you can quickly do this by placing them in a bowl of hot tap water for 5 minutes. Egg whites at room temperature have more lift when whisked.

1. Preheat oven to 150C, fan-forced. Butter and line 20cm square cake tin with baking paper.
1. Warm the milk and vanilla in a small saucepan on the stove until it is lukewarm. Or microwave on MEDIUM for approx. 3 minutes. Set aside.
1. With a stand mixer or handheld mixer, whisk the egg yolks and the icing sugar until light and fluffy. Add the melted butter and stir until well combined.
1. Now fold the flour into the batter, and slowly add the lukewarm warm milk. It is a very liquidy batter.
1. In the bowl of a stand mixer, beat the eggwhites with a pinch of salt until they are foamy, add the caster sugar and beat until thick.
1. Gently fold in the eggwhites, one third at a time. I found it best to use a whisk for this. The whites will look a little curd-like after mixing them in, just try to mix/fold them to the point where there are no big chunks.
1. Pour the batter into the prepared cake tin and bake for between 45 - 60 minutes. (mine took 55 minutes, but it will depend on your oven) If you find the top is browning too quickly, just cover with a piece of baking paper. The cake is cooked when there is a sight 'jiggle' when you gently shake the tin. If it is still quite runny, leave it in for another few minutes.
1. Remove the tin from the oven and allow the cake to cool completely.
1. I like to trim off the edges, but it's not necessary. Cut the cake into serving portions and dust with icing sugar.
