# Traditional Jewish Cheesecake

## Ingredients

- 10 plain digestive biscuits
- 450g curd cheese
- 2 size 3 eggs
- 3 tbsp cornflour
- 1 small carton sour cream
- 55g melted butter
- 200g caster sugar

## Method

1. Grease an 8" loose-bottomed tin well, placed crushed biscuits in the bottom
1. Using an electric beater, mix the cheese, sour cream, butter and sugar
1. Beat in the cornflour slowly
1. Add the eggs and beat until smooth
1. Pour mix over the biscuits and bake in a preheated oven at 180ºC for 20 minutes
1. The cake will look loose but allow it to set and cool before removing from the tin
1. It can be served with fruit
