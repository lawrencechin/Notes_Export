# Scalloped Potatoes and Ham with Panko Crumb Crust

## Ingredients
 
* 4 tablespoons butter, divided
* 2 tablespoons flour
* 1 1/2 cups milk
* ¼ cup dehydrated minced onion
* 6 medium red potatoes, boiled and cooled
* 1 – 2 cups shredded baked ham
* 2 cups grated cheddar cheese
* Salt & pepper to taste
* 1 cup Japanese panko bread crumbs

## Directions

1. Preheat oven to 350 degrees. Butter a baking dish (or use non-stick cooking spray). In a saucepan, melt 2 tablespoons of the butter over medium high heat. Stir in flour and cook for 1 minute. Remove saucepan from heat and whisk in milk and onions. Return pan to heat and bring to a simmer while stirring. When sauce has thickened remove from heat, add the cheese and stir until the cheese has melted and sauce is creamy. Remove from the heat and stir in the ham.
2. Taste for salt (some ham will have enough salt and no additional will be needed). Peel and slice the potatoes and add half of the potatoes to the buttered casserole dish. Evenly pour half of the cheese and ham mixture over the potatoes. Add the remainder of the sliced potatoes and finish with the rest of the sauce, making sure to distribute it evenly. Season with black pepper over the top.
3. Melt the remaining butter in a small non-stick skillet and add the panko crumbs. Sauté just until the crumbs begin to brown and remove from the heat. Sprinkle the crumbs evenly over the potatoes and bake for 45 minutes at 350 degrees or until golden brown and bubbly.

Serves 4-6
