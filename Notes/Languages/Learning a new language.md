# Learning a new language
## What to Do After Duolingo: The Definitive Guide

Nov 29, 2017 | [Link](https://medium.com/@clozemaster/what-to-do-after-duolingo-the-definitive-guide-99a000731bee)

You've finished the entire Duolingo tree — congratulations\! But now you're faced with the inevitable question — what to do *after* Duolingo?

If you want to keep improving after Duolingo and get the best return on your time, you'll need to get a little creative and customize your learning experience to match your goals and needs.

**This article will give you some actionable ideas to help you build your own post-Duolingo strategy that will propel you towards fluency.**

### Where you stand after finishing a Duolingo course

![](https://cdn-images-1.medium.com/max/1600/0*nruzKyKBVTywxcIx.png)

**An average Duolingo tree teaches you about 2,000 words.** It should be more than enough to get a good sense of how the language works and hold most everyday conversations. Not bad for a free online course.

However, vocabulary is just one competency you need to navigate the language with ease.

**If you only rely on Duolingo, you won't have the chance to read long-form content, participate in actual conversations in the target language, or even write utterances of more than one sentence.**

Getting to fluency without mastering all these valuable skills (and several more) is just plain impossible.

Even the two competencies most emphasized on Duolingo — vocabulary and grammar — will still require a great deal of practice before you can become reasonably fluent. Remember all these times hover hints saved you from failing an exercise? They won't be there to help you once you move on from Duolingo.

**The key to effective learning is keeping an open mind.** There are countless ways to learn a foreign language. Why stick to just one? The skills you've been building over your entire Duolingo career have prepared you for more immersive kinds of practice. The natural next step is to give some of them a try, see what works best for you and start developing a balanced language strategy.

It doesn't have to be anything fancy. As long as you pick activities that are worthy of your time and do them regularly, you're guaranteed to see progress. It's also a good idea to change things up every once in a while to give your brain some extra novelty and stimulation.

**I'm not saying you should turn your back on Duolingo the moment you complete the last skill in the tree.** In my experience, the "Strengthen" option is one of the most underrated features on Duolingo. Try to use it to revisit specific skills that gave you a particularly hard time.

But the sooner you shift your focus to other activities, the sooner you'll be able to reap the benefits of a varied "language diet". This post will introduce you to some of the techniques, tools, and resources that will help you along the way.

### Vocabulary: How to improve your vocabulary after Duolingo

![](https://cdn-images-1.medium.com/max/1600/0*2sVsLHPut58MTHUX.png)

Finishing a Duolingo tree should give you a solid foundation of essential words and expressions. However, if you've ever tried to read an article or listen to a podcast in your target language, you've probably noticed that there are still major gaps in your vocabulary.

If your goal is to be able to effortlessly interact with the language as it is used by native speakers, you'll have to find a way to fill these gaps.

Before choosing the digital tools you'll be using to expand your vocabulary, you need to make sure they will let you take advantage of a concept known as **spaced repetition**.

Apps like Duolingo, Memrise, Clozemaster, and Quizlet all use spaced repetition algorithms to choose the best moment to put a previously learned item in front of your eyes.

And if you prefer to build your own vocabulary decks, you should look for something that will do that as well — I'll have a suggestion for you below.

#### [Use Clozemaster](https://www.clozemaster.com/)

Clozemaster lets you gamify your learning experience and learn new vocabulary by filling in the blanks (*clozes*) in sentences. You can use it for free on the [Web](https://www.clozemaster.com/), [Android](https://play.google.com/store/apps/details?id=com.clozemaster.v2) and [iOS](https://itunes.apple.com/us/app/clozemaster/id1149199075). Here are some things that set it apart from other similar solutions:

  - **The sentences you practice with are actual sentences written by native speakers** (from a [crowd-sourced collection](https://tatoeba.org/)). Because of this, they are rich in context and tend to reflect how the language is actually spoken.
  - **The order in which the words are taught is based on frequency lists instead of thematic skills.** You start with the most used words and gradually move towards less common ones. You can also choose to skip the most common words and start somewhere in the middle, which might be a good idea if you're coming from Duolingo.
  - **Clozemaster is highly customizable, so it's easy to adjust it to your needs.** You can pick the right answer from multiple options or type it in yourself (which yields more points), you can display the translation into your native language or choose to hide it, and so on.
  - **Cloze exercises let you process vocabulary really fast.** Thus, it's possible to expose yourself to a large amount of true-to-life foreign language material in a relatively short time. And yes, there's a spaced repetition algorithm built into the review sessions.
  - **The number of offered language combinations in pretty mind-boggling.** If you've studied a language pair in Duolingo, it's probably on Clozemaster too.

**My advice would be to finish Clozemaster's Fluency Fast Track.** It's intended to help you get to fluency as fast as possible.

#### [Use Memrise](https://www.memrise.com/)

Memrise is a great-looking app built around user-generated vocabulary courses. It's available on the [Web](https://www.memrise.com/), [Android](https://play.google.com/store/apps/details?id=com.memrise.android.memrisecompanion) and [iOS](https://itunes.apple.com/us/app/memrise-language-learning-app/id635966718). Here are some of its distinguishing features:

  - **The app lets you jump on the flashcard bandwagon with minimal investment of time.** Instead of building your own decks, you can use extensive courses made by other people. They are usually built around specific topics or proficiency levels, so you can still customize your experience by choosing those that appeal to you the most.
  - **Memrise uses a spaced repetition algorithm and comes with some extra features** such as automatic reminders on mobile, so you don't have to worry that you'll forget to practice your vocabulary when it's due for review.
  - **You can add your own "mems" — images, examples, etymologies, or funny mnemonics — to each of the studied items.** This is supposed to help you [form vivid memories](https://www.memrise.com/science/) by connecting new knowledge to something that's easy to remember. You can also use mems created by other users.
  - **Memrise's space-themed graphics, smooth animations, and pleasant sound effects** **help you feel that you're making progress.** The combination of all these factors puts it on par with Duolingo in terms of effective gamification design.

**I recommend trying out the official courses built by people from Memrise.** They have great audio recordings and a nice selection of useful expressions. Just look for the courses with names like "French 1", "French 2", etc. If they're available for your target language, they should be displayed somewhere near the top of the list. Having finished a Duolingo course, you should probably start with something more advanced like "French 4" or "French 5".

#### Build your own flashcard deck

Let's be honest: if you're serious about learning vocabulary, you should develop your own flashcard deck at some point.

**Setting it up takes some time and effort, but once you sort it all out, you'll actually start saving time**.

With a custom deck, you're no longer stuck with what someone else has decided to include in your lessons. You can focus on these vocabulary items which you have yourself chosen as worthy of your effort. This is especially important for someone who has finished a thematically-organized Duolingo course.

Here are some ideas to help you build your flashcard collection:

  - **First, you'll need a system to store your flashcards in.** I can recommend the [Anki](https://apps.ankiweb.net/) spaced repetition software. Though it's free, it has some really powerful features, which makes it popular among power users. That doesn't mean it's a bad choice if you only need the basics — it's quite easy to set up, and you can always choose to learn the more advanced features later on (there are many great manuals on the Web). You can sync your collection between [apps](https://apps.ankiweb.net/##download) for most desktop and mobile operating systems, as well as the [AnkiWeb](https://ankiweb.net/) client.
  - **Now that you've set up your system, you can start filling it with vocabulary.** Each time you encounter a potentially useful word or expression that you don't know, take a second to write it down. As your list grows longer, you'll need to sit down and add these items to your spaced repetition system.
  - **The obvious next step is to review your collection on a regular basis.** The system should know which items to show you and when. Your job is to find the time to study them.
  - **You probably shouldn't divide your flashcards into topics or categories.** Though it might seem like the intuitive thing to do, it's generally better to have them all in a single deck. Mixing various topics in once place results in a less predictable learning experience that forces your brain to create new connections. Plus, a single deck takes less time to maintain.
  - **Once you become more familiar with the software, you can start experimenting with the form of your flashcards.** Try creating [cloze deletion exercises](https://apps.ankiweb.net/docs/manual.html##cloze-deletion) that will require you to fill in a gap in a sentence. Add images to your flashcards to make them more visually stimulating. If some item is giving you an especially hard time, upgrade its flashcard with a third field and add explanations or examples of use.

#### General vocabulary tips

  - **Learn the common building blocks of words.** Remember that long, complex word that you've never known how to spell or pronounce? It might help if you break it up into several parts and process each of them separately. Most languages have a limited inventory of prefixes, suffixes, and roots that are commonly used to form longer words. For example, most of the letters in the Dutch word *verantwoordelijkheid* ("responsibility") are grammatical affixes. It's enough to know the very basic ones to see it as *ver* + *antwoord* + *e(n)* + *lijk*+ *heid*, with the word *antwoord* ("response") at its core.
  - **Extract most useful phrases from phrasebooks.** Phrasebooks are mostly written for tourists in need of a quick fix, but that doesn't mean you can't use them in your long-term learning. Skim through a paper or online phrasebook and look for phrases that meet two simple criteria: (1) you will probably need to use them at some point in the future, and (2) it's unlikely that you would be able to formulate them on the spot. Make sure to learn them all — they'll give your vocabulary a nice boost. If you've set up a flashcard system, it's a good idea to add them there.

### Grammar: How to improve your grammar skills after Duolingo

![](https://cdn-images-1.medium.com/max/1600/1*jFgu3jTw-Lh3Yjf-geGDYw.png)

While most Duolingo courses cover an impressive number of relevant grammar topics, they don't really teach you grammar in any explicit way outside of the brief Tips & Notes sections.

This is usually enough to get an intuitive grasp of the most basic concepts and a quick taste of the more advanced ones. **However, if you want to really *understand* the grammar, you'll probably have to do your own research**.

Here are a few ideas for activities that will help you elevate your grammar skills after completing the Duolingo tree:

#### Keep a grammar journal

Grammar is a tricky beast, but it has one major weakness: with proper research, it can be reduced to a manageable number of clear-cut topics. You can use this to your advantage and create your very own grammar journal — a place where you'll be taking apart all kinds of grammar topics and trying to make sense of them.

  - **Make a list of fundamental grammar topics that you'd like to master.** You can probably list a few off the top of your head: articles, pronouns, past tense, etc. Use Google and the materials you've been using so far (including Duolingo) to find the rest. This will be even easier if you have a grammar textbook on hand. The list is your scaffolding — use it to give structure to your grammar journal. You can keep it in a text file, a note-taking app like Evernote, or a physical notebook.
  - **Research your topics one by one and explain them to yourself in simple terms.** Find good sources and make notes in your grammar journal. Supply your notes with tables, examples, and anything else that might help you better visualize and understand the topic (it's okay to copy them from your sources). Imagine you're trying to teach the topic to someone else in a clear and concise way. Cut out all the fluff and simplify where possible.
  - **You don't have to cover each and every grammar area.** If you stumble upon some commonly discussed topic that doesn't pose much challenge to you, just skip it and move on to something more worthwhile. The same applies to these elements of grammar that don't have much use in everyday life, like structures that are going out of use or are only used in extremely formal contexts.
  - **Come back to your notes every once in a while.** You'll gain new insights into the grammar as you make progress in the language. Revisiting your notes will let you solidify them. And of course, you can keep adding new things to your journal.
  - **Consider adding grammar flashcards to your spaced repetition system.** Who said flashcards are just for vocabulary? If there's some grammar rule or exception that you have trouble remembering, turn it into a flashcard. For example, you can phrase it as a question ("What is the superlative form of *weinig*?") and add some extra explanation in the answer. Or simply make it into a cloze exercise. Give it some time, review it a few times, and it will eventually sink in.

#### Study grammar books

I know it's the most boring piece of advice ever, but hey, it works. Sure, you can learn good grammar through immersion, but it takes a lot of precious time and resources. **Structuring and scheduling practice with grammar books is much easier, and often equally effective.**

Your Duolingo course has already taught you to intuitively distinguish some of the fundamental grammar patterns. Now it's time to take a look at the big picture.

  - **Choose textbooks with many examples and exercises.** You want to have some context, not just long tables and dry explanations. If you're learning a popular language, you might have some luck finding good free textbooks online.
  - **Reading alone won't take you far.** Ideally, you want to read, then practice, then read some more. If your textbook doesn't give you enough opportunity to practice, look somewhere else. Try revisiting specific topics on Duolingo or use another language learning app.
  - **Have a routine. And don't overdo it.** I'm not sure if it's possible to learn *too much* grammar, but it's certainly possible to learn it inefficiently. That's what happens when you slog through grammar textbooks without taking time to immerse yourself in the language and apply the knowledge you've gained. Try sticking to a routine: learn grammar for a set amount of time in a day, and then move on to other competencies.
  - **Remember you can always come back later.** If some difficult topic has left you stuck, don't waste your time and energy trying to unravel it. Move on and revisit it later. Let's be realistic: you probably won't be able to gain a perfect understanding of each concept the first time you approach it.

#### General grammar tips

  - **Use online tools to check your grammar.** If you're unsure of what form a certain word should take in a specific sentence, google the phrase with an asterisk (\*) instead of the problematic word and check the results. Or, if you only have two or three variants to choose from (like *por* and *para* in Spanish, or *der*, *die*, and *das* in German), search for your phrase with each of them and compare the number of results. You can also use more specialized tools: try checking in databases of sentences like [Tatoeba](https://tatoeba.org/) or search in language corpora.
  - **Stay in touch with the grammar by using** [**Clozemaster**](https://www.clozemaster.com/)**.** Testing yourself on real sentences is the most effective way to improve your intuitive grasp of the grammar. Clozemaster makes this kind of practice much smoother and faster. And if you're having problems with some concepts, you can target them specifically with Clozemaster's Grammar Challenges (available for a number of languages, with more coming soon).
  - **If you absolutely hate studying grammar, let it go.** There's no point making your learning experience more challenging than it has to be. You'll only get discouraged. Many people have learned proper grammar through pure exposure to the language, so maybe you can too. I can't guarantee it will get you there faster than structured practice, but at least it'll be much more enjoyable.

### Reading: How to improve your reading skills after Duolingo

![](https://cdn-images-1.medium.com/max/1600/0*s6sF7S8FPDx3YINm.png)

Reading is a great way to immerse yourself in the language, get used to how it's used in writing, learn new vocabulary in context, and strengthen common grammar patterns.

**Finishing a Duolingo course might teach you how to process the written language on some basic level, but going through thousands of short and unrelated sentences doesn't really have much in common with actual reading.**

This is why you should develop your own reading practice. Many learners find it surprisingly difficult. The problem is not so much the reading process itself, but rather deciding what to read.

Here's how you can tackle it coming from Duolingo:

#### [Use Readlang](http://readlang.com/)

Readlang gives you instant translations of words and phrases you don't know so that you don't have to distract yourself from the reading to search in a dictionary. It's a bit like adding Duolingo's hover hints to your reading material. And it's free\!

I've been doing most of my foreign language reading on Readlang, and it's amazing how much time this simple service can save you.

You can upload any text from the Web or your computer and jump right into reading in your web browser. There are no mobile apps, but the web version works perfectly fine on mobile devices.

Here are a few tips to make the most of Readlang:

  - **Install the browser extension and use it to gather reading material.** Readlang's [Chrome extension](http://readlang.com/webReader) lets you import online texts into your Readlang library. While you'll have access to texts and subtitled videos added by other users, you will probably want to fill your library with content you've chosen yourself.
  - **Read away. No need to fetch the dictionary.** You can even select multiple-word phrases to display their translations. Every word you look up will be saved in your word list.
  - **Check out Readlang's flashcards — and consider moving them to your own system.** The Flashcards feature allows you to practice the words you've saved to your word list while reading. It's a great way to collect new vocabulary. Ideally, you'll want to browse your word list, choose the words you'd like to learn, and move them to your own flashcard system.

#### Choose the right reading material… and read\!

Obviously, I can't direct you to any specific content. Your choice will depend on your current skills, interests, and the language itself. I can, however, give you some hints that will help you find your perfect reading material:

  - **Make time for both intensive and extensive reading.** Intensive reading involves dissecting the text down to the very last word, analyzing its grammar and looking up the meanings of unfamiliar words. It's perfect for shorter texts that you can finish in a single reading session. Extensive reading is more about quantity: you simply try to absorb as much as possible while still having a pretty good idea of what the text is about. This makes it a good fit for longer pieces that you might want to read for pleasure.
  - **Progress from easy texts to more challenging ones.** Assessing the difficulty of a piece of text can be really tricky, and you'll probably end up choosing something that's too easy or too hard every once in a while. Try not to stress yourself too much over this. You can always just stop reading and move to the next item on your list. Here are a few types of written texts that are worth considering at a post-Duolingo level (roughly in order of increasing difficulty):
  - **Children's books.** Books for children are usually written in simple language. You shouldn't have much difficulty following the plot, but you'll still learn tons of useful words and phrases.
  - **Comics.** These are full of dialogue without all the decorative fluff, which makes them perfect for learning the language as it's spoken. Comics for children are usually the easiest, but more serious ones can work too.
  - **Wikipedia articles.** If you focus on the introductory parts (less specialized vocabulary), Wikipedia entries can be a rich source of diverse reading material.
  - **Books adapted for language learners.** These might be hard to find, but if you manage to get your hands on one of those, it will provide you with an enjoyable experience that feels surprisingly close to reading a "real" book.
  - **Books you've already read in your own language.** If you know what to expect, you can read at a higher difficulty level without getting lost. This works great if you want to ease your way into more challenging genres.
  - **Short stories, blog posts, news stories, magazines.** Once you're ready to tackle those, many new possibilities will open up. Reading across various topics will give your reading skills an extra boost.
  - **Young adult fiction.** The vocabulary and plot are usually simpler than in "adult" fiction, which makes it a good warm-up before taking on more serious novels.
  - **Extract value from your reading.** Your reading practice can be a fantastic source of valuable words and phrases. Jot down any vocabulary that is new to you, or just highlight it and make screenshots. Add it to your spaced repetition system and let the algorithm take care of the rest.

#### General reading tips

  - **Ask other learners for recommendations.** They might point you to vast libraries of free e-books, short stories, or even comics. Some learners might also be willing to share their own reading lists.
  - **Read while listening to the audiobook.** If you manage to find an e-book that comes together with an audiobook, try using both at the same time. You can also read a chapter before listening to it, or the other way around. Either way, you will deepen your understanding of the text and make new connections between the written and spoken word.
  - **Check out Cloze-Reading on** [**Clozemaster**](https://www.clozemaster.com/)**.** This smart feature makes reading more interactive by combining it with cloze exercises. Since filling in the gaps requires you to understand the context, you'll focus better on each sentence and ultimately get out more of your reading practice. As of now, you can practice with hundreds of Wikipedia article introductions in several languages.
  - **Leverage your interests.** Reading about things you're passionate about will help you learn to talk about them in your language, which is very likely to prove useful at some point. Additionally, you'll find it easier to motivate yourself to do the reading. Just search for online articles on your topic of interest and see if you can find something matching your language skills.

### Listening: How to improve your listening skills after Duolingo

![](https://cdn-images-1.medium.com/max/1600/0*qwG6rA1gIqXbzAmx.png)

If you try reading a random text after finishing a Duolingo course, you shouldn't have much trouble understanding some basic structures, and perhaps even entire sentences. However, if you try listening to native speakers speaking the language at a normal pace, you'll be lucky to distinguish a single word.

**Why is it so much more difficult to understand the spoken language?**

Don't worry, it's not your fault. Listening is always tricky, especially when you're just starting out and not living in a country where the language is spoken.

**Having completed a Duolingo tree, you've only had the chance to listen to single sentences pronounced by a text-to-speech system**. Understanding everyday speech is a much more complex task.

Here's how you can ease yourself into listening to the spoken language — and then move on to the real stuff:

#### Get used to really processing the spoken language

If you've only practiced listening on Duolingo, you're definitely not at a level where you could just listen to anything and expect to get better through mere exposure. **My advice for you is to take it slow** **and start by practicing deliberately.** Once you become more comfortable with authentic spoken language, you will be able to benefit from a more aggressive, immersion-based approach.

  - **Start with content produced specifically for learners.** Look for audio programs, podcasts, and YouTube channels that give you time to process everything that is being said, often with additional explanations in English (feel free to skip the beginner lessons though). They might get tedious after a while, but they should have done their job by then.
  - **Practice catching individual words with** [**Clozemaster**](https://www.clozemaster.com/)**'s listening exercises.** The Cloze-listening feature will play a sentence to you and prompt you to choose or type in one of the words you've just heard. It's a nice way to practice attentive listening with instant feedback.
  - **Look for short audio recordings that come with a transcript.** This way, you can choose something that is slightly above your level and still feel in control. As you get used to the natural pace of speech, you can switch to only checking the transcript if you missed something.

#### Find the right content and listen attentively

When it comes to extensive learning, not all types of content are created equal. However, before I point you to several genres that you might want to use in your learning, let me address a belief that leads many people to rely on ineffective learning methods.

**[Passive listening will hardly do anything for you.](https://www.fluentin3months.com/passive-learning/)** If you're not focusing on what is being said and actively processing it, you're not really learning. Sure, it might help you get used to the flow of the language, but that's pretty pointless without real understanding.

**At your level, 30 minutes of focused listening will teach you more than 10 hours of passive listening (or rather *hearing*).** The numbers are made-up, but they still convey the point.

Now that we've got that out of the way, let's talk about things you can *actively* listen to when working on your listening skills:

  - **Cartoons and kids' shows.** Anything from *Peppa Pig* to *Spongebob Squarepants* will do the job. It doesn't really matter if it has been produced in your target language or translated from another language. What matters is that the language is relatively simple and the visuals make it easier to figure out what is going on. If you're having trouble following the audio, looks for something with subtitles.
  - **TV shows.** These are usually much more challenging than cartoons, but if you enjoy them, it's worth a try even quite early on. You'll have many options to choose from, and there's really no reason to limit yourself to subtitled foreign television series (especially if it's hard for you to find something interesting). Watching shows that are produced/dubbed in your native language and subtitled in the foreign language can work too — just try to pay attention to the subtitles. And if you're planning to re-watch *Breaking Bad* anyway, why not do it with Spanish dubbing this time?
  - **Movies.** Even more tricky than TV shows. A two-hour movie may end before you have the chance to get comfortable with its setting and the characters' speech. This is why you should mostly choose movies that you've already seen. Since you already know the plot, you'll have an easier time focusing on the language.
  - **Podcasts.** Look for podcasts that are closely related to your hobbies and interests, or just general enough to use a lot of everyday language (news, storytelling). If you're using iTunes, a great way to find non-learner podcasts is to switch your iTunes language to the language you're learning. And if youru application of choice supports variable playback speed, you should definitely try listening at x0.9 or x0.8 speed. Also, keep in mind that some podcasts might publish episode transcripts on their websites.
  - **Audiobooks.** These can be great if you get sucked into the plot (fiction) or are really interested in the topic (non-fiction). However, be careful not to get stuck in the middle of a 50-hour long novel. No one expects you to listen to the entire thing, and changing the topic/setting every once in a while will only help you expose yourself to more diverse language. [LibriVox](https://librivox.org/) is an amazing source of free audiobooks in a number of languages.

#### General listening tips

  - **Listen while doing mindless tasks.** Running errands, doing chores, commuting, playing simple video games — all these activities leave a significant part of your mind unoccupied, which means you can sneak in some listening practice during your day. However, you should still try to listen carefully and interpret as much of what is being said as possible.
  - **Don't expect to catch all the details. Or even most of them.** At an intermediate level, you can be proud of yourself if you can make out 25% of what is being said. Listen for the gist. Your goal should be to first understand the general message and only then focus on the details.
  - **Do what feels most natural to you.** If you love listening to podcasts, make them the core of your listening practice. If you prefer to spend your evenings binge watching, focus on TV series. It will be much easier for you to get into the right mindset and make the most of the time spent listening.

### Speaking: How to improve your speaking skills after Duolingo

![](https://cdn-images-1.medium.com/max/1600/1*x65yuEIunx136Xd2vbXjgQ.png)

Speaking in a foreign language can be extremely hard, especially if you're self-taught. Even with an extensive vocabulary and excellent command of grammar, you can still have trouble holding basic conversations with native speakers.

The only effective way to develop good speaking skills is… well, speaking. Duolingo offers simple speaking exercises that are supposed to get you started with that. However, there are two major problems with them.

First, Duolingo only requires you to *pronounce* individual sentences, which is only one of several sub-skills necessary to hold a real, two-sided conversation. Second, many users completely disable speaking exercises. Perhaps they're too embarrassed to do them on the subway, or their PC simply doesn't have a microphone.

**This is how, after finishing a Duolingo course, you can find yourself in a vicious circle. Your speaking skills are inadequate, so you're too ashamed to use them, which means you're not getting any practice, which in turn makes it impossible for you to improve.**

Here's how you can break this cycle and make speaking as natural as reading or writing:

#### Practice speaking alone

The kind of practice described below can be seen as a natural extension of Duolingo's speaking exercises. It's meant to help you build confidence by simulating actual conversation, but in the long term, it won't be as effective as the real thing.

So if you don't feel ready to hold conversations yet, that's okay. Put some of the ideas below into action to build a strong foundation of speaking skills.

But if you can't wait to start speaking with people, feel free to skip to the next section. Still, I think some basic preparation wouldn't hurt.

  - **Learn to pronounce all the sounds of the language correctly.** Sure, you already have a pretty good idea of how the language should sound. However, if you don't practice this explicitly, you'll inevitably be pronouncing some of the sounds wrong, which will make your speech harder to understand. There are many resources that can help you learn correct pronunciation: textbooks, audio recordings, YouTube videos, and even charts showing the correct position of the tongue.
  - **Read aloud and evaluate yourself.** Find some audio recordings with transcripts (preferably dialogues — often included in textbooks) and read the transcript aloud. Then, carefully listen to how the native speaker pronounces the words. This practice will be much more effective if you record yourself speaking. You'll be surprised to hear how many glaring errors escape your attention when you listen to yourself in real time.
  - **Talk to yourself. Pretend you're in a conversation.** Narrate your life in the language you're learning: tell yourself stories about what you're doing and what's happening around you, or simply try describing your surroundings object by object. Holding imaginary conversations is a great way to practice as well. It works best if you do it aloud, but if that's not possible, just talk to yourself in your head.
  - **Collect useful small talk phrases and learn them.** It's not about planning the entire conversation before it happens. It's about developing an arsenal of phrases that are likely to come in handy in any casual conversation. These include all-purpose expressions you can find in most phrasebooks, but also phrases that will be more specific to you: describing your work, family, interests, home country or the reasons why you're learning the language. Learn to pronounce them clearly, commit them to memory, and be ready to use them in a conversation.

#### Talk to speaking partners

Thanks to the Internet, finding speaking partners is now easier than ever before. If you know where to look, you can find people willing to exchange languages or just chat with you. But that doesn't mean you should only look for native speakers. Talking to other learners in the language you're both learning can be surprisingly instructive.

Here are a few suggestions to help you find speaking partners and get as much as possible out of the time spent talking to them:

  - **Search in language learning communities and language exchange apps.** You might have some luck finding partners on [HelloTalk](http://hellotalk.com/), [Speaky](https://www.speaky.com/), [Interpals](https://www.interpals.net/app/langex), and [Meetup](https://www.meetup.com/find/language/) (for in-person meetings), or any of the dozens of similar online services. Once you find someone you would like to talk to, you can connect with them on Skype or via built-in voice chat.
  - **Remember there's no reason to fear being judged.** A vast majority of native speakers will admire you for taking the effort to learn their language and will be happy to talk with you, even if your speaking skills are far from perfect. If they're language learners themselves, they'll be even more supportive, as they know very well how difficult it is to overcome one's fear of speaking.
  - **Take notes during the conversation.** Jot down a few words about the expressions which escaped your memory or the grammar areas you need to practice. If your speaking partner gives you some specific feedback, make sure to write it down as well. Don't expect you will remember all this later. The excitement of the chat will make you forget the important points faster than you think.

#### General speaking tips

  - **Chat with Siri or Google Assistant.** If your voice assistant supports the language you're learning, try issuing it a few commands or even holding a conversation with it in that language (remember to change the settings first). See if you can get various bits of information out of it. Ask it about the name of the current President, the number of calories in your favorite food, the year in which a specific event took place, and so on.
  - **Repeat after native speakers in movies or audio recordings.** This kind of practice will help you improve your pronunciation, but it's also great for getting into the habit of speaking in a natural tone and rhythm. Alternatively, you can look at the subtitles/transcript and say the words before the actor says them.
  - **Find someone to text with.** Sure, audio calls are much more immersive, but it might be difficult to schedule them at regular intervals. Texting with native speakers or other learners can help you stay on top of your conversation game. Try not to overthink it too much though. Treat it as a spontaneous chat, and not an exchange of carefully planned messages.

### Writing: How to improve your writing skills after Duolingo

![](https://cdn-images-1.medium.com/max/1600/0*rAyZGwXzpzwnaji-.png)

Writing isn't just about using correct grammar and vocabulary. It's an entire competency that is essential to communication in many diverse situations.

**Connecting sentences into cohesive paragraphs, using natural expressions to make your point clear, choosing the correct forms of address in correspondence — these are all skills that you must develop to write well in a foreign language.**

Unfortunately, completing a Duolingo tree will teach you none of these things. It's just a limitation of the lesson structure — translating 15 unrelated sentences one-by-one is nothing like composing a 15-sentence paragraph.

To learn to communicate in writing effectively, you will have to take matters into your own hands. Here's how you can build your own writing practice program:

#### Set writing assignments for yourself

My guess is that you don't have a teacher who would regularly give you assignments to keep your writing skills sharp. Or an upcoming exam with a writing section which would add some motivating pressure to your learning process.

But that's not really a problem. As an ambitious learner, you can set your own writing assignments and keep yourself accountable without any outside pressure.

  - **Summarize or rewrite something you've read.** Short stories, blog posts, news articles — these are all great places to start. Feel free to stay close to the original structure, there's really no need to strive for originality. The point is to clearly restate the main point and convey the same message with other words.
  - **Practice writing specific text types.** This will help you learn to write at different levels of formality. It's a good idea to try writing pieces that are typically used to test language learners on exams: short essays, letters, reports, reviews, and so on. Think about your goals: if you'd like to use the language in your career, practice writing business e-mails. If you need it to study abroad, practice writing academic texts.
  - **Ask someone to correct your writing.** Perhaps you know a native speaker or a more advanced learner who could read your pieces and give you some honest feedback. Ask them to mark your errors and point to areas that need improvement. Then, rewrite the text and correct all the errors.

#### Write down your thoughts

Putting your thoughts on paper (or screen) is a healthy way to clear your mind, so why not try to use it as part of your language learning strategy? Get a nice notebook or simply create a text file on your computer and start jotting down your thoughts in a foreign language.

  - **Write whatever you feel like writing.** Write about your day, your opinions on various topics, your plans for the future. Make up a story or scribble down some random thoughts. As long as it's in the foreign language, you're making progress.
  - **Stick to a simple conversational style.** This is to help you get used to the natural flow of the language and prepare you for casual chats with native speakers. If you're writing about your life and thoughts, it's likely that some of these things will one day come up in a conversation.
  - **Don't force yourself to write a lot.** If you only feel like writing two sentences, that's okay. You want to make it a daily practice, and setting unrealistic expectations will only discourage you from coming back to your journal.
  - **Start a blog.** Many learners decide to write a blog in the language they're learning. It helps them get into the right mindset and makes it much easier to share their writing with other people. If you decide to give it a try, see it as an extension of your journaling routine. You're only accountable to yourself, so feel free to write as much as you want on any topic you'd like.

#### General writing tips

  - **Write simple sentences. You don't have to impress anyone.** When you use complex structures, you're much more likely to commit errors. Master the simple things first. They will work just as well (or even better) in most everyday situations.
  - **Use the dictionary as much as you want.** Using new vocabulary in a context that is meaningful to you will help you build strong memories. It doesn't matter if you first saw it in a dictionary.
  - **Dwell on your mistakes.** Mark them in red, think about why you made them, and how you can avoid making them in the future. Get rid of bad writing habits as early as possible.
  - **Use online tools to improve your writing.** If you can't find an equivalent of an expression in your own language, use [Linguee](https://www.linguee.com/) to search for it in a huge database of translations. You might also want to check out the [Reverso Context](http://context.reverso.net/) database. It's full of movie subtitle translations, so it works great if you're looking for something more colloquial. Grammar checkers like [LanguageTool](https://languagetool.org/) can be extremely useful as well: they highlight sections of your text which might contain errors, which should give you a pretty good idea of what to focus on when revising and rewriting.

### Final words

The techniques gathered in this post are just a small sample of the many opportunities that have opened up for you after you have finished a Duolingo course. You really can't go wrong with any of them — if you feel overwhelmed, pick a few things that feel most natural to you and start learning. You can always adjust your strategy as you go.

In the end, your progress on the way to language mastery will largely depend on how consistent you are in your practice. Once you develop a smart learning strategy, the most important thing you can do is ensure that you stay motivated and focused on your goal.

When feeling short on motivation, think about how you can make learning more fun. Don't cling to activities that you dislike just because someone says they are "good for you". There is no perfect formula that would work for everyone. Keep an open mind and try out some new things — you'll find your own path in no time.

#### Clozemaster — the next step after Duolingo

If you're looking for interactive language practice which would feel like a natural extension of a Duolingo course, Clozemaster is your best bet.

Clozemaster's exercises are based on a deceptively simple premise. Your task is to fill in gaps in sentences sourced from an online database. Since the sentences come from native speakers of the language, they reflect how it is actually spoken and offer more realistic context.

Features such as Grammar Challenges, Cloze-Listening, and Cloze-Reading will help you complement your learning strategy by isolating individual competencies and practicing them in short intensive sessions. [**Get started practicing one of the dozens of available languages on Clozemaster\!**](https://www.clozemaster.com/sign-up)

## [Kaspain](https://community.memrise.com/t/what-resources-do-you-use-to-learn-spanish/6456/3)
I taught myself *German* using:

1. **Memrise** - I started with the raw beginner courses, such as **Basic German**, **Hacking German**, and **Minimal Viable German**. Then I started the course for **Comprehensive German Duolingo** Vocabulary. Once I'd planted 2-4 lessons, I started using **Duolingo**. There are similar Spanish courses: [Basic Spanish][BS], [Hacking Spanish][HS], and [Minimum Viable Spanish][MVS]. One advantage to these old courses is that they've been around for years, so they have a lot of good *mems*, have had typos corrected, and have had confusing clues clarified. Edit: here's the link to [Spanish Duolingo][SD].
2. **Duolingo** - Learning the words in **Memrise** made using **Duolingo** a lot more useful and fun, since I could use it to focus grammar and sentences.
3. **Podcasts** - I found 2-3 podcasts aimed at complete beginners. **GermanPod101.com** is one. There is a Spanish version— [SpanishPod101.com][SP101]. There's also **Coffee Break German** and [Coffee Break Spanish][CBS]. Both **SpanishPod101** and **Coffee Break Spanish** give you several episodes for free, and offer more options if you pay. I aimed to spend 10-30 minutes listening 3-5 times per week.
4. **Deutsch Interaktiv** - This German course by the radio company **DeutscheWelle** aims to be a fairly comprehensive beginner language course. I'm not aware of a parallel in Spanish, but [http://studyspanish.com/grammar4][SSG] is quite useful. When I was focused on improving my Spanish, it was my go-to resource for grammar questions. You might also try the Spanish courses by the [Göthe Institute][GI] and the Spanish courses by the [BBC4][BBC]. (Disclaimer: I have not tried either of these...)

 No longer beginner... moving up to intermediate:

1. More **Memrise** - After completing some beginner courses, I added more **Memrise** courses. In my case, that was **Deutsch für Euch**, **1000 Words of Elementary German**, **German A1**, **German A2**, and **5000 Words** sorted by frequency. You might like [Comprehensive Spanish Vocab][CSV], [501 Spanish Verbs][5SV], and [First 5000 Words of Spanish][F5WS] (*I have completed all three of these courses*).
2. Classroom - After studying German on the internet for 9-10 months, I spent a month in Berlin in a German course for 3 hours per day, 5 days per week.
3. More **Memrise** & more **Duolingo**.
4. [Clozemaster][C] is great for seeing vocabulary in context. It presents you with sentences that are missing one word. You fill in the blank, either multiple choice or typing (you choose).
5. Speaking - The biggest gap in my self-taught German is speaking. The classroom experience helped, but what really made the difference was spending time this summer with Germans that don't speak English! I visited friends whose parents only speak German, and my ability to speak skyrocketed. If I were to do it all over again, I might use **iTalki** or some other language exchange website to practice speaking much sooner.

## [Resource Review by Rayxi](https://news.ycombinator.com/item?id=16822888)

I've been trying a few of these lately. I passed French DELF A2 late last year and going for B1 in a week.
My chronological progression of picking up French via apps/courses (lots of trial-and-error):

1. [Duolingo][D]
2. Michel Thomas audio course - this has its critics but it gave me a much more solid, if limited, foundation on grammar.
3. [Lingvist][L] - I was lucky to find this early. Added around 2000 words to my vocabulary (yes they claimed ~5000 - that was inaccurate; conjugations and plurals are counted separately). Today you need to pay $23 / month to get the same number of words.
4. [Clozemaster][C] - it offers a lot more words, so I got a paid subscription. Got disappointed really fast though because its automated method to offer mass sentences really shows its flaws quickly: there are many errors and nonsensical cloze placement. I got tired of flagging.
5. [Assimil][A] - seems good, but without any gamification or anything I can play on my phone while waiting, commuting, etc., this soon dropped off the radar.
6. [Speechling][S] - this one should not have the problem of Clozemaster or Duolingo, because they have a human teacher correcting your speech. I imagine their sentences are more carefully curated as well. I started the trial, but dropped after a while because I found I just couldn't allocate the time to sit in a quiet place and record myself for an extended amount of time. Yes, it's a genuine "it's not you, it's me" thing. I really like their recent features and will probably try this again during a less hectic life period. Hongyu (the CEO) is super responsive too, which is great.
7. [Glossika][G] - this is what I'm currently on. It's not perfect; their main thing is you hear someone saying X in English and a moment later the equivalent in French. They offer mass repetitions as well, with the same class of problems: occasional inaccurate translations. The reason I picked this one instead of Speechling is not quality: it allows me to listen to mass sentences while doing mindless boring stuff that I have to do anyway (e.g.: doing the dishes, walking to/from the subway station, etc.). This allows me to get French exposure every day, even if not perfect, which ends up meaning more exposure than what Speechling can give me.
8. I'm a subscriber of one more app, I can't believe I forgot to add this one the first time: [Kwiziq][K]. They have a very specific target: teach you grammar from A0 (i.e.: the level below A1) to C1. My main problem is that they don't have an "aging" feature. Something I learned fully in A1 9 months ago will be forever marked as "mastered", despite my having forgotten it completely. Nevertheless I continue to be a happy subscriber. It's fun to finish a quick grammar quiz here and there as you're waiting for food, queueing for stuff, etc.

[LANL]: https://community.memrise.com/t/what-resources-do-you-use-to-learn-spanish/6456/3 "Learning a new language"
[BS]: http://www.memrise.com/course/98959/basic-spanish-spain/ "Basic Spanish"
[HS]: http://www.memrise.com/course/1157/hacking-spanish/ "Hacking Spanish"
[MVS]: http://www.memrise.com/course/42/minimum-viable-spanish/ "Minimum Viable Spanish"
[SD]: http://www.memrise.com/course/114794/spanish-duolingo/ "Duolingo Spanish"
[SP101]: SpanishPod101.com "Spanish Pod 101"
[CBS]: http://radiolingua.com/tag/cbs-season-1/ "Coffee Break Spanish"
[SSG]: http://studyspanish.com/grammar "Study Spanish Grammar"
[GI]: http://www.goethe-verlag.com/book2/EN/ENES/ENES002.HTM "Göthe Institute"
[BBC]: http://www.bbc.co.uk/schools/gcsebitesize/spanish/ "BBC bytesize courses"
[CSV]: http://www.memrise.com/course/8350/comprehensive-spanish-vocab/ "Comprehensive Spanish Vocab"
[5SV]: http://www.memrise.com/course/65362/501-spanish-verbs-2/ "501 Spanish Verbs"
[F5WS]: http://www.memrise.com/course/1288/first-5000-words-of-spanish/ "First 5000 words of Spanish Verbs"
[C]: https://www.clozemaster.com/ "Clozemaster"
[D]: https://www.duolingo.com
[L]: https://lingvist.com
[A]: http://fr.assimil.com
[S]: https://speechling.com
[G]: https://ai.glossika.com
[K]: https://www.kwiziq.com
