# Conversation and Expressions

## Question Words

* Who | Quién
* What | Qué
* When | Cuándo
* Where | Dónde
* Why | Por qué
* How | Cómo
* Which | Cuál
* Which ones | Cuáles
* From where | De dónde
* Why, for what | Para qué
* How much | Cuánto
* How many | Cuántos
* To where | Adónde
* To whom | A quién
* From where | De dónde
* With whom | Con quién

## Introductory Conversation

* Hello/Hi | Hola
* How | Cómo
* You are (*temporarily*) | Estás
* How are you? | ¿Cómo estás?
* How are you? | Que tal?
* I am (*temporarily*) | (Yo) Estoy
* I am (*permanently*) | (Yo) Soy
* Well/good | Bien
* Very | Muy
* Very good | Muy bien
* I | Yo 
* You (*informal*) | Tú
* You (*formal*) | Usted
* You call, to you (*singular, informal*) | Te
* And you? | ¿Y tu?
* And you (*formal*) | Y usted
* Thank you | Gracias
* Please | Por favor
* You call (*singular, informal*) | Llamas
* What is your name? (*informal*) | ¿Cómo te llamas?
* What is your name? (*formal*) | ¿Cómo se llama usted?
* You are (*permanently, singular informal*) | (Tú) Eres
* You are a genius! | ¡Eres un genio!
* Not well/terrible | Fatal
* I'm very good | Yo muy bien
* I am very well | Estoy muy bien
* Stupendous | Estupendo
* Phenomenal | Fenomenal
* Splendid | Espléndido
* Ok | Regular
* Where are you from? (*informal*) | ¿De dónde eres?
* Where are you from? (*formal*) | ¿De dónde es usted?
* I'm from Scotland | Soy de Escocia
* United States | Los Estados Unidos
* England | Inglaterra
* English | Inglés
* American | Americano
* I am English (*masculine*) | Soy Inglés
* I am English (*feminine*) | Soy Inglesa
* Are you Spanish? | ¿Eres español?
* I am Spanish (*feminine*) | Soy española
* I am not American (*feminine*) | No soy americana
* Are you from Scotland? | ¿Eres de Escocia? 
* No, I'm not from Scotland | No, no soy de Escocia
* Are you from Spain? (*formal*) | Es usted de España?
* I live in Glasgow | Vivo en Glasgow
* Where do you live? (*informal*) | ¿Dónde vives?
* Where do you live? (*formal*) | ¿Dónde vive usted?
* I am originally from London, but now I live in St. Albans | Soy de London, pero ahora vivo en St. Albans
* Of course… | Claro que
* Do you speak Spanish? | ¿Hablas español?
* I speak a little Spanish | Hablo un poco de español
* I speak a little English | Hablo un poco de inglés
* Of course I speak English | Claro que hablo inglés
* You speak Spanish very well | Hablas español muy bien
* What's going on? Everything is fine | ¿Qué pasa? Todo está bien
* Is your food good? Yes, it's very tasty! | ¿Está bien su comida Sí, ¡qué rico!

## Introductory Phrases

* Good morning | Buenos días
* Good afternoon | Buenas tardes
* Good night | Buenas noches
* See you later | ¡Hasta luego!
* See you soon | ¡Hasta pronto!
* See you tomorrow | Hasta mañana
* Goodbye | Adiós
* Myself, to me | Me
* I call | Llamo
* My name is | Me llamo
* My name is Lawrence | Me llamo Lawrence
* Nice to meet you | Mucho gusto
* Pleased to meet you (*masculine*) | Encantado
* Pleased to meet you (*feminine*) | Encantada
* Great! | ¡Grandioso!
* I am sorry | Lo siento
* Cheers | Salud
* What’s up? | ¿Qué pasa?
* You understand | Entiendes
* Do you understand? | ¿Entiendes?
* I don’t understand | No entiendo
* Me too | Yo también
* Me neither | Yo tampoco
* Where is…? | ¿Dónde está?
* Where is the hospital? | ¿Dónde está el hospital?
* Here | Aquí
* Here it is | Aquí está
* Of course! | ¡Claro!
* Excuse me | Disculpe
* How terrible! | ¡Qué terrible!
* You are welcome | De nada
* What a shame | Qué lastima
* What are you doing? | ¿Qué haces?
* That’s enough! | ¡Basta!
* How do you say … in Spanish? | ¿Cómo se dice … en español?
* How can I? | ¿Cómo puedo?
* Can you show me (*formal*)? | ¿Puede mostrarme?
* Not now, thank you | Ahora no, gracias
* Take/throw a look | Echa un vistazo

## Formal and Informal Language

* Mister | Señor
* Missus | Señora
* The cinema | El cine
* Or what | O qué
* To come | Venir
* To help | Ayudar
* With me | Conmigo
* The pizza | La pizza
* You are a really good teacher Mr. Garcia | Es muy buen maestro Sr. García
* Do you want to go to the museum Mrs. Diaz? | ¿Quiere ir al museo Sra. Diaz?
* Would you like any desserts? | ¿Les gustaría algo de postre?
* Do you want to go to the cinema or what? | ¿Quieres venir al cine o qué?
* You cannot help me Dr. López | No me puede ayudar doctor López
* Would you like to buy a new bag? | ¿Le gustaría comprar un bolso nuevo?
* You can watch television and eat pizza with me | Puedes ver la televisión y comer pizza conmigo
* Do you want to hang out later today? | ¿Quieres salir más tarde hoy?
* To take, to bring | Llevar
* Can you take me to this hotel please? | ¿Puede llevarme a este hotel por favor?
* The singer | El cantante
* You are a terrible singer | Eres un cantante terrible
* Sweetheart | Cariño
* The phone | El teléfono
* Do you need a new phone sweetheart? | ¿Necesitas un teléfono nuevo cariño?
* What would you like to do tomorrow Sra. López? | ¿Qué le gustaría hacer mañana Sra. López?
* Aid, help | La ayuda
* Consecutive | Seguido
* Although | Aunque
* Greater, older | Mayor
* Minor, lesser, younger | Menor
* Several | Varios

## Idioms and Expressions

* The seven deadly sins are: pride, envy, greed, anger, lust, gluttony and sloth | Los siete pecados capitales son: la vanidad, la envidia, la avaricia, la ira, la lujuria, la gula y la pereza
* Maths is a piece of cake (*Maths is eaten bread*) | Las Matemáticas son pan comido
* The face | La cara
* It costs an arm and a leg (*It costs an eye of the face*) | Cuesta un ojo de la cara
* Break a leg! (*Much shit*) | ¡Mucha mierda!
* The envelope | El sobre
* Hit the hay (*Go oneself to the envelope*) | Irse al sobre
* He just wants to hit the hay | Solo quiere irse al sobre
* The tongue | La lengua
* Let the cat out of the bag (*Go oneself of the tongue*) | Irse de la lengua
* The nail | El clavo
* You gave | Diste
* You hit the nail on the head (*You gave on the nail*) | Diste en el clavo
* The million | El millón
* Not in a million years | ¡Ni en un millón de años!
* It is raining cats and dogs (*It is raining to the seas*) | Está lloviendo a mares
* To grin | Sonreír
* She grinned from ear to ear | Sonrió de oreja a oreja
* The rose | La rosa
* Life is a bowl of cherries (*life is a bed of roses*) | La vida es un lecho de rosas
* The heaven | El cielo
* I was in seventh heaven | Estaba en el séptimo cielo
* The box | La caja
* To break (*a thing*) | Partir
* I broke (*something on me*) | Me partí
* I was in stitches (*I broke the box*) | Me partí la caja 
* Thrilled | Entusiasmado
* I was thrilled | Estaba entusiasmado
* That's cool, I find it cool (*Spanish, coloq*) | Me mola
* I had a great time (*Spanish, coloq*) | Lo pasé guay
* Wow, oh no (*Spanish, coloq*) | Hostia
* It's very cool (*Spanish, coloq*) | Es la hostia
* A lot (*Spanish, coloq*) | Mogollón
* OK (*Spanish, coloq*) | Vale
* Terrible (*Spanish, coloq*) | Me cago en la leche
* Dude (*Spanish, coloq*) | Tío
* You're having a laugh! (*Spanish, coloq*) | Qué va tío
* You've got to watch out for that guy (*Spanish, coloq*) | Ese tío es de abrigo
* Dickhead, really stupid (*Spanish, coloq*) | Gilipollas
* What's up? (*Méxican, coloq*) | ¿Qué onda?
* Way cool, that's great (*Méxican, coloq*) | ¿Qué buena onda?
* Funny, comical (*Méxican, coloq*) | Chistoso
* Cool (*Méxican, coloq*) | Chido
* Oh no! (*Informal, Méxican, coloq*) | Chale
* Wow (*Méxican, coloq*) | Órale
* Oh well (*Méxican, coloq*) | Ni modo
* Dude (*Méxican, coloq*) | Carnal
* Next time (*Méxican, coloq*) | Ahí para la otra
* Alright them (*Méxican, coloq*) | Sale
* That's terrible (*Méxican, coloq*) | Qué mala onda
* You're have a laugh (*Méxican, coloq*) | No inventes
* Stupid (*Méxican, coloq*) | Menso
* Right now (*coloq*) | Ahorita
* Forget it (*coloq*) | Olvídalo
* Hello (*coloq, telephone*) | Bueno
* Hurry up (*coloq*) | Apúrate
* Hey (*coloq*) | Oye
* Good idea | Buena idea
* Actually, in fact (*coloq*) | De hecho
* Really? (*coloq*) | ¿En serio?

## Topics of Conversation

* The conversation | La conversación
* About | Sobre
* About, approximately | Aproximadamente
* The weather, the time | El tiempo
* We can always talk about the weather | Siempre podemos hablar sobre el tiempo
* The politics | La política
* We can always talk about politics | Siempre podemos hablar sobre política
* French | Francés
* Talking | Hablar, hablando
* The French | Los franceses
* The French love talking about food | Los franceses les encanta hablar sobre comida
* The young people | Los jóvenes
* Young people love talking about films and music | A los jóvenes les encanta hablar sobre las películas y la música
* The health | La salud
* Do you want to talk your health? | ¿Quieres hablar sobre tu salud?
* The goal (*in life*) | El objetivo
* The love life |  La vida amorosa
* I don't want to talk about my love life | No quiero hablar sobre mi vida amorosa
* The relationship | La relación
* He doesn't want to talk about our relationship | No quiere hablar sobre nuestra relación
* To tell | Contar
* Tell me (*a command*) | Cuéntame
* The interest | El interés
* Tell me about your interests | Háblame sobre tus intereses
* Interesting | Interesante
* Tell me about your goals | Háblame sobre tus objetivos
* The punishment | El castigo
* The crime | El crimen
* The topic | El tema
* Crime and punishment are always interesting topics | Los crímenes y los castigos son siempre temas interesantes
* (*The*) sport | Los deportes
* (*The*) culture | La cultura
* Sports and culture are always exciting topics | Los deportes y la cultura son siempre temas emocionantes 
* (*Something*) angers (*someone*) | …enfada…
* Talking about politics makes me angry | Me enfada hablar sobre política
* (*The*) science | La ciencia
* (*The*) technology | La tecnología
* (*something*) excites (*someone*) | …apasiona…
* Talking about technology makes me excited | Me apasiona hablar sobre tecnología
* The embarrassment | La vergüenza
* To embarrass |  Dar vergüenza
* Talking about his relationships makes him embarrassed | Le da vergüenza hablar sobre sus relaciones  
* My dad doesn't know anything about technology | Mi padre no sabe nada de tecnología
* The argument (*a fight*) | La pelea
* The holiday | Las vacaciones
* Let's talk about the argument | Vamos a hablar sobre la pelea
* I don't want to talk about my health | No quiero hablar sobre mi salud
* I don't know anything about sport | No sé nada sobre deportes
* Let's tell them about our holiday | Vamos a contarles sobre nuestras vacaciones
* Politics is always an interesting topic | La política siempre es un tema interesante
* The grandparents | Los abuelos
* Let's talk about French culture | Vamos a hablar sobre cultura francesa
* (*Something*) confuses (*someone*) | …desconcierta…
* My grandparents don't know anything about science | Mi abuelos no saben nada de ciencia
* The English | Los ingleses
* The English love talking about the weather | A los ingleses les encanta sobre el tiempo

## Useful Phrases

* Wait a minute | Espera un minuto
* Oh no! | ¡Oh no!
* Oh dear | ¡Vaya!
* Thank God | ¡Gracias a Dios!
* In your face | ¡En los morros!
* I guess so | Supongo
* No problem | Sin problema
* You are right | Tienes razón
* Whatever | Lo que sea
* What is the point? | ¿Para qué?
* Come on! | ¡Venga!
* I don't mind | No me importa
* Don't worry | No te preocupes
* Calm down | Cálmate
* Oh my god! | ¡Oh Dios mío!
* Can you imagine? | ¿Te imaginas?
* Seriously? | ¿En serio?
* Be quiet | Cállate
* Sick! | ¡Leches!
* Unbelievable! | ¡Increíble!
* Do you know what I mean? | ¿Me entiendes?
* That sucks! | ¡Qué mierda!
* Hold on (*wait up*) | Espera
* Hold on (*hang in there*) | Aguanta
* I see | Ya veo
* Damn! | ¡Maldita sea!
* One time | Una vez
* Many times | Muchas veces
* Two times | Dos veces
* Sometimes | Unas veces
* Another time | Otra vez
* Maybe | Tal vez

## Tongue Twisters

* Tongue twisters | Los trabalenguas
* My mom spoils me a lot | Mi mamá me mima mucho
* Joe Freckles chops potatoes with a pick, with a pick Joe Freckles chops potatoes | Pepe Pecas pica papas con un pico, con un pico pica papas Pepe Pecas
* Buddy, buy me a coconut. Buddy, I don't buy coconuts because a person who eats few coconuts buys few coconuts. Since I eat few coconuts, I buy few coconuts | Compadre, cómprame un coco. Compadre, coco no compro porque el que poco coco come, poco coco compra. Como poco coco como, poco coco compro.
* Today is already yesterday and yesterday is already today. The day has arrived, and today is today | Hoy ya es ayer y ayer ya es hoy. Ya llegó el día, y hoy es hoy
* Silly Yañez eats yams in the morning with the boy | Ñoño Yáñez come ñame en las mañanas con el niño
* If the servant that serves you doesn't serve you as a servant, what good does it do to have a servant that doesn't serve? | Si la sierva que te sirve no te sirve como sierva, ¿de qué sirve que te sirvas de una sierva que no sirve?
* R and r guitar, r and r barrel. The carts roll along quickly, laden with sugar from the train | Erre con erre guitarra, erre con erre barril. Rápido corren los carros, cargados de azúcar del ferrocarril
* I've heard a saying which they say I said. That saying is misquoted; if I had said it, it would be better said than the saying which they say I said | Me han dicho un dicho que han dicho que he dicho yo. Ese dicho está mal dicho; si lo hubiera dicho yo, estaría mejor dicho que el dicho que han dicho que he dicho yo
* Three sad tigers were eating wheat in a field. One tiger, two tigers, three tigers were eating in a wheat field. Which tiger ate more? They all ate the same amount. | Tres tristes tigres tragaban trigo en un trigal. Un tigre, dos tigres, tres tigres tragaban en un trigal. ¿Cuál tigre tragaba más? Todos tragaban igual
* The wine arrived, but the wine didn't arrive as wine; the wine arrived as vinegar | El vino vino, pero el vino no vino vino; el vino vino vinagre
* Juan had a tube, and the tube he had broke, and to get back the tube that he had, he had to buy a tube just like the tube that he had | Juan tuvo un tubo, y el tubo que tuvo se le rompió, y para recuperar el tubo que tuvo, tuvo que comprar un tubo igual al tubo que tuvo
* Pancha irons with four irons. With how many irons does Pancha iron? | Pancha plancha con cuatro planchas. ¿Con cuántas planchas Pancha plancha?
* When you tell stories, say how many stories you tell when you tell stories | Cuando cuentes cuentos, cuenta cuantos cuentos cuentas cuando cuentes cuentos
* The volcano of Parangaricutirimicuaro wants to "desparangaricutiriguarize" and whoever "desparangaricutiriguarizes" it will be a good "desparangaricutiriguarizer." | El volcán de Parangaricutirimícuaro se quiere desparangaricutiriguarízar, y él que lo desparangaricutiricuarízare será un buen desparangaricutirimízador

## Michel Thomas Words & Phrases

* It is not for you, it is for me | No es para usted, es para mí
* Why isn't it acceptable for you? | ¿Por qué no es aceptable para usted?
* It is like that | Es así
* It is not possible that way | No es posible así
* I'm sorry but it isn't possible for me that way | Lo siento pero no es posible para mí así
* I want it but I don't need it now | Lo quiero pero no lo necesito ahora
* I need it now, it is very urgent | Lo necesito ahora, es muy urgente
* Why don't you have it for me now because I need it now? | ¿Por qué no lo tiene para mí ahora porque lo necesito ahora? 
* I want to eat something now because I am hungry | Quiero comer algo ahora porque tengo hambre
* I'm sorry, but I don't have it and I don't want it because I don't need it now | Lo siento, pero no lo tengo y no lo quiero porque no lo necesito ahora
* On the contrary | Al contrario
* I want to know why you can't do it that way | Quiero saber por qué no puede hacerlo así 
* Do you have a preference? | ¿Tiene una preferencia?
* What preference do you have? | ¿Qué preferencia tiene?
* For what restaurant do you have a preference? | ¿Para qué restaurante tiene una preferencia?
* Do you have a reservation for me tonight? | ¿Tiene una reservación para mí para esta noche?
* Do you have the confirmation of the reservation for me for tonight? | ¿Tiene la confirmación de la reservación para mí para esta noche?
* What impression do you have of the situation? | ¿Qué impresión tiene de la situación?
* Why can't you make a reservation for me? | ¿Por qué no puede hacer una reservación para mí?
* What impression do you have of the political and economic situation in Spain now? | ¿Qué impresión tiene de la situación política y económica en España ahora?
* I'm not going to buy it because it is very expensive | No voy a comprarlo porque es muy caro
* I am going to be here later | Voy a estar aquí más tarde
* I'm not going to do it now because I'm going to be very busy today | No voy a hacerlo ahora porque voy a estar muy ocupado hoy
* At what time are you going to be here tonight (*form.*)? | ¿A qué hora va a estar aquí esta noche?
* At what time are you going to be ready (*form., masc.*)? | ¿A qué hora va a estar listo?
* I want to know at what time it is going to be ready because I need it and I have to have it today if it is possible? | ¿Quiero saber a qué hora va a estar listo porque lo necesito y tengo que tenerlo si es posible?
* Will you tell me when it is going to be ready? | ¿Puede decirme cuando va a estar listo?
* Nothing is ready for you today but everything is going to be ready tomorrow | Nada va a estar listo para usted hoy pero todo va a estar listo mañana
* I want very much to accept the condition but I'm sorry, I cannot accept it because it is not acceptable for me that way | Quiero mucho aceptar la condición pero lo siento, no puedo aceptarlo porque no es aceptable para mí así
* Can you come and see it with me tonight? | ¿Puedes venir a verlo conmigo esta noche?
* I'm sorry but I cannot see you today because I am going to be very busy | Lo siento pero no puedo verle hoy porque voy a estar muy ocupado
* Can you tell me where it is because I cannot find it? | ¿Puede decirme donde está porque no puedo encontrarlo?
* I like to go and see it | Me gusta ir a verlo
* What are you writing? | ¿Qué escribe?
* What are you (*in the process of*) writing? | Qué está escribiendo?
* I'm sorry but I don't understand what you're saying | Lo siento pero no comprendo lo que dice
* I don't know what you want | No sé lo que quiere
* It is not what I want | No es lo que quiero
* I don't understand very well what you mean (lit. what you want to say) | No comprendo muy bien lo que quiere decir
* You don't understand what I mean | No comprende lo que quiero decir
* Because that's not what I mean | Porque no es lo que quiero decir
* Who speaks English here? | ¿Quién habla inglés aquí?
* Nobody speaks English here | Nadie habla inglés aquí 
* Everybody speaks Spanish here | Todo el mundo habla español aquí
* I'm not selling it because I don't want to sell it | No lo vendo porque no quiero venderlo 
* I don't know how long I am staying | No sé cuanto tiempo me quedo
* I don't know how long I am going to stay | No sé cuanto tiempo voy a quedarme
* I don't know how long I can stay | No sé cuanto tiempo puedo quedarme
* A few days | Unos días
* We are going to stay here a few days | Vamos a quedarnos unos días
* What is there? What is going on? | ¿Qué hay?
* A little more | Todavía un poco
* I don't know yet, still I don't know | Todavía no sé
* I don't know yet how long I am going to stay | Todavía no sé cuanto tiempo voy a quedarme
* To send | Mandar
* He is sending it | Lo manda
* (*with "it - to me", the personal word comes first*)
* He is sending it to me | Me lo manda
* He is not sending it to me today but he will send it to me tomorrow | No me lo manda hoy pero me lo mandará mañana
* (*both words can be hooked onto the end of the "whole" verb*)
* He is going to send it to me tomorrow | Va a mandármelo mañana
* He is sending it to me tomorrow | Me lo manda mañana
* He wouldn't send it to me today | No me lo mandaría hoy
* (*"it - to you" is not le lo but se lo*)
* I am sending it to you | Se lo mando
* I am going to send it to you | Voy a mandárselo
* (*se lo can mean "it - to you", or "it to him", "it to her", "it to them", "it to all of you".  If the context is not clear, you could clarify with voy a mandárselo a usted, or voy a mandárselo a él etc*)
* Will you send it to me? | Puede mandármelo?
* Can you send it to him? | Puede mandárselo (*a él*)?
* I would like, I may want, I might want | Quisiera
* I would like to see it, I might want to see it | Quisiera verlo
* It would please me to see it | Me gustaría verlo
* It won't be necessary | No será necesario
* It wouldn't be necessary | No sería necesario
