# Adverbs

* Here | Aquí
* Here | Acá
* There | Allí
* There | Allá
* Near | Cerca
* About (*on the subject of*) | Acerca
* Under | Bajo
* According to | Según
* Towards | Hacia
* Far | Lejos
* In front of | Delante
* Behind | Detrás
* Behind | Atrás
* On top | Encima
* Underneath | Debajo
* Inside | Dentro
* Outside | Fuera
* Ahead | Adelante
* Up, above | Arriba
* Down, below | Abajo
* Inside | Adentro
* Outside | Afuera
* Today | Hoy
* Now | Ahora
* Tomorrow | Mañana
* Yesterday | Ayer
* Last night | Anoche
* The day before yesterday | Anteayer
* The night before last | Anteanoche
* Previously (*p…*) | Previamente
* Recently | Recientemente
* Currently | Actualmente
* Briefly | Brevemente
* Permanently | Permanentemente
* Daily | Diariamente
* Frequently | Frecuentemente
* Still | Todavía
* Already | Ya
* Never | Nunca
* Always | Siempre
* Occasionally | Ocasionalmente
* Weekly | Semanalmente
* Very | Muy
* Too much | Demasiado
* A lot | Mucho
* A little | Poco
* Less | Menos
* More | Más
* So much | Tanto
* Enough | Bastante
* Somewhat | Algo
* Not at all | Nada
* Well | Bien
* Badly | Mal
* Better | Mejor
* Worse | Peor
* Fast, quickly | Rápido
* Slowly | Despacio
* Clearly | Claramente
* Intelligently | Inteligentemente
* Beautifully | Bonito
* Carefully | Cuidadosamente
* Loudly | Alto
* Quietly | Quieto
* Sweetly | Dulcemente
* Stupendously | Estupendamente
* Seriously | Seriamente
* Voluntarily | Voluntariamente
* Like this, this way | Así
* Easily | Fácilmente
* Slowly (*l…*) | Lentamente
* Quickly | Rápidamente
* He kissed me sweetly | Me besó de manera dulce
* I completed the exam easily | Hice el examen de manera fácil
* I ran quickly to escape the dogs | Corrí de modo rápido para escaparme de los perros
* I wrote romantically to win her heart | Escribe de modo romántico para ganar su corazón
* Me besó con dulzura
* I completed the exam easily | Hice el examen con facilidad 
* Blindly | A ciegas
* Sometimes | A veces
* In the end | Al final
* On time | A tiempo
* During the day | De día 
* At night | De noche
* Really, truly | De verdad
* Finally | Por fin
* Carelessly | Sin cuidado
* Carefully | Con cuidado
* Perfectly (*expression*) | A la perfección
* At the same time | A la vez
* In the beginning | Al principio
* Once and for all | De una vez por todas
* Daily | A diario
* Often | A menudo
* Immediately | De inmediato
* Once again | De nuevo
* Really, seriously | En serio
* Really | En realidad
* All over the place | A diestra y siniestra
* Secretly | A escondidas
* At dusk/at sunset | Al anochecer
* At dawn | Al amanecer
* In the dark | A oscuras
* Fashionably | A la moda
* At home | En casa
* Frequently | Con frecuencia
* Happily | Con alegría
* Sadly | Con tristeza
* Willingly | De buena gana
* Unwillingly | De mala gana
* Suddenly | De repente
* Suddenly | De pronto
* Unfortunately | Por desgracia
* Luckily | Por suerte
* By heart | De memoria
* Quickly | De prisa
* Meanwhile | Mientras
* But | Sino
* Absolutely | Absolutamente
* Forward | Adelante
* Besides | Además
* Around | Alrededor
* Previously | Anteriormente
* Before | Antes
* Hardly | Apenas
* Approximately | Aproximadamente
* Like that | Así
* Still (*a…*) | Aún
* Definitely | Definitivamente
* After | Después
* Then (*e…*) | Entonces
* Especially | Especialmente
* Exactly | Exactamente
* General | General
* Usually | Generalmente
* Equally, likewise | Igualmente
* Immediately | Inmediatamente
* Never (*j…*) | Jamás
* Then, later | Luego
* Naturally | Naturalmente
* Necessarily | Necesariamente
* Normally | Normalmente
* Again | Nuevamente
* Perfectly | Perfectamente
* Practically | Prácticamente
* Principally | Principalmente
* Perhaps | Quizá
* Relatively | Relativamente
* Certainly | Seguramente
* Simply | Simplemente
* Only, just (*s…*) | Solamente
* Of course (*alt*) | Por supuesto
* Neither | Tampoco
* So, as (*much*) | Tan
* Totally | Totalmente
* Only (*solely*) | Únicamente
* Frankly | Francamente
* On the contrary | Al contrario
* Sinking | Hundiendo
