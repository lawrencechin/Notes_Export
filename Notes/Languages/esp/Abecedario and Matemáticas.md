# [Abecedario y Matemáticas](https://www.spanishdict.com/guide/the-spanish-alphabet)

> Quick Answer
> The Spanish alphabet, or **abecedario**in Spanish, is composed of 27 letters. It includes one letter, **la letra ñ**, that we don't have in English.

![Abecedario](./@imgs/abecedario.jpg)

## What's in a (Letter) Name?

The majority of the letters in Spanish have their own special names (some even have more than one!) and people use them all the time when spelling out words.

Below you'll find all 27 letters of the **abecedario** and their names, along with an example word for each.

Letter | Spanish Name (*s*) | Example Word
-- | -- | --
a | a | armadillo (*armadillo*)
b | be or be larga or be alta | biblioteca (*library*)
c | ce | carcajada (*loud laugh*)
d | de | decidir (*to decide*)
e | e | elefante (*elephant*)
f | efe | falsificar (*to forge*)
g | ge | gigante (*giant*)
h | hache | hechizo (*spell*)
i | i or i latina | iniciar (*to begin*)
j | jota | jajajear (*to laugh*)
k | ka | kaki (*khaki*)
l | ele | labial (*lipstick*)
m | eme | mamá(mom)
n | ene | nene (*baby*)
ñ | eñe | ñoño (*weakling*)
o | o | coco (*coconut*)
p | pe | papá(dad)
q | cu | quiquiriquí (*cock-a-doodle-doo*)
r | erre | ronronear (*to purr*)
s | ese | sisear (*to hiss*)
t | te | tetera (*teapot*)
u | u | ulular (*to hoot*)
v | uve or ve corta or ve chica or ve baja | vivir (*to live*)
w | uve doble or doble uve or doble ve or doble u | wifi (*Wi-Fi*)
x | equis | sexto (*sixth*)
y | ye or i griega | yoyó (*yoyo*)
z | zeta | zarzamora (*blackberry*)

## The Company You Keep Matters

While the majority of the letters in Spanish are always pronounced the same way, there are a few whose pronunciation changes depending on the letters with which they combine. Let's take a look at some of the trickier combinations.

### Ge Before a Vowel

When **ge** comes before **i** or **e**, it's pronounced like a raspy English *h*.

- gente (*people*)
- Gibraltar (*Gibraltar*)

Before other vowels (**a**, **o**, **u**), it's pronounced like the *g* in English *good*.

- gol (*goal*)
- guapo (*handsome*)
- gato (*cat*)

### Ce Before hache

When ce comes before hache, it's pronounced like the *ch* in English *cheese*.

- chícharo (*pea*)
- chicharra (*cicada*)

### Double ele

When two eles appear together, they can be pronounced like the *y* in English *yellow*, the *j* in English *judge*, or the *sh* in English *show*, depending on what country you're in.

- llamar (*to call*)
- valle (*valley*)

### Double erre

When two erres appear together, they are trilled (the sound you make when you roll your tongue). A single erre at the beginning of a word is also trilled.

- carro (*car*)
- burro (*donkey*)
- rojo (*red*)

### Equis Marks the Spot

The equis is usually pronounced like the *ks* in English *socks*. However, in place and person names (especially those from Mexico), it can be pronounced like a raspy English *h*, an *s*, or even the *sh* in English *show*.

Check out these examples:

- Like the *ks* in English *socks*: examen (*exam*)
- Like a raspy English *h*: México (*Mexico*)
- Like an *s*: Xochimilco (*Xochimilco*, a neighborhood in Mexico City)
- Like *sh* in English *show* Xicalango (*Xicalango*, a town in Mexico)

Let's finish up by seeing how the **abecedario** is used in everyday life!

**Customer**: Buenas tardes. Vengo a recoger un paquete. *Good afternoon. I'm here to pick up a package.*

**Clerk**: Muy bien. ¿Cuál es su nombre? *Very good. What's your name?*

**Customer**: Me llamo Víctor Hugo. *My name is Victor Hugo.*

**Clerk**: No lo encuentro. ¿Cómo se deletrea su apellido? *I can't find it. How do you spell your last name?*

**Customer**: Hache-u-ge-o. *H-u-g-o.*

**Clerk**: Ah, sí. Aquí está su paquete. *Ah, yes. Here's your package.*

## [Spanish Terms of Arithmetic](https://www.thoughtco.com/mathematical-terms-in-spanish-3079614)

Here are the words for the simple mathematical functions and how they're used with numbers:

- Addition (Suma): Dos más tres son cinco. (Two plus three is five.) Note that in other contexts, más is usually an adverb.
- Subtraction (Resta): Cinco menos cuatro son uno. (Five minus four is one.)
- Multiplication (Multiplicación): Tres por cuatro son doce. (Three times four is twelve.) In other contexts, por is a common preposition.
- Division (División): Doce dividido entre cuatro son tres. (Twelve divided by four is three.) Doce dividido por cuatro son tres. (Twelve divided by four is three.) Entre is another common preposition.

Note that all of these sentences use the verb son, which is plural, in contrast with the singular verb "is" of English. It is also possible to use es or the phrase es igual a (is equal to).

## Other Mathematical Terms

Here are some less common mathematical terms:

- el cuadrado de — the square of
- el cubo de — the cube of
- ecuación — equation
- elevado a la enésima potencia — raised to the nth power
- función — function
- número imaginario — imaginary number
- promedio — average, mean
- quebrado, fracción — fraction
- raíz cuadrada — square root
- raíz cúbica — cube root

## Sample Sentences

- Solo un idiota que no sabe que dos más dos son cuatro le creería. (Only an idiot who doesn't know that two plus two equals four would believe him.)
- Una fracción es un número que se obtiene de dividir un entero en partes iguales. (A fraction is a number that is obtained by dividing a whole number into equal parts.)
- Pi se obtiene al dividir la circunferencia de un círculo del diámetro. (Pi is obtained by dividing the circumference of a circle by the diameter.)
- El triple de un número menos el doble del mismo número son ese número. (Three times a number minus two times that number is that number.)
- Una función es como una máquina: tiene una entrada y una salida. (A function is like a machine: It has an input and an output.)
- Una ecuación es una igualdad matemática entre dos expresiones matemáticas. (An equation is an equality between two mathematical expressions.)
- La ciudad está dividida en dos partes iguales. (The city is divided into two equal parts.)
- ¿Cuál es el resultado de dividir 20 por 0.5? (What is the quotient of 20 divided by 0.5?)
- El cuadrado de un número menos el doble del mismo número son 48. ¿Cuál es ese número? (The square root of a number minus double the same number is 48. What is that number?)
- El promedio de edad de los estudiantes es de 25 años. (The average age of the students is 25.)
- La división entre cero es una indeterminación. Así la expresión 1/0 carece de sentio. (Division by zero yields an indeterminable number. Thus the expression 1/0 doesn't make sense.)
- Los números imaginarios pueden describirse como el producto de un número real por la unidad imaginaria i, en donde i denota la raíz cuadrada de -1. (Imaginary numbers can be described as the product of a real number by the imaginary unit i, where i denotes the square root of -1.)

## [Decimals, Fractions, And Percentages](https://www.lexico.com/es/gramatica/spanish-usage-notes-for-english-speakers-translating-into-spanish-6-decimals-fractions-and-percentages)

### Decimals

In most Spanish-speaking countries, a comma indicates the decimal place:

- 0.5 (point five) = 0,5 (cero coma cinco)
- 12.76 (twelve point seven six) = 12,76 (doce coma setenta y seis)
 
Some Latin American countries, however, use a point as in English:

- 0.5 (point five) = 0.5 (cero punto cinco)
- 12.76 (twelve point seven six) = 12.76 (doce punto setenta y seis)

### Fractions

- 1/2 = un medio 1/8 = un octavo
- 1/3 = un tercio 1/9 = un noveno
- 1/4 = un cuarto 1/10 = un décimo
- 1/5 = un quinto 2/3 = dos tercios
- 1/6 = un sexto 3/4 = tres cuartos
- 1/7 = un séptimo

Any fraction smaller than a tenth is formed by adding the suffix -avo to the cardinal number:

- 1/11= un onceavo 1/20 = un veinteavo
- 1/12= un doceavo 1/30 = un treintavo
- 7/16= siete dieciseisavos 1/40 = un cuarentavo

But:

- 1/100 = un centésimo 1/1000 = un milésimo

The less common fractions are expressed as follows:

- 3/52 = tres sobre cincuenta y dos
- 7/102 = siete sobre ciento dos

When not used in a strictly mathematical context, fractions are expressed in a different way:

- half an apple = media manzana or la mitad de una manzana
- a third of the mixture = la tercera parte de la mezcla
- three quarters of the population = las tres cuartas de la población
 
### Percentages

The article is used before a percentage in Spanish:

- 10% of the population = el 10% (diez por ciento) de la población
- prices have gone up by 15% = los precios han subido un 15% (un quince por ciento)
