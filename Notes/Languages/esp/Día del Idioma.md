# Día del Idioma
[Día del Idioma BBC enlace](https://www.bbc.com/mundo/noticias-america-latina-65337112)

**¿Cómo entendernos?**

A veces, el español puede ser una verdadera torre de Babel.  Aunque es la lengua común en muchos países, lo cierto es que se usan
términos tan distintos que muchas veces parecen diferentes idiomas.  Hay coloquialismos que solo se comprenden en el país de origen, mientras otras palabras son contradictorias entre unas culturas y otras.

**¿Palomitas o pochoclo?** **¿Mina o jeva? ¿Cruda o guayabo?** son
algunos de los modismos que se usan para hablar de la misma cosa.

En el Día del Idioma Español, BBC Mundo te comparte un diccionario *sui generis* que incluye términos usados comúnmente en las naciones
latinoamericanas y también en España. Nuestros lectores también contribuyeron en la creación de esta lista.

No están todos ni muchos menos. Es tan sólo un ejercicio para intentar enriquecer el diálogo. Si quieres ayudarnos a completarla, búscala en nuestras cuentas de Facebook e Instagram.

## A
**Amuñuñados**: en Venezuela se refiere a varias personas apretujadas o amontonadas en un lugar pequeño.

**Al tiro:** en Chile, de inmediato. Se usa como "voy al tiro", mientras en Uruguay, "al toque".

**Alberca**: así se dice piscina en México. En Argentina, es pileta.

## B

**Bagayo**: significa contrabando en Uruguay mientras que en Argentina es una persona muy pero que muy fea.

**Banqueta**: así se dice acera o vereda en México.

**Birome**: término que se usa en Argentina y Uruguay para hablar de bolígrafo. No es un invento, sino un homenaje al creador de este tipo de lápices, el húngaro Lazlo Biro, quien vivió en Argentina. En Chile o México, se le llama lápiz pasta o lapicero, y en Colombia, se le dice esfero.

**Birra**: cerveza en Argentina o España.

**Burda**: en Venezuela, sinónimo de mucho. Léase "burda de bueno", como "muy bueno"; o "burda de gente", como "mucha gente".

## C

**Cachai**: la clásica expresión chilena para preguntarte si entendiste o no, viene del inglés "to catch" (captar, pillar).

**Cajeta**: El popular dulce de leche, como se conoce en Argentina, Uruguay y Venezuela, en México se llama cajeta (aunque en este país se hace con leche de cabra y no de vaca). En Bolivia, Chile y Colombia se dice manjar -aunque en Bogotá le dicen arequipe-, en Cuba es fanguito y así.

**Calato**: desnudo en Perú.

**Catafixia**: en México se usa "catafixiar" como sinónimo de intercambio de un objeto por otro. La palabra la inventó el recientemente fallecido comediante Chabelo.

**Championes**: es como se conoce el calzado deportivo en Uruguay y Paraguay. En Chile y Argentina se dice zapatillas, en Cuba tenis, en Bolivia, kids, y en Venezuela, zapatos de goma.

**Chino**: para decir niño en Colombia, que en Argentina es un chico y puede ser un pibe; un chavo en México; un gurí o botija en Uruguay; un mitaí en Paraguay; un chamo en Venezuela; un patojo en Guatemala; un cipote o güirro en Honduras y El Salvador; un cabro chico en Chile y un crío en España.

**Chancho**: en Chile se usa a este animalillo en la expresión coloquial irse al chancho, que significa algo exagerado, sin mesura.

**Copado**: bueno o divertido en Argentina o Uruguay.

**Cuate**: en México, se le puede decir así a un amigo (o más popular aún, güey). En Colombia, se dice parce o parcero.

**Curado**: chilenismo para borracho. Equivale a lo que en Argentina y Uruguay es un mamado, término que en Colombia y Venezuela quiere decir cansado y en México se refiere a un musculoso. "Curado" en el norte de México es algo que es divertido.

**Cují**: significa tacaño en la costa Caribe de Colombia.

## D

**Diúrex**: es como se le dice en México a la cinta adhesiva, por una marca comercializada en el siglo XX que se llamaba Durex. En el Cono Sur se conoce como cinta scotch. En Cuba es escortei, en Colombia, cinta pegante, en Venezuela, teipe, y en España celo.

## E

**Echar la pera:** en Ecuador significa fugarse de un lugar, faltar a clases o no presentarse. A eso mismo se le llama jubilarse en Venezuela, hacer pellas en España, capar en Colombia o hacer la cimarra, en Chile.  Irse de pinta, en México.

## F

**Fanchop**: bebida chilena, mitad fanta mitad cerveza. En Colombia se llama refajo.

**Faso**: así se le dice al cigarrillo en Argentina. En Chile, Colombia y otros países de América del Sur se le llama pucho.

**Futbolín**: dícese en España del juego que en varias partes se conoce como fútbol de mesa. En Argentina es metegol, en Uruguay y Venezuela futbolito y en Chile taca taca, que para los españoles es el andador de los bebés.

## G

**Gásfiter**: en Chile es la persona que se dedica a arreglar cañerías.  Sí, sí, el plomero o el fontanero en México, España y otros países.

**Guatero**: en la mayoría de los países se usan bolsas de agua caliente para calentarse los pies en los días fríos del invierno. En Chile se llama guatero.

**Guagua**: así se le dice en Cuba al autobús pero en otros países, como Chile, guagua significa bebé.

**Guayabo**: en Colombia el guayabo es la resaca, ratón en venezolano.  En Chile se dice caña. Cruda, en México, y chuchaqui en Ecuador.

**Guachimán**: así se le dice al vigilante en Venezuela, Honduras y Panamá (o su variante "wachimán").

**Guita**: dinero en Argentina (al que también se le puede llamar mosca). En gran parte de Centroamérica se le dice pisto (que en España es una comida). En México se usa lana y en otros países pasta.

## H

**Huarache**: son unas sandalias de cuero en México. En otros sitios son chalitas, chalas o cholas, y si separan el dedo gordo de los demás son ojotas en Argentina o chancletas en Uruguay.

**Hanguear**: en Puerto Rico se usa para decir que vas a salir a divertirte. Viene del inglés "to hang around" (juntarse, andar con).

## I

**Itacate:** así se le llama en México a una provisión de comida que se lleva a un paseo o un viaje. También pude ser el sobrante de una comida que se le da a un invitado.

## J

**Jama**: en Cuba, comida.

**Jetón**: en Colombia para referirse a alguien que habla mucho. Para otros es alguien de boca grande\... o cara grande. También puede ser simplemente un idiota. En México, alguien que se queda dormido está "jetón".

**Jersey**: es un buzo o pulóver en España.

**Jinetera**: prostituta en Cuba.

**Jocho**: expresión mexicana para referirse a los perros calientes. La palabra es una traducción directa del inglés "hot dog". También se usa en otros países como Panamá o Puerto Rico. En Argentina, se les dice pancho.


## K

**Kencha**: palabra de origen aymara y/o quechua que se utiliza en Bolivia para hablar de alguien que tiene y da mala suerte. Lo mismo es yeta en Uruguay y Argentina, y mufa en Argentina.

**Kurepa o Kurepí:** término que usan los paraguayos para hablar de los argentinos/as. Dicen que en la guerra contra la Triple Alianza los soldados argentinos usaban botas de cuero de chancho. En guaraní Kuré es chancho y py pie. De ahí viene el kurepí o kurepa para llamar a los argentinos.

## L

**Lagarto**: un litro de lagarto es un litro de cerveza en Cuba o una persona que toma mucho sol en Uruguay y Argentina. En Colombia un lagarto es un arribista, un trepador.

## M

**Mina**: además del significado que todos conocemos es también una mujer, generalmente joven, en Argentina y Chile, una lola o una jeva en Venezuela.

**Mono**: en Colombia es alguien rubio, güero en México y catire en Venezuela.

**Muerto**: dejar un muerto es cuando uno se va de un establecimiento sin pagar (Uruguay). En Chile, se dice hacer perro muerto.

## N

**Nabo**: tonto en Argentina. En Chile se dice gil.

## Ñ

**Ñoqui**: trabajar pero no hacer nada. Un empleado que cobra su sueldo todos los meses (o sea que pasa por su lugar de trabajo a cobrar a fin de mes, pero para trabajar nunca aparece). Viene de la costumbre argentina de comer ñoquis todos los 29. Zoquetero: equivalente al ñoqui argentino en Paraguay.

**Ñoño**: botella de cerveza de un litro, porque su forma se parece a la barriga del Ñoño, el personaje del Chavo del 8. Ejemplo: ¿nos tomamos un ñoño? (en Paraguay), que en México sería una Caguama o Ballena. "Ñoño" en Chile es una persona "perna", es decir, inocente o "nerd".

**Ñero**: alguien vulgar en México.

## O

**Once**: tecito con tostadas que se toma puntualmente a las 5 de la tarde en Chile y Bogotá. Te invito a tomar once.

**Ortiva**: soplón en Argentina, botón en Uruguay (que es policía en Argentina). ¡Es una historia que no acaba nunca!

## P

**Pandorga**: en Paraguay se le llama así al volantín… o cometa, barrilete, papalote, etcétera.

**Piloto**: en casi todos los países es alguien que maneja un auto, pero en Argentina es también un impermeable.

**Pena**: en Colombia, México y parte de Centroamérica, tener pena es sentir vergüenza. En otros países esa palabra está asociada a la tristeza.

**Pecueca**: "qué pecueca tiene este zapato"… así le llaman los colombianos al mal olor en los pies.

**Pololo**: en Chile es un noviazgo que no implica compromiso matrimonial. Un poco más que un amigovio (palabra que se usa en el Cono Sur para hablar de una relación amorosa un poco indefinida) o amigos con derecho (como se usa en Venezuela). También se utiliza como verbo: ¿querí pololear? Ver letra Q.

## Q

**Quilombo**: en Uruguay y Argentina se usa para hablar de caos, desorden o lío. Despelote en Colombia o Chile, zaperoco en Venezuela.

**Queque**: en Chile y otros países se le dice queque a la torta. El Colombia se le dice ponqué.

**¿Querí?**: modismo o deformación chilena del verbo querer: ¿quieres?

## R

**Redondel**: término utilizado en Ecuador y algunos países de Centroamérica para lo que en otros países es rotonda, redoma rompoin o glorieta, es decir, una plaza en la que desembocan varias calles y que sirve para distribuir el tráfico.

**Remera**: camiseta de manga corta en Argentina, Uruguay o España. En Chile y Bolivia se dice polera, en Venezuela franela y en México playera.

**Rumba**: salir de fiesta en Colombia. Parranda o juerga en España, o salir de joda, en Argentina. Carrete en Chile.

**Rositas**: en Cuba tienen nombre de flor. En Argentina se las llama pochoclo, en Venezuela cotufas, y en México y España palomitas. En Colombia crispetas, en Chile cabritas, en Bolivia pipoca, en Perú popcor o canchita, en Paraguay pororó y en Uruguay en el estadio gritan ¡Pó, acaramelado el pó! Por si te quedan dudas, en inglés es *popcorn*.

## S

**Sorijchi**: mal de altura en Bolivia, país andino. Sus vecinos le dicen apunamiento.

**Sifrino**: término venezolano para lo que en Chile es cuico, en Argentina concheto, en Uruguay cheto, en Bolivia y Perú pituco, en Colombia gomelo y en México fresa. Para que nos entendamos, la Real Academia define "pijo" como persona que manifiesta gustos de una clase social acomodada.

## T

**Tinieblo**: término casi aterrador que en Colombia se utiliza para hablar del amante.

**Tinto**: si estás en Colombia y pides un "tinto" te van a traer un café con agua. Olvídate del vino.

**Tlapalería**: en México para referirse a la ferretería.

**Temperar**: irse en vacaciones, según nicaragüenses, venezolanos y dominicanos.

**Torta:** es un tipo de sándwich salado en México (como un bocadillo español), pero en otros países puede ser un postre dulce como una tarta o un pastel.

**Tostón**: en España, alguien o algo que es un pelmazo, algo fastidioso e insufrible. En algunos países caribeños y centroamericanos, es plátano verde machacado y frito.

## U

**Untar**: sobornar en Venezuela (coimear en la mayoría de los demás países).

## V

**Vaina**: ¿quién no ha escuchado o usado la tropicalísima palabra vaina? Bueno, pues tiene varios significados. Además de ser una funda ajustada para armas blancas o instrumentos cortantes o punzantes o una cáscara tierna y larga en la que están encerradas las semillas de algunas plantas, echar vainas en Cuba es decir tonterías y en Venezuela molestar mucho a alguien. Ni de vaina es de ninguna manera en Ecuador y Venezuela, aunque en este país como en Panamá también se utiliza para referirse a objetos o asuntos generales.

**Viruña**: en Colombia, mala gente.

## W

**Wáter**: se usa en varios países del cono sur y España (váter) para referirse al inodoro, excusado o poceta (Venezuela).

## X

**X**: en México se le dice "X" a algo que da lo mismo.

## Y

**Yacaré**: entrar a algún lugar sin pagar la entrada, entrar de colado, sin ser invitado. Es lo que en Venezuela se dice como arrocero.

**Yegua**: en Venezuela, una mujer atractiva de grandes proporciones (también en Argentina pero con distintas connotaciones). En otros países se entiende como una mujer sin límites, sin escrúpulos. Siguiendo en el mundo fauno, en Uruguay potra.

## Z

**Zarpado**: es una persona atrevida en países como Argentina y Uruguay.

**Zarcillos**: palabra que se usa en Venezuela para aros, aretes, pendientes, en fin, las joyas que se usan en las orejas.

**Zapato**: aparte de la acepción obvia, en Colombia es mujer fea u hombre feo. ¡Qué tipo más zapato! En Paraguay sería, qué tipo más vairo.
