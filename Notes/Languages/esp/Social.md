# Social

## About Me and About You

* Happy | Feliz
* Wrong (*masculine*) | Equivocado
* Wrong (*feminine*) | Equivocada
* Tired (*masculine*) | Cansado
* Tired (*feminine*) | Cansada
* Attractive (*masculine*) | Guapo
* Attractive (*feminine*) | Guapa
* Sick (*masculine*) | Enfermo
* Sick (*feminine*) | Enferma
* Hunger | Hambre
* Thirst | Sed
* I am happy | Estoy Feliz
* You are happy | Estás feliz
* Are you happy? | ¿Estás feliz?
* Am I wrong? | ¿Estoy equivocado?
* You are attractive (*feminine*) | Estás guapa
* I am tired | Estoy cansado
* Are you sick (*feminine*)? | ¿Estás enferma?
* I have | Tengo
* You have (*singular informal*) | Tienes
* A (*feminine*) | Una
* A (*masculine*) | Un  
* I am hungry (*I have hunger*) | Tengo hambre
* I am thirsty (*I have thirst*) | Tengo sed
* You are hungry | Tienes hambre
* Are you thirsty? | ¿Tienes sed?

## Arguing

* To argue | Discutir (*pelear*)
* The mistake | El error
* To make a mistake | Cometer un error
* I made a mistake | Cometí un error
* Obviously | Obviamente
* Obviously she made a mistake | Obviamente cometió un error
* To hurt (*someone physically*) | Herir (*a alguien*)
* To hurt (*emotionally*) | Hacer daño
* Him, it (*object*) | Lo
* You hurt him | Lo heriste
* The feeling | El sentimiento
* You hurt her feelings | Heriste sus sentimientos
* To mean (*what was said*) | Decir en serio
* I meant (*what I said*) | Lo dije en serio
* I didn't mean it | No lo dije en serio
* To forgive | Perdonar
* Forgive me | Perdóname
* He meant (*what he said*) | Lo dijo en serio
* You brother didn't mean it | Tu hermano no lo dijo en serio
* Did you mean it? | ¿Lo dijiste en serio?
* Yes I did (*say that*) | Sí lo dije 
* No I didn't (*say that*) | No lo dije
* On purpose | A propósito
* To mean (*to do something*), to do (*something*) on purpose | Hacer (*algo*) a propósito
* She didn't hurt you on purpose | No te hizo daño a propósito
* To yell | Chillar
* To scare (*someone*) | Asustar
* He didn't scare you on purpose | No te asustó a propósito
* I hurt | Herí
* I think I hurt his feelings | Creo que herí sus sentimientos
* Upset | Molesto
* To make (*someone*) upset | Molestar
* Her | La
* I think I made her upset | Creo que la molesté
* I am sure that he didn't mean it | Estoy seguro de que no lo dijo en serio
* I am sure that you can be friends again | Estoy seguro de que podéis ser amigos otra vez
* I didn't yell on purpose | No chillé a propósito
* I am sure that everything will be okay | Estoy seguro de que todo estará bien
* I didn't make him upset on purpose | No lo molesté a propósito
* I think I made my boyfriend very angry | Creo que enfadé mucho a mi novio
* Obviously you made a mistake | Obviamente cometiste un error
* To agree | Estar de acuerdo
* We don't agree | No estamos de acuerdo
* I don't agree with you | No estoy de acuerdo contigo
* To get (*themselves*) along | Llevar(se) bien
* The parents | Los padres
* My parents don't get along | Mis Padres no se llevan bien
* My sons don't get along | Mis hijos no se llevan bien
* The truth | La verdad
* True | Verdadero
* It is true | Es verdad
* The lie | La mentira
* To lie | Mentir
* That is a lie | Eso es mentira
* Fair | Justo
* Unfair | Injusto
* That is not fair | Esto no es justo
* Silly | Tonto
* Just | Solo
* This is just a silly argument | Esto es solo una pelea tonta
* Stupid | Estúpido
* This is a stupid argument | Esto es una pelea estúpida
* She never listens to me | Nunca me escucha 
* Don't lie (*subjunctive*) | No mientas
* No me mientas
* You never listen to him | Nunca le escuchas
* Don't be (*subjunctive*) | No seas
* Don't be silly | No seas tonto
* That is a big fat lie | Eso es un mentira bien gorda
* Tell me the truth | Dime la verdad
* This is so unfair | Esto es tan injusto
* Just talk to me | Solo háblame
* They don't get along | No se llevan bien
* Just listen to me | Solo escúchame
* My boyfriend never talks to me | Mi novio nunca me habla
* Just listen for a second | Solo escucha un segundo
* He is just an idiot | Solo es un idiota
* Stop yelling | Deja de chillar
* To share | Compartir
* The sweets | Las golosinas
* I don't want to share my sweets | No quiero compartir mis golosinas
* He doesn't want to share his toys | No quiere compartir sus juguetes
* To take (*something*) away | Quitar (*algo*)
* I will take your toys away | Te quitaré tus juguetes
* I will take your sweets away | Te quitaré tus golosinas
* If you don't share your toys I will take them away | Si no compartes tus juguetes te los quitaré
* The jerk (*man, woman*) | El capullo, la capulla
* What a jerk | Qué capullo
* Mad | Loco
* To get (*oneself*) mad | Volver(*se*) loco
* If you don't share your sweets I will get mad | Si no compartes tus golosinas me volveré loco
* What a terrible birthday | Qué cumpleaños más terrible
* To ruin | Arruinar
* He ruined my birthday | Arruinó mi cumpleaños
* She always ruins everything | Siempre lo arruina todo
* Don't be stupid | No seas estúpido
* What an idiot | Qué idiota
* Don't be silly | No seas tonto
* To answer | Reponder
* Please answer me | Por favor repóndeme
* Honest | Sincero
* Be honest | Sé sincero
* To let | Dejar
* To stop (*doing something*) | Dejar (*de hacer algo*)
* Please stop talking | Por favor deja de hablar
* Please stop lying | Por favor deja de mentir
* You have to answer me | Tienes que responderme
* Was that an honest answer? | ¿Era esa una respuesta sincera?
* You have to stop yelling | Tienes que dejar de chillar
* To apologise (*oneself*) | Disculpar(*se*)
* Someone has to apologise | Alguien tiene que disculparse
* To finish | Terminar
* Please let me finish | Por favor déjame terminar
* Illogical | Ilógico
* The argument (*reasoning*) | El argumento
* Your argument is illogical | Tu argumento es ilógico
* To be kidding | Estar de broma
* To be kidding? | ¿Estás de broma?
* Please stop yelling | Por favor deja de chillar
* You have to stop lying | Tienes que dejar de mentir
* The apology | La disculpa
* Was that an apology? | ¿Era eso una disculpa?
* The point of view | El punto de vista
* That is an interesting point of view | Ese es un punto de vista interesante
* You have to be honest | Tienes que ser sincero
* She doesn't want to apologise | No quiere disculparse
* You have to apologise | Tienes que disculparte
* To contradict (*someone*) | Contradecir (*a alguien*)
* Contradicting | Contradiciendo
* You are contradicting yourself | Te estás contradiciendo
* That is an interesting argument | Ese es un argumento interesante
* I don't understand your point of view | No entiendo tu punto de vista

## Do you have plans?

* The plan | El plan
* Later | Más tarde
* A drink | Una bebida
* Do you have plans for tonight? | ¿Tienes planes para esta noche?
* Do you have any plans later? | ¿Tienes planes para más tarde?
* I am going to watch my favourite TV show | Voy a ver mi programa de televisión favorito
* He is going to watch the news | Va a ver las noticias
* Our daughter is going to make us lunch | Nuestra hija nos va a hacer la comida
* We are going to make you a cake | Vamos a hacerte una tarta
* They are going to go out for a drink | Vamos a salir a beber algo
* We are going to go out for dinner | Vamos a salir a cenar
* Do you want to some? | ¿Quieres venir?
* Do you have any plans tomorrow? | ¿Tienes planes para mañana?
* We are going to watch my favourite film | Vamos a ver mi película favorita
* I am going to go out for lunch | Voy a salir a comer
* To do the laundry | Hacer la colada
* To do the dishes | Fregar los platos
* The essay | La redacción
* To stay | Quedarse
* Alone | Solo
* So | Así que
* She never has to do the laundry | Nunca tiene que hacer la colada
* We always have to do the dishes | Siempre tenemos que fregar los platos
* I have to write an essay | Tengo que escribir una redacción
* I want to sleep | Quiero dormir
* He wants to watch the news | Quiere ver las noticias
* He wants to be alone | Quiere estar solo
* I want to be alone | Quiero estar solo
* We want to stay home | Queremos quedarnos en casa
* He is angry so he wants to stay in his room | Está enfadado así que quiere quedarse en su habitación
* She is very sick so she wants to stay in bed | Está muy enferma así que quiere quedarse en la cama
* We want to be alone today | Queremos estar solos hoy
* To meet | Conocer
* Italy | Italia
* To dress up | Vestirse elegante
* To get drunk | Emborracharse
* To play a game | Jugar a un juego
* To relax, to chill out | Relajar(se)
* Do you want to meet my mum and dad? | ¿Quieres conocer a mi madre y a mi padre?
* Does he want to meet me? | ¿Quiere conocerme?
* Do you want to meet my friend from Italy? | ¿Quieres conocer a mi amigo de Italia?
* We could dress up and go dancing | Podríamos vestirnos elegantes e ir a bailar
* We could get drunk | Podríamos emborracharnos
* We could go out for lunch | Podríamos salir a comer
* We could play a game | Podríamos jugar a un juego
* We could chill out | Podríamos relajarnos
* We could relax | Podríamos relajarnos
* Later (*alt*) | Al rato
* Still hasn't arrived | Todavía no ha llegado

## Dreams

* The football player (*man*) | El futbolista
* I want to be a football player | Quiero ser futbolista
* The pop star | La estrella del pop
* My daughter wants to be a popstar | Mi hija quiere ser una estrella del pop
* The writer (*man, woman*) | El escritor, la escritora
* The expert (*man, woman*) | El experto, la experta
* Professional | Profesional
* The basketball player (*man*) | El baloncestista
* I want to be a professional basketball player | Quiero ser baloncestista profesional 
* To travel the world | Viajar por el mundo
* He wants to travel the world | Quiere viajar por el mundo
* To open | Abrir
* She wants to open a restaurant | Quiere abrir un restaurante
* The degree (*at university*) | La carrera
* To do a degree | Estudiar una carrera
* The literature | La literatura
* My friend wants to do a degree | Mi amiga quiere estudiar una carrera
* My brother wants to be a language expert | Mi hermano quiere ser un experto en idiomas 
* To start | Empezar
* They want to start a business | Quieren empezar un negocio
* Poor | Pobre
* Rich | Rico
* He wants to be rich | Quiere ser rico
* His family is poor so he wants to be rich | Su familia es pobre así que quiere ser rico
* The musician (*man, woman*) | El músico, la música
* He is good at playing music so he wants to be a musician | Se le da bien tocar música así que quire ser músico
* Famous | Famoso
* The actress | La actriz
* The actor | El actor
* I always wanted to be a famous actress | Siempre quise ser una actriz famosa
* She always wanted to be a computer expert | Siempre quiso ser una experta en ordenadores
* Whole | Todo
* The designer (*man, woman*) | El diseñador, la diseñadora
* My whole life I wanted to be a designer | Toda mi vida quise ser diseñadora
* My whole life I wanted to be a writer | Toda mi vida quise ser escritor
* To sell | Vender
* I want to sell my business | Quiero vender mi negocio
* The boat | El barco
* I want to sell my house and buy a boat | Quiero vender mi casa y comprar un barco
* My mum wants to be a writer | Mi madre quiere ser escritora
* My daughter wants to do a degree in literature | Mi hija quiere estudiar la carrera de literatura
* They want to sell their car and travel the world | Quieren vender su coche y viajar por el mundo
* My dad always wanted to be a famous actor | Mi padre siempre quiso ser un actor famoso
* My sister is good at writing stories so she wants to be a writer | A mi hermana se le da bien escribir historias así que quiere ser escritora
* No one wants to be poor | Nadie quiere ser pobre
* My parents want to travel the world | Mis padres quieren viajar por el mundo
* The ice hockey player (*man*) | El jugador de hockey sobre hielo
* My son wants to be a professional ice hockey player | Mi hijo quiere ser jugador profesional de hockey sobre hielo
* To not care (*oneself about someone*) | No preocupar(*se*) (*por alguien*)
* I don't care about money | No me preocupa el dinero
* Single | Soltero
* Forever | Para siempre 
* I don't want to stay single forever | No quiero quedarme soltera para siempre
* To have success | Tener éxito
* I am sure that you will be very successful | Estoy seguro de que tendrás mucho éxito
* To imagine | Imaginar
* The dream | El sueño
* You dream job | El trabajo de tus sueños
* Imagine you dream job | Imagina el trabajo de tus sueños
* I don't about mi career | No me preocupa mi carrera
* I want to earn a lot of money | Quiero ganar un montón de dinero
* Who wants to live forever? | ¿Quién quiere vivir para siempre?
* I want to be really healthy and live forever | Quiero estar realmente sano y vivir para siempre
* I am sure that you will be very rich | Estoy seguro de que serás muy rico
* Who wants to be poor forever? | ¿Quién quiere ser pobre para siempre?
* Imagina you dream house | Imagina la casa de tus sueños
* The love | El amor
* I don't care about love | No me preocupa el amor
* The novel | La novela
* Fantastic | Fantástico
* I want to write a fantastic novel | Quiero escribir una novela fantástica
* The song | La canción
* I am sure that you will find a nice girlfriend | Estoy seguro de que encontrarás una novia agradable
* Would be | Sería
* That would be fantastic | Eso sería fantástico
* I want to write a fantastic song | Quiero escribir una canción fantástica
* That would be incredible | Eso sería increíble
* Imagine you dream car | Imagina el coche de tus sueños
* That would be horrible | Eso sería horrible
* Who wants to stay single forever? | ¿Quién quiere quedarse soltero para siempre?
* The smile | La sonrisa
* Love | El amor
* Friendship | La amistad
* Happy | Feliz
* The tension | La tensión
* Joy, happiness (*a…*) | La alegría
* Happiness (*f…*) | La felicidad
* Loneliness | La soledad
* Hope | La esperanza
* The personality | La personalidad
* Fear (*m…*) | El miedo
* The pain | El dolor
* The surprise | La sorpresa
* Fear (*t…*) | El temor
* The celebration | La celebración
* Curiosity | La curiosidad
* Sad | Triste
* The desire | El deseo
* Nervous | Nervioso
* The mood | El humor
* The satisfaction | La satisfacción
* The feeling | El sentimiento
* Pleasure | El placer

## Family

* The family | La familia
* The brother | El Hermano
* The sister | La hermana
* The mother | La madre
* The father | El padre
* Momma | Mamá
* Papa | Papá
* The granddad | El abuelo
* The grandmother | La abuela
* The wife | La esposa
* The husband | El esposo
* The son | El hijo
* The daughter | La hija
* The father in law | El suegro
* The mother in law | La suegra
* The brother in law | El cuñado
* The sister in law | La cuñada
* The nephew | EL sobrino
* The niece | La sobrina
* The boyfriend | El novio
* The girlfriend | La novia
* The friend (*male*) | El amigo
* The friend (*female*) | La amiga
* The kids | Los hijos
* These | Estos, estas
* They | Ellos, ellas
* Who | Quién, quiénes
* The job | El trabajo
* Your (*sing. informal*) | Tu
* The school | La escuela
* The office | La oficina
* He, she works | Trabaja
* In | En
* This is my mother | Esta es mi madre
* This is my father | Este es mi padre
* He is my brother | Él es mi hermano
* She is my sister | Ella es mi hermana
* This is my mother and my father | Estos son mi madre y mi padre
* This is my granddad and my grandma | Estos son mi abuelo y mi abuelo
* Who are they? | ¿Quiénes son ellos?
* Who is she? | ¿Quién es ella?
* That is not my daughter | Esa no es mi hija
* Does he have a girlfriend? | ¿Tiene novia?
* This is my friend (*female*) from Mexico | Esta es mi amiga de México
* I want a husband | Quiero un esposo
* I want a wife | Quiero una mujer
* I want kids | Quiero hijos
* Does your girlfriend have a job? | ¿Tiene trabajo tu novia?
* He works in an office | Trabaja en una oficina
* She works in a shop | Trabaja en un tienda
* This is my brother and sister | Estos son mi hermano y mi hermana
* Who are you? | ¿Quién eres tú?
* Does your husband have a job? | ¿Tiene trabajo tu marido?
* He works in a school | Trabaja en una escuela
* A year | Un año
* How many | Cuántos
* Old | Viejo
* Young | Joven
* Strong | Fuerte
* This is my dog | Este es mi perro
* How old is she? | ¿Cuántos años tiene?
* He is six years old | Tiene seis años
* I am twenty years old | Tengo veinte años
* My dog is very old | Mi perro es muy viejo
* You are very young | Eres muy joven
* He is very old | Es muy viejo
* This is my cat | Este es mi gato
* How old are you? | ¿Cuántos años tienes?
* You are very strong | Eres muy fuerte
* The woman | La mujer
* That woman | Esa mujer
* The man | El hombre
* That man | Ese hombre
* The boy | El niño
* The girl | La niña
* To study | Estudiar
* The university | La universidad
* The kindergarten | La guardería
* His, her, their | Su
* The student (*boy*) | El estudiante
* The student (*girl*) | La estudiante
* Los niños | The children
* I am an artist | Soy artista
* Who is this boy? | ¿Quién es este niño?
* Who is that women? | ¿Quién es esa mujer?
* She is a teacher | Es maestra
* The girl studies in Spain | La niña estudia en España
* My brother studies at the university | Mi hermano estudia en la universidad
* Mu daughter goes to kindergarten | Mi hija va a la guardería
* His sister goes to the school | Su hermana va a la escuela
* My son and my daughter are students | Mi hijo y mi hija son estudiantes
* Who is that man? | ¿Quién es ese hombre?
* He is a lawyer | Es abogado
* My boyfriend studies at the university | Mi novio estudia en la universidad
* My son studies at the university | Mi hijo estudia en la universidad
* My brother goes to the school | Mi hermano va a la escuela
* The children are artists | Los niños son artistas
* My husband and my brother are lawyers | Mi esposo y mi hermano son abogados
* Dad | El papá
* Mum | La mamá
* The relative | El pariente
* The grandchild (*male, female*) | El nieto, la nieta
* The neighbour | El vecino, la vecina
* The uncle | El tío
* The husband (*m…*)) | El marido
* The cousin | El primo
* The population | La población
* The worker | El trabajador
* The revolution | La revolución
* The citizen | El ciudadano
* The colleague, the partner | El compañero
* The audience | La audiencia
* The couple | La pareja
* The marriage | El matrimonio
* The individual | El individuo
* The tourism | El turismo
* The enemy | El enemigo
* The custom, the habit | La costumbre
* The girl (*c…*) | La chica
* The peasant | El campesino
* The fair | La feria
* The gentleman | El caballero
* The adult | El adulto

## Giving advice

* The advice | El consejo
* Let me give you some advice | Déjame darte un consejo
* Let me help you | Déjame ayudarte
* To complain (*oneself*) | Quejar(se)
* To worry (*oneself*) | Preocupar(se)
* You should forgive him | Debería perdonarlo
* The attention | La atención
* To pay attention | Prestar atención
* He should pay more attention | Debería prestar más atención
* Someone | Alguien 
* Nobody | Nadie
* You should complain to someone | Deberías quejarte a alguien
* She should tell someone | Debería decírselo a alguien
* She shouldn't tell anyone | No debería decírselo nadie
* The message | El mensaje
* Should I forgive her? | ¿Debería perdonarla?
* She should give him a message | Debería darle un mensaje
* Stop worrying | Deja de preocuparte
* You should listen to yourself | Deberías escucharte
* You shouldn't listen to your family | No deberías escuchar a tu familia
* You should eat a lot of chocolate and drink some wine | Deberías comer mucho chocolate y beber algo de vino
* To forget | Olvidarse
* Simply, just | Simplemente
* Should I simply forget about it? | ¿Debería simplemente olvidarme de ello?
* Stop calling him | Deja de llamarlo
* He shouldn't call her anymore | No debería llamarla nunca más
* You should simply forget about him | Deberías simplemente olvidarte de él
* To make an effort | Hacer un esfuerzo
* You have to make an effort | Tienes que hacer un esfuerzo
* To try hard | Esforzarse
* You must try harder | Debes esforzarte más
* To suggest | Sugerir
* To go to bed | Irse a la cama
* You forget (*to you*) | Olvidarte
* I think you should forget about it | Creo que deberías olvidarte de ello
* You go (*yourself*) | Irte
* I think you should go to bed now | Creo que deberías irte a la cama ahora
* Same | Mismo
* Everyone makes mistakes | Todo el mundo comete errores
* To be alright (*someone*) | Estar bien
* To be alright (*something*) | Ir bien
* You will be alright | Estarás bien
* Don't make, don't commit (*subjunctive*) | No cometas
* Don't make the same mistake again | No cometas el mismo fallo otra vez
* Don't go to the same cinema again | No vayas al mismo cine otra vez
* He has to make an effort | Tiene que hacer un esfuerzo
* If you try hard it will be alright | Si te esfuerzas irá bien
* He must try harder | Debe esforzarse más
* I suggest we go for a walk now | Propongo que vayamos a pasear ahora
* If you make an effort everything will be alright | Si haces un esfuerzo todo irá bien
* Don't go to his house again | No vayas a su casa ahora
* I make a mistake | Cometí un error
* Everything will be alright | Todo irá bien
* If you try hard everything will be alright | Si te esfuerzas todo irá bien

## Giving Presents

* The present | El regalo
* The ring | El anillo
* The coat | El abrigo
* A kiss | Un beso
* A suit | Un traje
* A watch | Un reloj
* A hug | Un abrazo
* Also | También
* A T-shirt | Una camiseta
* The book | El libro
* Already | Ya
* But | Pero
* I have to buy a present for my mum | Tengo que comprar un regalo para mi madre
* You have to buy a present for you girlfriend | Tienes que comprar un regalo para tu novia
* He gives me a present every day | Me da un regalo todos los días
* I give him a present every year | Le doy un regalo todos los años
* My brother gives them a dog every year | Mi hermano les da un perro todos los años
* We give them shoes every year | Les damos zapatos todos los años
* He wants to give her a ring | Quiere darle un anillo
* We want to give her a new cat | Queremos darle un gato nuevo
* She wants to give him a kiss | Quiere darle un beso
* My dad wants to give him a suit | Mi padre quiere darle un traje
* My mum also wants a watch | Mi madre también quiere un reloj
* Sometimes I also want a hug | A veces también quiero un abrazo
* I already have a watch | Ya tengo un reloj
* My dad already has a nice suit | Mi padre ya tiene un traje bonito
* My brother already has a T-shirt but he wants a new one | Mi hermano ya tiene una camiseta pero quiere una nueva
* She already has a book but she wants a new one | Ya quiere un libro pero quiere uno nuevo
* I have to buy milk for my mum | Tengo que comprar leche para mi madre
* Actually I give you a new hat every year | En realidad te doy un sombrero nuevo todos los años
* He wants to give her a coat | Quiere darle un abrigo
* I really want to give him a kiss | Realmente quiero darle un beso
* He also wants a kiss | También quiere un beso
* We already have a table but we need a new one | Ya tenemos una mesa pero necesitamos una nueva
* For you | Para ti
* For him | Para él
* My grandmother doesn't want anything put I have to buy her a present | Mi abuela no quiere nada pero le tengo que comprar un regalo
* I have to give her something but I don't know what | Tengo que darle algo pero no sé qué
* Is that for you? | ¿ese es para ti?
* Is that for him? | ¿ese es para él?
* I don't want to eat anything today | No quiero comer nada hoy
* He doesn't want to give her anything | No quiere darle nada
* He has to buy her something | Tiene que comprarle algo
* We have to give them something to drink | Tenemos que darles algo de beber

## Giving Your Opinion

* He | Él 
* She | Ella
* Wonderful | Maravilloso
* Nice | Agradable
* Ugly | Feo
* Beautiful | Hermoso
* Cool | Genial
* I think | Creo
* You think (*sing, informal*) | Crees
* What | Qué
* That (*followed by a sentence*) | Que
* I think that… | Creo que…
* Big | Grande
* Long | Largo
* Short | Corto
* Small | Pequeño
* Too, too much | Demasiado
* She is beautiful | Ella es hermosa
* He is wonderful | Él es maravilloso
* It is ugly | Es feo
* How wonderful! | ¡Qué maravilloso!
* How nice! | ¡Qué agradable!
* What do you think? | ¿Qué crees tú?
* I don’t think so | Creo que no
* I think it’s too big | Creo que es demasiado grande
* I think it’s too small | Creo que es demasiado pequeño
* New | Nuevo
* The hat | El sombrero
* The moment | El momento
* The bag | El bolso
* Look (*a command*) | Mira
* Is it possible…? | ¿Se puede…?
* Expensive | Caro
* Really | Realmente
* Cheap | Barato
* It is new | Es nuevo
* I need a new hat | Necesito un sombrero nuevo
* I need a moment to think | Necesito un momento para pensar
* You need a new bag | Necesitas un bolso nuevo
* I need a big bag | Necesito un bolso grande
* Look at that | Mira ese
* Look at this | Mira este
* Is it possible to try this on? | ¿Se puede probar este?
* It is really expensive | Es realmente caro
* It’s a little expensive | Es un poco caro
* This one is cheaper | Este está más barato
* Actually this one is bigger | En realidad este es más grande
* He needs a big, yellow hat | Necesita un sombrero amarillo grande
* Is it possible to try on that hat? | ¿Se puede probar ese sombrero?
* Is it possible to try on that scarf | ¿Se puede probar esa bufanda?
* This one is newer | Este es más nuevo
* The money | El dinero
* The time | El tiempo
* Much | Mucho
* Enough | Suficiente
* Too much | Demasiado
* To spend | Gastar
* It is very cheap | Es muy barato
* It is too expensive | Es demasiado caro
* I don’t have much money | No tengo mucho dinero
* He doesn’t have enough money | No tiene suficiente dinero
* She has too much money | Tiene demasiado dinero
* We have too much time | Tenemos demasiado tiempo
* I can’t spend too much money | No puedo gastar demasiado dinero
* It is too small | Es demasiado pequeño
* He doesn’t have much pasta | No tiene mucha pasta
* You don’t have much time | No tienes mucho tiempo
* We don’t have enough meat | No tenemos suficiente carne
* She doesn’t have enough water | No tiene suficiente agua
* She can’t eat too much chicken | No puede comer demasiado pollo
* We can’t drink too much coffee | No podemos beber demasiado café

## Relationships

* The relationship | La relación
* We used to eat together but not anymore | Solíamos comer juntos pero ya no
* The couple | La pareja
* We were (*permanently, imperfect past*) | Éramos
* We used to be a couple but not anymore | Éramos pareja pero ya no
* To miss (*someone*) | Echar de menos (*a alguien*)
* The ex (*man, woman*) | El ex, la ex
* I don't miss my ex anymore | Ya no echo de menos a mi ex
* Each other | El uno al otro
* They don't speak to each other anymore | Ya no se hablan el uno al otro 
* To care (*about something*) | Preocupar(*se*)(*por algo*)
* He still cares about his ex | Todavía se preocupa por su ex
* To despise (*someone*) | Despreciar (*a alguien*)
* She still despises her mum | Todavía desprecia a su madre
* We still care about each other | Todavía nos preocupamos el uno del otro
* She still misses her ex | Todavía echa de menos a su ex
* You never speak to each other |  Nunca os habláis el uno al otro
* They still speak to each other sometimes | Todavía se hablan el uno al otro a veces
* She doesn't love her boyfriend anymore | Ya no ama a su novio
* To date (*someone*) | Salir (*con alguien*)
* He still cares about you | Todavía se preocupa por ti
* They used to date but not anymore | Solían salir pero ya no
* I still miss my friends sometimes | Todavía echo de menos a mis amigos a veces
* We never listen to each other | Nunca nos escuchamos el uno al otro
* To hate (*someone*) | Odiar (*a alguien*)
* He doesn't hate his ex anymore | Ya no odia a su ex
* I still miss my family | Todavía echo de menos a mi familia

## Talking about People

* He lives, she lives | Vive
* We live | Vivimos
* Angry | Enfadado
* Always | Siempre
* Never | Nunca
* Fat | Gordo
* Sad | Triste
* Why | Por qué
* Because | Porque 
* So | Tan
* I live in Mexico | Vivo en México
* They live in Barcelona | Viven em Barcelona
* They are in Madrid | Están en Madrid
* They are angry | Están enfadados
* You are always happy | Siempre estás feliz
* He is never happy | Nunca está feliz
* They are always happy | Siempre están felices
* Why are you sad? | ¿Por qué estás triste?
* Because you are angry | Porque estás enfadada
* Why are they so fat? | ¿Por qué están tan gordos?
* They are fat because they eat too much chocolate | Están gordos porque comen demasiado chocolate
* She lives in Spain | Vive en España
* We live in Madrid | Vivimos en Madrid
* He is always angry | Siempre está enfadado
* I am never angry | Nunca estoy enfadada
* Why is he so sad? |  ¿Por qué está tan triste?
* Why are you so angry? | ¿Por qué estás tan enfadado?
* He is a little bit sad because he doesn't have a girlfriend | Está un poco triste porque no tiene novia 
* I am angry because I am hungry | Estoy enfadado porque tengo hambre
* The house | La casa
* Germany | Alemania
* The tie | La corbata
* To wear (*clothes*) | Vestir
* To wear (*shoes*) | Calzar
* I wear | Visto
* Sometimes | A veces
* Pink | Rosa
* White | Blanco
* Orange | Naranja
* He wears (*clothes*), she wears (*clothes*), you wear (*clothes, formal, sing*) | Viste
* The hair | El cabello
* Blonde | Rubio
* The eye | El ojo
* Brown | Marrón
* Grey | Gris
* The waiter | El camarero, la camarera
* The journalist | El periodista
* The doctor | El doctor
* The story | La historia
* To write | Escribir
* The day | El día
* He orders, she orders, you order (*sing. formal*) | Pide
* Take-away | Para llevar
* She has a house in Germany | Tiene una casa en Alemania
* We have a house in Mexico | Tenemos una casa en México
* I have a house in England | Tengo una casa en Inglaterra
* Sometimes I wear a red dress | A veces visto un vestido rojo
* Sometimes he wears white shoes | A veces calza zapatos blancos
* Sometimes they wear big pink shoes | A veces calzan zapatos rosas grandes
* She has blonde hair | Tiene cabello rubio
* She has brown eyes and grey hair | Tiene ojos marrones y cabello gris
* I am a journalist | Soy periodista
* He is a waiter | Es camarero
* I write stories every day | Escribo historias todas los días
* We drink coffee everyday | Bebemos café todos los días
* She orders take-away everyday | Pide comida para llevar todos los días
* Sometimes he wears an orange tie | A veces vista una corbata naranja
* Sometimes she wears a white dress | A veces viste un vestido blanco
* He has red hair and big eyes | Tiene cabello rojo y ojos grandes
* She has blonde hair and blue eyes | Tiene cabello rubio y ojos azules
* My dad is a doctor | Mi padre es doctor
* To love | Encantar
* To do, to make | Hacer
* He loves (*something*), she loves (*something*), you love (*something, formal, sing.*) | Le encanta
* To hate | Odiar
* Interested | Interesado
* The piece of news | La noticia
* To read | Leer
* I love (*more than one thing*) | Me encantan
* The newspaper | El periódico
* The magazine | La revista
* Our | Nuestro
* The art | El arte
* The music | La música
* The history | La historia
* The sports | Los deportes
* The film | La película
* The television show | El programa de televisión
* What does he love to do? | ¿Qué le encanta hacer?
* What do you hate to do? | ¿Qué odias hacer?
* What does your friend love to do? | ¿Qué le encanta hacer a tu amigo?
* What does her friend like to do? | ¿Qué le gusta hacer a su amigo?
* What are you interested in? | ¿En qué estás interesado?
* What are they interested in? | ¿En qué están interesados?
* I love to read | Me encanta leer
* He hates to read the paper | Odia leer el periódico
* Out father loves to read the newspaper | A nuestro padre le encanta leer el periódico
* Are you interested in art? | ¿Estás interesado en el arte?
* He is interested in music | Está interesado en la música
* I am really interested in history (*female*) | Estoy realmente interesada en la historia
* Are you interested in sports? | ¿Está interesada en los deportes?
* His girlfriend hates art and music (!) | Su novia odia el arte y la música
* I really love sports and music | Realmente me encantan los deportes y la música
* Their mother always watches the news | Su madre siempre ve las noticias
* I never watch films | Nunca veo películas
* What is he interested in? | ¿En qué está interesado?
* Mu mother loves reading magazines | A mi madre le encanta leer revistas
* My sister loves the news | A mi hermana le encanta leer las noticias
* We really hate history (!) | Realmente odiamos la historia
* She always watches a TV show | Siempre ve un programa de televisión 
* The newspaper | El diario
* The letter (*mail*) | La carta

## Tell me about you life

* Tell me (*a command*) | Cuénteme
* Tell me something about your life | Cuénteme algo sobre su via
* To move (*oneself*) | Mudar(se)
* He moved | Se mudó
* He moved to New York three weeks ago | Se mudó a Nueva York hace tres semanas
* To come back | Volver
* We came back | Volvimos
* Abroad | En el extranjero
* We came back to England three days ago | Volvimos al Inglaterra hace tres días
* My sister moved to Australia | Mi hermana se mudó al Australia
* I will be in Italy for about three months | Estaré en Italia durante tres meses aproximadamente
* Still | Todavía
* My family still has a house in Canada | Mi familia todavía tiene una casa en Canada
* Junto | Together
* My family usually have dinner together | Mi familia suele cenar junta
* My children still play together | Mis hijos todavía juegan juntos
* We still play badminton together on Wednesdays | Todavía jugamos al bádminton juntos los miércoles
* We still go to the theatre together on Fridays | Todavía vamos al teatro en los viernes
* To go skiing | Ir a esquiar
* To usually (*do something*) | Soler
* We usually go abroad in the winter | Solemos ir al extranjero en invierno
* They usually go skiing in Norway | Suelen ir a esquiar a Noruega
* Pretty (*less than 'really'*), especially (*bad*) | Bastante
* The career | La trayectoria
* I had a pretty good career five years ago | Tenía una trayectoria bastante buena hace cinco años
* My house in Australia was pretty big | Mi casa en Australia era bastante grande
* To retire (*oneself*) | Jubilar(se)
* To quit | Dimitir
* We will retire in about three years | Nos jubilaremos en tres años aproximadamente
* To go (*food*) shopping | Ir a hacer la compra
* My grandparents usually go shopping together | Mis abuelos suelen ir a hacer la compra juntos
* Our daughter was abroad for eight months | Nuestra hija estuvo en el extranjero durante ocho meses
* He will quit in about five weeks | Dimitirá en cinco semanas aproximadamente 
* They moved abroad thirty years ago | Se mudaron al extranjero hace treinta años
* To have dinner | Cenar
* My mum and dad still have dinner together on Sundays | Mi madre y mi padre todavía cenan juntos los domingos
* My mum will go to Canada in about two months | Mi madre irá a Canadá en dos meses aproximadamente
* His dad worked abroad for ten years | Su padre trabajó en el extranjero durante diez años
* We used to live abroad | Vivíamos en el extranjero
* To teach | Enseñar
* My wife used to teach Japanese | Mi esposa enseñaba japonés
* He used to (*do something on a regular basis*) | Solía
* My brother used to play basketball on Fridays | Mi hermano salía jugar al baloncesto los viernes
* My friend used to live in Africa | Mi amigo vivía en África
* You used to play football on Mondays | Solías jugar al fútbol los lunes
* To own | Poseer
* The business | El negocio
* We used to own a business | Poseíamos un negocio
* Not anymore | Ya no
* But not anymore | Pero ya no
* We owned (*imperfect past*) | Poseíamos
* We used to own a business but not anymore | Poseíamos un negocio pero ya no
* To be successful | Tener éxito
* He used to be successful but not anymore | Tenía éxito pero ya no
* When (*not a question*) | Cuando
* My grandparents went to Africa when they were young | Mis abuelos fueron a África cuando eran jóvenes 
* We used to live in America but we moved | Vivíamos en América pero nos mudamos
* The professor | El profesor, la profesora
* I was a Japanese teacher when I was young | Fui profesor de japonés cuando era joven
* I used to teach Chinese in Africa but not anymore | Enseñaba chino en África pero ya no
* My granddad played football when he was a kid | Mi abuelo jugaba al fútbol cuando era un niño
* My parents used to live here but they moved | Mis padres vivían aquí pero se mudaron
* They owned (*imperfect past*) | Poseían
* They used to own a car but not anymore | Poseían un coche pero ya no
* He quit (*past*) | Dimitió
* My dad used to work in the museum but he quit | Mi padre trabajaba en el museo pero dimitió
* We used to play badminton together on Wednesdays | Solíamos jugar al bádminton juntos los miércoles
* The company | La compañía
* The employee | El empleado
* Business trip | Viaje de negocios
* The boss | El jefe
* To do business | Hacer negocios

## Tell us about your holiday

* Tell us (*command*) | Contadnos
* Tell us something about your holiday | Contadnos algo sobre vuestras vacaciones
* America | América
* Europe | Europa
* Which countries did you visit? | ¿Qué países visitasteis?
* We went to France and Germany | Fuimos a Francia y a Alemania
* Why did you go to Europe? | ¿Por qué fuisteis a Europa?
* Diverse | Diverso
* The place | El lugar
* Because it is a very diverse place | Porque en un lugar muy diverso
* Remarkable | Excepcional
* How was your holiday? | ¿Qué tal fueron vuestras vacaciones?
* Why did you go to America? | ¿Por qué fuisteis a América?
* Famous | Famoso
* Because it is a remarkable place | Porque es un lugar excepcional 
* The experience | La experiencia
* Best, better | Mejor
* Parachuting | Hacer paracaidismo
* Which | Cuál 
* Which was your best experience? | ¿Cuál fue tu mejor experiencia? 
* I really enjoyed parachuting | Realmente disfruté haciendo paracaidismo
* Funny | Divertido
* Funniest, funnier | Más divertido
* Which was your funniest experience? | ¿Cuál fue vuestra experiencia más divertida?
* Scary | Escalofriante
* More scary, scariest | Más escalofriante
* To river raft | Hacer rafting
* We really enjoyed river rafting | Realmente disfrutamos haciendo rafting
* Hiking | Hacer senderismo 
* Strange | Extraño
* The best one | El mejor
* More strange, strangest | Más extraño
* The second restaurant was the best one | El segundo restaurante fue el mejor
* The festival | El festival
* Which was your strangest experience? | ¿Cuál fue vuestra experiencia más extraña?
* We much enjoyed the festival | Disfrutamos mucho el festival
* Definitely | Definitivamente
* The bus trip | La excursión en autobús
* The first bus trip was definitely the scariest one | El primera excursión en autobús fue definitivamente la más escalofriante
* Why did you go to New York? | ¿Por qué fuisteis a Nueva York? 
* Because it is a very famous place | Porque es un lugar muy famoso
* The hostel | El hostal
* Worse, worst | Peor
* The blue hostel was definitely the worst | El hostal azul fue definitivamente le peor
* The tour guide (*man, woman*) | El guía, la guía (*turístico, turística*)
* The first tour guide was definitely the funniest | El primer guía fue definitivamente el más divertido
* Which was your scariest experience? | ¿Cuál fue vuestra experiencia más escalofriante?
* We hated hiking | Odiamos hacer senderismo
* The cheapest restaurant was actually the best one | El restaurante más barato fue en realidad el mejor 
* The photo | La foto
* Do you guys want to see our photos? | ¿Queréis ver nuestras fotos?
* The video | El vídeo
* Do you guys want to see our videos? | ¿Queréis ver nuestros videos?
* The souvenir | El recuerdo
* The village | El pueblo
* Remote | Remoto
* We went to a remote village | Fuimos a un pueblo remoto
* The island | La isla
* We went to a remote island | Fuimos a una isla remota
* The animal | El animal
* There were a lot of animals on that island | Había un montón de animales en esa isla
* The elephant | El elefante
* This is a video of an elephant | Este es un vídeo de un elefante
* The picture, the photo | La imagen
* The lion | El león
* The kangaroo | El canguro
* This is a picture of a kangaroo | Esta es una imagen de un canguro
* Again | Otra vez
* Those | Esos
* Show us those photos again | Enseñadnos esas fotos otra vez
* Don't show (*subjunctive*) | No enseñéis
* Please don't show us those photos again | Por favor no nos enseñéis esas fotos otra vez
* Fantastic | Fantástico
* The view | La vista
* There was, there were | Había
* There was a fantastic view from our room | Había una vista fantástica desde nuestra habitación
* Incredible | Increíble
* The buffet | El buffet
* Show us that video again | Enseñadnos ese vídeo otra vez
* Please don't show us that video again | Por favor no nos enseñéis ese vídeo otra vez
* To hear | Oír
* I want to hear that story again | Quiero oír esa historia otra vez
* In that hotel there was an incredible buffet | En ese hotel había un buffet increíble
* We want to go to Asia again | Queremos ir a Asia otra vez
* The boat | El barco
* That is a video of a boat | Ese es un vídeo un barco
* Do you want to see our souvenirs? | ¿Queréis ver nuestros recuerdos?
* That is a picture of a lion | Esa es una imagen de un león
* Tell us that story again | Contadnos esa historia otra vez
* Don't tell (*subjunctive*) | No contéis
* Please don't tell us that story again | Por favor no nos contéis esa historia otra vez
* Never again, anymore | Nunca más 
* They never want to go to Australia again | No quieren ir a Australia nunca más
* A little bit | Un poco
* Surprised | Sorprendido
* I was a little surprised | Estaba un poco sorprendido
* Disappointed |  Decepcionado
* He was a little bit disappointed | Estaba un poco decepcionado
* Worried | Preocupado
* I got (*myself*) scared | Me asusté
* I got really scared | Me asusté mucho
* To drop (*by accident*) | Caer(se)
* I dropped (*by accident*) | Se me cayó
* The ice cream | El helado
* So (*adjective*) that… | Tanto que…
* I got so scared that I dropped my ice cream | Me asusté tanto que se me cayó mi helado
* To fall (*oneself*) asleep | Dormir(se)
* We fell (*ourselves*) asleep | Nos dormimos
* We were so tired that we fell asleep at eight o'clock | Estábamos tan cansados que nos dormimos a las ocho en punto
* To bring | Traer
* The toothbrush | El cepillo de dientes
* I brought  | Traje
* You brought | Trajiste
* I didn't bring my toothbrush | No traje mi cepillo de dientes
* The walking boots | Las botas de montaña
* You didn't bring your walking boots | No trajiste tus botas de montaña
* Excited, nervous | Nervioso
* The kids were so excited that they woke up at six o'clock | Los niños estaban tan nerviosos que se despertaron a las seis en punto
* By | Junto a 
* We went to a nice hotel by the sea | Fuimos a un hotel agradable junto al mar
* To fly | Volar
* We flew | Volamos
* My son got really sick | Mi hijo enfermó mucho
* My son got so scared that he cried | Mi hijo se asustó tanto que lloró
* The rain forest | La selva
* Over | Sobre
* I was a little bit worried | Estaba un poco preocupado
* The helicopter | EL helicóptero
* We flew over the rain forest in a helicopter | Volamos sobre la selva en un helicóptero
* The backpack | La mochila
* Our tour guide didn't bring his backpack | Nuestro guía no trajo su mochila
* Suddenly | De repente
* The toilet break | La pausa para ir al servicio
* Suddenly we needed a toilet break | De repente necesitamos una pausa para ir al servicio 
* Suddenly he got really scared | De repente se asustó mucho
* He got so scared that he suddenly needed toilet paper | Se asustó tanto que de repente necesitó el papel de baño
* The bridge | El puente
* Yesterday we flew over the bridge | Ayer volamos sobre el puente
* We were very exited | Estábamos muy nerviosos
* The sleeping bag | El saco de dormir
* My boyfriend didn't bring his sleeping bag | Mi novio no trajo su saco de dormir
* The border | La frontera
* By the border | Junto a la frontera
* We took a toilet break by the border | Hicimos una pausa para ir al servicio junto a la frontera
* I was in Asia for three months | Estuve en Asia durante tres meses
* They were in India for a year | Estuvieron en la India durante un año
* India is a huge country | La India es un país enorme
* To carry | Llevar
* Huge | Enorme
* He carried my huge backpack for three days | Llevó mi enorme mochila durante tres días
* To walk | Caminar
* Around (*something*) | Alrededor (*de algo*)
* We walked around the city in a day | Caminamos alrededor de la ciudad en un día
* To climb (*a mountain*) | Escalar (*una montaña*)
* My friend climbed the mountain in two days | Mi amiga escaló la montaña en dos días
* Awful | Horrible
* The hostel was awful | El hostal era horrible
* One and a half | Uno y medio
* They drove | Condujeron
* They drove around the lake in one and a half days | Condujeron alrededor del lago en un día y medio
* We ate everything on the table in three minutes | Comimos todo lo que había en la mesa en tres minutos
* Disappointing | Decepcionante
* the food was disappointing | La comida fue decepcionante
* We were in Africa for five weeks | Estuvimos en África durante cinco semanas
* The room was incredible | La habitación era increíble
* Canada is a huge country | Canadá es un país enorme
* We saw | Vimos 
* We saw all of India in two weeks | Vimos toda la India en dos semanas
* The jungle | La jungla
* To enter | Entrar
* We entered the jungle | Entramos en la jungla
* To get (*oneself into something*) | Meter(*se*) (*en algo*)
* We got (*outselves into something*) | Nos metimos (*en algo*)
* We got into the sea | Nos metimos en el mar
* We got into the water but it was to cold | Nos metimos en el agua pero estaba demasiado fría 
* The tree | El árbol
* The waterfall | La cascada
* Towards | Hacia
* We walked towards the trees | Caminamos hacia los árboles
* We walked towards the waterfall | Caminamos hacia la cascada
* We walked towards the waterfall but it was too dangerous | Caminamos hacia la cascada pero era demasiado peligroso
* The river | El río
* Right | Correcto
* It was the right river but not the right place | Era el río correcto pero no el lugar correcto
* It was not the right tree | No era el árbol correcto
* He was gone |  No estaba
* Suddenly our tour guide was gone | De repente nuestro guía no estaba
* Finally | Finalmente
* The road sign | La señal de tráfico
* Finally we found a road sign | Finalmente encontramos una señal de tráfico
* It was not the right place | No era el lugar correcto
* Finally we found the right place | Finalmente encontramos el lugar correcto
* The wallet | La cartera
* Suddenly my husband's wallet was gone | De repente la cartera de mi esposo no estaba
* To borrow | Coger prestado
* His wallet was gone so he had to borrow some money | Su cartera no estaba así que tuvo que coger algo de dinero prestado
* We followed the river | Seguimos el río
* To get (*oneself*) lost | Perder(*se*)
* We got (*ourselves*) lost | Nos perdimos
* We followed the river so we didn't get lost | Seguimos el río así que no nos perdimos
* Finally we found the right tree | Finalmente encontramos el árbol correcto
* I said | Dije
* He said | Dijo
* They said | Dijeron
* There were a lot of road signs | Había un montón de señales de tráfico
* We got lost (*subjunctive*) | Nos perdamos
* He said "I hope we don't get lost" | Dijo: "espero que no nos perdamos"
* There were a lot of road signs so we didn't get lost | Había un montón de señalas de tráfico así que no nos perdimos
* We walked towards the mountain but it was very far away | Caminamos hacia la montaña pero estaba muy lejos
* We saw | Vimos
* Finally we saw the river | Finalmente vimos el río
* They said "we want to follow the river" | Dijeron: "queremos seguir el río"
* I said "I want to go to the mountain" | Dije: "quiero ir a la montaña"
* The room | El cuarto
* Double | Doble
* Simple | Sencillo
* Can I reserve a room? | ¿Puedo reservar un cuarto?
* For how many people? | ¿Para cuántas personas?
* What's the total? | ¿Cuál es el total?

## What I Like and Don’t Like

* I like (*one thing*) | Me gusta
* I like (*multiple things*) | Me gustan
* I don't like | No me gusta…
* I like (*the*) bread | Me gusta el pan
* I like cinema | Me gusta el cine
* I like sport | Me gusta el deporte
* I really like | Me gusta mucho 
* I love | Me encantar
* I like to sing | Me gusta cantar
* I like to dance | Me gusta bailar
* I like to go out | Me gusta salir
* I like to read | Me gusta leer
* I like to eat | Me gusta comer
* I like to learn Spanish | Me gusta aprender español
* Music | La música
* Literature | La literatura
* Photography | La fotografía
* The cinema | El cine
* Chocolate | El chocolate
* Awesome | Increíble
* He is, she is, it is (*permanently*) | Es
* Delicious | Delicioso
* Possible | Posible
* Disgusting | Asqueroso
* Coffee | El café
* And | Y, e
* I don't like meat | No me gusta la carne
* I like oranges | No me gustan las naranjas
* I don't like apples | No me gustan las manzanas
* It is awesome | Es increíble
* It is not possible | No es posible
* Coffee is delicious | El café es delicioso
* Tea is disgusting | El té es asqueroso
* The chicken is delicious | El pollo es delicioso
* I like coffee and tea | Me gustan el café y el té
* I am awesome | Soy increíble
* I like fruit and vegetable | Me gustan las frutas y las verduras
* I don't like beer, beer is not very delicious and beer is disgusting | No me gusta la cerveza, la cerveza no es muy delicioso y la cerveza es asqueroso
* Himself, to him, herself, to her | Le
* Neither, nor | Ni
* Neither rice nor pork | Ni el arroz ni el cerdo
* Neither coffee nor tea | Ni el café ni el té
* Favourite | Favorito
* My | Mi
* All, every | Todo
* Actually | En realidad
* This | Este, esta
* That | Ese, esa, eso
* I know | Sé
* I don't know | No sé
* He likes coffee | Le gusta el café
* She doesn't like chicken nor pork | No le gustan ni el pollo ni el cerdo
* There is no food | No hay comida
* There is no milk | No hay leche
* Is there soup? | ¿hay sopa? 
* They are my favourite food | Son mis favoritos
* Bananas are my favourite food | Los plátanos son i comida favorita
* Actually I like all food | En realidad me gusta toda la comida
* This is my favourite | Este es mi favorito
* This is delicious | Este es delicioso
* That is beautiful | Esa es hermosa
* What is that? | ¿Qué es eso?
* What is this? | ¿Qué es esto?
* I don't know | No sé
* Of course he likes salad | Claro que le gusta la ensalada
* He likes pork | Le gusta el cerdo
* She doesn't like potatoes nor rice | No le gustan ni las patatas ni el arroz
* She doesn't eat meat | No come carne
* Lemons are my favourite food | Los limones son mi comida favorita
* Actually I like all vegetables | En realidad me gustan todas las verduras
* Actually I like all meat | En realidad me gusta toda la carne
* Good | Bueno
* Vegetarian | Vegetariano
* Hot | Caliente
* Cold | Frío
* Allergic | Alérgico
* Addicted | Adicto
* Do you like? | ¿Te gusta?
* It is very good | Es muy bueno
* It is delicious | Es delicioso
* I am vegetarian | Soy vegetariano
* Is the soup hot? | ¿La sopa está caliente?
* Is it vegetarian? | ¿Es vegetariano?
* Where are the nuts? | ¿Dónde están las nueces?
* Do you like cheese ? | ¿Te gusta el queso?
* He is allergic to nuts | Es alérgico a las nueces
* We are addicted to sugar | Somos adictos al azúcar
* She is addicted to cheese | Es adicta al queso
* Does he like it? | ¿Le gusta?
* It is good | Es bueno
* The pork is disgusting | El cerdo es asqueroso
* Is it hot? | Está caliente?
* Is the soup cold? | ¿La sopa está fría?
* She is allergic to cheese | Es alérgica al queso
* She is addicted to salt | Es adicta a la sal
* An order of | Una ración de
* A glass of wine | Una copa de vino

## Possessions

* To pick | Escoger
* The colour | El color
* Pick (*command*) | Escoge
* Pick your favourite animal | Escoge tu animal favorito
* Pick your favourite one | Escoge tu favorito
* The puppy | El perrito
* This is Juan's puppy now | Este es el perrito de Juan ahora
* That is my friend's house | Esa es la casa de mi amiga
* The kitten | El gatito
* That is my friend's bike | Esa es la bicicleta de mi amigo
* This is my mum's favourite colour | Este es el color favorito de mi madre
* These are Juan and Raquel's bikes now | Estas son las bicicletas de Juan y de Raquel ahora
* The slippers | Las zapatillas de andar por casa
* These are my dad's slippers | Estas son las zapatillas de andar por casa de mi padre
* Those are my son's books | Esos son los libros de mi hijo
* Cute | Mono
* Pick your favourite colour | Escoge tu color favorito
* That is Raquel's favourite film | Esa es la película favorita de Raquel
* Which colour is the prettiest one? | ¿Qué color es el más mono?
* Those are my dad's favorite shoes | Esos son los zapatos favoritos de mi padre
* Those are my friend's shoes | Esos son los zapatos de mi amigo
* That's Juan's problem now | Ese es el problem de Juan ahora
* Cool | Guay
* Which bike is the coolest one? | ¿Qué bicicleta es la más guay?
* These are my girlfriend's clothes | Esta es la ropa de mi novia
* This is my daughter's kitten | Este es el gatito de mi hija
* The pen | El bolígrafo
* Mine | Mío
* This is my pen | Este es mi bolígrafo
* This pen is mine | Este bolígrafo es mío
* The pants (*men's underwear*) | Los calzoncillos
* Are these my pants? | ¿Son estos mis calzoncillos?
* These pants are mine | Estos calzoncillos son míos
* The jumper | El jersey
* Yours | Tuyo
* That is your jumper | Ese es tu jersey
* Is that your jumper? | ¿Es ese jersey tuyo?
* The earring | El pendiente
* Hers, his, theirs, yours (*formal*) | Suyo
* Those are her earrings | Esos son sus pendientes
* Can those earrings be hers? | ¿Pueden esos pendientes ser suyos?
* The sock | El calcetín
* Actually those are his socks | En realidad esos son sus calcetines
* Those socks are his |  Esos calcetines son suyos
* The notepad | La libreta
* Ours | Nuestro
* We think that this is our notepad | Creemos que esta es nuestra libreta
* This notepad is ours | Esta libreta es nuestra
* The laptop | El portátil
* They are (*subjunctive, perm.*) | Sean
* We hope that these are their laptops | Esperamos que estos sean sus portátiles
* These laptops are theirs | Estos portátiles son suyos
* The skirt | La falda
* Yours | Vuestro
* These skirts are yours | Estas faldas son vuestras
* The bracelet | La pulsera
* This is my mum's bracelet | Esta es la pulsera de mi madre
* I think that this bracelet is my mum's | Creo que esta pulsera es de mi madre
* The necklace | El collar
* I think that that is my grandma's necklace | Creo que ese es el collar de mi abuela
* That necklace is my grandma's | Ese collar es de mi abuela
* Whose | De quién
* Whose are these pens? | ¿De quién son estos bolígrafo?
* The pillow | La almohada
* Whose are those pillows? | ¿De quién son esas almohadas?
* Which boots are mine? | ¿Qué botas son mías?
* Which jumper is his? | ¿Qué jersey es suyo?
* Purple | Morado
* The blue one is mine | El azul es mío
* These socks are ours | Estos calcetines son nuestros
* The purple ones are theirs | Los morados son suyos
* Broken | Roto
* The broken laptop is yours | El portátil roto es tuyo
* The duvet | El edredón
* Another | Otro
* I don't need another scarf | No necesito otra bufanda
* This duvet is yours | Este edredón es vuestro
* Those earrings are hers | Esos pendientes son suyos
* The video game | El videojuego
* He doesn't need another video game | No necesita otra videojuega
* Those shoes are ours | Esos zapatos son nuestros
* She doesn't need another skirt | No necesita otra falda
* My sister doesn't need another necklace | My hermana no necesita otro collar
* That pen is his | Ese bolígrafo es suyo
* Which bike is yours? | ¿Qué bicicleta es tuya?
* Whose are those pants? | ¿De quién son esos calzoncillos?
* The expensive ones are ours | Los caros son nuestros
* That car is theirs | Ese coche es suyo
* Which car is ours? | ¿Qué coche es nuestro?
* This pillow is mine | Esta almohada es mía
* The new one is yours | El nuevo es tuyo
* The trainers | Las zapatillas
de deporte
* Those trainers are mine | Esas zapatillas de deporte son mías 
* That necklace is hers | Ese collar es suyo
* These earrings are my wife's | Estos pendientes son de me esposa
* The blanket | La manta
* This blanket is Juan's | Esta manta es de Juan
* Those pills are not ours | Esas pastillas no son nuestras
* These shorts are not yours | Estos pantalones cortos no son tuyos
* The toy | El juguete
* That toy is not my son's | Ese juguete no es de mi hijo
* Those toys are for him | Esos juguetes son para él
* The flower | La flor
* This flower is for my mum | Esta flor es para mi madre
* This passport is not my husband's | Este pasaporte no es de mi esposo
* These flowers are for them | Estas flores son para ellos
* That ring is for my girlfriend | Ese anillo es para mi novia
* This house is theirs now |  Esta casa es suya ahora
* The coin | La moneda
* He has some coins in his pocket | Tiene algunas monedas en el bolsillo
* Those coins are hers | Esas monedas son suyas
* Was the smallest puppy Raquel's | ¿Era el perrito más pequeño de Raquel?
* The cookie, the biscuit | La galleta
* Were the chocolate cookies yours? | ¿Eran tuyas las galletas de chocolate?
* Whose was the red scarf? | ¿De quién era la bufanda roja?
* Dirty | sucio
* Whose were the dirty socks? | ¿De quién eran los calcetines sucios?
* The fridge | El frigorífico
* My mum always had cookies in the fridge | Mi madre siempre tenía galletas en el frigorífico
* Clean | Limpio
* My grandma always had biscuits in her bag | Mi abuela siempre tenía galletas en su bolso
* Those biscuits were mine | Esas galletas eran mías
* This house is not ours anymore | Esta casa ya no es nuestra 
* The cupboard | El armario (*de la cocina*)
* Sometimes my mum has cookies in the cupboard | A veces mi madre tiene galletas en el armario
* The freezer | El congelador
* The black kitten is mine now | El gatito negro es mío ahora
* I think the chicken in the freezer was Juan's | Creo que el pollo en el congelador ear de Juan
* That dirty puppy is not ours | Ese perrito sucio no es nuestro
* He thinks the ice cream in the freezer was his | Cree que el helado del congelador ear suyo
* My dad always had clean hands | Mi padre siempre tenía las manos limpias
* The clean fridge is ours | El frigorífico limpio es el nuestro

## In the house

* The toilet | El servicio
* The bathroom | El baño
* Between | Entre
* Next to | Junto a
* The bedroom | La habitación
* The kitchen | La cocina
* The living room | El salón
* Where are the toilets? | ¿Dónde están los servicios?
* It is next to the kitchen | Está junto a la cocina
* Where is the bedroom? | ¿Dónde está la habitación?
* They are between the kitchen and the living room | Están entre la cocina y el salón
* Where is the bathroom? | ¿Dónde está el baño?
* Where is the bank? ¿Dónde está el banco?
* It is between the bedroom and the toilets | Está entre la habitación y los servicios
* It is next to the supermarket | Está junto al supermercado
* The chair | La silla
* The bed | La cama
* The door | La puerta
* The room, the bedroom | La habitación
* You know | Sabes
* The key | La llave
* The sofa | El sofá
* Under | Debajo de
* Behind | Detrás de
* In front of | Delante de
* A window | Una ventana
* How many | Cuántos
* On, in | En
* How many chairs are there in the living room? | ¿Cuántas sillas hay en el salón?
* How many beds are there in the bedroom? | ¿Cuántas camas hay en la habitación?
* Do you know where I left my keys? | ¿Sabes dónde están mis llaves?
* Do you know where my shoes are? | ¿Sabes dónde están mis zapatos?
* You keys are under the sofa | Tus llaves están debajo del sofá
* Your dress is behind the bed | Tu vestido está detrás de la cama
* Your keys are in front of the television | Tus llaves están delante de la televisión
* You books are under the sofa | Tus libros están debajo del sofá
* How many windows are there in the house? | ¿Cuántas ventanas hay en la casa?
* How many books are there in your bedroom? | ¿Cuántos libros hay en tu habitación?
* How many doors are there in the kitchen? | ¿Cuántas puertas hay en la cocina?
* Do you know where my bag is? | ¿Sabes dónde está mi bolso?
* How many knives are there on the table? | ¿Cuántos cuchillos hay en la mesa?
