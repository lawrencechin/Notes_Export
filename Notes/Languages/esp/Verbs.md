# Verbs

## 'Ser' vs 'Estar'

* To be (*permanently*) | Ser
* To be (*temporarily*) | Estar
* I am (*perm*) | (*Yo*) soy
* You are (*perm, sing, inform*) | (Tú) eres
* He is (*perm*) | (*Él*) es
* She is (*perm*) | (*Ella*) es
* You are (*perm, form*) | (*Usted*) es
* We are (*perm*) | (*Nosotros*) somos
* You are (*perm, plural, inform*) | (*Vosotros*) sois
* They are (*perm*) | (*Ellos*) son
* You are (*perm, plural, formal*) | (*Ustedes*) son
* I am (*temp*) | (*Yo*) estoy
* You are (*temp, sing, informal*) | (*Tú*) estás
* He is (*temp*) | (*Él*) está
* She is (*temp*) | (*Ella*) está
* You are (*temp, formal, sing*) | (*Usted*) está
* We are (*temp*) | (*Nosotros*) estamos
* You are (*temp, plural, informal*) | (*Vosotros*) estáis
* They are (*temp*) | (*Ellos*) están
* You are (*temp, formal, plural*) | (*Ustedes*) están

## Verbs with '-er'

* To drink | Beber
* To think | Creer
* I drink | Bebo 
* I think | Creo
* You drink (*sing. informal*) | Bebes
* You think (*sing. informal*) | Crees
* He drinks, she drinks, you drink (*sing. formal*) | Bebe
* He thinks, she thinks, you think (*sing. formal*) | Cree
* We drink | Bebemos
* We think | Creemos
* You drink (*pl. informal*) | Bebéis
* You think (*pl. informal*) | Creéis
* They drink, you drink (*pl. formal*) | Beben
* They think, you think (*pl. formal*) | Creen

## 'Necesitar', 'Encantar'

* To need | Necesitar
* To love | Encantar
* I need | Necesito
* I love | Me encanta
* You need (*sing. informal*) | Necesitas
* You love (*sing. informal*) | Te encanta
* He needs, she needs, you need (*sing. formal*) | Necesita
* He loves, she loves, you love (*sing. formal*) | Le encanta
* We need | Necesitamos
* We love | Nos encanta
* You need (*plu. informal*) | Necesitáis
* You love (*plu. informal*) | Os encanta
* They need, you need (*plu. formal*) | Necesitan
* They love, you love (*plu. formal*) | Les encanta

## Verbs With '-ar' and '-ir'

* To speak | Hablar
* To work | Trabajar
* To live | Vivir
* I speak | Hablo
* I work | Trabajo
* I live | Vivo
* You speak (*sing. informal*) | Hablas
* You work (*sing. informal*) | Trabajas
* You live (*sing. informal*) | Vives
* He speaks, she speaks, you speak (*sing. formal*) | Habla
* He works, she works, you work (*sing. formal*) | Trabaja
* He lives, she lives, you live (*sing. formal*) | Vive
* We speak | Hablamos
* We work | Trabajamos
* We live | Vivimos
* You speak (*plu. informal*) | Habláis
* You work (*plu. informal*) | Trabajáis
* You live (*plu. informal*) | Vivís
* They speak, you speak (*plu. formal*) | Hablan
* They work, you work (*plu. formal*) | Trabajan
* They live, you live (*plu. formal*) | Viven

## Reflexive Verbs - 'Gustar'

* To like | Gustar
* Would like | Gustaría
* I like (*one thing*), (*one thing*) pleases me | Me gusta
* I would like | Me gustaría
* You like (*one thing, sing. informal*) | Te gusta
* You would like (*sing. informal*) | Te gustaría
* He likes (*one thing*), she likes, you like (*sing. formal*) | Le gusta
* He would like, she would like, you would like (*sing. formal*) | Le gustaría
* We like (*one thing*) | Nos gusta
* We would like | Nos gustaría
* You like (*one thing, plu. informal*) | Os gusta
* You would like (*plu. informal*) | Os gustaría
* They like (*one thing*), you like (*one thing, plu. formal*) | Les gusta
* They would like, you would like (*plu. formal*) | Les gustaría

## Reflexive Verbs

* To be called | Llamarse 
* To stay | Quedarse 
* To fall | Caerse 
* To resemble, look like | Parecerse 
* To brush | Cepillarse 
* To get into | Meterse 
* To sit | Sentarse 
* To go to bed | Acostarse 
* To remember | Acordarse 
* To get rid of | Deshacerse de
* To dare | Atreverse 
* To complain | Quejarse 
* To behave yourself | Portarse 
* To find out | Enterarse 
* To put, to set, to become | Ponerse 
* To become aware of, to discover | Darse cuenta
* To make a mistake | Equivocarse 
* To scratch | Rascarse 
* To slip, slide | Resbalarse 
* To escape | Escaparse 
* To imagine | Imaginarse 
* To get down | Bajarse (de)
* To get up, to climb | Subirse (a)
* To hurry | Apurarse 
* To be glad | Alegrarse 
* To get even, to retaliate | Desquitarse (de)
* To appear, look | Verse 
* To blush, to flush | Ruborizarse 
* To become | Volverse 
* To care of yourself | Cuidarse 
* To get lost | Perderse 
* To cut yourself | Cortarse 
* To wash yourself | Lavarse 
* To dry oneself | Secarse 
* To burn yourself | Quemarse 
* To hurt yourself | Lastimarse 
* To take (*a look*), To notice | Fijarse
* To breathe | Alentarse
* To save | Ahorrar(se)
* To escape, to slip out | Escabullirse
* To faint, to become downhearted, to become demoralized | Desmayarse
* To kneel | Arrodillarse
* To dive (*in*) | Zambullirse
* To get fed up | Hartarse
* To file (*nails*) | Limarse (*uñas*)
* To spread, to propagate, to disseminate | Propagarse

## Conditional Verbs - 'Poder', 'Deber'

* I could | Podría
* You could (*sing. informal*) | Podrías
* He/she could, you could (*sing. formal*) | Podría
* We could | Podríamos
* You could (*plu. informal*) | Podríais
* They can, you can (*plu. formal*) | Podrían
* I should | Debería
* You should (*sing. informal*) | Deberías
* He should, she should, you should (*sing. formal*) | Debería
* We should | Deberíamos
* You should (*plu, informal*) | Deberíais
* They should, you should (*plu, formal*) | Deberían

## Irregular Verbs - 'Querer' 'Tener' and 'Poder', 

* To want | Querer
* I want | Quiero
* You want (*sing. informal*) | Quieres
* He wants, she wants, you want (*sing. formal*) | Quiere
* We want | Queremos
* You want (*plu. informal*) | Queréis
* They want, you want (*plu. formal*) | Quieren
* To have | Tener
* I have | Tengo
* You have (*sing. informal*) | Tienes
* He have, she have, you have (*sing. formal*) | Tiene
* We have | Tenemos
* You have (*plu. informal*) | Tenéis 
* They have, you have (*plu. formal*) | Tienen
* Can | Poder
* I can | Puedo
* You can (*sing. informal*) | Puedes
* He can, she can, you can (*sing. formal*) | Puede
* We can | Podemos
* You can (*plu. informal*) | Podéis
* They can, you can (*plu. formal*) | Pueden

## Irregular Verbs - 'Dar', 'Ir', 'Ver'

* To give | Dar
* I give | Doy
* You give (*sing. informal*) | Das
* He gives, she gives, you give (*sing. formal*) | Da
* We give | Damos
* You give (*plu. informal*) | Dais
* They give, you give (*plu. formal*) | Dan
* To go | Ir
* I go | Voy
* You go (*sing. informal*) | Vas
* He goes, she goes, you go (*sing. formal*) | Va
* We go | Vamos
* You go (*plu. informal*) | Vais
* They go, you go (*plu. formal*) | Van
* To see, to watch | Ver
* I see, I watch | Veo
* You see, you watch (*sing. informal*) | Ves
* He sees, he watches, she sees, she watches, you see, you watch (*sing. formal*) | Ve
* We see, we watch | Vemos
* You see, you watch (*plu. informal*) | Veis
* They see, they watch, you see, you watch (*plu. formal*) | Ven

## Irregular Verbs and First Person Present Nouns

* To govern | Gobernar
* The government | El gobierno
* To freeze | Helar
* The ice | El hielo
* To tell | Contar
* The story | El cuento
* To fly | Volar
* The flight | El vuelo
* To console | Consolar
* The consolation | El consuelo
* To discount | Descontar
* The discount | El descuento
* To untie, to let loose | Soltar
* Loose change | El suelto
* To dream | Soñar
* The dream | El sueño
* To thunder | Tronar
* The thunder | El trueno
* To encounter | Encontrar
* The encounter | El encuentro
* To remember | Recordar
* The remembrance | El recuerdo
* To play | Jugar
* The game | El juego
* To squeeze | Apretar
* The squeeze | El aprieto
* To bet | Apostar
* The bet | La apuesta
* To show | Mostrar
* The sample | La muestra
* To taste, to prove | Probar
* The proof | La prueba
* To roll | Rodar
* The wheel | La rueda
* To count | Contar
* The bill | La cuenta

## Imperative (*commands*)

* Speak Spanish with me! (*imperativa*) | ¡Hable Español conmigo!
* Buy the book! | ¡Compre el libro!
* This (*without a noun*) | Esto
* I'm going to buy this | Voy a comprar esto
* Don't buy it! | ¡No lo compre!
* Why don't you sell it? | ¿Por qué no lo vende?
* Don't sell it! | ¡No lo venda!
* Don't sell it! (*plural*) | ¡No lo vendan!
* Don't sell it! (*informal*) | ¡No lo vendas!
* Sell that book! | ¡Venda ese libro! 
* Buy that book! | ¡Compre ese libro!
* Don't buy it! | ¡No lo compre!
* (for a positive command, pronouns are hooked on to the verb)
* Buy it! | ¡Comprelo!
* Buy it! (*plural*) | ¡Comprenlo!
* Buy them! (*talking to several people*) | ¡Comprenlos!
* Don't buy them! (*talking to several people*) | ¡No los compren!
* Why don't you take it? | ¿Por qué no lo toma?
* Don't take it! | ¡No lo tome!
* Take it! | ¡Tomelo!
* Eat it! | ¡Comalo!
* Call me later please! | ¡Llameme más tarde por favor!
* Call me! | ¡Llameme!
* (*in the imperative, the only time you don't switch tracks is when speaking to "you, friend".  You also miss out the last -s*)
* Call me! (*friend*) | ¡Llamame!
* Call me! | ¡Llameme!
* Wait! | ¡Espere!
* Wait for me here! | ¡Espereme aquí!

## Irregular Verbs with 'go/ga' endings

* I come | Vengo
* I am leaving | Salgo
* To oppose | Oponer
* To compose | Componer
* I put | Pongo
* I am putting it here | Lo pongo aquí
* I do | Hago
* I tell | Digo
* I am doing it | Lo hago
* I am telling you | Le digo
* I am bringing | Traigo
* (*in the imperative, the -go verbs turn into -ga verbs*)
* Come with me! | ¡Venga conmigo!
* Don't leave! | ¡No salga!
* Don't leave! (*plural*) | ¡No salgan!
* Don't leave! (friend) | ¡No salgas!
* Don't put it here! | ¡No lo ponga aquí!
* Put it here! | ¡Pongalo aquí!
* Can you put it here, will you please put it here | Puede ponerlo aquí
* Bring it! | ¡Traigalo!
* Bring me something! | ¡Traigame algo!
* Don't put it here! | ¡No lo ponga aquí!
* Put it there! | ¡Pongalo allí!
* Don't put it here, put it there! | ¡No lo ponga aquí, pongalo allí!

## Verbs that change from 'e' to 'ie' during conjugation

* To come | Venir
* We are coming | Venimos
* He is coming | Viene
* They are coming | Vienen
* You are coming (*friend*) | Vienes
* I am coming | Vengo
* I am starting | Comienzo
* You are starting, he is starting, she is starting, it is starting | Comienza
* At what time are you starting? | ¿A qué hora comienza?
* At what time are you all starting? | ¿A qué hora comienzan?
* At what time are you starting? (*friend*) | ¿A qué hora comienzas?
* At what time do we start? | ¿A qué hora comenzamos?
* I am starting | Empiezo
* At what time are you starting? | ¿A qué hora empieza?
* I think | Pienso
* What do you think? | ¿Qué piensa?
* What do you think of the situation? | ¿Qué piensa de la situación?
* (*pensar is also used in the sense of thinking forward, or planning*)
* I plan on leaving soon | Pienso salir pronto
* When do you plan on leaving? | ¿Cuándo piensa salir?
* I don't understand it | No lo entiendo
* I don't understand you | No le entiendo
* I don't understand you (*friend*) | No te entiendo
* Do you understand me? (*friend*) | ¿Me entiendes?
* We don't understand | No entendemos
* (*usually you can deduce that the whole verb contains the sound -e-, if the present tense verb contains -ie-*)

## Verbs that change from 'o' to 'ue' during conjugation

* I can | Puedo
* I find | Encuentro
* I don't find it | No lo encuentro
* I remember | Recuerdo
* I am coming back soon | Vuelvo pronto
* At what time are you coming back? | ¿A qué hora vuelve?
* Are you all coming back? | ¿Vuelven?
* Are you coming back? (*friend*) | ¿Vuelves?
* We are coming back soon | Volvemos pronto

## Radical Changing Verbs

* To compete | Competir
* To take leave of | Despedirse
* To impede | Impedir
* To measure | Medir
* To melt | Derretir
* To fry | Freír
* To laugh | Reírse
* To grin | Sonreírse
* To moan | Gemir
* To consent, to spoil | Consentir
* To repent | Arrepentirse
* To digest | Digerir
* To have a good time | Divertirse
* To boil | Hervir
* To pervert | Pervertir
* To squeeze | Apretar
* To break (*q…*) | Quebrar
* To heat | Calentar
* To irritate, to sprinkle | Regar
* To regiment | Regimentar
* To disconcert | Desconcertar
* To mend | Remendar
* To burst | Reventar
* To lock in, to enclose | Encerrar
* To sow, to seed | Sembrar
* To bury | Enterrar
* To tremble | Temblar
* To tempt, to touch | Tentar
* To freeze | Helar
* To concern | Concernir
* To light (*a fire*) | Encender
* To hang out (*clothes*) | Tender
* To stink | Heder
* To bet | Apostar
* To approve | Aprobar
* To renew | Renovar
* To strain (*juice*) | Colar
* To roll | Rodar
* To let loose | Soltar
* To thunder | Tronar
* To move (*emotionally*) | Conmover 
* To bite | Morder
* To grind | Moler
* To move | Mover
* To blind | Cegar
* To calm, to quiet | Sosegar
* To hang | Colgar
* To beg, to implore | Rogar
* To overturn | Volcar
