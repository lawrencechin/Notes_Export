# Other Verbs

These verbs have been moved from the main "*Verbs*" file in order to make it easier to review them as flash cards.

* To brake | Frenar
* To pressure | Presionar
* To beg | Suplicar
* To compromise | Comprometer
* To honor | Honrar
* To threaten | Amenazar
* To grow | Crecer
* To be born | Nacer
* To move | Mover
* To send | Enviar
* To meow, to mew | Maullar
* To define | Definir
* To creep, to crawl, to slither | Reptar
* To pay | Pagar
* To touch | Tocar
* To finish | Acabar
* To search | Buscar
* To fit | Caber
* To begin (*alt*) | Comenzar
* To blame | Culpar
* To depend | Depender
* To begin | Empezar
* To deliver | Entregar
* To sign | Firmar
* To matter | Importar
* To include | Incluir
* To look at | Mira
* To show | Mostrar
* To think | Pensar
* To weigh | Pesar
* To introduce | Presentar
* To remain | Quedar
* To require | Requerir
* To feel | Sentir
* To serve | Servir
* To dream | Soñar
* At least, even | Siquiera
* To pass, to happen (*p…*) | Pasar
* To explain | Explicar
* To receive | Recibir
* To decide | Decidir
* To achieve, to reach (*l…*) | Lograr
* To reach (*a…*) | Alcanzar
* To happen (*o…*) | Ocurrir
* To begin (*i…*) | Iniciar
* To fall | Caer
* To get | Conseguir
* To allow | Permitir
* To obtain | Obtener
* To answer (*c…*) | Contestar
* To take out | Sacar
* To deny | Negar
* To consider | Considerar
* To try (*i…*) | Intentar
* To insist | Insistir
* To offer | Ofrecer
* To doubt | Dudar
* The fill | Llenar
* To save | Salvar
* To return, to come back (*r…*) | Regresar
* To continue | Continuar
* To possess, to own | Poseer
* To mix | Mezclar
* To stop (*p…*) | Parar
* To add (*a…*) | Añadir
* To use (*u…, long*) | Utilizar
* To cost, to be worth (*c…*) | Costar
* To contain | Contener
* To spend | Gastar
* To affect, to upset, to damage | Afectar
* To improve | Mejorar
* To desire (*d…*) | Desear
* To observe | Observar
* To consult | Consultar
* To fly | Volar
* To create | Crear
* To think, to believe (*c…*) | Creer
* To reserve | Reservar
* To express | Expresar
* To recognise | Reconocer
* To take an interest, to become interested | Interesarse
* To cost, to be worth (*v…*) | Valer
* To handle, to drive | Manejar
* To reject | Rechazar
* To treat | Tratar
* To use (*u…, short*) | Usar
* To belong | Pertenecer
* To walk, to go (*a…*) | Andar
* To lift | Levantar
* To need | Necesitar
* To rest | Descansar
* To hope, to wait | Esperar
* To throw, to toss | Tirar
* To turn | Girar
* To defeat | Derrotar
* To heal, to cure | Curar
* To jump | Saltar
* To dry | Secar
* To rent | Alquilar
* To wake up | Despertar
* To avoid, to prevent | Evitar
* To maintain, to support, to hold | Mantener
* To establish | Establecer
* To solve, to resolve | Resolver
* To produce | Producir
* To cut (*p…*) | Partir
* To understand | Entender
* To reduce | Reducir
* To forget | Olvidar
* To abandon | Abandonar
* To increase | Aumentar
* To learn | Aprender
* To overcome | Superar
* To control | Controlar
* To recover | Recuperar
* To discover | Descubrir
* To apply | Aplicar
* To demonstrate, to prove | Demostrar
* To elect, choose | Elegir
* To defend | Defender
* To sum, to add | Sumar
* The stop, to detain (*d…*) | El detener
* To publish | Publicar
* To dedicate | Dedicar
* To wound | Herir
* To have (*h…*) | Haber
* To send | Enviar
* To be born | Nacer
* To force | Obligar
* To carry out | Realizar
* To quote, to cite | Citar
* To develop | Desarrollar
* To suffer | Sufrir
* To determine | Determinar
* To foresee | Prever
* To form | Formar
* To manage, to run | Dirigir
* To suppose | Suponer
* To participate | Participar
* To build | Construir
* To act | Actuar
* To kill | Matar
* To analyse | Analizar
* To place | Colocar
* To sell | Vender
* To go up, to climb, to get on | Subir
* To define | Definir
* To check, to prove | Comprobar
* To assume | Asumir
* To cover | Cubrir
* To look after, to attend to | Atender
* To prevent | Impedir
* To break | Romper
* To remove, to eliminate | Eliminar
* To exist | Existir
* To have, must, to owe | Deber
* The agree | Acordar
* To remove, to peel off | Despegar
* To find out | Averiguar
* To fill, to swell | Henchir
* To occur, to happen | Suceder
* To become, to convert | Convertir
* To find | Hallar
* To enjoy | Gozar
* To throw | Arrojar
* To trivialise | Banalizar
* To brand | Tildar
* To stink | Apestar
* To demand | Exigir
* To encourage, to cheer on | Animarse
* To hit, to stick, to give | Pegar
* To rip out, to tear off, to extract | Arrancar
* To get tangled up, to complicate | Enredar
* To pretend, to fake | Fingir
* To defeat, to vanquish | Vencer
* To hold, to grab | Agarrar
* To do without | Prescindir
* To not give a damn (*vulgar*) | Sudar
* To urge | Instar
* To hold, to accommodate | Albergar
* To shake | Sacudir
* To make good use of | Aprovechar
* To suck | Chupar
* To growl, to grumble | Gruñir
* To bother, to mess up, to pester | Fastidiar
* To howl | Aullar
* To take out, to get out, to draw | Sacar
* To fix (*on*) | Fijar
* To burst, to puncture | Pinchar
* To feel like | Apetecer
* To endeavour, to try | Procurar
* To spill, to shed, to scatter | Derramar
* To cheer on, to encourage | Alentar
* To turn to, to appeal | Recurrir
* To make … look like, to compare, to liken | Asemejar
* To turn over, to winnow | Voltear
* To exhaust, to run out | Agotar
* To slip, to slide, to glide | Deslizar
* To melt, to smelt, to cast, to blow | Fundir
* To prune | Podar
* To knock down, to demolish, to pull down | Derribar
* To block | Atascar
* To whistle | Silbar
* To bribe | Sobornar
* To crack, to tear, to rip | Rajar
* To press | Oprimir
* To set sail, to weigh anchor | Zarpar
* To water | Regar
* To embark on, to undertake | Emprender
* To beat (*heart*), to pulsate | Latir
* To suffer from | Padecer
* To rewind | Rebobinar
* To carry out, to keep | Cumplir
* To fell, to cut down | Talar
* To dress up, to disguise | Disfrazar
* To cross out, to delete | Tachar
* To flirt | Coquetear
* To yawn | Bostezar
* To postpone, to put off | Aplazar
* To subject | Someter
* To furnish | Amueblar
* To decipher | Descifrar
* To sniff | Olfatear
* To frighten off | Ahuyentar
* To boast | Jactarse
* To praise | Alabar
* To cast a spell, to captivate | Hechizar
* To dare | Osar
* To slap | Abofetear
* To let up/ease raining | Escampar
* To flatter | Halagar
* To mess up, to fluff, to goof | Pifiar
* To underline, to highlight | Subrayar
* To alter the course of, to divert | Desviar
* To predict, to forecast | Vaticinar
* To fill to the brim, to fulfill | Colmar
* To make narrow | Estrechar 
* To crack | Agrietar
* To rub out, to erase | Borrar
* To sand, to polish | Pulir
* To challenge, to dare | Desafiar
* To ride | Cabalgar
* To pay, to yield, to produce | Rendir
* To turn up | Acudir
* To hire | Contratar 
