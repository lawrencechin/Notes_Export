# Activities and Events

## Asking Directions

* The train station | La estación de tren
* The city centre | El centro de la ciudad
* The park | El parque
* Over there | Ahí
* The problem | El problema
* The map | El mapa
* The guide book | La guía
* The foreigner | El extranjero
* Lost | Perdido
* Confused | Confuso
* The bar | El bar
* The theatre | El teatro
* The library | La biblioteca
* To find | Encontrar
* Where is the train station? | ¿Dónde está la estación de tren?
* Where is the city centre? | ¿Dónde está el centro de la ciudad?
* The park is over there | El parque está ahí
* There is a problem | Hay un problema
* Do you have a map? | ¿Tienes un mapa?
* We have a problem | Tenemos un problema
* I have a guide book | Tengo una guía
* I am a foreigner | Soy extranjero
* We are lost | Estamos perdidos
* I am a little confused | Estoy un poco confusa
* Where are you going? | ¿A dónde van?
* I am going to Spain | Voy a España
* I am going to eat something | Voy a comer algo
* She is going to a bar | Va a un bar
* The bar is over there | El bar está ahí
* I am going to the theatre | Voy al teatro
* I want to find the library | Quiero encontrar la biblioteca
* I want to find my friends | Quiero encontrar a mis amigos
* He wants to find his friends | Quiere encontrar a sus amigos
* Actually we have a map | En realidad tenemos un mapa
* He is a foreigner | Es extranjero
* Where is she going? | ¿A dónde va?
* We are going to the train station | Vamos a la estación de tren 
* She is going to the city centre | Va al centro de la ciudad
* He wants to find the park | Quiere encontrar el parque
* She wants to find the bar | Quiere encontrar el bar
* To ask | Preguntar
* Turn (*sing. formal*) | Gire
* Left | Izquierda
* Right | Derecha
* The street | La calle
* Follow (*sing. formal*) | Siga
* Go (*sing. formal*) | Vaya
* Straight | Recto
* Slowly | Despacio
* Stop (*sing. formal*) | Pare
* Can I ask you something? | ¿Puedo preguntarle algo?
* Turn left | Gire a la izquierda
* Turn right | Gire a la derecha
* Follow this street | Siga esta calle
* Follow that car | Siga ese coche
* Godspeed | Vaya con dios
* Go straight | Vaya recto
* Can you speak slower please? | ¿Puede hablar más despacio por favor?
* Stop here please | Pare aquí por favor
* Close, near | Cerca
* A museum | Un museo
* To repeat | Repetir
* The church | La iglesia
* Right here | Aquí mismo
* Far away, miles away | Muy lejos
* The hotel | El hotel
* Is there a museum near here? | ¿Hay un museo cerca de aquí?
* Can you repeat that please? | ¿Puede repetir eso por favor?
* Where is the museum? | ¿Dónde está el museo?
* It is over there | Está ahí
* The hotel is over there | El hotel está ahí
* The museum is far away | El museo está muy lejos
* The church is near the bear | La iglesia está cerca del bar
* It is miles away | Está muy lejos
* Where is the church? | ¿Dónde está la iglesia?
* It is right here | Está aquí mismo
* Actually it is very close | En realidad está muy cerca
* The train station is near my house | La estación de tren está cerca de mi casa
* The city centre is miles away | El centro de la ciudad está muy lejos
* How can I get to the supermarket? | ¿Cómo puedo llegar al supermercado?
* Excuse me, I'm lost. Can you help me? | Disculpe, estoy perdido. ¿Puede ayudarme?
* I'm looking for the bathroom, where is it? | Estoy buscando el servicio. ¿Dónde está?
* Can you tell me where the money exchange is? | ¿Puede decirme dónde está la casa de cambio?
* The address | La dirección
* Take me to this address | Lléveme a esta dirección
* I need to go to the airport | Necesito ir al aeropuerto
* Stop here (*alt*) | Deténgase aquí
* Can you do it like this? | ¿Lo puedes hacer así?
* The track, road | El camino
* Inside | Adentro
* Left side | Lado izquierdo
* Right side | Lado derecho
* Above, on top | Arriba, encima
* Below, under | Abajo, debajo
* The side | El lado
* The center | El centro
* The position | La posición
* The entrance | La entrada
* The outside | El exterior
* The exit | La salida
* The arrival | La llegada
* The traffic | El tráfico
* The orientation | La orientación
* The end (*f…*) | El fin
* The sign, the signal | La señal
* The course (*r…*) | El rumbo
* The step | El paso

## Buying Things

* To buy | Comprar
* I want | Quiero
* The thing | La cosa
* The market | El mercado
* To go shopping | Ir de compras
* There is, there are | Hay
* The ATM | El cajero
* The pharmacy | La farmacia
* The kiosk | El quiosco
* The people | La gente
* The shop | La tienda
* The factory | La fabrica
* The office | La oficina
* The restaurant | El restaurante
* The hospital | El hospital
* Many | Muchos
* The supermarket | El supermercado
* The car | El coche
* The bus (*o…*) | El ómnibus
* The truck (*the bus in México*) | El camión
* The elevator | El elevador
* The taxi | El taxi
* The sidewalk | La acera
* The basket | La canasta
* The bank | El banco
* The bookshop | La librería
* I want to buy something | Quiero comprar algo
* I want to buy a beer | Quiero comprar una cerveza
* What thing? | ¿Qué cosa?
* Let's go to the market | Vamos al mercado
* Let's go shopping | Vamos de compras
* There is a big pharmacy | Hay una farmacia grande
* There is a small kiosk | Hay un quiosco pequeño
* There are many people | Hay mucha gente
* There are many shops | Hay muchas tiendas
* Is there a supermarket? | ¿Hay un supermercado?
* Is there an ATM? | ¿Hay un cajero?
* There are no banks | No hay bancos
* There are no desserts | No hay postres
* There are many big cars | Hay muchos coches grandes
* There are many small bookshops | Hay muchas librerías pequeñas
* Blue | Azul
* Yellow | Amarillo
* Black | Negro
* Green | Verde
* Red | Rojo
* The red wine | La vino tinto
* To pee | Mear
* Closed | Cerrado
* Open | Abierto
* I want the blue shirt | Quiero la camisa azul
* Do you want a shirt ? | ¿Quieres la camisa?
* Do you want a bottle of wine? | ¿Quieres una botella de vino? 
* He wants the blue trousers | Quiere los pantalones azules
* I want the yellow shoes | Quiero los zapatos amarillos
* She wants the green dress | Quiere el vestido verde
* Do you want to eat something? | ¿Quieres comer algo?
* Do you want to drink something? | ¿Quieres beber algo? 
* What do you want to buy? | ¿Qué quieres comprar? 
* What do you want to drink? | ¿Qué quieres beber? 
* I want to buy some clothes | Quiero comprar algo de ropa
* I want to drink some red wine | Quiero beber algo de vino tinto 
* I have to pee | Tengo que mear
* I have to buy an umbrella | Tengo que comprar un paraguas
* The bank is open | El banco está abierto
* The bank is closed | El banco está cerrado
* The restaurant is open | El restaurante está abierto
* The pharmacy is open | La farmacia está abierta
* Pull (a command) | Tire
* Push (a command) | Empuje
* How much | Cuánto
* The pound (£) | La libra
* The dollar ($) | El dólar
* The euro (€) | El euro
* It costs | Cuesta
* Free (of charge) | Gratis
* How much is this? | ¿Cuánto es este?
* How much is the pink hat? | ¿Cuánto es el sombrero rosa?
* That costs four pounds | Eso cuesta cuatro libras
* It costs six dollars | Cuesta seis dólares
* It is free | Es gratis
* How much is the blue shirt | ¿Cuánto es la camisa azul?
* How much is the small dog? | ¿Cuánto es el perro pequeño?
* It costs ten euros | Cuesta diez euros
* It costs eight pounds | Cuesta ocho libras
* The person | La persona
* The price, fee, charge | El precio
* How much is it? | ¿Cuánto cuesta?
* I don't want it | No lo puedo
* Can I help you (*formal*) | ¿Puedo ayudarle?
* Yes, I'll take it | Sí, me lo llevo
* This is very expensive, I don't want it | Esto es muy caro, no lo quiero
* Can you show me a smaller one? | ¿Puede mostrarme uno más pequeño?
* I need to buy a map. How much is it? | Necesito comprar un mapa ¿Cuánto cuesta?
* It's too expensive! | ¡Es muy caro!
* Street market | El tianguis
* I'm just looking | Sólo estoy viendo
* The corner shop | La tiendita
* Your change | Su cambio
* Be serious! | ¡Habla serio!

## Emergency 

* The emergency | La emergencia
* I feel | Me siento
* He feels, she feels | Se siente
* The pain | El dolor
* To hurt (*d…*) | Doler
* It hurts (*me*) | Me duele
* It hurts (*him, her*) | Le duele
* The infection | La infección
* The cold (*illness*) | El resfriado
* The fever | La fiebre
* I have a cold | Tengo un resfriado
* She had a fever | Tiene fiebre
* The medicine | La medicina
* To vomit | Vomitar
* The psychologist | El psicólogo
* The toilet paper | El papel de baño
* This is an emergency | Esto es una emergencia
* I don't feel well | No me siento bien
* What is wrong? | ¿Qué te pasa?
* She doesn't feel well | no se siente bien
* He needs medicine | Necesita medicina
* I have a stomachache | Me duele el estómago
* He always has a headache | Siempre le duele la cabeza
* You have an infection | Tienes una infección
* I think that I want to vomit | Creo que quiero vomitar
* I think I need medicine | Creo que necesito medicina
* You should see a doctor | Deberías ver a un doctor
* He should see a psychologist | Debería ver a un psicólogo
* I think that he needs more toilet paper | Creo que necesita más papel de baño
* You should see a psychologist | Deberías ver a un psicólogo
* She should see a doctor | Debería ver a un doctor
* The ambulance | La ambulancia
* Call (*sing. informal*) | Llama
* The police | La policía
* Broken | Roto
* Call the doctor | Llama al doctor
* Call an ambulance | Llama una ambulancia
* Call the police | Llama a la policía
* Where does it hurt? | ¿Dónde te duele?
* My arm hurts | Me duele el brazo
* My back hurts | Me duele la espalda
* Your leg is broken | Su pierna está rota
* Sometimes his neck hurts | A veces le duele el cuello
* My foot is broken | Mi pie está roto
* Be careful | Ten cuidado
* My back hurts a little | Me duele un poco la espalda
* Your foot is broken | Su pie está roto
* Her finger is broken | Su dedo está roto
* His hand is broken | Su mano está rota
* The heating | La calefacción
* It is fixed | Ya está arreglado
* The air conditioning | El aire acondicionado
* The medic | El médico
* The pressure | La presión
* The dose | La dosis
* The patient | El paciente
* The symptom | El síntoma
* The diagnosis | El diagnóstico
* The condition | La condición
* The diet | La dieta
* The emotion | La emoción
* The clinic | La clínica
* The virus | El virus
* The tear (*crying*) | La lágrima
* The circulation | La circulación
* The remedy | El remedio
* The protein | La proteína
* The taste | El sabor
* Serious | Grave
* The smell | El olor

## Interests

* The kind | El tipo
* What kind of sports do you like? | ¿Qué tipo de deportes te gustan?
* To enjoy | Disfrutar
* Playing | Jugar, jugando
* Basketball | El baloncesto
* Sailing | Navegar, navegando
* I enjoy playing basketball | Disfruto jugando al baloncesto
* She enjoys sailing | Disfruta navegando
* Badminton | El bádminton
* She enjoys playing badminton but she likes sailing more | Disfruta jugando al bádminton pero le gusta más navegar
* Fishing | Pescar, pescando
* I enjoy sailing bit I like fishing more | Disfruto navegando pero me gusta más pescar
* Singing | Cantar, cantando
* Gardening | La jardinería
* To prefer | Preferir
* What kind of films does your friend prefer? | ¿Qué tipo de películas prefiere tu amigo?
* The crime novel | La novela policiaca
* The crime film | La película policiaca
* She prefers crime films | Profiere las películas policiacas
* The comedy | La comedia
* Her boyfriend prefers comedy | Su novio prefiere las comedias
* What kind of books do you like? | ¿Qué tipo de libros te gustan?
* The documentary | El documental
* The fiction | La ficción
* The drama | El drama
* The poetry | La poesía
* I like documentaries but I prefer fiction | Me gustan los documentales pero prefiero la ficción
* What kind of food does you dad prefer? | ¿Qué tipo de comida prefiere tu padre?
* Indian | Indio
* Asian | Asiático
* European | Europeo
* He likes Indian food but he prefers French food | Le gusta la comida india pero prefiere la comida francesa
* I like Asian food but I prefer European food | Me gusta la comida asiática pero prefiero la comida europea
* The computer | El ordenador
* My brother enjoys playing basketball but he likes singing more | Mi hermano disfruta jugando al baloncesto pero le gusta más cantar
* My brother likes computers but he prefers gardening | A mi hermano la gustan los ordenadores pero prefiere la jardinería
* My mum enjoys singing but she like gardening more | Mi madre disfruta cantando pero le gusta más la jardinería
* I like poetry but I prefer drama | Me gusta la poesía pero prefiero el drama
* To be good (*at something*) | Se …dar bien…
* I am good (*at something*) | Se me da bien…
* He is good (*at something*) | Se le da bien…
* They are good at many things | Se les dan bien muchas cosas
* The guitar | La guitarra
* The piano | El piano
* To play the guitar | Tocar la guitarra
* To play the piano | Tocar la piano
* They are good at playing the piano | Se les da bien tocar el piano
* To be bad (*at something*) | Se (*a alguien*) dar mal (*algo*)
* She is bad at playing the guitar | Se la da mal tocar la guitarra
* She is good at many things | Se le dan bien muchas cosas
* Especially (*good*) | Especialmente
* She is especially good at singing | Se le da especialmente bien cantar
* To cook | Cocinar
* To tell | Contar
* The joke | El chiste
* He is bad at cooking | Se le da mal cocinar
* I am especially bad at telling jokes | Se me da bastante mal contar chistes
* He is especially bad at telling stories | Se le da bastante mal contar historias
* Easy | Fácil
* (*The*) Ice hockey | El hockey sobre hielo
* He doesn't like ice hockey because it is too easy | No le gusta el hockey sobre hielo porque es demasiado fácil
* Difficult | Difícil
* I don't like basketball because it is difficult | No me gusta el baloncesto porque es difícil
* He is especially good at playing music | Se le da especialmente bien tocar música
* To listen | Escuchar
* I am not good at listening to people | No se me da bien escuchar a la gente
* My brother is really good at writing songs | A mi hermano se le da realmente bien escribir canciones
* The geography | La geografía 
* For example | Por ejemplo
* For example he is really good at geography | Por ejemplo se le da realmente bien la geografía
* Foreign | Extranjero
* The language | El idioma
* I am especially bad at speaking foreign languages | Se me da bastante mal hablar idiomas extranjeros
* The bike | La bicicleta
* To ride a bike | Montar en bicicleta
* They are especially good at speaking to people | Se les da especialmente bien hablar con la gente
* Is he good at playing the piano? | ¿Se le da bien tocar el piano?
* My friend is really good at writing stories | A mi amiga se le da realmente bien escribir historias
* My son is good at riding a bike | A mi hijo se le da bien montar en bicicleta
* For example she is really good at science | Por ejemplo se le dan realmente bien las ciencias | For example she is really good at science
* Dangerous | Peligroso
* I don't like skiing because it is dangerous | No me gusta esquiar porque es peligroso
* (*The*) maths | Las matemáticas
* For example they are pretty good at maths | Por ejemplo se les dan bastante bien las matemáticas
* I don't like maths because it is difficult | No me gustan las matemáticas porque son difíciles

## Let’s Go to the Restaurant

* We go, let's go | Vamos
* For me | Para mí
* The table | La mesa
* The menu | El menú
* He can, she can, you can (*sing. formal*) | Puede
* To give | Dar
* Ourselves, to us | Nos
* The fork | El tenedor 
* The spoon | La cuchara
* The knife | El cuchillo
* Let’s go to the restaurant | Vamos a un restaurante
* You are (*temp, plural, formal*) | están
* Ready | listos
* To order | Pedir
* To eat | Comer
* To try | Probar
* Is it for me? | ¿Es para mí?
* A coffee please | Un café por favor
* A table for two please | Una mesa para dos por favor
* Can you give us…? | ¿Nos puede dar…?
* Can you give us a fork please? | ¿Nos puede dar un tenedor por favor?
* Are you ready? | ¿Están listos?
* Are you ready to order? | ¿Están listos para pedir?
* You can try the wine | Puede probar el vino
* You can order the beer | Puede pedir la cerveza
* Yes please | Sí por favor
* No thanks | No gracias
* The bill please | La cuenta por favor
* Are you ready to eat? | ¿Están listos para comer?
* Are you ready to try the wine? | ¿Están listos para probar el vino?
* Something | Algo
* To drink | Beber
* Themselves, to them, yourselves (*plu. formal*), to you (*plu. formal*) | Les
* The cup | La taza
* The bottle | La botella
* The glass | El vaso
* Yourself (*singular formal*), to you (*sing. formal*), himself, to him, to her | Le
* The dessert | El postre
* The main course | El plato principal
* The starter | El aperitivo
* He has, she has, you have (*sing. formal*) | Tiene
* Nothing | Nada
* What would you like? | ¿Qué les gustaría?
* I would like a coffee | Me gustaría un café
* I would like something to drink | Me gustaría algo de beber
* What would you like to eat? | ¿Qué les gustaría comer?
* What would you like to drink? | ¿Qué les gustaría beber?
* I would like a cup of tea | Me gustaría una taza de té
* Would you like a bottle? | ¿Le guastaría una botella?
* Would you like a glass? | ¿Le gustaría un vaso?
* I would like a latte please | Me gustaría un café con leche por favor
* I would like two beers please | Me gustarían dos cervezas por favor
* I would like some milk please | Me gustaría algo de leche por favor
* Do you have any desserts? | ¿Tiene algo de postre?
* Do you have any beers? | ¿Tiene algo de cerveza?
* I don’t have anything | No tengo nada
* I don’t have any water | No tengo nada de agua
* What would you like to order? | ¿Qué les gustaría pedir?
* I would like a bottle of water | Me gustaría una botella de vino
* I would like a spoon please | Me gustaría una cuchara por favor
* Of course I have some water | Claro que tengo algo de agua
* Breakfast | Desayuno 
* To have breakfast | Desayunar
* To have dinner | Cenar
* The tips | Las propinas
* More | Más
* To accept | Aceptar
* A card | Una tarjeta
* The cash | El dinero en efectivo
* I would like some breakfast please | Me gustaría algo de desayuno por favor
* We would like some red wine please | Nos gustaría algo de vino tinto por favor
* Would you like more beer | ¿Les gustaría más cerveza?
* Would you like more bread? | ¿Les gustaría más pan?
* Just a little | Sólo un poco
* Can you give me the bill please? | ¿Me puede dar la cuenta por favor?
* Can you give me a bottle of wine please? | Me puede dar una botella de vino por favor?
* I don’t have any cash | No tengo de dinero en efectivo
* She doesn’t have any tea | No tiene nada de té
* Do you accept cards? | ¿Acepta tarjetas?
* Do you accept cash? | ¿Acepta dinero en efectivo?
* We would like something to eat please | Nos gustaría algo de comer por favor
* Would you like more water? | ¿Les gustaría más agua?
* Can you give me a menu please? | ¿Me puede dar un menú por favor? 
* Do you accept tips? | ¿Acepta propinas?
* What would you like to order? (*alt*) | ¿Qué desea ordenar?
* Give me an order of spicy potato bites | Me pone una ración de patatas bravas
* I'll have the shrimp | Para mí las gambas
* It's very tasty | Qué rico
* It tastes bad | Sabe mal
* Give me (*one, two…*) | Dame un
* Without chilli | Sin chile
* To invite, to treat | Invitar
* Can I order? | ¿Puedo ordenar?
* The napkin | La serviletta
* Rich, tasty, yummy | Rico
* An order of | Una orden de
* Another please | Otro por favor
* The waiter (*alt*) | El mesero

## Travel

* To travel | Viajar
* The city | La ciudad
* The country | El país
* To visit | Visitar
* The visa | El visado
* The boarding ticket | La tarjeta de embarque
* The passport | El pasaporte
* The ticket | El billete
* The train | El tren
* The airport | El aeropuerto
* To show | Enseñar
* The bus | El autobús
* To take | Tomar
* The luggage | El equipaje
* The suitcase | La maleta
* Heavy | Pesado
* Light | Ligero
* Which city should we visit? | ¿Qué cuidad deberíamos visitar?
* Which country should we visit? | ¿Qué país deberíamos visitar?
* We have to buy a ticket | Tenemos que comprar un billete
* You have to have a ticket and a visa | Tienes que tener un billete y un visado
* We have to have a boarding pass and a visa | Tenemos que tener una tarjeta de embarque y un visado
* You have to show your passport in the airport | Tienes que enseñar tu pasaporte en el aeropuerto
* You have to show your ticket on the train | Tienes que enseñar tu billete en el tren
* They have to go to the airport | Tienen que ir al aeropuerto
* We have to take a taxi | Tenemos que tomar un taxi
* They always have to take the bus | Siempre tienen que tomar el autobús
* My luggage is too heavy | Mi equipaje es demasiado pesado
* Actually my luggage is really light | En realidad mi equipaje es realmente ligero
* Which friend should we visit? | ¿A qué amigo deberíamos visitar?
* He has to have a passport and a ticket | Tiene que tener un pasaporte y un billete
* She has to show her visa in the airport | Tiene que enseñar su visado en el aeropuerto
* I always have to take the train | Siempre tengo que tomar el tren
* Your luggage is very light | Tu equipaje es muy ligero
* His suitcase is too heavy | Su maleta es demasiado pesado
* To drive | Conducir
* Japan | Japón
* A flight | Un vuelo
* Has been | Se ha
* Delayed | Retrasado
* On time | Puntual
* To wait | Esperar
* The ship | El barco
* The plane | El avión
* The mountain  | La montaña
* The sea | El mar
* The lake | El lago
* They are going to the sea | Van al mar
* We have to drive to Japan | Tenemos que conducir a Japón
* The flight has been delayed | El vuelo se ha retrasado
* The train has been delayed | El tren se ha retrasado
* The flight is always on time | El vuelo siempre es puntual
* We have to wait for a plane | Tenemos que esperar un avión
* We are going to a mountain in Japan | Vamos a una montaña en Japón
* She has to buy a ticket | Tiene que comprar un bilete
* He has to wait for a ship | Tiene que esperar un barco
* He is going to a lake in China | Va a un lago en China
* The journey | El viaje
* The trip | La excursión
* Have | Ten
* The gap | El hueco
* Belongings | Pertenencias
* Guard (*sing. formal*) | Vigile
* Constantly | Constantemente
* It is a long journey | Es un viaje largo
* It is a long trip | Es una excursión larga
* It is a really long book | Es un libro realmente largo
* Have a good flight | Ten un buen vuelo
* Have a good trip | Ten una buena excursión
* Be careful with the gap | Ten cuidado con el hueco
* Guard your bags constantly | Vigile constantemente sus pertenencias
* The castle | El castillo
* The place (*l…*) | El lugar
* The area (*z…*) | La zona
* The community | La comunidad
* The (*city*) square | La plaza
* The region | La región
* The port, the harbour | El puerto
* The coast | La costa
* The room (*s…*), the lounge | La sala
* The institution | La institución
* The area (*a…*) | El área
* The nation | La nación
* The department, the apartment | El departamento
* The land | El terreno
* The palace | El palacio
* The island | La isla
* The town, the village, people | El pueblo
* The territory | El territorio
* The building | El edificio
* The neighbourhood | El barrio
* The jail | La cárcel
* The ranch | La hacienda
* The road | La carretera
* The homeland | La patria
* The tower | El torre
* The home (*h…*) | El hogar
* The backyard | El patio
* The track, the clue | La pista
* The avenue | La avenida
* The continent | El continente
* The corner | La esquina
* The route | La ruta
* The colony | La colonia
* The gallery | La galería
* The agency | La agencia
* The distance | La distancia
* The capital (*main city*) | La capital

## What do you want to do?

* The afternoon, the evening | La tarde
* Today | Hoy
* Tonight | Esta noche
* Tomorrow | Mañana
* A cake | Una tarta
* To go dancing | Ir a bailar
* To go out, to hang out, to leave | Salir
* To go for a swim | Ir a nadar
* To go out for dinner | Salir a cenar
* To go out for lunch | Salir a comer
* To sleep | Dormir
* Popcorn | Palomitas
* What do you want to do? | ¿Qué quieres hacer?
* To stay up | Quedarse despierto
* Until | Hasta
* Late | Tarde
* To go for a run | Ir a correr
* To cry | Llorar
* To go home | Ir a casa
* What do you want to do today? | ¿Qué quieres hacer hoy?
* I want to make a cake | Quiero hacer una tarta
* I like to go dancing | Me gusta ir a bailar
* Tomorrow we are going to go dancing | Mañana vamos ir a bailar
* I want to go for a swim | Quiero ir a nadar
* Do you want to go for a swim? | ¿Quieres ir a nadar?
* Do you want to go out for lunch? | ¿Quieres salir a comer?
* Tonight we are going to go out for dinner | Esta noche vamos a salir a cenar
* I want to watch films | Quiero ver películas
* He also wants to call his mother | También quiere llamar a su madre
* I want to stay up late and eat popcorn | Quiero quedarme despierto hasta tarde y comer palomitas
* I want to stay up late and watch films | Quiero quedarme despierto hasta tarde y ver películas
* They never want to go for a run | Nunca quieren ir a correr
* They always want to go home | Siempre quieren ir a casa
* What do you want to do tonight? | ¿Qué quieres hacer esta noche?
* What does she want to do this afternoon? | ¿Qué quiere hacer esta tarde?
* He also wants to sleep | También quiere dormir
* We also want to go for lunch | También queremos salir a comer
* She wants to stay up late and eat cake | Quiere quesdarse despierta hasta tarde y comer tarta
* I alway want to cry | Siempre quiero llorar
* The world | El mundo
* The play | La obra
* The garden | El Jardín
* The party | La fiesta
* The club | El club
* With him | Con él
* I would love | Me encantaría
* He/she would love | Le encantaría
* To get up | Levantar
* Early | Pronto
* With you | Contigo
* No one | Nadie
* Everyone | Todo el mundo
* I don't want to do anything | No quiero hacer nada
* He doesn't want to watch anything | No quiere ver nada
* He wants to see the world | Quiere ver el mundo
* He wants to see a play in the theatre | Quiere ver una obra en el teatro
* Do you want to come to the party with me? | ¿Quieres venir a la fiesta conmigo?
* Do you want to go the cinema with him? | ¿Quieres ir al cine con él?
* I would love to but I have to get up early tomorrow | Me encantaría pero tengo que levantar pronto mañana 
* I would love to go to the club with you | Me encantaría ir al club contigo
* No one wants to hang out with me | Nadie quiere salir conmigo
* Why not? | ¿Por qué no?
* Of course everyone wants to hang out with you | Claro que todo el mundo quiere salir contigo
* They don't want to buy anything | No quieren comprar nada
* We want to want in the garden | ¿Queremos comer en el jardín?
* Do you want to come to the club with me? | ¿Quieres venir al club conmigo?
* He would love to have dinner with you | Le encantaría cenar contigo
* Everyone wants to hang out with me | Todo el mundo quiere salir conmigo
* The morning | La mañana
* To take a shower | Ducharse
* To sleep in | Quedarse dormido
* To go for a walk | Ir a pasear
* Then | Después
* The life | La vida
* The education | La educación
* I hope | Espero
* What do you like to do in the morning? | ¿Qué te gusta hacer por la mañana?
* I like to go for a run and then take a shower | Me gusta ir a correr y después ducharme
* She likes to watch the news and then eat dinner | Le gusta ver las noticias y después cenar
* What do you want to do in life? | ¿Qué quieres hacer en la vida?
* What does your son want to do in life? | ¿Qué quiere hacer tu hijo en la vida?
* Do you want an education? | ¿Quieres una educación?
* He likes to have breakfast and then go to school | Le gusta desayunar y después ir a la escuela
* Does he want to go to university? | ¿Quiere ir a la universidad?
* I hope so | Eso espero
* What do you like to do in the evening? | ¿Qué te gusta hacer por la tarde?
* What does he like to do in the morning? | ¿Qué le gusta hacer por la mañana?
* He like to sleep in and then go for a run in the morning | Le gusta quedarse dormido y después ir a correr por la mañana
* We like to take a shower and then go for a walk | Nos gusta ducharnos y después ir a pasear
* What does she want to do in life? | ¿Qué quiere hacer en la vida?
* Does she want an education? | ¿Quiere una educación?

## What is happening?

* We are talking about my goals | Estamos hablando sobre mis objetivos
* Having dinner | Cenando
* We are having dinner in the garden | Estamos cenando en el jardín
* At the moment | En este momento
* At the moment my parents are having dinner together | En este momento mis padres están cenando juntos
* Visiting | Visitando
* At the moment they are visiting my grandparents | En este momento están visitando a mis abuelos
* In the place where… | En el sitio donde…
* Playing | Jugando
* I am playing badminton in the place where you live | Estay jugando al bádminton en el sitio donde tú vives
* At the moment they are speaking French | Están hablando francés en este momento 
* Waiting | Esperando
* My brother is waiting for you in the shop where you buy coffee | Mi hermano te está esperando en la tienda dónde compras café 
* Telling | Contando
* I am telling you a story about an elephant | Te estoy contando una historia sobre un elefante 
* Buying | Comprando
* My mum is buying wine in the shop where we also buy chocolate | Mi madre está comprando vino en la tienda dónde también compramos el chocolate
* Now | Ahora
* Right now | Ahora mismo
* Lloviendo | Raining
* Right now it is raining | Está lloviendo ahora mismo
* Snowing | Nevando
* In the place where I live it is snowing | En el sitio dónde vivo está nevando
* Watching, seeing | Viendo
* Right now we are watching basketball | Ahora mismo estamos viendo el baloncesto
* Eating | Comiendo
* I think that he is eating a hotdog | Creo que está comiendo un perrito caliente
* Right this second | Justo en este momento
* He is examining my chest right this second | Me está examinando el pecho justo en este momento
* The dentist is looking at her tooth right now | El dentista le mirando el diente ahora mismo
* He is playing ice hockey right now | Está jugando al hockey sobre hielo ahora mismo
* As we speak | Mientras hablamos 
* I am examining his eyes as we speak | Le estoy examinando los ojos mientras hablamos
* Sleeping | Durmiendo
* She is sleeping right now | Está durmiendo ahora mismo
* We are flying over London right this second | Estamos volando sobre Londres justo en este momento
* They are arriving in Madrid right this second | Están llegando a Madrid justo en este momento
* My dad thinks my mum is eating with a friend | Mi padre cree que mi madre está comiendo con una amiga
* They are voting for the Democrats right this second | Están votando a los demócratas justo en este momento
* It is clearing up as we speak | Se está despejando mientras hablamos
* My sister is picking her favourite kitten right now | Mi hermana está escogiendo su gatito favorito ahora mismo
* We are driving to the hospital right this second | Estamos conduciendo al hospital justo en este momento
* They are showing us the photos as we speak | Nos están enseñando las fotos mientras hablamos
* To shine | Brillar
* The sun is shining at the moment | El sol está brillando en este momento
* He is putting on his raincoat right this second | Se está poniendo su permeable justo en este momento

## Sports

* The talent | El talento
* He is very talented | Tiene mucho talento
* The player (*man, woman*) | El jugador, la jugadora
* That player is very talented | Ese jugador tiene mucho talento
* The stadium | El estadio
* The biggest (*one*) | El más grande
* (*the*) Football, (*the*) Soccer | El fútbol
* This is the biggest stadium in Europe | Este es el estadio más grande de Europa
* The football player | El futbolista, la futbolista
* He is the biggest football player in Great Britain | Es el futbolista más grande de Gran Bretaña
* The team | El equipo
* It is the best team | Es el mejor equipo
* The manager, the coach (*man, woman*) | El entrenador, la entrenadora
* He is the worst manager | Es el peor entrenador
* Entertaining | Entretenido
* Ice hockey is an entertaining sport | El hockey sobre hielo es un deporte entretenido
* Most, more | Más
* Ice hockey is the most entertaining sport | El hockey sobre hielo es el deporte más entretenido
* The goal (*sports*) | El gol
* That was never a goal! | ¡Eso nunca fue gol!
* That man is the most talented player | Ese hombre es el jugador con más talento
* This boy was the strongest player | Este niño fue el jugador más fuerte
* (*the*) Baseball | El béisbol
* The foul | La falta
* That was a foul! | ¡Eso era falta!
* (*the*) Basketball | El baloncesto
* I enjoy watching basketball | Disfruto viendo el baloncesto
* I love watching baseball | Me encanta ver el béisbol
* Popular | Popular
* Basketball is the most popular sport | El baloncesto es el deporte más popular
* Against | Contra
* Tomorrow they will play against the best team | Mañana jugarán contra el mejor equipo
* The century | El siglo
* The (*sports*) match | El partido
* Go (*a command*) | Vete
* Go home! | ¡Vete a tu casa!
* The man of the match | El hombre del partido
* This is the match of the century | Este es el partido del siglo
* He was man of the match | Fue el hombre del partido
* The world cup | El mundial
* He is the youngest player in the world cup | Es el jugador más joven del mundial
* He hates watching football | Odia ver el fútbol
* To send (*someone*) off | Expulsar (*a alguien*)
* Send him off! | ¡Expúlsalo!
* He is the player of the year | Es el jugador del año
* He is the oldest manager in the world cup | Es el entrenador más viejo del mundial
* Baseball is the most dangerous sport | El béisbol es el deporte más peligroso
* Yesterday they played against the worst team | Ayer jugaron contra el peor equipo
* That boy is the smallest player | Ese niño es el jugador más pequeño
* Available | Disponible
* The toilet | El baño
* There are still toilets available | Todavía hay baños disponibles
* The seat | El asiento
* There are no seats available | No hay asientos disponibles
* Unfortunately | Lamentablemente
* Unfortunately there were no seats available | Lamentablemente no había asientos disponibles
* The half (*sports*) | La mitad
* They scored four goals in the first half | Marcaron cuatro goles en la primera mitad
* Completely | Completamente
* Full | Lleno
* The stadium was completely full | El estadio estaba completamente lleno
* To win, to earn | Ganar
* He won | Ganó
* She won because she was better | Ganó porque fue mejor
* To lose | Perder
* I lost | Perdí
* The game, the match | El partido
* Good game | Buen partido
* To concentrate (*onself*) | Concentrar(*se*)
* We lost because we didn't concentrate enough | Perdimos porque no nos concentramos lo suficiente
* I heard | Oí
* The radio | La radio
* I heard the results over the radio | Oí los resultados por la radio
* The referee (*man, woman*) | El árbitro, la árbitra
* The idiot (*man, woman*) | El idiota, la idiota
* The referee is an idiot! | ¡El árbitro es un idiota!
* Magical | Mágico
* The technique | La técnica
* Unfortunately there were no toilets available | Lamentablemente no había baños libres
* Unfortunately the referee was sick | Lamentablemente el árbitro estaba enfermo
* To score | Marcar
* Magical technique! | ¡Técnica mágica!
* The point | El punto
* Any (*before sing. masc. nouns*) | Ningún
* We didn't score any points | No marcamos ningún punto
* Injured (*in sports*) | Lesionado
* He lost because he was injured | Perdió porque estaba lesionado
* They scored | Marcaron
* They didn't score any points in the second half | No marcaron ningún punto en la segunda mitad
* Unfortunately the referee was an idiot | Lamentablemente el árbitro era un idiota
* The coach (*man, woman*) | El entrenador, la entrenadora
* Unfortunately their coach is an idiot | Lamentablemente su entrenador es un idiota
* Empty | Vacío
* The stadium was completely empty | El estadio estaba completamente vacío
* Well played! | ¡Bien jugado!
* My team didn't score any goals | Mi equipo no marcó ningún gol
* I watched the match on TV | Vi el partido en la televisión
* The member (*man, woman*) | El miembro, la miembro
* The fan club | El club de fans
* I want to be a member of the fan club | Quiero ser un miembro del club de fans
* The sports club | El club deportivo
* I want to be a member of a sports club | Quiero ser un miembro de un club deportivo
* To draw (*in sports*) | Empatar
* The championship | El campeonato
* If they draw they will lose the championship | Si empatan perderán el campeonato
* The league | La liga
* The tournament | El torneo
* If they win this match they will win the league | Si ganan este partido ganarán la liga
* The season (*in sports*) | La temporada
* To begin | Empezar
* It begins | Empieza
* The season always begins in August | La temporada siempre empieza en agosto
* To finish | Terminar
* The hotdog | El perrito caliente
* (*the*) Half time | El intermedio
* Should we get a hotdog at half time? | ¿Deberíamos coger un perrito caliente en el intermedio?
* Should we watch the game together? | ¿Deberíamos ver el partido juntos?
* To support a team | Ser de un equipo
* Which team do you support? | ¿De qué equipo eres?
* He wants to be a member of the football club | Quiere ser miembro del club de fútbol
* If they lose this match they will lose the tournament | Si pierden este partido perderán el torneo
* The interview | La entrevista
* The season always finishes in February | La temporada siempre termina en febrero
* My daughter wants to be a member of the badminton club | Mi hija quiere ser miembro del club de bádminton
* The cheerleader | La animadora
* Should we get a beer at half time? | ¿Deberíamos coger una cerveza en el intermedio?
* Usually | Normalmente
* intermedio | Normalmente hay una entrevista con el entrenador en el
There is usually an interview with the coach at half time
* Which player do you support? | ¿A qué jugador apoyas?
* There are usually cheerleaders at half time | Normalmente hay animadoras en el intermedio
* To turn (*something*) off | Apagar (*algo*)
* To turn (*something*) on | Encender (*algo*)
* Turn the TV off | Apaga la televisión
* Turn the TV on | Enciende la televisión
* The sound | El sonido
* To turn (*something*) up | Subir (*algo*)
* To turn (*something*) down | Bajar (*algo*)
* Turn the sound up | Sube el sonido
* Turn the sound down | Baja el sonido
* Bad | Malo
* To not have a chance | No tener ninguna posibilidad
* They don't have a chance | No tienen ninguna posibilidad
* Great Britain's team is pretty bad | El equipo de Gran Bretaña es bastante malo
* Turn the radio off | Apaga la radio
* Unlikely | Improbable
* They win (*subjunctive*) | Ganen
* It might be that they win the league but I don't think so | Puede ser que ganen la liga pero no lo creo
* May | Poder (*probability*)
* I think my team may win tomorrow | Creo que mi equipo puede ganar mañana
* I think they may lose tonight | Creo que esta noche pueden perder
* It might be that they win but I don't think so | Puede ser que ganen pero no lo creo
* My dad thinks his team may win the championship | Mi padre cree que su equipo puede ganar el campeonato
* Turn the radio on | Enciende la radio
* It loses (*subjunctive*) | Pierda
* It might be that my brother's team loses but it is unlikely | Puede ser que el equipo de mi hermano pierda pero es improbable
* Good chance | Probabilidad alta
* My brother's team have a pretty good chance of winning | El equipo de mi hermano tiene una probabilidad bastante alta de ganar
* My mum thinks my dad's team may lose the tournament | Mi madre cree que el equipo de mi padre puede perder el torneo
* It wins | Gane
* It might be that my mum's team wins but it is unlikely | Puede ser que el equipo de mi madre gane pero es improbable
* To explain | Explicar
* (*the*) Tennis | El tenis
* The rule | La regla
* Can you explain the rules of tennis please? | ¿Puedes explicar las reglas del tenis por favor?
* Can you explain the rules of cricket please? | ¿Puedes explicar las reglas del cricket por favor?
* The difference | La diferencia
* (*the*) American football | El fútbol americano
* There is a difference between football and American football | Hay una diferencia entre el fútbol y el fútbol americano
* (*the*) Gymnastics | La gimnasia
* Big (*not descriptive*) | Gran
* There is a big difference between badminton and tennis | Hay una gran diferencia entre el bádminton y el tenis
* To throw | Lanzar
* The ball | El balón
* They are very good at throwing the ball | Se les da muy bien lanzar el balón
* To kick | Chutar
* He is very good at kicking the ball | Se le da muy bien chutar el balón
* Huge | Enorme
* The American football player (*man*) | El jugador de fútbol americano
* American football players are huge | Los jugadores de fútbol americano son enormes
* The gymnast (*man, woman*) | El gimnasta, la gimnasta
* Tiny | Bajito
* Gymnasts are tiny | Las gimnastas son bajitas
* To train | Entrenar
* Professional basketball players train a lot | Los baloncestistas profesionales entrenan mucho
* Cricket used to be really popular | El cricket era realmente popular
* The tennis player (*man, woman*) | El tenista, la tenista
* Professional tennis players train every day | Los tenistas profesionales entrenan todos los días
* They are very good at catching the ball | Se les da muy bien atrapar el balón
* Can you explain the rules of golf please? | ¿Puedes explicar las reglas del golf por favor?
* Football players used to be really poor | Los futbolistas eran realmente pobres
* The dancing | El baile
* There is a difference between gymnastics and dancing | Hay una diferencia entre la gimnasia y el baile
* The golf player (*man, woman*) | El golfista, la golfista
* Golf players are really rich | Los golfistas son realmente ricos
