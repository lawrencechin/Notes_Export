# Nouns

## Home

* The towel | La toalla
* The tablecloth | El mantel
* The napkin | La servilleta
* The dining room | El comedor
* The curtains/the drapes | Las cortinas
* The rug | La alfombra
* The cup (*c…*) | La copa
* The refrigerator (*alt*) | El refrigerador
* The bed sheet | La sábana
* The frying pan | La sartén
* The dryer | La secadora
* The crib | La cuna
* The desk | El escritorio
* The mirror | El espejo
* The sponge | La esponja
* The oven | El horno
* The soap | El jabón
* The lamp | La lámpara
* The washing machine |  La lavadora
* The wall | La pared
* The pool | La piscina
* The floor | El piso
* The razor | La rasuradora
* The ceiling, roof | El techo
* The bedroom | El dormitorio
* The perfume | El perfume
* The shelf | El estante
* The makeup | El maquillaje
* The cushion | El cojín
* The pool | La piscina
* The basement | El sótano
* The wireless | El inalámbrico
* The garage | El garaje
* The garden (*with vegetables*) | El huerta
* The (*Méxican*) blankets | Los sarapes
* Headphones | Auriculares

## Animals

* Seagulls | Gaviotas
* The animal | El animal
* The spider | La araña
* The horse | El caballo
* The crab | El cangrejo
* The bear | El oso
* The bird | El pájaro
* The duck | El pato
* The penguin | El pingüino
* The turtle | La tortuga
* The mouse | El ratón
* The pig | El cerdo
* The bull | El toro
* The monkey | El mono
* The rabbit | El conejo
* The eagle | El águila
* The snake | La culebra
* The dragonfly | El libélula
* The hummingbird | El colibrí
* The rhea | El ñandú
* The hedgehog | El erizo
* A dog | Un perro
* A cat | Un gato
* Bee | Abeja
* Hedgehog | Erizo
* Mole | Topo
* Otter | Nutria
* Opossum  | Zarigüeya
* Wild boars | Jabalís
* Swarm | Enjambre

## Nature

* The mountain  | La montaña
* The sea | El mar
* The lake | El lago
* The environment (*m…*) | El medioambiente
* The sun | El sol
* The beach | La playa
* The pasture, the lawn, the grass | El pasto
* The moon | La luna
* The volcano | El volcán
* The fire | El fuego
* The earth | La tierra
* The rain | La lluvia
* The sky | El cielo
* The wood | La madera
* The field | El campo
* Nature | La naturaleza
* The species | El especie
* Evolution | La evolución
* The light | La luz
* The planet | El planeta
* The stone | La piedra
* The universe | El universo
* The leaf, the sheet (*of paper*) | La hoja
* The gas | El gas
* The cell | La célula
* The climate | El clima
* The forest | El bosque
* The rainforest | La selva
* The landscape, the scenery | El paisaje
* The root | La raíz
* The brain | El cerebro
* The bone | El hueso
* The wind | El viento
* The smoke | El humo
* The plant | La planta
* The environment, the atmosphere (*masc.*) | El ambiente
* The current, the trend | La corriente
* The mountain (*masc.*) | El monte
* The sand | La arena
* The space, space | El espacio
* (*Bolts of*) lightning | Relámpagos
* Acorns | Bellotas
* Frost | Escarcha
* Oak | Roble
* Pine | Pino
* Willow | Sauce
* Ash | Fresno
* Elm | Olmo

## Food

* Puff pastry | Hojaldre
* Pretzels, type of doughnut | Rosquillas
* Snack, afternoon snack, tea | Merienda
* Peas | Guisantes
* Fried | Frito
* Fried fish | Pescado frito
* Broiled chicken | Pollo a la parrilla
* Roasted duck | Pato al horno
* The celery | El apio
* The bacon | El tocino
* The marmalade | La mermelada
* The radish | El rábano
* The beans | Los frijoles
* The orange juice | El jugo de naranja
* The toast | El pan tostado
* The fried egg | El huevo frito
* The scrambled eggs | Los huevos revueltos
* The boiled eggs | Los huevos pasados por agua
* The cauliflower | La coliflor
* The lunch | El almuerzo
* The onion | La cebolla
* The sandwich | El emparedado
* The strawberry | La fresa
* The juice | El jugo
* The potato | La papa
* The tomato | El tomate
* The vegetable (*alt*) | El vegetal
* The pumpkin | La calabaza
* The mushroom | El champiñón
* The pea | El guisante
* The chilli pepper | El ají
* The bread | El pan
* The beer | La cerveza
* The snacks/finger foods | Las tapas
* The pasta | La pasta
* The fruit | La fruta
* The apple | La manzana
* The meat | La carne
* The pig/pork | El cerdo
* The salad | La ensalada
* The orange | La naranja
* Tea | El té
* The fish | El pescado
* The chicken | El pollo
* The vegetable | La verdura
* The rice | El arroz
* The potato | La patata
* The banana | El plátano
* The food, the lunch | La comida
* The lemon | El limón
* The soup | La sopa
* The pepper | La pimienta
* The nut | La nuez
* The cheese | El queso
* The sugar | El azúcar
* The salt | La sal
* Steak | El bistec
* Shrimp | Las gambas
* Spicy potatoes | Patatas bravas
* The wine | El vino
* The milk | La leche
* The sauce | La salsa
* The water | El agua
* The egg | El huevo
* Typical Mexicano dish | Las enchiladas
* Typical Mexicano snack | La quesadilla
* Spanish omelet | La tortilla
* The Chocolate | El Chocolate
* The garlic | El ajo
* The butter | La mantequilla
* The oil | El aceite
* The carrot | La zanahoria
* The lettuce | La lechuga
* The grape | La uva
* The pineapple | La piña
* The cake (*alt*) | El pastel
* The banana (*alt*) | El banano
* The mushroom (*alt*) | El hongo
* The tuna | El atún
* The corn | El maíz
* The turkey | El pavo
* The ice | El hielo
* Asparagus | Los espárragos
* Fruit cocktail | El coctel de frutas
* Peanut | Cacahuete
* Sauerkraut | Chucrut

## Clothing

* Slippers | Pantuflas
* Handkerchiefs | Pañuelos
* The scarf | La bufanda
* The stockings | La media
* The sweater | El suéter
* The belt | El cinturón
* Purple (*alt*) | Púrpura
* Brown (*alt*) | Café
* The glasses (*eyewear*) | Las gafas
* The jacket | La chaqueta
* The colour | El color
* The shirt | La camisa
* The shoes | Los zapatos
* The trousers | Los pantalones
* The dress | El vestido
* The clothes | La ropa
* The umbrella | El paraguas
* The swim suit | El bañador
* The boot | La bota
* The glove | El guante
* The wellies | Las botas de agua
* The size | El tamaño
* The slip | El fondo
* Zip/fly | Cremallera
* Sleeve | Manga
* Silk | Seda
* Cotton | Algodón

## Jobs

* The devil | El diablo
* Baker | El panadero, la panadera
* Doctor/Medic | El médico, la médica
* Surgeon | El cirujano, la cirujana
* Architect | El arquitecto, la arquitecta
* Pharmacologist | El farmacéutico, la farmacéutica
* Fireman | El bombero, la bombera
* Librarian | El bibliotecario, la bibliotecaria
* Archaeologist | El arqueólogo, la arqueóloga
* Stylist | El peluquero, la peluquera
* Butcher | El carnicero, la carnicera
* Clown | El payaso, la payasa
* Chemist | El químico, la química
* Jeweller | El joyero, la joyera
* Translator | El traductor, la traductora
* Dancer | El bailarín, la bailarina
* Accountant | El contable, la contable
* Athlete | El atleta, la atleta
* Psychiatrist | El psiquiatra, la psiquiatra
* Beautician | El esteticista, la esteticista
* Poet | El poeta, la poeta
* Therapist | El terapeuta, la terapeuta
* Interpreter | El intérprete, la intérprete
* Sales clerk | El dependiente, la dependienta
* Duke/duchess | El duque, la duquesa
* Mayor | El alcalde, la alcaldesa
* The mailman/postman | El cartero, la cartera
* The teacher | El maestro, la maestra
* The lawyer | El abogado, la abogada
* The artista | El artista, la artista
* The agent | El agente
* The athlete | El atleta
* The author | El autor
* The cashier | El cajero
* The captain | El capitán
* The carpenter | El carpintero
* The cook | El cocinero
* The commander | El comandante
* The driver | El conductor
* The colonel | El coronel
* The doctor | El doctor
* The owner | El dueño
* The businessman | El empresario
* The nurse | El enfermero
* The speciality | La especialidad
* The specialist | El especialista
* The student | El estudiante
* The farmer | El granjero
* The guard | El guardia
* The engineer | El ingeniero
* The waiter (*alt*) | El mesero
* The baker | El panadero
* The painter | El pintor
* The poet | El poeta
* The prince | El príncipe
* The profession | La profesión
* The priest | El sacerdote
* The secretary | El secretario
* The soldier | El soldado
* The maid | La criada
* The driver (*c…*) | El chofer
* Tailor | Sastre
* Fraudster | Estafadora
* Liar | Embustero

## Countries

* Chinese | Chino
* French | Francés
* European | Europeo
* International | Internacional
* North American | Norteamericano
* Italian | Italiano
* Cuban | Cubano
* British | Británico
* Mexican | Mexicano
* American (*US*) | Estadounidense
* American, from the Americas | Americano
* Italy | Italia
* China | China
* Spain | España
* Mexico | México
* France | Francia
* Foreign, abroad | Extranjero
* England | Inglaterra
* Paris | París
* Rome | Roma
* Argentina | Argentina
* Europe | Europa
* America, the Americas | América
* Argentinian | Argentino 
* Germany | Alemania
* German | Alemán
* New York | Nueva York
* Canada | Canada
* Norway | Noruega
* Japanese | Japonés
* Africa | África
* Great Britain | Gran Bretaña

## Business

* Guest (in house or hotel) | Huésped
* Shop window | Escaparate
* Theft, robbery, shop lifting | Hurto
* Farm equipment | Labranza
* The interest | El interés
* The offer | La oferta
* The number (*c…*) | La cifra
* The check, the account | La cuenta
* The contract | El contrato
* The industry | La industria
* The executive | El ejecutivo
* The debt | La deuda
* The value | El valor
* The rate, the tax (*t…*) | La tasa
* The trade, commerce | El comercio
* The fortune | La fortuna
* The product | El producto
* The percentage | El porcentaje
* The promotion | La promoción
* The loan, the credit | El crédito
* The entry, the admission | El ingreso
* The income | Los ingresos
* The property | La propiedad
* The gold | El oro
* The meeting | La reunión
* The company, the venture (*e…*) | La empresa
* the company (*c…*) | La compañía
* The meeting, the board | La junta
* The lecture, the conference | La conferencia
* The inflation | La inflación
* Economic, cheap | Económico
* The sale | La venta
* The cost | El costo
* The negotiation | La negociación
* The office | El despacho
* The post office | EL correo
* The ink | la tinta
* Air mail | Correo aéreo
* The mailbox | El buzón
* Show business | Farándula
* Barter | El trueque

## Communication

* The press | La prensa
* The article | El artículo
* The voice | La voz
* The communication | La comunicación
* The conversation (*c…*) | La conversación
* The language (*masc.*) | El lenguaje
* The news | La noticia
* The call | La llamada
* The secret | El secreto
* The contact | El contacto
* The shout, the scream | El grito
* The message | El mensaje
* The argument, the plot | El argumento
* The comment, the essay | El comentario
* The stamp | La estampilla
* The postcard | La postal
* The sentence, the phrase | La frase
* The editorial | El editorial
* The newsreel | El noticiario

## Arts

* The style | El estilo
* The poetry, the poem | La poesía
* The rhythm | El ritmo
* The sound | El sonido
* The fashion | La moda
* The violin | El violín
* The flute | La flauta
* The instrument | El instrumento
* The concert | El concierto
* The orchestra | La orquesta
* The opera | La ópera
* The dance | La danza
* The architecture | La arquitectura
* The song | La canción
* The disc | El disco
* The stage | El escenario
* The show | El espectáculo
* The painting, the paint | La pintura
* The performance, the acting | La actuación
* The photograph | La fotografía
* The painting, the square | El cuadro
* The letter (*of the alphabet*) | La letra
* The alphabet | El abecedario
* The act | El acto
* The passion | La pasión
* The score (*music*) | La partitura
* Fable | Fábula

## Politics

* The society | La sociedad
* The politician | El político
* The law, the right (*d…*) | El derecho
* The organisation (*fem.*) | La organización
* The advice | El consejo
* The safety, the security | La seguridad
* The justice | La justicia
* The crisis | La crisis
* The ministry | El ministerio
* The plan | El plan
* The congress | El congreso
* The administration | La administración
* The defense | La defensa
* The army | El ejército
* The campaign | El campaña
* Liberty, freedom | La libertad
* The democracy | La democracia
* The republic | La república
* Politics | La política
* The opposition | La oposición
* The socialist | El socialista
* The speech | El discurso
* The statement | La declaración
* The organism, the organization (*masc.*) | El organismo
* The mayor | El alcalde
* The constitution | La constitución
* The leader | El líder
* The violence | La violencia
* The presidency | La presidencia
* The budget, the estimate | El presupuesto
* The candidate | El candidato
* The strategy | La estrategia
* The conflict | El conflicto
* The conversation, the talks (*d…*) | El diálogo
* The independence | La independencia
* The federation | La federación
* The corruption | La corrupción
* The cooperation | La cooperación
* The governor | El gobernador
* The ambassador | El embajador
* Agriculture | La agricultura
* The deficit | El déficit
* The tax (*i…*) | El impuesto
* The demonstration, the protest | La manifestación
* The crime | El crimen
* The dictatorship | La dictadura
* The empire | El imperio
* The poverty | La pobreza
* The stability | La estabilidad
* The civilian | El civil
* The wealth | La riqueza
* The power | El poder
* The legislation | La legislación
* The senator | El senador
* The difference | La diferencia
* The arrest | El arresto

## Science

* The energy | La energía
* The measurement | La medida
* The analysis | La análisis
* The line | La línea
* The dot, the period | El punto
* The matter, the substance | La sustancia
* The function | La función
* Science | La ciencia
* The surface | La superficie
* The technology | La tecnología
* The concept | El concepto
* The theory | La teoría
* Physics | La física
* The technique | La técnica
* The technician | El técnico
* The element | El elemento
* The speed | La velocidad
* The temperature | La temperatura
* The mass, the dough | La masa
* The method | El método
* The volume | El volumen
* Philosophy | La filosofía
* The thesis | La tesis
* The balance | El equilibrio
* The border, the limit | El límite
* The formula | La fórmula
* The scientist | El científico
* The average | El promedio
* The substance | La sustancia
* The atmosphere (*fem.*) | La atmósfera
* Chemistry | La química
* The chemist | El químico
* The nucleus | El núcleo
* The laboratory | El laboratorio
* The proportion | La proporción
* The discovery | El descubrimiento
* The observation | La observación
* The depth | La profundidad
* The metric system | El sistema métrico decimal

## Other Nouns

* The soul | El alma
* The axe | El hacha
* The cart | El carro
* The cloud | La nube
* The harp | El arpa
* The classroom | El aula
* The screen/monitor | La pantalla
* The page | La página
* A corridor | Una galería
* The escalator, the ladder, the stairs  | La escalera
* The pencil | El lápiz
* The secret | El secreto
* The smile | La sonrisa
* The reporter | El enviado
* The cheek | La mejilla
* The pencil | El lápiz
* The leaf | La hoja
* The blouse | La blusas
* The jewellery | La joya
* The painting | El cuadro
* The word | La palabra
* Happiness | La felicidad
* The virtue | La virtud
* The light | La luz
* Imperfect | El copretérito
* The apparatus | El aparato
* The car (*a…*) | El auto
* The flag | La bandera
* The battery | La batería
* The bomb | La bomba
* The chain | La cadena
* The box, the cash register | La caja
* The camera | La cámara
* The bell | La campana
* The mobile phone (*c…*) | El celular
* The column | La columna
* The thing | La cosa
* The document | El documento
* The source, the fountain | La fuente
* The list | La lista
* The machine | La máquina
* The motorcycle | La moto
* The motor | El motor
* The note | La nota
* The object | El objeto
* The screen | La pantalla
* The paper | El papel
* The newspaper | El periódico
* The piece | La pieza
* The feather, the pen | El polvo
* The bridge | El puente
* The rule, the ruler | La regla
* The clock | El reloj
* The magazine | La revista
* The wheel | La rueda
* The card | La tarjeta
* The scissors | Las tijeras
* The vehicle | El vehículo
* The level | El nivel
* The school (*c…*) | El colegio
* The study, the studio | El estudio
* The response | La repuesta
* The title, the degree (*t…*) | El título
* The degree (*g…*) | El grado
* The material | El material
* The course (*c…*) | El curso
* The chapter | El capítulo
* The class | La clase
* The task, the chore, the homework | La tarea
* The absence | La ausencia
* The reading | La lectura
* Intelligence | La inteligencia
* The section | La sección
* Knowledge | El conocimiento
* The academy | La academia
* The faculty | La facultad
* The explanation | La explicación
* The solution | La solución
* The prize | El premio
* The age | La edad
* The group | El grupo
* The process | El proceso
* The information | La información
* The truth | La verdad
* The action | La acción
* The service | El servicio
* The production | La producción
* The sector | El sector
* The image | La imagen
* The movement | El movimiento
* The activity | La actividad
* The code (*masc.*) | El código
* The capacity | La capacidad
* The effect | El efecto
* The research, the investigation | La investigación
* The need, the necessity | La necesidad
* The program | El programa
* The quality | La calidad
* The character, the characteristic (*c…*) | El carácter
* The control | El control
* The result | El resultado
* The importance | La importancia
* The origin | El origen
* Official | Oficial
* The objective | El objetivo
* The growth | El crecimiento
* The success | El éxito
* The construction | La construcción
* The memory | La memoria
* The expression | La expresión
* The practice | La práctica
* The conscience | La conciencia
* The creation | La creación
* Luck | La suerte
* The aspect | El aspecto
* The occasion | La ocasión
* The look | La mirada
* The issue, the matter | El asunto
* The beginning, the principle | El principio
* The trial | El juicio
* Vision | La visión
* The selection | La selección
* The internet, the network (*r…*) | La red
* The access | El acceso
* The content | El contenido
* The end, the term (*t…*) | El término
* The frequency | La frecuencia
* The danger | El peligro
* The height | La altura
* The scene | La escena
* The thought | El pensamiento
* The application | La aplicación
* The character (*p…*) | El personaje
* The competition | La competencia
* The protection | La protección
* The circumstance | La circunstancia
* The purpose | El propósito
* The gesture | EL gesto
* The relationship | La relación
* The influence | La influencia
* The intention | La intención
* The beauty | La belleza
* The ceremony | La ceremonia
* The loss | La pérdida
* The situation | La situación
* The version | La versión
* The union | La unión
* The consequence | La consecuencia
* The unity, the unit | La unidad
* The behaviour (*fem.*) | La conducta
* The commitment | El compromiso
* The identity | La identidad
* The presence | La presencia
* The edition | La edición
* The presentation | La presentación
* The behaviour (*.masc*) | El comportamiento
* The distribution | La distribución
* The shadow | La sombra
* The channel, the canal | El canal
* The division | La división
* The continuation | La continuación
* The decision | La decisión
* The error | El error
* The position, the place, the job (*p.*) | El puesto
* The concern | La preocupación
* The perspective | La perspectiva
* The factor | El factor
* The mission | La misión
* The fall | La caída
* The effort | El esfuerzo
* The margin | El margen
* The session | La sesión
* The motive, the reason (*m.*) | El motivo
* Concentration | La concentración
* The honor | El honor
* The resource | El recurso
* The category | La categoría
* The proposal | La propuesto
* The meaning | El significado
* The impact | El impacto
* The intensity | La intensidad
* The noise | El ruido
* The exception | La excepción
* The review, the criticism | La crítica
* The dream | El sueño
* The order | El orden
* The pause | La pausa
* The area, the extension (*e…*) | La extensión
* The charge | El cargo
* The failure | El fracaso
* The angel | El ángel
* The faith | La fe
* The spirit | El espíritu
* The cross, the crucifix | La cruz
* The soul | El alma
* The destiny, the destination | El destino
* The tradition | La tradición
* The imagination | La imaginación
* The summit | La cumbre
* The contest | El concurso
* The difficulty | La dificultad
* The reflection | La reflexión
* The sign (*masc.*) | El signo
* The opinion | La opinión
* The evidence (*e…*) | La evidencia
* The childhood | La infancia
* The collection | La colección
* The obligation | La obligación
* The dimension | La dimensión
* The request | La petición
* The transformation | La transformación
* The installation | La instalación
* The publication | La publicación
* The circle | El círculo
* The procedure | El procedimiento
* The phenomenon | El fenómeno
* The advantage | La ventaja
* The attitude | La actitud
* The introduction | La introducción
* The tail, the line, the glue | La cola
* The adventure | La aventura
* The amount | La cantidad
* The variety | La variedad
* The transportation | El transporte
* The opportunity | La oportunidad
* The darkness | La oscuridad
* The decrease | La disminución
* The conclusion | La conclusión
* The permission | El permiso
* The panorama | El panorama
* The destruction | La destrucción
* The symbol | El símbolo
* The rival | El rival
* The equality | La igualdad
* The revision, the inspection | La revisión
* The scandal | El escándalo
* The profile | El perfil
* The well-being | El bienestar
* The fame | La fama
* The identification | La identificación
* The way, the manner (*m…*) | La manera
* The damage | El daño
* The description | La descripción
* The confusion | La confusión
* The reaction | La reacción
* The reason (*r…*) | La razón
* The alternative | La alternativa
* The detail | El detalle
* The responsibility | La responsabilidad
* The code, the key (*fem., c…*) | La clave
* The attack | El ataque
* The strength | La fuerza
* The idea | La idea
* The form, the way (*f…*) | La forma
* The rest, the remainder | El resto
* The cause | La causa
* The project | El proyecto
* The figure | La figura
* The place (*s…*) | El sitio
* The progress | El progreso
* The part | La parte
* The discipline | La disciplina
* The benefit | El beneficio
* Hell | El infierno
* The ticket | El boleto
* The terrace | La terraza
* The ticket snub | El talón
* The box office |  La taquilla
* The courtesy | La cortesía
* Moonlit nights | Las noches de luna
* Bottom/end | Fondo
* Atrocity | Barbaridad
* Lane | Carril
* Dirt, filth | Mugre
* Index finger | Índice
* Middle finger | Corazón
* Summer | Estivales
* Harvest | Cosechas
* Famine | Hambruna
* Darkness | Tinieblas
* Stagnation | Estancamiento
* Parade | Desfile
* Luxury | Lujo
* Fingernails | Uñas
* Loft, attic | Desván
* Pilgrim | Peregrino
* Thumb | Pulgar
* Clutch | Embrague
* Alms | Limosna
* Gift | Obsequio
* Fellow man | Prójimo
* Godmother | Madrina
* Happiness | Dicha
* Tug | Tirón
* Stress, emphasis | Hincapié
* Trifle | Nimiedad
* Slow, dulled-witted | Lerdo
* Bruise | Moretón
* Misdeed | Fechoría
* Nuance | Matriz
* Guffaw | Carcajada
* Lazybones | Gandul
* Fusspot | Tiquismiquis
* Close-range, point-blank | Quemarropa
* Password | Contraseña
* Hangover | Resaca
