# Words to recall (from Clozemaster)

Move the words you are comfortable with to the other notes so we can work on the words we have issues with. This list is always changing and not permanent   

## Verbs

* To lower, to bend down | Agachar
* To beat | Apalear
* To sadden | Apenar
* To kick, to boot, to stamp | Patear
* To hoot | Ulular
* To catch, to seize | Prenderse
* To award, to bestow | Otorgarse
* To rub | Frotar
* To neglect, to disregard, to leave unattended | Desatender
* To climb | Trepar
* To declare terminally ill | Desahuciar
* To endorse | Visar
* To make…whither (*flores*), to fade away | Marchitar
* To hesitate, to stutter | Titubear
* To whirl around | Arremolinar
* To grumble, to grouch | Refunfuñar
* To knock down, to run over | Atropellarse
* To pass, to go by, to flow | Discurrir 
* To totter, to sway | Tambalearse
* To stalk, to lie in wait for | Acechar
* To wander, to roam | Deambular
* To tie | Atar
* To long for, to yearn | Anhelar
* To lie, to settle | Radicar
* To interfere, to meddle | Inmiscuirse
* To puncture | Ponchar
* To infuriate, to get mad | Cabrear
* To make… shudder | Estremecer
* To straighten, to put right | Enderezar

## Vocab

* Omen | Agüero
* Terrifying | Espeluznante
* Lazy/idle, lazybones | Holgazán
* Long distance | Interurbana
* Earthly | Mundano
* Agitation, excitement, racket | Alboroto
* Telling off, scolding | Rapapolvo
* Eccentric, bizarre | Estrafalario 
* Basket | Canasta
* Amazing, great, brilliant (*slang*) | Cojonudo
* Upright | Erguido
* Squirrel | Ardilla
* Vacuous | Hueras
* Crowd, throng, mass of people | Muchedumbre

