# Society

## Heath

* To sit (*oneself*) down | Sentar(se)
* To lie (*oneself*) down | Tumbar(se)
* To stand (*oneself*) up | Levantar(se)
* Please sit down | Por favor siéntese
* Please lie down | Por favor túmbese
* To examine | Examinar
* The chest, the breast | El pecho
* The head | La cabeza
* The arm | El brazo
* The neck | El cuello
* The back | La espalda
* A leg | Una pierna
* A foot | Un pie
* A finger | Un dedo
* A hand | Una mano
* The body (*alive*) | El cuerpo
* The corpse, the body (*dead*) | El cadáver
* The mind | La mente
* The mouth | La boca
* The face | La cara
* The lip | El labio
* The tongue, the language (*fem.*) | La lengua
* The skin | La piel
* The chest | El pecho
* The hair (*p…*) | El pelo
* The front | El frente
* The forehead | La frente
* The nose | La nariz
* The stomach | El estómago
* The wrist | La muñeca
* The rib | La costilla
* The shoulder | El hombro
* The knee | La rodilla
* The elbow | El codo
* The heart | El corazón
* The organ | El órgano
* I have to examine your chest | Tengo que examinarle el pecho
* The cut | El corte
* The ear | La oreja
* The (*inner*) ear | El oído
* He has has to examine your ears | Tiene que examinarle los oídos
* Rare | Raro
* The disease | La enfermedad
* You have a rare disease | Tiene una enfermedad rara
* Actually this medicine is very rare | En realidad esta medicina es muy rara
* The dentist | El dentista, la dentista
* The tooth | El diente
* The dentist has to examine your tooth | El dentista tiene que examinarle el diente
* Pregnant | Embarazada
* Injured | Herido
* She is pregnant | Está embarazada
* The flu | La gripe
* You have the flu | Tiene la gripe
* He is injured | Está herido
* He is not injured | No está herido
* Dead | Muerto
* You are not dead | No estás muerto
* Common | Común
* Actually this disease is very common | En realidad esta enfermedad es muy común
* To die | Morir
* You are not going to die | No vas a morir
* Please stand up | Por favor levántese
* I have to examine the cut | Tengo que examinar el corte
* She is not going to die | No va a morir
* To break (*oneself, something*) | Romper(*se, algo*)
* How did you break your nose? | ¿Cómo te rompiste la nariz?
* How did she break her wrist? | ¿Cómo se rompió la muñeca?
* The treatment | El Tratamiento
* Immediately | Inmediatamente
* To work, to have an effect | Hacer efecto
* This treatment works immediately | Este tratamiento hace efecto inmediatamente
* The operation | La operación
* I think you need an operation immediately | Creo que necesita una operación inmediatamente
* The antibiotics | Los antibióticos
* The doctor thinks you need antibiotics immediately | El doctor cree que necesitas antibióticos inmediatamente
* At least | Por lo menos
* To rest | Descansar
* You have to rest for at least a week | Tiene que descansar por los menos una semana
* The pill | La pastilla
* To use | Usar
* The cream | La crema
* You have to use this cream for at least three weeks | Tiene que usar esta crema por lo menos tres semanas
* To avoid | Evitar
* Unhealthy | Poco saludable
* He should avoid unhealthy food | Debería evitar la comida poco saludable
* Healthy (*e.g. food*) | Saludable
* The alcohol | El alcohol
* You should avoid alcohol | Deberías evitar el alcohol
* The junk food | La comida basura
* You shouldn't eat junk food | No deberías comer comida basura
* How did you break your rib? | ¿Cómo te rompiste la costilla?
* You shouldn't drink too mucho alcohol | No deberías beber demasiado alcohol
* He needs treatment immediately | Necesita tratamiento inmediatamente
* I think we need an ambulance immediately | Creo que necesitamos una ambulancia inmediatamente 
* You have to take these pills for at least ten days | Tiene que tomar estas pastillas por lo menos diez días
* Children should eat healthy food | Los niños deberían comer comida saludable
* The blood pressure | La tensión arterial
* High | Alto
* Low | Bajo
* You blood pressure is very high | Su tensión arterial es muy alta
* The pain | El dolor
* The anaesthetic (*a pill*) | El analgésico
* If you eat too much junkfood you will get sick | Si comes demasiada comida basura enfermarás
* If he is sick we should call a doctor | Si está enfermo deberíamos llamar a un doctor
* Healthy (*not sick*) | Sano
* He was sick but now he is healthy again | Estaba enfermo pero ahora está sano otra vez
* Depressed | Deprimido
* The depression | La depresión
* She was depressed but now is happy again | Estaba deprimida pero ahora está feliz otra vez
* She was in pain | Le dolía
* She was in so much pain we gave her anaesthetics | Le dolía tanto que le dimos analgésicos
* Her blood pressure is very low | Su tensión arterial es muy baja
* To exercise | Hacer ejercicio
* I hate exercising | Odio hacer ejercicio
* You should exercise a little everyday | Deberías hacer un poco de ejercicio todos los días
* I was in pain but not anymore | Me dolía pero ya no
* My dad was so sick they called an ambulance | Mi padre estaba tan enfermo que llamaron a una ambulancia
* My boyfriend loves exercising | A mi novio le encanta hacer ejercicio
* If you are in pain you need anaesthetics | Si le duele necesita analgésicos
* I was really sick but not anymore | Estaba realmente enfermo pero ya no

## Crime

* The guy | El tipo
* To the left | A la izquierda
* I think that it was the guy to the left | Creo que fue el tipo de la izquierda
* To the right | A la derecha
* In the middle | En el medio
* I think that it was the man to the right | Creo que fue el hombre de la derecha
* To recognise | Reconocer
* To describe | Describir
* Can you recognise the man? | ¿Puede reconocer al hombre?
* The picture, the drawing | El dibujo
* To point (*to someone, something*) | Señalar
* Can you draw a picture of him? | ¿Puede hacer un dibujo de él?
* Either…or | O…o
* Either describe or draw | O describir o dibujar
* You can either describe the man or draw a picture | Puede o describir al hombre o hacer un dibujo
* Can you describe the guy? | ¿Puede describir al tipo?
* Either in the middle or to the right | O en el medio o a la derecha
* It is either the guy in the middle or the guy to the right | Es o el tipo del medio o el tipo a la derecha
* Can you point to the man? | ¿Puede señalar al hombre?
* Either the blue knife or the black one | O el cuchillo azul o el negro
* It was either the blue knife or the black one | Fue o el cuchillo azul o el negro
* He thinks it was the guy in the middle | Cree que fue el tipo del medio
* Either point or describe | O señalar o describir
* You can either point to the man or describe him | Puede o señalar al hombre o describirlo
* The murder | El asesinato
* It was murder | Fue un asesinato
* The robbery | El robo
* It was a robbery | Fue un robo
* The suspect (*man, woman*) | El sospechoso, la sospechosa
* The witness (*man, woman*) | El testigo, la testigo
* He saw | Vio
* The witness saw the suspect | El testigo vio al sospechoso
* The gun | La pistola
* The drug | El drogo
* I saw the suspect with the drugs | Vi al sospechoso con las drogas
* The robber (*man, woman*) | El ladrón, la ladrón
* The mask | La máscara
* The girl saw the robber with the mask | La niña vio al ladrón con la máscara
* The murderer (*man, woman*) | El asesino, la asesina
* To catch | Atrapar
* The police want to catch the murderer | La policía quiere atrapar al asesino
* Red-handed | Con las manos en la masa
* The police want to catch the robber red-handed | La policía quiere atrapar al ladrón con las manos en la masa
* The weapon | El arma
* Unfortunately we can't find the murder weapon | Lamentablemente no podemos atrapar el arma homicida
* I found | Encontré
* The victim | La víctima 
* We found the weapon net to the victim | Encontramos el arma junto a la víctima
* The pocket | El bolsillo
* The police found drugs in the victim's pockets | La policía encontró drogas en los bolsillos de la víctima 
* To stab | Apuñalar
* To shoot | Disparar
* I shot | Disparé
* The murderer stabbed the victim | El asesino apuñaló a la víctima
* To kill | Matar
* Unfortunately we can't find the drugs | Lamentablemente no podemos encontrar las drogas
* The robber killed a women | El ladrón mató a una mujer
* The witness saw the suspect with a gun | El testigo vio al sospechoso con una pistola
* The police have to speak to the witnesses | La policía tiene hablar con los testigos
* Without | Sin
* There is no murder without a victim | No hay asesinato sin víctima
* The evidence | La prueba
* The case (*crime*) | El caso
* There is no case without evidence | No hay caso sin pruebas
* The alibi | La coartada
* Unfortunately the suspect has an alibi | Lamentablemente el sospechoso tiene coartada 
* The suspect has no alibi | El sospechoso no tiene coartada
* Everywhere | Por todas partes
* There were witnesses everywhere | Había testigos por todas partes
* The blood | La sangre
* The apartment | El apartamento
* In the apartment there was blood everywhere | En el apartamento había sangre por todas partes
* To steal | Robar
* He stole | Robó
* Someone stole my wallet | Alguien robó mi cartera
* She still has the drugs | Todavía tiene las drogas
* To prove | Probar
* The police can't prove anything | La policía no puede probar nada
* The police can't prove anything without evidence | La policía no puede probar nada sin pruebas
* To arrest | Arrestar
* The police can't arrest the suspect without evidence | La policía no puede arrestar al sospechoso sin pruebas
* There is no suspect without the murder weapon | No hay sospechoso sin el arma homicida
* Someone stole the drugs | Alguien robó las drogas
* The police have all the evidence | La policía tiene todas las pruebas
* Unfortunately you have no alibi | Lamentablemente no tienes coartada
* We can't arrest the murderer without evidence | No podemos arrestar al asesino win pruebas 
* Someone stole my passport | Alguien robó mi pasaporte
* That doesn't prove anything | Eso no prueba nada
* He knows nothing | No sabe nada
* To swear | Jurar
* I swear | Lo juro
* To remember | Recordar
* You remember (*sing. formal*), he remembers | Recuerda
* To look like | Cómo ser
* He looked like | Cómo era
* Do you remember what he looked like? | ¿Recuerda cómo era?
* The monster | El monstruo
* Normal | Normal
* To look like (*to appear like*) | Parecer
* He looked like (*he appeared like*) | Parecía
* He looked like a monster | Parecía un monstruo
* The customer (*man, woman*) | El cliente, la clienta
* Less | Menos
* Than | Que (*comparison*)
* He had less hair than that guy | Tenía menos cabello que ese tipo
* Tall | Alto
* He was taller | Era más alto
* He was taller than this guy | Era más alto que este tipo
* To believe | Creer
* I don't believe him | No le creo
* The judge (*man, woman*) | El juez, la jueza
* To sentence (*someone*) | Condenar (*a alguien*)
* He sentenced (*someone*) | Condenó (*a alguien*)
* The judge sentenced the robber | El juez condenó al ladrón
* The prison | La prisión
* The judge sentenced the robber to five years in prison | El juez condenó al ladrón a cinco años de prisión
* The life sentence | La cadena perpetua
* The judge sentenced the murderer with a life sentence | El juez condenó al asesino a cadena perpetua
* I know nothing | No sé nada
* I don't believe you | No te creo
* The judge sentenced the suspect to twelve years in prison | El juez condenó al sospechoso a doce años de prisión 
* The police don't believe the witness | La policía no cree al testigo
* He looked like a normal customer | Parecía un cliente normal
* I look like | Cómo soy
* Does he remember what I look like? | ¿Recuerda cómo soy?
* He was younger | Era más joven
* He was younger than the guy in the middle | Era más joven que el tipo de en medio

## Politics

* The president (*man, woman*) | El presidente, la presidenta
* Who was the first president of the United States? | ¿Quién fue el primer presidente de los Estados Unidos?
* The first president of the United States was… | El primer presidente de los Estados Unidos fue…
* Great Britain | Gran Bretaña
* The primer minister (*man, woman*) | El primer ministro, la primara ministra
* Who was the first prime minister of Great Britain? | ¿Quién fue la primera ministra de Gran Bretaña?
* …who was the prime minister of Great Britain in nineteen eighty two | …fue la primera ministra de Gran Bretaña en mil novecientos ochenta y dos
* The queen | La reina
* The king | El rey
* Is there a king in the United States? | ¿Hay rey en los Estados Unidos?
* There is no king or queen in the United States | No hay ni rey ni reina en los Estados Unidos
* The government | El gobierno
* Is there a prime minister in Spain? | ¿Hay primer ministro en España?
* There is a king and a queen in Spain | Hay rey y reina en España
* A lot | Un montón
* The power | El poder
* The government doesn't have a lot of power | El gobierno no tiene un montón de poder
* The military | El militar
* The military doesn't have a lot of power | Los militares no tienen un montón de poder
* The (*political*) party | El partido
* Republican | Republicano 
* The Republican Party is pretty big | El Partido republicano es bastante grande 
* Is there a president in Great Britain? | ¿Hay presidente en Gran Bretaña?
* There is no president in Great Britain | No hay presidente en Gran Bretaña
* Democratic (*party*) | Democrático
* Democratic (*politics*) | Demócrata
* Communist | Comunista
* The Democratic Party is pretty small | El Partido Democrático es bastante pequeño
* Who is the queen of England? | ¿Quién es la reina de Inglaterra?
* The queen of England is Elizabeth the second | La reina de Inglaterra es Isabel segunda
* The Communist Party is pretty big | El Partido Comunista es bastante grande
* The king doesn't have a lot of power | El rey no tiene un montón de poder 
* Sometimes | A veces
* The war | La guerra
* Necessary | Necesario
* Sometimes was is necessary | A veces la guerra es necesaria
* War is never necessary | La guerra nunca es necesaria
* The discussion | La discusión
* Sometimes discussions are necessary | A veces las discusiones son necesarias
* The peace | La paz
* The people want peace | La gente quiere la paz
* Everything | Todo
* The people want everything | La gente quiere todo
* The politician | El político, la política
* To lower | Bajar
* The tax | El impuesto
* Sometimes politicians promise they are going to lower taxes | A veces los políticos prometen que van a bajar los impuestos
* To control | Controlar
* The state | El estado
* The economy | La economía
* The state wants to control the economy | El estado quiere controlar la economía
* Nothing | Nada
* Actually politicians control nothing | En realidad los políticos no controlan nada
* Excellent | Excelente
* The leader | El líder, la líder
* The president is an excellent leader | El presidente es un líder excelente
* The Republican | El republicano
* The Democrat | El demócrata
* The Republicans have an excellent leader | Los republicanos tienen una líder excelente
* The Democrats have a pretty good leader | Los demócratas tienen un líder bastante bueno
* The people want war | La gente quiere la guerra
* Politicians always promise to lower taxes | Los políticos siempre prometen bajar los impuestos
* The prime minster is a pretty good leader | La primera ministra es una líder bastante buena
* The state want to control everything | El estado quiere controlar todo
* The (*political*) election | Las elecciones
* When is the next election ? | ¿Cuándo son las próximas elecciones?
* The next election is in two years | Las próximas elecciones son en dos años
* The debate | El debate
* When is the next debate? | ¿Cuándo es el próximo debate?
* The next debate is in seven days | El próximo debate es en siete días
* Reliable | Fiable
* Do you think the president is reliable? | ¿Crees que el presidente es fiable?
* I think that he is a reliable president | Creo que es un presidente fiable
* To be unreliable | No ser fiable
* The poll | El sondeo
* I think that polls are unreliable sometimes | Creo que a veces los sondeos no son fiables
* The candidate | El candidato, la candidata
* To suggest (*propose*) | Proponer
* To increase | Aumentar
* I suggest lowering taxes | Propongo bajar los impuestos
* Sometimes they suggest increasing taxes | A veces proponen aumentar los impuestos
* The religion | La religión
* Important | Importante
* Religion is an important topic | La religión es un tema importante
* The environment | El medioambiente
* Education is a very important topic | La educación es un tema muy importante
* The vote | El voto
* Voting | Votar, votando
* Voting makes him nervous | Votar le pone nervioso
* The result | El resultado
* This year the Democratic Party will get better results | Este año el Partido Democrático conseguirá mejores resultados
* Poll results make him nervous | Los resultados de los sondeos le ponen nervioso
* Votes are important | Los votos son importantes
* Yesterday the Republican Party got better results | Ayer el Partido Republicano consiguió mejores resultados
* Do you think he is a reliable prime minister? | ¿Crees que es un primer ministro fiable?
* Sometimes poll results makes me nervous | A veces los resultados de sondeos me ponen nervioso
* The health care system | El sistema de asistencia sanitaria
* Debates about the economy make them nervous | Los debates sobre la economía les ponen nervioso
* The environment is an important topic | El medioambiente es un tema importante
* Discussions about the health care system make her angry | Los discusiones sobre la sistema de asistencia sanitaria le enfadan
* The president thinks that he is an excellent candidate | El presidente cree que es un candidato excelente
