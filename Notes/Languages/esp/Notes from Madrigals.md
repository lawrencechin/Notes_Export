# Notes From Madrigals

## Pronounciation Key

* "A" - "ah"
* "E" - "E" as in bet, test, bless
* "I" - "EE" as in greet, beet
* "O" - "O" as in obey but without the slightest trace of a U sound 
* "U" - "OO" as in cool, pool
* "B" - 
    1. "B" as in bit
    2. When it appears between vowels it is very soft like the word abundancia
* "C" -
    1. "C" before "A", "O", "U" is hard as in can
    2. "C" before "E", "I", is soft as in cent
* "CC" - "X". Acción is pronounced "axion"
* "CH" - like child
* "D" - 
    1. "D" as in do
    2. When it is the last letter or between vowels it is pronounced as a very soft "TH". Practice with ciudad
* "G" - 
    1. Before "A", "O", "U" is hard as in get
    1. Before "E" or "I" is pronounced "H" as in hen (general ~ heneral)
* "H" - Silent
* "J" - "H" as in hen
* "L" - "L" as in let
* "LL" - "Y" as in yes. Caballo is pronounced cabayo.
* "ñ" - "NY". Cabaña is pronounced cabanya.
* "R" - Slightly trilled unless it is the first letter when it is strongly trilled
* "RR" - Strongly trilled
* "T" - "TT" as in attractive. "Patio" (pattio), "simpático" (simpáttico)
* "Y" - 
    1. "Y" as in yet
    2. When it stands alone is pronounced "EE" as in beet
* "Z" - "S" as in sent
* "QUE" - "KE" as in kept
* "QUI" - "KEE" as in keen, keep
* "GUE" - "GUE" as in guest
* "GUI" - "GEE" as in geese

In some parts of Spain the "C" before "E" or "I" is pronounced "TH" and "Z" is always pronounced "TH".

## Converting from English to Spanish

Eng Ending | Esp Ending | Examples | Notes | Lección Número
-- | -- | -- | -- | --
or | or | el actor, el color | Stress is on last syllable | 1
al | al | el animal, el natural | | 1
ble | ble | posible, horrible, inevitable | | 1
ent/ant | ente/ante | presidente, excelente, conveniente | | 1
ist | ista | pianist, pianista | |  2
ous | oso | famous, famoso | | 2
tion | ción | action, acción | | 2
sion | sión | confusion, confusión | Add an accent over the o | 2
ty | dad | capacity, capacidad | | 4
ry | rio | canary, canario | | 5
em, am | ema, ama | program, programa | | 5
ce | cia | distance, distancia | | 7
cy | cia | agency, agencia | | 7
^S | ^Es | special, especial | | 7
ine | ina | sardine, sardina | | 10
ive | ivo | active, activo | | 11
ly | mente | constantly, constantemente | You create adverbs with "mente" rather than translating "ly" to "mente" from English | 13
consonant + "y" | "ía" | energy, energía | | 16
ct | cto | product, producto | | 19
ure | ura | estructura, prematura | | 20
ute | uto | bruto, minuto, atributo | | 20

## Irregular Verbs
### Noncomformist Verbs
#### Group 1

Present | 1st Person | Singular | Ends in | "go"
-- | -- | -- | -- | --
hacer | hago | hace | hacemos | hacen
poner | pongo | pone | ponemos | ponen
traer | traigo | trae | traemos | traen
caer | caigo | cae | caemos | caen
salir | salgo | sale | salimos | salen
tener | tengo | tiene | tenemos | tienen
venir | vengo | viene | vinimos | vienen
oír | oigo | oye | oímos | oyen
decir | digo | dice | decimos | dicen
-- | -- | -- | -- | --
Past | Preterite | Endings | | 
-- | -- | -- | -- | --
hacer | hice | hizo | hicimos | hicieron
poner | puse | puso | pusimos | pusieron
traer | traje | trajo | trajimos | trajeron
caer | caí | cayó | caímos | cayeron
salir | salí | salió | salimos | salieron
tener | tuve | tuvo | tuvimos | tuvieron
venir | vine | vino | vinimos | vinieron
oír | oí | oye | oímos | oyeron
decir | dice | dijo | dijimos | dijeron

#### Group 2

Present | 1st Person | Singular | Ends in | "oy"
-- | -- | -- | -- | --
ir | voy | va | vamos | van
estar | estoy | está | estamos | están
dar | doy | da | damos | dan
ser | soy | es | somos | son
-- | -- | -- | -- | --
Past | Preterite | Endings | |
-- | -- | -- | -- | --
ir | fui | fue | fuimos | fueron
estar | estuve | estuvo | estuvimos | estuvieron
dar | dí | dio | dimos | dieron
ser | fui | fue | fuimos | fueron

#### Group 3

Radical | Changing | in | Present | --
-- | -- | -- | -- | --
poder | puedo | puede | podemos | pueden
querer | quiero | quiere | queremos | quieren
-- | -- | -- | -- | --
Past | Preterite | Endings | |
-- | -- | -- | -- | --
poder | pude | pudo | pudimos | pudieron
querer | quería | quería | queríamos | querían

##### Group 4

Different | from | Other | Groups | --
-- | -- | -- | -- | --
saber | sé | sabe | sabemos | saben
ver | veo | ve | vemos | ven
-- | -- | -- | -- | --
Past | Preterite | Endings | |
-- | -- | -- | -- | --
saber | sabía | sabía | sabíamos | sabían
ver | vi | vio | vimos | vieron

Regular | completely | in | present | --
-- | -- | -- | -- | --
andar | ando | anda | andamos | andan
-- | -- | -- | -- | --
Past | Preterite | Endings | |
-- | -- | -- | -- | --
andar | anduve | anduvo | anduvimos | anduvieron

## Direct and Indirect Pronoun Objects

Any verb that one can do 'to' or 'for' takes the indirect pronouns 'le', 'me', 'nos' and 'les' (e.g. I brought you some records ~ Le traje unos discos).

The word order is always the same and must be adhered to:
*To person* **verb** *object* **to whom** ~ Le dí un libro a Roberto (To him I gave a book to Robert). To ask a question follows the same word order but with question marks or inflection.

The direct pronouns are:
* Lo : Him
* La : Her
* Los : Them (*masc*)
* Las : Them (*fem*)

When using the direct and indirect pronouns together the word order is: *indirect* **direct** *verb* ~ Me lo dio (He gave it to me). In addition, using 'le' with the direct pronoun changes 'le' to 'se' (e.g. Se lo dio ~ You gave it to her, 'lo' refers to 'it' in this sentence so is 'lo' rather than 'la').

Lastly, when specifying who did the action you say the person first (e.g. Carlos se lo dio ~ Carlos gave it to her).

## Lección Número Uno

* English words that end in "or" are often identical in Spanish (el actor, el color). The stress is on the last syllable.
* Words that end in "al" are also often identical (el animal, natural).
* Words that end in "ble" are also largely the same. The stress is on the penultimate vowel (posible, horrible, inevitable).
* Words that end in "ic" need an "o" appended. The stress is on the penultimate vowel or the accented letter (Atlántico, democrático, automático).
* Words that end with either "ent" or "ant" require appending an "e" (presidente, excelente, conveniente).

To form questions in Spanish you change the word order. Instead of "is the actor popular?" you would say "is popular the actor?" (¿es popular el actor?).

### Exercise in Translation

1. El actor es popular
2. El tenor es popular
3. El restaurante es excelente
4. El hotel es excelente
5. El doctor es inteligente
6. El cable es urgente
7. El hospital es excelente
8. El cliente es importante
9. El conductor es paciente
10. El general es importante
11. El presidente es democrático
12. El inventor es inteligente
13. El general es diplomático
14. El cable es importante

### Basic Spelling Differences

1. "ph" - "f" (elefante, teléfono, fonético, fonógrafo)
2. "th" - "t" (catedral, autor, auténtico, metodista, católico)
3. Double letters become single in Spanish (aside from "ll", "rr")(comisión, aparente, anual, atractivo, diferente)
    * "cc" is not a double but part of two separate syllables. The first has a "k" sound and the second an "s" or "th" (corrección)
4. "tion" becomes "ción" (convención, convencional).

## Lección Número Dos

* Words that in "ist" convert into Spanish by adding an "a" (pianist, pianista)
* Word ending in "ous" translate to "oso" (famous, famoso)
* Words that end in "tion" change the "t" to "c" (action, acción)
* Words that end in "sion" are the same but with an accent "sión" (confusion, confusión)

### Exercise in Translation

1. El pianista es famoso
2. El artista es ambicioso
3. El doctor es generoso
4. El novelista es inteligente
5. El hotel es excelente
6. El rosbif es delicioso
7. El bistec es delicioso
8. En mi opinión, el restaurante es excelente
9. El programa es muy interesante

## Lección Número Tres

* Nouns that end in "ación" make for simple past tense verbs
    * Conversación -> Conversé (Conversation -> (I) conversed)
    * Preparación -> Preparé (Preparation -> (I) prepared)
* The "you" form (formal) replaces "e" with "o"
    * Combinación -> Combinó
    * Presentación -> Presentó

### Exercise in Translation

1. Preparé la lección esta mañana
2. Preparé la cena
3. Preparé la lección para la clase
4. No preparé la cena esta tarde
5. No preparé la lección esta mañana
6. ¿Preparó usted la lección?
7. ¿Preparó usted la cena?
8. ¿Preparó usted la lección esta mañana?
9. ¿Preparó usted la cena esta tarde?
10. ¿Preparó usted la lección para la clase?
11. ¿Preparó la lección esta tarde?

## Lección Número Cuatro

* Words ending in "ty" convert to "dad" (capacity, capacidad)
* Some "ty" -> "dad" words that are seldom used in English are common in Spanish like "velocidad" is used as one might use "speed" in English. Examples:
    * Debilidad -> Weakness
    * Autoridad -> Authority
    * Facilidad -> Ease
    * Caridad -> Charity
    * Castidad -> Chastity
    * Felicidad -> Happiness
    * Inmortalidad -> Immortality
    * Necesidad -> Need
    * Obscuridad -> Darkness
    * Velocidad -> Speed

### Exercise in Translation

1. Tomé rosbif
2. Tomé una ensalada en el restaurante
3. Tomé sopa
4. Tomé té en el club
5. Tomé un emparedado en la estación
6. Tomé una aspirina esta mañana
7. Tomé un taxi esta tarde
8. No tomé un bistec 
9. Tomé el avión en el aeropuerto
10. ¿Tomó usted sopa?
11. ¿Tomó usted una ensalada?
12. ¿Tomó usted la cena en el aeropuerto?
13. ¿Tomó usted café en el club?
14. ¿Tomó usted una aspirina?
15. ¿Tomó usted un taxi?
16. ¿Tomó usted el tren?
17. ¿Tomó usted el avión?

## Lección Número Cinco

* Words ending in "ry" often can be changed with "rio" (canary, canario)
* Words ending in "em" or "am" sometimes can be used with "a" (program, programa)
* When PERSONS follow verbs the letter "a" is used in between, called the personal "a" (invité a Roberto)

### Exercise in Translation

1. Invité a mi mamá al cine
2. Invité su mamá a la fiesta
3. Invité a Roberto al cine esta noche
4. Visité a su mamá esta mañana
5. Visité a Roberto esta tarde
6. ¿Visitó usted a mi papá esta tarde?
7. ¿Visitó usted a mi mamá esta mañana?
8. ¿Visitó usted a Roberto esta noche?
9. ¿Invitó usted a Roberto a la fiesta?
10. ¿Invitó usted a un amigo a la clase?
11. ¿Invitó usted a su mamá al cine?
12. ¿Invitó usted a su papá al teatro?

## Lección Número Seis
### Exercise in Translation

1. I invited a friend to me house last night
2. Roberto visited a friend in Cuba
3. Mother prepared the dinner last night
4. The president accepted the invitation
5. I finished the composition for the class
6. María studied the lesson this morning
7. Roberto dined in a restaurant 
8. The doctor had a aspirin
9. I used vocabulary in the class
10. Alexander Graham Bell invented the telephone
11. Don Ameche didn't invent the telephone
12. Marconi invented the radio
13. I copied the lesson last night
14. The profesor had a Pepsi
15. The agent imported perfume from France
16. My mother visited a friend in hospital
17. I had asparagus
18. I parked my car
19. My dad recommended his friend (*your* friend)
20. The general entered the capital
21. The minister prepared the sermon for the congregation
22. My client imported coffee from Cost Rica
23. I toasted the bread for the sandwiches
24. Juan entered the hotel and greeted his friend
25. The mechanic lubricated the car in the garage
26. My friend cultivated coffee on the ranch
27. The group presented a drama
28. The professor greeted the student (*congratulated*)
29. Did you vote this morning?
30. The doctor cured the patient
31. I deposited money in the bank this morning
32. Did you copy the composition?
33. Did you study the lesson for the class?
34. Did you accept the invitation?
35. My father invited Juan to the cinema
36. Juan accepted the invitation
37. Edison invented the phonograph

## Lección Número Siete

* Some words that end in "ce" can be converted by changing to "cia" (distance, distancia)
* Some words that end in "cy" translate with "cia" (agency, agencia)
* When a word in English starts with "S" and is followed by a consonant you must put an "e" before the "s" in Spanish (special, especial)

### Exercise in Translation

1. Hablé con María esta mañana
2. Hablé de México en la clase
3. No hablé con el doctor
4. Habló María con Juan
5. El doctor habló por teléfono
6. Roberto habló por teléfono
7. El doctor habló del accidente
8. Roberto habló del teatro
9. ¿Habló usted con el doctor?
10. ¿Habló usted de México en la clase?
11. ¿Habló usted del teatro?
12. ¿Habló usted por teléfono?
13. ¿Habló mamá con el doctor?
14. ¿Habló Roberto en la clase esta mañana?
15. ¿Habló María del teatro?

## Lección Número Ocho

* Words that end in "tion" translate to "ción". Removing this ending and then adding "é" or "ó" gives you the past verb
    * Invitation (eng)
    * Invitación (esp)
    * Invité (esp - I invited)
    * Invitó (esp - You invited)

1. Invité a María a la fiesta
2. Preparé la cena anoche
3. El general cooperó con el presidente
4. El mono imitó a María
5. Roberto recitó un poema en la clase

### Exercise in Writing

Nouns | 1st Person | 3rd Person
-- | -- | --
la acumulación | acumulé | acumuló
la acusación | acusé | acusó
la administración | administré | administró
la admiración | admiré | admiró
la adoración | adoré | adoró
la afirmación | afirmé | afirmó
la agitación | agité | agitó
la animación | animé | animó
la anticipación | anticipé | anticipó
la apreciación | aprecié | apreció
la asociación | asocié | asoció
la aspiración | aspiré | aspiró
la aproximación | aproximé | aproximó
la calculación | calculé | calculó
la coagulación | coagulé | coaguló
la colaboración | colaboré | colaboró
la combinación | combiné | combinó
la compensación | compensé | compensó
la compilación | compilé | compiló
la concentración | concentré | concentró
la condensación | condensé | condensó
la confirmación | confirmé | confirmó
la congratulación | congratulé | congratuló
la conservación | conservé | conservó
la consideración | consideré | consideró
la consolación | consolé | consoló
la consolidación | consolidé | consolidó
la contaminación | contaminé | contaminó
la contemplación | contemplé | contempló
la continuación | continué | continuó
la conversación | conversé | conversó
la cooperación | cooperé | cooperó
la coordinación | coordiné | coordinó
la culminación | culminé | culminó
la cultivación | cultivé | cultivó
la declaración | declaré | declaró
la decoración | decoré | decoró
la deliberación | deliberé | deliberó
la denunciación | denuncié | denunció
la deterioración | deterioré | deterioró
la determinación | determiné | determinó
la detestación | detesté | detestó
la dominación | dominé | dominó 
la estimulación | estimulé | estimuló
la estrangulación | estrangulé | estranguló
la evaporación | evaporé | evaporó
la exageración | exageré | exageró
la exasperación | exasperé | exasperó
la excavación | excavé | excavó
la excitación | excité | excitó
la exclamación | exclamé | exclamó
la experimentación | experimenté | experimentó
la exploración | exploré | exploró
la exportación | exporté | exportó
la exterminación | exterminé | exterminó
la fascinación | fasciné | fascinó
la fermentación | fermenté | fermentó
la fluctuación | fluctué | fluctuó
la formación | formé | formó
la formulación | formulé | formuló
la graduación | gradué | graduó
la imaginación | imaginé | imaginó
la imitación | imité | imitó
la imploración | imploré | imploró
la improvisación | improvisé | improvisó
la inauguración | inauguré | inauguró
la inflación | inflé | infló
la información | informé | informó
la iniciación | inicié | inició
la inoculación | inoculé | inoculó
la insinuación | insinué | insinuó
la inspiración | inspiré | inspiró
la instalación | instalé | instaló
la interpretación | interpreté | interpretó
la invitación | invité | invitó
la irritación | irrité | irritó
la laceración | laceré | laceró
la lamentación | lamenté | lamentó
la limitación | limité | limitó
la manifestación | manifesté | manifestó
la manipulación | manipulé | manipuló
la matriculación | matriculé | matriculó
la meditación | medité | meditó
la observación | observé | observó
la ocupación | ocupé | ocupó
la operación | operé | operó
la orientación | orienté | orientó
la participación | participé | participó
la penetración | penetré | penetró
la perforación | perforé | perforó
la precipitación | precipité | precipitó
la predominación | predominé | predominó
la premeditación | premedité | premeditó
la preocupación | preocupé | preocupó
la preparación | preparé | preparó
la presentación | presenté | presentó
la preservación | preservé | preservó
la proclamación | proclamé | proclamó
la protestación | protesté | protestó
la recitación | recité | recitó
la reclamación | reclamé | reclamó
la recomendación | recomendé | recomendó
la recuperación | recuperé | recuperó
la reformación | reformé | reformó
la regeneración | regeneré | regeneró
la registración | registré | registró
la reparación | reparé | reparó
la representación | representé | representó
la salvación | salvé | salvó
la saturación | saturé | saturó
la separación | separé | separó
la situación | situé | situó
la toleración | toleré | toleró
la transformación | transformé | transformó
la transportación | transporté | transportó
la vacilación | vacilé | vaciló
la variación | varié | varió
la ventilación | ventilé | ventiló
la vibración | vibré | vibró

### Exercise in Translation

1. Roberto visitó a mi padre
2. María invitó a su amigo
3. El paciente preocupó al doctor
4. El general salvó a su amigo
5. ¿Quién invitó a Juan?
6. ¿Quién recomendó a Juan?
7. ¿Quién invitó a Roberto?
8. El novelista acumuló una fortuna
9. María depositó el dinero en el banco
10. El estudiante usó el vocabulario
11. Alberto terminó la composición 
12. Mi madre preparó la cena
13. Juan estacionó el auto
14. Mi padre habló por teléfono
15. ¿Quién instaló el fonógrafo?

## Lección Número Nueve

* In Spanish "es" and "está" mean "is". "Está" is used to say where things are or to ask where things are (¿Dónde está el banco?)
* "Está" is also used to say how people are or ask how people are (¿Cómo está usted?)

### Exercise in Translation

1. ¿Dónde está la estación?
2. ¿Dónde está el banco?
3. ¿Dónde está el teléfono?
4. ¿Dónde está su papá?
5. ¿Dónde está el auto?
6. El mecánico está en el garage
7. El dinero está en el banco
8. El tren está en la estación
9. Su sombrero está en el sofá
10. La crema está en el refrigerador
11. ¿Cómo está usted?
12. ¿Cómo está su mamá?
13. Mi mamá está bien, gracias
14. El paciente está mejor
15. Mi papá está mejor, gracias

## Lección Número Diez

* Some words that end in "ine" translate into Spanish with "ina" in its stead (sardine, sardina)

### Exercise in Translation

1. ¿Compró usted una blusa esta mañana?
2. ¿Compró usted el auto en la agencia de automóviles?
3. ¿Compró usted el perfume en París?
4. ¿Compró usted el auto la semana pasada?
5. ¿Compró usted chocolates para su madre?
6. ¿Compró usted un suéter para su padre?
7. Compré un sombrero esta mañana
8. ¿Compró Marta un sombrero en París?
9. No compró Alicia pan esta mañana
10. ¿Qué compró Marta en la tienda?
11. ¿Qué compró Roberto en la farmacia?
12. ¿Qué compró su padre esta tarde?
13. ¿Cuándo compró usted la casa?
14. ¿Cuándo compró usted el auto?
15. ¿Cuándo compró Marta la blusa?

## Lección Número Once

* Using the phrase "voy a" (I'm going to) in combination with the infinitive verb allows one to express future action (Voy a visitar, I'm going to visit).
* Words that end in "ive" often translate to "ivo" (active, activo)
* "Por" is used communications and time (por radio, on the radio)
* "Para" is used with people and occasions (para mi mamá, for my mother)

### Exercise in Translation

1. Voy a tomar un taxi
2. Voy a comprar un auto
3. Voy a visitar a María esta noche 
4. Voy a comprar un sombrero esta tarde
5. Voy a estar en casa esta mañana
6. Voy a estudiar la lección esta tarde
7. Voy a invitar a Alicia a la fiesta
8. ¿Va a terminar la composición?
9. ¿Va a Cuba?
10. ¿Va a la clase?
11. ¿Va a estar en casa esta noche?
12. ¿Va a estacionar el auto?

### Test Your Progress
#### Test 1

1. El actor
2. Natural
3. EL presidente
4. El favor
5. Elástico
6. Probable
7. El restaurante
8. Artístico
9. El cable
10. Superior
11. El doctor
12. Importante
13. Urgente
14. El cliente
15. Personal
16. La licencia X Got it wrong
17. La aspirina
18. Descriptivo
19. Curiosidad
20. La situación
21. Delicioso
22. Flexible
23. La agencia
24. Atractivo
25. La sardina
26. Terrible
27. El dentista
28. Generoso
29. La invitación
30. Insignificante
31. El optimista
32. Famoso
33. La acción
34. El pianista
35. Meticuloso
36. La universidad
37. El tractor
38. Prosperidad
39. El telegrama X Got it wrong
40. La producción
41. La distancia
42. La medicina
43. La televisión
44. La coincidencia
45. Activo
46. Electricidad
47. La recomendación
48. Excelente
49. Responsabilidad
50. La tendencia

#### Test 2

1. Preparé
2. ¿Tomó usted?
3. No invité
4. Visité
5. Hablé
6. ¿Habló usted?
7. Roberto habló
8. Compré
9. ¿Compró usted?
10. Voy a invitar
11. ¿Va a visitar?
12. Voy a Cuba
13. No preparé
14. Invité
15. Roberto visitó
16. ¿Va a comprar?
17. Voy a tomar
18. Voy a hablar
19. ¿Va a hablar?
20. Voy a estudiar

#### Test 3

1. It is important
2. The programme is very interesting
3. I prepared the lesson this morning
4. I had dinner with Robert last night
5. Mary talked on the phone
6. I spoke about México in the class
7. Did you copy the composition?
8. My father parked the car
9. Who spoke on the phone this morning?
10. Who entered?
11. The patient worried the doctor
12. How are you?
13. I bought a blouse this morning
14. What did Alice buy this afternoon
15. When did you buy the house?
16. Where is my hat?
17. Are you going to be in Cuba tomorrow?
18. I am going to the cinema tonight
19. I had dinner on the train X Lazy error! I going to have…
20. Who is going to buy the tickets?

## Lección Número Doce
### Exercise in Translation

1. ¿Va a comprar una casa Roberto?
2. ¿Va a trabajar mañana María?
3. ¿Va a estacionar el auto Carlos?
4. ¿Va a tomar un taxi su papá?
5. ¿Va a preparar la cena su mamá?
6. ¿Va a comprar un auto el doctor?
7. ¿Va a preparar la lección Carlos?
8. ¿Va a tomar la cena Marta?
9. Carlos va a importar café
10. Marta va a votar mañana
11. El doctor va a visitar al paciente
12. Mi mamá va a aceptar la invitación
13. Mi padre va a tomar la cena
14. Alicia va a tomar un taxi
15. María va a recitar un poema 

## Lección Número Trece

* You create adverbs in Spanish by adding "mente" like you would add "ly" in English (personally, personalmente)
* If the adjective in Spanish ends in "o" change it to an "a" and add "mente" (completo, completamente)

### Exercise in Translation

1. ¿Va a escribir la carta María?
2. ¿Va a vender el radio Carlos?
3. ¿Va a vender la casa su papá?
4. ¿Va a servir café Alicia?
5. ¿Va a comprender el cable Marta?
6. ¿Va a recibir el telegrama su secretaria?
7. ¿Va a comprender la lección Roberto?
8. Carlos va a escribir una carta
9. María va a vender el fotógrafo
10. María va a escribir una composición
11. Mi madre va a servir café
12. Carlos va a recibir el cable

## Lección Número Catorce

* The past tense of verbs that end in "ar" is "é" or "ó"
* For verbs that end in "er" or "ir" the endings are "í" and "ió"
* If a word ends in a vowel, add "s" to form a plural (fábrica, fábricas)
* If a word ends in a consonant, add "es" to form the plural (universidad, universidades)

### Written Exercise
#### Verb List

Infinitives | I | You, He, She, It
-- | -- | --
asistir | asistí | asistió
batir (*to beat eggs*) | batí | batió
confundir (*to confuse*) | confundí | confundió
consistir | consistí | consistió
decidir | decidí | decidió
describir | describí | describió
descubrir | descubrí | descubrió
discutir | discutí | discutió
dividir | dividí | dividió
escribir | escribí | escribió
evadir | evadí | evadió
exhibir | exhibí | exhibió
existir | existí | existió
interrumpir | interrumpí | interrumpió
invadir | invadí | invadió
ocurrir | ocurrí | ocurrió
permitir | permití | permitió
persuadir | persuadí | persuadió
prohibir | prohibí | prohibió
recibir | recibí | recibió
resistir | resistí | resistió
sufrir | sufrí | sufrió
vivir | viví | vivió
absorber | absorbí | absorbió
aprehender | aprehendí | aprehendió
cometer | cometí | cometió
comprender | comprendí | comprendió
conmover (*to move emotionally*) | conmoví | conmovió
convencer (*convince*) | convencí | convenció
depender | dependí | dependió
disolver | disolví | disolvió
exceder | excedí | excedió
extender | extendí | extendió
favorecer (*to favor*) | favorecí | favoreció
mover | moví | movió
ofender | ofendí | ofendió
preceder | precedí | precedió
resolver | resolví | resolvió
suspender | suspendí | suspendió
vender | vendí | vendió

### Exercise in Translation

1. ¿Recibió usted una carta esta mañana?
2. ¿Recibió usted un cable de Carlos?
3. ¿Recibió un telegrama de mi papá?
4. Recibí muchas cartas esta mañana
5. Recibí un telegrama de mi padre
6. Recibí un paquete de México
7. Escribí una composición anoche
8. Escribí una carta esta mañana
9. ¿Escribió usted un poema?
10. ¿Vendió usted la casa la semana pasada?
11. ¿Vendió usted la radio esta mañana?
12. ¿Vendió usted el auto esta tarde?
13. ¿Compró usted la guitarra en Colombia?
14. Compré el auto en México
15. Compré un rancho en México

## Lección Número Quince

* Adjectives must match both the gender and quantity of the subject
* If the adjective ends in an "e" then the word doesn't change based on gender
* Examples :
    * El actor famoso | Los actores famosos
    * El doctor ambicioso | Los doctores ambiciosos
    * La mujer bonita | Las mujeres bonitas
    * La blusa negra | Las blusas negras
    * El sombrero grande | La casa grande
    * Los hombres interesantes | Las mujeres interesantes

### Exercise in Translation

1. ¿Vio usted a Carlos anoche?
2. ¿Vio usted al doctor esta tarde?
3. ¿Vio usted al general la semana pesada?
4. ¿Vio usted a Antonio anoche?
5. Vi un programa de televisión
6. Vi a su secretaria esta tarde
7. Vi a Antonio anoche
8. Vi a Carlos esta mañana
9. Voy a ver al doctor mañana
10. Voy a ver a Roberto esta noche
11. Voy a ver a su secretaria mañana
12. Voy a ver a su padre esta noche
13. Vamos a ver un programa de televisión 
14. Vamos a ver al general en Cuba
15. Vamos a ver a Roberto mañana

## Lección Número Dieciséis 

* Words that end in a consonant and "y" can often be converted into Spanish replacing "y" to "ía" (anatomy, anatomía)

### Exercise in Translation

1. ¿Leyó usted el periódico esta mañana?
2. ¿Leyó usted una novela la semana pasada?
3. ¿Leyó usted las frases en la clase?
4. ¿Leyó usted muchas cartas en el despacho?
5. ¿Leyó usted la revista en el tren?
6. Leí una artículo en el periódico
7. Leí un libro en el avión
8. Leí un poema en la clase
9. ¿Va a leer las frases?
10. ¿Va a leer la revista en el tren?
11. ¿Leyó Roberto un libro en el avión?
12. ¿Vio usted las fotografías en el periódico?
13. ¿Vio usted las flores en el parque?
14. Vi a Roberto en el club
15. Vi las fotografías en la revista

## Lección Número Diecisiete
### Written Exercise

Infinitives | We | They, you(pl.)
-- | -- | --
tomar | tomamos | tomaron
visitar | visitamos | visitaron
invitar | invitamos | invitaron
comprar | compramos | compraron
trabajar | trabajamos | trabajaron
hablar | hablamos | hablaron
preparar | preparamos | prepararon
cultivar | cultivamos | cultivaron
votar | votamos | votaron
inventar | inventamos | inventaron
importar | importamos | importaron
exportar | exportamos | exportaron
copiar | copiamos | copiaron
depositar | depositamos | depositaron
progresar | progresamos | progresaron
aceptar | aceptamos | aceptaron
estudiar | estudiamos | estudiaron
terminar | terminamos | terminaron
comenzar | comenzamos | comenzaron
entrar | entramos | entraron
estacionar | estacionamos | estacionaron
recomendar | recomendamos | recomendaron
instalar | instalamos | instalaron
ventilar | ventilamos | ventilaron

### Exercise in Translation

1. ¿Tomaron (ustedes) la cena en el restaurante?
2. ¿Tomaron rosbif para la cena?
3. ¿Visitaron a María en el hospital?
4. ¿Invitaron a Juan a la fiesta?
5. Compraron una casa en el campo
6. Copiaron la frases en la clase
7. Pasaron la noche en San Francisco
8. Pasaron una semana en Cuba
9. Carlos y yo visitamos a Roberto
10. El doctor y yo tomamos un taxi
11. Mi madre y yo compramos un coche
12. Los estudiantes prepararon una composición

## Lección Número Dieciocho

Infinitives | We | They, you(pl.)
-- | -- | --
comprender | comprendimos | comprendieron
vender | vendimos | vendieron
ver | vimos | vieron
convencer (*to convince*) | convencimos | convencieron
ofender | ofendimos | ofendieron
extender | extendimos | extendieron
escribir | escribimos | escribieron
aplaudir | aplaudimos | aplaudieron
describir | describimos | describieron
recibir | recibimos | recibieron
asistir | asistimos | asistieron
discutir | discutimos | discutieron
dividir | dividimos | dividieron
sufrir | sufrimos | sufrieron
permitir | permitimos | permitieron
existir | existimos | existieron

### Exercise in Translation

1. ¿Escribieron muchas cartas esta mañana?
2. ¿Vendieron la casa ayer?
3. ¿Comprendieron ustedes la conversación en la clase?
4. ¿Recibieron el cable esta tarde?
5. ¿Leyeron el artículo en el periódico?
6. ¿Compraron una casa en el campo?
7. Mi madre y yo tomamos un taxi
8. Mi madre y yo vimos el accidente
9. Roberto y yo vimos a Carlos
10. Luis y yo leímos el artículo
11. Carlos y María vieron una película
12. Mis clientes escribieron muchas cartas
13. Los estudiantes comprendieron la lección
14. Los Mexicanos aplaudieron con entusiasmo
15. Los estudiantes vieron una película mexicana

## Lección Número Diecinueve

* Words that end in  "ct" often can be changed by adding an "o" ( conflicto, correcto, perfecto, extracto)
* The letter "j" sometimes becomes a "y" in corresponding Spanish words (proyecto, inyección, mayoría, proyectar)
* Nouns cannot be used to modify another noun. Rather, the two nouns are separated by "de" 
    * Programa de radio
    * Música de guitarra
    * Clase de Español
    * Sopa de tomate
    * Jugo de naranja

### Exercise in Translation

1. Canté una canción en la clase
2. Roberto cantó muy mal anoche
3. Cantaron muy bien esta tarde
4. Cantaron muchas canciones por radio
5. Vi un noticiario en el cine
6. Vi una película anoche
7. Vimos una obra ayer
8. Mis amigos vieron un programa de televisión
9. Marta vio las fotografías en el periódico
10. ¿Leyó el periódico esta mañana?
11. Mis amigos leyeron las noticias en el periódico
12. Leí la carta anoche
13. Luis leyó las frases en la clase
14. ¿Oyó usted el discurso ayer?
15. Oímos la conversación en la clase
16. ¿Oyó usted el concierto por radio?

## Lección Número Veinte

* Words that end in "URE" can often be changed to "URA" (agricultura, escultura, caricatura)
* Words that end in "UTE" can often be changed to "UTO" (absoluto, irresoluto, tributo)
* Questions of "where" or "how" always use "está" rather than "es"
    * Está furioso (*he is furious*)
    * Está enojada (*she is angry*)
    * Está en el corredor (*it is in the corridor*)
* "Es" is used with a noun or a characteristic (*permanent quality*)
    * El elefante es un animal (*animal is a noun*)
    * El elefante es grande (*big is a permenent quality of an elephant*)
    * El doctor es inteligente
    * Es rico (*he is rich*)
    * Es alto (*she is tall*)

### Exercise in Translation

1. Alicia está contenta 
2. Juan está listo
3. ¿Está usted bien?
4. ¿Está usted triste?
5. Juan está mejor
6. Eduardo va a estar cansado
7. Voy a estar ocupado
8. Voy a estar ocupada
9. Estoy cansado
10. Estoy cansada
11. Estoy listo
12. Estoy lista
13. Estoy contento
14. Estoy contenta
15. ¿Está usted solo?
16. ¿Está usted sola?
17. ¿Está usted preocupado?
18. ¿Está usted preocupada?
19. Estamos cansados
20. Estamos cansadas
21. Están listos
22. Están listas
23. ¿Están ustedes solos?
24. Estoy en la oficina / en el despacho
25. Alberto y María están en Cuba
26. Están en el club
27. ¿Está usted en casa?
28. Estoy en casa
29. Están en casa
30. Eduardo es en el hospital

### Test Your Progress
#### Test 1

1. las casas
2. las plantas
3. los sombreros
4. las guitarras 
5. los radios
6. los hombres guapos
7. las señoritas bonitas
8. los doctores
9. los animales
10. los colores
11. las naciones 
12. las ciudades
13. las novelas interesantes
14. los artistas famosos

#### Test 2

1. Trabajamos
2. Compramos x Compraron - misread!
3. Canté
4. Hablamos
5. Van a comprar
6. Vamos a invitar
7. ¿Terminaron?
8. Pasé
9. Invitamos
10. Recibí
11. Vimos
12. Oí
13. Leímos
14. Van a ver
15. Cantaron
16. ¿Cómo está Maria?
17. Vieron
18. ¿Quién vio?
19. Escribimos
20. Serví
21. Entendieron x comprendieron (same thing?)
22. Compramos 
23. Trabajaron
24. Entendí x comprendí (same as above)
25. Recibimos
26. ¿Quién escribió?
27. ¿Vendió usted?
28. Vi
29. Leí
30. Entendimos x comprendimos (same as above)
31. Oyeron
32. Voy a vender
33. Oímos
34. María está contenta
35. Vendieron
36. Roberto vio

#### Test 3

1. Natural, naturalmente
2. Personal, personalmente
3. General, generalmente
4. Principal, principalmente
5. Posible, posiblemente  
6. Probable, probablemente
7. Normal, normalmente
8. Final, finalmente
9. Completo, completamente
10. Absoluto, absolutamente
11. Público, públicamente 
12. Comparativo, comparativamente

Tests are going really well!

## Lección Número Veintiuno

* To use the present participle ("ing") you remove the "ar" ending and add "ando"
    * visitando
    * hablando 
    * comprando
    * trabajando
* For "ir" or "er" verbs you do the same thing but the new ending is "iendo"
    * escribiendo
    * recibiendo
    * viendo
    * comprendiendo

### Written Exercises

Infinitive | Present Participle | English
-- | -- | --
hablar | hablando | speaking
comprar | comprando | buying
tomar | tomando | taking
votar | votando | voting
exportar | exportando | exporting
fumar | fumando | smoking
estudiar | estudiando | studying
preparar | preparando | preparing
copiar | copiando | copying
dictar | dictando | dictating
recitar | recitando | reciting
demandar | demandando | demanding
flotar | flotando | floating
marchar | marchando | marching
murmurar | murmurando | murmuring
conservar | conservando | conserving
anticipar | anticipando | anticipating
celebrar | celebrando | celebrating
acumular | acumulando | accumulating
cooperar | cooperando | cooperating
cultivar | cultivando | cultivating
trabajar | trabajando | working
--- | --- | ---
escribir | escribiendo | writing
vivir | viviendo | living
aplaudir | aplaudiendo | applauding
recibir | recibiendo | receiving
decidir | decidiendo | deciding
asistir | asistiendo | assisting
insistir | insistiendo | insisting
describir | describiendo | describing
dividir | dividiendo | dividing 
persuadir | persuadiendo | persuading
sufrir | sufriendo | suffering
ver | viendo | seeing
comprender | comprendiendo | understanding
vender | vendiendo | selling
absorber | absorbiendo | absorbing
convencer | convenciendo | convincing
extender | extendiendo | extending 
mover | moviendo | moving
ofender | ofendiendo | offending
ofrecer | ofreciendo | offering
leer | leyendo | reading
oír | oyendo | listening

### Exercise in Translation

* Estoy comprando
* Está trabajando
* Estamos preparando 
* Están copiando
* Estoy estudiando
* Está progresando
* Estamos fumando 
* Está dictando 
* Está recitando
* Estoy escribiendo 
* Está vendiendo 
* Estamos viendo
* Están comprendiendo
* Estoy leyendo 
* Está viviendo
* Están aplaudiendo

1. Estoy trabajando en la oficina / en el despacho
2. Estoy preparando la cena
3. ¿Está copiando las frases?
4. Estoy estudiando el vocabulario
5. Estoy escribiendo un artículo
6. ¿Está leyendo un novela?
7. Estamos copiando las frases
8. Mi abuelo está dictando una carta
9. Mi abuelo está fumando un puro
10. Están escribiendo las frases
11. Están preparando la cena
12. ¿Está estudiando Roberto?
13. ¿Está leyendo su abuela?
14. ¿Está fumando su abuelo?
15. ¿Está trabajando su padre?

## Lección Número Veintidós
### Written Exercise

Infinitive | 1st Person | Translation
-- | -- | --
tomar | tomo | I take
comprar | compro | I buy
hablar | hablo | I speak
trabajar | trabajo | I work
estudiar | estudio | I study
terminar | termino | I finish
bailar | bailo | I dance
invitar | invito | I invite
visitar | visito | I visit
comprender | comprendo | I understand
vender | vendo | I sell
leer | leo | I read
aprender | aprendo | I learn
escribir | escribo | I write
vivir | vivo | I live
recibir | recibo | I receive
insistir | insisto | I insist
describir | describo | I describe
decidir | decido | I decide
sufrir | sufro | I suffer
--- | --- | ---
comprar | compra | you (he, she, it) buy
hablar | habla | you speak
tomar | toma | you take
trabajar | trabaja | you work
estudiar | estudia | you study
bailar | baila | you dance
votar | vota | you vote
visitar | visita | you visit
invitar | invita | you invite
preparar | prepara | you prepare
importar | importa | you import
exportar | exporta | you export
depositar | deposita | you deposit 
aceptar | acepta | you accept
entrar | entra | you enter
estacionar | estaciona | you park
dictar | dicta | you dictate
pasar | pasa | you pass
usar | usa | you use 
copiar | copia | you copy

### Exercise in Translation

1. Comprendo la situación muy bien
2. Trabajo con Roberto en la tienda
3. Estudio la lección después de la cena
4. Leo el periódico en la mañana
5. Tomo huevos para la desayuno
6. Tomo té para el almuerzo
7. Tomo café para la cena
8. Tomo jugo de naranja para el desayuno
9. Tomo huevos revueltos para la desayuno
10. Roberto habla español muy bien
11. Luis estudia mucho
12. Alberto trabaja en el banco
13. Mi abuelo fuma después de la cena
14. ¿Habla español?
15. ¿Estudia después de la cena?
16. ¿Trabaja en el banco?

## Lección Número Veintitrés
### Written Exercise

1 | 2 | 3 | 4 | 5
-- | -- | -- | -- | --
escribir | escribo | I write | escribe | you write
recibir | recibo | I receive | recibe | you receive
vivir | vivo | I live | vive | you live
comprender | comprendo | I understand | comprende | you understand
vender | vendo | I sell | vende | you sell
aprender | aprendo | I learn | aprende | you learn
leer | leo | I read | lee | you read
describir | describo | I describe | describe | you describe
decidir | decido | I decide | decide | you decide
sufrir | sufro | I suffer | sufre | you suffer
dividir | divido | I divide | divide | you divide
permitir | permito | I permit | permite | you permit
persuadir | persuado | I persuade | persuade | you persuade
resistir | resisto | I resist | resiste | you resist
comer | como | I eat | come | you eat
beber | bebo | I drink | bebe | you drink

### Exercise in Translation

1. Vivo en un departamento en Nueva York
2. Leo el periódico todas las mañanas
3. Comprendo la lección muy bien
4. Aprendo mucho en la clase
5. Leo lección en la clase
6. ¿Lee usted muchos todos los días?
7. ¿Vive usted en una casa o en el departamento?
8. ¿Escribe usted las frases en la clase?
9. ¿Aprende el vocabulario en la clase?
10. ¿Lee una revista en la clase?
11. Carlos vende muchos autos
12. María escribe artículos interesantes
13. Luis comprende la conversación
14. Roberto come mucho

## Lección Número Veinticuatro
### Written Exercise

"AR" Verbs | 1st Person Plural | Translation
-- | -- | --
comprar | compramos | we buy
hablar | hablamos | we speak
tomar | tomamos | we take
estudiar | estudiamos | we study
trabajar | trabajamos | we work
cantar | cantamos | we sing
terminar | terminamos | we finish
aceptar | aceptamos | we accept
votar | votamos | we vote
estacionar | estacionamos | we park
estar | estamos | we are
invitar | invitamos | we invite 
comprender | comprendemos | we understand
vender | vendemos | we sell
leer | leemos | we read
aprender | aprendemos | we learn
depender | dependemos | we depend
ofrecer | ofrecemos | we offer
vivir | vivimos | we live
recibir | recibimos | we receive 
oír | oímos | we hear
discutir | discutimos | we discuss
describir | describimos | we describe

3rd Man Singular | | | 3rd Man Singular
-- | -- | -- | --
habla | you speak | hablan | they speak
compra | you buy | compran | they buy
termina | you finish | terminan | they finish
trabaja | you work | trabajan | they work
visita | you visit | visitan | they visit
invita | you invite | invitan | they invite
canta | you sing | cantan | they sing
baila | you dance | bailan | they dance 
comprende | you understand | comprenden | they understand
vende | you sell | venden | they sell
lee | you read | leen | they read
aprende | you learn | aprenden | they learn
escribe | you write | escriben | they write
vive | you live | viven | they live
decide | you decide | deciden | they decide
recibe | you receive | reciben | they receive 
describe | you describe | describen | they describe

### Exercise in Translation

1. Visitamos a mi primo con frecuencia
2. Comprendemos la lección muy bien
3. Escribimos muchas cartas en el despacho
4. Vivimos en una casa en el campo
5. Tomamos café en el despacho
6. Tomamos el almuerzo a la una
7. Tomamos el desayuno a las ocho en punto
8. Tomamos la cena a las siete en punto
9. Los estudiantes hablan muy bien
10. las mujeres llevan canastas
11. Mis primos viven en Costa Rica
12. Mis amigos trabajan en el banco
13. Las secretarias escriben muchas cartas
14. Carlos y Roberto comprenden la lección
15. María y Alica preparamos la cena

### Written Exercise

I | You | We | They 
-- | -- | -- | --
hablar | habla | hablamos | hablan
comprar | compra | compramos | compran
trabajar | trabaja | trabajamos | trabajan
bailar | baila | bailamos | bailan
visitar | visita  | visitamos | visitan
llevar | lleva | llevamos | llevan
invitar | invita | invitamos | invitan
tomar | toma | tomamos | toman
vender | vende | vendemos | venden
comprender | comprende | comprendemos | comprenden
aprender | aprende | aprendemos | aprenden
vivir | vive | vivimos | viven
recibir | recibe | recibimos | reciben
escribir | escribe | escribimos | escriben
decidir | decide | decidimos | deciden

## Lección Número Veinticinco
### Written Exercise

Infinitives | Nouns
-- | --
cantar | el canto, the song 
usar | el uso, the use
robar | el robo, the robbery
besar | el beso, the kiss
insultar | el insulto, the insult
pesar | el peso, the weight
odiar | el odio, the hatred
refrescar | el refresco, the refresh
trabajar | el trabajo, the work
cepillar | el cepillo, the brush
dibujar | el dibujo, the drawing
fracasar | el fracaso, the failure
gritar | el grito, the scream
saludar | el saludo, the greeting
cambiar | el cambio, the change
caminar | el camino, the road

### Exercise in Translation

1. Tuve catarro la semana pasada
2. Tuve una fiesta anoche
3. Tuve una cita esta tarde
4. Tuve visitas el sábado
5. ¿Tuvo una fiesta anoche?
6. ¿Tuvo una conversación interesante?
7. ¿Tuvo visitas anoche?
8. Estuve en Cuba el verano pasado
9. Estuve ocupado esta tarde
10. Estuve en el despacho esta tarde
11. Estuve en casa anoche
12. Estuve enfermo en el hospital
13. ¿Estuvo en casa esta mañana?
14. ¿Estuvo ocupado en el despacho?
15. ¿Estuvo en Francia en la primavera?
16. Tuve que estudiar mucho
17. Tuve que trabajar ayer
18. Tuve que caminar al despacho
19. Tuve que leer el libro
20. ¿Por qué no vende la casa?
21. ¿Por qué no compra un auto?
22. ¿Por qué no va al cine?
23. ¿Por qué no esta contento?
24. ¿Por qué no lee el periódico?
25. Por qué no toma un avión?

## Lección Número Vientiséis
### Exercise in Translation

1. Estuve en casa anoche
2. Estuve en el parque toda la tarde
3. ¿estuvo en México el verano pesado?
4. Tuve catarro la semana pasada
5. Tuve una fiesta linda
6. Tuve visitas el sábado
7. ¿Tuvo una fiesta el sábado?
8. Vine a Nueva York en tren
9. ¿Vino Carlos a la fiesta anoche?
10. ¿Vine María a la playa la semana pasada?
11. ¿Tuvo visitas el sábado?
12. ¿Vino usted a México en avión?
13. Hice muchas cosas interesantes
14. ¿Vino Carlos al despacho esta tarde?
15. ¿Hizo usted el trabajo esta mañana?
16. Carlos hizo el trabajo en el despacho 
17. Hice muchas cosas interesantes en Cuba
18. Carlos hizo muchas cosas interesantes
19. Voy a trabajar mañana
20. Vamos a vender el auto
21. Van a comprar una casa
22. ¿Va a pescar mañana?
23. ¿Va a nadar mañana?
24. Fuimos a trabajar esta mañana
25. Fueron a caminar en el parque
26. Fueron a pescar con mi primo
27. Voy al cine esta noche
28. ¿Va a la clase mañana?
29. Vamos al campo el sábado
30. Van al teatro con frecuencia
31. Voy a una fiesta esta noche
32. Van al campo el sábado
33. ¿Va al cine esta noche?
34. Voy al cine con frecuencia
35. Vamos a la ópera todas las semanas
36. Voy al campo todas las semanas
37. Fueron a la estación con Marta
38. Fuimos al parque esta tarde
39. ¿Fue al campo el sábado?
40. ¿Fue a la clase esta tarde?
41. Fui al cine con Carlos
42. Fui a la fiesta el sábado

## Lección Número Veintisiete
### Exercise in Translation

1. He estudiado la lección
2. He terminado la composición
3. He viajado mucho
4. He trabajado en el despacho
5. He contestado el teléfono
6. He escrito la carta
7. He visto a Roberto
8. He mandado el cable
9. He pescado en Cuba
10. He vivido en Guatemala
11. He vendido la casa
12. ¿Ha depositado el dinero?
13. ¿Ha estudiado español?
14. ¿Ha mandado el cable?
15. ¿Ha trabajado hoy?
16. ¿Ha visto a Roberto últimamente?
17. ¿Ha leído el libro?
18. Hemos vendido la casa
19. Hemos viajado mucho 
20. Hemos tenido la cena
21. Hemos terminado el trabajo
22. Han estudiado español
23. Han estado ocupados
24. Han estado en Venezuela 

## Lección Número Veintiocho
### Exercise in Translation

1. Jugué tenis esta tarde
2. Voy a jugar al bridge esta noche
3. Roberto está jugando tenis en el club
4. ¿Ha jugando tenis con Marta?
5. Llegué al teatro temprano
6. Voy a llegar al despacho a tiempo esta semana
7. El tren llegó a Nueva York a tiempo
8. Luis llegó a Cuba a las ocho esta noche
9. ¿Llegó al teatro a tiempo?
10. Luis tocó el violín en la orquesta
11. ¿Tocó el acordeón a la fiesta?
12. ¿Quién tocó la guitarra a la fiesta?
13. Estoy tocando el timbre
14. Pagué la cuenta en el restaurante 
15. Luis pagó la renta este mes
16. Papá paga las cuentas
17. ¿Cuánto pagó por el vestido?
18. Pagué cinco dólares por la pipa
19. Pagué seis dólares por el perfume
20. Luis pagó las cuentas la semana pasada
21. Pagaron cuatro dólares por los discos
22. Marta pagó dos dólares por el libro
23. Papá pagó veintidós centavos por la revista
24. Pagamos treinta centavos por el pan
25. Alberto pagó cincuenta centavos por la canasta 

## Lección Número Veintiueve
### Conjugating verbs

Verb | Present | Past (*preterite*) | Future | Present Perfect | Present Progressive
-- | -- | -- | -- | -- | --
Ganar | gano, gana, ganamos, ganan | gané, ganó, ganamos, ganaron | voy a ganar, va a ganar, vamos a ganar, van a ganar | he ganado, ha ganado, hemos ganado, han ganado | estoy ganando, está ganando, estamos ganando, están ganando
llorar | lloro, llora, lloramos, lloran | lloré, lloró, lloremos, lloraron | voy a llorar, va a llorar, vamos a llorar, van a llora | he llorado, ha llorado, hemos llorado, han llorado | estoy llorando, está llorando, estamos llorando, están llorando
limpiar | limpio, limpia, limpiamos, limpian | limpié, limpió, limpiamos, limpiaron | voy a limpiar, va a limpiar, vamos a limpiar, van a limpiar | he limpiado, ha limpiado, hemos limpiado, han limpiado | estoy limpiando, está limpiando, estamos limpiando, están limpiando
gritar | grito, grita, gritamos, gritan | grité, gritó, gritamos, gritaron | voy a gritar, va a gritar, vamos a gritar, van a gritar | he gritado, ha gritado, hemos gritado, han gritado | estoy gritando, está gritando, estamos gritando, están gritando
mandar | mando, manda, mandamos, mandan | mandé, mandó, mandamos, manaron | voy a gritar, va a gritar, vamos a gritar, van a gritar | he gritado, ha gritado, hemos gritado, han gritado | estoy gritando, está gritando, estamos gritando, están gritando  
tomar | tomo, toma, tomamos, toman | tomé, tomó, tomamos, tomaron | voy a tomar, va a tomar, vamos a tomar, van a tomar | he tomado, ha tomado, hemos tomado, han tomado | estoy tomando, está tomando, estamos tomando, están tomando
manejar | manejo, maneja, manejamos, manejan | manejé, manejó, manejamos, manejaron | voy a manejar, va a manejar, vamos a manejar, van a manejar | he manado, ha manejado, hemos manejado, han manejado | estoy manejando, está manejando, estamos manejando, están manejando
cantar | canto, canta, cantamos, cantan | canté, cantó, cantamos, cantaron | voy a cantar, va a cantar, vamos a cantar, van a cantar | he cantado, ha cantado, hemos cantado, han cantado | estoy cantando, está cantando, estamos cantando, están cantando
pintar | pinto, pinta, pintamos, pintan | pinté, pintó, pintamos, pintaron | voy a pintar, va a pintar, vamos a pintar, van a pintar | he pintado, ha pintado, hemos pintado, han pintado | estoy pitando, está pintando, estamos pintando, están pintando
cocinar | cocino, cocina, cocinamos, cocinan | cociné, cocinó, cocinamos, cocinaron | voy a cocinar, va a cocinar, vamos a cocinar, van a cocinar | he cocinado, ha cocinado, hemos cocinado, han cocinado | estoy cocinando, está cocinando, estamos cocinando, están cocinando 
planchar (*to iron*) | plancho, plancha, planchamos, planchan | planché, planchó, planchamos, plancharon | voy a planchar, va a planchar, vamos a planchar, van a planchar | he planchado, ha planchado, hemos planchado, han planchado | estoy planchando, está planchando, estamos planchando, están planchando
terminar | termino, termina, terminamos, terminan | terminé, terminó, terminamos, terminaron | voy a terminar, va a terminar, vamos a terminar, van a terminar | he terminado, ha terminado, hemos terminado, han terminado | estoy terminando, está terminando, estamos terminando, están terminando
lavar | lavo, lava, lavamos, lavan | lavé, lavó, lavamos, lavaron | voy a lavar, va a lavar, vamos a lavar, van a lavar | he lavado, ha lavado, hemos lavado, han lavado | estoy lavando, está lavando, estamos lavando, están lavando
contestar | contesto, contesta, contestamos, contestan | contesté, contestó, contestamos, contestaron | voy a contestar, va a contestar, vamos a contestar, van a contestar | he contestado, ha contestado, hemos contestado, han contestado | estoy contestando, está contestando, estamos contestando, están contestando
saltar | salto, salta, saltamos, saltan | salté, saltó, saltamos, saltaron | voy a saltar, va a saltar, vamos a saltar, van a saltar | he saltado, ha saltado, hemos saltado, han saltado | estoy saltando, está saltando, estamos saltando, están saltando
arreglar (*to arrange*) | arreglo, arregla, arreglamos, arreglan | arreglé, arregló, arreglamos, arreglaron | voy a arreglar, va a arreglar, vamos a arreglar, van a arreglar | he arreglado, ha arreglado, hemos arreglado, han arreglado | estoy arreglando, está arreglando, estamos arreglando, están arreglando 
cuidar | cuido, cuida, cuidamos, cuidan | cuidé, cuidó, cuidamos, cuidaron | voy a cuidar, va a cuidar, vamos a cuidar, van a cuidar | he cuidado, ha cuidado, hemos cuidado, han cuidado | estoy cuidando, está cuidando, estamos cuidando, están cuidando 
nadar | nado, nada, nadamos, nadan | nadé, nadó, nadamos, nadaron | voy a nadar, va a nadar, vamos a nadar, van a nadar | he nadado, ha nadado, hemos nadado, han nadado | estoy nadando, está nadando, estamos nadando, están nadando 
ayudar | ayudo, ayuda, ayudamos, ayudan | ayudé, ayudó, ayudamos, ayudaron | voy a ayudar, va a ayudar, vamos a ayudar, van a ayudar | he ayudado, ha ayudado, hemos ayudado, han ayudado | estoy ayudando, está ayudando, estamos ayudando, están ayudando
trabajar | trabajo, trabaja, trabajamos, trabajan | trabajé, trabajó, trabajamos, trabajaron | voy a trabajar, va a trabajar, vamos a trabajar, van a trabajar | he trabajado, ha trabajado, hemos trabajado, han trabajado | estoy trabajando, está trabajando, estamos trabajando, están trabajando
alquilar (*to rent*) | alquilo, alquila, alquilamos, alquilan | alquilé, alquiló, alquilamos, alquilaron | voy a alquilar, va a alquilar, vamos a alquilar, van a alquilar | he alquilado, ha alquilado, hemos alquilado, han alquilado | estoy alquilando, está alquilando, estamos alquilando, están alquilando
hablar | hablo, habla, hablamos, hablan | hablé, habló, hablamos, hablaron | voy a hablar, va a hablar, vamos a hablar, van a hablar | he hablado, ha hablado, hemos hablado, han hablado | estoy hablando, está hablando, estamos hablando, están hablando
dudar | dudo, duda, dudamos, dudan | dudé, dudó, dudamos, dudaron | voy a dudar, va a dudar, vamos a dudar, van a dudar | he dudado, ha dudado, hemos dudado, han dudado | estoy dudando, está dudando, estamos dudando, están dudando
comprar | compro, compra, compramos, compran | compré, compró, compramos, compraron | voy a comprar, va a comprar, vamos a comprar, van a comprar | he comprado, ha comprado, hemos comprado, han comprado | estoy comprando, está comprando, estamos comprando, están comprando
cambiar | cambio, cambia, cambiamos, cambian | cambié, cambió, cambiamos, cambiaron | voy a cambiar, va a cambiar, vamos a cambiar, van a cambiar | he cambiado, ha cambiado, hemos cambiado, han cambiado | estoy cambiando, está cambiando, estamos cambiando, están cambiando
pasar | paso, pasa, pasamos, pasan | pasé, pasó, pasamos, pasaron | voy a pasar, va a pasar, vamos a pasar, van a pasar | he pasado, ha pasado, hemos pasado, han pasado | estoy pasando, está pasando, estamos pasando, están pasando
regresar | regreso, regresa, regresamos, regresan | regresé, regresó, regresamos, regresaron | voy a regresar, va a regresar, vamos a regresar, van a regresar | he regresado, ha regresado, hemos regresado, han regresado | estoy regresando, está regresando, estamos regresando, están regresando
dejar | dejo, deja, dejamos, dejan | dejé, dejó, dejamos, dejaron | voy a dejar, va a dejar, vamos a dejar, van a dejar | he dejado, ha dejado, hemos dejado, han dejado | estoy dejando, está dejando, estamos dejando, están dejando
llevar | llevo, lleva, llevamos, llevan | llevé, llevó, llevamos, llevaron | voy a llevar, va a llevar, vamos a llevar, van a llevar | he llevado, ha llevado, hemos llevado, han llevado | estoy llevando, está llevando, estamos llevando, están llevando
bajar | bajo, baja, bajamos, bajan | bajé, bajó, bajamos, bajaron | voy a bajar, va a bajar, vamos a bajar, van a bajar | he bajado, ha bajado, hemos bajado, han bajado | estoy bajando, está bajando, estamos bajando, están bajando
--- | --- | --- | --- | --- | ---
vender | vendo, vende, vendemos, venden | vendí, vendió, vendimos, vendieron | voy a vender, va a vender, vamos a vender, van a vender | he vendido, ha vendido, hemos vendido, han vendido | estoy vendiendo, está vendiendo, estamos vendiendo, están vendiendo
comprender | comprendo, comprende, comprendemos, comprenden | comprendí, comprendió, comprendimos, comprendieron | voy a comprender, va a comprender, vamos a comprender, van a comprender | he comprendido, ha comprendido, hemos comprendido, han comprendido | estoy comprendiendo, está comprendiendo, estamos comprendiendo, están comprendiendo
correr | corro, corre, corremos, corren | corrí, corrió, corrimos, corrieron | voy a correr, va a correr, vamos a correr, van a correr | he corrido, ha corrido, hemos corrido, han corrido | estoy corriendo, está corriendo, estamos corriendo, están corriendo
sorprender | sorprendo, sorprende, sorprendemos, sorprenden | sorprendí, sorprendió, sorprendimos, sorprendieron | voy a sorprender, va a sorprender, vamos a sorprender, van a sorprender | he sorprendido, ha sorprendido, hemos sorprendido, han sorprendido | estoy sorprendiendo, está sorprendiendo, estamos sorprendiendo, están sorprendiendo
comer | como, come, comemos, comen | comí, comió, comimos, comieron | voy a comer, va a comer, vamos a comer, van a comer | he comido, ha comido, hemos comido, han comido | estoy comiendo, está comiendo, estamos comiendo, están comiendo
prometer | prometo, promete, prometemos, prometen | prometí, prometió, prometimos, prometieron | voy a prometer, va a prometer, vamos a prometer, van a prometer | he prometido, ha prometido, hemos prometido, han prometido | estoy prometiendo, está prometiendo, estamos prometiendo, están prometiendo
beber | bebo, bebe, bebemos, beben | bebí, bebió, bebimos, bebieron | voy a beber, va a beber, vamos a beber, van a beber | he bebido, ha bebido, hemos bebido, han bebido | estoy bebiendo, está bebiendo, estamos bebiendo, están bebiendo
barrer (*to sweep*) | barro, barre, barremos, barren | barrí, barrió, barrimos, barrieron | voy a barrer, va a barrer, vamos a barrer, van a barrer | he barrido, ha barrido, hemos barrido, han barrido | estoy barriendo, está barriendo, estamos barriendo, están barriendo
coser (*to sew*) | coso, cose, cosemos, cosen | cosí, cosió, cosimos, cosieron | voy a coser, va a coser, vamos a coser, van a coser | he cosido, ha cosido, hemos cosido, han cosido | estoy cosiendo, está cosiendo, estamos cosiendo, están cosiendo
--- | --- | --- | --- | --- | ---
vivir | vivo, viva, vivimos, viven | viví, vivió, vivimos, vivieron | voy a vivir, va a vivir, vamos a vivir, van a vivir | he vivido, ha vivido, hemos vivido, han vivido | estoy viviendo, está viviendo, estamos viviendo, están viviendo
recibir | recibo, recibe, recibimos, reciben | recibí, recibió, recibimos, recibieron | voy a recibir, va a recibir, vamos a recibir, van a recibir | he recibido, ha recibido, hemos recibido, han recibido | estoy recibiendo, está recibiendo, estamos recibiendo, están recibiendo
decidir | decido, decide, decidimos, deciden | decidí, decidió, decidimos, decidieron | voy a decidir, va a decidir, vamos a decidir, van a decidir | he decido, ha decido, hemos decido, han decido | estoy decidiendo, está decidiendo, estamos decidiendo, están decidiendo
persuadir | persuado, persuade, persuadimos, persuaden | persuadí, persuadió, persuadimos, persuadieron | voy a persuader, va a persuadir, vamos a persuadir, van a persuadir | he persuadido, ha persuadido, hemos persuadido, han persuadido | estoy persuadiendo, está persuadiendo, estamos persuadiendo, están persuadiendo
dividir | divido, divide, dividimos, dividen | dividí, dividió, dividimos, dividieron | voy a dividir, va a dividir, vamos a dividir, van a dividir | he dividido, ha dividido, hemos dividido, han dividido | estoy dividiendo, está dividiendo, estamos dividiendo, están dividiendo  
asistir | asisto, asiste, asistimos, asisten | asistí, asistió, asistimos, asistieron | voy a asistir, va a asistir, vamos a asistir, van a asistir | he asistido, ha asistido, hemos asistido, han asistido | estoy asistiendo, está asistiendo, estamos asistiendo, están asistiendo
subir (*to go up*) | subo, sube, subimos, suben | subí, subió, subimos, subieron | voy a subir, va a subir, vamos a subir, van a subir | he subido, ha subido, hemos subido, han subido | estoy subiendo, está subiendo, estamos subiendo, están subiendo
discutir | discuto, discute, discutimos, discuten | discutí, discutió, discutimos, discutieron | voy a discutir, va a discutir, vamos a discutir, van a discutir | he discutido, ha discutido, hemos discutido, han discutido | estoy discutiendo, está discutiendo, estamos discutiendo, están discutiendo
resistir | resisto, resiste, resistimos, resisten | resistí, resistió, resistimos, resistieron | voy a resistir, va a resistir, vamos a resistir, van a resistir | he resistido, ha resistido, hemos resistido, han resistido | estoy resistiendo, está resistiendo, estamos resistiendo, están resistiendo
interrumpir (*to interrupt*) | interrumpo, interrumpe, interrumpimos, interrumpen | interrumpí, interrumpió, interrumpimos, interrumpieron | voy a interrumpir, va a interrumpir, vamos a interrumpir, van a interrumpir | he interrumpido, ha interrumpido, hemos interrumpido, han interrumpido | estoy interrumpiendo, está interrumpiendo, estamos interrumpiendo, están interrumpiendo  

### Exercise in Translation

1. Estoy estudiando español
2. ¿Habla usted español?
3. ¿Dónde vive usted?
4. ¿Dónde vive Carlos?
5. Trabaja en una tienda
6. ¿Escribió usted la carta?
7. Viven en un departamento en Avenida Madison
8. Leí el periódico esta mañana
9. ¿Ha leído "Don Quixote"?
10. Marta habla español muy bien
11. He estudiando la lección
12. Tomé huevos para el desayuno esta mañana
13. Va a Cuba mañana
14. ¿Va en barco o en avión?
15. ¿Quién compró los boletos?
16. ¿Comprende usted?
17. Hemos estado muy ocupados
18. ¿Ha visto a Enrique?
19. ¿Ha escrito la carta?
20. ¿Tomó una taza de café para el desayuno?
21. ¿Ha depositado mucho dinero en el banco esta año?
22. ¿Cómo está?
23. ¿Cómo está su madre?
24. ¿Dónde está su hermana?
25. ¿Quién va a comprar los boletos?
26. Hemos terminado el trabajo
27. ¿Ha recibido el cable?
28. Vi una película interesante anoche
29. He vivido en esta casa durante dos años
30. ¿Qué decidió?
31. Está hablando por teléfono
32. Estoy aprendiendo a manejar un auto
33. Estamos aprendido a hablar español
34. He aprendido muchas palabras en la clase
35. Mi abuelo siempre fuma una pipa después de la cena
36. ¿Cuándo vendió la casa?
37. Voy a tomar una aspirina
38. Enrique pasó una semana en Cuba
39. Hemos viajando mucho este año
40. ¿Ha contestando la carta?

## Lección Número Treinta

* When conjugating some verbs, the root or stem of the verb changes but not the ending. When the stress for the 'present' tense verb falls on an 'e' it will change to 'ie':
    * Pensar (*to think*)
    * Pienso (*I think*)
    * Piensa (*you think*)
    * Piensan (*they think*)
    * Pensamos (*we think*)
* Because the stress doesn't fall on the 'e' in *we think* the word isn't modified. 
* This same thing occurs with the letter "o" but changes to "ue":
    * Encontrar (*to find*)
    * Encuentro (*I find*)
    * Encuentra (*you find*)
    * Encuentran (*they find*)
    * Encontramos (*we find*)
* These changes only occur in the present tense as other tenses put the stress on the last or penultimate vowel in the verb

### Exercise in Translation

1. ¿Juega tenis?
2. Juego bridge
3. ¿Juegan béisbol?
4. Jugamos bridge
5. Jugué golf
6. ¿Jugó tenis?
7. Jugué tenis esta mañana
8. Jugamos bridge hoy
9. Jugaron béisbol hoy
10. Jugué tenis con Roberto
11. ¿Entiende la lección?
12. ¿Entiende español?
13. Entiendo italiano
14. Entendemos el vocabulario
15. ¿Entiende?
16. Entiendo la lección
17. ¿Quién abrió la ventana?
18. Carlos abrió la ventana
19. Abrí las ventanas
20. ¿Quién cerró la puerta?
21. Cierro la puerta
22. Cerré las ventanas
23. Carlos cierra las ventanas
24. ¿Recuerda el número de teléfono
25. Recuerdo la dirección
26. ¿No recuerda?
27. ¿Recuerda?
28. ¿Recuerda el poema?
29. No recuerdo el número de teléfono
30. ¿Recuerda la dirección?

### Test Your Progress
#### Test 1

1. Más raro
2. El más raro
3. Más bonito
4. El más bonito
5. Más grande
6. El más grande
7. Más alto
8. El más alto
9. Más gordo
10. El más gordo
11. Más delgado
12. El más delgado
13. Más feo
14. El más feo
15. Mejor
16. El mejor
17. Más peor
18. El peor
19. Más bonita
20. La más bonita

#### Test 2

1. Trabajo
2. Compraron
3. Tomamos
4. ¿Habla (*usted*)?
5. Invito
6. Eduardo canta
7. Visitamos
8. He terminado
9. Voy a pasar
10. Estoy cantando
11. He recibido 
12. María está escribiendo
13. He entendido
14. Hemos vendido
15. ¿Ha leído?
16. Oí 
17. Vi
18. ¿Dónde está?
19. Ha invitado
20. Están leyendo
21. Fui
22. Había comprar x Había comprando
23. Estaba escribiendo
24. ¿Fue?
25. Tuve una accidente
26. Preparo
27. ¿Ha comprar? x ¿Han comprando?
28. Han invitado
29. Fuma
30. Está nadando
31. Compré x Pagué
32. Vive
33. Visten x Llevan 
34. ¿Quién compró? x ¿Quién pagó?
35. Espero
36. Espero ver
37. Esperamos comprar
38. Cierro
39. Cuento
40. Encontramos
41. Juego
42. Va a comprar
43. Están trabajando
44. Estamos estudiando 
45. Están estudiando 
46. Vino
47. Hicieron
48. Fuimos
49. Tuve que trabajar
50. Tuvo que ver

## Lección Número Treinta y Uno
### Noncomformist Verbs
#### Group 1

Present | 1st Person | Singular | Ends in | "go"
-- | -- | -- | -- | --
hacer | hago | hace | hacemos | hacen
poner | pongo | pone | ponemos | ponen
traer | traigo | trae | traemos | traen
caer | caigo | cae | caemos | caen
salir | salgo | sale | salimos | salen
tener | tengo | tiene | tenemos | tienen
venir | vengo | viene | vinimos | vienen
oír | oigo | oye | oímos | oyen
decir | digo | dice | decimos | dicen
-- | -- | -- | -- | --
Past | Preterite | Endings | | 
-- | -- | -- | -- | --
hacer | hice | hizo | hicimos | hicieron
poner | puse | puso | pusimos | pusieron
traer | traje | trajo | trajimos | trajeron
caer | caí | cayó | caímos | cayeron
salir | salí | salió | salimos | salieron
tener | tuve | tuvo | tuvimos | tuvieron
venir | vine | vino | vinimos | vinieron
oír | oí | oye | oímos | oyeron
decir | dice | dijo | dijimos | dijeron

#### Group 2

Present | 1st Person | Singular | Ends in | "oy"
-- | -- | -- | -- | --
ir | voy | va | vamos | van
estar | estoy | está | estamos | están
dar | doy | da | damos | dan
ser | soy | es | somos | son
-- | -- | -- | -- | --
Past | Preterite | Endings | |
-- | -- | -- | -- | --
ir | fui | fue | fuimos | fueron
estar | estuve | estuvo | estuvimos | estuvieron
dar | dí | dio | dimos | dieron
ser | fui | fue | fuimos | fueron

#### Group 3

Radical | Changing | in | Present | --
-- | -- | -- | -- | --
poder | puedo | puede | podemos | pueden
querer | quiero | quiere | queremos | quieren
-- | -- | -- | -- | --
Past | Preterite | Endings | |
-- | -- | -- | -- | --
poder | pude | pudo | pudimos | pudieron
querer | quería | quería | queríamos | querían

##### Group 4

Different | from | Other | Groups | --
-- | -- | -- | -- | --
saber | sé | sabe | sabemos | saben
ver | veo | ve | vemos | ven
-- | -- | -- | -- | --
Past | Preterite | Endings | |
-- | -- | -- | -- | --
saber | sabía | sabía | sabíamos | sabían
ver | vi | vio | vimos | vieron

Regular | completely | in | present | --
-- | -- | -- | -- | --
andar | ando | anda | andamos | andan
-- | -- | -- | -- | --
Past | Preterite | Endings | |
-- | -- | -- | -- | --
andar | anduve | anduvo | anduvimos | anduvieron

### Exercise in Translation

1. Tengo un perro
2. ¿Tiene un caballo?
3. Tenemos una casa en el campo
4. Tienen un gato
5. Tuve un accidente ayer
6. Tengo que ir al despacho
7. Tengo que trabajar el sábado
8. ¿Tiene que lavar la ropa?
9. Tenemos que comprar una casa
10. Tuve que escribir una carta
11. Tienen que pintar la casa
12. ¿Tuvo que vender el auto?
13. Tuvimos cuidar a los niños
14. Mi tío tiene que pagar la cuenta
15. Mi tío tiene que trabajar el sábado

1. ¿Cuándo viene a México?
2. ¿Cuándo viene a la clase?
3. ¿Por qué no viene a la fiesta?
4. ¿Por qué no viene a la playa?
5. José vino a la fiesta
6. Mi tío va a venir a la fiesta 
7. Quiero ir al cine
8. ¿Quiere terminar el trabajo?
9. Queríamos ver la casa
10. Querían comprar el auto
11. ¿Quiere ver la casa?
12. Quieren jugar al tenis
13. ¿Puede ir al despacho?
14. José puede nadar el domingo
15. Podemos terminal el trabajo
16. No puedo terminar el trabajo
17. No pudo vender la casa
18. Pueden nadar el domingo
19. He podido escribir toda la tarde

1. Hago la cama todas las mañanas
2. María hace dulces deliciosos
3. Hacemos dulces para los muchachos 
4. Hacen emparedados para las muchachas 
5. Hice una limonada esta tarde
6. ¿Hizo café para mi tío?
7. Hicieron las blusas para las muchachas
8. Hago lo que puedo en casa
9. Carlos hace el trabajo muy bien
10. Hacemos muchas cosas interesantes
11. Hacen el trabajo muy bien
12. Isabel hizo una cosa interesante
13. Isabel hizo lo que pudo ayer
14. Digo que es imposible
15. Helena dice que es terrible
16. Dicen muchas cosas indiscretas
17. Dicen que es muy interesante
18. Dije que era inteligente
19. Eduardo dijo que era muy bueno
20. Dijimos que no era my bueno
21. Salgo el despacho a las cinco en punto
22. Salimos de la casa tarde
23. Salí del teatro a las once un punto
24. Salieron a las ocho en punto anoche
25. Salí del hotel a las cinco

## Lección Número Treinta y Dos

Verb | I Present | You Present | We Present | They Present | I Past | You Past | We Past | They Passed
-- | -- | -- | -- | -- | -- | -- | -- | --
Besar | Beso | Besa | Besamos | Besan | Besé | Besó | Besamos | Besaron
Cuidar | Cuido | Cuida | Cuidamos | Cuidan | Cuidé | Cuidó | Cuidamos | Cuidaron
Odiar | Odio | Odia | Odiamos | Odian | Odié | Odió | Odiamos | Odiaron
Sorprender | Sorprendo | Sorprende | Sorprendemos | Sorprenden | Sorprendí | Sorprendió | Sorprendimos | Sorprendieron 
Interrumpir | Interrumpo | Interrumpe | Interrumpimos | Interrumpen | Interrumpí | Interrumpió | Interrumpimos | Interrumpieron
Criticar | Critico | Critica | Criticamos | Critican | Critiqué | Criticó | Criticamos | Criticaron 
Castigar | Castigo | Castiga | Castigamos | Castigan | Castigué | Castigó | Castigamos | Castigaron 

### Exercise in Translation

1. Carlos la vio en el despacho
2. Roberto la vio esta mañana
3. Juan la invitó al cine
4. Papá los vio en el parque
5. Luis nos invitó al cine
6. María nos visitó la semana pasada
7. Mi abuelo los vio esta tarde
8. Marta lo abrazó cuando entró
9. Juan me invitó a la fiesta
10. Mi abuelo lo saludó cuando entró
11. Juan lo insultó esta tarde
12. Eduardo la besó
13. Roberto la sorprendió 
14. Mi tío lo castigó
15. Mi tía los cuidó
16. Helena lo odió 
17. Isabel las vio
18. Mi tío los vio
19. Mi tío me sorprendió
20. Mi tía me besó
21. Mi tío nos vio
22. Lo vi la semana pasada
23. La vi esta tarde
24. Los invité a la fiesta
25. Lo besé cuando entro
26. Lo cuidé toda la tarde
27. Los invité al cine
28. Los visité el otro día
29. Los vi el sábado
30. Lo vi ayer

## Lección Número Treinta y Tres
### Written Exercise

Inglés | Español
-- | --
He saw her | La vio
She saw him | Lo vio
You saw him | Lo vio
You saw her | La vio
He saw you | Lo vio
He saw you | La vio
She invited him | Lo invitó
You invited him | Lo invitó
He invited her | La invitó
She invited her | La invitó
She invited you (*masc*) | Lo invitó
He invited you (*fem*) | La invitó
He visited her | La visitó
She visited him | Lo visitó
You visited her | La visitó
You visited him | Lo vistió
He visited you (*masc*) | Lo visitó
He visited you (*fem*) | La visitó

### Exercise in Translation

1. Mi primo me llevó al cine
2. Mi hermano me llevó a la playa
3. Mi tío los llevó al circo
4. Mi tía las llevó a la fiesta
5. Mi hermana nos llevó al mercado
6. El doctor nos llevó a la ópera
7. Elena nos invitó al concierto
8. Dorotea lo invitó a la fiesta
9. Mi tío la llevó al despacho
10. Mi tío me llevó al campo
11. Lo he visto dos veces 
12. Los he visto muchas veces
13. ¿Lo ha visto últimamente?
14. Voy a verlo el sábado
15. Elena va a verlo mañana
16. Vamos a verlo esta noche
17. Los ha visto tres veces
18. Vamos a verlo el sábado
19. Lo estoy castigando porque es travieso
20. ¿Lo ha vista últimamente?

### Written Exercise

1. Lo leí ayer
2. La leí la semana pasada
3. La vi anoche
4. ¿Lo vio?
5. Lo terminé hoy
6. La vendí la semana pasada
7. Lo compré esta mañana
8. Lo dejé a casa
9. ¿Dónde la dejó?
10. ¿La tomó?
11. ¿Las vio?
12. ¿Los compró?
13. ¿Los vio?
14. Los vio
15. Los vi
16. Lo dudo
17. No lo dudo
18. No lo entiendo
19. Lo arreglé 
20. Voy a arreglarlo

## Lección Número Treinta y Cuatro

Verbs | Acid Test | Correct Pronoun | Sentence
-- | -- | --
wrote | to | le | Le escribí
visited | no | lo, la  | Lo visité
bought | to | le | Le compré
invited | no | lo, la | La invité
bothered | no | lo, la | Lo preocupé
gave | to | le | Le dí
examined | no | lo, la | Lo examiné
worried | no | lo, la | 
sang | to | le | Le canté
kissed | no | lo, la | La besé
sold | to | le | le vendí
spoke | to | Le hablé
insulted | no | La insulté
congratulated | no | lo, la | 
saw | no | lo, la | Lo vi

### Exercise in Translation

1. Le escribí una carta a Roberto
2. Le vendí una casa a Luis
3. Le hablé por el teléfono al doctor
4. Le compré una pipa a mi abuelo
5. Le compré un libro a Isabel
6. Le compré una blusa a mi abuela
7. ¿Le compró una pipa a su abuelo?
8. ¿Le compró una blusa a su abuela?
9. ¿Le vendió la casa a Luis?
10. Le mandé unos discos
11. Le mandé un libro
12. Le mandé un cable
13. Me mandó unas rosas
14. Nos mandó un telegrama
15. Le expliqué la situación
16. Le entregué el paquete 
17. Le expliqué la lección
18. Le mandaron un regalo
19. Nos mandó unos discos
20. Le mandaron un cable

## Lección Número Treinta y Cinco
### Exercise in Translation

1. Le dí un disco a mi prima
2. Le dio una blusa a su hermana 
3. Le dimos un radio a su hermano
4. Le dieron una cámara a su madre
5. Voy a darle una guitarra a Roberto
6. ¿Va a darle un tractor a Alberto?
7. ¿Va a darle una novela a Isabel?
8. Le dio una corbata a su hermano
9. Le dí una blusa a su hermana
10. Le dí una corbata a su primo
11. Le dimos una cámara a Roberto
12. Voy a darle un sofá a Roberto
13. Le mandé un libro a Carlos
14. ¿Le mandó un cable a su tío?
15. Le mandamos unas rosas a Elena
16. Le dieron un cable a su tía
17. Voy a mandarle unos discos a su tío
18. Le mandé un regalo a su tía
19. Voy a mandarle un regalo a Carlos
20. ¿Va a mandarle las flores a Elena?

## Lección Número Treinta y Seis

### Non Conformist Past Participles

*Nb. Not going to list regular past participles*

-- | --
-- | --
traer | he traído
caer | he caído
oír | he oído
hacer | he hecho
decir | he dicho
poner | he puesto
ver | he visto

### Poner Family
All members of verbs that end in poner (to put) have the same irregularities as poner.

-- | --
-- | --
poner | pongo
exponer (*to expose*) | expongo
imponer (*to impose*) | impongo
oponer (*to oppose*) | opongo
proponer (*to propose*) | propongo
suponer (*to suppose*) | supongo
componer (*to compose*) | compongo
disponer (*to dispose*) | dispongo
descomponer (*to decompose*) | descompongo

### Tener Family

-- | --
-- | --
tener | tengo
abstener (*to abstain*) | abstengo
contener (*contain*) | contengo
detener (*to detain*) | detengo
retener (*to retain*) | retengo
sostener (*to sustain*) | sostengo
obtener (*to obtain*) | obtengo
entretener (*to entertain*) | entretengo
mantener (*to maintain*) | mantengo

### Traer Family

-- | --
-- | --
traer | traigo
atraer (*to attact*) | atraigo
contraer (*to contract*) | contraigo
distraer (*to distract*) | distraigo
extraer (*to extract*) | extraigo
substraer (*to subtract*) | substraigo

### Exercise in Tranlation

1. Le traje un libro a mi tío
2. ¿Le trajo una pipa a mi abuelo?
3. Le dimos el periódico a mi abuelo
4. Le trajeron unas rosas a mi abuela
5. Le traje un libro a mi hermano
6. Le traje una rosas a mi abuela
7. Le dí una corbata a mi primo
8. Carlos le dio un libro a mi hermana
9. Le dimos un reloj al doctor
10. Le dieron una blusa a mi hermana
11. Le deje que habían estudiado
12. Le dijimos que habíamos trabajado 
13. Le dijeron que habían terminado
14. Me dijo que había terminado 
15. Me dijo que había estudiado
16. Me dijo que habían descansado
17. ¿Quiere ir a Cuba?
18. Queremos trabajar
19. Quería nadar esta tarde
20. Queríamos cenar
21. Querían decir muchas cosas
22. Quiero verlo hoy
23. Quería manejar el tractor
24. Quería manejar el auto
25. ¿Sabe bailar el tango?
26. Sabían hacer dulces
27. Puedo verlo hoy
28. ¿Puede ir a la fiesta?
29. ¿Pudieron comprender el problema?
30. ¿Pueden terminar el trabajo?

## Lección Número Treinta y Siete

Infinitive | Singular
-- | --
establecer | establezco
conocer | conozco
reconocer | reconozco
crecer (*to grow*) | crezco
desaparecer (*to disappear*) | desparezco
obedecer (*to obey*) | obedezco
desobedecer (*to disobey*) | desobedezco
merecer (*to deserve*) | merezco
nacer | nazco
ofrecer | ofrezco
parecer | parezco
compadecer (*to sympathise*) | compadezco

### Exercise in Translation

1. ¿Conoce a Roberto?
2. Conocemos a su hermano
3. Conocen a su tío
4. Conocí a Luis en el barco
5. Conocí a Henri en una fiesta
6. Conocí a su tío el año pasado
7. Me gusta la casa
8. ¿Le gusta el disco?
9. Nos gusta el libro
10. ¿Les gusta la casa?
11. ¿Le gusto la película?
12. ¿Le gustó el libro?
13. Nos gustó la película
14. ¿Les gustó el disco?
15. Me encanta el jamón
16. Le encanta el ajo
17. Le encanta nadar
18. Les encanta pescar
19. Nos encanta nadar
20. Le encanta el té
21. Le encanta el chocolate
22. Nos gustó el jamón x Nos gustó el tocino (*bacon*)
23. ¿Le gustan los espárragos?
24. Nos gustan los discos
25. Me gustan los tomates
26. ¿Le gustan los frijoles?
27. ¿Le gustan las cebollas?
28. Me encantaron las flores
29. ¿Le gustaron los discos?
30. Nos gustaron las zanahorias
31. Nos encantaron las aceitunas 
32. Les encantaron los rábanos
33. A María le gusta la obra
34. A mi hermano le gusta la novela
35. A mi tío le gusta el libro
36. A Señora Miranda le gusta el arroz
37. Al Señor Miranda le gusta la obra
38. A Señorita Mirando le gusta el disco
39. ¿Le gusta el sombrero a Elena?
40. ¿Le gusta la blusa a su hermana?
41. ¿Le gusta le disco a Enrique?
42. ¿Le gusta la película a su primo?

## Lección Número Treinta y Ocho
### Reflexive Verbs

Verb | Present | Past (*preterite*) | Present Perfect | Present Progressive | Future
-- | -- | -- | -- | -- | --
casarse (*to get married*) | me caso, nos casamos, se casa, se casan | me casé, nos casamos, se casó, se casaron | me he casado, nos hemos casado, se ha casado, se han casado | me estoy casando, nos estamos casando, se está casando, se están casando | me voy a casar, nos vamos a casar, se va a casar, se van a casar 
afeitarse (*to shave*) | me afeito, nos afeitamos, se afeita, se afeitan | me afeité, nos afeitamos, se afeitó, se afeitaron | me he afeitado, nos hemos afeitado, se ha afeitado, se han afeitado | me estoy afeitando, nos estamos afeitando, se está afeitando, se han afeitando | me voy a afeitar, nos vamos a afeitar, se está afeitar, se están afeitar
levantarse (*to get up*) | me levanto, nos levantamos, se levanta, se levantan | me levanté, nos levantamos, se levantó, se levantaron | me he levantado, nos hemos levantado, se ha levantado, se han levantado | me estoy levantando, nos estamos levantando, se está levantando, se están levantando | me voy a levantar, nos vamos a levantar, se va a levantar, se van a levantar
lavarse (*to wash*) | me lavo, nos levamos, se lava, se lavan | me lavé, nos lavamos, se lavó, se lavaron | me he lavado, nos hemos lavado, se ha lavado, se han lavado | me estoy lavando, nos estamos lavando, se está lavando, se están lavando | me voy a lavar, nos vamos a lavar, se va a lavar, se van a lavar
peinarse (*to comb*) | me peino, nos peinamos, se peina, se peinan | me peiné, nos peinamos, se peinó, se peinaron | me he peinado, nos hemos peinado, se ha peinado, se han peinado | me estoy peinando, nos estamos peinando, se está peinando, se están peinando | me voy a peinar, nos vamos a peinar, se va a peinar, se van a peinar
quitarse (*to take off*) | me quito, nos quitamos, se quita, se quitan | me quité, nos quitamos, se quitó, se quitaron | me he quitado, nos hemos quitado, se ha quitado, se han quitado | me estoy quitando, nos estamos quitando, se está quitando, se están quitando | me voy a quitar, nos vamos a quitar, se va a quitar, se van a quitar

### Exercise in Translation

1. Nos ponemos los abrigos
2. Se ponen los aguantes
3. Me pongo el sombrero
4. Se puso la bufanda
5. Me puse los zapatos
6. Se pusieron las medias
7. Me he puesto el traje
8. Me estoy poniendo la camisa
9. Me acuesto temprano
10. Carlos se acuesta tarde
11. ¿Se acuesta muy temprano?
12. Nos acostamos a las once
13. Se acuestan muy temprano
14. Me levanto a las siete
15. ¿Se levanta a las ocho?
16. Nos levantamos a las cinco
17. Se levantan a las seis
18. Me voy a levantar a las cinco mañana
19. Anoche me acosté muy temprano
20. ¿Se acostó muy temprano?
21. Nos acostamos a las diez
22. Me levanté a las seis
23. ¿Se levantó tarde?
24. Nos levantamos a las ocho
25. Se levantaron a las cinco
26. Me casé en junio
27. Eduardo se casó en agosto
28. Elena se casó con Eduardo
29. Marta se casó con Juan
30. Se casaron el año pasado
31. Enrique se casó en enero
32. Juan se casó en abril
33. Isabel se casó en febrero
34. Se van a casar en septiembre
35. Marta se casó en mayo

### Uses of Reflexive Verbs

1. "Ponerse a" means "to start to" and is used with the infinitive
    * Me puse a cantar ~ I started to sing
    * Se puso a llorar ~ He started to cry
2. "Ponerse" also means "to become" when followed by an adjective
    * Se puso furioso ~ He became furious
    * Se puso pálido ~ He became pale
3. Sometimes reflexive pronouns are used instead of a subject
    * Se habla español ~ Spanish is spoken
    * Se permite ~ It is allowed
4. The reflexive is used with reciprocal actions, that is, when people do things to each other
    * Se besaron ~ They kissed each other
    * Se parecen ~ They resembled each other
    * No se hablan ~ They don't speak to each other
5. When you use "ir" without saying where you are going like saying "going away" in English
    * Me fui con Roberto ~ I went with Roberto
    * Se fue solo ~ He went alone
    * ¿Por qué se fue? ~ Why did you go?
6. "Ya" is used in conjunction with "ir" and the reflexive pronoun  
    * Ya me voy ~ I'm going now
    * Ya se fueron ~ They've already left
    * Ya nos vamos ~ We're going now

### List of Reflexive Verbs

Verb | Example | Notes
-- | -- | --
Llamarse (*to be called*) | Me llamo Elena ~ I'm called Helen | Llamar means "to call" when not used reflexively and "to be called" otherwise
Quedarse (*to stay*) | Me quedé con Luis ~ I stayed with Luis | 
Caerse (*to fall*) | Me caí ~ I fell down | 
Parecerse (*to resemble, look like*) | Se parece a su mamá ~ He resembles his mother | 
Cepillarse (*to brush*) | Se cepilló el pelo ~ You brushed your hair | 
Meterse (*to get into*) | Me metí en un lío ~ I got myself into a scrape/mess | 
Sentarse (*to sit*) | Me siento en el sofá ~ I sit on the sofa | Radical changing verb
Acostarse (*to go to bed*) | Siempre me acuesto temprano ~ I always go to bed early | Radical changing
Acordarse (*to remember*) | ¿Se acuerdo? ~ Do you remember? | Radical changing
Deshacerse de (*to get rid of*) | Me deshice de él ~ I got rid of it | Conjugated like "hacer"
Atreverse (*to dare*) | Me atreví ~ I dared |
Quejarse (*to complain*) | Se quejó mucho ~ You complain a lot |
Portarse (*to behave yourself*) | Se porta bien ~ He behaves | 
Enterarse (*to find out*) | Se enteró para mi ~ Find out for me | 
Ponerse (*to put, to set, to become*) | Me puse el traje ~ I put on the suit | 
Darse cuenta (*to become aware of, to discover*) | Me dí cuenta ~ I found out | 
Equivocarse (*to make a mistake*) | Si no me equivoco ~ If I'm not mistaken | 
Rascarse (*to scratch*) | El mono se rascó la cabeza ~ The monkey scratched his head | 
Resbalarse (*to slip, slide*) | Me resbalé en el hielo ~ I slipped on the ice | 
Escaparse (*to escape*) | Me escapé de la fiesta ~ I got away from the party | 
Imaginarse (*to imagine*) | Me imagino qué llego esta noche ~ I imagine that he arrived tonight |
Bajarse (de)(*to get down*) | Se bajó del tren ~ He got of the train | 
Subirse (a) (*to get up, to climb*) | Se subió al árbol ~ She climbed up the tree | 
Apurarse (*to hurry*) | ¿Por qué no se apura? ~ Why don't you hurry? | 
Alegrarse (*to be glad*) | Me alegro de verlo ~ I'm glad to see it | 
Desquitarse (de)(*to get even, to retaliate*) | Hay que gozar mucho para desquitarse de la vida ~ One must enjoy much to get even with life | 
Verse (*to appear, look*) | Se ve mejor ~ He looks better | 
Ruborizarse (*to blush, to flush*) | Me ruborizó ~ I blushed | 
Volverse (*to become*) | Se volvió loco ~ He became mad | 
Cuidarse (*to care of yourself*) | Se va a cuidar ~ He's going to take care of himself | 
Perderse (*to get lost*) | Se perdió ~ She got lost | 
Cortarse (*to cut youself*) | Me corté ~ I cut myself | |
Lavarse (*to wash yourself*) | Nos lavamos ~ We wash ourselves | |
Secarse (*to dry oneself*) | Me sequé ~ I dried myself | | 
Quemarse (*to burn yourself*) | Me estoy quemando ~ I am burning myself | | 
Lastimarse (*To hurt yourself*) | Enrique se lastimó la espalda ~ Henry hurt his back | | 

### Test Your Progress
#### Test 1

1. Dije
2. Hizo
3. Hizo
4. ¿Quién dijo?
5. Vinieron
6. Tuvimos
7. He querido 
8. Dijeron
9. Voy a traer
10. ¿Ha estado en Cuba?
11. He estado trabajando 
12. Dijimos 
13. Hicieron
14. No pude
15. Trajo
16. Ha hecho
17. Están haciendo 
18. Ha traído
19. Salí
20. Oímos
21. Vieron
22. Han oído
23. Fue
24. Vimos
25. ¿Salió?
26. Oyeron 
27. Van a venir
28. Tengo
29. Sale
30. ¿Quién vino?
31. ¿Tiene?
32. Pongo
33. Traíamos 
34. Dice 
35. Va
36. Hacen
37. ¿Hace?
38. ¿Ve?
39. Oyen
40. Vamos
41. Tienen
42. ¿Quién fue?
43. Tiene diez años
44. Me siento yendo
45. Tengo un refriado
46. ¿Tiene hambre?
47. Tienen razón
48. ¿Está dormido?
49. Tengo frío
50. Quiero ver
51. No veo
52. ¿Puede ir?
53. Tiene frió
54. Hace una hora
55. Hace mucho tiempo
56. He visto
57. ¿Qué ha hecho?
58. Hemos decido 
59. Han visto
60. Propongo

#### Test 2

1. Lo vi
2. Nos vio
3. Lo recomendaron 
4. ¿Lo visitó? 
5. La vio
6. Los vimos 
7. ¿Quién la vio?
8. Nos vio
9. Me invitó 
10. ¿Quién lo trajo?
11. ¿Lo vio?
12. ¿La invitaron?
13. Me sorprendí x sorprendió
14. No la vio
15. Nos llevaron al cine
16. Me llevó a la fiesta
17. ¿Quién la trajo?
18. No lo vi
19. Le hablé
20. Me lo trajo
21. Le escribí
22. Las invitó
23. Le escribió a mi abuela 
24. Le compró una pipa a mi tío 
25. Les mandé un cable
26. Se lo dio
27. Le dio un suéter a Carlos 
28. Se lo dí 
29. Se lo vendió 
30. Me lo dio

#### Test 3

1. Me acuesto temprano
2. Se levantó tarde
3. Se casaron 
4. Se puso el sombrero 
5. Se afeitó
6. Me lavé las manos
7. ¿Por qué se fue?
8. Se quedó con su tía 
9. Se he acostado
10. Se está bañando

## Lección Número Treinta y Nueve
### Written Exercise 1

Infinitives | Preterite | Imperfect
-- | -- | --
comprar | compré | compraba
estudiar | estudié | estudiaba
tomar | tomé | tomaba
hablar | hablé | hablaba
copiar | copié | copiaba
trabajar | trabajé | trabajaba
visitar | visité | visitaba

### Written Exercise 2

Infinitives | Preterite | Imperfect
-- | -- | --
vender | vendí | vendía
correr | corrí | corría
ofrecer | ofrecí | ofrecía
recibir | recibí | recibía
decidir | decidí | decidía
salir | salí | salía

### Written Exercise 3

-- | --
-- | --
I had, used to have  | tenía
I wanted, used to want | quería
I knew, used to know | sabía
we sold, used to sell | vendíamos
we had, used to have | teníamos
they were, used to be (*estar*) | estaban
they sang, used to sing | cantaban
they did, used to do | hacían
they worked, used to work | trabajaban
they bought, used to buy | compraban
he made, used to make | hacía
she could, used to be able to | podría
you went out, used to go out | salía
he said, used to say | decía
we took, used to take | tomábamos
we danced, used to dance | bailábamos
they wrote, used to write | escribían 
they came, used to come | venían
she spoke, used to speak | hablaba
we spoke, used to speak | hablábamos
he used, used to use | usaba
she prepared, used to prepare | preparaba
I thought, used to think | creía
they said, used to say | decían
we imported, used to import | importábamos
she described, used to describe | describía
I read, used to read | leía
she learned, used to learn | aprendía
I hoped, used to hope | esperaba
we deposited, used to deposit | depositábamos
you ate, used to eat | comía
we walked, used to walk | caminábamos
they took care of us, used to take care of us | cuidaban
I helped, used to help | ayudaba
he won, used to win | ganaba
she played, used to play | jugaba

### Exercise in Translation 1

1. Quería comprar el caballo
2. Quería jugar al tenis
3. Carlos quería ir al cine
4. Carlos quería venir a la clase
5. Queríamos hacer dulces
6. Queríamos tomar la cena
7. Querían quedarse en casa
8. Querían levantarse temprano
9. Tenía un caballo
10. Alberto tenía una vaca
11. Teníamos una criada
12. Tenían un auto
13. Me levantaba temprano
14. Alberto se levantaba tarde
15. Nos levantábamos a las seis
16. Se levantaban muy temprano
17. Me acostaba muy tarde
18. Nos acostábamos a las diez y media
19. Me acostaba a las once
20. Me levantaba a las cinco

### Exercise in Translation 2

1. Dije que era delicioso
2. José dije que era maravilloso
3. ¿Quién dijo que era delgado?
4. ¿Quién dijo que era maravilloso?
5. Dijimos que era simpático
6. José dije que era simpática
7. Dijeron que era alto
8. José dijo que era gordo
9. Dije que era delgada
10. Creía que era guapo
11. Pablo creía que era bonita
12. Voy a estar en casa
13. Fui a ver las pinturas
14. Iba a llamarla al cine
15. Pablo iba darle el dinero
16. Iban a venir al despacho
17. Íbamos a hacer dulces
18. Iba a levantarme temprano
19. Íbamos a limpiar la casa
20. Iban a sentarse en la hamaca

## Lección Número Cuarenta
### Written Exercise

-- | --
-- | --
tomar | tome
hablar | hable
trabajar | trabaje
entrar | entre
pasar | pase
bailar | baile
terminar | termine
estacionar | estacione
comprar | compre
anunciar | anuncie
dibujar | dibuje
cambiar | cambie
cantar | cante
arreglar | arregle
caminar | camine
enseñar | enseñe
vender | venda
correr | corra
comprender | comprenda
aprender | aprenda
leer | lea
comer | coma
beber | beba
decidir | decida
escribir | escriba
permitir | permita
resistir | resista
vivir | viva
describir | describa

-- | -- | --
-- | -- | --
venir | vengo | venga
tener | tengo | tenga
hacer | hago | haga
traer | traigo | traiga
salir | salgo | salga
caer | caigo | caiga
oír | oigo | oiga
poner | pongo | ponga
decir | digo | diga

### Exercise in Translation 1

1. Compre unas aspirinas, por favor
2. Compre unas toallas
3. Compre una docena de huevos
4. Compre media docena de naranjas, por favor
5. Compre azúcar
6. Compren unas toallas
7. Siéntese por favor
8. Siéntese en el sofá por favor
9. Siéntese en el sillón
10. Siéntese junto a mí
11. Siéntese en la primera fila
12. Venga a la clase mañana
13. Venga al despacho
14. Vengan al club
15. No venga al despacho
16. Vaya a la playa
17. Vengan a vernos pronto
18. Vayan a la tienda
19. Venga al despacho ahora mismo
20. No vaya al correo

### Exercise in Translation 2

1. Tráigame un vaso de agua, por favor
2. Tráigame el boleto, por favor
3. Tráigame unas toallas, por favor
4. Tráigase unos helados
5. Llévese una flores
6. Llévese unas revistas
7. Llévese un regalo
8. No me traiga los libros
9. No me traigo los boletos 
10. Deme el dinero, por favor
11. Dele cincuenta centavos
12. No le dé dulces
13. Dele el paquete
14. Dele lo que quiere
15. Dele lo que quiere

### Test Your Progress
#### Test 1

1. Escribí una carta
2. Tenía un auto
3. Nos visitaba con frecuencia
4. Trabajaba el trabajo muy bien x Hacía el trabajo muy bien
5. Compramos una casa
6. La casa tenía un terraza
7. Tomaba un taxi todas las mañanas
8. Era muy simpático
9. Vendió la casa
10. Iba a hacerlo
11. Íbamos a vender la casa
12. Dijo que era terrible
13. Tenía un perro
14. Quería jugar al tenia
15. Íbamos a la playa
16. Nadaba muy bien
17. Era muy bonita
18. El vestido era lindo
19. Quería a su madre
20. Lo vi esta mañana

#### Test 2

1. Abra la ventana
2. Cuente el teléfono x Conteste el teléfono
3. Levántese
4. Cierre la puerta
5. Venga acá 
6. Póngalo en la mesa 
7. Escuche / Oiga
8. Cuénteme x Dígame
9. No se caiga
10. Siéntese
11. Tráigamelo 
12. Démelo 
13. Vaya a la tienda
14. Cómprelo
15. Hable despacio, por favor

## Lección Número Cuarento y Uno
### Exercise in Translation 1

1. Quiero que compre un sombrero
2. Quiero que venda la casa
3. Alberto quiere que escriba el artículo
4. Alberto quiere que termine el trabajo
5. Espero que lea el poema
6. Espero que mande el paquete
7. Eduard espera que conteste la carta
8. Espero que venga a la fiesta
9. Alberto quiere que venga a la fiesta
10. Alberto quiere que mande el paquete
11. Quiero que termine el trabajo
12. Espero que venda la casa

### Written Exercises

Infinitive | Present (*indicative*) | Command, Subjunctive
-- | -- | --
hablar | hablo | hable
terminar | termino | termine
caminar | camino | camine
mandar | mando | mande
manejar | manejo | maneje
lavar | lavo | lave
ayudar | ayudo | ayude
prometer | prometo | prometa
barrer *to sweep* | barro | barra
sorprender | sorprendo | sorprenda
aprender | aprendo | aprenda
asistir | asisto | asista
decidir | decido | decida
pensar | pienso | piense
cerrar | cierro | cierre
contar | cuento | cuente
recordar | recuerdo | recuerde
tener | tengo | tenga
venir | vengo | venga
hacer | hago | haga
salir | salgo | salga
poner | pongo | ponga
traer | traigo | traiga
oír | oigo | oiga
conocer | conozco | conozca
obedecer | obedezco | obedezca
ofrecer | ofrezco | ofrezca
merecer | merezco | merezca
ver | veo | vea
-- | -- | --
levantarse | me levanto  | se levante
acostarse | me acuesto | se acueste
enfermarse | me enfermo | se enferme
asustarse | me asusto | se asuste 
mejorarse | me mejoro | se mejore
irse | me voy | se vaya
quedarse | me quedo | se quede
casarse | me caso | se case

### Exercise in Translation 2

1. Quiero que lo haga
2. Quiero que lo traiga
3. Espero que lo vea
4. Dudo que lo haga
5. Temo que lo haga
6. Quiero que lo vea
7. Quiero que me lo compre
8. Espero que me lo dé 
9. Dudo que me lo mande
10. Temo que lo haga
11. Espero que me le mande
12. Espero que me lo explique
13. Espero que me lo preste
14. Dudo que mo lo preste
15. Quiero que se levante
16. Quiero que se acueste
17. No quiero que se enferme
18. No quiero que se asuste
19. Quiero que se quede
20. No quiero que se vaya
21. Espero que se mejore
22. Espero que se divierta
23. Elena quiere que se quede
24. Pedro quiere que se divierta
25. Pedro quiere que se case

### Exercise in Translation 3

1. Quiere que cante
2. ¿Quiere que cante?
3. ¿Quiere que canten?
4. Quiere que cantemos
5. Quiere que canten
6. Pedro quiere que cante
7. Quieren que cante
8. Quieren que escriba
9. Alberto quiere que lo hagamos
10. Pedro quiere que lo hagan
11. Quiere que escriba
12. Alberto quiere que cante
13. Albero quiere que cantemos
14. Espera que lo hagan
15. Espera que la haga
16. Pablo espera que escriba pronto
17. Esperan que escriba pronto

### Exercise in Translation 4

1. Es necesario que lo haga
2. Es terrible que lo haga
3. Es posible que lo haga
4. Es increíble que lo haga
5. Es lástima que lo hagan
6. Es preferible que lo hagan
7. Es natural que lo haga
8. Es bueno que lo haga

### Exercise in Translation 5

1. Que cante
2. Que venga
3. Que salga
4. Que lo haga
5. Que lo traiga
6. Que entre
7. Que vaya
8. Que se quede
9. Que juegue
10. Espero que venda el auto
11. Espero que compre la casa
12. Ojalá que encuentre el dinero 
13. Ojalá que vengan a la fiesta
14. Dios quiera que vea a su hijo
15. Dice que mande el dinero
16. Dice que vaya al despacho
17. Dicen que termine el trabajo

## Lección Número Cuarenta y Dos
### Exercise in Translation

1. Quería que trabajara mañana
2. Quería que trabajara esta tarde
3. Quería que trabajara esta mañana
4. Esperaba que trabajara esta tarde
5. Esperaba que trabajara esta mañana
6. Dijo que trabajara esta noche
7. Dijo que trabajara esta tarde
8. Queríamos que trabajara mañana
9. Querían que trabajara esta mañana
10. Dijimos que trabajara esta mañana
11. Quería que fuera a la fiesta
12. Quería que fuera a la fiesta
13. Esperaba que fuera a la fiesta
14. Esperaba que fuera a la fiesta
15. Queríamos que fuera a la clase
16. Querían que fuera al cine
17. Dijeron que fuera a la clase
18. Quería que vendiera la casa
19. Quería que vendiera el auto
20. Querían que vendiera el edificio
21. Quería que trajera la guitarra
22. Dijo que trajera el dinero
23. Esperábamos que trajera a su primo
24. Quería que se levantara 
25. Queríamos que se levantara
26. Dijo que se levantara
27. Quería que me lo diera 
28. Esperaba que me lo diera
29. Dijo que me lo diera
30. Querían que me lo diera

### Exercise in Translation

1. Hubiera ido si hubiera tenido un auto
2. Hubiera ido si hubiera podido
3. Hubiéramos ido si hubiéramos tenido un auto
4. Hubieran ido si hubieran podido
5. Hubiera estudiado si hubiera tenido tiempo
6. Hubieran estudiado si hubieran tenido tiempo
7. Hubiera terminado si hubiera tenido tiempo
8. Hubiéramos escrito si hubiéramos sabido
9. Hubiera terminado si hubiera tenido tiempo
10. Hubiera escrito si hubiera podido
11. Si hubiera tenido un coche, hubiera ido
12. Si hubiéramos tenido tiempo, hubiéramos estudiado
13. Si hubieran sabido, hubieran escrito
14. Si hubieran tenido tiempo, hubieran estudiado

## Lección Número Cuarenta y Tres
### Written Exercise

Present Tense | -- | -- | -- | --
-- | -- | -- | -- | --
Verb | Yo | Usted | Nosotros | Los
competir | compito | compite | competimos | compiten
pedir | pido | pide | pedimos | piden
despedirse (*to take leave of*) | me despido | se despide | nos despedimos | se despiden
repetir | repito | repite | repetimos | repiten
impedir (*to impede*) | impido | impide | impedimos | impiden
medir (*to measure*) | mido | mide | medimos | miden
derretir (*to melt*) | derrito | derrite | derretimos | derriten
freír (*to fry*) | frío | fríe | freímos | fríen 
vestirse | me visto | se viste | nos vestimos | se visten
desvestirse | me desvisto | se desviste | nos desvestimos | se desvisten
reírse (*to laugh*) | me río | se ríe | nos reímos | se ríen
sonreírse | me sonrío | se sonríe | nos sonreímos | se sonríen
Preterite | -- | -- | -- | --
competir | competí | compitió | competimos | compitieron
pedir | pedí | pidió | pedimos | pidieron
despedirse | me despedí | se despidió | nos despedimos | se despidieron
impedir | impedí | impidió | impedimos | impidieron
repetir | repetí | repitió | repetimos | repitieron
medir | medí | midió | medimos | midieron
gemir (*to moan*) | gemí | gimió | gemimos | gimieron 
derretir | derretí | derritió | derretimos | derritieron
vestirse | me vestí | se vistió | nos vestimos | se vistieron
desvestirse | me desvestí | se desvistió | nos desvestimos | se desvistieron 
reírse | reí | rió | reímos | rieron
sonreírse | me sonreí | se sonrió | nos sonreímos | se sonrieron

### Exercise in Translation

1. Me reí cuando vi al payaso 
2. Pedro se rió cuando vio la caricatura
3. Nos reímos cuando vimos al payaso
4. Se rieron cuando oyeron el chiste
5. Alberto se rió cuando leyó el chiste
6. Juan se rió cuando vio el sombrero
7. Me sonreí cuando lo vi anoche
8. Elena se sonrió cuando habló con Juan
9. Nos sonreímos cuando vimos al nene
10. Se sonrieron cuando lo vieron con Marta

## Lección Número Cuarenta y Cuatro
