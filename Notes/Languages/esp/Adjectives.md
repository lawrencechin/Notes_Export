# Adjectives

## Determiners

* Some | Alguno
* Both | Ambos
* That (*over there*) | Aquel
* Each | Cada
* Any | Cualquier
* A few (*c…*) | Cuantos
* That (*masc.*) | Ese
* This (*masc.*) | Este
* A lot, much | Mucho
* No (*not one*) | Ninguno
* Other | Otro
* Few, little (*p…*) | Poco
* All | Todo
* One | Uno

## Other 

* Picturesque | Pintoresco
* Lazy | Perezoso
* Opposite | Opuesta
* Unpicked | Descoser
* Deception | Engaño
* Dumb | Gafa
* Faith | Creyente
* Bitter | Amargo
* Stubbornness | Testarudez
* Thin | Delgado
* Chestnut | Castaños
* Funny | Cómico
* Really hilarious | Comiquísimo
* Very white | Blanquísimo
* Super fresh | Fresquísimo
* Very long | Larguísimo
* Really bitter | Amarguísimo
* Very old | Antiquísimo
* Super nice | Agradabilísimo
* Extremely happy | Felicísimo
* Really hot | Calentísimo
* Really nice | Amabilísimo
* Really outstanding | Notabilísimo
* Really miserable | Misirabilísimo
* Quite inferior | Inferiorcísimo
* Very talkative | Habladorcísimo
* Very young | Jovencísimo
* Extremely bitter | Acérrimo
* Extremely famous | Celebérrimo
* Extremely free | Libérrima
* Extremely healthy | Salubérrima
* Extremely wretched | Misérrimo
* Bilingual | Bilingüe
* Pretty | Bonito
* Able | Capaz
* True (*alt*) | Cierto
* Distinct | Distinto
* Hard | Duro
* Familiar | Familiar
* Final | Final
* Historical | Histórico
* Impossible | Imposible
* Intelligent | Inteligente
* Fair | Justo
* Ready, smart, clever | Listo
* Local | Local
* Bad | Malo
* Older | Mayor
* Younger | Menor
* Same | Mismo
* Natural | Natural
* Necessary | Necesario
* Normal | Normal
* Past | Pasado
* Flat | Plano
* Positive | Positivo
* Main | Principal
* Own | Propio
* Public | Público
* Fast | Rápido
* Real | Real
* Recent | Reciente
* Responsible | Responsable
* Safe | Seguro
* Following | Siguiente
* Simple | Simple
* Alone | Solo
* Only | Sólo
* So much | Tanto
* Traditional | Tradicional
* Last (*final*) | Último
* Only, unique | Único
* Useful | Útil
* Dirty | Sucio
* Clean | Limpio
* Available | Disponible
* Special | Especial
* Universal | Universal
* Intellectual | Intelectual
* Wide, spacious, ample | Amplio
* Maximum | Máximo
* Strange | Extraño
* Modern | Moderno
* Numerous | Numeroso
* National | Nacional
* Superior, top | Superior
* Industrial | Industrial
* True real (*v…*) | Verdadero
* Worldwide, world (*adj*) | Mundial
* Childish, infantile | Infantil
* Profound, deep | Profundo
* Appropriate | Adecuado
* Dark, gloomy | Oscuro
* Sweet | Dulce
* Electric | Eléctrico
* Old, ancient, antique (*a…*) | Antiguo
* Aware, conscious | Consciente
* Classical | Clásico
* Efficient, effective | Eficaz
* Convenient | Conveniente
* Previous | Previo
* Negative | Negativo
* Pending | Pendiente
* High, tall, elevated (*e…*) | Elevado
* Scare | Escaso
* Bright, shiny, sparkling, brilliant | Brillante
* Basic | Básico
* Difficult | Difícil
* Private | Privado
* Perfect | Perfecto
* Alive | Vivo
* Bitter, sour, acidic | Ácido
* Minimum, minimal | Mínimo
* Free (*at liberty, available*) | Libre
* Legal | Legal
* Weak, faint, dim | Débil
* Formal | Formal
* Present (*of  place*) | Presente
* Feminine | Femenino
* Equal, same (*i…*) | Igual
* Quiet, calm, peaceful | Tranquilo
* Simple, easy, straightforward (*se…*) | Sencillo
* Multiple | Múltiple
* Religious | Religioso
* Catholic | Católico
* Holy | Santo
* Federal | Federal
* Democratic | Democrático
* Presidential | Presidencial
* Civil | Civil
* Electoral | Electoral
* Fast | Aprisa
* Most interesting | Interesantisimo
* Charming | Simpático
* Out of tune | Desafinado
* Luxurious | Lujosos
* Worth the trouble | Vale la pena
* Proud | Orgulloso
* Miracle | Milagro
* Light (*as in a light meal*) | Liviana
* Squeezed (*as in orange juice*) | Exprimido
* Lukewarm | Tibios
* Muggy, sticky, sultry | Bochornosos
* Everyday, daily | Cotidiana
* Hectic, busy | Ajetreado
* Dumbfounded, astonished | Boquiabierto
* Chaos, commotion, mess | Zafarrancho
* Random  | Aleatorio
* Unlikely | Inverosímil
* Similar | Semejante
* Idle | Ociosas
* Slow, awkward, clumsy | Torpe
