# Time and Weather

## Days and dates

* The month | El mes
* January | Enero
* February | Febrero
* March | Marzo
* April | Abril 
* May | Mayo
* June | Junio
* July | Julio
* August | Agosto
* September | Septiembre
* October | Octubre
* December | Diciembre
* What day is today? | ¿Qué día es hoy?
* Today is January second | Hoy es dos de enero
* Today is February twenty-seventh | Hoy es veintisiete de febrero
* Which month is it? | Qué mes es?
* It is July | Es julio
* It is December | Es diciembre
* Today is June twelfth | Hoy es doce de junio
* Today is August first | Hoy es uno de agosto
* It is October | Es octubre
* It is May | Es mayo
* Monday | Lunes
* Tuesday | Martes
* Wednesday | Miércoles
* Thursday | Jueves
* Friday | Viernes
* Saturday | Sábado
* Sunday | Domingo
* The birthday | El cumpleaños
* Which day of the week is today? | ¿Qué día de la semana es hoy?
* Today is Monday so tomorrow is Tuesday | Hoy es lunes así que mañana es martes
* When is her birthday? | ¿Cuándo es su cumpleaños?
* Her birthday is on February the third | Su cumpleaños es el tres de Febrero
* Happy birthday! | ¡Feliz cumpleaños!
* Which day of the week is tomorrow? | ¿Qué día en la semana es mañana?
* Today is Thursday so tomorrow is Friday | Hoy es jueves así que mañana es viernes
* Today is Saturday so tomorrow is Sunday | Hoy es sábado así que mañana es domingo
* When is your birthday? | ¿Cuándo es tu cumpleaños?
* My birthday is on August the first | Mi cumpleaños es el uno de agosto
* His birthday is on September the tenth | Su cumpleaños es el diez de septiembre
* The calendar | El calendario
* Since | Desde
* The season (*alt*) | La estación
* The date | La fecha
* The dawn | La madrugada
* Noon | El mediodía
* A while | Un rato
* The time | La vez
* The end of the week | El fin de semana

## Weather

* Wet | Mojado
* To get (*oneself*) wet | Mojar(se)
* You will get wet | Te mojarás
* To get sick | Enfermar
* He will get sick | Él enfermará
* If | Si
* To rain | Llover
* It rains | Llueve
* If it rains you will get wet | Si llueve te mojarás
* To get (*oneself*) angry | Enfadar(se)
* If it rains he will get angry | Si llueve se enfadará
* To be hot | Tener calor
* You will have | Tendrás
* The sun shines | Hace sol
* If the sun shines you will get really hot | Si hace sol tendrás mucho calor
* Way too | Demasiado
* I am way too hot | Tengo demasiado calor
* Then (*solution*) | Entonces
* To take (*oneself*) off (*something*) | Quitar(se) (algo)
* Then take off your coat | Entonces quítate el abrigo
* To put (*oneself*) on (*something*) | Poner(se) (algo)
* Then put on your swim suit | Entonces ponte el bañador
* To be cold | Tener frío
* I will have | Tendré
* To snow | Nevar
* It snows | Nieva
* If it snows I will get cold | Si nieva tendré frío
* I am way too cold | Tengo demasiado frío
* Then put on your coat | Entonces ponte el abrigo
* Then put on you boots and gloves | Entonces ponte las botas y los guantes
* I will get wet | Me mojaré
* The raincoat | El impermeable
* Then put no your raincoat | Entonces ponte el impermeable
* Inside | Dentro
* If it is really cold we can stay inside | Si hace mucho frío nos podemos quedar dentro
* It was really cold so we stayed inside | Hizo mucho frío así que nos quedamos dentro
* The snow | La nieve
* The snowman | El muñeco de nieve
* It snowed | Nevó
* If it snows we can make a snowman | Si nieva podemos hacer un muñeco de nieve
* It snowed so we made a snowman | Nevó así que hicimos un muñeco de nieve
* If the sun shines we can go to the beach | Si hace sol podemos ir a la playa
* Probably | Probablemente
* To clear (*itself*) up | Despejar(se)
* It will probably clear up | Probablemente se despejará
* Up north | En el norte
* It will probably clear up up north | Probablemente se despejará en el norte 
* Down south | En el sur
* Cloudy | Nublado
* To get (*itself*) cloudy | Nublar(se)
* It will probably get cloudy down south | Probablemente se nublará en el sur
* The fog | La niebla 
* There is fog today | Hoy hay niebla 
* The risk | El riesgo
* The chance |  La probabilidad, la posibilidad 
* Lightning | Rayos
* There is a risk of lightning | Hay riesgo de rayos
* The thunderstorm, the storm | La tormenta
* Out west | En el oeste
* Light (*rain*) | Débil
* The shower (*rain*) | La llovizna
* Out east | En el este
* There will be | Habrá
* There will be light showers out west | Habrá débiles llovizna en el oeste
* The wind | El viento
* There will be strong winds down south | Habrá fuertes vientos en el sur
* The weather man/woman | El hombre del tiempo, la mujer del tiempo
* The weather forecast | El pronóstico del tiempo
* Always listen to the weather man | Escucha siempre al hombre del tiempo
* It might be that… | Puede ser que…
* It snows (*subjunctive*) | Nieve
* It might be that it snows | Puede ser que nieve
* It clears up (*subjunctive*) | Se despeje
* It might be that it clears up later | Puede ser que se despeje más tarde 
* To get (*oneself*) soaked | Empapar(se)
* He will get (*himself*) soaked | Se empapará
* If he doesn't bring his umbrella he will get soaked | Si no trae su paraguas se empapará
* Heavy (*weather*), intense | Intenso
* There is a risk of heavy rain down south | Hay riesgo de lluvias intensa en el sur
* We need (*subjunctive*) | Necesitemos
* Always read the weather forecast | Lee siempre el pronóstico del tiempo
* It will probably rain | Probablemente lloverá
* There will be heavy rain out east | Habrá intensas lluvias en el este
* It will probably snow | Probablemente nevará
* There is a risk of thunderstorms out west | Habrá riesgo de tormentas en el oeste 
* To get cold (*weather*) | Hacer frío
* To be hot (*weather*) | Hacer calor
* It makes, it does (*subjunctive*) | Haga
* It might be that it gets cold out east | Puede ser que haga frío en el este
* It might be that it gets hot today | Puede ser que hoy haga calor
* There was a risk of strong winds | Había riesgo de fuertes vientos
* There was a risk of heavy rain | Había riesgo de intensas lluvias
* There was a risk of lightning so we stayed inside | Había riesgo de rayos así que nos quedamos dentro
* The breeze | La brisa
* There was a light breeze | Había una ligera brisa
* There was a light breeze so we put on our coats | Había una ligera brisa así que nos pusimos los abrigos
* The suntan lotion | La crema solar
* We brought | Trajimos
* There was a change of sunshine so we brought suntan lotion | Había probabilidad de que hiciera sol así que trajimos crema solar
* To forget | Olvidar
* Must | Deber
* They must not forget the wellies | No deben las botas de agua
* You must not forget your raincoat | No debes olvidar el impermeable
* Dry | Seco
* It will rain tomorrow so they will need their raincoats | Mañana lloverá así que necesitarán los impermeables
* You must not forget the umbrella | No debes olvidar el paraguas
* Sunny | Soleado
* It will be sunny tomorrow so you won't need the wellies | Mañana estará soleado así que no necesitarás las botas de agua
* To go outside | Salir fuera
* It snowed so mucho we couldn't go outside | Nevó tanto que no pudimos salir fuera 

## Numbers

* The number | El número
* 1 | Uno
* 2 | Dos
* 3 | Tres
* 4 | Cuatro
* 5 | Cinco
* 6 | Seis
* 7 | Siete
* 8 | Ocho
* 9 | Nueve
* 10 | Diez
* 11 | Once
* 12 | Doce
* 13 | Trece
* 14 | Catorce
* 15 | Quince
* 16 | Dieciséis
* 17 | Diecisiete
* 18 | Dieciocho
* 19 | Diecinueve
* 20 | Veinte
* 21 | Veintiuno
* 25 | Veinticinco
* 30 | Treinta
* 33 | Treinta y tres
* 40 | Cuarenta
* 49 | Cuarenta y nueve
* 50 | Cincuenta
* 60 | Sesenta
* 70 | Setenta
* 80 | Ochenta
* 90 | Noventa
* 100 | Cien
* 300 | Trescientos
* 700 | Setecientos
* First | Primero
* Second | Segundo
* Third | Tercero
* Fourth | Cuarto
* Fifth | Quinto
* Sixth | Sexto
* Seventh | Séptimo
* Eighth | Octavo
* Ninth | Noveno
* Tenth | Décimo
* Eleventh | Decimoprimero
* Eleventh (*u…*) | Undécimo
* Twelfth | Decimosegundo
* Twelfth (*du…*) | Duodécimo
* Fifteen | Decimoquinto
* Twenty-first | Vigésimo primero
* Thirty-second | Trigésimo segundo
* Sixty-third | Sexagésimo tercero
* Eighty-seventh | Octogésimo séptimo
* Twenty-third | Vigésimo tercero
* Thirty-first | Trigésimo primero
* Forty-fifth | Cuadragésimo quinto
* One thousand | Mil
* Two thousand | Dos Mil
* Three thousand | Tres mil
* Nineteen forty-five | Mil novecientos cuarenta y cinco
* Nineteen eighty-two | Mil novecientos ochenta y dos
* Nineteen ninety-seven | Mil novecientos noventa y siete
* Two thousand and one | Dos mil uno
* Two thousand and ten
* Two thousand and fourteen | Dos mil catorce
* Four hundred and twenty five | Cuatrocientos veinticinco
* Six hundred and forty two | Seiscientos cuarenta y dos
* Eight hundred and one | Ochocientos uno
* Three thousand four hundred and eighteen | Tres mil cuatrocientos dieciocho
* Five thousand two hundred and one | Cinco mil doscientos uno
* Ten thousand one hundred and ninety three | Diez mil ciento noventa y tre
* The majority | La mayoría
* Third | Tercer
* Double | Doble

## Measurements & Dimensions

* The dimensions | Las dimensiones
* The meter | El metro
* The pair | El par
* The kilometer | El kilómetro
* Millimetre | Milímetro
* Centimetre | Centímetro
* Milligram | Miligramo
* Gram | Gramo
* Kilogram | Kilogramo
* Milliliter | Mililitro
* Liter | Litro
* Ounce | Onza
* Pound | Libra
* Cup | Taza
* Pint | Pinta
* Quart | Cuatro de galón
* Gallon | Galón
* The ton | La tonelada
* Inch | Pulgada
* Foot | Pie
* Yard | Yarda
* Mile | Milla
* Length (*la…*) | Largo
* Length (*lo…*) | Longitud
* Width (*…o*) | Ancho
* Width (*…a*) | Anchura
* Height (*of a person*) | Estatura
* Line | Línea
* Angle | Ángulo
* Right angle | Ángulo recto
* Perimeter | Perímetro
* Circumference | Circunferencia
* Area | Área
* Volume | Volumen
* Two-dimension | Bidimensional
* Three-dimensional | Tridimensional
* Measurement | Medida
* To draw (*such as a line between two points*) | Trazar
* To measure | Medir
* Ruler | Regla
* Tape measure | Cinta métrica
* Pass me the tape measure please. I need to measure the length of the table before buying the tablecloth | Pásame la cinta métrica, por favor. Necesito medir la longitud de la mesa antes de comprar el mantel
* Use the ruler to draw a line on the wall after having calculated the length and width | Usa la regla para trazar una línea en la pared después de haber calculado su largo y ancho
* Forty-two meters squared | Cuarenta y dos metros cuadrados
* Forty-two meters cubed | Cuarenta y dos metros cúbicos
* 4 x 4 (*four by four*) | Cuatro por cuatro
* It's about 20 kilometres from here to the airport | Desde aquí hasta e; aeropuerto son uno 20 kilómetros
* How many yards long is a (*american*) football field? | ¿Cuántas yardas se extiende una cancha de fútbol americano?
* The doctor recommends that I drink two litres of water everyday | El doctor recomienda que tome dos litros de agua todos los días
* Under my new regimen of diet and exercise, I lost ten pounds in just one month | Bajo mi nuevo régimen de dieta y ejercicio, perdí diez libras en un solo mes

## Asking the time

* The hour | La hora
* O’clock, on the dot | En punto
* Half | Media
* The minute | El minuto
* The date, the appointment | La cita
* When | Cuándo
* What time is it? | ¿Qué hora es?
* It is eleven o’clock | Son las once en punto
* It is half past eight | Son las ocho y media
* It is half past one | Es la una y media
* It is six past five | Son las cinco y seis
* When is your date? | ¿Cuándo es tu cita?
* When is you appointment? | ¿Cuándo es tu sita?
* At ten to four | A las cuatro menos diez
* At five to two | A las dos menos cinco
* It is twelve o’clock | Son las doce en punto
* It is half past nine | Son las nueve y media
* It is two past three | Son las tres y dos
* At ten past eight | A las ocho y diez
* The quarter | El cuarto
* How long | Cuánto dura
* The second | El segundo
* It is quarter to one | Es la una menos cuarto
* It is quarter past five | Son las cinco y cuarto
* How long is the film? | ¿Cuánto dura la película?
* How long is the show? | ¿Cuánto dura el espectáculo?
* Two hours twenty minutes and five seconds | Dos horas veinte minutos y cinco segundos
* Six hours thirty minutes and thirteen seconds | Seis horas treinta minutos y trece segundos
* It is quarter to seven | Son las siete menos cuarto
* How long is an hour? | ¿Cuánto dura una hora?
* One hour four minutes and twenty-five seconds | Una hora cuatro minutos y veinticinco segundos
