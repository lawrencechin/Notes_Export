# The Past

## Past Tense

* I was (*temporarily, unfinished event*) | Estaba
* You were (*temp. un fin, sing. informal*) | Estabas
* He/she was (*temp. un fin*) | Estaba
* We were (*temp, un fin*) | Estábamos
* You were (*temp. un fin, plu. informal*) | Estabais
* They were (*temp. un fin*) | Estaban
* I was confused | Estaba confuso
* I was very happy | Estaba muy feliz
* You were sick | Estabas enfermo
* He was really angry | Estaba realmente enfadado
* She was already ready | Ya estaba lista
* We were ready to eat | Estábamos listos para comer
* Were they sad? | Estaban tristes?
* I was (*perm, un fin*) | Era
* You were (*perm, un fin, sing. informal*) | Eras
* He was (*perm, un fin*) | Era
* We were (*perm, un fin*) | Éramos
* You were (*perm, un fin, plu, informal*) | Erais
* They were (*perm, un fin*) | Eran
* I was young | Era joven
* I was vegetarian | Era vegetariano
* You were blond | Eras rubia
* It was a very small car | Era un coche muy pequeño
* The dog was too old | El perro era demasiado viejo
* We were brothers | Éramos hermanos
* The boy and the girl were from Spain | El niño y la niña eran de España
* I was (*temp, fin event*) | Estuve
* You were (*temp, fin event, sing. informal*) | Estuviste
* He/she was (*temp, fin, sing. informal*) | Estuvo
* We were (*temp, fin event*) | Estuvimos
* They were (*temp. fin event*) | Estuvieron
* Once | Una vez
* Ago | Hace
* Three weeks ago | Hace tres semanas
* Yesterday | Ayer
* Yesterday I was in school | Ayer estuve en la escuela
* I was in Spain last year | Estuve en España el año pasado
* Last night you were in the office | Anoche estuviste en la oficina
* Yesterday you were at home | Ayer estuviste en casa
* He was in Madrid two days ago | Estuvo en Madrid hace dos días
* She was in the park | Estuvo en el parque
* Last night we were in a restaurant | Anoche estuvimos en un restaurante
* Were they in London last month? | ¿Estuvieron en Londres el mes pasado?
* I was (*per. fin event*) | Fui
* You were (*perm. fin event, sing. informal*) | Fuiste
* He was (*perm. fin event*) | Fue
* We were (*perm. fin event*) | Fuimos
* You were (*perm. fin event, plu. informal*) | Fuisteis
* They were (*perm. fin event*) | Fueron
* I was young once | Fui joven una vez
* I was blond once | Fui rubia una vez
* You were a doctor many years ago | Fuiste doctor hace muchos años 
* You were a terrible singer many years ago | Fuiste un cantante terrible hace muchos años
* That was a school once | Una vez eso fue un escuela
* My granddad was a doctor many years ago | Mi abuelo fue doctor hace muchos años
* Were were teachers once | Fuimos maestras una vez
* Were they lawyers once? | ¿Fueron abogados una vez?

## Haber - Diving into the Past & the Wing Tense

* (*to go into the past tense in Spanish, you dive.  To say "I bought something" you say "I have bought something".  After "have", you dive into the past with "bought", which in Spanish ends with -ado with the -ar track, and ends with -ido with the other track*)
* Spoken | Hablado
* Left | Salido
* Eaten | Comido
* Sold | Vendido
* Bought | Comprado
* Spoken | Hablado
* Taken | Tomado
* Lived | Vivido
* (*there are two Spanish verbs for "to be"; similarly, there are two verbs for "to have". The other verb is haber, and it is used for diving into the past.  In the present tense, this verb is like the future tense endings, but with the -r- replaced with a -h-*)
* I have bought something | He comprado algo
* We have bought | Hemos comprado
* (*to say "we have bought it", the "it" comes before "have"*)
* We have bought it | Lo hemos comprado
* He has bought it | Lo ha comprado
* Where have you bought it?, where did you buy it? | ¿Dónde lo ha comprado?
* He has sold it | Lo ha vendido
* Made, done | Hecho
* I have done it | Lo he hecho
* Said | Dicho
* He has told me | Me ha dicho
* (*there is also in Spanish an -ing tense in Spanish, corresponding to "was -ing" and "were so-ing"; we'll call it the "wing tense". For the -ar track, the ending is -aba*)
* I was waiting | Esperaba
* I was speaking | Hablaba
* I was buying it | Lo compraba
* I was preparing it | Lo preparaba
* (*for the other tracks, the ending is -ía*)
* I was leaving | Salía
* I was eating | Comía
* I was doing it | Lo hacía
* I was buying it | Lo compraba
* I have not understood what you were saying | No he comprendido lo que decía
* (*in Spanish, the "wing" tense expresses a straight line in the past, but it can also express a broken line in the past, which in English is "I used to do it" or "I did it very often", "I did it every day", "I did it all the time"*)
* He did it every day | Lo hacía todos los días
* It is done | Está hecho
* I have done it | Lo he hecho
* I prepared it, I have prepared it | Lo he preparado 
* Dinner is prepared | La cena está preparada
* I accepted the condition, I have accepted the condition | He aceptado la condición
* I haven't seen it yet, I still haven't seen it | Todavía no lo he visto
* Put | Puesto
* Where did you put it? Where have you put it? | ¿Dónde lo ha puesto?
* We have put it here | Lo hemos puesto aquí
* I won't forget it | No lo olvidaré
* That we won't forget it que | No lo olvidaremos
* I didn't forget it, I haven't forgotten it | No lo he olvidado
* Message | Recado
* I have left a message for you | He dejado un recado para usted
* We have spent much time | Hemos pasado mucho tiempo 
* We didn't spend much time, we have not spent much time | No hemos pasado mucho tiempo
* How much time did you spend? | ¿Cuánto tiempo ha pasado?
* To feel like | Tener ganas de
* I feel like staying here | Tengo ganas de quedarme aquí
* I feel like being here with all of you | Tengo ganas de estar aquí con ustedes
* How long do you plan on staying? | ¿Cuánto tiempo piensa quedarse?
* I would ask you later | Le preguntaría más tarde
* I will ask you later | Le preguntaré más tarde
* I will call you later | Le llamaré más tarde
* (*whenever "to" implies "in order to", in Spanish you must use para*)
* I am calling you in order to know | Le llamo para saber
* I am going to call you later | Voy a llamarle más tarde
* I will call you later to ask you | Le llamaré mas tarde para preguntarle
* If you can come see it with us tonight | Si puede venir a verlo con nosotros esta noche
* (*to say "I have just seen it", in Spanish you say "I just finished from seeing it": acabo de… which translates "I have just…"; this is a quick and painless way to speak in the past tense*)
* I have just left | Acabo de salir
* He has just left | Acaba de salir
* He has just left ten minutes ago | Acabo de salir hace diez minutos
* I have just seen it | Acabo de verlo
* I have just arrived here two days ago | Acabo de llegar aquí hace dos días

## Questions

* They saw, they watched | Vieron
* You gave (*sing. informal*) | Diste
* Was my dog in your garden yesterday? | ¿Estuvo mi perro en tu jardín ayer?
* Could you go to the cinema? | ¿Pudiste ir al cine?
* Did he work in the United States? | ¿Trabajó en los Estados Unidos?
* Did you show your passport in the airport? | ¿Enseñaste tu pasaporte en el aeropuerto?
* Did they see the mountains? | ¿Vieron las montañas?
* Did your sister have a good trip? | ¿Tuvo tu hermana una buena excursión?
* When were you in the park? | ¿Cuándo estuviste en el parque? 
* Where were you last night? | ¿Dónde estuviste anoche?
* Where did your brother go last year? | ¿A dónde tu fue hermano el año pasado?
* When did you visit your grandmother? | ¿Cuándo visitaste a tu abuela?
* When were your friends on TV? | ¿Cuándo estuvieron tus amigos en televisión? 
* What could he do? | ¿Qué pudo hacer?
* You ordered (*sing. informal*) | Pediste
* We saw, we watched | Vimos
* My dad had to buy a new car | Mi padre tuvo que comprar un coche nuevo
* We had breakfast and then we took a bus to London | Desayunamos y luego tomamos un autobús a Londres
* You looked at the book but you didn't buy it | Miraste al libro pero no lo compraste
* They went out for lunch and then they went for a swim | Salieron a comer y luego fueron a nadar
* You ordered fish and then a cake for dessert | Pediste pescado y luego una tarta de postre
* We visited my brother but we didn't see his wife | Visitamos a mi hermano pero no vimos a su esposa
* Yesterday you bought a train ticket | Ayer compraste un billete de tren
* I had to go to Liverpool last year | Tuve que ir a Liverpool el año pasado
* When did you go to university? | ¿Cuándo fuiste a la universidad?
* Where could you buy more wine last night? | ¿Dónde pudiste comprar más vino anoche?
* Did your mum meet your dad in a train station? | ¿Conoció tu madre a tu padre en una estación de tren?
* Could you visit your friend last year? | ¿Pudiste visitar a tu amigo el año pasado?
* I couldn't visit my family last week | No pude visitar a mi familia la semana pasada

## Verbs

* I wrote | Escribí
* You wrote (*sing. informal*) | Escribiste
* He/she wrote (*sing. informal*) | Escribió
* We wrote | Escribimos
* You wrote (*plu. informal*) | Escribisteis
* They wrote, you wrote (*plu. formal*) | Escribieron
* I met | Conocí
* You met (*sing. informal*) | Conociste
* You met (*plu. informal*) | Conocisteis
* He/she met (*sing. informal*) | Conoció
* We met | Conocimos
* They met, you met (*plu. formal*) | Conocieron
* Last night | Anoche
* Last week | La semana pasada
* Last year | El año pasada
* For two months | Durante dos meses
* Yesterday I helped my mother in the kitchen | Ayer ayudé a mi madre en la cocina
* We tried the dessert yesterday | Probamos el postre ayer
* It was a great party last night | Fue una fiesta grandiosa anoche
* They travelled to Germany last week | Viajaron a Alemania la semana pasada
* We ordered four starters | Pedimos cuatro aperitivos
* I studies in Japan for two months | Estudié en Japón durante dos meses
* They lived in China for eight years | Vivieron en China durante ocho años
* We waited for two weeks | Esperamos durante dos semanas
* I loved the cakes and the chocolate | Me encantaron las tartas y el chocolate
* We waited for two years | Esperamos durante dos años
* They helped the girl | Ayudaron a la niña

## Irregular verbs

* I had | Tuve
* You had (*sing. informal*) | Tuviste
* He/she had, you had (*sing. formal*) | Tuvo
* We had | Tuvimos
* You had (*plu. informal*) | Tuvisteis
* They had, you had (*plu. formal*) | Tuvieron
* I could | Pude
* You could (*sing. informal*) | Pudiste
* He/she could, you could (*sing. formal*) | Pudo
* We could | Pudimos
* You could (*plu. informal*) | Pudisteis
* They could, you could (*plu. formal*) | Pudieron
* We came | Vinimos
* They came | Vinieron
* I slept | Dormí
* We slept | Dormimos
* I saw, I watched | Vi
* They made, they did | Hicieron
* I wrote a book two years ago | Escribí un libro hace dos años
* He bought that hat twenty years ago | Compró ese sombrero hace viente años
* Last night they had dinner in the Spanish restaurant | Anoche cenaron en el restaurante español
* Last night we drank too much beer | Anoche bebimos demasiada cerveza
* We came to this country many years ago | Vinimos a este país hace muchos años
* He spoke Spanish very well | Habló español muy bien
* They made a big yellow hat for you | Hicieron un sombrero amarillo y grande para ti
* We slept on the train | Dormimos en el tren
* We travelled to Germany and met our friends three months ago | Viajamos a Alemania y conocimos a nuestros amigos hace tres meses
* I slept very well thank you | Dormí muy bien gracias
* You met him in the club two weeks ago | Lo conociste en el club hace dos semanas 
* My grandad and grandma came to my birthday | Mi abuelo y mi abuela vinieron a mi cumpleaños
* I saw her in the library yesterday | La vi en la biblioteca ayer
* We went dancing so today we are very tired | Fuimos a bailar así que hoy estamos muy cansados
* Last Saturday my friend made me a new pink dress | El sábado pasado mi amigo me hizo un vestido rosa nuevo 
* My dad never went out for dinner | Mi padre nunca salió a cenar
* You spoke terrible English | Hablaste terrible inglés
* I spoke to her for three hours last Monday | Hablé con ella durante tres horas hace el lunes pasado
* We went shopping yesterday | Fuimos de compras ayer

## Negation

* He made, she made, he did, she did | Hizo
* They slept | Durmieron
* We ordered | Pedimos
* They were not in restaurant four hours ago | No estuvieron en el restaurante hace cuatro horas
* He couldn't go for a swim | No pudo ir a nadar
* Last night she couldn't come to the cinema | Anoche no pudo venir al cine
* We didn't hate the film | No odiamos la película
* They didn't have dinner with us | No cenaron con nosotros
* He didn't meet his mother | No conoció a su madre
* I couldn't go dancing so I was very sad | No pude ir a bailar así que estaba muy triste
* My brother couldn't go to school yesterday | Mi hermano no pudo ir a la escuela ayer
* His friends didn't sleep at home | Sus amigos no durmieron en casa
* We didn't take the bus | No tomamos el autobús
* We didn't order any desserts | No pedimos nada de postre

## What happened?

* I was buying a skirt | Estaba comprando una falda
* He was buying a necklace for his mum | Estaba comprando un collar para su madre
* My girlfriend was telling me a story when you called | Mi novia estaba contando una historia cuando llamaste
* The witness was describing the man again | El testigo estaba describiendo al hombre otra vez
* It was snowing when we played golf | Estaba nevando cuando jugamos al golf
* It was raining in the place where we live | Estaba lloviendo en el sitio donde nosotros vivimos 
* My dad was calling my mum when I arrived home | Mi padre estaba llamando a mi madre cuando llegué a casa 
* My friend was turning on the TV when we heard the results | Mi amigo estaba encendiendo la televisión cuando oímos los resultados 
* The victim was eating a hotdog when the murderer stabbed him | La víctima comiendo un perrito caliente cuando el asesino lo apuñaló
* It was raining when we stopped to have a toilet break | Estaba lloviendo cuando paramos para ir al servicio 
* My friends were entering the jungle when they heard the waterfall | Mis amigos estaban entrando en la jungla cuando oyeron la cascada 
* They were walking towards the mountain when they saw the elephant | Estaban caminando hacia la montaña cuando vieron el elefante
* The ambulance was arriving when my mum came home | La ambulancia estaba llegando cuando mi madre llegó a casa
* I was buying earrings for my sister when she saw me | Estaba comprando unos pendientes cuando me vio
* The police were arresting the suspect | La policía estaba arrestando al sospechoso
* I was skiing in Norway when I broke my arm | Estaba esquiando en Noruega cuando me rompí el brazo
* We were eating together when he gave me the ring | Estábamos comiendo juntos cuando me dio el anillo
* My mum and my dad were watching TV when my grandma called | Mi madre y mi padre estaban viendo la televisión cuando mi abuela llamó
* My dad was playing golf when he broke his arm | Mi padre estaba jugando al golf cuando se rompió el brazo
* He was playing American football | Estaba jugando al fútbol americano 
* The doctor was asking questions when the police arrived | El doctor estaba haciendo preguntas cuando la policía llegó
* I have been | He estado
* I have stayed | Me he quedado
* It has not arrived | No ha llegado
* I've lost it | Lo he perdido
* He was late | Llegó tarde
