# Future

## Future Verbs - 'Hacer', 'Estar', 'Ir'

* I will do, I will make | Haré
* You will do, you will make | Harás
* He/she will do/make, you will do/make (*sing. formal*) | Hará
* We will do/make | Haremos
* You will do/make (*plu. informal*) | Haréis
* They will do/make, you will do/make (*plu. formal*) | Harán
* I will be (*temp*) | Estaré
* You will be (*temp, sing. informal*) | Estarás
* He/she will be (*temp, sing. formal*), you will be (*temp, sing. formal*) | Estará
* We will be (*temp*) | Estaremos
* You will be (*temp*) | Estaréis
* They will be (*temp*), you will be (*temp, plu. formal*) | Estarán
* I will go | Iré
* You will go (*sing. informal*) | Irás
* He/she will go, you will go (*sing. formal*) | Irá
* We will go | Iremos
* You will go (*plu. informal*) | Iréis
* They will go, you will go (*plu. formal*) | Irán

## Using the Present Tense for Future

* I am going to call you tomorrow | Voy a llamarle mañana
* (*in the future tense, you can also say "I will see": for "I will", you start with the whole verb*)
* (*often, the easiest way to talk about the future is to use "going"*)
* We will call you later | Vamos a llamarle más tarde
* At what time will you call me? | ¿A qué hora va a llamarme?
* I don't know yet how long we are going to stay | Todavía no sé cuanto tiempo vamos a quedarnos
* I will stay me | Quedaré
* (*the future tense ending for everything else starts with -rá-*)
* They won't buy it because it's too expensive | No lo comprarán porque es demasiado caro
* It will be ready for you tomorrow | Estará listo para usted mañana
* (*the -go verbs, in the future, use the ending -dre- instead of -re-*)
* I will leave | Saldré
* We will leave | Saldremos
* He will leave | Saldrá
* I will put | Pondré
* We will put | Pondremos
* (*with the two friends, digo and hago, you drop the -go- and add the ending -re-*)
* I will do | Haré
* I will tell | Diré 
* I will tell you later | Le diré más tarde
* I will tell it | Lo diré 
* I will tell you | Le diré
* (*for "would", you have a similar ending to the future: -ría*)
* It will be necessary | Será necesario
* It would be necessary | Sería necesario
* It wouldn't be necessary | No sería necesario
* It wouldn't be possible that way | No sería posible así
* I would like to go see it with you | Me gustaría ir a verlo con usted

## Other

* Maybe | Quizás
* He/she will read | Leerá
* Valencia | Valencia
* Back | De vuelta
* Soon | Pronto
* Before | Antes de
* After | Después de
* Or | O, u
* London | Londres
* The week | La semana
* Next | Próximo
* At home, home | En casa
* At school | En la escuela
* France | Francia
* Winter | Invierno
* Summer | Verano
* Autumn | Otoño
* Spring | Primavera
* Instead | En su lugar
* What will you do tomorrow? | ¿Qué harás mañana?
* What will he do tomorrow? | ¿Qué hará mañana?
* Maybe I will go to the theatre | Quizás iré al teatro
* Maybe I will go to the cinema | Quizás iré al cine
* Maybe he will read the newspaper | Quizás leerá el periódico
* I will go to the museum and to the library | Iré al museo y a la biblioteca
* She will go to the park and to the pub | Irá al parque y al bar
* Do you want to go to the park tomorrow | ¿Quieres ir al parque mañana?
* Do you want to go to Madrid tomorrow? | ¿Quieres ir a Madrid mañana?
* I think that I will go shopping instead | Creo que iré de compras en su lugar
* What will you do later today? | ¿Qué harás más tarde hoy?
* We will go to the supermarket and to the museum | Iremos al supermercado y al museo
* Do you want to go to the library later today? | ¿Quieres ir a la biblioteca más tarde hoy?
* I think I will go to Valencia instead | Creo que iré a Valencia en su lugar
* Will you be back soon? | ¿Estarás de vuelta pronto?
* Will you be back before or after dinner? | ¿Estarás de vuelta antes o después de cenar?
* He will go to London next week | Irá a Londres la próximo semana
* I will be home at four o'clock | Estaré en casa a las cuatro en punto
* I won't be at school tomorrow | No estaré en la escuela mañana
* We are going to go to France in the summer | Vamos a ir a Francia en verano
* Who are you going to go with? | ¿Con quién vas a ir?
* Will he be back soon? | Estará de vuelta pronto?
* Will they be back before or after the film? | ¿Estarán de vuelta antes o después de la película?
* They will go to the appointment at four o'clock | Irán a la cita a las cuatro en punto
* She will go to England next winter | Irá a Inglaterra el próximo invierno
* He won't be back at seven o'clock | No estará de vuelta a las siete en punto
* They won't be in Madrid at eleven o'clock | No estarán en Madrid a las once en punto
* Actually we won't be home tomorrow | En realidad no estaremos en casa mañana
* They are going to go to Valencia in the autumn | Van a ir a Valencia en otoño
* She is going to go home in the spring | Va a ir a casa en primavera
* Who is she going to go with? | ¿Con quién va a ir?
* Who are they going to go with? | ¿Con quién van a ir?
* It will be (*permenantly*) | Será
* Great | Fantástico
* Boring | Aburrido
* To arrive | Llegar
* To leave | Marcharse
* Exciting | Emocionante
* Fun | Divertido
* It will be great | Será fantástico
* It will be really boring | Será realmente aburrido
* When are you going to leave? | ¿Cuándo vas a marcharte?
* When are they going to arrive? | ¿Cuándo van a llegar?
* Are you going to watch a film tonight? | ¿Vas a ver una película esta noche?
* Are they going to go out tonight? | ¿Van a salir esta noche?
* It will be exciting | Será emocionante
* It will be fun | Será divertido
* We could go to the cinema | Podríamos ir al cine
* He is going to go to a museum this afternoon | Va a ir a un museo esta tarde
* They are going to go to a restaurant tonight instead | Van a ir a un restaurante esta noche en su lugar 
* We will speak | Hablaremos
* We will speak later | Hablaremos más tarde
* Is she going to come to the party tonight? | ¿Va a venir a la fiesta esta noche?
* You could write a really good book | Podrías escribir un libro realmente bueno
* We could make a cake | Podríamos hacer una tarta
* We are going to go shopping this afternoon | Vamos a ir de compras esta tarde
