# HIIT Workout

A simple 25-minute **HIIT** workout you can do from home (*and it's more effective than running*).

This **HIIT** session only includes three exercises for a full-body cardio workout. **HIIT** is the go-to workout of our times, with searches for the training style peaking and remaining high since lockdown began. It's understandable as to why: standing for high-intensity interval training, it's great for those who are short on time and don't have a lot of kit.

Plus, unlike other forms of cardio exercise, it doesn't involve us facing the cold, grey weather in order to get cardio benefits. "*The most significant benefit of interval training has to do with heart health*," says Lianna Swan, resident personal trainer at fitness app Shreddy. "*Intervals can boost cardio-respiratory health in a shorter time period in comparison to continuous forms of exercise*." 
Essentially, this means a 20-minute **HIIT** session is more effective for boosting fitness than a 20-minute run.

It's all because of the improved VO2 max, Lianna explains, which is "a measure of endurance that calculates the maximum volume of oxygen the body can use. We're not talking about bigger muscles, but rather getting you sweaty and your heart rate soaring."

Don't be put off if it sounds tough. The short bursts of hardcore exercise followed by even shorter rest breaks are intense, but the workouts themselves are pretty short. "*Excluding a warm-up, __HIIT__ usually lasts for a maximum of 20-30 minutes, and instead of working to sets and reps, you are working for a time limit – as hard and as fast as you can.*"

This workout in particular, is great for those who are new to the training style, are extra busy, or simply can't be bothered to fuss with a lot of different moves and equipment.

## 25-Minute HIIT Session that only Includes Three Exercises

This workout utilises simple full-body exercises to get the most out of your short training session. It's structured like so: one minute on, thirty seconds off.

- 1 minute of burpees 
- 30 seconds-1 minute rest
- 1 minute of goblet squats 
- 30 seconds-1 minute rest 
- 1 minute of press-ups

Rest for 1 minute between rounds and repeat five times

If you need to take a longer rest, by all means, increase the rest period to up to one minute. Repeat that circuit five times with one-minute rest between rounds for a workout that lasts for 25 minutes (or slightly longer if you increase your rest periods).

### Move One: Burpee

> Note: if you have joint pain, remove all jumps from the burpee. You can also take the press-up out of the move if it is too intense by simply jumping or stepping out to high plank, then bringing the feet straight back in.

![Burpee](./@imgs/Exercise_imgs/burpee.gif)

1. From a standing position, bend the knees to place your hands onto the ground and jump or step back your feet to a high plank position.
1. With control, lower your body all the way down to the ground.
1. Use your upper body and core strength to press back up to a high plank.
1. Jump or step your feet back towards your hands. 
1. Jump or step back to a standing position.

<video controls loop=true src=./@imgs/Exercise_imgs/burpees_alt.mp4></video>

### Move Tow: Goblet Squat

> Note: if you aren't experienced with performing a squat, remove the weight until you know that your form is perfect. Even if you know how squat, don't go too heavy, as you'll be moving fast through the exercise.

![Goblet Squat](./@imgs/Exercise_imgs/goblet_squat.gif)

1. Stand with your feet shoulder-width apart and hold a dumbbell or kettlebell in both hands at chest height.
1. Bend your knees and sit back to lower into a squat position – keep your back straight and chest upright.
1. Press through your heels to come back up to standing.

### Move Three: Press-Up

> Note: you can do these with your knees on the floor if doing a minute of full press-ups is too intense (we don't blame you!).

1. Place your hands on the floor slightly wider than shoulder-width with your arms straight. Your legs should be outstretched behind you with your toes tucked under – essentially in a high plank.
1. Draw your belly button in, squeeze your glutes and maintain a straight spine - with no arching through the back - and inhale.
1. Bend your upper arms and elbows to bring your chest to the floor.
1. Maintaining the tension, push the ground away and come back to your high plank.

## [The Best 6-Minute HIIT Circuit](https://www.stylist.co.uk/fitness-health/easy-hiit-workouts-when-tired/492224)

### Move 1: Drop Squats 

"The squat is a fundamental movement pattern we all should master," reminds Danyele. "Adding the dynamic plyometric element at the top is a fun way to turn up the intensity on this movement and it's also much more knee-friendly than the squat jump."

![Drop Squats](./@imgs/Exercise_imgs/drop_squat.gif)

- Start standing, with your feet shoulder-width apart. 
- Jump your feet out to wider than hip-width. 
- As you land, bend your knees to lower into a squat. 
- Touch the floor between your legs with one hand.
- Lift your hand off the floor and jump your feet back together to come to the starting position. 

### Move 2: Speed Skaters

"This one is all about creating lateral power while maintaining balance and stability. It's a fun, dynamic movement that really makes you feel like an athlete," says Danyele.

![Speed Skaters](./@imgs/Exercise_imgs/speed_skaters.gif)

- Start standing, with your feet shoulder-width apart. 
- Place your weight into your right foot and bend your right knee. Lift your left foot off the floor, hovering it behind your right calf.
- Hop to your left, powering through your right foot to landing on your left foot. Bring your right foot behind your left calf.
- Continue hopping from side to side, ensuring your head is up, chest is open and back flat. Drive your arms as you jump to help power the move, too.

### Move 3: Sprints

"On the treadmill or outside on the track or field, the efficiency of sprints can't be beaten," says Danyele. Don't have access to either of those? Try high-knee sprints from home as "a great way to get your heart rate up fast and challenge yourself physically and mentally."

- Start standing, with your feet shoulder-width apart.
- Relax your shoulders so they aren't hunched to your ears.
- Drive one knee up to your chest and jump change to bring the opposite knee in. 
- Repeat as fast as you can, so you are sprinting on the spot. Drive your arms as you do so to engage the whole body. 

To structure a **HIIT** workout, you need to factor in bursts of movement alongside a short recovery period. Do each move, followed by a rest period, back to back. Beginners should start with 30 seconds of work followed by 30 seconds of rest, and as you get more advanced, make it a 45 second work period followed by 15 seconds rest.

Repeat the circuit twice for the ultimate six-minute **HIIT**. Think you can keep going? Feel free to add more rounds in – but this is a pretty tough session with the two rounds alone. 
