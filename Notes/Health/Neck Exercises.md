# Neck Exercises

## Chin Tucks

![Chin Tucks Position 1](./@imgs/Exercise_imgs/chin_tucks_01.jpg)
![Chin Tucks Position 2](./@imgs/Exercise_imgs/chin_tucks_02.jpg)

* Lie on your back on a mat or the carpet. Place a small, flat cushion or book under your head. Bend your knees and keep your feet straight and in line with your hips. Keep your chest and ribcage relaxed and your chin gently tucked in.
* Gently lengthen the back of the neck feeling the chin slowly draw inwards. Hold for 8 to 10 breaths.

> Repeat up to 10 times

### Watch Points

* Make sure your head remains on the floor
* Keep the large muscles on the front the neck relaxed throughout
* Ensure your movements are slow and smooth, not fast and jerky
* Make sure your shoulders and chest remain relaxed
* Variations: After drawing the neck inwards, roll your neck slowly to the side. Never move into pain. Slowly return and repeat to the other side.

## Shoulder Blade & Neck Strengthening

![Shoulder blade and neck strengthening Position 1](./@imgs/Exercise_imgs/sbns_01.jpg)
![ Shoulder blade and neck strengthening Position 2](./@imgs/Exercise_imgs/sbns_02.jpg)

* Lie on your front with a small folder towel or flat cushion under your forehead. Keep your neck long, as you do in the "Chin Tucks" exercise.
* Slide your shoulder down into your back away from your ears. Hold for 3 to 5 breaths. Relax the shoulder blades to the starting position. The head remains down and neck long.

> Repeat 8 to 10 times

### Watch Points

* Do not squeeze your arms into your sides. Imagine you are holding a ripe peach between your arm and your body
* Do not allow your low back to arch. Use a folded towel under your stomach to avoid this
* Stop if you feel pain in your neck or back
* Variations: Try hovering your hands one inch from the floor after sliding your shoulder blades down. Once comfortable with this, try hovering your forehead one inch off the towel at the same time, keeping neck long.

## Chest Stretch

![Chest stretch Position 1](./@imgs/Exercise_imgs/chest_stretch_01.jpg)
![Chest stretch Position 2](./@imgs/Exercise_imgs/chest_stretch_02.jpg)

* Stand with your spine in correct alignment and your hands on your hips. Keep your neck long and shoulders blades down throughout the exercise.
* Slowly pull your shoulders back and your elbows towards each other. Hold for 20-30 seconds. Slowly return to the starting position. 

> Repeat 8 to 10 times

### Watch Points

* Do not arch your back 
* Keep neck long
* Only stretch as far as is comfortable
* Variations: Interlock your fingers behind your back, pull your shoulders back and lift your hands away from you.
