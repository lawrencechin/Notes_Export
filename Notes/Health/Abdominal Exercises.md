# Abdominal Exercises

## How to Do Crunches

![Crunch](./@imgs/Exercise_imgs/Crunch-Exercise.jpg)

1. Lie on the floor with your back flat, both knees bent, and feet planted on the floor about shoulder-width apart. Cross your arms, and place your hands on your chest.
1. Inhale, then exhale and activate your core muscles to lift your shoulder blades off the ground.
1. Hold for a second, then inhale while slowly lowering your shoulder blades to the floor. That's one rep.

## Weighted Pike Crunch

![Weighted Pike Crunch](./@imgs/Exercise_imgs/wpc.jpg)

1. Lie on a mat with one dumb-bell on your chest and another between your feet, which should be held slightly off the floor
1. Use your abs to crunch up and bring your knees into your chest

## Reverse Crunch

![Reverse Crunch](./@imgs/Exercise_imgs/reverse_crunch.jpg)

1. Start with your knees bent at 90° and your heels off the floor.
2. Contract your abs then curl your knees towards your chest, keeping your knees bent at 90°.
3. Pause at the top then lower slowly to the start.

Sets | Reps | Rest
-- | -- | --
5 | 6-8 | 90 Seconds

## Reverse Crunch alt

Taylor says reverse crunches are one of her favourite crunch variations. Here's how to do them.

![Reverse Crunch alt](./@imgs/Exercise_imgs/Crunch-Variation-Reverse-Crunch.jpg )

1. Lying on your back, lift your legs in the air with your knees bent at about 90 degrees. Place your hands on the floor beside your hips.
1. Without momentum, use your lower abs to slowly curl your hips off the floor and into your chest. Slowly lower them back to the starting position.
1. That's one rep.

## Foot Raise and Hold

![Foot Raise and Hold](./@imgs/Exercise_imgs/frah.jpg)

1. Lie on your back with your legs straight and your arms slightly out to the sides for balance
1. Raise both legs a few centimetres off the floor and hold that position

Sets | Reps | Rest
-- | -- | --
5 | 6-8 | 90 Seconds

## V-Sit (😓 Hard!)

![V-Sit](./@imgs/Exercise_imgs/vsit.jpg)

1. Sit on a mat with your knees bent at 90º and your upper body off the floor
1. Contract your abs to raise you upper body and your feet

Sets | Reps | Rest
-- | -- | --
3 | 12-14 | 60 Seconds

## V Crunch

![V Crunch](./@imgs/Exercise_imgs/v_crunch.jpg)

Work your upper and lower abs with one highly effective move.

1. Lying on your back, lift your legs and arms up so they are extended toward the ceiling. Lift your upper back off the floor, reaching your hands toward your feet.
1. Lower your legs toward the floor while reaching your arms overhead, keeping your shoulders off the mat and lower back pressed into the ground.
1. Repeat the crunch motion to complete one rep.

Do 15 reps.

## Dumbbell Side Bend

![Dumbbell Side Bend](./@imgs/Exercise_imgs/dsb.jpg)

1. Hold one dumbbell by your side then bend towards that side, sending the dumbbell down the outside of your thigh
1. Straighten up, then bend over to the other side
1. Do all the reps for that set then swap the weight over to the other side

Sets | Reps | Rest
-- | -- | --
5 | 6-8 | 90 Seconds

## Oblique Crunch

Though most of your ab workouts should be spent on exercises that target your entire core, a little bit of isolation work goes a long way. This crunch variation specifically targets your obliques (the muscles at the side of your torso) for core stability and powerful side-to-side movement.

![Oblique Crunch](./@imgs/Exercise_imgs/Dumbbell_Oblique_Crunch.jpeg)

1. Stand with your feet shoulder-width apart, holding a dumbbell in your left hand, and place your right hand behind your ear.
1. Inhale, and exhale as you engage your right obliques to crunch your right side body. Lift and straighten your torso, returning to standing.
1. Repeat for 30 seconds on each side.

## Heel Tap

Don’t be fooled! This simple-but-effective ab exercise engages your lower abs and improves core stability, particularly in the hip flexors.

![Heel Tap](./@imgs/Exercise_imgs/Heel_Tap.jpeg)

1. Lie on your back with your arms by your sides, knees bent, and feet hip-width apart.
1. Slowly lift your head, shoulder blades, and arms off the floor while gently drawing your ribs toward your hips to engage your core.
1. Bend your torso to the right, reaching your right hand toward your right ankle.
1. Return to the starting position, then bend your torso to the left, reaching your left hand toward your left ankle.
1. Continue alternating between right and left for 12 reps.

## Elbow Plank with Knee Drive

![Elbow Plank with Knee Drive](./@imgs/Exercise_imgs/elbow_plank_knee_drive.jpg)

1. Start in an elbow plank, and bring your right knee into your nose; your pelvis will rise toward the ceiling. Place right foot back on the ground.
1. Alternate sides, bringing your left knee in.

## Double-Leg Stretch

This classic Pilates ab move and crunch variation is perfect for working both your upper and lower abs. The goal is to feel a deep scoop in your abs, keeping your core strong and stable, while your extremities move.

![Double-Leg Stretch](./@imgs/Exercise_imgs/Crunch-Variation-Double-Leg-Stretch.jpg )

1. Start lying on your back with your hips and knees bent to 90 degrees in a tabletop position. Lift your upper back and head off the mat, reaching your fingers toward your toes.
1. Lengthen your legs away from your centre (at about a 45-degree angle to the floor) as you lower your upper body, reaching your arms overhead. Keep your low back pressing into the floor.
1. Bend your legs back into tabletop position as you lift your upper body off the floor and reach your hands toward your toes. That's one rep.

## Pilates Scissor

Another Pilates mainstay, scissors demand the same core stability as the double-leg stretch, but when extending one leg at a time.

![Pilates Scissor](./@imgs/Exercise_imgs/Crunch-Variation-Pilates-Scissor.jpg )

1. Lie on your back, and engage your core to lift your upper body off the floor so your shoulder blades hover.
1. Lift your left leg to hover off the floor, then bring it up toward the ceiling, gently holding your right shin.
1. Keep your upper body lifted as you switch legs. That's one rep.

## Seated Knee Tuck

This advanced crunch variation can be a little hard on your hip flexors and lower back; if you experience any pain, stop doing this exercise and rest or try a different move.

![Seated Knee Tuck](./@imgs/Exercise_imgs/Crunch-Variation-Seated-Knee-Tuck.jpg )

1. Start seated on the ground with your feet on the floor in front of you, knees bent. Place your hands about an inch behind your back with your fingers facing forward. Lift both feet up off of the ground, and balance on your glutes.
1. Extend both legs to hover just off the floor as you simultaneously lower your upper body a few inches.
1.Using your abs, bring your legs back to your chest. That's one rep.

## Overhead Reach With Leg Lower

 This move challenges both your lower and upper abs to maintain torso stability while your arms and legs move away from your centre. If you don't have a dumbbell, you can do this move with just bodyweight, too.

![Overhead Reach with Leg Lower](./@imgs/Exercise_imgs/Crunch-Variation-Overhead-Reach-With-Leg-Lower.jpg )

1. Lie on your back with your arms reaching toward the ceiling, holding one weight with both hands. With your left leg bent and right leg out long, bring your right toes toward the ceiling. This is your starting position.
1. Lower your arms and leg toward the floor, keeping your lower back touching the mat.
1. Lift your arms and leg to return to the starting position. That's one rep.

## Pilates Roll-Down

This Pilates roll-down is essentially a sit-up in reverse. By carefully and slowly lowering yourself down one vertebra at a time, you can improve your spinal mobility as well as the strength of your core in every position.

![Pilates Roll-Down](./@imgs/Exercise_imgs/Crunch-Variation-Pilates-Roll-Down.jpg )

1. Start sitting on your mat, with your knees bent and legs parallel with feet flat on the floor. Reach your arms toward the ceiling to lengthen your spine.
1. Exhale and pull your abs deeply toward your spine, and begin to roll down the floor one vertebra at a time; the movement should be smooth and controlled. Once your head reaches the mat, reach your arms overhead so they're parallel to the floor.
1. Exhale and begin to roll up, peeling your spine off the mat and coming all the way to sitting. Inhale your arms up toward the ceiling. That's one rep.

## Butterfly Crunch

This crunch and reverse-crunch hybrid works both your upper and lower abs.

![Butterfly Crunch](./@imgs/Exercise_imgs/Crunch-Variation-Butterfly-Crunch.jpg )

1. Lie on your back with your knees open and the soles of your feet together (in a butterfly position). Lengthen your arms overhead so they're resting on the floor.
1. Exhale and bring your hands and knees toward each other, performing a full-body crunch: lift your shoulder blades and hips just slightly off the mat. Hold this position for a moment, engageing your abs.
1. Slowly lower your arms and legs back to starting position. That's one rep.

## Dead Bug

![Dead Bug](./@imgs/Exercise_imgs/Dead_Bug.avif)

- Start with the correct core engagement. Lie flat on the floor with your knees bent and arms directly up above your shoulders. Breathe in and as you exhale, engage your abs and push your back into the floor, so there is no gap.
- Lift your legs off the floor so that they hit a right angle, knees above your hips, heels in line with your knees.
- While maintaining the position of the back, extend one leg and the opposite arm. Extend only so far as you’re able to, if your back loses contact with the floor, don't lower the arms and legs so far.
- Ensure your bent knee doesn't creep up towards your chest, keep it fixed above your hips.
- Return the leg and arm so that both arms are straight above your shoulders and legs are at a right angle.
- Repeat on the other side.

### Dead Bug Dumbbell

![Dead Bug Dumbbell](./@imgs/Exercise_imgs/Dead_Bug_Dumbbell.avif)

- With a dumbbell in each hand, lift your arms towards the ceiling. Raise your legs, your knees bent at 90º. Slowly extend your left arm and right leg simultaneously, keeping your back straight. Return, then repeat on the other side.

### Dead Bug Crunch

![Dead Bug Crunch](./@imgs/Exercise_imgs/Dead_Bug_Crunch.avif)

- Lie on the floor and raise your arms. Bring your legs up, pulling your knees towards you. Point your kneecaps at the ceiling, with your calves parallel to the ground. Keep your toes pointing up, too. Extend one leg, with your lower back pinned to the floor. Keeping both of your arms straight, lower them towards the floor over your head (in a regular dead bug, you’d lower them one at a time). Ensuring that your lower back is still pinned to the floor, bring your arms up to the starting position and crunch up, bringing your shoulders towards your hips.

## Double Leg Stretch

![Double Leg Stretch](./@imgs/Exercise_imgs/Double_Leg_Stretch.webp)

- Start lying on your back with your hips and knees bent to 90 degrees in a tabletop position. Lift your upper back and head off the mat, reaching your fingers toward your toes.
- Lengthen your legs away from your center (at about a 45-degree angle to the floor) as you lower your upper body, reaching your arms overhead. Keep your low back pressing into the floor.
- Bend your legs back into tabletop position as you lift your upper body off the floor and reach your hands toward your toes. That's one rep.

## Bicycle Crunches

![Bicycle Crunches](./@imgs/Exercise_imgs/Bicycle_Crunches.webp)

- Lie flat on the floor with your lower back pressed to the ground (pull your abs down to also target your deep abs). Put your hands behind your head.
- Bring your knees in toward your chest in a tabletop position and lift your shoulder blades off the ground.
- Straighten your right leg out to hover off the floor ground while turning your upper body to the left, bringing your right elbow toward your left knee. Make sure your rib cage is moving and not just your elbows.
- Switch sides and do the same motion on the other side. That's one rep.

## Weighted Scissor Kick

This dynamic exercise homes in on the lower abs and strengthens the hip flexors while challenging core stability. Plus, holding a dumbbell above your chest while scissor kicking engages your upper-body muscles as well.

![Weighted Scissor Kick](./@imgs/Exercise_imgs/Weighted_Dumbbell_Scissor_Kicks.jpeg)

1.Lie on your back with your legs and arms extended, holding one dumbbell above your chest with both hands, palms facing each other. 1. Engage your core by drawing your belly button toward your spine.
1. Slightly raise your right leg and lower your left leg simultaneously, making sure neither leg touches the floor.
1. Switch sides by slightly raising your left leg and lowering your right leg, maintaining control and ensuring that neither leg touches the floor.
1. Continue alternating between your right and left legs in a "scissor-like" motion for 12 reps.

## Body Saw

![Body Saw](./@imgs/Exercise_imgs/Body_Saw.webp)

- Start in an elbow plank position.
- Keep your back flat — don't let your hips droop or lift up. Picture your body as a long straight board, or plank.
- Maintaining this position, pull your body forward, shifting your weight forward onto your toes and forearms.
- With control, shift your weight back to the starting position. That's one rep.

## Slow Mountain Climber

![Slow Mountain Climber](./@imgs/Exercise_imgs/Slow_Mountain_Climber.webp)

- Start in a high plank with your shoulders over your wrists.
- Bring your right knee forward toward your chest, pulling your abs to your spine to deepen the ab work, holding the position for two to three seconds.
- Switch legs, returning your right leg to plank and pulling your left leg in toward your chest. That's one rep.

## Pilates Scissor Kicks

![Pilates Scissor Kicks](./@imgs/Exercise_imgs/Pilates_Scissor_Kicks.webp)

- Lie on your back with both legs in the air.
- Lift your head and shoulders off the ground. Hold your right ankle as you lower your left leg toward the floor, hovering a few inches off the ground.
- Keep your abs pulled to your spine, and switch legs. That's one rep.

## Elbow Plank with Leg Lift and Rock

![Elbow Plank with Leg Lift and Rock](./@imgs/Exercise_imgs/Elbow_Plank_With_Leg_Lift_and_Rock.webp)

- Start in an elbow plank.
- Lift your right foot about six inches off the ground, keeping your pelvis parallel to the floor. Avoid arching your lower back to lift your leg. Hold this position.
- Keeping your body straight and aligned, shift backward from your wrists and your left ankle joints. Shift forward again, and set your right foot down.
- Repeat the lift and shift with your right leg. That's one rep.

## Bear Hold and Drop

![Bear Hold and Drop](./@imgs/Exercise_imgs/Bear_Hold_and_Drop.webp)

- Start in a quadruped position (on your hands and knees) with your wrists directly underneath your shoulders and your knees directly underneath your hips. Your core should be engaged, and your spine should be in a neutral position.
- Inhale and lift your knees one inch off the ground. Be sure to keep your back flat and your abs engaged.
- Exhale and lower your knees to the floor with control. That's one rep.
- To add intensity, slowly step your feet back into a high plank position, hold, then step them forward again to return to your bear hold, keeping your core engaged throughout.
