# Stretches

## The 90/90

![The 90/90](./@imgs/Exercise_imgs/90_90.jpg)

"The 90/90 is one of my favourite general stretches for the hips," Merrick says. "It hits every single angle you're going to require, covering both internal and external rotation of the joint, with the focus of bringing more movement and more awareness into the hips."

He says that most people he sees are "very locked up around the hips". As a result, "their back then ends up doing a lot of the movement for them".

"Also, if you're sitting a lot in the day, you're going to get some compression and tightness of the glutes, and they then have an impact on other lower body positions like the squat," Merrick adds. "If we get more movement into the hips, I've found that's one of the best ways to help people feel more free and move better."

1. Sit upright with your right thigh perpendicular to your torso and your left thigh directly out in front of you. Your knees should both form a right angle, and your hands can be placed on the ground behind you for support. 
1. From here, lift both knees so they point at the ceiling, then allow them to fall in the opposite direction so your left thigh is perpendicular to your torso and your right thigh is extended in front of you. 
1. Continue to transition between these two positions, holding each side for a few deep breaths, for 90 seconds to two minutes. 
1. In the video above, Merrick demonstrates some methods you can use to elevate this stretch or target specific muscles and movements. 

## The Couch Stretch

![The Couch Stretch](./@imgs/Exercise_imgs/the_couch_stretch.jpg)

This is another stretch that targets the hip flexors – the muscles around the pelvis, responsible for bringing your knees towards your chest – as well as the quadriceps on the front of the thigh.

"If you're doing sports like running and cycling, or you're doing a lot of sitting, it can cause this tightening up of the quad and hip flexors," Merrick says.

"The nature of these activities means you're encouraging that flexed forward, tightened position [of the hips]. The couch stretch provides more of an extended position – the opposite experience to the stuff you would normally do day to day.

"It's also a really nice one because a lot of hip stretches won't include knee flexion [bending], which is going to stretch our rectus femoris – part of the quad muscle."

1. Place your left knee on the floor against a wall so your shin extends vertically upwards. 
1. Step your right foot forward so you're in a lunge position, with your left thigh and torso forming a straight line. Think about tucking your hips by squeezing your glutes (buttock muscles), and trying to pull your rear knee forward to contract the hip flexors. 
1. Place your hands on the floor inside your right foot, beneath your shoulders, for support. 
1. Hold this position for 60 to 90 seconds on each side. 
1. You can deepen this stretch by moving your torso more upright. 
1. Merrick says the couch stretch can be scaled to suit most fitness levels too. If you're new to flexibility training, you can start by keeping your hands on the floor for support and leaning forward over your front leg. As your flexibility increases, you can sit more upright to increase extension at the hip and flexion of the knee.

## The Squat

![The Squat Stretch](./@imgs/Exercise_imgs/the_squat_stretch.jpg)

This one might sound more like a strength-building exercise than a stretch, but Merrick says being able to hold a solid squat position "demonstrates a reasonable flexibility at several joints including the ankle, knee and hips".

"This is a fundamental human position, and it's a useful one to be able to get into," he adds. "If you're going to do any form of resistance training, you're probably going to come across a squat, and if you have kids or want to pick something up from the ground, you're going to want to be able to access this low position."

1. Stand with your feet roughly hip-width apart and your toes pointed slightly outwards. 
1. Keeping your chest up and your spine long, sink your hips down into a deep squat position. 
1. If you find yourself falling backwards, or you're unable to keep your chest up and your spine long in the bottom of the squat, hold onto a sturdy anchor point in front of you for extra support. 
1. Hold this position for 60 to 120 seconds, or watch Merricks' video for some variations to try. 
1. Most people will find they fall backwards when they try to get into a deep squat position, Merrick says. This is due to a lack of flexibility at either the ankles or hips, which forces the spine to round and more weight to be placed towards the back of the squat.

To remedy this, he recommends practising sitting in a deep squat position while holding a sturdy anchor point in front of you, focusing on keeping your chest up and maintaining good positions.

"This comes down to the SAID [specific adaptation to imposed demand] principle," says Merrick. "If you want to get better at X, do X more, so find a way you can sit in a squat comfortably, then you can spend some time wiggling about, moving, and trying to get a good stretch."

Read more: Why squats should be a staple feature in your workouts, according to an expert coach

## The Elephant Stretch

![The Elephant Stretch](./@imgs/Exercise_imgs/the_elephant_stretch.jpg)

The hamstrings are the large muscles that run down the back of the thigh. In his time working with people to improve their flexibility, Merrick says tightness in this area is one of the main things that hampers clients' freedom of movement.

"If you want to work on more advanced flexibility, the hamstrings are almost like a key. If you unlock them, it lets the hips tilt and move more freely, which then is going to make you feel more flexible in general for things like squatting. So stretching the hamstrings in one form or another is great."

He says the elephant walk, where you reach towards the ground and straighten one leg at a time to stretch your hamstrings and shift your hips, is his favourite position for achieving this.

1. Stand upright with a slight bend in your knees, then reach your hands towards the ground in front of you. 
1. With your hands on the ground for support, straighten your left leg while keeping your right knee slightly bent. 
1. Hold this position for a second, then switch sides. 
1. Continue to do this for 60 seconds, then straighten both legs and try to place your hands on the back of your lower legs, using them to pull you deeper into the stretch.
1. Hold this position for 30 to 60 seconds.

## The Hang Stretch

![The Hang Stretch](./@imgs/Exercise_imgs/the_hang_stretch.jpg)

Like the squat before it, Merrick says the hang covers a lot of bases. It can aid overhead flexibility for improved shoulder health, stretch a range of muscles around the shoulder joint including the latissimus dorsi in the back and pectorals in the chest, and it provides decompression of the spine too.

"During the day we might get a lot of compression in the spine just from being on our feet, moving around, sitting down," he explains. "Hanging allows gravity to pull that spine down and lengthen it."

1. Grab a pull-up bar with an overhand grip and your hands roughly shoulder-width apart. 
1. Take your feet off the ground to support your weight through your grip. 
1. Think about pulling your chest in to create a straighter body position.
1. Hold this position for 30 to 60 seconds.
1. If you are unable to support your bodyweight, use a lower pull-up bar and keep your feet on the ground to support some of your weight. 
1. Slowly reduce the weight taken by your feet over time to progress this move.   
1. Hanging can also develop your grip strength, which has been identified as an "indispensable biomarker" for older adults in a review published in the Clinical Interventions in Aging journal. This is because it indicates good generalised strength, bone density and other important health markers.

"We know that grip strength is a really important factor, one for sports but also for longevity," Merrick says. "From hanging, we're going to naturally build some strength in the grip - being able to hang for between 30 and 60 seconds would be a good target for most people."

## Dynamic Prone Plank

![Dynamic Prone Plank](./@imgs/Exercise_imgs/dpp.jpg)

1. Get on your hands and toes, facing the floor, keeping your head, back and legs in a straight line and your arms underneath your shoulders. Lift your rear toward the ceiling, pulling the belly towards the spine, forming a pike or downward dog position, lengthening your arms and legs. 
2. Return to plank position and bend your elbows against your sides, lowering your torso and legs to the floor.
3. Keeping your lower body flat on the floor, use your arms to push your chest and head up toward the ceiling, similar to the cobra pose in yoga, stretching out the front of your body.
4. Lower yourself and push your body back into plank position. 
5. Repeat 5 to 10 times. As you get stronger increase number of repetitions.

Sets | Reps | Rest
-- | -- | --
1 | 5 - 10 | 0

## Inchworm

![Inchworm](./@imgs/Exercise_imgs/inchworm.jpeg)

If you're a fan of yoga, you probably know how to do an inchworm already — it's essentially the same movement that's used to help people transition from downward dog to plank. Here's a step-by-step guide.

1. Starting from standing, create a soft bend in the knees and push the hips back behind you, hinging at the hips into a forward fold.
1. Slowly walk both hands out in front of you, passing through a downward-dog pose, until you reach a full plank position.
1. Optional: For an added challenge, from the plank position complete one push up.
1. From here, you have the choice of walking your feet towards your hands, trying to keep the knees as straight as possible, then returning to standing, or rewinding the motion by walking your hands back towards your feet, then returning to standing.

### Inchworm Alternatives

An inchworm might look basic, but it's no joke. If the full motion is a bit too much of a challenge, there are plenty of modifications to try.

- Forearm Plank Hold: Starting with your belly on the floor or a mat, push up gently off the ground to come into a forearm plank, while keeping the hips and spine level with one another. Start by holding for five to 10 seconds for one to three sets, slowly adding on more time as you grow stronger.
- Plank Hold: To progress from a forearm plank hold, try a performing a plank hold with both arms extended and your palms on the floor. Again, start lying face-down, then push off the ground onto your hands, keeping a nice even alignment between your spine and hips. Start off for one to two sets of five to ten seconds.
- Downward Dog: Start from a prone position (meaning your stomach is on the floor) and slowly curl the toes underneath you and push away from the floor, bringing your hips towards the sky. The body will create an inverted "V" shape. Aim to have your feet fully on the floor, your knees straight but not locked. You can also slowly bend one knee at a time, pedaling out the feet, to feel a gentle stretch in the back of the calf muscles. Work on holding this position for short intervals of time, such as three to five seconds for one to three sets.
- Hip Hinges: Hone the first movement of the inchworm walk-out by practicing your hip hinge form. To do: stand about six inches in front of a wall, with your back facing the wall. With your back flat and a slight bend to your knees, bend or hinge forward at the hips and reach your butt backward until it taps the wall, and then return to standing.

## Standing Figure-4 Stretch

![Standing Figure-4](./@imgs/Exercise_imgs/figure-four.jpeg)

"I do this every day, especially after running," Ezekh says. "It hits all the right spots."

1. Begin standing with your feet hip-width apart and arms by your sides.
1. Shift your weight onto your right foot and bend your left knee, lifting your left foot off the ground.
1. Cross your left ankle over your right thigh, creating a "4" shape with your legs.
1. Sit back into an imaginary chair by bending your right knee and lowering your hips down and back.
1. Release, stand back up, and repeat on the other leg.
1. Hold for 20-30 seconds per side.

## Butterfly Fold

![Butterfly Fold](./@imgs/Exercise_imgs/butterfly-fold.jpeg)

"This one's a classic for a reason," Ezekh says. "I love this after long periods of sitting or driving—it's a quick way to feel looser."

1. Start in seated position on the floor and bring the soles of your feet to touch, letting your knees open out to the sides.
1. Place your hands on your knees or ankles.
1. Twist your torso to the left and place your right hand on your left knee. Place your left hand on the floor behind you.
1. Look to the side or behind you.
1. Hold for 20-30 seconds, then come back to centre with a neutral spine, placing your hands on your knees.
1. Repeat on the opposite side.

## Lateral Lunge

![Lateral Lunge](./@imgs/Exercise_imgs/alternating-lateral-lunge.jpeg)

"This is a great combo of stretching and strengthening, and it's perfect for warming up before workouts," Ezekh says.

1. Stand with your feet at hip-width distance, toes facing forward. Clasp your hands in front of your chest.
1. Shift your weight to the right and step to your right with your right foot.
1. With a flat back, bend your right knee and shift your hips back, keeping your left leg straight.
1. Keep your toes pointed forward on both feet.
1. Hold here for 20–30 seconds, or shift side to side for a more dynamic stretch.

## Pigeon Pose

![Pigeon Pose](./@imgs/Exercise_imgs/half_pigeon.jpeg)

"This one's my personal favorite," Ezekh says. "After a run or heavy leg day, I sink into pigeon pose and let my hips stretch deeply. It's the ultimate hip opener."

1. Begin in downward-facing dog. Take a deep breath in, lifting your right leg up to the sky.
1. As you exhale, bring your right knee forward toward your right wrist. Place your right knee on the mat and position it just outside your right hip rather than directly in front.
1. Slowly walk your right foot toward the left side of your mat, aiming to bring your shin parallel to the front edge of the mat.
1. Gently lower your hips toward the mat, ensuring they are squared to the front of your room or mat.
1. For a deeper stretch, slowly walk your hands forward, lowering your torso down over your right leg.
1. Hold the position for 20–30 seconds, then switch sides.

## Curtsy Lunge

![Curtsy Lunge](./@imgs/Exercise_imgs/curtsy-lunge-curl.jpg)

Turn up the power in your butt workouts at the gym or at home with this lunge variation that activates the gluteus medius, a smaller glute muscle on the side of your butt that helps to externally rotate the hip. To make the move more difficult, do it with a pair of dumbbells or a kettlebell held in the goblet squat position.

1. Stand with feet hips-width apart. Then, keeping weight in left foot, take a big step back with right leg, crossing it behind left leg. Make sure hips stay facing forward.
1. Slowly bend knees and lower down until front thigh is parallel to the floor, and both knees are bent at roughly 90 degrees.
1. Push through left heel to returning to starting position.

Do 3 sets of 15 reps per side.

## Quadruped Hip Extension

![Quadruped Hip Extension](./@imgs/Exercise_imgs/glute-kickback.jpg)

This bodyweight move can be done anywhere for ease, and you can make it more challenging with the use of resistance bands or a light dumbbell behind the knee. Plus, the flexed heel curling toward your butt activates your hamstrings nicely.

1. Start on hands and knees with knees directly below hips and wrists directly below shoulders, fingers pointing forward.
1. Keeping core engaged to avoid arching back, lift right leg to the sky (keeping the knee bent). The foot should be flexed so sole of foot is facing the ceiling. Avoid rotating hips by keeping shoulders and hips squared to the floor during the entire exercise. Return to start to finish the rep, then repeat.

Do 3 sets of 8 to 12 reps per leg.

## Pistol Squat

![Pistol Squat](./@imgs/Exercise_imgs/pistol-squat-1.jpg)

"You cannot cheat on this exercise," says Michele Olson, Ph.D., professor of exercise physiology at Huntingdon College in Montgomery, Alabama. "This butt workout move takes the best glute activation a squat can offer and the best hip and thigh activation that a lunge can offer all rolled into one truly challenging but oh-so-worth-it exercise!"

To modify this advanced glute exercise, put a chair directly behind you, so you can quickly tap your butt to the edge of the seat. As you progress, you can use objects (such as the bottom of a step) that are lower to the ground.

1. Stand on right leg with entire foot rooted firmly into the floor, left leg lifted slightly forward to start.
1. Bend right knee and send hips backward, reaching both arms forward while extending left leg forward, lowering body until hips are below parallel.
1. Squeeze glutes and hamstring to stop the descent, then imagine pushing the standing leg through the floor to press back up to standing.

Do 2 sets of 5 reps on each leg.

## Stationary Lunge

![Stationary Lunge](./@imgs/Exercise_imgs/stationary-lunge.jpg)

The stationary lunge, also called a split squat, is a classic glute-strengthening move, and was suggested for your butt workout by Cari Shoemate, an ACE-certified personal trainer. Plus, the split stance challenges your balance, making your glutes work even harder. Pro tip: Keep the front heel pressed into the ground and avoid lunging forward — always drop down and up out of the lunge.

1. Stand with feet together holding a dumbbell in each hand. Take a big step behind body with left leg, lifting left heel off of the floor, keeping most of the weight in right leg.
1. Slowly bend both knees, lowering body straight down until both knees create 90-degree angles, making sure to keep front knee in line with ankle.
1. Push down through front heel to slowly stand back up.

Do 3 sets of 15 reps per leg.

## Plié Squat

![Plié squat](./@imgs/Exercise_imgs/sumo-squat-heel-lift.jpg)

This butt-building exercise not only helps to strengthen your glutes with every rep, but it also targets the inner thighs and calves, thanks to the wide stance and raised heels. To keep your balance on your tippy-toes, make sure to draw the abs in tight while you squat.

1. Stand with palms pressed together at chest, feet twice as wide as shoulders-width apart, and toes turned out about 45 degrees.
1. Lift heels off the floor, balancing on balls of feet. Bend knees and lower body straight down, keeping hips under shoulders and back straight. Make sure knees open over, but not past, toes as they bend.
1. Slowly straighten back up to standing and then lower heels to return to start.

Do 3 sets of 15 reps.

## Chair Twist

![Chair Twist](./@imgs/Exercise_imgs/chair_twist.avif)

1. While seated, clasp both hands behind your head.
1. Rotate your upper body to one side and look up to the opposite elbow.
1. Hold for 5 seconds and return to the starting position before repeating two more times.
1. Once you finish all 3 repetitions, perform the same stretch on the other side.

## Hip Flexor Stretch

![Hip Flexor Stretch](./@imgs/Exercise_imgs/hip_flexor_stretch.avif)

1. Get into a lunge position with the leg you are trying to stretch farther from the wall.
1. With one hand against the wall, reach with the opposite arm up in the air and reach towards the wall.
1. Hold for 5 seconds and repeat two more times before changing sides.

## Seated Pretzel

![Seated Pretzel](./@imgs/Exercise_imgs/seated_pretzel.avif)

1. Cross one leg over the other with your shin parallel to the floor.
1. Push one arm into the inside of your knee to push your crossed leg down.
1. Reach forward and towards the ground in front of you.
1. Hold here for 5 seconds and return to the starting position.
1. Repeat 2 more times before repeating on the other side.

## Wall-Assisted Single-Leg RDL

![Wall-Assisted Single-Leg RDL](./@imgs/Exercise_imgs/wall_assisted_s_l_rdl.avif)

1. Plant your hands against a wall.
1. Bring one leg up with your knee bent and hold this position for 5 seconds.
1. Extend the same leg back behind you until it straightens and hold for 5 seconds.
1. Repeat 2 more times before changing sides.

## Wall Slide

![Wall Slide](./@imgs/Exercise_imgs/wall_slide.avif)

1. Stand with your back against a wall, with your elbows bent at 90 degrees and the back of your hands and forearms against the wall.
1. Slide your hands up the wall for 5 seconds, reaching as high as you can without your elbows losing contact with the wall.
1. Reverse the movement downwards.
1. Repeat 2 more times.

## [Yoga/Pilates Warmup Stretches](https://www.verywellfit.com/how-to-warm-up-for-yoga-3567192)  

Keep in mind that you don't need to do the fullest expression of each of these poses—you are just starting to move your body and shake off the cobwebs. You can also use these stretches for your ​home practice, before doing a yoga video, or just to relieve tension at the end of the day.

### Pelvic Tilts

![pelvic tilts](./@imgs/Exercise_imgs/pelvic_tilts.jpg)

Begin by lying down on your back with your knees bent for a few pelvic tilts.

To do these, press your lower back gently against the floor, tilting your pelvis toward your face, and then release it. It doesn't sound like much, but this very subtle movement has a wonderful effect on the spine, warming it and getting it moving freely. If you have a stiff back, doing about 20 of these will generally loosen things up.

### Leg Stretch

![leg stretch](./@imgs/Exercise_imgs/leg_stretch.jpg)

Begin to work your legs by lifting them perpendicular to the floor, either one at a time or both together.

From the pelvic tilt position, lift one leg off the floor and aim the sole of your foot at the ceiling. Keep the other foot on the floor or bring it up to join the first one.

If straightening your legs is a challenge, it's fine to keep them bent. They also don't have to come to fully perpendicular; lift them as high as is comfortable for you. Stretching a strap around the sole of your foot may make this position more comfortable.

Once your leg is lifted, begin to strongly flex and then point your foot. Notice how these contrasting positions feel different all the way up your leg. You are beginning to stretch the hamstrings, feet, ankles, calves, and fronts of the shins.

### Eye of the Needle Pose

![eye of the needle pose](./@imgs/Exercise_imgs/eye_of_the_needle_pose.jpg)

Remaining on your back, cross your right ankle over the opposite knee for the eye of the needle pose (Sucirandhrasana). Since you are just getting started, you can keep your left foot on the floor, especially if you have tight hips.

If you want a bigger stretch, draw your left knee toward your body. Go easy since your hips may be stiff at first. Once you finish on one side switch legs to loosen up the other side.

### Easy Pose

![easy pose](./@imgs/Exercise_imgs/easy_pose.jpg)

For easy pose (Sukhasana) come up to sit in a comfortable cross-legged position. Place one or two folded blankets under your seat so that your knees are lower than your hips. Do a few neck rolls here.

First, let your chin drop toward your chest. Then roll your chin over to the left shoulder, circle the head back, then bring the chin to the right shoulder. Continue circling slowly, moving through any areas of tightness, for about five rotations. Then do an equal number of rotations in the opposite direction.

If you have trouble with your neck, skip the part where you let the head drop back and just move the chin forward from ear to ear instead.

### Eagle Arms

![eagle arms](./@imgs/Exercise_imgs/eagle_arms.jpg)

While staying seated in easy pose, take the arm position for eagle pose (arms crossed, bent, and parallel to the floor). This gives you a really nice stretch across the shoulder blades and center of the back, an area that is otherwise hard to stretch.

If you do the position with the right arm on top first, make sure to spend equal time with the left arm on top.

### Easy Twist

![easy twist](./@imgs/Exercise_imgs/easy_twist.jpg)

Keep your legs in easy pose and twist to the right, bringing your left hand to your right knee and the right hand behind your back. Take your gaze gently over your left shoulder, then twist to the left, bringing the right hand to your left knee and the left hand behind your back. Remember that this is just a warm up, so this shouldn't be your deepest twist.

This is also a good place to take your easy pose into a forward bend. Since you've been sitting cross-legged for a while, switch the position of your legs so that the opposite leg is in front. You can continue to sit here until class starts or continue with a few more stretches if you have the inclination.

### Cat-Cow Stretch

![cat-cow stretch](./@imgs/Exercise_imgs/cat-cow_stretch.jpg)

If you still have some time, do a few rounds of cat-cow stretches (on all fours alternating arching and rounding your spine). These will further loosen the spine.

Since you are doing this on your own, take care to synchronize your body to your breath, letting the breath initiate the movement. Begin each motion in your tailbone, letting it ripple up the spine until your head is the last thing to move.

### Downward Facing Dog

![downward facing dog](./@imgs/Exercise_imgs/downward_facing_dog.jpg)

You may want to come into a downward facing dog (Adho Mukha Svanasana), primarily to stretch out the legs one last time. Pedal the heels up and down here to lengthen the calves and hamstrings.

### Child's Pose

![childs pose](./@imgs/Exercise_imgs/childs_pose.jpg)

Child's pose (Balasana) is always a good addition to a warm-up routine. Though often thought of simply as a resting pose, child's pose also offers a nice stretch for the hips and thighs and gives you a chance to turn your attention inward in preparation for your upcoming class.

### Goddess Pose

![goddess pose](./@imgs/Exercise_imgs/goddess_pose.jpg)


Many people like to await the start of class in goddess pose (Supta Baddha Konasana)—a standing wide-legged squat—to further open the hips, foregoing the previously mentioned poses. If this is your preference, by all means, do it.

You can also come into the seated version of the pose (cobbler's pose) or just return to easy pose for a few minutes until your class begin

## Toe Touches

Not only does this exercise help you warm up your back, it stretches some of the muscles around it, such as the hamstrings, which may also be tight.

HOW TO DO IT: Stand with your feet together without locking your knees. Reach your arms up overhead and look up. Fold forward and reach your hands toward the ground. At the same time, push your hips back and shift your weight into your heels. When you feel that you can't reach any lower, roll up slowly and reach your arms up. Do 15 of these.

<iframe width="640" height="360" src="https://www.youtube.com/embed/pb2Pk23jUXo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Rock-Back Rotation

This exercise prevents your lower back from moving too much, emphasizing rotation of the middle segment of your spine, known as the thoracic spine. This segment of the spine is much better at rotating than the lumbar spine, according to a 2008 study in the Journal of Neurosurgery: Spine.

HOW TO DO IT: Start on all fours with your hands under your shoulders and knees under your hips. Rock your butt back onto your heels, keeping your hands planted. Put your right hand behind your head and turn your shoulders and head to the right as far as possible as you exhale. Come back to the center and switch sides. Do 10 rotations on each side.

<iframe width="640" height="360" src="https://www.youtube.com/embed/GAZYujygIVI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Ladders

In this exercise, you'll reach your arms up and pretend you're climbing a ladder to practice bending your spine side-to-side. 

HOW DO TO IT: Stand with a tall posture and reach your arms high up. Reach with your arms, one at a time, alternating each time. The goal is to reach as high as possible moving your shoulders and spine to help you reach higher. Perform 10 reaches with each arm.

<iframe width="640" height="360" src="https://www.youtube.com/embed/dQALZT1RK80" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## World's Greatest Stretch

Not only does this exercise help you warm up your back, it also stretches your hips and shoulders. 

HOW DO TO IT: Take a large, lunging step forward with your right foot. Bend the right knee to drop down into a lunge position. Place the left hand on the ground. Turn your shoulders and torso toward the right leg and reach your right arm up toward the ceiling. Perform five repetitions on each side.

<iframe width="640" height="360" src="https://www.youtube.com/embed/yLOOqGzET5E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Spiderman Stretch With Rotation

Stretch your hips, back and shoulders with this all-encompassing stretch.

HOW TO DO IT: Start in a push-up position. Plant your left foot next to your left hand. Turn to the left and raise your left arm up toward the ceiling. Return to a push-up position. Repeat on the right side. Do 10 repetitions on each side.

<iframe width="640" height="360" src="https://www.youtube.com/embed/xYDc5CphM6A?ecver=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
