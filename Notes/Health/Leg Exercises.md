# Legs Exercises

Your glutes are a pretty powerful muscle group. In fact, they include the largest muscle in your body (the gluteus maximus), along with the smaller gluteus minimus and medius. Your glutes also make up an important facet of your [core](https://www.mindbodygreen.com/articles/best-core-exercises)—essential to helping you move seamlessly through everyday life. So yeah, they're pretty incredible.

To help strengthen this vital body zone, we've rounded up some of our favorite bodyweight glutes exercises to try at home. Work through a few of your favorites for a glute-centric routine, mix them into your [leg workout](https://www.mindbodygreen.com/articles/bodyweight-leg-exercises), or try one out when you want to light up this magical muscle group:

## Squats

### Hammer Curls Squat

![Hammer Curls Squat](./@imgs/Exercise_imgs/hammer_curls_squat.jpeg)

Stand with your legs straight (but not stiff or locked) and knees aligned under the hips. Your arms are at your side with a dumbbell in each hand, the weights resting next to the outer thigh. Your palms are facing the thighs, thumbs facing forward, and shoulders relaxed.

1. Bend at the elbow, lifting the lower arms to pull the weights toward the shoulders. Your upper arms are stationary and the wrists are in line with the forearms.
1. Hold for one second at the top of the movement. Your thumbs will be close to the shoulders and palms facing in, toward the midline of your body.
1. Lower the weights to return to the starting position.
1. Make hammer curls more challenging by adding a squat. This helps you work your legs and glutes while also working your arms. After lifting the weights to the shoulders, drop into a squat position. Hold briefly, stand back up, and return the weights to your side.

### Squat Press

![Squat Press](./@imgs/Exercise_imgs/sp.jpg)

- Sink into a squat with your knees in line with your feet, holding dumb-bells by your shoulders
- As you stand up press the weights directly overhead, then reverse the movement back to the start

### Squat and Pulse

This squat variation relies on a principle called time under tension, increasing the time you’re in the hardest part of the exercise. By keeping tension on the quads, glutes, and hamstrings, you’ll build even more lower-body strength and endurance.

![Squat and Pulse](./@imgs/Exercise_imgs/Squat_and_Pulse.jpeg)

1. Plant your feet slightly wider than shoulder-width apart.
Look straight ahead and bend at your hips and knees, keeping your knees aligned with your toes.
1. Lower your body until your thighs are parallel to the floor, ensuring your back remains at a 45- to 90-degree angle to your hips.
1. Push through your heels to extend your legs slightly, lifting yourself just a few inches out of the squat.
. Bend your knees again to return to the full squat.
1. Continue this pulsing motion, alternating between slightly extending your legs and returning to the full squat position, for 12 reps.

### Dumbbell Squat

![Dumbbell Squat](./@imgs/Exercise_imgs/ds.jpg)

1. Stand with your feet shoulder-width apart with your toes turned out slightly and your core muscles braced
1. Hold the dumbbells by your sides and lower until your thighs are parallel with the ground
1. Don't round you back and keep your knees in line with your feet
1. Push back up through knees

Sets | Reps | Rest
-- | -- | --
4 | 10-12 | 75 Seconds

### Sumo Squats

![Sumo Squats](./@imgs/Exercise_imgs/sumo-squat.jpg)

- Take your feet wider than hip-width apart, with your toes turned slightly outwards.
- Keep your chest up and open, looking straight ahead while holding your hands out horizontally in front of your chest.
- Lower down into a squat so that your thighs come parallel to the floor.
- Squeeze through your heels and glutes to come back up to the original standing position with wide legs. 

Do 12 reps

### Pistol Squats

![Pistol Squats](./@imgs/Exercise_imgs/pistol-squat.jpg)

- Stand in front of a box, chair, sofa or equivalent, with your feet shoulder-width apart.
- Place your weight onto your right foot and lift your left foot off of the floor.
- Begin to bend your right knee to lower yourself down to sit on the chair or box. Go slow to control the movement.
- Push through your right heel to lift yourself back to standing. 

Do 12 reps 

### Plie Squats

![Plie Squats](./@imgs/Exercise_imgs/plie-squat.jpg)

- Take your legs out very wide into a plie position, with your toes turned out as wide as they can go.
- Tuck your tailbone under so that there is no arch through your back.
- Bend your knees and lower down into a squat while keeping your back straight – ensuring that your knees don't fall inwards.
- Squeeze through your legs to come back to standing.  

Do 12 reps

Rest for 60 seconds after each round. Do a total of 3 rounds.

### Sumo Squat to Upright Row

![Sumo Squat to Upright Row](./@imgs/Exercise_imgs/Sumo-Squat-to-Upright-Row.webp)

- Grab your dumbbells and stand up, taking a wide stance with your toes pointed out. Hold the dumbbells in front of you with your palms facing towards you.
- Squat down until you’ve reached about 90 degrees, keeping your knees tracking over your toes and your back flat with a proud chest (left).
- Squat back up and as you get near to the top, begin the upright row. Pull with your elbows nice and high, maintaining a soft wrist until your hands reach your upper chest (right). Lower the dumbbells to the start position and repeat from the squat.

### Dumbbell Thruster

![Dumbbell Thruster](./@imgs/Exercise_imgs/Dumbbell_Thruster.webp)

- Rack the dumbbells at shoulder height, with palms facing in and elbows raised
- Squat down, keeping your chest up
- Power up and, as soon as you reach the top part of your squat, thrust the dumbbells overhead into a full press
- Bring the dumbbells back to the rack position

### Front-Rack Squat

![Front-Rack Squat](../@imgs/Exercise_imgs/front-rack_squat.avif)

- Clean a pair of dumbbells up onto your shoulders. Take a breath and brace your core
- Sink your hips back and bend at the knees, squatting down until the crease of your hips passes below your knee
- Drive back up explosively and repeat.

### Dumbbell Deadlift

![Dumbbell Deadlift](../@imgs/Exercise_imgs/dumbbell_deadlift.avif)

- With your dumbbells on the floor just outside your feet, hinge down and grip them with soft knees and back as flat as possible
- Squeeze your lats, grip your weights and stand upright, ‘pushing the ground away’ with your feet
- Take a deep breath and slowly reverse the movement, returning your weights to the ground, keeping them close to your body throughout
- Your arms should be hanging straight throughout this movement – think of them as hooks.

### Squat and Reach

![Squat Reach](./@imgs/Exercise_imgs/squat_reach.jpg)

Adding an overhead reach to a squat challenges the core while engaging and toning the upper back.

1. Begin with your feet slightly wider than hip width apart and toes pointed slightly outward.
1. Keeping your weight in your heels, and sit back into your deep squat. Make sure your knees do not go beyond your toes.
1. Holding your squat, raise both of your arms overhead. Hold this position for a moment, then return to standing while lowering your arms to your sides. This completes one rep.

Do 15 reps.

### Romanian Deadlift

![Romanian Deadlift](./@imgs/Exercise_imgs/romanian_deadlift.jpeg)

Similar to the good morning exercise, this classic weight-lifting move is highly effective for targeting the hamstrings and glutes.

1. Hold one dumbbell by gripping the top bell with both hands. Hold it in front of your legs, and plant your feet shoulder-width apart.
2. Pull your shoulder blades down and back, lifting your chest slightly, and gently engage your core.
3. Inhale and bend your knees slightly, then hinge forward from your hips, letting the dumbbell slide down the front of your thighs and halfway down your shins.
4. Keep your chest lifted and ensure that your head stays aligned with your spine. You should feel tension in your hamstrings.
5. Once you reach halfway down your shins, push through your heels and engage your glutes and hamstrings to extend your knees and hips, returning to standing.
6. Repeat for 12 reps.

### Goblet Sumo Squat

![Goblet Sumo Squat](./@imgs/Exercise_imgs/goblet_sumo_squat.jpeg)

The goblet sumo squat promotes better hip mobility through a wider stand and strengthens the lower body, emphasizing the inner thighs more than other squat variations.

1. Hold a dumbbell or kettlebell with both hands directly in front of your chest, and plant your feet slightly wider than hip-width apart, toes pointing slightly outward.
2. Inhale and bend at your hips and knees, making sure your knees track over your toes.
3. Continue bending your knees until your thighs are parallel to the floor, maintaining a back angle of 45 to 90 degrees to your hips.
4. Exhale and push through your heels, extending your knees to stand back up and return to standing.
5. Repeat for 30 seconds or 12 reps, whichever comes first.

### Squat 

![Squat Exercise](./@imgs/Exercise_imgs/squat_glute_exercise.gif)

1. Start in a standing position. Bring your feet out wider than hip-distance apart. Toes turn out to 1 and 11 on a clock.
1. Lower down, keeping knees over heels. Reach your butt back, and flip tailbone toward the sky. Drag your shoulder blades down your back. Separate the knees away from each other, don't let them knock in, and activate the outer glutes.
1. Engage glutes to lift back up to start. That's one rep. Repeat for 2 minutes.

### Jump squat 

![CJ Frogozo - Jump Squat](./@imgs/Exercise_imgs/cj-frogozo-jump-squat.gif)

1. Start in your squat position.
1. At the bottom of the squat, squeeze your glutes, press into your heels, then roll through your feet and propel upward off your toes.
1. Land softly on your feet, then use the momentum from landing to move into your next squat. That's one rep. Continue for 1 minute.

### Wide Squat 

![CJ Frogozo - Wide Leg Squat](./@imgs/Exercise_imgs/wide_leg_squat_exercise.gif)

1. Stand with your feet wider than hip-width apart, and turn your toes out to 10 and 2 on a clock.
1. Bend your knees and lower down halfway and freeze. Knees should stack right over heels, and heels press into the ground. Tailbone is heavy. Keep shoulders and hips in the same line. Pull your waist in, drop your shoulders.
1. From here, sink as low as you can. If your knees start to pop in, actively press them toward the wall behind you. Feel the outer glutes engage.
1. Bring your arms out to the sides. Drop your shoulder blades down your back, pull the waist in, drop the collarbone, and sit a little lower.
1. Continue lifting and lowering for 3 minutes while including movement variations with your arms. 

## Lunges

### Lunge with Back Row

![Lunge with Back Row](./@imgs/Exercise_imgs/lwbr.jpg)

1. Hold weights in each hand, step your right foot forward and your left foot back, keeping both heels on the floor and feet pointing straight ahead. Bend right knee until it is over your right ankle. Lower your chest toward your thigh, bringing your arms perpendicular to the floor, keeping your back flat.
2. Straighten your right leg, row your elbows back and squeeze your shoulder blades together, keeping torso angled slightly forward. Return to start position. Repeat 10 to 15 times for each leg.

Sets | Reps | Rest
-- | -- | --
2 | 10-15 | 45 Seconds

### Multi-Direction Lunge

![Multi-Direction Lunge](./@imgs/Exercise_imgs/mdl.jpg)

- Lunge forwards, keeping your back upright, until both knees are bent at 90°
- Return to the start then lunge to the side, keeping your leading knee in line with your foot and your torso upright
- Return to the start then lunge backwards at a 45° angle, keeping your knee in line with your leading leg

### Reverse Lunge

![Reverse Lunge](./@imgs/Exercise_imgs/reverse_lunge.gif)

- Stand with feet together. Take a controlled lunge (or large step) backward with your left foot.
- As you lunge back with your left foot, drive your left arm forward to maintain your balance.
- Lower your hips so that your right thigh (front leg) becomes parallel to the floor and your right knee is positioned directly over your ankle. Keep your left knee bent at a 90-degree angle and pointing toward the floor. Your left heel should be lifted.
- From the ground, drive your left knee up coming into a standing position with your left leg lifted at a 90-degree angle. Simultaneously drive your right arm up to maintain your balance.
- If it's too hard to come into to perform the knee drive from the lunge, step your left foot in to meet your right, then raise your left knee up.
- This completes one rep.

Do three sets of 12 reps on each leg.

### Reverse Lunge to Torso Rotation

![Reverse Lunge to Torso Rotation](./@imgs/Exercise_imgs/Reverse-Lunge-torso-Rotation.webp)

- Stand at the top of your mat, feet around shoulder-width apart, holding one dumbbell with both hands (left).
- Lunge your right leg back and as you lower down, rotate over your left leg, keeping your spine as straight as possible (right).
- Reverse the rotation to face front, lift your back leg up to the top of the mat, and repeat on the other side.

### Reverse Lunge to Scaption (Front Raise)

![Reverse Lunge to Scaption Front Raise](./@imgs/Exercise_imgs/Reverse-Lunge-to-Scaption.webp)

- Stand at the top of your mat, feet around shoulder-width apart and dumbbells held by your side (left).
- Place one leg back into a reverse lunge, and as you do so lift your arms up out in front of you, keeping your hands neutral, palms facing in, lifting to around shoulder height with hands slightly wider than shoulder-width (right).
- Push up from the bottom of the lunge, bringing your back leg to the start position, lowering your arms as you do. Repeat with the other leg.

### Dumbbell Reverse Lunge

![Dumbbell Reverse Lunge](./@imgs/Exercise_imgs/Dumbbell_Reverse_Lunge.webp)

- Stand with your feet slightly apart, your back upright and the dumbbells by your sides
- Step back into a lunge, bending your back leg so that your back knee nearly touches the floor
- Keep your torso upright throughout the move and make sure your front knee is over your front toe
- Push off the back foot to return to the start
- Repeat with the other leg

### Reverse Lunge and Kick

![Reverse Lunge and Kick](./@imgs/Exercise_imgs/reverse_lunge_kick.jpg)

This is a great move for warming up the entire body. It's a dynamic stretch for both the hip flexors and the hamstrings.

1. Step back with your left foot, coming into a deep lunge and bending both knees to 90 degrees.
1. Press the right heel into the ground as you push off with your left foot, kicking your left leg to touch your left toes to your right hand.
1. With control, return to the lunge position. This completes one rep.

Do 15 reps on each leg.

### Alternating Reverse Lunge

![Alternating Reverse Lunge](./@imgs/Exercise_imgs/alternating_reverse_lunge.jpeg) 

Looking for a knee-friendly lower-body exercise? This lunge variation improves balance, coordination, and lower-body strength while reducing stress on the knees.

1. Stand with your feet shoulder-width apart and engage your core.
2. Inhale and take a large step back with your right foot. As you place your right foot on the floor, bend both knees to about 90 degrees, distributing your weight evenly between both legs. Ensure your front knee is aligned over your ankle and your back knee hovers just above the floor.
3. Exhale as you extend both knees, shifting your weight back onto your left foot, and step your right foot forward to return to the starting position.
4. Inhale and take a large step back with your left foot, repeating the same motion.
5. Continue alternating between your right and left legs for 16 reps total (8 on each leg).

### Lunge with Toe Tap 

![Helen Phelan - Lunge with Toe Tap](./@imgs/Exercise_imgs/helen-phelan-lunge-with-toe-tap.gif)

1. Start by standing. Step one leg forward and bend that knee slightly to get into a lunge position. Keep your knee directly over your ankle. Try to shorten your foot rather than let your arch collapse.
1. Tuck your pelvis and hinge your hips forward. Create a long line from your head to the heel of your back foot.
1. With 90% of your weight in the front leg, bring your back leg forward and tap your toe next to your opposite foot. Keeping the rest of your body still, return the foot to its starting position.

### Lunge with Chest Lift 

![Helen Phelan - Lunge with Chest Lift](./@imgs/Exercise_imgs/helen-phelan-lunge-with-chest-lift.gif)

1. Start in a lunge position.
1. Engaging your core and keeping your spine neutral, hinge your chest forward, and slowly lower until your torso is nearly parallel with the ground.
1. With control, rise back to your starting position.

### Lunge with Heel Lift 

![Helen Phelan - Lunge with Heel Lift](./@imgs/Exercise_imgs/helen-phelan-lunge-with-heel-lift.gif)

1. Start in a [lunge](https://www.mindbodygreen.com/articles/side-to-side-lunges) position.
1. Keeping the rest of your body still, lift the heel of your front foot until you're balancing on your toes.
1. With control, slowly return your foot to the starting position.

### Side-to-Side Lunges 

![CJ Frogozo - Side to Side Lunges](./@imgs/Exercise_imgs/side_to_side_lunge.gif)

1. Bring your feet wider than hip-width apart, with your toes facing forward.
1. Bend one knee and shoot your hip creases back; flip your butt cheek to the sky.
1. Feel a big stretch in your inner thigh, then switch to the other side.
1. Continue for 3 minutes, and incorporate some arm movements throughout.

### Curtsy Lunge to Squat 

![CJ Frogozo - Curtsy Lunge To Squat](./@imgs/Exercise_imgs/cj-frogozo-curtsy-lunge-to-squat.gif)

1. Start in a standing position. Bring your feet out wider than hip-distance apart, with toes pointed at 1 and 11.
1. Lift one knee up next to your body, then cross that leg behind your opposite leg.
1. Press your back toes into the ground, and bend your knees. Send your hips back, tailbone up, waist in, and shoulders down.
1. Lift your back knee back up, place your foot back down into a squat position, then lower down into a squat.
1. At the top of your squat, lift the opposite knee up and repeat on the other side. That's one rep. Continue for 3 minutes.

### Box Lunge 

![Dino Malvone - Box Lunge](./@imgs/Exercise_imgs/dino-malvone-box-lunge.gif)

1. Start in a standing position. Bring your feet parallel and hip-width distance apart. 
1. Step one foot back, bend both knees, and hold. Stack your front knee over your heel, and drop your back knee directly below your hip. Don't dip very low if you're experiencing any knee discomfort. 
1. Push deeply into your front heel to activate your glutes, and add a slight hinge forward to deepen the sensation. About halfway through, you can add a knee lift to get the heart rate up slightly.
1. Continue for 3 minutes (1.5 minutes on each side).

## Other

### Reverse Nordic Curl

![Reverse Nordic Curl](./@imgs/Exercise_imgs/reverse_nordic_curl.jpeg)

1. Start in a tall kneeling position with the tops of your feet facing the ground (toes un-tucked).
2. Slowly lower yourself backward, bringing your glutes closer to your heels while simultaneously keeping a straight line between your knees, hips, and shoulders.
3. The goal is for your glutes to successfully touch your heels, but go back as far as you can.
4. Then, by pushing the tops of your feet into the ground, return to the start position using your quadriceps and hip flexors.

#### Tip

If you struggle with recurring knee injuries, start out with low doses and gradually build up with higher volumes/rep ranges, according to Noel. “It's important to assess tolerance to the movement before jumping right in and doing high volumes,” he says.

### Hinge and Open

Unlock the power of your posterior (chain)! This exercise targets the posterior chain while improving hip mobility, glute activation, and strengthening your lower back and hamstrings.

![Hinge and Open](./@imgs/Exercise_imgs/Hinge_and_Open_Side.jpeg)

1. Begin kneeling with your knees hip-width apart and your arms extended in front of your chest at shoulder height, palms facing inward.
1. Inhale and slowly lower your torso back toward your heels, hinging only at the knees and keeping your spine in a neutral position.
1. Exhale as you open your arms out to the sides, ensuring they stay in line with your shoulders.
1. Inhale as you bring your arms back in front of your chest.
1. Exhale and elevate your torso back to the starting position.
1. Repeat for 10 reps.

### Step Tap

This seemingly simple and straightforward exercise will sneak up on you with a solid cardio burnout! Fortunately, it's a low-impact move that also helps improve coordination and balance.

![Step Tap](./@imgs/Exercise_imgs/Step_Tap.jpeg)

1. Stand with your feet shoulder-width apart and clasp your hands in front of your chest. Bend your knees slightly.
1. Keeping your right knee slightly bent, step your left foot straight out in front of you.
. Return your left foot to the starting position, then step your right foot straight out in front of you, keeping your left knee slightly bent.
1. Continue alternating between your left and right foot for 60 seconds.

### Single-Leg Balance Touch

![Single-leg Balance Touch](./@imgs/Exercise_imgs/single_leg_balance_touch.jpg)

This exercise really works the deep glutes while challenging your core and sense of balance.

1. Begin standing with your arms overhead with all your weight on your left foot.
1. Keeping your spine long, reach forward, bending your left knee, and touch both hands to the ground. Keep your abs engaged to keep your torso stable.
1. Lower your right leg down while lifting your torso, bringing your arms overhead to complete one rep.

Do 15 reps on each side.

### Leg Balance Warrior 3

![Leg Balance Guerrero Trois](./@imgs/Exercise_imgs/leg_balance_warrior.jpg)

This exercise fires up your core by challenging your balance. As you move in and out of the pose, you will be working the back of your body, too.

1. Stand on your left foot with your right leg lifted to 90 degrees and your right knee bent.
1. Reach your torso forward as you lengthen your right leg behind you. Reach your arms overhead for balance as your torso and leg come parallel to the floor. Keep your left knee slightly bent.
1. Hold this position for a moment, and reach through your right heel to engage the back of the right leg.
1. Moving in one piece, bring your right leg forward, and return to standing upright. This completes one rep.

Do 10 reps, then switch sides.

### Good Morning

![Good Morning](./@imgs/Exercise_imgs/good_morning.jpeg)

Like the name implies, this move says, "good morning" to your glutes and hamstrings! This exercise strengthens your posterior chain (the muscles along the back of your body), improves the hip-hinge movement, and supports lower-back strength, Wells says.
  
1. Stand with your feet hip-width apart.
2. Place your hands behind your head and pull your shoulder blades
down and back, opening up your chest slightly. This is your starting
position.
3. Inhale and bend your knees slightly, then hinge forward from your
hips, keeping your chest lifted and your spine aligned with your
head.
4. As you hinge forward, engage your core and maintain a straight
back. You should feel tension in your hamstrings.
5. Exhale and push through your heels, using the strength of your glutes and hamstrings to straighten your hips and return to the
starting position.
6. Repeat the movement for 30 seconds,focusing on form and controlled motion.

### Standing Glute Kickback

![Standing Glute Kickback](./@imgs/Exercise_imgs/standing_glute_kickbacks.jpeg)

Though compound moves (those involving more than one muscle) give you more bang for your buck, isolation exercises like this one give your glutes a little extra kick. By zeroing in on the gluteus muscles, this exercise helps improve hip stability, promote better posture, and fire up your lower body for the workout to come.

1. Stand with your feet hip-width apart. Step your left foot slightly behind you.
2. Exhale and lift your left foot off the mat, squeezing your glute as you kick your heel straight back behind you while keeping your leg extended and foot flexed, toes facing forward.
3. Inhale as you lower your right foot back to the starting position, maintaining control.
4. Repeat the movement on the same side for 15 seconds.
5. Switch sides and perform on your left leg for 15 seconds.

### Weighted Glute Bridge

![Weighted Glute Bridge](./@imgs/Exercise_imgs/weighted_glute_bridge.jpeg)

This exercise strengthens the glutes and lower back while helping to build core stability and reduce lower-body imbalances.

1. Lie on your back, bend your knees, and place your feet hip-width apart.
2. Carefully place a dumbbell on your hip bones, holding it securely.
3. Exhale and press your heels into the mat while activating your
glutes, lifting your pelvis off the floor until your body forms a straight line from your chin to your knees, resting on your shoulders.
4. Inhale and lower your pelvis back to the starting position with control.
5. Repeat for 30 seconds or 12 reps, whichever comes first.

### Fire Hydrant

![Fire Hydrant](./@imgs/Exercise_imgs/fire_hydrants.jpeg)

Though it has an odd name and you may feel a bit awkward performing it, the fire hydrant exercise is a great way to target the glute medius, helping stabilize the hips and improve overall leg strength and mobility.

1. Begin on all fours, knees below your hips and your hands below your shoulders.
2. Keeping your knee bent, lift your left leg out to the side while ensuring your hips and shoulders stay square and parallel to the floor.
3. Inhale and lower your left leg back to the starting position, but keep your knee hovering just above the mat.
4. Repeat for 30 seconds on each side.

### Fire Hydrant and Extend

Show your hips some love and wake up your gluteus medius to prepare for the rest of this workout. This exercise targets your glutes and outer thighs, improving hip mobility while enhancing lower body strength and stability.

![Fire Hydrant and Extend](./@imgs/Exercise_imgs/Fire_Hydrant_and_Extend.jpeg)

1. Begin on all fours with your knees under your hips and your hands under your shoulders.
1. While keeping your right knee bent, lift your right leg out to the side and then extend your leg fully until your thigh is parallel to the floor.
1. Hold the extended position for 1 to 2 counts, engaging your glutes and core.
1. Slowly return your right leg to the starting position.
1. Repeat for 30 seconds on each side.

### Narrow Bridge Lift Knee Fan 

![Narrow Bridge Lift Knee Fan](./@imgs/Exercise_imgs/narrow-bridge-lift-knee-fan.gif)

1. Lie on your back. Place your feet together so the inside edges of your feet are touching, knees are touching, and inner thighs are squeezing together. You should almost be able to touch the heels with your fingertips.
1. [Activate your core](https://www.mindbodygreen.com/articles/how-to-engage-your-core), keep your ribs knit together, and reach your tailbone toward the backs of your knees. Shoulders stay wide and down your back, and you'll soften your jaw and upper body. Squeeze your knees together as though you're holding a hundred-dollar bill between them.
1. Begin to fan your knees out and in while maintaining a deep push into your heels. Drop your back closer to the ground if you begin to feel tension in your lower back.
1. Continue this for 1 minute.

### Single-Leg Tabletop Triceps Pushup 

![Helen Phelan - Single-Leg Tabletop Tricep Pushup](./@imgs/Exercise_imgs/helen-phelan-single-leg-tabletop-tricep-pushup.gif)

1. Start on all fours, in a tabletop position. Stack the shoulders right on top of the hands, and your hips right over your knees.
1. Extend one leg out, keeping it at hip height. Be sure the hips stay parallel. Keep your elbows pointed toward your knees.
1. Inhale as you bend your elbows, and bring your chest toward the floor. Go as far down as you can; try to line your nose up with your fingertips. Press the opposite shin into the ground.
1. Engage your core, and slowly lift your chest back up to start. Keep your chest open, but don't arch your back. Repeat for 8 breaths.

### Glute Kickback Pulse

Now that your glutes are warmed up, you'll want to fire them all the way up. A powerful glute activation move that isolates and strengthens the glutes, promoting better hip stability and muscle endurance.

![Glute Kickback Pulse](./@imgs/Exercise_imgs/Kneeling_Glute_Kickback.jpeg)

1. Start on all fours with a resistance band looped around your lower thighs.
. Exhale as you extend your right leg backward and upward until it's in line with your spine, keeping your foot pointed.
1. Inhale and lower your right leg slightly, then elevate it back into the full glute kickback position, initiating the movement from your hip.
1. Repeat the movement for 5 pulses, inhaling for 5 pulses and exhaling for 5 pulses.
1. Complete 15 seconds on each leg.

### Donkey Kick 

![Donkey Kick Exercise](./@imgs/Exercise_imgs/donkey-kick-exercise-side-a.gif)

1. Get on all fours and come down to your forearms. Lift your armpits away from the floor and shift your weight into your upper body.
1. Lift one leg up and hold. Bring your leg to a 90-degree angle, flex the heel, and square off your hips.
1. Pull your waist in, and lift the ribs off the floor.
1. Then lower your leg to the ground and lift it back up. That's one rep. Continue for 2 minutes.

### Diamond 

![Diamond Exercise](./@imgs/Exercise_imgs/diamond_exercise.gif)

1. Lie on your side. Come down onto your forearm. Bend your knees.
1. Lift out of your waist, lift out of your shoulder, and lift your heels. Keep your heels or toes glued together. Then lift and lower your knee.
1. That's one rep. Continue to lift and lower for 2 minutes.

### Bridge Lift 

![Bridge Lift Exercise](./@imgs/Exercise_imgs/bridge_lift_exercise.gif)

1. Lie on your back. Place your feet hip-distance apart. You should almost be able to touch the heels with your fingertips.
1. Pull your waist in, press through the heels, and hover your glutes just a few inches off the ground. Keep your waist pulled in, keep the ribs knitted in—don't puff the chest; keep it low. Keep the shoulder blades on the ground. Squeeze the glutes a lot; reach the tailbone toward the backs of the knees.
1. Lift the glutes up by squeezing them and pressing into the heels. Then lower back down. to the starting position. That's one rep. Continue for 2 minutes.

### Side Seat Lift 

![Dino Malvone - Side Seat Lift](./@imgs/Exercise_imgs/dino-malvone-side-seat-lift.gif)

1. Start by lying on your side using your forearm as a kickstand (forearm parallel to the front edge of your mat). 
1. Push into your forearm to come out of your shoulder, and then lengthen your top leg so that it reaches 1 inch longer (think: roll your top hip down).
1. With your top leg reaching long, you'll add a small lift and lower, activating your side seat.
1. Continue for 3 minutes (1.5 minutes on each side).
