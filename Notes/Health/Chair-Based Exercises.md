# Chair-Based Exercises

## Chair Dips

![chair dips](./@imgs/Exercise_imgs/c-chair-dip.gif)

> **Target**: Triceps, chest, shoulders

"The chair dip exercise helps in developing the triceps, and that will assist you in exercises like the chest press and push-ups," says Leventhal.

- While still sitting in a stationary chair, grip the seat at the forward edge of the chair. Then scoot your butt forward until you're supported only by your arms.
- You can keep your legs bent and feet flat on the floor, or extend your legs so your weight is on your heels.
- Keeping your back flat and your core engaged, slowly bend your elbows to lower your body. Stop when your arms are parallel to the floor.
- Reverse the move by pushing yourself back up to the starting position.
 
## Half Lotus

![half lotus](./@imgs/Exercise_imgs/c-half-lotus.gif)

> **Target**: Hips

"This move helps open the hips," Leventhal says, noting it's especially important if you sit frequently or for long periods. "Doing these a few times a day will give you a better squat, and they're also great for reducing low-back pain caused by tight hips."

- Sit tall in your chair with your right foot planted firmly on the floor in front of you, knee bent 90 degrees.
- Lift your left foot off the floor and place the outside of your left ankle on your right knee.
- Flex your left ankle and lean forward at your hips — keeping your back flat throughout — until you feel a stretch in your left hip.
- Hold for 30 seconds, then switch legs.
 
## Air Chair

![air chair](./@imgs/Exercise_imgs/c-air-chair.gif)

> **Target**: Core, legs, glutes

You don't even have to move to strengthen your lower body and core. Perkins recommends that you just "sit" a few inches above your chair for an effective isometric exercise.

- Rise from a seated position to standing, with your feet shoulder-width apart.
- Keeping your back flat and core engaged, push your hips back, bend your knees, and lower your body as if you're going to sit down. Stop just before you make contact with the chair so you're "sitting" in the air.
- Hold for at least 30 seconds.
 
## Squat to Squeeze

![squat to squeeze](./@imgs/Exercise_imgs/c-squat-squeeze.gif)

> **Target**: Glutes, hip flexors, quads

Rising and lowering using squat position is already great for your form, but Perkins likes kicking it up a level. "Twice every hour, I activate my glutes to innervation, which means stimulating the nerves to wake up the muscle fibres."

- From a seated position, plant your feet on the floor and push through your heels to stand quickly.
- Squeeze your glutes hard for 10 reps, pausing for a one-count during each contraction.
- Slowly lower back down to the chair, either all the way to seated or to "air chair" position before repeating.
- Make this a cardio move by working quickly for 10 reps.
 
## Chair Leg Lift

![chair leg lift](./@imgs/Exercise_imgs/c-chair-leg-lift.gif)

> **Target**: Glutes, hamstrings

Using the back of a chair can add a healthy dose of Barre power to your chair workouts. There are numerous moves you can do, but Perkins favors a backward extension to work the lower body.

- Stand with your feet hip-width apart behind a stationary chair, holding onto the back with one or both hands.
- Extend your right leg behind you as far as possible without arching your back or letting your hips tilt too far forward.
- Squeeze your glutes as you reach the top of the move, and hold for two seconds, then lower your leg back down to the floor.
- Repeat for 10 reps, switch sides and repeat.

Via [Openfit](https://www.openfit.com/chair-exercises)