# Back Exercises

![Back Exercises Leading Image](./@imgs/Exercise_imgs/back_exercises_leading_image.jpg)

Admit it, you haven't been super kind to your back lately. If you work behind a computer, chances are your back muscles and posture could use a bit of love. That's where some handy at-home moves come in.

Not only are back exercises important to keep your [upper body](https://www.mindbodygreen.com/articles/arms-and-abs-workout) strong, stable, and balanced—but they can also help prevent injury and posture issues in your future. Below, you'll find some of our favorite at-home back exercises, demonstrated by body-neutral Pilates instructor [Helen Phelan](https://www.mindbodygreen.com/wc/helen-phelan). It's time to make moves toward better mobility, posture, and overall [core strength](https://www.mindbodygreen.com/articles/best-core-exercises).

## Plank Single-Arm Row

![Plank Single-Arm Row](./@imgs/Exercise_imgs/psar.jpg)

1. Start in the up position of a press-up while holding two dumbbells
1. Row one of the dumbbells up to your chest while keeping your hips level
1. Return to the start and do the move on the other side

Sets | Reps | Rest
-- | -- | --
3 | 12-14 each side | 60 Seconds

## Dumbbell Bent-Over Row

![Dumbbell Bent-Over Row](./@imgs/Exercise_imgs/dbor.jpg)

1. Start with your back straight, your feet shoulder-width apart, your core braced and your shoulders retracted
1. Lean forward from the hips, not the waist, and bend your knees slightly. Hold the dumbbell just outside your knees
1. Pull the weights up until your elbows are bent at 90º. Squeeze your shoulder blades together at the top of the move
1. Lower the dumbbells slowly back to the start

Sets | Reps | Rest
-- | -- | --
4 | 10-12 | 75 Seconds

### Alternative Instructions

![Dumbbell Bent-Over Row](./@imgs/Exercise_imgs/dbora.jpg)

1. Stand with your feet shoulder-width apart, arms at your sides, a dumbbell in each hand. Bend your knees slightly, pushing your hips back, and bend forward without rounding your back. Hold the weights straight down from your shoulders with your wrists facing back and your knuckles facing forward. Pull the weights straight up to the sides, bending your elbows, while keeping your torso in the same position. You are only moving your arms, not the rest of your body. Squeeze your shoulder blades together as your elbows reach to the ceiling.
2. Slowly lower the weights to the start position. Be sure to keep your back flat. Do one to two sets of 15 repetitions each.

Sets | Reps | Rest
-- | -- | --
2 | 15 | 60 Seconds

## Dumbbell Single Arm Overhead Squat

![Dumbbell Single Arm Overhead Squat](./@imgs/Exercise_imgs/dsaos.jpg)

1. Hold a dumbbell in each hand, one dumbbell weighing twice as much as the other. Stand with your feet shoulder-width apart and toes pointed straight ahead.
2. Hold the light dumbbell overhead in your non-dominant hand, with the heavier dumbbell between your legs, keeping both arms straight. Push your hips back and lower yourself until your upper thighs are parallel to the floor, holding the lighter dumbbell straight over your shoulders, tightening the back of your shoulder and upper back muscles. Keep your abs pulled in tight.
3. Rise back to the start position and done to two sets of 15 repetitions each. Switch arms and repeat.

Sets | Reps | Rest
-- | -- | --
2 each arm | 15 | 60 Seconds

## Dumbbell One Point Row

![Dumbbell One Point Row](./@imgs/Exercise_imgs/dopr.jpg)

1. Holding a dumbbell in each hand, balance your weight on your left foot, bending forward at the hips and raising your right leg so it forms a T with your torso and left leg. Your chest and right leg are parallel to the floor and your shoulders are square to the floor.
2. Hold the weights below your shoulders, arms straight (remain balanced on your left leg). Pull the weights straight up to your sides, keeping shoulders square to the floor, and squeeze your shoulder blades together.
3. Slowly lower weights to start (you are still balanced on one leg) and repeat for eight repetitions. Switch legs and repeat for 8, two sets.

### Alternative Version (*Hip Extension with Reverse Fly*)

Rather than pulling the weights to your side, lift arms straight out forming a T at your shoulders, squeezing shoulder blades together. This sounds very difficult but one can try alternating with the above exercise.

Sets | Reps | Rest
-- | -- | --
2 each leg | 2 | 60 Seconds

## Lat Pullover

![Lat Pullover](./@imgs/Exercise_imgs/lp.jpg)

1. Lie on your back on bench or floor, feet flat, knees bent; hold a weight in both hands or in each hand over chest straight up. Lower the weight straight back behind your head until your arms are in line with your torso and parallel to the floor. If not using a bench, lower to just above the floor but not touching.
2. Keeping your arms straight, pull your arms to start position over your chest. As you reach the start position, think about tightening your lats. Do one to two sets of 15 repetitions each.

Sets | Reps | Rest
-- | -- | --
2 | 15 | 45 Seconds

## Dumbbell Deadlift

![Dumbbell Deadlift](./@imgs/Exercise_imgs/Dumbbell_Deadlift.webp)

- Stand tall with soft knees, shoulders back and a pair of dumbbells at your sides
- Press your hips back, keep your chest up and slowly lower the dumbbells towards the ground
- Once the dumbbells are midway down your shins, pause, squeeze your glutes and return to the starting position

## Hang Cleans

![Hang Cleans](../@imgs/Exercise_imgs/hang_cleans.avif)

- Hold your dumbbells at your side, hinge at the hips, bend your knees and allow the weights to swing back behind your legs
- Stand back up explosively and use the momentum to pull the dumbbells up on to your shoulders
- Allow the dumbbells to drop back between your legs and repeat. Avoid excessive rounding of the lower back throughout.

## Romanian Deadlift

![Romanian Deadlift](../@imgs/Exercise_imgs/romanian_deadlift.avif)

- Stand tall holding a pair of dumbbells at waist height
- With a slight bend in the knees, push your hips back and slowly lower the bells towards the ground, pinning your shoulders down and maintaining a flat back
- Push your hips back until you feel a stretch in your hamstrings, pause and explosively return to an upright position.

## Kickstand Deadlift

![kickstand_deadlift](../@imgs/Exercise_imgs/kickstand_deadlift.webp)

- Stand with your feet hip-width apart, holding a dumbbell in your right hand in front of your thigh. Take a small step back with your left foot, resting your toes on the ground like a kickstand. The majority of your body weight is in your right foot, and your left foot is providing some balance support.
- Engage your core by bracing as if someone is about to punch you in the stomach. This is the starting position.
- Keep your chin tucked, back flat, and weight in your right foot. On an inhale, hinge forward from the hips to lower the dumbbell to the floor. Keep the dumbbell close to your body, as if you're shaving your leg. Continue lowering until you feel a stretch in your hamstrings, your chest is parallel with the floor, or your back starts to round.
- On an exhale, slowly shoot the hips forward to return to standing, pulling the dumbbell up your leg. That's one rep.

Do 8 to 12 reps. Switch sides; repeat.

## Hindu Press-up

![Hindu Press-up](../@imgs/Exercise_imgs/hindu_press-up.avif)

Assume a strong press-up position with your hands shoulder-width apart. Walk your hands backwards until your hips are in the air, almost above your hands
- Flex at the elbows, lowering your nose to the ground between your hands before shifting your weight forward. Push the ground away, straightening your arms as you drop your hips to the floor
- Reverse this movement until your hips are back in the air.

## Back Extensions

![Back Extensions](./@imgs/Exercise_imgs/be.jpg)

1. Lie face-down on floor, arms straight but your sides, palms up, forehead facing the floor. The tops of your feet should be flat against the floor.
2. Slowly lift your head and shoulders off the floor, lifting your arms, squeezing your shoulder blades together and hold for five seconds. Lower to start position and do one to two sets of 15 repetitions each.

Sets | Reps | Rest
-- | -- | --
2 | 15 | 45 Seconds

## Forward Fold

![Forward Fold](./@imgs/Exercise_imgs/forward_fold.jpg)

Here's how to do it: If you're sitting down, lengthen your legs out in front of you, bringing them as close together as you can. Sit up tall, rock side to side on your sit bones until you're comfortable. Raise your hands overhead and slowly drop your chest toward your thighs. Relax the neck and lengthen through your fingertips as you reach for your toes. The goal is to keep your knees down. Let the back round out and relax as you get deeper into the stretch. You might not be able to touch your toes, but you should be able to lean forward and reach that relaxed position.

## Plank

Position 1: 
![Plank](./@imgs/Exercise_imgs/plank_01.jpg)
Position 2:
![Plank](./@imgs/Exercise_imgs/plank_02.jpg)

* Lie on your front propped up on your forearms, knees and toes.
* Lift your pelvis and knees off the floor, creating a horizontal line from shoulders to ankles. Hold for 5 to 10 seconds.

> Repeat 8 to 10 times

### Watch Points

* Do not allow your low back to dip down during exercise
* Keep your deep abdominals activated throughout
* Use a mirror to check your alignment during the exercise

## Side Planks

Position 1:
![Side planks](./@imgs/Exercise_imgs/side_planks_01.jpg)
Position 2:
![Side Planks](./@imgs/Exercise_imgs/side_planks_02.jpg)

* Lie on your side supporting your upper body weight on your forearm and keeping a straight line from your shoulders to your ankles. Keep your neck long and shoulder blades down.
* Raise your pelvis upwards until you achieve a straight line from shoulders to ankles. Hold for 5 to 10 seconds.

> Repeat 8 to 10 times

### Watch Points

* Keep your pelvis forward throughout the exercise
* Keep your deep abdominals activated throughout
* Use a mirror to check your alignment during the exercise

## Swimming

Position 1:
![Swimming](./@imgs/Exercise_imgs/swimming_01.jpg)
Position 2: 
![Swimming](./@imgs/Exercise_imgs/swimming_02.jpg)

* Kneel on all fours, with knees under hips and hands under shoulders. Make sure you keep a small, inward curve in your low back. Keep your neck long, your shoulder blades down and your elbows locked.
* Reach one arm forwards off the floor whilst simultaneously sliding the opposite foot along the floor away from the body. Continue to reach and raise the leg off the floor as far as control can be maintained through your pelvis and low back.

> Repeat alternating arms and legs 8 to 10 times

### Watch Points

* Keep your neck long and deep abdominals activated throughout
* Keep the correct curvature throughout the spine during the exercise
* Lengthen and raise the leg by activating the buttocks
* Imagine a glass of water on your back throughout the exercise

## Bridging

Position 1:
![Bridging](./@imgs/Exercise_imgs/bridging_01.jpg)
Position 2:
![Bridging](./@imgs/Exercise_imgs/bridging_02.jpg)

* Lie on your back on a mat or the carpet. Place a small, flat cushion or book under your head. Bend your knees and keep your feet straight and in line with your hips. Keep your chest and ribcage relaxed and your chin gently tucked in.
* After raising the spine off the floor, straighten one leg and slowly lower it, just as you did for Bridging level 1.

> Repeat, alternating legs, 8 to 10 times.

### Watch Points

* Keep your deep abdominals activated throughout
* Keep both sides of your pelvis level and do allow one side to dip whilst straightening one knee

## Prone Swan Lift 

![Helen Phelan - Prone Swan Lift](./@imgs/Exercise_imgs/helen-phelan-prone-swan-lift.gif)

1. Start by lying on your stomach. Glue your feet together, or spread them apart if you have any back pain. Open your arms into a cactus position.
1. Exhale, drop your belly button away from the floor, and press your pubic bone into the ground. Lift your head, chest, and arms upward. Squeeze your [shoulder](https://www.mindbodygreen.com/articles/pilates-arms-abs-workout) blades together.
1. Inhale as you lower your body to the ground.

## Swan With Shoulder Squeeze 

![Helen Phelan - Swan With Shoulder Squeeze](./@imgs/Exercise_imgs/helen-phelan-swan-with-shoulder-squeeze.gif)

1. Start by lying on your stomach. Glue your feet together, or spread them apart if you have any back pain. Open your arms into a cactus position. Lift your head, chest, and arms upward. This is your starting position.
1. Exhale as you bring your elbows toward your ribs, squeezing your shoulder blades together.
1. Inhale as you reach your arms out in front of you.

## Half Pushup Hover 

![Helen Phelan - Half Pushup Hover](./@imgs/Exercise_imgs/helen-phelan-half-pushup-hover.gif)

1. Get into a plank position, with your wrists directly under your shoulders. [Engage your core](https://www.mindbodygreen.com/articles/how-to-engage-your-core).
1. Bend your elbows, and slowly lower until your body is halfway toward the floor. Hold for a few breaths, then lower to the ground.

## Swan to Pushup 

![Helen Phelan - Swan To Pushup](./@imgs/Exercise_imgs/helen-phelan-swan-to-pushup.gif)

1. Start by lying on your stomach. Glue your feet together, or spread them apart if you have any back pain. Open your arms into a cactus position.
1. Exhale, drop your belly button away from the floor, and press your pubic bone into the ground. Lift your head, chest, and arms upward. Squeeze your shoulder blades together.
1. Inhale as you lower your body to the ground.
1. From the ground, press your body up into a [plank position](https://www.mindbodygreen.com/articles/forearm-plank) as you exhale. You can keep your knees on the ground, or rise all the way up onto your toes. Inhale as you lower back down.

## Plank With Shoulder Retraction 

![Helen Phelan - Plank With Shoulder Rotation](./@imgs/Exercise_imgs/helen-phelan-plank-with-shoulder-rotation.gif)

1. Get into a high-plank position, with your hands stacked right under your shoulders. Engage your core to stabilize your plank.
1. Inhale, as you allow your chest to lower and your shoulder blades to press together.
1. Exhale as you lift your body back to a plank position, spreading your shoulder blades apart. Repeat for 5 breaths.

## Bird Dog 

![Helen Phelan - Bird Dog](./@imgs/Exercise_imgs/helen-phelan-bird-dog.gif)

1. Get on all fours, and place shins flat on the ground. Release your shoulder blades away from your ears.
1. Inhale as your reach your right arm and left leg up, stretching away from your body. As you lift, don't arch your back or bring your leg too high. Tuck your pelvis and hug your abdominals in; [squeeze your glutes](https://www.mindbodygreen.com/articles/glute-exercises) for stability.
1. Exhale as your return your arm and leg back to the ground.
1. Repeat on the opposite side. Continue for 8 breaths.

## Bird Dog in Knee Hover 

![Helen Phelan - Bird Dog In Knee Hover](./@imgs/Exercise_imgs/helen-phelan-bird-dog-in-knee-hover.gif)

1. Start on all fours. Float your knees a couple of inches off the ground, and hold.
1. Without shifting your hips, inhale and extend your right arm forward. Exhale as you crunch your abs and pull your elbow toward your torso. Reach it back out, then return it to the ground.
1. Inhale as you extend your left leg out; squeeze through the glutes. Try not to lift your leg up much higher than your torso. Then exhale to curl your spine and crunch your body inward.
1. Repeat with the opposite arm and leg. Continue for 8 breaths.
