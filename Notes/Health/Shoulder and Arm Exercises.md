# Shoulder & Arm Exercises

## Knee Lift with Lateral Raise

![Knee Lift with Lateral Raise](./@imgs/Exercise_imgs/klwlr.jpg)

1. Stand tall with weight in each hand, arms to your sides. Lift your right knee until it reaches your hip level while lifting your arms straight out to the side to form a T at your shoulders. Hold for two seconds, making sure your belly button is pulled back toward spine and then lower to start position. Repeat 10 to 15 times for each leg.

Sets | Reps | Rest
-- | -- | --
1 per leg | 10-15 | 45 Seconds

## Alternating Dumb-Bell Curl

![Alternating Dumb-Bell Curl](./@imgs/Exercise_imgs/alternating_dumb_bell_curl.jpg)

1. Stand with the dumbbells by your sides, your shoulders back and your core braced.
2. Life the weight with your palm facing forwards. As you lift, keep your elbow tucked in to your side and don't rock back and forth to use momentum.
3. Pause at the top of the move and lower slowly back to the start.

Sets | Reps | Rest
-- | -- | --
5 | 6-8 each side | 90 seconds

## Concentration Curl

![Concentration Curl](./@imgs/Exercise_imgs/concentrationcurl.avif)

*Refer to picture for instructions*

The incline curl works the long head of the biceps muscle and encourages a larger range of motion in comparison to standard bicep curls. Due to the nature of the incline set up, more resistance is placed on the biceps in the stretched position, therefore increasing the potential for muscle growth.

**Suggestion**: 6 reps and 4 sets (*16kg*)

## Incline Bicep Curl

![Incline Bicep Curl](./@imgs/Exercise_imgs/incline-bicep-curl.avif)

*Refer to picture for instructions*

Short for concentration curls, the 'con' curl is a bodybuilding staple. They work the long and short head of the biceps. As you work the arms one at a time, the move can increase mind to muscle connection.

**Suggestion**: 6 reps and 4 sets (*16kg*)

## Zottman Curl

![Zottman Curl](./@imgs/Exercise_imgs/zottman_curl.jpg)

1. Stand tall holding a dumbbell in each hand at arm’s length by your sides, palms facing forward (underhand grip).
1. Keeping your elbows tucked and locked by your sides, curl the weights toward your shoulders.
1. Flip your grip 180 degrees (to overhand), lower the weights back down to your sides, and then flip your grip again (to underhand) to return to the starting position.

## Seated Dumbbell Shoulder Press

![Seated Dumbbell Shoulder Press](./@imgs/Exercise_imgs/sdsp.jpg)

1. Sit on a bench and hold the dumbbells at shoulder height with your palms facing forwards
1. Press the weights straight up before lowering them to the start 

Sets | Reps | Rest
-- | -- | --
3 | 12-14 | 60 seconds

## Upright Row and Shrug

![Upright Row and Shrug](./@imgs/Exercise_imgs/uras.jpg)

1. Start with the dumbbells in front of your thighs
1. Raise your elbows to the sides to lift the dumbbells up to your chest then raise your shoulders

Sets | Reps | Rest
-- | -- | --
4 | 10-12 | 75 Seconds

## Arnold Press

![Arnold Press](./@imgs/Exercise_imgs/arnold.jpg)

1. Start holding dumbbells with your palms facing you and your elbows out to the front
1. Rotate your palms to face forward as you press the weights up
1. Pause at the top with your palms facing forward, then reverse the movement back to the start

Sets | Reps | Rest
-- | -- | --
4 | 10-12 | 75 Seconds

## Hammer Curls

![Hammer Curls](./@imgs/Exercise_imgs/hammer_curls.jpeg)

Stand with your legs straight (but not stiff or locked) and knees aligned under the hips. Your arms are at your side with a dumbbell in each hand, the weights resting next to the outer thigh. Your palms are facing the thighs, thumbs facing forward, and shoulders relaxed.

1. Bend at the elbow, lifting the lower arms to pull the weights toward the shoulders. Your upper arms are stationary and the wrists are in line with the forearms.
1. Hold for one second at the top of the movement. Your thumbs will be close to the shoulders and palms facing in, toward the midline of your body.
1. Lower the weights to return to the starting position.

## Close-Grip Dumbbell Floor Press

![Close-Grip Dumbbell Floor Press](./@imgs/Exercise_imgs/cgdfp.avif)

1. Lie flat on the ground, your knees bent, pushing your feet into the floor. 
1. Keeping your upper arms tight to your body, press the weights up above your chest, locking out your elbows.
1. Lower them slowly until your upper arms are resting on the floor, before explosively pressing back up, squeezing your triceps hard at the top. 
1. Keep your elbows from flaring throughout.

## Lying Tricep Extension

![Lying Tricep Extension](./@imgs/Exercise_imgs/lte.avif)

1. Lie flat on a bench or the floor with a pair of dumbbells locked out overhead. 
1. Bend at the elbows, slowly lower the bells towards your head, while keeping your upper arms locked in place. 
1. Stop just short of the bells, touching the floor before extending back up explosively. Repeat.

## Rolling Tricep Extension

![Rolling Tricep Extension](./@imgs/Exercise_imgs/rte.avif)

1. Perform **Lying Tricep Extension** 
1. Rotate arms 90° forward and perform **Close-Grip Dumbbell Floor Press**

## Tricep Push-Up

![Tricep Push-Up](./@imgs/Exercise_imgs/tricep_push_up.jpg)

1. Start in a plank position with your arms and legs straight, shoulders above the wrists.
1. Keeping your upper arms parallel, bend your elbows lowering your chest until your shoulders are in line with your elbows. The elbows should touch your ribcage.
1. Straighten the arms to complete the rep. Rest your knees on the floor if you need to.

Do as many push-ups as you can in 45 seconds. Take a 15-second rest.

## Dumbbell Tricep Extension

![Dumbbell Tricep Extension](./@imgs/Exercise_imgs/dumbbell_triceps_extension.jpg)

Stand tall holding a dumbbell in each hand over your head, with arms straight. Keeping your chest up, core braced and elbows pointing up, lower the weights behind your head, then raise them back to the start.

Sets **4** | Reps **12-15** | Rest **45sec**  

## Reverse Flies

![Reverse Flies](./@imgs/Exercise_imgs/reverse_flies.jpg)

You will need a pair of dumbbells for this exercise. Hold the dumbbells in your hands, stand straight and keep your feet hip-width apart. Bend your knees a little. Now bend frontwards and keep your arms straight down with the palms facing up.

Slowly raise both the arms on the sides and squeeze the blades of the shoulder as you do so. Go back to the normal position. Do 10 to 12 such repetitions.

## Bent over Double Dumbbell Lat Row

![Bent Over Double Dumbbell Lat Row](./@imgs/Exercise_imgs/bent_over_double_dumbbel_lat_row.jpg)

1. Starting Position: Grab a pair of dumbbells, preferably closer to the 10lbs range, stand with feet hip-width apart and slightly bend at the knees as you hinge at the hips. With a straight back, roll the shoulders slightly back creating a high chest and let the dumbbells hang in front of your knees, palms facing each other. This is starting position.
1. Take a deep breath, brace your core, and draw the weights back by squeezing through the shoulder blades and pulling through the lats (think bra bulge area). Your head should be down, and your biceps should be grazing against your ribcage as you pull your elbows behind you. Pause at the top of the movement for two seconds as you exhale.
1. Slowly lower the dumbbells back to starting position. Continue for your preferred number of reps.
1. In the second image, you will see the arms extended, this is one of the best tricep workouts for women.

## YWT

![YWT](./@imgs/Exercise_imgs/ywt.jpg)

### Y

1. Start by lying face down on the floor. First squeeze your shoulder blades together then raise your arms at a 45-degree angle above your head (imagine that your arms form the top of the Y shape and your torso the bottom.)
1. Point your thumbs to the ceiling and keep your elbows straight throughout the exercise.
1. Slowly raise your arms as high as possible by squeezing your shoulder blades together by flexing your lower trapezius and upper back. Bring your head 1-2cm off the floor, keeping it neutral (i.e. looking at the floor) at all times.
1. Hold for about three to five seconds at the top and slowly lower your arms to the floor.

### W & T

1. Repeat the exercise as you have done above, but this time with your elbows bent and below the level of your shoulders.
1. Repeat as above but with your arms in the “T” position

For each of the Y’s W’s and T’s repeat a total of 10 repetitions, 3 times (once a day). Add light dumbbells (1-2Kg) as you increase strength in the lower trapezius. Alternatively progress to a stability ball.

## Half-Kneeling Woodchopper

![Half-Kneeling Woodchopper](./@imgs/Exercise_imgs/Half-kneeling-woodchopper.webp)

- Begin in a half-kneeling position, both legs bent at 90 degrees, with the back knee on the ground and toes tucked under. Hold one dumbbell in both hands by the side of the rear leg, making sure to rotate your torso – this is the start position (left).
- Lift your arms up and across your body until just above shoulder height, keeping your elbows slightly bent and twisting with your whole torso, not just your arms (right).
- Reverse and repeat for 12 reps and then switch sides.

## Dumbbell Alternating Lateral Raise

![Dumbbell Alternating Lateral Raise](./@imgs/Exercise_imgs/Alternating-Lateral-Raise.webp)

- Stand up with feet around shoulder-width apart, holding your dumbbells by your side.
- With soft elbows, raise one arm out in front of you, and the other out to the side, lifting until your hands are around shoulder height (left).
- Lower back down to the start and repeat, alternating which arm goes forward and which goes to the side (right). Continue for 12 reps.

## Single-arm Dumbbell Snatch

![Single-arm Dumbbell Snatch](./@imgs/Exercise_imgs/Single-arm_Dumbbell_Snatch.webp)

- Using an overhand grip, grab a dumbbell in one hand
- Lower your hips to the floor until your knees are bent at 90° and the dumbbell is resting on the floor
- Quickly pull the dumbbell toward the ceiling while simultaneously extending your knees and hips, and raising your body on the balls of your feet. Keep the dumbbell close to your body
- As the dumbbell reaches its highest point, quickly rotate your elbow under the weight and extend your arm. The dumbbell will rest over the top of your shoulder with the palm facing away from your body

## Bicep Curl to Shoulder Press

![Bicep Curl to Shoulder Press](./@imgs/Exercise_imgs/Bicep-Curl-to-Shoulder-press.webp)

- Hold your dumbbells by your side with palms facing forwards and curl up nice and slowly to the top of your shoulders (left).
- Start to open your chest by pulling the dumbbells out to the side, rotating your palms until they are facing forwards, then press the dumbbells up to the top position (right).
- Reverse the movement back to the start position and repeat.

## Standing Alternating Shoulder Press

![Standing Alternating Shoulder Press](../@imgs/Exercise_imgs/standing_alternating_shoulder_press.webp)


- Stand with your feet hip-width apart, holding a dumbbell in each hand racked on your shoulders. Your palms should be facing away from you, so your upper body looks like a goalpost.
- Bring your elbows in front of your body slightly to protect the shoulder joint. Engage your core by bracing as if someone is about to punch you in the stomach. This is the starting position.
- On an exhale, press the dumbbell in your right hand up toward the ceiling, straightening but not locking your right arm. Keep your core engaged to avoid overextending the lower back.
- On an inhale, slowly bend your right elbow and lower the dumbbell back to the starting position. That's one rep.
- Repeat with your left arm.

Do 8 to 12 reps each, alternating sides.

## Push Press

![Push Press](../@imgs/Exercise_imgs/push_press.avif)

- Clean a pair of dumbbells onto the front of your shoulders. Take a breath and brace your core
- Dip at the knees and using your legs to help drive the dumbbells up overhead
- Lower them under slow control to your shoulders and repeat.
