# Yoga and Pilates Exercises

![Yoga Header Image](./@imgs/Exercise_imgs/yoga_1.jpg)

## Yoga
### Upper & Lower Back Workout - Strengthen Back Muscles with Yoga

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/POSWXpJB5sY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### How to Strengthen Lower Back

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/O_y_2Yjar44" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Downward Dog

![Downward Dog](./@imgs/Exercise_imgs/downward_dog.jpg)

1. Begin on your hands and knees. Align your wrists directly under your shoulders and your knees directly under your hips. The fold of your wrists should be parallel with the top edge of your mat. Point your middle fingers directly to the top edge of your mat.
2. Stretch your elbows and relax your upper back.
3. Spread your fingers wide and press firmly through your palms and knuckles. Distribute your weight evenly across your hands.
4. Exhale as you tuck your toes and lift your knees off the floor. Reach your pelvis up toward the ceiling, then draw your sit bones toward the wall behind you. Gently begin to straighten your legs, but do not lock your knees. Bring your body into the shape of an "A." Imagine your hips and thighs being pulled backwards from the top of your thighs. Do not walk your feet closer to your hands — keep the extension of your whole body.
5. Press the floor away from you as you lift through your pelvis. As you lengthen your spine, lift your sit bones up toward the ceiling. Now press down equally through your heels and the palms of your hands.
6. Firm the outer muscles of your arms and press your index fingers into the floor. Lift from the inner muscles of your arms to the top of both shoulders. Draw your shoulder blades into your upper back ribs and toward your tailbone. Broaden across your collarbones.
7. Rotate your arms externally so your elbow creases face your thumbs.
8. Draw your chest toward your thighs as you continue to press the mat away from you, lengthening and decompressing your spine.
9. Engage your quadriceps. Rotate your thighs inward as you continue to lift your sit bones high. Sink your heels toward the floor.
10. Align your ears with your upper arms. Relax your head, but do not let it dangle. Gaze between your legs or toward your navel.
11. Hold for 5-100 breaths.
12. To release, exhale as you gently bend your knees and come back to your hands and knees.

### Hamstring Lengthener Yin Yoga

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ToIOdDEfcHE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Pilates

### How to do the Hundred ab Exercise

![Pilates Hundred Exercise](./@imgs/Exercise_imgs/pilates_hundred_legs_bent.jpg)

The exercise involves coming into a 'dish' shape on the floor and pulsing your arms up and down 100 times. Yes you read that right – 100 times. 

1. Lie on your back and lift your head and upper back off of your mat, so that only the base of your shoulders is touching the ground. 
1. Lift your legs off the floor and bend the knees into a 90° position, so your calves are parallel with the floor.
1. Place your arms by your sides and extend them out towards your toes. 
1. Pulse your arms up and down by just a few inches, exhaling as you press down.
1. Keep the movements dynamic but controlled while maintaining tension in your arms. 
1. Pulse the arms for 100 reps. You can do this in 10 sets of 10, or do as many as you can and then rest for a few seconds before jumping straight back in. 

![Pilates Hundred Exercise with Straight Legs](./@imgs/Exercise_imgs/pilates_hundreds.gif)

#### Variations for the Hundred ab Exercise

Leg placement is the easiest way to change up the intensity of this exercise. To make pilates hundreds more challenging, keep your legs straight and lift your feet a few inches off of floor. The closer the legs are to the floor, the harder it is. Make sure they aren't so close that you end up losing form by arching your back. 

For an easier variation, bend your legs so your knees point towards the ceiling and place your feet on the floor a hands-width away from your bum. 

#### Training Notes for the Hundred abs Exercise

It's a challenging move, so making sure your form is perfect is crucial. The two most common mistakes include: 

- Dropping the head back
- Letting your head fall back and your chin point towards the ceiling will put make you strain through your neck, shoulders and back. Make sure you keep your chin tucked in towards your chest.
- Arching through the spine

Arching will put pressure on the lower back rather than the abdominals and could cause more pain that it solves. Instead, draw your belly button in towards your spine so your spine is imprinted in your mat.

### Back Strengthening Pilates - Michelle Merrifield

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/yuX7uVOa0sE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Back Strengthening Pilate Steps
#### Right Side

* Pointed, stretched toe life to neutral position, exhale on life (20 reps)
* (*always from previous position unless otherwise stated*) flex calf and then stretch to pointed neutral position on exhale (20 reps)
* Hold in flexed calf position, lift thigh up on exhale (20 reps)
* Place left hand on lower back with palm facing upwards (5 reps)
* Extend left hand, palm facing towards face, pointed stretched leg (10 reps, inhales and exhales) 
* Bring left fist and right knee into chest and the extend back into previous position on exhale (20 reps)
* Extend leg and point toe to floor, raise leg and left arm to neutral position on exhale (20 reps)

#### Left Side

* Repeat right side on opposite side

#### Next Steps

* Return to start position, lift head up on inhale then look down and raise midriff to the ceiling (5 reps)
* Raise right arm on inhale and them tuck arm under left and exhale (10 reps)
* Stay in tucked-under position for 5 reps
* Repeat on other side
* Return to raised arm position and hold for 5 reps
* Grip outer heel with left hand and hold (10 reps, inhales and exhales)
* Grip inner heel with right hand and hold (10 reps, inhales and exhales)
* Repeat on other side
* Sit back down your heels for 10 reps
* Sit in a crossed legged position, place right hand on left knee and turn to the left (5 reps)
* Do opposite side
* Place right hand on mat, bring left hand over head and stretch over to the right side, exhale. Repeat on the other side (10 reps)
* Done!

### Bridge Booty Burn

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WOYVTAhQxD8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Bridge Booty Burn Instructions

* Lay flat on your back with your feet under your knees flat on the floor.
* Exhale and roll up the pelvis to create a straight line from shoulders to hips to knees.
* Lower pelvis an while inhaling and lift back to original position on the exhale. (*20 reps*)
* Repeat the same thing but *pulse*. (*100 reps*)
* Bring your knees together then spread as wide as your shoulders on the exhale. (*50 reps*)
* Return to start position, lower hips push up in two steps, exhaling on each upward motion. Don't touch the floor and keep toes down. (*20resp*)
* While in up position, raise left knee on inhale, extend leg to point straight up on exhale, return to previous position on inhale, and return to ground on exhale. Repeat on other side. (*20 reps*) ![leg raise](./@imgs/Exercise_imgs/leg_raise.jpg)
* Roll down and take a breathe to relax.
* Roll back up and *tuck* the pelvis bone and stomach together. (*20 reps*) ![tuck](./@imgs/Exercise_imgs/tuck.jpg)
* Repeat the above with pulses. (*100 reps*)
* Get into the *leg raise* position and move down, inhale, an inch and then up an inch on exhale. (*20 reps on each side*) ![leg raise hold](./@imgs/Exercise_imgs/leg_raise_hold.jpg)
* Take a moment rest.
* Lift hips then lower slightly, swing hips to the right on exhale and then repeat for the left. (*20 reps*)
* Repeat the above by swinging to each side 4 times. (*10 reps*)
* Move feet on tip toes further towards your body. Pulse your hips up and down. (*100 reps*)
* Return to normal position, place knees and feet together. Kick left leg straight out on exhale, return leg and repeat for the other side. (*20 reps*)
* Place left foot on/over the right knee, thread arms under left leg and clasp the shin for five breathes. Repeat for other side.
* Done!

### Ab Blaster Power Pilates

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/bW-IxuNB4Ow" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Luscious Legs Power Pilates

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oPhK30sjdfw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Standing Cardio Power Pilates

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/YL9FAZZzXaE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![Closing Image](./@imgs/Exercise_imgs/yoga_2.jpg)
