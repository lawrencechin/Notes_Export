# Stability Ball Exercises

## How do you use an exercise ball?

You can use an exercise ball in a number of ways, and since it doesn't take up a lot of space it's an excellent piece of home workout equipment. One common way to use a stability ball is as a substitute for a bench. If you're accustomed to performing exercises with a stable bench, using an exercise ball will require firing up your core, hip, and shoulder stabilizers in a new way, Braun explains. Just keep in mind that because of this extra requirement for stability, you should start by using less weight than you would use with a bench.

Stability balls also allow movement in exercises like ab or hamstring rollouts. And you can use a stability ball as a sort of weight (a very large, light weight) and move it from one side of your body to the other, or pass it between your hands and feet.

Via [Openfit](https://www.openfit.com/exercise-ball-workouts)

## Getting Started

If you're doing exercises on your back, reach down through your legs and hold the ball in place and while bending at the knees, sit down in the middle of the ball. Yep, it's that simple. Once you're seated and as you lower yourself to a reclining position, keep your feet stable on the ground and your core nice and tight. When I say to keep your core tight, I'm not talking about just sucking your tummy in. I mean to 'zip up' your ab muscles. Here's how– starting at your pelvic bone, pull your abdominal muscles towards the middle and go all the way up to your rib cage. This allows you to keep your core stable and still be able to breathe.

If you're doing exercises where you're on your belly; to get into position, get on your knees and place the ball nice and close to your abdomen and then roll forward, once again, keeping your core nice and tight. You'll notice in the pictures below that in the prone position, my body is flat and you can tell that my core is tight. This is so important to avoid any kind of back pain as well as keeping proper balance.

These exercises that we're about to go through are great when done all together for a nice full body workout or you can pick some of them for great additions to workouts you're already doing.

Via [Coach Amber George](https://coachambergeorge.com/stability-ball-exercises/)

## Abs

### Jackknife

![Jackknife](./@imgs/Exercise_imgs/bb_stability-ball_jacknife.gif)

> **Benefits**: This core exercise does double duty by strengthening the hip flexors and crunching your abs.

- Get in a high-plank position with your hands directly underneath your shoulders and your shins on top of a stability ball.
- Brace your core to keep your body in a straight line from head to toes. This is your starting position.
- Squeeze your core and bend your knees to roll the stability ball toward your hands until only your toes are resting on the ball, keeping your hips down as you do so.
- Pause, then slowly straighten your legs back behind you, returning to the starting position.

### Gym Ball Jackknife Twist

![Gym Ball Jackknife Twist](./@imgs/Exercise_imgs/gbjt.jpg)

- Rest your instep on top of the ball with your body in a straight line and your hands directly beneath your shoulders
- Roll your feet over the ball to draw your knees into your chest
- As you do so, twist your knees to one side before twisting them back and returning to the start

### Deadbug

![Deadbug](./@imgs/Exercise_imgs/bb_stability-ball_dead-bug.gif)


> **Benefits**: Deadbug exercises teach your core to work as it was designed to do — keeping your spine stable while your arms and legs do their own thing. This variation cranks it up a notch by requiring an extra ab squeeze to keep the stability ball in place, while also targeting your obliques.

- Lie with your back flat on the floor with your arms extended straight up, your legs bent at 90 degrees, and holding a stability ball between your knees and your hands. This is the starting position.
- Keeping the ball in place with your right hand and left knee, brace your core and slowly lower your right leg and left arm to within six inches of the floor (both should remain in line with your body). Only go as low as you can with your low back pressing in to the floor.
- Reverse the move to return to the starting position, and repeat on your other side.
- Continue alternating sides, performing equal reps on each side.

### Gym Ball Crunch

![Gym Ball Crunch](./@imgs/Exercise_imgs/gbc.jpg)

- Touch your fingers to your temples and lean as far back as you can on the ball
- Contract your abs to curl your chest towards your knees, keeping your lower back in contact with the ball
- Pause at the top of the move to squeeze your abs and lower slowly to the start

### V-Up

![V-up](./@imgs/Exercise_imgs/bb_stability-ball_v-up.gif)

> **Benefits**: Challenge your entire body with this next-level stability ball exercise. It works your core as you pass the ball between your hands and feet, and you have to engage your inner thighs and arms to keep the ball from falling to the ground.

- Lie with your back flat on the floor with your legs extended straight on the floor, holding a stability ball overhead with both hands. Brace your core to minimize any arch in your lower back. This is your starting position.
- Squeeze your abs to lift your arms and legs to place the ball between your calves, creating a "V" position.
- Lower back down to the starting position, but this time with the ball between your legs.
- Repeat the movement, passing the ball back and forth between your hands and legs.

### Russian Twist

![Russian Twist](./@imgs/Exercise_imgs/bb_stability-ball_russian-twist.gif)

> **Benefits**: Improve your core's stability and rotational strength in one simple move that really targets your obliques and transverse abdominis.

- Lie with your upper back on a stability ball and your feet flat on the floor with your knees bent at a 90 degree angle. Brace your core and keep your hips extended so your torso forms a straight line from head to knees.
- Extend your arms straight above your chest and press your palms together. This is your starting position.
- Rotate your torso to roll onto one shoulder as far as you can while raising the other from the ball. Your hips should stay square with the floor.
- Reverse the move to return to the starting position.
- Repeat, rolling onto the opposite shoulder

### Gym Ball Russian Twist

![Gym Ball Russian Twist](./@imgs/Exercise_imgs/gbrt.jpg)

- Start with your shoulders resting on the ball, your body in a straight line and a dumb-bell at arm's above your head
- Twist your torso over to one side until your arms are parallel to the floor
- Twist back to the opposite side, looking in the direction of the dumb-bell

### Gym Ball Reverse Curl

![Gym Ball Reverse Curl](./@imgs/Exercise_imgs/gbrc.jpg)

- Lie on your back with your fingers to your temples and your knees bent at 90°
- Keeping your upper body still, use your abs to curl your backside off the floor

### Rollout

![Rollout](./@imgs/Exercise_imgs/bb_stability-ball_rollout.gif)

> **Benefits**: This seemingly simple move will leave your abs shaking as it tests your core strength and stability.

- Place your hands on a stability ball and kneel with your knees hip-width apart and your toes on the floor for stability.
- Keeping your back flat and core braced, and without moving your knees, slowly roll forward so the ball comes to your forearms, until your body forms a straight line from your head to your knees.
- Pause, then roll back to the starting position.

### Mountain Climbers

> Tip for balance – press down into the ball with your elbows and forearms to keep your balance. 

![Mountain Climbers](./@imgs/Exercise_imgs/SB-Mountain-Climber.jpg)

> This exercise works your lower abs, your arms, and your leg muscles.

- Starting in a steady plank position (on your elbows) on the ball, you will raise one knee up toward the chest, then back, then repeat with the other leg.
- Make sure that you stay up on your toes and keep your core nice and tight.

### Side-Lying V-Up

![Side-Lying V-Up](./@imgs/Exercise_imgs/SB-Side-Lying-V-up.jpg)

> Tip- For balance, lay your opposite arm out flat on the ground beside you (as pictured).

Here's another ab killer, you ready?

- Lying on your side, place the ball between your ankles. You will feel like a dork, but hey, who cares, right?
- While lying on your side, raise your legs and use your hand to reach up for your feet.
- You will see that I'm not lying directly on my hip bone; I am curved a little bit to make sure that I am targeting that abdomen area without killing that hip bone!

## Arms

### Dumbbell Pullover

![Dumbbell Pullover](./@imgs/Exercise_imgs/SB-Dumbbell-Pullover.jpg)

> This exercise works your triceps, rear delts, and just into some of the lat muscle.

- Put one of your dumbbells down on the ground and then hold the one dumbbell with both of your hands.
- Roll down on the ball into the same position as you were with the chest press. Holding the weight between both of your hands, start with your elbows by your side and at a 90(ish) degree angle. Keeping that position pull the weight up over your head (as pictured) then bring back to starting position.

### Gym Ball Reverse Flye

![Gym Ball Reverse Flye](./@imgs/Exercise_imgs/gbrf.jpg)

- Use a light weight and keep your bellybutton on the centre of the ball
- Raise the dumb-bell, keeping your arms in the ten-to-two position and your thumbs pointing towards the ceiling to activate your stabilising muscles
- Pause at the top of the move, then lower the weights slowly

### Skull Crushers

![Skull Crushers](./@imgs/Exercise_imgs/SB-Skull-Crushers.jpg)

> This exercise targets the triceps! Can you say, hello tank top arms? 🙂

- Grab one dumbbell this time and position yourself like a chest press but only holding one dumbbell straight overhead.
- Bending at the elbows, lower the weight carefully to your head then back up. Be sure to hold onto the weight so you don't drop it on your head! Remember, safety first.

## Back

### IYT Raise

![IYT Raise](./@imgs/Exercise_imgs/bb_stability-ball_iyt-shoulder-raise.gif)

> **Benefits**: Fend off the effects of sitting at a computer all day by training your shoulder stabilizers and mid-back muscles with this deceptively difficult exercise.

- Lie facedown on a stability ball with your stomach on a stability ball and your legs extended out straight behind you, digging your toes into the floor for support. Brace your core so that your body forms a straight line from head to heels.
- Let your arms hang straight toward the floor, holding your hands in fists with your thumbs up. Squeeze your shoulder blades down and away from your ears. This is your starting position.
- Leading with your thumbs, raise your arms straight up and close to your ears, keeping your shoulders pressed down. This is the "I" formation.
- Lower your arms down toward the floor, then lift your arms up again, but this time diagonally, making a "Y" formation.
- Lower your arms down toward the floor, then lift your arms straight out to your side to form a "T" formation with your palms facing the floor.
- The cycle of "I-Y-T" is one set.
- Make this move harder by holding a dumbbell in each hand.

### Arm/Back Row

![Arm/Back Row](./@imgs/Exercise_imgs/SB-Arm-Back-Row.jpg)

> This exercise targets the upper back area.

- Grab your dumbbells again and get into the starting position for alt arm and leg raises. Spread your feet wide for good balance and pull that core nice and tight.
- Engaging your trap and lat muscles, bend at the elbow and pull up into a row position then back down as pictured below.

### Prone Back Extension

![Prone Back Extension](./@imgs/Exercise_imgs/SB-Prone-Back-Extension.jpg)

Last, but certainly not least, the prone back extension.

> This targets the back and make sure to keep your core zipped up nice and tight to protect that awesome back of yours!

- Position the ball at your hips, right in the curve and spread your feet nice and wide for balance.
- Crossing your arms in front of your body, raise your body up to parallel then back down.

### Gym Ball Back Extension

![Gym Ball Back Extension](./@imgs/Exercise_imgs/gbbe.jpg)

- Rest your stomach on a gym ball with your fingers to your temples and your feet jammed against a wall for support
- Raise your upper body off the ball. Pause at the top, then return to the start

### Alt Arm/Leg Raises

![Alt arm/leg raises](./@imgs/Exercise_imgs/SB-Alt-Arm-and-Leg-Raise.jpg)

> This exercise works your core strength, balance, butt, arms, and back all at once.

- Roll onto the ball on your belly and position the ball right at the curve of your hips with both feet planted on the floor and both hands planted on the floor as well. It's basically a plank position but with the ball underneath you.
- Now, keeping your core zipped up and your butt engaged, raise your left hand and your right foot off of the ground. You might feel like you're going to roll off the ball, so make sure you have your balance and alternate sides while raising the alternate arm and leg. Example when raising the left leg, you raise the right arm.

## Chest

### Decline Push-Up

![Decline Push-up](./@imgs/Exercise_imgs/bb_stability-ball_decline-push-up.gif)

> **Benefits**: This advanced bodyweight move is a version of a decline push-up that challenges your core just as much as your arms. You should be able to perform regular push-ups with confidence before taking on this exercise. 

- Get in a high-plank position with your hands directly underneath your shoulders and your toes on top of a stability ball. Brace your core and squeeze your glutes to keep your body in one straight line from head to toes for the entire move.
- Bend your elbows to lower your chest toward the ground, keeping your elbows tucked close to your body. They should form a 45 degree angle to your torso when viewed from above.
- Press your arms straight to return to the plank position and repeat.

### Gym Ball Press-Up

![Gym Ball Press-Up](./@imgs/Exercise_imgs/gbpu.jpg)

- Rest your feet on top of the ball, keeping your body in a straight line from head to heels
- Lower slowly by bending at the elbows and push back up powerfully

### Chest Press

![Chest Press](./@imgs/Exercise_imgs/SB-Chest-Press.jpg)

> This works your balance, and your chest muscles!

- Grab a pair of dumbbells for this one! I recommend at least 8 pounds but more typically between 10-20 pound dumbbells, depending on your strength and balance.
- Sit on the ball and roll down until the ball is positioned between your shoulder blades. Keep your butt up and your core tight. It will be very easy for your back and butt to just curve around the ball but remember to keep your butt away from the ball and up.
- Put the dumbbells end to end and then lower the weights while opening up the chest to a parallel then press back up.

## Legs

### Hamstring Curl

![Hamstring Rollout](./@imgs/Exercise_imgs/bb_stability-ball_hamstring-roll-out.gif)

> **Benefits**: Strengthen your hamstrings and glutes with this seemingly simple move, while also engaging your core.

- Lie with your back flat on the floor with the back of your calves on top of a stability ball and your legs straight.
- Brace your core and squeeze your glutes to raise your hips off of the floor so that your body forms a straight line from shoulders to heels. This is your starting position.
- Drag your heels to roll the ball as close to your butt as possible or until your knees form 90-degree angles.
- Pause, then slowly straighten your legs as you roll your feet away from your glutes, returning to the starting position.

### Wall Squat

![Wall Squat](./@imgs/Exercise_imgs/bb_stability-ball_wall-squat.gif)

> **Benefits**: Strengthen your glutes, hamstrings, and quads while building stability through your hips and core. Perform back-to-back reps, or hold each rep as long as possible.

- Stand with your feet hip-width apart and a stability ball between the middle of your back and a wall. Your feet should be slightly in front of your body. This is your starting position.
- Bend your knees to roll your body down the ball until your thighs are parallel to the floor. Your knees should be in line with your middle toes
- Pause, then press through your heels to return to starting position and repeat.
- Make this move harder by holding a dumbbell in each hand.

### Hip Thrust

![Hip Thrust](./@imgs/Exercise_imgs/bb_stability-ball_hip-thrust.gif)

> **Benefits**: This advanced hip thrust variation builds your glutes and hamstrings by training one leg at once, helping to correct any muscle imbalance between legs. Using a stability ball rather than bench hones in on single-leg hip stability even further. Practice with both feet flat on the floor, and then both feet on the ball before graduating to this advanced move.

- Lie with your back flat on the floor and both feet on a stability ball, legs bent at a 90 degree angle. Rest your arms straight on the ground by your sides.
- Lift one foot off the ball and extend it out straight.
- Squeeze your glutes to thrust your hips off the ground. Brace your core so that your torso forms a straight line from head to knees.
- Pause, then slowly lower your hips back to the ground.
- Perform all reps, then repeat on the opposite leg.

### Bridge

![Bridge](./@imgs/Exercise_imgs/SB-Bridge.jpg)

Now, this one is much more simple than exercise two because it doesn't have the hamstring pull but it doesn't make it any less effective!

> This exercise works your butt, balance and core!

- Lying on your back and keeping your core tight, lift your butt off of the ground by pressing down through your feet into the ball then lower back to the floor.

