# Chest Exercises

## Push up with Hip Extension

![Push up with Hip Extension](./@imgs/Exercise_imgs/puwhe.jpg)

1. On your hands and knees, place your hands wider than shoulder width apart. Extend your right leg straight back and pull your belly button up toward your spine, tightening your core muscles.
2. Keeping your leg lifted, lower your chest to the ground until each of your elbows is at a 90° angle then push up. Repeat 10 to 15 times per leg.
3. As you get stronger, increase the angle of your hips. Increase the distance of your knees from your hands until eventually you can perform the exercise with straight legs: one leg lifted, the other positioned on your toes.

Sets | Reps | Rest
-- | -- | --
1 per leg | 10 - 15 | 60 Seconds 

## Supine Brige with Arm Extension


![Supine Bridge with Arm Extension](./@imgs/Exercise_imgs/sbwae.jpg)

1. Sit on the floor with your hands underneath your shoulders, knees bent and feet flat on the ground. Keeping your arms straight, use your legs to push your hips up toward the ceiling until your torso is flat like a tabletop. 
2. Lift your right arm straight up toward the ceiling, rotating your upper body so that it supported by your left arm, keeping your hips lifted. 
3. Lower your right arm to the start position and just slightly lower your hips but don't let them return to the floor. 
4. Repeat with your left arm and do 10 to 15 repetitions for each side.
5. As you get stronger, hold the arms and hips in the upward position for a few seconds.

Sets | Reps | Rest
-- | -- | --
1 per arm | 10 - 15 | 45 Seconds

## Chest Press

![Chest Press](./@imgs/Exercise_imgs/chest_press.jpg)

1. Lie on your back, grab your dumbbells and lift them up to the ceiling. Make sure they are in line with your chest.
1. Keep your knees up and your feet flat on the floor.
1. Bring the dumbbells towards your chest, slow and steady.
1. When you touch the floor with your elbows return slowly to the starting position.
1. This is one rep.

### Repetitions

* 10 slowly chest press exercises
* 10 quickly chest press exercises
* 5 slowly
* Pulse 10 times (half way through)

## Staggered Push Ups

![Staggered Push Up](./@imgs/Exercise_imgs/staggered_push_up.jpg)

Push ups is one of the best upper body workout and it really tones the entire 'armpit fat' area.

Instead of the normal push up, for staggered push up you need to place one arm at the chest level and one arm about 3 inches forward. This will challenge your arms even more.

Do 8 reps on each side.

## Dumbbell Flies

![Dumbbell Flies](./@imgs/Exercise_imgs/dumbbell_flies.jpg)

1. Start from the same position as in the first exercise (chest press).
1. Palms facing in, open it slowly until the upper arm touches the floor then squeeze it up.
1. Control your moves slowly to incorporate every single chest and arm muscle.

### Repetitions

1. 10 slowly fly exercises
1. 10 quickly fly exercises
1. 5 slowly flies
1. Pulse 10 times (half way through)

### Routine

Do the above three exercises like so:

Take a one minute rest between sets and repeat this workout to get rid of armpit fat 2 or 3 times. Do this  2-3 times weekly combined with your total body cardio and strength training.

## Dumbbell Chest Press

![Dumbbell Chest Press](./@imgs/Exercise_imgs/dcp.jpg)

1. Lie on the bench with your knees bent at 90º
1. Hold the weights at chest level with your palms facing inward
1. Press the weights straight up without arching your back

Sets | Reps | Rest
-- | -- | --
3 | 12-14 | 60 seconds

## Press-Up

![Press-Up](./@imgs/Exercise_imgs/press_up.jpg)

1. Start with your hands level with your shoulders, just wider than shoulder-width apart
1. Keep your body in a straight line from head to heels throughout the move
1. Lower your body, making sure you keep your elbows pointing back rather than to the sides

Sets | Reps | Rest
-- | -- | --
3 | 12-14 | 60 Seconds

## Diamond Press-up

![Diamond Press-up](./@imgs/Exercise_imgs/diamond.jpg)

1. Hold your body in a straight line from head to heels and your thumbs and index fingers together to form a diamond shape
1. Lower your body, making sure to keep your elbows pointing back rather than to the sides

Sets | Reps | Rest
-- | -- | --
3 | 12-14 | 60 Seconds

## Hip Bridge to Chest Fly

![Hip Bridge to Chest Fly](./@imgs/Exercise_imgs/Hip-Bridge-to-Chest-Fly.webp)

- Lie on your mat facing up, bend your knees, and bring your heels up towards your bum. Press your dumbbells up and hold them with your arms locked in a neutral grip, palms facing each other.
- Press your feet into the floor and squeeze your glutes to bridge your hips into the air, then hold this position (left).
- Begin to lower your hands out towards the ground, softening your elbows slightly and keeping your arms in line with your chest. Go as low as you feel comfortable, or until your elbows touch the ground (right), then fly your arms back up to A and repeat. Keep your glutes activated and bridged up.
