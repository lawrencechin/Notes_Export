# [Think like a Programmer](https://zellwk.com/blog/think/)

The main gist of the piece is to separate a big task into smaller segments. This will make it easier to manage the task as well as allowing one to get their head around something potentially difficult. The blog post uses the example of attempting to get an elephant into a fridge. I did not like the whimsical silliness of this so have left it out. Onwards to a proper example!

## Let's use a real example
### Breaking down the task

The basic task will be to create a button that, when clicked, shows the user a sidebar. We start by breaking this down into smaller tasks.

1. What is the markup of this button?
2. How should the button look?
3. What happens when the button gets clicked once?
4. What happens when the button gets clicked again?
5. What happens when the button gets clicked a third time?
6. What is the markup of this sidebar?
7. How does the sidebar look when it is shown?
8. How does the sidebar look when it is hidden?
9. How does the sidebar show up?
10. How does the sidebar go away?
11. Should the sidebar show up when the page loads?

Now we answer the questions.

#### What is the markup of this button?

The markup is an `<a>` tag with a class of `.button` (*Ed: Why not an actual button with a markup of a button?*).

``` html
<a href="#" class="button">Click me</a>
```

#### How should this button look?

This button should have the following CSS:

``` css
.btn {
    display : inline-block;
    font-size : 2em;
    padding : 0.75em 1em;
    background-color : #1ce;
    color : #fff;
    text-transform : uppercase;
    text-decoration : none;
}
```

#### What happens when the button gets clicked once? Twice? A third time?

The sidebar appears on the first click whilst vanishing upon the second. A third click will once more show the sidebar and a subsequent click will do the opposite.

#### What is the markup of the sidebar?

The sidebar will be a `<div>` that contains a list of links : 

``` html
<div class="sidebar">
    <ul>
        <li><a href="#">Link Uno</a></li>
        <li><a href="#">Link Dos</a></li>
        <li><a href="#">Link Tres</a></li>
        <li><a href="#">Link Cuatro</a></li>
        <li><a href="#">Link Cinco</a></li>
    </ul>
</div>
```

#### How does the sidebar look when it is shown?

The sidebar should be placed at the right of the window. It needs to be fixed in place so the user sees it. It should be 300px wide…

``` css
.sidebar {
    position : fixed;
    width : 300px;
    right : 0;
    top : 0;
    bottom : 0;
    background-color : #333;
}

.sidebar ul {
    margin : 0;
    padding : 0;
}

.sidebar li {
    list-style : none;
}

.sidebar li + li {
    border-top : 1px solid white;
}

.sidebar a {
    display : block;
    padding : 1em 1.5em;
    color : #fff;
    text-decoration : none;
}
```

#### How does the sidebar look when it is hidden?

The sidebar should be shifted 300px to the right and thus fully offscreen. With this in mind, perhaps more questions come to mind:
1. How do you know whether the sidebar is shown or hidden?
2. How do you style the hidden sidebar?

#### How do you know whether the sidebar is hown or hidden?

When the sidebar is in the hidden state it will have a class of `.is-hidden` applied. Should the sidebar not have this class then it is NOT hidden.

#### How do you style the hidden sidebar?

The `.is-hidden` class has a property of `transform` and `translateX` that will move the sidebar when the class is applied.

``` css
.sidebar .is-hidden {
    transform : translateX( 300px );
}
```

#### How does the sidebar show up?

The sidebar will not appear instantly rather it will emerge from the right into the screen to it's final position. This can be achieved via the `transition` property.

``` css
.sidebar {
    /* other properties */
    transition : transform 0.3s ease-out;
}
```

#### How does the sidebar disappear?

Once the `.is-hidden` class is reapplied the sidebar will return to it's off screen position.

#### Should the sidebar show up when the page loads?

No. The HTML markup should have the `.is-hidden` class applied from the outset and removed only when requested by the user (*by pressing the button!*).

So that's everything taken care of. Congratulations, you are now a fully fledged, thinking programmer. You're ability to handle complex tasks has… ¡espero un momento!

We have taken care of the html and the css what clicking the button doesn't do anything. The sidebar does not show when clicking once nor does it do anything when clicking it a second time. A new question, how do you know when the button has been clicked?

#### How to know when you clicked the button?

Now we introduce javascript and event listeners. We monitor the button for a specific event and take action when that occurs. In this case we are *listening* to a *click* event. We can add an event listener to an element by first identifying the element within the document object model (*DOM*).

``` javascript
const button = document.querySelector( ".btn" );
button.addEventListener( "click", () => {
    console.log( "Button is clicked" );
});
```

#### What happens when the button is clicked once?

When the button is clicked for the first time the `.is-hidden` class is removed using javascript.

``` javascript
const button = document.querySelector( ".btn" );
const sidebar = document.querySelector( ".sidebar" );

button.addEventListener( "click", () => {
    sidebar.classList.remove( "is-hidden" );
});
```

#### What happens when the button is clicked twice?

The obvious thing to do will be to add the hidden class back to the sidebar. We must check if the class exists on the element and either add it if missing or take it away if present.

``` javascript
const button = document.querySelector( ".btn" );
const sidebar = document.querySelector( ".sidebar" );

button.eventListener( "click", e => {
    e.preventDefault();
    if( sidebar.classList.contains( "is-hidden" ))
        return sidebar.classList.remove( "is-hidden" );
    return sidebar.classList.add( "is-hidden" );
});
```

### Refactor and improve

What you set out to achieved was achieved. A hearty pat on the back and swift congratulations. You sit there with a inane grin on your face; clicking that button and marveling at the sidebar appearing and disappearing… But not all is right in sidebar-button land. Just because you completed the task doesn't mean the job is done. Answering questions should lead to more questions that also need answering. And in doing so you may have to change what you've written to facilitate that. And perhaps the code you have written can be written in a better way. And maybe what you have written is perfect and can't possibly be improved…until the rule makers depreciate the code you have used and is no longer deemed best practice. And breathe.

So what questions readily spring to mind when reviewing the code?
1. How do you make this sidebar component accessible to users who have visual disabilities?
2. How do you make this sidebar component easier to use for people with keyboards?
3. Can you improve the code in any way (*vague*)?

Without delving to far into the other questions, the third can be answered quite simply. When looking into the `classList` syntax you may have noticed the `toggle` method. It can be used thusly :

``` javascript
const button = document.querySelector( ".btn" );
const sidebar = document.querySelector( ".sidebar" );

button.addEventListener( "click", e => {
    e.preventDefault();
    sidebar.classList.toggle( "is-hidden" );
});
```

Et voila! We have refactored our code to be more succinct whilst retaining meaning for the casual peruser. 
