# Bots! Bots! Bots!

Whether we realize it or not, we have been interacting with bots, especially voice bots, for some time now. The somewhat annoying voice-prompter of some airline support lines (the ones that have enough voice-recognition ability to hear me yelling “Customer Service!”) are powered by types of bots. [Cookie Monster](http://appleinsider.com/articles/16/03/16/apple-enlists-help-of-cookie-monster-to-highlight-hey-siri-in-new-iphone-6s-ad) taught us how to use “Hey Siri” commands to set timers and play music by voice commands, and recently Google announced its competitor to Siri, the voice-activated [Google Assistant](https://kopitiambot.com/2016/05/19/google-unveils-assistant-a-smarter-more-conversational-voice-assistant/). Love them or hate them, bots are making computers talk back, and they’re getting more and more sophisticated.

Bots integrated with popular chat apps, in particular, are proliferating everywhere:

  - Kik’s bot platform has been running for several years, but [as they state](http://avc.com/2016/04/the-kik-bot-platform), the only entities using it have been advertisers and enterprise companies who were able to put significant effort into custom integrations with Kik.
  - Facebook heralded the arrival of bots available in Facebook Messenger, an app already used by a large user base; find a list of them [here](http://www.engadget.com/2016/04/13/here-are-all-the-facebook-messenger-bots-we-know-about-so-far/).
  - WeChat and [Line](https://www.techinasia.com/line-bots) are similarly opening up bots and bot stores to their users.
  - [Slackbots](http://www.wired.com/2015/08/slack-overrun-bots-friendly-wonderful-bots/) – services already configured out of the box in Slack – can be designed to set up reminders via a few text entries.

Bots, specifically chatbots, are being hailed as the next big thing, replacing the need for downloadable apps.

> I genuinely can’t remember the last time a concept blew up as quickly as bots.
> 
> — Benedict Evans (@BenedictEvans) [April 13, 2016](https://twitter.com/BenedictEvans/status/720369752246386689)

## So what does a bot do?

Technically speaking, bots are a form of artificial intelligence that analyze input and emit output in human language. Per wikipedia:

> Bots are “a computer program which conducts a conversation via auditory or textual methods. Such programs are often designed to convincingly simulate how a human would behave as a conversational partner, thereby passing the [Turing test](https://en.wikipedia.org/wiki/Turing_test).”

Bots automate manual tasks and respond to input, often in a fun, interactive way. Microsoft’s [captionbot.ai](http://captionbot.ai), for example, analyzes images – a seemingly fun activity, but it could be used in security screenings as well. Leveraging artificial intelligence, bots can be ‘taught’ to intelligently react to users’ input. While they can serve useful purposes, bots can also go horribly awry as Microsoft learned with their ‘Tay’ bot that was trained by malicious users to [emit racist and sexist messages](http://money.cnn.com/2016/03/24/technology/tay-racist-microsoft/index.html). The ‘Tay’ fiasco, while unpleasant, taught us that bots can be used for both good and evil – artificial intelligence must be carefully ‘trained’ by humans if it is to move to the next level.

Integrated into an already viable platform with many users, a bot can do things that a normal human chat session or voice command wouldn’t be able to accomplish easily, like check the local weather, order pizza, or hail a cab. Because they are built on top of services with large userbases, third-party [companies](http://www.economist.com/news/business-and-finance/21696477-market-apps-maturing-now-one-text-based-services-or-chatbots-looks-poised) are starting to leverage bots to perform specialized tasks, such as check bank account balances or order plane tickets via text prompts. Bots are becoming available via distribution channels such as Bot Stores like [Kik’s](https://bots.kik.com/#/) or Slack’s.

> Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots Bots
> 
> — Benedict Evans (@BenedictEvans) [April 12, 2016](https://twitter.com/BenedictEvans/status/720006087206785024)

Want to create your own bot? The software behind bots is both becoming more available and more consumable by developers. You can more and more easily create increasingly sophisticated bots using tools such as [wit.ai](http://wit.ai). Slack ships with a sophisticated bot service, [Slackbot](https://get.slack.help/hc/en-us/articles/202026038-Slackbot-your-assistant-notepad-programmable-bot), that is powered by Slack’s [Real Time Messenger API](https://api.slack.com/rtm). RTM in turn uses [WebSockets](https://en.wikipedia.org/wiki/WebSocket) to connect users to bots; WebSockets are a “standard way to open a long-lived bi-directional communication channel with a server over TCP”.

Although the initial splash of hype may have dimmed a little, bots are going to continue to grow. Integrated with other apps, bots reduce the need to download a specialized app to perform tasks – a trend that we’re seeing become more and more popular. If you can order takeout in WeChat or schedule a meeting or reminder from right within Slack, why would you need a separate app to do this task?

If the specialized app landscape is shrinking, bot integration will make the appeal of more widespread apps broader. Clearly the landscape is changing for mobile interactions, and bots are part of the trend. Watch for a bot talking back to you on Slack or in a chatroom in the near future\! Or head over to Miss Piggy’s page on Facebook and have a chat with her there, right now\!

![](./@imgs/01/083994b4fce13e6991bd77845a8f6710bbc19bfb.jpg)

## Create your own Slack chat bot

There are several ways to create your own chat bot. The first challenge is to figure out on what platform you want to run your bot. I’m on Slack all day long, in many channels, so I’m most familiar with it and decided to build a Slack chat bot.

> Note: You may not need to create a custom bot at all. Slack has a dizzying variety of commands you can use to interface with its built-in SlackBot. Slash commands, like /remind, tell it to set up reminders to either a user or a channel, for example to managers who tend to be late to team meetings:
> 
> ![](./@imgs/01/fc8a4a82d69baa23ae03408a765a46c5e8e78ba9.jpg)
> 
> You can also set up webhooks to post messages from external sources into Slack by means of an HTTP request with a JSON payload, but this lies a bit outside the scope of this article.

The next decision to make is how you want to build it. Some services have been created to allow for no-code solutions, including [FlowXO](https://flowxo.com/) and [Botsify](https://botsify.com/), but I found them somewhat obtuse. It seemed easier to use Botkit by the amazing [Howdy.ai](https://howdy.ai/). Botkit is [endorsed by Slack](https://slackhq.com/the-slack-platform-launch-7a3feb5a423a#.hr9isqaeh), so it seemed a great place to start.

> If you haven’t tried Howdy’s amazing trainable Slack bot for managing a distributed team’s interactions, I highly recommend it!
> 
> ![](./@imgs/01/eda2b2803635aecc204b29f1d538a5c5a7c9a4a8.jpg)

Now, all you have to do is decide what you want to build\!

Chat bots can have all kinds of cool integrations. One clever user [recreated her resume as a chat bot](http://lifehacker.com/how-i-turned-my-resume-into-a-chat-bot-1775565350) using [smooch.io](https://smooch.io/). I was inspired to create a bot named @mom whose function is simply to dispense advice, powered by a very simple external API that I found. If you invite @mom into your channel and type `@mom, give me some advice?` , the bot, which listens for the word ‘advice’ in a direct mention, will generate a little snippet of wisdom, like this:

![](./@imgs/01/ac154c78d6adcbe4e59c128431e1f9a8027ce8e7.jpg)

In the future, I want @mom to tell me what’s for dinner, once I find a great recipe API to integrate.

There are just a few steps to setting up a bot on Slack, unless you want to publish it so that everyone can use it, which I decided not to do (yet) for @mom. Here are instructions on creating not just a chat bot with canned responses, but a bot with a simple API configuration.

### Step 1: Grab a copy of Botkit

Clone Botkit following the directions [here](https://github.com/howdyai/botkit) to your local machine, and open the codebase in your editor of choice.

### Step 2: Setup your Slack channel

In the Slack channel where you want your bot to reside, follow these setup steps:

  - Ensuring that you have admin access to enable bots in a Slack channel, navigate to https://.slack.com/apps/build/custom-integration.
  - Click ‘bots’ and ‘add configuration’. The Slack interface guides you through setting up a bot, including naming it and giving it an avatar.![](./@imgs/01/de00c0e0ed1804b3e3b364758010b8075768a3fa.jpg)
  - By the end of the process you will have set up a bot that has an API token. Copy this and save it for later.

### Step 3: Test the bot locally

`cd` to the local installation of the Botkit codebase. In your terminal window, type:

```
token=<insert your token here> node slack_bot.js`
```

You should see a series of messages appear in the terminal. Your bot is awake and is listening for your commands. Now you can take a look at your Slack channel and invite your bot to talk to you by typing its name (note: the @nana bot pictured below is the same as the @mom bot):

![](./@imgs/01/e9a984b5abc05124ff6967aa6c486056c07193de.jpg)

### Step 4: Program Botkit’s Slack script to talk to you

Now you can get creative. If you look at the slack\_bot.js code, you’ll see what goes on behind the scenes. For example, by typing Hello @mom, my Mom bot, which is listening for certain words, knows to respond – by adding a reaction (a heart) and replying to the user:

``` javascript
controller.hears(['hello', 'hi'], 'direct_message,direct_mention,mention', function(bot, message) {
    
    bot.api.reactions.add({
        timestamp: message.ts,
        channel: message.channel,
        name: 'heart',
    }, function(err, res) {
        if (err) {
            bot.botkit.log('Failed to add emoji reaction :(', err);
        }
    });
    
    
    controller.storage.users.get(message.user, function(err, user) {
        if (user && user.name) {
            bot.reply(message, 'Hello ' + user.name + '!!');
        } else {
            bot.reply(message, 'Hello!');
        }
    });
});
```

I wanted my bot to give advice, if it overhears the word “advice” and is asked directly. So all I needed to do was to add a new `controller.hears` function:

``` javascript
controller.hears(["advice"], ["mention", "direct_mention", "direct_message"], function(bot,message){

        request('http://api.adviceslip.com/advice', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            var advice = data.slip.advice;
            bot.reply(message, advice);        
       }
    })       
});
```

### Step 5: Deploy your bot

Now you need to get your bot to production, so that it will stay ‘alive’ indefinitely, as opposed to ‘dying’ when you close your terminal window. I decided to use [Modulus](https://modulus.io/) as a home for my bot as it’s extremely easy to deploy. Modulus is also one of our sister companies in the Progress family, and we like it a lot\!

  - In the Botkit codebase, take a look at `package.json` in the root. It’s set to run the vanilla Botkit code from /lib. Change that to point it to slack\_bot.js:
    
        "main": "slack_bot.js"
    
    This will tell Modulus to run the slack\_bot.js file by default.

  - Create a home for your bot in Modulus. After logging in to Modulus, create a new project.

  - Using the Modulus CLI, deploy your Botkit code to this project by typing `modulus deploy` ,  authenticating, and choosing the project you just created.  
    
    > You can [install the Modulus CLI via npm](https://www.npmjs.com/package/modulus).

  - Making sure that the token you have saved is actually related to the channel where your bot will reside, create an environment variable to store your bot’s token:![](./@imgs/01/2c5a7e5abaa3f5c2f666216051d8ab2446ef759e.jpg)

  - Restart the servo on which your bot project is running, and it should be ready to go\!

### Step 6: Invite your bot to its home channel and start interacting!

After following the same steps outlined previously, I created the bot on my company’s Slack account and then invited it to the our channel Now, my coworkers know that if they ask the @mom bot nicely for advice, she obliges:

![](./@imgs/01/cc3613015d67c3baf9fdb1e61139c8b3f35d52d2.jpg)

## Conclusion

I have a lot of plans for @mom. At some point, she might be released as an installable Slack app. What are you currently building for your teams? Do you find Slack bots helpful or annoying? Where do you see bots headed in the future? Tell me in the comment section below.
