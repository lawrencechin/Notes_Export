#  Mac Refresh (*28/06/2023*)

- Grab applications from backup
- Setup any application that requires fiddling
    - Add **Dark Notify** to login items
- Look in various locations for preferences, config, support files (from backup):
    - ~/Library/Application Support
    - ~/Library/Preferences
    - ~/Library/Containers
    - ~/Library/Group Containers
    - ~/.{config, local, cache, yarn/bin}
- Copy `$HOME` folders from backup
- Run shell script in **iCloud** to install developer folder
- Run `sys_refresh` to install all homebrew stuff
- Browse through Mac command lists to setup some defaults
- Install services and shortcuts
    - Add **Finder** to 'Full Disk Access' to avoid annoying permissions errors: `/System/Library/CoreServices/Finder.app`
- Add any keyboard shortcuts like hiding layers in **Pixelmator**
- Go through **System Preferences** and change settings
- Install fonts using **Right Font** or directly
