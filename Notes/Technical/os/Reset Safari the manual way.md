## Reset Safari the manual way

Gone is the ye old '*reset*' menu item from **Safari**. Should one feel **Safari** isn't running as well as one would hope you can manually reset by doing the following:

All files/folders are located in the user library (`~/Library`).
Quit and close **Safari** before doing this.

1. Delete the **Safari** folder
2. Remove the `com.apple.Safari.savedState` folder in the **Saved Application State** folder
3. Within the **Caches** folder, remove all entries that begin with `com.apple.Safari`
4. In the **Cookies** folder remove the file `com.apple.Safari.SafeBrowsing.binarycookies`
5. Remove any file that features `com.apple.Safari` from the **Preferences** folder

---

Once that is done you should restart the computer and then change whatever preferences are necessary before going about your normal business. 