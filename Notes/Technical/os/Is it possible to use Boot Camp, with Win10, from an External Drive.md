# [🤖 > Is it possible to use Boot Camp, with Windows 10, from an external drive?](https://apple.stackexchange.com/questions/218105/is-it-possible-to-use-boot-camp-with-windows-10-from-an-external-hdd)

**Q:** I need to run Visual Studio on my MacBook Pro and I need Windows to do that. The easiest and well known way to install Windows on a Mac is by running the Boot Camp app on the Mac and let it do what's necessary. The problem is that I don't want to partition the Internal SSD so the remaining option is to install Boot Camp Windows 10 on an External Drive and boot it every time I need it by plugging in my external USB 3.0 HDD. Is this achievable?

**A:** Here's an updated procedure for Windows 10, based on orkoden's excellent answer.

I tested this process on a MacBookPro11,1 running OS X 10.11.5 (15F34). Throughout the process, directly connect all devices to your Mac. I found that certain operations failed more frequently if I used the USB hub in my monitor.

In addition to the external drive that will host your Windows installation (the "destination drive"), you will need another USB drive (the "driver drive") to temporarily store the Boot Camp drivers.

## I used these parts:

- Destination drive: Samsung T3 Portable 500GB USB 3.0 External SSD (MU-PT500B/AM)
- Driver drive: SanDisk Extreme 32GB USB 3.0 Flash Drive (SDCZ80-032G-GAM46)

## Here are the steps:

1. Install VMware Fusion 8.1.1 from VMware's site
1. The non-professional free evaluation version is sufficient
1. VMware Fusion 7.1.3 couldn't connect my external USB disk to my Windows VM
1. I downloaded VMware-Fusion-8.1.1-3771013.dmg (SHA256: 29cad381a36374e58a85fb58f7aaad8cae41ad50ef07fdda0db6d782c95c0a95)
1. Download the Windows 10 ISO file from <https://www.microsoft.com/en-us/software-download/windows10ISO>
1. I chose Windows 10, English, 64-bit
1. I downloaded Win10_1511_1_English_x64.iso (SHA256: cf5cff9e23c853fed769cf382e18b29889dcc0055b69226f0164ab51eca3069c)
1. Download the Windows 7 Automated Installation Kit from <https://www.microsoft.com/en-us/download/details.aspx?id=5753>
1. I downloaded KB3AIK_EN.iso (SHA256: c6639424b2cebabff3e851913e5f56410f28184bbdb648d5f86c05d93a4cebba)

### Prepare the driver drive.

1. Run diskutil list to determine the device name. In my case, the device name was /dev/disk2.
1. Erase the disk using diskutil eraseDisk fat32 DRIVERS MBR /dev/disk2
1. Open Boot Camp Assistant
    1. In "Introduction", click "Continue"
    1. In "Select Tasks":
        1. Uncheck "Create a Windows 7 or later version install disk"
        1. Check "Download the latest Windows support software from Apple"
        1. Uncheck "Install Windows 7 or later version"
        1. Click "Continue"
    1. If "Select Tasks" is not there, try "Action->Download Windows Support Software".
    1. In "Save Windows Support Software", choose the driver drive, and click "Continue".
    1. Wait for the process to complete.
1. Eject the driver drive: diskutil eject /dev/disk2
1. Disconnect the driver drive.
1. Prepare the temporary Windows VM. We'll use the temporary Windows VM to write the disk image to the destination drive. In VMware Fusion:

### Create a new VM:

1. File > New
1. In "Select the Installation Method", choose "Install from disc or image", and click "Continue".
1. In "Create a New Virtual Machine", choose "Use another disc or disc image…", locate Win10_1511_1_English_x64.iso, and click "Continue".
1. In "Microsoft Windows Easy Install", uncheck "Use Easy Install", and click "Continue".
1. Click "Finish".

### Start the VM and complete Windows Setup. The settings don't really matter, because we're just using this VM to write the disk image.

1. Choose "Next"
1. Choose "Install now"
1. Choose "I don't have a product key"
1. Choose "Windows 10 Pro"
1. Choose "I accept the license terms"
1. Choose "Custom: Install Windows only (advanced)"
1. Choose "Drive 0 Unallocated Space" and click "Next"
1. Choose "Use Express settings"
1. Choose "I own it"
1. Choose "Skip this step"
1. Enter a username

### Install the Automated Installation Kit:

1. From the "Virtual Machine" menu, choose "CD/DVD (SATA)" > "Choose Disc or Disc Image...". Select KB3AIK_EN.iso.
1. From the Start menu, choose "File Explorer", and then "This PC". Double-click "DVD Drive (D:) KB3AIK_EN".
1. Choose "Yes"
1. Choose ".NET Framework Setup"
    1. Choose "Download and install this feature"
    1. After installation completes, choose "Close"
1. Choose "Windows AIK Setup"
    1. Choose "Next"
    1. Choose "I agree"
    1. Choose "Next"
    1. Choose "Next"
    1. After installation completes, choose "Close"

### Prepare and image the destination drive:

1. From the Start menu, choose "All apps". Choose "Microsoft Windows AIK". Right-click "Deployment Tools Command Prompt". Choose "More", then choose "Run as administrator". Choose "Yes".
1. Prepare the destination drive:
    1. Run diskpart
    1. Plug in the destination drive. In the "Choose where you would like to connect " prompt, choose "Connect to Windows"
    1. Run list disk to determine the disk number of the destination drive. In my case, the disk number was 1.
    1. Run: select disk 1
    1. Run: clean
    1. Run: create partition primary
    1. Run: format fs=ntfs quick
    1. Run: assign
    1. Run: active
    1. Run: list volume
    1. Note the drive letter for the selected volume (marked with a \*); this is the drive letter of the destination drive. In my case, the letter was "E".
    1. Run: exit
1. Image the destination drive:
1. From the "Virtual Machine" menu, choose "CD/DVD (SATA)" > "Choose Disc or Disc Image...". Select Win10_1511_1_English_x64.iso.
1. Run: imagex /check /verify /apply d:\sources\install.wim "Windows 10 Home" e:
1. For Windows 10 Pro, use "Windows 10 Pro"
1. For Windows 10 Education, use "Windows 10 Education Retail Technical Preview"
1. Run: bcdboot e:\windows /v /s e:
1. Run: shutdown /p
1. Quit VMware Fusion

### Restart into Windows to complete installation

1. Restart the Mac while holding the Option key
1. When the startup disk list appears, use the arrow keys to choose Windows. Complete Windows Setup. If Windows restarts during Setup, restart again while holding the Option key to return to Windows.
1. Insert the driver drive. Open the Start menu, choose "File Explorer", choose "DRIVERS (E:)", open the "BootCamp" folder, and open "Setup". Complete the installation.

### Windows is now installed and ready.

1. To restart into OS X, click the Boot Camp icon in the notification area and choose "Restart in OS X…".
1. To restart into Windows, open "System Preferences" from the Apple menu, choose "Startup Disk", choose "BOOTCAMP", and then choose "Restart…".
1. To choose an OS at boot time, hold down the Option key.
