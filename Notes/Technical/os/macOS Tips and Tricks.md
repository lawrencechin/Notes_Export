# macOS Tips and Tricks

😎: Tips from the excellent [Saurabh Site](https://saurabhs.org/)

## System
### Native UI Conventions 😎

- Press ⇧⌘/ to search all of the current app's menu items. Then use the Up/Down arrow keys to navigate the results and press Return to execute that menu bar action.
- Press ⌃F2 to move keyboard focus to the application's menu bar. Start typing the first few letters of a menu title to jump to that menu.
- Hold the Option key while expanding an outline view to recursively expand all children. (The easiest place to test this is in Finder's List view.)
- On modal dialogs/sheets, press Command + the first letter of the button to press that button. ⌘. is the shortcut equivalent of the Escape key.
- Hold Control and Option while clicking on a window to switch focus to that window without raising it.
- Hold Command while dragging a toolbar icon to move it to a new position.
- By default, clicking inside a scroll bar will scroll partially towards the clicked location. Hold Option while clicking in the scroll bar to jump directly to the clicked location.
- Hold Option while dragging the scroller to slowly scroll.
- In a scroll view, use the Up/Down keys to scroll in small increments. Hold Option to scroll in larger increments, and hold Command to scroll to the beginning or end.
- Hold Option while pressing the Page Up/Page Down keys to also move the cursor while scrolling.
- In a text field that treats the Tab key as an input, press Control-Tab and Control-Shift-Tab to move focus to the previous or next control.
- Press ⌃⌘D while holding the pointer over a word to view an inline dictionary definition of the word.
- Press ⌃F6 to move focus to a floating window.
- To quickly find text, select some text and press ⌘E followed by ⌘G.
- Press ⌃⌫ to delete only the accent mark from the previous character (e.g. é will become e).
- In Fonts windows, enter \*X to scale the current font size, e.g. \*1.5.
- When entering text, press ⌥⇧K (on U.S. keyboards) to insert an Apple logo.
- Hold Command while dragging a Picture-in-Picture (PiP) video player to move it anywhere without having it snap to one of the screen corners.

---

### Mission Control / Window Management 😎

- When a window is inactive, use the Command key to interact with it without making it active.
- Hold Control when pressing the Mission Control function key to only show the current app's windows.  Hold Command when pressing the Mission Control function key to show the desktop.
- Hold Option while double-clicking a window's corner to expand the window to fill the screen.
- Option-Click to switch windows while hiding the previous app. Command-Option-Click to switch windows and hide all other apps.
- In the Command-Tab app switcher, press the Up or Down arrow keys (or the 1 key) on an app to view that app's windows.
- In the Command-Tab app switcher, hold Option while switching to an app to un-minimize all its windows.
- Press ⌘\` to cycle between windows of the foreground application.
- Press ⌃F4 to cycle between windows across all applications in the current desktop space.
- Drag a window to the top of the screen and push it against the top of the screen to enter Mission Control with that window selected.
- Press ⌥⌘W or hold Option while clicking the red close button, to close all of the foreground application's open windows.
- Press ⌥⌘M, or hold Option while clicking the yellow minimize button, to minimize all of the foreground application's open windows.
- Hold Option while clicking the green zoom button to fill the window to the screen instead of entering full-screen mode.
- When configuring Hot Corners in System Settings, hold any or all of Control, Option, Command, or Shift to only activate the corner while the selected keys are also held down.
- On a trackpad, use two fingers to double-tap (tap, not click) on an app's Dock icon to show all of the app's windows. Alternatively, perform the App Exposé trackpad gesture over an app's Dock icon.
- Hold Option while resizing a window to resize from the center of the window.  Hold Shift while resizing a window to lock the aspect ratio.
- Double-click a window's border to resize that edge to fill the screen. Hold Option while double-clicking to expand both edges.
- In Mission Control, perform a scroll up over a group of app windows to reveal the individual windows. Press the Space bar while holding the pointer over any window to magnify it.
- In Mission Control, Option-click another desktop space to switch to that space while staying in Mission Control.
- In Mission Control, drag the application icon underneath the windows to move all of an app's windows to another space.
- In Mission Control app window mode, press Tab and Shift-Tab to switch applications.
- If a window was minimized in a different space, hold Command while un-minimizing to restore it to the current space.
- Hold Option while un-minimizing a window to un-minimize all windows from that app.
- If an app has windows in multiple spaces, click the app's Dock icon repeatedly to cycle through the spaces with that app's windows.
- If keyboard shortcuts are enabled for switching desktop spaces, pressing a shortcut while clicking and holding a window will move the window to that space.
- In Stage Manager, hold Shift while clicking on a window to add that window to the current stage instead of replacing it.

---

### Save Dialogs 😎

- Drag a file or folder from Finder into an open/save dialog to jump directly to that file.
- In save dialogs, press ⌘= to switch between the compact and expanded layout.
- In save dialogs, press ⌘⌫ to activate the Delete button, ⌘D to activate the Don't Save button, and ⌘. (or Esc) to activate the Cancel button.
- Press ~ to open a Go To File dialog prefilled with the home directory. Press / to open it prefilled with the root directory.
- Press ⌘R to reveal the selected item in Finder.

---

### Turn Off Default iCloud Save Location

I'm sure I recall a checkbox or option for this but a web search only returns this command:
`defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false`

---

### Function Keys 😎

- Hold down Shift and Option when changing the volume or brightness to make smaller adjustments.
- Hold down Option while changing the brightness to quickly open Display settings, or while changing the volume to open Sound settings.
- Hold down Shift while changing the volume to audibly preview the volume level.
- When connected to an external display, hold down Control while changing the brightness to adjust the brightness of the non-active display.
- Press and hold down on the Mission Control function key to automatically exit Mission Control after letting go of the key.

---

### Menu Bar / Notification Center 😎

- Hold Option while opening the Wi-Fi and Bluetooth menus to access extra options.
- Hold Command while dragging a menu bar icon to move it to a new position.
- Add new menu bar items by dragging icons from Control Center to the menu bar.
- Option-click the date/time in the menu bar to toggle Do Not Disturb.
- Right-click a widget to change its size.
- On a trackpad, swipe horizontally with two fingers over a notification to dismiss that notification.

---

### Dock 😎

- Press ⌥⌘D to hide and show the dock.
- Press ⌃F3 to move keyboard focus to the Dock. Then use the Left and Right arrow keys to select an app, or type the first few letters of an app. Press Enter to open the selected app, or press the Up arrow key to open the app's menu.
- Hold Control and Shift while mousing over the Dock to temporarily turn on magnification.
- Quickly move the Dock to a different side of the screen by holding Shift while dragging the resize handle.
- Hold Option while resizing the Dock to resize in multiples of 16 points.
- Hold Option and Command while clicking a running app's Dock icon to hide all other applications.
- Open a file in a specific application by dragging the file to the application's Dock icon. If the application doesn't accept the file type by default, force open the file by holding Option and Command while dragging the file.
- If a dock icon is bouncing repeatedly, stop the bouncing by mousing over the icon.
- Hold Control and Command when right-clicking a Dock icon to only see the default system menu options.
- Hold Option and Command when clicking on a folder in the Dock to open the folder in a new Finder window.
- To open multiple items from a stack, hold Option while selecting an item to open it in the background while keeping the stack open.
- After opening a stack, hover the cursor over an item and press the Space bar to preview the item with quick look.
- Right-click the Launchpad dock icon to open an app from an inline menu.
- In Launchpad, hold Option to enter "jiggle mode" to re-arrange and delete apps.
- Add shortcuts to System Settings to the Dock by navigating to /System/Library/PreferencePanes and dragging preference panes to the Dock.
- To add AirDrop to the Dock, navigate to /System/Library/CoreServices/Finder.app/Contents/Applications in Finder and drag the AirDrop icon to the Dock.

---

### Spotlight 😎

- Press ⌘B to search the web for the current query.
- Press ⌘C to copy the full path to the selected file, or to copy the result of the current calculation.
- Press ⌘D to show a dictionary preview for the current query.
- Press ⌘L to jump to the dictionary section in the results (if present).
- Press ⌘⏎ or ⌘R to reveal the selected file in Finder.
- Use the name: filter to only search in the filename.
- Add kind:folder to only search for folder names.
Hold Command to show the path to the currently selected file.

---

### Screenshots 😎

- After pressing ⇧⌘4 and while drawing the screen capture area, hold Option while resizing to resize from the centre, and hold Shift while resizing to adjust only one axis. After drawing the area, hold the Space bar and drag to move the selected area.
- After pressing ⇧⌘4, hold Control while taking the screenshot to copy to the clipboard instead of saving to file.
- After pressing ⇧⌘4, press the Space bar to select a window to screenshot. Hold down Option while taking the screenshot to remove the window's shadow.
- Right click on the floating screenshot preview to access additional actions.

---

### Remove Dropshadows from Screenshots
To remove dropshadows from screenshots enter the following command : 

`defaults write com.apple.screencapture disable-shadow -bool true`

You have to log out or restart to see changes. Also bare in mind that you cannot add the shadows back in by using the **option** key which would have been neat.

---

### Rebuild the '_Launch Services_' to remove duplicates in the '_Open With…_” menu.

Fire this command into the terminal and you're good to go!

`/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user`

---

### Refresh LaunchPad

`defaults write com.apple.dock ResetLaunchPad -bool true; killall Dock`

---

### Flush DNS cache

```sh
sudo killall -HUP mDNSResponder;
sudo killall mDNSResponderHelper;
sudo dscacheutil -flushcache

```

---

### Enable text selection in Quicklook

`defaults write com.apple.finder QLEnableTextSelection -bool true && killall Finder`

---

### Refresh Icon Cache

Should you ever find a need to refresh icons here’s the ticket : 
`sudo find /private/var/folders/ -name com.appple.dock.iconcache -exec rm {} \;`
`sudo find /private/var/folders -name com.apple.iconservices -exec rm -rf {} \;`
`sudo mv /Library/Caches/com.apple.iconservices.store com.apple.ic`
`killAll Dock`

---

### View Installed Files from PKG Manifest (BOM)

To view what files were installed via a package you need to have a look in the `/var/db/receipts/`. This folder holds all the **bill of materials** files, otherwise known as BOM, left by package installers. Once you've found the culprit issue the following command to get a list of installed files : 

`lsbom -f <BOM FILE>`

---

### Fix Attempt for WindowServer
Sometimes the UI lags in **macOS**. Sucks. It's difficult to pinpoint the whys and wherefores of this phenomena but a potential solution could be: 

```sh
killall Dock
killall Finder
killall SystemUIServer

process -a $PROCESS
```
Don't run all of these together: try each one in order and check if it fixes the problem. The second part, `process -a …` is used in the event that the process killed doesn't come back.

---

### After Safe Boot

**Safe Boot**( hold down shift while booting ) is useful to refresh the system. System caches are deleted and will be rebuilt. The problem is after booting normally you may find **ram** usage is rather high. Should you find this enter this command to fix the problem : 

`sudo update_dyld_shared_cache -force`

---

### Disable SIP

Sometimes you may need to make some system modifications and that means disabling SIP. Here's how : 

Reboot the computer and at the chime hold down the following `CMD+R+S`. This will startup recovery mode in single user-mode. The benefit of using single-user mode is how much faster it is to boot into rather than the full gui version. Once you're at the prompt write this `csrutil disable`. Done.

---

### Reset NVRAM
You know the drill. Hit these keys immediately after starting up the computer :

`⌃ + ⌘ + P + R`

After about 20 seconds, or after two chimes, release and startup as per usual. Change any settings that may have been reset.

---

### Change Hibernate Mode

The default hibernation mode is "*safe sleep*". This stores the contents of RAM to file that is restored when the laptop runs out of power. This sounds just swell but if one runs the laptop connected to power and in clamshell mode all the time its a waste of space so let's change it.

```sh
# Use this to determine the current status (default is mode '3')
pmset -g | grep hibernatemode
# Change hibernate mode
sudo pmset -a hibernatemode 0
```

After doing this you should restart to take effect. Any sleep images in `/private/var/vm/` can be deleted if not already.

---

### Font Smoothing Post High Sierra

Font smoothing has taken a turn for the worse on non-retina screens in **Mojave** and requires some cajoling to get it right.

`defaults write -g CGFontRenderingFontSmoothingDisabled -bool NO`

Restart or log out to take effect.

---

### Enabling SSD Trim

When doing a clean install one usually has to re-enable trim on the third-party SSD and then restart. By the way, if you need to do a few commands that require restarting then doing them all together! Remember, when running trimforce it will restart automatically so run that command last.

`sudo trimforce enable`

---

### Fix GOG Installers

Often **GOG** package installers do not actually install anything. In order to get a working application try the following instructions:

``` sh
xar -xf $PACKAGE
cd package.pkg
tar -xvf Scripts
```

Have a look inside the `package.pkg` file for the `payload` folder. Drag this folder out to another location and rename `$GAME_NAME.app`. Test that it runs and then move the application anywhere you see fit. Consider automating to procedure.

---

### Lockscreen Background not Reflecting your Wallpaper?

¿El fondo de lockscreen no se parece al papel de empapelar? Sin problema. Una carpeta necesaria no existe en su sistema. Vamos a arreglar lo. Coge su *UUID* desde *usarios & grupos* del preferencias de sistema y navega a `/Library/Caches/`. Si una capeta nombrada 'Desktop Pictures' existe, entra en la o crea la capeta con ese nombre. Dentro, crea otra carpeta con su *UUID*. Listo.

---

## Finder
### Tips and Tricks 😎

- After copying a file, press ⌥⌘V to move the file instead of pasting a copy of it.
- Press ⌃⌘N with multiple files selected to create a new folder with those items.
- Press Tab and Shift-Tab to navigate through files alphabetically, regardless of the current sort ordering (only in Icons and List view).
- Hold Option while activating Quick Look to immediately launch into full-screen view.
- After opening Quick Look with multiple files selected, press ⌘⏎ to display a grid view of all items. Use the arrow keys to navigate and press Return to select an item to focus on.
- Right-click on the "Open with" button to select a different app to use to open the file.
- In Columns view, hold Option while resizing a column to simultaneously resize all columns.
- In Columns view, double-click a column separator to auto-resize that column. Hold Option while double-clicking on any separator to auto-resize all columns.
- In Columns view, click the empty space at the bottom of a folder to go to the parent folder.
- In Columns view, and when in a deeply nested file, press Shift-Tab and Tab to navigate through the parent directories without losing the path to the file.
- Hold Option while dragging a file to make a new copy instead of moving the original. Hold Command and Option to create an alias to the file.
- In List view, press ⌘+ and ⌘- to increase and decrease the row size.
- In List view, press ⌥↑ and ⌥↓ to select the top-most and bottom-most item.
- In List view, use the Left and Right arrow keys to collapse and expand directories. When a file is selected, press the Left arrow key to jump to the parent folder.
- Press ⌘I to show the inspector for the current file.  Press ⌥⌘I to show a floating inspector that updates with the selected file.
- Press ⌥⌘C to copy the full pathname of the currently selected file.
- Press ⇧⌘. to toggle showing hidden files.
- Press ⌥⌘⌫ to immediately delete a file without sending it to the Trash.
- Merge folders by holding Option while dragging one folder on top of another folder.
- Set a custom icon for a folder by copying the new icon, inspecting the folder (⌘I), and pasting the icon by selecting the folder icon in the upper-left of the inspector window and pressing ⌘V.
- Drag selected text into a Finder window to quickly create and save a text clipping. (Text clippings are text files that can't be edited and don't require a filename to be saved.)
- Press ⌥⌘O to open the selected file and automatically close the Finder window.
- Press ⌥⇧⌘V to paste an item while preserving the file permission flags.
- Hold Command while dragging an icon in Icon view to align it to a grid.
- Restart Finder by holding Option while right-clicking the Finder dock icon and selecting Relaunch.
- Drag a folder to the new tab button (only visible if multiple tabs are already open) to open the folder in a new tab.
- Press ⌃⌘↑ to open the parent folder in a new window.
- If the toolbar is hidden (⌥⌘T), Finder will open folders in a new window.

---

### Reset Finder Views

*Finder* can be a touch pedantic about remembering how it was last displayed. Sometimes you may want to reset all folders to the default view. Use this code to remedy it : 

`sudo find / -name .DS_Store -delete; killall Finder`

---

## Safari

### Tips and Tricks 😎

- Tab groups organise tabs spatially. Use ⌥⌘{↑,↓,←,→} to navigate tabs in 2D space.
- When a video is playing, right click the speaker icon in the address bar or tab to enter Picture-in-Picture (PiP) mode.
- Hold Option while closing a tab to close all other tabs except the current tab.
- Click and hold the back button to see recent browsing history. Hold Option to see page URLs instead of page titles.
- Drag selected text onto the Safari dock icon to quickly search the web for that text.
- Press ⇧⌘T to re-open the most recently closed tab or window.
- Click and hold on the new tab icon in the toolbar to view recently closed tabs.
- Press the Space bar at the beginning of the address bar to change the search engine and to see recent web searches.
- Add kind:bookmark to Spotlight searches to search Safari bookmarks and browsing history.
- Shift-click a link to add it to Reading List.
- Click and hold a bookmark in the bookmarks bar to edit its display title.
- Option-click the reload icon or press ⌥⌘R to force a fresh reload of the current webpage.
- Right-click the reader icon in the address bar to automatically turn on Reader Mode for all pages on the current website.
- After performing a web search and opening a result in the same tab, press ⌥⌘S to go back to the search results page.
- Press ⇧⌘I to create a new email message with the contents of the current page.
- Hold Option over the History > Clear History menu item to only clear browsing history while preserving website data.

---

### Extensions Vanished?

Run this: `/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -f -R /Applications/Safari.app`

---

### Debug Menu
`defaults write com.apple.Safari IncludeInternalDebugMenu 1` - use this command to enable the **Safari** debug menu. This is useful for many reasons but in particular one can disable autoplaying video.
Incidentally, should you wish to use **Safari Technology Preview** you can use the same command as above but add amend it to read as follows : `com.apple.SafariTechnologyPreview`.

---

### Permanently Change User Agent in Safari

To find user agent strings you can hover over the options in `Developer -> User Agent` or search online.

`defaults write com.apple.Safari CustomUserAgent "\" user agent string here "\"`

---

### Disable Hyperlink Auditing Beacon

nb. This should probably be in the legacy section…
[Source](https://github.com/el1t/uBlock-Safari/wiki/Disable-hyperlink-auditing-beacon)
Essentially, hyperlink auditing (`<a ping>` *attribute*) is designed to notify an arbitrary website when you click on a link.

Use these commands in the **terminal** to disable :

``` sh
defaults write com.apple.Safari WebkitHyperlinkAuditingEnabled -bool false
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2HyperlinkAuditingEnabled -bool false
```

---

## Mail 😎
### Tips and Tricks
- Command-click multiple mailboxes in the left sidebar to simultaneously view all messages from the selected mailboxes.
- Select part of a message's text before replying or forwarding to only include the selected text in the new message.
- Select an attachment in a message before forwarding to only include the attachment in the forwarded message.
- Drag a file to the Mail dock icon to compose a new message with the file already attached.
- Press ⌥⇧⌘N to create a new tab.
- Press the Space bar at the end of a message to go to the next message. Hold Shift while pressing the Space bar at the top of a message to go to the previous message.
- Start typing the first few characters of the sender, subject, or body to jump to that message in the messages list.
- Press ⌥⌘↑ and ⌥⌘↓ to jump to the top-most or bottom-most message in the messages list.
- Save an extra copy of an important message by dragging it to the Finder.
- If a message bounces, use Message > Send Again on the bounced message to re-send it to a different address.
- To add a message to multiple folders, hold Command while dragging the message to each folder.
- Press ⇧⌘C to assign a color to a message.
- Drag a message into the Notes or Reminders app to add a link to the message.
- Press ⌘R and ⇧⌘R while replying to a message to switch between reply and reply-all.
- Press ⌥⌫ to delete a message without automatically opening the next message.
- Command-click on the currently selected message to unselect it.
- If a message has already been replied to, click on the reply icon in the message list to open the reply in a new window.
- When creating a new mailbox, add a forward slash to create a nested mailbox.
- After performing a search, select a mailbox from the left sidebar to filter the search to that mailbox.
- Hold Shift while launching Mail to reset the index.
- Playing around with fonts can be fun but shifting back to **San Francisco** isn't possible through normal means. Hark! A Solution: `defaults delete -app Mail MessageListFont`. This command will reset the font declaration for the list font.

---

## Preview 
### Tips and Tricks 😎
- Press \` to bring up a magnifier, and then press + and - to resize it.
- In a PDF document, re-order the pages in the document by re-ordering the pages in the sidebar.
- Merge two PDF documents by dragging pages from one document's sidebar to the other document's sidebar.
- In the save dialog for an image, hold Option while opening the Format menu to access an extended list of formats.
- Hold Option and the Space bar to activate the pan tool.
- Hold Option while in text selection mode to switch to rectangular text selection.

---

## Calendar
### Tips and Tricks 😎

- Hold Shift while dragging an event to set a more precise time instead of snapping to 15-minute intervals.
- Click and hold the Accept button in a calendar invite to change which calendar the event is accepted to.
- Hold Option while pressing the Accept button in a calendar invite to accept all events.
- Hold Command while clicking any calendar's checkbox in the left sidebar to show or hide all calendars.
- Hold Command and Option while clicking a calendar's checkbox to only show that calendar.
- Add kind:event to Spotlight searches to search calendar events.
- Resize the mini-calendar in the bottom-left to preview more months.
- With an event selected, press ⌃⌥↑ and ⌃⌥↓ to adjust the time of the event. This also works with multiple events selected.
- In Week view, press ⌥⌘← and ⌥⌘→ to shift the view by a single day.

---

## Messages
### Tips and Tricks 😎

- Press ⌘R to directly reply to the latest message in the conversation.  Press ⇧⌘R to reply to the latest thread in the conversation.
- Press ⌘T to bring up the tapback selector for the latest message. Use the 1-6 number keys to select a reaction.
- Press ⌘E to edit the latest sent message.
- Press ⌥↑ and ⌥↓ in the message input field to cycle through previously sent messages.
- Press ⌘1-9 to jump to a pinned conversation.
- Right-click on a message and select Show Times to view exact timestamps for each message.
- Right-click the Messages icon in the Dock to quickly view and jump to unread conversations.

---

## Photos
### Tips and Tricks 😎

- Drag an item to the "My Albums" header section to quickly create an album with that item.
- After adding an item to an album, use ⌃⌘A to add other items to that same album.
- When editing an image, hold down the M key to compare the modifications to the original.
- Hold Option while launching Photos to choose a different photo library to open.
- Drag a photo from the Photos app into a Finder window to quickly export the photo, or into the Mail or Messages app to attach the photo.
- When editing an image, double-click an adjustment slider to reset it.
- When editing an image, hold Option while holding the pointer over an adjustment slider to extend the adjustment range.
- Hold Option while clicking the rotate button to reverse the rotation direction.

---

## TextEdit
### Tips and Tricks 😎

- In rich text mode, press Option-Tab to insert an outlined list.
- Press Option-Escape to autocomplete the current word.
- Press ⌃⌥⌘P in the find text field (⌘F) to access advanced search operators.
- Hold Option while selecting text to make vertical text selections.
- The select line dialog (⌘L) supports the following formats:  1-3 selects lines 1-3 in the document  +2 selects the 2nd line below the cursor  -2 selects the 2nd line above the cursor  +2-4 selects 3 lines, starting from 2 lines below the cursor  -2-4 selects 3 lines, starting from 2 lines above the cursor

---

## Terminal
### Tips and Tricks 😎

- Press ⇧⌘A to select the output from the previous command.
- Press ⌘L to clear the output from the previous command.
- Press ⌃⌘V to paste and format text that is properly escaped for the shell.
- Press ⌃T while a command is executing to view runtime statistics about the execution so far.
- Press ⌘{↑,↓} to select the previous/next commands. Then press ⇧⌘A to select the output of the currently selected command.
- Press ⇧⌘I to set a title for the current window and tab.
- Drag a file or folder into a Terminal window to insert its full path. Alternatively, copy a file or folder in Finder and paste it in Terminal to insert its path.
Calculator
- Press ⌘T to open a new window that keeps a running history of calculations.
- Press ⌘R to enable Reverse Polish notation (RPN) mode.
- Press p to insert pi.
- Right-click the number display and select "Large Type" to view the current result in a large overlay window.
- Use the Convert menu to perform various unit conversions.
- In Programmer view (⌘3), click the individual binary bits to toggle between 0 and 1.
QuickTime Player
- Grab a single frame from a video by pausing on the desired frame (using the Left and Right arrow keys to navigate individual frames) and pressing ⌘C.
Photo Booth
- Hold Option while taking a picture to skip the countdown.
- Hold Shift while taking a picture to disable the screen flash.
- When choosing one of the distortion effects (on the third page), click and drag the cursor on the image preview to change the effects origin.

---

## App Store
### Show App Store Debug Menu

`defaults write com.apple.appstore ShowDebugMenu -bool true `

---

## Dictionary
### Webster's Unabridged Dictionary for Mac OS X Dictionary app!

Better default dictionary from [http://jsomers.net/blog/dictionary](http://jsomers.net/blog/dictionary).

```sh
git clone https://github.com/altosaar/webster-mac.git
cp -r webster-mac/Webster\'s\ Unabridged\ Dictionary.dictionary /Users/`whoami`/Library/Dictionaries/
```

1. Clone and copy `Webster's Unabridged Dictionary.dictionary` to your `/Library/Dictionaries` folder:
1. Go to *Preferences* of **Dictionary.app**, select **Webster's Unabridged Dictionary** and drag it to the top.
1. Enjoy better defaults.

---

## Legacy
### Manually fix ownership of System Files
The command to change ownership back to the system is as follows. Use this when it **DOES NOT** make sense to do a full repair disk command.

 `sudo chown -R root:wheel ___file_____`

---

### Mavericks AppleRTC patch
Use this command in the terminal _(on the file within the MacOS folder_) to fix **CMOS** resets:

`sudo perl -pi -e 's|\x75\x2e\x0f\xb6|\xeb\x2e\x0f\xb6|'`

---

### Show or Hide hidden Files  
Use '_killAll Finder_' to refresh.
`defaults write com.apple.finder AppleShowAllFiles TRUE / FALSE`

**Update**: no need for this anymore, simply hit `⇧⌘.`

---

### Enable Airdrop on *unsupported* models
`defaults write com.apple.NetworkBrowser BrowseAllInterfaces 1`
`killAll Finder`

---

### Disabling Photos Photo Scanning

To stop `photoanalysisd` from running and slowing down Photos immensely, try the following command:

``` sh
sudo launchctl remove com.apple.photoanalysisd

# To re-enable
sudo launchctl load com.apple.photoanalysisd
```

You may need to restart or logout to see changes but it seems to survive subsequent restarts/shut downs and OS updates.

#### Catalina 

The above command doesn't seem to work on Catalina so try this instead: `launchctl unload -w /System/Library/LaunchAgents/com.apple.photoanalysisd.plist`.

---

### Disable iTunes backup

`defaults write com.apple.iTunes DeviceBackupsDisabled -bool YES` - What's the point of backing up your iPhone? It's not clear at all and it's a pain so let's disable that shall we?

---
