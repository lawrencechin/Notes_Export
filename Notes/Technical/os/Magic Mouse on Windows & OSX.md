## Magic Mouse on Windows & OSX

Setting up bluetooth with *magic mouse* and *trackpad* in both **Windows** and **Mac** Dual boot

#### In Windows first download:

* The latest version of **PTools** from *http://technet.microsoft.com/en-us/s.../bb896649.aspx*, this will allow you to edit your *registry* as **System** later, allowing you to edit your *bluetooth* device link keys.
* **Bootcamp** drivers for both **Magic Mouse** and the **Magic Trackpad** (*you can get these from http://www.trackpadmagic.com or follow the guide there for installation of Bootcamp 4.0 drivers using 7-zip 4.65 and BootCampESD.pkg Mouse and Trackpad utilities from: http://www.trackpadmagic.com*).
* Install **PTools** (*put files in `System Drive:\Windows\System32`*).
* Install the drivers for both the *trackpad* and the *magic mouse*.
* As you would ordinarily, connect both the *trackpad* and *magic mouse* via *bluetooth* (*Add Device etc…*).
* Once installed, right-click and select *Properties* for either one of the devices and go to the *Bluetooth* tab, take a note of the *Unique Identifier* near the bottom of the page under *Troubleshooting Information* (*this will be noted as a series of letters and numbers, eg. 1a:b2:c3:45:67:8d*). You will need this later to identify your device in **Regedit**. 
* Restart your machine and boot into **Mac**.

#### Back to the Mac:

* Connect to either your *magic trackpad* or *mouse* as usual via *blueooth*
* Open **terminal** and copy-paste the following command and hit enter:
	*  `sudo defaults read /private/var/root/Library/Preferences/blued.plist`
* Take a note of the link key that this will then give you, this will be a series of letters and numbers formatted for **OS**. You will need to reverse these for use in **Windows**. Below is an example:
	* **Mac**: 12345ab6 78c91234 567891d2 3456789e 
	* **Windows**: 9e785634 d2917856 3412c978 b65a3412
* Now connect to your other device and carry out the same procedure to get the link key for that device.
* Reboot into **Windows**, neither of your devices will work yet, but fret not!
* Open the **command line tool** as an **Administrator**.
	* `psexec -s -i regedit`
* Hit enter, **regedit** will now open with full access to the keys you need.
* Navigate to: `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\servic es\BTHPORT\Parameters\Keys\Unique ID`
* You will see a list of devices in the right-side pane, you can determine which is your *trackpad* or *mouse* by using the *Unique Identifiers* you noted earlier in step 5, right-click on the relevant device and now click **Modify Binary Data…** and type in the appropriate link key for your device (*the link key that you changed into the correct **Windows** format in step 9*)
* Do this for each device.
* Exit **regedit**, click your *magic mouse* and your *trackpad*and hey-presto, they'll start working!
* Reboot and they'll still be bonded and working for **Mac**!