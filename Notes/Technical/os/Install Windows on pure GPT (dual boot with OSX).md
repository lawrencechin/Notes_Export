# Install Windows in pure GPT([reddit](http://www.reddit.com/r/mac/comments/2fy3be/by_popular_demand_my_better_than_bootcamp_guide/))
 
## Preparing to Erase OS X

Before installing *Windows 8*, your drive will need to be erased. If you would like to retain your *OS X* setup, make sure it is backed up if it isn't already. *Time Machine* is of course a great way to do this, as are disk cloning tools such as *CarbonCopyCloner*.

If using *Time Machine*, you will need a way to booting into an *OS X recovery environment* after your drive is completely erased. If you have a Mac which is from *2010* or later, it supports internet recovery, no external drive required. Otherwise, you will need to create a recovery disk using Recovery Disk Assistant, or an entire OS X installer using DiskMaker X and a copy of OS X downloaded from the App Store.
 
## Creating a Windows 8 Installer

Next, you will need to make a Windows 8 USB installer out of an ISO. The easiest way of obtaining an ISO is to download one from Microsoft. If you have a retail DVD, it can easily be converted to an ISO as well.

To create the USB installer, open Bootcamp Assistant from your Applications' Utilities installer and select the options for "Create a Windows 7 or later version install disk" and "Download the latest Windows support software from Apple."

If the "Create a Windows 7 or later version install disk" option is not available, or if Bootcamp Assistant claims your Mac does not support booting Windows from a USB drive, this can be overcome by modifying the Bootcamp Assistant application.
 
## Installing Windows

Make sure the Windows USB drive is connected to your computer, then restart while holding the option key to display the boot chooser. A USB drive labelled "EFI Boot" should appear... Select that, and your Mac should boot into the Windows installer!

When the Windows installer reaches the setup screen, hit shift+F10, which will bring up a command prompt. Enter the following commands one-by-one to reformat your internal drive and add the supporting partitions:

```
diskpart
select disk 0
clean
convert gpt
create partition efi size=200
format fs=fat32
create partition msr size=128
```
    
For the next part, you will need to have decided how much space you would like to dedicate to Windows, then calculate that amount in terms of megabytes. The units used by diskpart are such that one gigabyte is equal to 1,024 megabytes, so if you would like your Windows partition to be 100 gigabytes, you will be using the number 102400 in the following command:

```
create partition primary size=102400
Finally, format the new partition and exit:
format fs=ntfs quick label=Windows
exit
```
	
You can now close the command prompt window, and proceed to follow along with the guided Windows installation. Once completed, you should have an operational Windows system! Once booted into it, go ahead and and install the Window support software that Bootcamp Assistant placed on your USB drive.

At this point, your Mac should be booting into Windows automatically every time you restart... Don't worry though, we'll have OS X back up and running soon.
 
## Reinstating OS X

If you created an external OS X recovery drive to boot from, you may now reboot while holding the option key and select it. If you are relying in internet recovery, simply reboot while holding command+option+R and you should be greeted by a spinning globe.

Once in the recovery system, open Disk Utility. Select your disk and navigate to the "Partitions" tab. You will see a large empty space following your Windows partition. We will be adding two new partitions to fill this space, so click the "+" button twice.

The first new partition will be your OS X partition. Set its format to Mac OS Extended (Journaled), and name it something like "Macintosh HD" or "Mac OS X."

The second new partition will be what allows you to choose Windows to boot from. Set its format to Mac OS Extended (Journaled), name it "Windows Boot," and set its size to a mere 128 MB.
Click "Apply" and allow the process to complete! 

Once that's done, you may reinstate your data to the OS X partition from the Time Machine backup, or use Disk Utility to restore a CarbonCopyCloner clone to it, or simply install OS X from scratch.
 
## Enabling Windows Boot Option

Once you've booted into your newly reinstated OS X system, we must now configure that "Windows Boot" partition we previously created... This will allow Windows to show up in the boot manager when holding the option key on startup.

To do this, we will be setting up an EFI script which points your Mac to Windows's bootloader. To run EFI scripts, you will need a simple EFI shell program, which you can download from Intel's repository [here][intellink]. Copy that "Shell_Full.efi" file to your "Windows Boot" partition.

To create the script itself, open Terminal from your Applications' Utilities menu, and copy and paste in the following line: `sudo nano /Volumes/Windows\ Boot/startup.nsh`

A Terminal-based text editor will open up, into which you paste the following line: `blk0:\EFI\Microsoft\Boot\bootmgfw.efi`

To save the file and exit, hit control+X, then the Y key, and then finally the Return key. Check to make sure the file named "startup.nsh" is now in your "Windows Boot" partition.

Finally, we are now going to mark this partition as bootable! Copy and paste the following line into Terminal to do so: `sudo bless --folder /Volumes/Windows\ Boot --file /Volumes/Windows\ Boot/Shell_Full.efi --options 'set StartupDelay 0' --label Windows`

That's it! You can now reboot while holding the option key, and you should be presented with both your Mac and Windows options to boot from. Try the Windows one and make sure it works!

If upon attempting to boot into Windows you are greeted with the persistent error 0xc00000e (or something of the like), then Disk Utility has mistakenly attempted to "fix" a nonexistent Bootcamp setup. This can be repaired by downloading and installing GPT fdisk, then running the following in Terminal: `sudo gdisk /dev/disk0 <<<$'x\nn\nw\ny\n'`
 
[intellink]: https://svn.code.sf.net/p/edk2/code/trunk/edk2/EdkShellBinPkg/FullShell/X64/Shell_Full.efi
 
## Changing Boot Option Icons

This part is of course optional, but is quite easy.
You can use any image you choose as a the icon, so long as it opens in Preview. If you'd like, feel free to use the ones I showed in the example earlier.

Open your image of choice in Preview, then hit command+C to copy that image to the clipboard. To change OS X's icon, select your OS X partition hit command+I to bring up its Info panel... Click on its icon in the panel such that it becomes highlighted, then hit command+V to paste the new one in. To change Windows's icon, simply select the "Windows Boot" partition and follow the same steps.

## OLD INSTRUCTIONS! - Use as reference 
### Install Windows on pure GPT (dual boot with OSX)

#### Preparation
  
You will need:

1. An **OSX** installation usb.
2. A **Windows** installation usb/dvd.
3. Temp **Windows** activation key: XHQ8N-C3MCJ-RQXB6-WCHYG-C9WKB

The easiest way to copy the **Windows 8 ISO** to a memory stick from **OSX** is to mount the *ISO* by double-clicking the file and format a *USB flash drive* in *MS-DOS (FAT)* with *Disk Utility*. Then open *Terminal* and enter the following: `sudo ditto <drag mounted Windows Installer Here> <drag mounted USB Drive here>`

That's it! Give it a few minutes to complete. When it's done, unmount both and remove the *USB Drive*. I recommend that any external drives are removed until this install is complete.

#### Resize OSX
  
Insert your **OSX** Installer drive and startup from it. Proceed to run *Disk Utility*. Select your boot drive (*The drive above your Boot Volume*).
  
Select the *Partition* tab and then adjust the size of your **Mac OS** volume to clear the space for **Windows** below it.
  
Press Apply to confirm the changes. If your **OSX **install has a *Recovery partition*, it will have automatically moved to immediately after the reduced partition. Quit out of *Disk Utility* and select *Shut Down* from the *Apple menu*

#### Install Windows
  
Remove the **OSX** Install drive and insert the **Windows 8** Install disk. Press the key to select *Startup Drive* for your computer and boot the *Windows 8 Installer*.  
  
At the initial Installation screen, press `<Shift> + <F10>` to open the *Command Prompt* screen. Enter *diskpart* and then the following commands:  
 
```
list disk
select disk X (*where Windows and OSX sit*)
list partition
create partition msr size=128
create partition primary(*used for the main Windows partition*)
format fs=ntfs quick label=Windows
assign letter=C
select partition 1 (*EFI partition*)
assign letter=S
list volume(*note the assigned letter for the install usb device*)
exit
```

We have now created all the partitions necessary for a proper **Windows 8** install. Now we need to copy over the *Installation files* to the **Windows** partition. Enter the following commands:  
 
```
cd \Windows\System32\
dism /apply-image /imagefile:D:\sources\install.wim /index:1 /applydir:C:\
```

The **dism** (*Deployment Servicing and Management*) tool is now installing the typical **Windows 8** install to your **Windows** partition. This is going to take a few minutes. Now's a great time to take a break and stretch…  
  
Next we'll install the Windows EFI boot files:  
 
```
C:
bcdboot C:\Windows /l en-us /s S:
```

You should receive a message that the boot files were successfully copied. You should now shutdown and remove your *Windows Installer drive*.
  
Restart your computer pressing the necessary key to select the boot drive and select the *EFI Partition*. If you used **Clover**, simply select the *Windows EFI Startup*. **Windows** should proceed to complete the installation. When prompted, enter your *Activation information*.
