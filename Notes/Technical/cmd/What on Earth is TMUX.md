# What on Earth is TMUX and How Can We Stop It?

Not really sure what the "How Can We Stop it?" bit refers to. I guess it made sense at the time of writing…

## What is it?

**TMUX** is a terminal multiplexer: oh course it is! ¿Qué? A terminal multiplexer enables one to create a number of terminals that can be accessed and displayed on a single screen. Each *session* can also be detached and run in the background waiting to be reattached. The crux of the matter is that **TMUX** allows one to open many terminal windows up at the same time and display any way you wish. Open *vim*, create a vertical split and open *lynx*, create a horizontal split beneath and chuck in a basic free terminal. Need more panes? Running out of screen space? Create a new window and set up some more panes then simply switch between when the mood arises. 

## Let's Stop It (*hacer que funcione en la realidad*) / Configuration

As with all things in *unix* world there's a handy file one can edit to configure **TMUX**. By default this file doesn't actually exist so you can either create it and then edit or grab an example file to edit. Since we have no idea what the file should look like let's grab a copy `cp /usr/local/share/tmux/example_tmux.conf ~/.tmux.conf`. Mess around with colours and what not but pay attention to the important things. **TMUX** is largely operated with keyboard shortcuts that one simply has to learn in order to effectively use the program. All shortcuts are proceeded with the 'prefix' that is by default set to `C-b` (*that means control + b*). We don't like that shortcut as the keys are too far apart and the combination isn’t natural enough for constant use. If you do actually like it then by all means keep using it otherwise take a look at the config file. The example config changes the 'prefix' to `C-a`. I personally have set mine to `C-Space` but bear in mind how you use **TMUX** and any potential conflicts with other software. In any case have a read through the example as it is well commented.

### Copy and Paste

*30^th December 2017 Update*: This section is depreciated. The most recent version of **TMUX** no longer needs `reattach to user namespace` to get functioning copy & paste. You can safely ignore all of the following nonsense.

Why do we need an entry for copy and paste? It's a pain, at least on **OS X**. In order to have functioning, clipboard-enabled, c&p we need to do some stuff first. First of all, install this : `brew install reattach-to-user-namespace`. This is a small utility that works with **TMUXs** 'copy-pipe' feature (introduced in version 1.8) and sends the sends the data through 'pbcopy'. In order to make these two pieces of software harmonise one must add the following to their config file : 

```sh
# Use vim keybindings in copy mode
setw -g mode-keys vi

# Setup 'v' to begin selection as in Vim
bind-key -t vi-copy v begin-selection
bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"

# Update default binding of `Enter` to also use copy-pipe
unbind -t vi-copy Enter
bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbc
opy"
```
Rest assured folks, I nabbed the above script from a website in **Lynx**, in another **TMUX** pane using *vim-styled* **V**isual selection and the system clipboard. And it was good.

## Commands

From here on out we will append commands with 'prefix' rather than `C-b` as you've probably changed the default. We will also just list shortcuts now as this is the meat and drink of **TMUX**. Observe.

### Session Management

* Start a new named session - `tmux new -s $NAME`
* Attach to a session - `tmux attach -t $NAME`
* Kill session - `tmux kill session -t $NAME`
* Detach session - `prefix d`
* Suspend session - `prefix C-z` 
* Switch to another named session - `tmux switch -t $NAME`
* Switch session with menu - `prefix s`
* Rename session - `prefix $`
* List sessions - `tmux list-sessions` | `tmux ls`

#### Sessions within TMUX

* New session - `:new`
* List Sessions - `prefix s`
* Name session - `prefix $`
* Previous/Next session - `prefix (` or `prefix )`
* Last session - `prefix L`

### Windows

* Create a window - `prefix c`
* Move to window n - `prefix n`
* Kill a window with prompt - `prefix &`
* Choose window with menu - `prefix w`   
* Rename window - `prefix ,`
* Cycle to next window - `prefix n`
* Cycle to previous window - `prefix p`
* Refresh current display - `prefix r`

### Panes

* Kill a pane with prompt - `prefix x`
* Expand selected pane to full window, toggle - `prefix z`
* Split pane horizontally - `prefix "`
* Split pane vertically - `prefix %`
* Rotate panes - `prefix C-o`
* Switch to next pane - `prefix o`
* Switch pane - `prefix [UDLR]`
* Cycle through pane layouts - `prefix space`
* Resize pane in small increments - `prefix alt+[UDLR]`
* Swap panes with another - `prefix { or }`
* Break the current pane out of the window - `prefix !`
* Change vertical pane to horizontal or vice versa - `prefix Space`
* Display pane numbers - `prefix q` 
* Toggle pane sync - `:setw synchronize-panes`

### Other

* View commands - `prefix ?`
* Scroll pane using vim-style controls - `prefix [`
* Lists session info, details - `tmux info`
* Reloads **tmux** config files - `tmux source-file ~/.tmux.conf`
* Enter **tmux** command prompt - `prefix :` 
* Show big time display in pane - `prefix t`

## Links

[Very basic **TMUX** tutorial](http://eoinoc.net/tmux-for-noobs/)
[A **TMUX** Crash Course](https://robots.thoughtbot.com/a-tmux-crash-course)
[Learn X in Y Minutes : Scenic Programming](https://learnxinyminutes/com/docs/tmux)
[Love, hate and **TMUX**](https://robots.thoughtbot.com/love-hate-tmux)
[Practical **TMUX**](https://mutelight.org/practical-tmux)
[HyperPolyGlot Multiplexers](http://hyperpolyglot.org/multiplexers)
[**TMUX** copy paste on OS X a better future](https://robots.thoughtbot.com/tmux-copy-paste-on-os-x-a-better-future)
[TMUX Cheatsheet](https://www.outcoldman.com/cheatsheets/tmux/)
