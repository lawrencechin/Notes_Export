# Utilizando Macports

[Enlace de referencia](https://guide.macports.org/#using.port) por más órdenes.

**Homebrew** no funciona como me gustaría en mí ordenador antiguo así que necesito un solución nueva. La respuesta es **¡Macports!**. No he utilizado ese gestor de paquetes anteriormente así que se debe anotar las órdenes y funciones aquí. Con suerte, en el futuro no necesitaré a referencer a esto documento pero ahora…

## Actualizaciones

- `sudo port (-v) selfupdate` - básico orden que actualizar el principal gestor de paquetes
- `port diagnose` - comprueba asuntos comunes con la instalación
- `port reclaim` - intenta a recuperar espacio elimina ports desactivados
- `port installed` - alista ports instalado
- `port installed inactive` - paquetes que están instalado pero no funcionan porque una versión nueva ha sido instalado. Pueden salvar la paquete o `uninstall` con:
- `sudo port uninstall inactive` - autoexplicativo
- `port search (--name, --line, --glob ) $PACKAGE` - busca término, la orden busca nombre y descripción así que `--name` es sólo nombre
- `port info $PACKAGE` - descripción de paquete 
- `sudo port install $PACKAGES` - se explica por sí solo
- `port outdated` - alista paquetes que están anticuadas y tienen actualizaciones nuevas para instalar

