# Curl Exercises
Julia Evans | [Link](https://jvns.ca/blog/2019/08/27/curl-exercises/)

Recently I've been interested in how people learn things. I was reading Kathy Sierra's great book [Badass: Making Users Awesome](https://www.amazon.com/Badass-Making-Awesome-Kathy-Sierra/dp/1491919019). It talks about the idea of deliberate practice.

The idea is that you find a small micro-skill that can be learned in maybe 3 sessions of 45 minutes, and focus on learning that micro-skill. So, as an exercise, I was trying to think of a computer skill that I thought could be learned in 3 45-minute sessions.

I thought that making HTTP requests with `curl` might be a skill like that, so here are some curl exercises as an experiment!

## What's `curl`?

`curl` is a command line tool for making HTTP requests. I like it because it's an easy way to test that servers or APIs are doing what I think, but it's a little confusing at first!

### Basic API

* `curl $address` - perform GET request to $address
* `-i` - show response headers
* `-I` - only show response headers
* `-X POST` - send POST request. Can be replaced with PUT or DELETE
* `-H` - set headers to send with request e.g. `curl $address -H "Accept-Encoding: gzip`
* `-v, -vv` - verbose mode
* `-k` - insecure, don't verify security settings/certificates
* `--cert, --key` - use a client certificate
* `--data, -d` - send a POST request with data e.g. `curl $address -d '{ "name" : "king' }'` or `curl $address @file.json`. The send option will send a file which is handy if the data is quite large.
* `copy as curl` - in some developers tools this option is presented allowing one to download from the command line.

## 21 curl exercises

These exercises are about understanding how to make different kinds of HTTP requests with curl. They're a little repetitive on purpose. They exercise basically everything I do with curl.

To keep it simple, we're going to make a lot of our requests to the same website: <https://httpbin.org>. httpbin is a service that accepts HTTP requests and then tells you what request you made.

1. Request <https://httpbin.org>
1. Request <https://httpbin.org/anything>. httpbin.org/anything will look at the request you made, parse it, and echo back to you what you requested. curl's default is to make a GET request.
1. Make a POST request to <https://httpbin.org/anything>
1. Make a GET request to <https://httpbin.org/anything>, but this time add some query parameters (set `value=panda`).
1. Request google's robots.txt file (<www.google.com/robots.txt>)
1. Make a GET request to <https://httpbin.org/anything> and set the header `User-Agent: elephant`.
1. Make a DELETE request to <https://httpbin.org/anything>
1. Request <https://httpbin.org/anything> and also get the response headers
1. Make a POST request to <https://httpbin.com/anything> with the JSON body `{"value": "panda"}`
1. Make the same POST request as the previous exercise, but set the `Content-Type` header to `application/json` (because POST requests need to have a content type that matches their body). Look at the json field in the response to see the difference from the previous one.
1. Make a GET request to <https://httpbin.org/anything> and set the header `Accept-Encoding: gzip` (what happens? why?)
1. Put a bunch of a JSON in a file and then make a POST request to <https://httpbin.org/anything> with the JSON in that file as the body
1. Make a request to <https://httpbin.org/image> and set the header `Accept: image/png`. Save the output to a PNG file and open the file in an image viewer. Try the same thing with with different Accept: headers.
1. Make a PUT request to <https://httpbin.org/anything>
1. Request <https://httpbin.org/image/jpeg>, save it to a file, and open that file in your image editor.
1. Request <https://www.twitter.com>. You'll get an empty response. Get curl to show you the response headers too, and try to figure out why the response was empty.
1. Make any request to <https://httpbin.org/anything> and just set some nonsense headers (like `panda: elephant`)
1. Request <https://httpbin.org/status/404> and <https://httpbin.org/status/200>. Request them again and get curl to show the response headers.
1. Request <https://httpbin.org/anything> and set a username and password (with `-u username:password`)
1. Download the Twitter homepage (<https://twitter.com>) in Spanish by setting the `Accept-Language: es-ES header`.
1. Make a request to the Stripe API with curl. (see <https://stripe.com/docs/development> for how, they give you a test API key). Try making exactly the same request to <https://httpbin.org/anything>.
