# Version Control using Dropbox

## Introduction

This is a quick primer on using **Dropbox** in conjunction with **git** to synchronise projects between machines. The instructions are very straightforward but alas one feels one will forget a step here or there. 

## Setup the 'bare' repository

We shall demonstrate using a fictional project named *Ultimate_Warrior_Fan_Site*. There are two locations we must pay attention to. We create a 'bare' repository in a specific **Dropbox** folder with the project name.

```sh
# jump to the dev folder using "$DEV"
cd ~/Dropbox/Documents/@Development/Sites/
git init --bare ultimate_warrior_fan_site.git
# done!
```
## Setup the project, or use a current one

The next part is to create the actual project with all files and what not. The location of the project(s) are entirely up to you but do not put them in **Dropbox** as that would defeat the purpose.

```sh
cd ~/Sites/ultimate_warrior_fan_site
# use a function to generate the ignore file
# crucial if you do not want a million node modules committed
genGitIgnore ~/Sites/ultimate_warrior_fan_site/.gitignore
git init
git add . # this will add all contents of the folder
git commit -m "Changed the theme song"
# hook up the bare repo to the actual repo
git remote add origin "$DEV"Sites/ultimate_warrior_fan_site.git
git push -u origin master
```

At this point in time you should see **Dropbox** spinning furiously in the menubar. Once it has completed you're good to go. From here you can easily clone the repo on other machines with **Dropbox** installed. Just remember that **Dropbox** isn't **GitHub** so don't be foolish. Always wait for **Dropbox** to finish syncing changes before doing anything else while multiple people working on the same repo at the same time will likely end in tears.





