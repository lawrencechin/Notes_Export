# [Git Reference](https://rogerdudler.github.io/git-guide/)  

* `git init` - create a new repository.
* `git push origin $BRANCH` - push to a specific branch.
* `git remote add origin $SERVER` - connect repository to remote server.
* `git checkout -b $BRANCH` - create a new branch and switch to it.
* `git push origin $BRANCH` - until you push the branch to the remote server no one else can access it.
* `git checkout master` - switch back to master.
* `git branch -d $BRANCH` - delete branch.
* `git merge $BRANCH` - merge current branch to active branch.
* `git diff $BRANCH $ACTIVE` - check differences between branches in case of merge conflicts.
* `git checkout -- $FILENAME` - replace local file with latest file commit.
* `git fetch origin && git reset --hard origin/master` - rest local changes to last commit for all files.
* `git clone --recurse-submodules -j8 git://github.com/foo/bar.git` - my repos have sub modules so use this command to fetch them along with the main repository.
* `git push --set-upstream git@gitlab.com:$USERNAME/repo.git master` - create remote repository on **Gitlab** from the terminal.
* `git submodule update --init --recursive` - this is similar to the above but allows one to grab the submodules after cloning the master repository. The `--recursive` option grabs any submodules from the submodules from the sub…
* `git submodule update --remote --merge` - updates and merges changes to submodules to parent repository.
* `git stash` - allows you to *stash* changes that you are not ready to commit and update the repository with latest data.
* `git stash clear` - remove all stashes.
* `git stash show $STASH` - shows the changes between current repo state and selected stash.
* `git stash pop/apply` - attempts to apply the stash, will use the latest stash if not otherwise selected, `pop` will `drop` the used stash after being applied whilst `apply` will not.
