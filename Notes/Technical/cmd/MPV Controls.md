# MPV Controls
## Keyboard Controls

* LEFT and RIGHT | Seek backward/forward 5 seconds. Shift+arrow does a 1 second exact seek (see --hr-seek).
* UP and DOWN | Seek forward/backward 1 minute. Shift+arrow does a 5 second exact seek (see  --hr-seek).
* Ctrl+LEFT and Ctrl+RIGHT | Seek to the previous/next subtitle. Subject to some restrictions and might not always work; see sub-seek command.
* [ and ] | Decrease/increase current playback speed by 10%.
* { and } | Halve/double current playback speed.
* BACKSPACE | Reset playback speed to normal.
* < and > | Go backward/forward in the playlist.
* ENTER | Go forward in the playlist.
* p / SPACE | Pause (pressing again unpauses).
* . | Step forward. Pressing once will pause, every consecutive press will play one frame and then go into pause mode again.
* , | Step backward. Pressing once will pause, every consecutive press will play one frame in reverse and then go into pause mode again.
* q | Stop playing and quit.
* Q | Like q, but store the current playback position. Playing the same file later will resume at the old playback position if possible.
* / and * | Decrease/increase volume.
* 9 and 0 | Decrease/increase volume.
* m | Mute sound.
* \_ | Cycle through the available video tracks.
* \# | Cycle through the available audio tracks.
* f | Toggle fullscreen (see also --fs).
* ESC | Exit fullscreen mode.
* T | Toggle stay-on-top (see also --ontop).
* w and e | Decrease/increase pan-and-scan range.
* o (also P) | Show progression bar, elapsed time and total duration on the OSD.
* O | Toggle OSD states between normal and playback time/duration.
* v | Toggle subtitle visibility.
* j and J | Cycle through the available subtitles.
* x and z | Adjust subtitle delay by +/- 0.1 seconds.
* l | Set/clear A-B loop points. See ab-loop command for details.
* L | Toggle infinite looping.
* Ctrl + and Ctrl - | Adjust audio delay by +/- 0.1 seconds.
* u | Switch between applying no style overrides to SSA/ASS subtitles, and overriding them almost completely with the normal subtitle style. See --sub-ass-style-override for more info.
* V | Toggle subtitle VSFilter aspect compatibility mode. See  --sub-ass-vsfilter-aspect-compat for more info.
* r and t | Move subtitles up/down.
* s | Take a screenshot.
* S | Take a screenshot, without subtitles. (Whether this works depends on VO driver support.)
* Ctrl s | Take a screenshot, as the window shows it (with subtitles, OSD, and scaled video).
* I | Show filename on the OSD.
* PGUP and PGDWN | Seek to the beginning of the previous/next chapter. In most cases, "previous" will actually go to the beginning of the current chapter; see --chapter-seek-threshold.
* Shift+PGUP and Shift+PGDWN | Seek backward or forward by 10 minutes. (This used to be mapped to PGUP/PGDWN without Shift.)
* d | Activate/deactivate deinterlacer.
* A | Cycle aspect ratio override.

## Keys that only Work when the Video Output Supports the Option

(The following keys are valid only when using a video output that supports the corresponding adjustment, or the software equalizer (--vf=eq).)

* 1 and 2 | Adjust contrast.
* 3 and 4 | Adjust brightness.
* 5 and 6 | Adjust gamma.
* 7 and 8 | Adjust saturation.
* Alt+0 (and command+0 on OSX) | Resize video window to half its original size.
* Alt+1 (and command+1 on OSX) | Resize video window to its original size.
* Alt+2 (and command+2 on OSX) | Resize video window to double its original size.
* command + f (OSX only) | Toggle fullscreen (see also --fs).
* command + [ and command + ] (OSX only) | Set video window alpha.

## Keyboards with Multimedia Keys

(The following keys are valid if you have a keyboard with multimedia keys.)

* PAUSE
* Pause.
* STOP
* Stop playing and quit.
* PREVIOUS and NEXT
* Seek backward/forward 1 minute.

## MPV Compiled with TV or DVB Input Support

(The following keys are only valid if you compiled with TV or DVB input support.)

* h and k | Select previous/next tv-channel.
* H and K | Select previous/next dvb-channel.

## Mouse Control

* button 3 and button 4 | Seek backward/forward 1 minute.
* button 5 and button 6 | Decrease/increase volume.
