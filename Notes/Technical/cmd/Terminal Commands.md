# Terminal Commands

## Basics

`pwd` - print working directory  
`ls` - list directory contents  

- `-a` - show hidden files
- `-l` - display in columns
- `-t` - order by last modified date

`cd $DIR` - change directory  
`cd ..` - go up one directory
`mkdir $DIR` - create directory
`touch $FILE` - create file
`cp $FILE_1 $FILE_2` - copy one file to another. The last argument will always be the destination so one can copy multiple files at the same time  
`mv $FILE_1 $DIR` - move files to new directory
 
- `mv $FILE_1 $FILE_2` - you can use the mv command to rename files

`rm $FILE` - remove file  

- `-r` - recursive, can be used to remove directories which are not empty

`echo "String"` - print to standard output 
 
- `echo "String" > hello.txt` - redirect *stdout* to file (file will be overwritten if exists)
- `-e` - can print new lines (`\n`) and colours
- `2>` - redirect *stderr* to file
- `&>` - redirect *stdout* & *stderr* to file
- `cat $FILE_1 >> $FILE_2` - append file\_1 to file\_2
- `<` - changes the 'flow' sending the right hand assignment to the left
- `|` - the *pipe*. Takes *stdout* from left assignment and pipes it as *stdin* to command on the right 

`cat $FILE` - output contents on file to terminal  
`wc` - outputs the number of lines, words and characters  
`sort` - takes *sdin* and sorts alphabetically  
`uniq` - filters out adjacent, duplicate lines in file. Use in conjunction with 'sort' to filter out lines that are duplicate but not adjacent  
`grep` - global regular expression print

- `-i` - case-insensitive search
- `-R` - searches all files in a directory
- `-l` - only outputs files with matches
- *nb.* an excellent alternative to `grep` is `ripgrep` (`rg`). Most of the syntax is the same.
	
`sed s/snow/rain` - stream editor. Accepts *stdin* and modifies it based on expression. In this example only the first match, per line, will be changed

- `s` - substitution
- `snow` - search string
- `rain` - replace string
- `/g` - global replace
 
`clear` - clear the current **terminal** display  
`source` - activate bash profile and make it's changes active in current session  
`alias something="pwd"` - create an alias that runs specified command  
`history` - print out **terminal** history  
`PS1` - environment variable that defines the style of the command prompt  
`env` - lists environment variables set for current session  

`Ctrl+z` - suspends a running command

- `jobs` - lists suspended processes
- `bg %n` - runs selected, suspended process in the background
- `fg %n` - resumes selected process normally in foreground
- `command &` - run process and send to background
- `command > output.log 2>&1 &` - run process, send to background and capture output, including *stdout* and *stderr*, to output.log. Use */dev/null* if you don't need the output

## macOS Tools (via [Saurabh 😎](https://saurabhs.org/))

macOS is fortunate to have access to the huge arsenal of standard Unix tools. There are also a good number of macOS-specific command-line utilities that provide unique macOS functionality. To view the full documentation for any of these commands, run `man <command>`.

- `caffeinate` - set Mac sleep behavior
    - Running `caffeinate` with no flags or arguments prevents your Mac from going to sleep as long as the command continues to run.
    - `caffeinate -u -t <seconds>` prevents sleep for the specified number of seconds.
    - Adding the `-d` flag also prevents the display from going to sleep.
    - Specifying an existing process with `-w <pid>` automatically exits the caffeinate command once the specified process exits.
    - Passing a command with `caffeinate <command>` starts the given command in a new process and will prevent sleep until that process exits.
- `textutil` - document file converter
    - `textutil` can convert files to and from Microsoft Word, plain text, rich text, and HTML formats.
    - `textutil -convert html journal.doc` converts journal.doc into journal.html.
    - The possible values for `-convert` are: txt, html, rtf, rtfd, doc, docx.
- `mdfind` - search with Spotlight
    - `mdfind <query>` performs a keyword-based Spotlight search with the given query.
    - `mdfind kMDItemAppStoreHasReceipt=1` finds all apps installed from the Mac App Store.
    - `mdfind -name <name>` searches for all files matching the given name.
    - The `-onlyin <dir>` flag restricts the search to the given directory.
- `networkQuality` - measure Internet speed
    - Run `networkQuality` to run an Internet speed test from your Mac.
    - Add the `-v` flag to view more detailed information.
    - Use the `-i` flag to run the network test on a specific network interface.
- `screencapture` - take screenshots
    - `screencapture -c` takes a screenshot and copies it to the clipboard.
    - `screencapture <file>` takes a screenshot and saves it to the given file.
    - Add the `-T <seconds>` flag to take the screenshot after the given delay in seconds.
- `pbcopy`, `pbpaste` - interact with system clipboard
    - `<command> | pbcopy` copies the output of the command to the clipboard.
    - `pbpaste` outputs the contents of the clipboard to stdout.
- `taskpolicy` - control scheduling of processes
    - `taskpolicy -b <command>` starts executing the given command in the background. On Apple silicon Macs, the process will only run on the efficiency cores.
    - `taskpolicy -b -p <pid>` downgrades an existing process to run in the background.
    - `taskpolicy -B -p <pid>` removes the specified process from running in the background. On Apple silicon Macs, the process may now run on the efficiency or performance cores. Note that this only works on processes that have been downgraded to the background, and not processes that started in the background.
    - `taskpolicy -s <command>` starts the given command in the suspended state. This is useful to allow a debugger to attach to the process right at the start of execution.
- `say` - text-to-speech engine
    - `say <message>` announces the given message.
    - `say -f input.txt -o output.aiff` will create an audiobook from the given text file.
- `sips` - image manipulation
    - `sips -z <height> <width> <image>` resizes the specified image, ignoring the previous aspect ratio.
    - `sips -Z <size> <image>` resizes the largest side of the specified image, preserving the aspect ratio.
- `sips -r <degrees> <image>` rotate the image by the specified degrees.
- `open` - open files and applications
    - `open -a <app> <file>` opens the given file with the specified application.
    - `open .` opens the current directory in a new Finder window.
- `pmset` - configure power management
    - `pmset -g` prints all available power configuration information.
    - `pmset -g` assertions displays information about power-related assertions made by other processes. This can be useful for finding a process that is preventing your Mac from going to sleep.
    - `pmset -g thermlog` displays information about any processes that have been throttled (useful when running benchmarks).
    - `pmset displaysleepnow` immediately puts the display to sleep without putting the rest of the system to sleep.
    - `pmset sleepnow` immediately puts the entire system to sleep.
- `networksetup` - configure network settings
    - `networksetup -listnetworkserviceorder` prints a list of available network services.
    - `networksetup -getinfo <networkservice>` prints information about the specified network service.
    - `networksetup -getdnsservers <networkservice>` prints the DNS servers for the specified network service.
    - `networksetup -setairportnetwork <device> <network> [password]` joins the specified Wi-Fi network. (In most cases, the <device> argument should be "en0".)
- `qlmanage` - manage Quick Look
    - `qlmanage -p <file>` opens a Quick Look preview window of the given file.
    - `qlmanage -m` prints status information about the Quick Look server process.
    - `qlmanage -r` restarts the Quick Look server process.
    - `qlmanage -r` cache resets the Quick Look thumbnail cache.
- `softwareupdate` - manage OS updates
    - `softwareupdate` --list prints out available software updates.
    - `sudo softwareupdate -ia` installs all available updates.
    - `softwareupdate --fetch-full-installer --full-installer-version <version>` tries to download the full installer of the specified macOS version to /Applications.
