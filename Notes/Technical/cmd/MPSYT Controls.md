# MPYST Controls

## Searching

`set search_music false` : search all YouTube categories or set to true and search music only.

* /<query> : search for videos
* //<query> : search for YouTube playlists
* n and p : next and previous pages
* p<n> : switch to page number n
* album <album title> : search for album title
* pl <url or id> : opens selected playlist
* url <url or id> : opens specific item
* url_file <file_absolute_path> : retrieves items from a text file with a url or id on separate lines

## Results page

After a search, conducted by typing `/$TERM`, use the following commands :
* i <n> : view info on n
* c <n> : view comments (*why?*)
* d <n> : down n
* r <n> : show related to n
* u <n> : show uploaders other items
* x <n> : copy item <n> url to clipboard

## Downloading and Playback

`set show_video true` : play video instead of audio.

* <number(s)> : play specified items, separated by commas.
* d <number> : view downloads available for an item.
* da <number(s)> : download best available audio file(s).
* dv <number(s)> : download best available video file(s).
* dapl <url or id> : download YouTube playlist (audio) by url or id.
* dvpl <url or id> : download YouTube playlist (video) by url or id.
* daupl <username> : download user's YouTube playlists (audio).
* dvupl <username> : download user's YouTube playlists (video).
* dlurl <url or id> : download a YouTube video by url or video id.
* daurl <url or id> : download best available audio of YouTube video by url or video id.
* playurl <url or id> : play a YouTube video by url or id.
* browserplay <number> : open a specified previous search in browser.
* all or * : play all displayed items.
* repeat <number(s)> : play and repeat the specified items.
* shuffle <number(s)> : play specified items in random order.
