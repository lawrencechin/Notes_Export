#!/bin/bash

#A script to make DuckDuckGo the default search provider on IOS7 and to update the hosts file using data from someonewhocares.org

#variables
SEARCHENGINES=/Users/lawrencechin/Library/Mobile\ Documents/com\~apple\~CloudDocs/iPhone\ Files/SearchEngines.plist 
SEPATH=/User/Library/Safari/SearchEngines.plist

SEARCHENGINESFALLBACK=/Users/lawrencechin/Library/Mobile\ Documents/com\~apple\~CloudDocs/iPhone\ Files/SearchEnginesFallback.plist 
SEFPATH=/Applications/MobileSafari.app/SearchEnginesFallback.plist

HOSTS=/Users/lawrencechin/Library/Mobile\ Documents/com\~apple\~CloudDocs/iPhone\ Files/hosts
HPATH=/etc/hosts
HOSTSLINK=http://someonewhocares.org/hosts/hosts

#toggle wifi before continuing
echo "Make sure to turn WIFI off and then on again before continuing"
echo "Continue ? Y/N"
read answer

#exit script if not Y or y
[ "$answer" != "Y" || $answer != "y" ] && exit 1	
#download hosts file
curl HOSTSLINK > HOSTS
echo "Hosts file updated"
echo "Enter iPhone IP address: "
read ipaddress

scp SEARCHENGINES root@$ipaddress:"SEPATH"
scp SEARCHENGINESFALLBACK root@$ipaddress:"SEFPATH"
scp HOSTS root@$ipaddress:"HPATH"

echo "All done! Bye"