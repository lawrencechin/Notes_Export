# A Picture is Worth a 1000 Words

![](./@imgs/a_picture_is_worth/header.jpg)

A collection of doodles explaining concepts in **GIT**, **Machine Learning**, **Web Development** and **Algorithms** by the awesome [*Girliemac*](https://github.com/girliemac/a-picture-is-worth-a-1000-words).

## Algorithms

[![Arrays and Linked Lists](./@imgs/a_picture_is_worth/algorithms/algorithm101_Array.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_Array.jpg)
[![Binary Search Tree Part 1](./@imgs/a_picture_is_worth/algorithms/algorithm101_BST_01.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_BST_01.jpg)
[![Binary Search Tree Part 2](./@imgs/a_picture_is_worth/algorithms/algorithm101_BST_02.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_BST_02.jpg)
[![BigO Part 1](./@imgs/a_picture_is_worth/algorithms/algorithm101_BigO_01.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_BigO_01.jpg)
[![BigO Part 2](./@imgs/a_picture_is_worth/algorithms/algorithm101_BigO_02.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_BigO_02.jpg)
[![Hash Table Part 1](./@imgs/a_picture_is_worth/algorithms/algorithm101_Hash_Table_01.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_Hash_Table_01.jpg)
[![Hash Table Part 2](./@imgs/a_picture_is_worth/algorithms/algorithm101_Hash_Table_02.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_Hash_Table_02.jpg)
[![Heap Part 1](./@imgs/a_picture_is_worth/algorithms/algorithm101_Heap_01.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_Heap_01.jpg)
[![Heap Part 2](./@imgs/a_picture_is_worth/algorithms/algorithm101_Heap_02.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_Heap_02.jpg)
[![Linked List](./@imgs/a_picture_is_worth/algorithms/algorithm101_Linked_List.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_Linked_List.jpg)
[![Queue](./@imgs/a_picture_is_worth/algorithms/algorithm101_Queue.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_Queue.jpg)
[![Stack](./@imgs/a_picture_is_worth/algorithms/algorithm101_Stack.jpg)](./@imgs/a_picture_is_worth/algorithms/fullsize/algorithm101_Stack.jpg)

## Git Purr! Git Commands Explained with Cats!


[![Cherry Pick](./@imgs/a_picture_is_worth/git_purr/git-cherry-pick.jpg)](./@imgs/a_picture_is_worth/git_purr/fullsize/git-cherry-pick.jpg)
[![Meowge](./@imgs/a_picture_is_worth/git_purr/git-meowge.jpg)](./@imgs/a_picture_is_worth/git_purr/fullsize/git-meowge.jpg)
[![Git Purr](./@imgs/a_picture_is_worth/git_purr/git-purr.jpg)](./@imgs/a_picture_is_worth/git_purr/fullsize/git-purr.jpg)
[![Git Puss](./@imgs/a_picture_is_worth/git_purr/git-puss.jpg)](./@imgs/a_picture_is_worth/git_purr/fullsize/git-puss.jpg)

## Machine Learning

[![Fairness](./@imgs/a_picture_is_worth/ml/ml-fairness.jpg)](./@imgs/a_picture_is_worth/ml/fullsize/ml-fairness.jpg)
[![History](./@imgs/a_picture_is_worth/ml/ml-history.jpg )](./@imgs/a_picture_is_worth/ml/fullsize/ml-history.jpg)
[![Real World](./@imgs/a_picture_is_worth/ml/ml-realworld.jpg)](./@imgs/a_picture_is_worth/ml/fullsize/ml-realworld.jpg)
[![Regression](./@imgs/a_picture_is_worth/ml/ml-regression.jpg)](./@imgs/a_picture_is_worth/ml/fullsize/ml-regression.jpg)
[![Reinforcement](./@imgs/a_picture_is_worth/ml/ml-reinforcement.jpg)](./@imgs/a_picture_is_worth/ml/fullsize/ml-reinforcement.jpg)
[![Time Series](./@imgs/a_picture_is_worth/ml/ml-timeseries.jpg)](./@imgs/a_picture_is_worth/ml/fullsize/ml-timeseries.jpg)

## Web Development

[![Accessibility](./@imgs/a_picture_is_worth/webdev/webdev101-a11y.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-a11y.jpg)
[![CSS](./@imgs/a_picture_is_worth/webdev/webdev101-css.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-css.jpg)
[![Github](./@imgs/a_picture_is_worth/webdev/webdev101-github.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-github.jpg)
[![HTML](./@imgs/a_picture_is_worth/webdev/webdev101-html.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-html.jpg)
[![JS Arrays](./@imgs/a_picture_is_worth/webdev/webdev101-js-arrays.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-js-arrays.jpg)
[![JS Datatypes](./@imgs/a_picture_is_worth/webdev/webdev101-js-datatypes.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-js-datatypes.jpg)
[![JS Decisions](./@imgs/a_picture_is_worth/webdev/webdev101-js-decisions.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-js-decisions.jpg)
[![JS Functions](./@imgs/a_picture_is_worth/webdev/webdev101-js-functions.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-js-functions.jpg)
[![Javascript](./@imgs/a_picture_is_worth/webdev/webdev101-js.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-js.jpg)
[![Programming](./@imgs/a_picture_is_worth/webdev/webdev101-programming.jpg)](./@imgs/a_picture_is_worth/webdev/fullsize/webdev101-programming.jpg)
