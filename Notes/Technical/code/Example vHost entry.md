# Example vHost entry

File Locations:
* etc/apache2/httpd.conf - main *apache* config file.
* etc/apache2/extra/vhosts-httpd.conf - *vhosts* config file.
* etc/hosts - hosts file.

Once you have enabled *virtual hosts* in your *apache* config you will need to add the new hosts. After doing this you must add an entry to your *hosts* file to redirect traffic to your local server. Here is an example of how it should look:

```sh
### Httpd.conf - *uncomment this line to enable __vhosts__*
	# Virtual hosts
	Include /private/etc/apache2/extra/httpd-vhosts.conf

### vHost setup - *note that you will lose localhost when enabling __vhosts__ but you can add it back here*
	<VirtualHost *:80>
		ServerName localhost
		ErrorLog "/private/var/log/apache2/error_log"
	    CustomLog "/private/var/log/apache2/access_log" common
		DocumentRoot "/Users/lawrencechin/Sites"
			<Directory "/Users/lawrencechin/Sites">
				Options FollowSymlinks Multiviews
				MultiviewsMatch Any
				AllowOverride All
				Require all granted
			</Directory>
	</VirtualHost>
	
	<VirtualHost *:80>
	    ServerAdmin rattenabend@icloud.com
	    DocumentRoot "/Users/lawrencechin/Sites/darkness"
	    ServerName darkness.com
	    ServerAlias www.darkness.com
	    ErrorLog "/private/var/log/apache2/darkness.com-error_log"
	    CustomLog "/private/var/log/apache2/darkness.com-access_log" common
	    	<Directory "/Users/lawrencechin/Sites/darkness">
				Options FollowSymLinks Multiviews
				MultiviewsMatch Any
				AllowOverride All
				Require all granted
	    	</Directory>	
	</VirtualHost>

### hosts - *add all address and alias’ to this file*
	##
	# Host Database
	#
	# localhost is used to configure the loopback interface
	# when the system is booting.  Do not change this entry.
	##
	127.0.0.1	localhost
	127.0.0.1 	darkness.com
	127.0.0.1 	designstacks.com
	127.0.0.1	darkdesign.com
	127.0.0.1 	www.darkness.com
	127.0.0.1 	www.designstacks.com
	127.0.0.1	www.darkdesign.com
	127.0.0.1 	www.medialibrary.dev
	127.0.0.1 	medialibrary.dev
	255.255.255.255	broadcasthost
	::1             localhost 
```

## Additional Notes

* After installing the *homebrew* version of *php* you need to add an entry to the **httpd.conf** file. This entry will be given to you as part of installation so I will not add it here in case it changes.

*  You may need to enable *mod_rewrite* in **httpd.conf** to utilise *.htaccess*.
*  After making any changes to these files you ought to restart apache, `sudo apachectl restart`, for changes to take effect. 
