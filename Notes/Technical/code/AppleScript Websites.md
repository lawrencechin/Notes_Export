# AppleScript Websites

	tell application "Safari"
	make new document
	open location "http://www.bbc.co.uk/iplayer"
	activate
	
	tell application "System Events" to tell process "Safari"
	set v to (value of attribute "AXMenuItemMarkChar" of menu item "Private Browsing" of menu "Safari" of menu bar 1)
	
	if (v is missing value) then
	click menu item "Private Browsing" of menu 1 of menu bar item "Safari" of menu bar 1
	end if
	
	if menu item "Hide Toolbar" of menu "View" of menu bar 1 exists then
	click menu item "Hide Toolbar" of menu "View" of menu bar 1
	end if
	end tell
	
	end tell`