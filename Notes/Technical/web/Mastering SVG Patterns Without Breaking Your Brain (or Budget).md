# [Mastering SVG Patterns Without Breaking Your Brain (or Budget)](https://www.sitepoint.com/mastering-svg-patterns-without-breaking-brain-budget/?utm_source=sitepoint&amp;utm_medium=relatedsidebar&amp;utm_term=javascript)

I want to share a little feature of Jarek Foksa’s [Boxy SVG Editor](http://boxy-svg.com/) that I think is nifty, but perhaps not super obvious to new users.

Patterns are the one of the most useful features of SVG – but it can be a little brain-bending trying to get tiling and scaling to do what you want it to.

Happily Boxy SVG handles pattern creation really nicely – if you know where to look. Here’s a quick run-through.

## Creating SVG Patterns with Boxy SVG.  

![Pattern fill](./@imgs/01/c3dc9eefa7a8dd91628d668cef4407c783a5253b.gif)

1). When you’ve created a new shape, select it and go to the FILL menu and click the stripey Pattern fill tile (shown here).

2). Below the Pattern selector dropdown you’ll see ‘*Add more patterns in DEFS Panel’*. Click on the link to switch to the DEFS panel.

3). Now click the ‘**+**‘ button to start a new pattern tile. This will be a copy of your current pattern to begin with. Double-click it to open it in a new window for editing. Feel free to rename your pattern if you like, but it ***MUST*** begin with a ‘\#’.

4). Delete whatever is currently in the editor view and begin creating your new pattern using the drawing tools. Resize your ’tile canvas’ if you need to by using the ‘VIEW’ panel.

![pattern-constructor](./@imgs/01/6999791d4240e20be37b5277fe2a50dc776fa16b.jpg)

In this demo, I’ve created some overlapping sine curves that should tile perfectly.

5). When you’re done (you can re-edit your pattern any time), click the SVG box at the top-left of screen to switch back to your *main* drawing again.

![Pattern fill](./@imgs/01/70664e07159fee488dbff82862a80f3a7dbfa8f6.jpg)

6). Select a shape, go to the Pattern drop-down in the FILL panel and select your newly created pattern (it should appear in the list now). You should see something like this.

But what about scaling and positioning the pattern?

![Pattern handles](./@imgs/01/ce51f80f2035b8ff5ba8559285d19a48f8680340.jpg)

7). In the tool panel on the left, select the shape Edit tool. You should now see pink pattern control handles *inside* your shape.

Use the pink circle to rotate your pattern. Use the pink square to re-scale it.

![Rotate and position your pattern.](./@imgs/01/ece5624fb9cc2e75c37bd8a6f035784ccc5bb321.gif)

That’s all there is to it. You can use that pattern as many times as you like in the same SVG document, at any SIZE you like without making the file any larger. For instance, I used a tiny section of SVG pattern for the grid in my [Star Trek vs Star Wars SVG chart](http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2016/04/1461758582optimised.svg) recently.

SVG patterns are certainly not a new idea but I’ve never found a tool as fast, easy, and clean as Boxy to create and manipulate them. Open your SVG in your code editor (or paste it into [Codepen.io’s HTML window](http://codepen.io/)) and you’ll see your pattern described inside DEFS tags at the top of your document.

In SVG you can think of SVG patterns (and SVG symbols for that matter) as being a little like \<STYLE\> blocks in the head of your HTML – they don’t do anything when you define them but are super-useful when you refer to them later.

## Pattern Inspiration  

Now you have the power of SVG pattern mastery, how will you use it? Check out Phillip Rogers [SVG Patterns Gallery](https://philiprogers.com/svgpatterns/) and [Patternbolt for some cool ideas](http://buseca.github.io/patternbolt/).

![Patternbolt](./@imgs/01/c4e79aad58bdec062c865ca6a9beac7304f628d4.jpg)

## Final word  

![Markup precision controls](./@imgs/01/c6966225cdc645900d7da16ee2c616ce0ed562a5.jpg)

It’s worth noting that Jarek has been working on Boxy SVG a lot lately and his updates get integrated into the Boxy SVG Chrome App weekly (sometimes daily). For instance, I know this week he added precision markup controls that let you round unruly path coordinates down to something more manageable.

So keep an eye on your Boxy SVG preferences and menu options – new functionality just appears magically some days.
