# [DuckDuckGo Search Syntax](https://duck.co/help/results/syntax)

**DuckDuckGo** supports search syntax you can use to fine-tune your queries.

## Search Operators:

Example | Result
--- | ---
cats dogs | Results about cats and dogs
"cats and dogs" | Results for exact term "cats and dogs"
cats -dogs | Fewer dogs in results
cats +dogs | More dogs in results
cats filetype:pdf | PDFs about cats
dogs site:example.com | Pages about dogs from example.com
cats -site:example.com | Pages about cats excluding results from example.comintitle:dogs | Page title contains the word "dogs"
inurl:cats | Page url includes the word "cats"

## Search Directly on Other Sites:

* Use `\` to go directly to the first search result. For example, `\futurama`
* Use `!` to search other sites' search engines directly. For example, `!a blink182` searches **amazon.com** for blink182. Check out all the '*bangs*' [available](https://duckduckgo.com/bang)

## Instant Answers:

* Add `news` to your searches to generate *Instant Answers* news results. For example , `LeBron James news`
* Add `map` to your search to generate *Instant Answer* map results. For example, `Philiadelphia map`
* Find out more about [*Instant Answers*](https://duck.co/ia)

## Safe Search:

* Add `!safeon` or `!safeoff` to the end of your search to turn on and off *safe search* for that particular query

