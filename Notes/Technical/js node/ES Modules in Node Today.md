# [ES Modules in Node Today!](https://blogs.windows.com/msedgedev/2017/08/10/es-modules-node-today/)

By [Microsoft Edge Team](https://blogs.windows.com/msedgedev/author/msedgeteam/ "Posts by Microsoft Edge Team")

*Editor's Note: Today's post is a guest post from [John-David Dalton](https://twitter.com/sindresorhus/status/861991464858812416), a Program Manager on the Microsoft Edge team and creator of the popular Lodash JavaScript library, sharing the news of a new community project to bring ECMAScript modules to Node.*

I'm excited to announce the release of [`@std/esm`](https://medium.com/r/?url=https%3A%2F%2Fwww.npmjs.com%2Fpackage%2F%40std%2Fesm) *(standard/esm)*, an opt-in, spec-compliant, ECMAScript (ES) module loader that enables a smooth transition between Node and ES module formats with near built-in performance! This fast, small, zero dependency package is all you need to enable ES modules in Node 4+ today!

![Animation showing @std-esm being used in the Node REPL in a command prompt](./@imgs/01/8dbcd1d6fc8ddfbf33502875ff54b1f54163dfc9.gif)

@std/esm used in the Node REPL

## A tale of two module formats

With ESM [landing in browsers](https://medium.com/r/?url=https%3A%2F%2Fcaniuse.com%2F%23feat%3Des6-module), attention is turning to Node's future ESM support. Unlike browsers, which have an out-of-band [parse goal signal](https://medium.com/r/?url=https%3A%2F%2Fhtml.spec.whatwg.org%2Fmultipage%2Fscripting.html%23attr-script-type) and no prior module format,* *support for ESM in Node is a bit more…prickly. Node's legacy module format, a [CommonJS](https://medium.com/r/?url=http%3A%2F%2Fwiki.commonjs.org%2Fwiki%2FCommonJS) *(CJS) *variant, is a big reason for Node's popularity, but CJS also complicates Node's future ESM support. As a refresher, let's look at an example of both module syntaxes.

CJS:

``` javascript
const a = require("./a")
module.exports = { a, b: 2 }
```

ESM:

``` javascript
import a from "./a"
export default { a, b: 2 }
```

*Note: For more in-depth comparisons see [Nicolás Bevacqua's](https://medium.com/r/?url=https%3A%2F%2Ftwitter.com%2Fnzgb) excellent [post](https://medium.com/r/?url=https%3A%2F%2Fponyfoo.com%2Farticles%2Fes6-modules-in-depth%23the-es6-module-system).*

Because CJS is not compatible with ESM, a distinction must be made. After much discussion, Node has [settled on](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fnodejs%2Fnode-eps%2Fblob%2Fmaster%2F002-es-modules.md%2332-determining-if-source-is-an-es-module) using the ".mjs" *(modular JavaScript) *file extension to signal the "module" parse goal. Node has a history of processing resources by file extension. For example, if you `require` a `.json`file, Node will happily load and `JSON.parse` the result.

ESM support is slated to land, unflagged, in Node v10 around [*April 2018*](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fnodejs%2FLTS)*. *This puts developers, esp. package authors, in a tough spot. They could choose to:

  - Go all in, shipping only ESM, and alienate users of older Node versions
  - Wait until Jan 1, 2020, the day after [Node 8 support ends](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fnodejs%2FLTS), to go all in
  - Ship both transpiled CJS and ESM sources, inflating package size and shouldering the responsibility for ensuring 1:1 behavior

None of those choices seem super appealing. The ecosystem needs something that meets it where it is to span the CJS to ESM gap.

[![Screen capture of tweet from @sindresorhus: "The strength of Node.js has always been in the community and user-land packages."](./@imgs/01/3052c2c3b63e28002a6170d2e210eeb72ab77d52.png)](https://twitter.com/sindresorhus/status/861991464858812416)

## Bridge building

Enter the `@std/esm` loader, a user-land package designed to bridge the module gap. Since Node [now supports](https://medium.com/r/?url=http%3A%2F%2Fnode.green%2F) most ES2015 features, `@std/esm` is free to focus solely on enabling ESM.

The loader stays out of your way and tries to be a good neighbor by:

  - Not polluting stack traces
  - Working with your existing tools like [Babel](https://medium.com/r/?url=https%3A%2F%2Fbabeljs.io) and [webpack](https://medium.com/r/?url=https%3A%2F%2Fwebpack.js.org%2F).
  - Playing well with other loaders like [babel-register](https://medium.com/r/?url=https%3A%2F%2Fbabeljs.io%2Fdocs%2Fusage%2Fbabel-register%2F) (using .babelrc `"modules":false`)
  - Only processing files of packages that explicitly opt-in with a `@std/esm` configuration object or having `@std/esm` as a dependency, dev dependency, or peer dependency
  - Supporting versioning (i.e. package "A" can depend on one version of `@std/esm` and package "B" on another)

Unlike existing ESM solutions which require shipping transpiled CJS, `@std/esm` performs minimal source transformations on demand, processing and caching files at runtime. Processing files at runtime has a number of advantages.

  - Only process what is used, when it's used
  - The same code is executed in all Node versions
  - Features are configurable by module consumers (e.g. module "A" consumes module "C" with the default`@std/esm` config while module "B" consumes module "C" with `cjs` compat rules enabled)
  - More spec-compliance opportunities (i.e. `@std/esm` can enforce [Node's ESM rules](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fnodejs%2Fnode-eps%2Fblob%2Fmaster%2F002-es-modules.md) for environment variables, error codes, path protocol and resolution, etc.)

## Standard features

Defaults are important. The `@std/esm` loader strives to be as spec-compliant as possible while following Node's [planned](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fnodejs%2Fnode-eps%2Fblob%2Fmaster%2F002-es-modules.md) built-in behaviors. This means, by default, ESM requires the use of the  `.mjs` extension.

Out of the box, `@std/esm` just works, no configuration necessary, and supports:

  - `import` / `export`
  - [Dynamic ](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Ftc39%2Fproposal-dynamic-import)`import()`
  - [The file URI scheme](https://medium.com/r/?url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FFile_URI_scheme)
  - [Live bindings](https://medium.com/r/?url=https%3A%2F%2Fponyfoo.com%2Farticles%2Fes6-modules-in-depth%23bindings-not-values)
  - [Loading ](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fnodejs%2Fnode-eps%2Fblob%2Fmaster%2F002-es-modules.md%2332-determining-if-source-is-an-es-module)`.mjs`[ files as ESM](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fnodejs%2Fnode-eps%2Fblob%2Fmaster%2F002-es-modules.md%2332-determining-if-source-is-an-es-module)

## Unlockables

Developers have strong opinions on just about everything. To accommodate, `@std/esm` allows [unlocking extra features](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fstandard-things%2Fesm%23unlockables) with the `"@std/esm"` package.json field. Options include:

  - Enabling unambiguous module support *(i.e. files with at least an `import`, `export`, or `"use module"` pragma are treated as ESM)*
  -  Supporting [named exports](https://medium.com/r/?url=https%3A%2F%2Fponyfoo.com%2Farticles%2Fes6-modules-in-depth%23importing-named-exports) of CJS modules
  - Top-level `await` in main modules
  - Loading gzipped modules

## Performance

Before I continue, let me qualify the following section:

**It's still super early, mileage may vary, and results may be hand wavey!**

Testing was done using Node 9 compiled from [PR \##14369](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fnodejs%2Fnode%2Fpull%2F14369), which enables built-in ESM support. I measured the [time](https://medium.com/r/?url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FTime_%28Unix%29) taken to load the 643 modules of [lodash-es](https://medium.com/r/?url=https%3A%2F%2Fwww.npmjs.com%2Fpackage%2Flodash-es), converted to `.mjs`, against a baseline run loading nothing. Keep in mind the `@std/esm` cache is good for the lifetime of the unmodified file. Ideally, that means you'll only have a single non-cached load in production.

  - Loading CJS equivs was ~0.28 milliseconds per module
  - Loading built-in ESM was ~0.51 milliseconds per module
  - First `@std/esm` no cache run was ~1.6 milliseconds per module
  - Secondary `@std/esm` cached runs were ~0.54 milliseconds per module

Initial results look very promising, with cached `@std/esm` loads achieving near built-in performance! I'm sure, with your help, parse and runtime performance will continue to improve.

## Getting started

1. Run `npm i --save @std/esm` in your app or package directory.
2. Call `require("@std/esm")` before importing ES modules.

*index.js:*

```javascript
require("@std/esm")
module.exports = require("./main.mjs").default
```

For package authors with sub modules:

```javascript
// Have "foo" require only "@std/esm". require("foo") // Sub modules work! 
const bar = require("foo/bar").default
```

Enable ESM in the Node CLI by loading `@std/esm` with [the ](https://medium.com/r/?url=https%3A%2F%2Fnodejs.org%2Fapi%2Fcli.html%23cli_r_require_module)`-r`[ option](https://medium.com/r/?url=https%3A%2F%2Fnodejs.org%2Fapi%2Fcli.html%23cli_r_require_module): `node -r @std/esm file.mjs`.

Enable ESM in the Node REPL by loading `@std/esm` upon entering:

```sh
$ node
> require("@std/esm")
@std/esm enabled
> import p from "path"
undefined
> p.join("hello", "world")
'hello/world'
```

## Meteor's might

The `@std/esm` loader wouldn't exist without [Ben Newman](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fbenjamn), creator of the [Reify](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fbenjamn%2Freify) compiler from which `@std/esm` is forked. He's proven the loader implementation [in production at Meteor](https://medium.com/@benjamn/70425fa45d81), since May 2016, in tens of thousands of Meteor apps!

## All green thumbs

Even though `@std/esm` has just been released, it's already had a positive impact on several related projects:

  - Fixing Acorn's [strict mode pragma detection](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fternjs%2Facorn%2Fissues%2F574) and [aligning parser APIs](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fternjs%2Facorn%2Fpull%2F533)
  - Improving dynamic import support of [Babel](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fbabel%2Fbabylon%2Fissues%2F527) and [Acorn plugin](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fkesne%2Facorn-dynamic-import%2Fissues%2F7)  
    *(the dynamic import Acorn plugin is used by webpack for code splitting)*
  - Improving [property iteration order](https://github.com/Microsoft/ChakraCore/issues/3505) in ChakraCore
  - Improving the parse, load time, and spec compliance of Reify
  - Inspiring a fast [top-level parser](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2FRReverser%2Fesmod) proof of concept
  - Spurred championing of `export as ns from "mod"` and `export default from "mod"` proposals

## What's next

Like many developers, I want ES modules yesterday. I plan to use `@std/esm` in Lodash v5 to not only transition to ESM but also leverage features like gzip module support to greatly reduce its package size.

The `@std/esm` loader is [available on GitHub](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fstandard-things%2Fesm). It's my hope that others are as excited and as energized as I am. ES modules are here! This is just the start. What's next is up to you. I look forward to seeing where you take it.

## Final Thought

While this is not a Microsoft release, we're proud to have a growing number of core contributors to fundamental JavaScript frameworks, libraries, and utilities at Microsoft. Contributors like [Maggie Pint](https://medium.com/r/?url=https%3A%2F%2Ftwitter.com%2Fmaggiepint) of [Moment.js](https://medium.com/r/?url=https%3A%2F%2Fmomentjs.com%2F), [Matthew Podwysocki](https://twitter.com/mattpodwysocki) of [ReactiveX](http://reactivex.io/), [Nolan Lawson](https://medium.com/r/?url=https%3A%2F%2Ftwitter.com%2Fnolanlawson) of [PouchDB](https://medium.com/r/?url=https%3A%2F%2Fpouchdb.com%2F), [Patrick Kettner](https://medium.com/r/?url=https%3A%2F%2Ftwitter.com%2Fpatrickkettner) of [Modernizr](https://medium.com/r/?url=https%3A%2F%2Fmodernizr.com%2F), [Rob Eisenberg](https://medium.com/r/?url=https%3A%2F%2Ftwitter.com%2FEisenbergEffect) of [Aurelia](https://medium.com/r/?url=http%3A%2F%2Faurelia.io%2F), [Sean Larkin](https://medium.com/r/?url=https%3A%2F%2Ftwitter.com%2FTheLarkInn) of [webpack](https://medium.com/r/?url=https%3A%2F%2Fwebpack.js.org%2F), and [Tom Dale](https://medium.com/r/?url=https%3A%2F%2Ftwitter.com%2Ftomdale) of [Ember](https://medium.com/r/?url=https%3A%2F%2Fwww.emberjs.com%2F), to name a few, who in addition to their roles at Microsoft, are helping shape the future of JavaScript and the web at large through standards engagement and ecosystem outreach. I'm happy to share this news on the Microsoft Edge blog to share our enthusiasm with the community!
