# Learning Node.js

## First, learn the core concepts of Node.js:

* [You'll want to understand the asynchronous coding style that Node encourages.](http://blog.shinetech.com/2011/08/26/asynchronous-code-design-with-node-js/)
* [Async != concurrent. Understand Node's event loop!](http://blog.mixu.net/2011/02/01/understanding-the-node-js-event-loop/)
* [Node uses CommonJS-style require() for code loading; it's probably a bit different from what you're used to.](http://docs.nodejitsu.com/articles/getting-started/what-is-require)
* [Familiarize yourself with Node's standard library.](https://nodejs.org/api/index.html)
* [Node School](http://nodeschool.io)

## Then, you're going to want to see what the community has to offer:

The gold standard for Node.js package management is NPM. It is a command line tool for managing your project's dependencies.

[Make sure you understand how Node and NPM interact with your project via the node_modules folder and package.json.](http://nodejs.org/api/modules.html)

[NPM is also a registry of pretty much every Node.js package out there](http://search.npmjs.org/)

Finally, you're going to want to know what some of the more popular packages are for various tasks:

## Tools

### Useful Tools for Every Project:

* [Underscore](http://underscorejs.org/) contains just about every core utility method you want.
* [Lo-Dash](http://lodash.com/) is a clone of Underscore that aims to be faster, more customisable, and has quite a few functions that underscore doesn't have. Certain versions of it can be used as drop-in replacements of underscore.
* [CoffeeScript](http://coffeescript.org/) makes JavaScript considerably more bearable, while also keeping you out of trouble!
Caveat: A large portion of the community frowns upon it. If you are writing a library, you should consider regular JavaScript, to benefit from wider collaboration.
* [JSHint](http://jshint.com/) is a code-checking tools that'll save you loads of time finding stupid errors. Find a plugin for your text editor that will automatically run it on your code.

### Unit Testing:

* [Mocha](https://github.com/mochajs/mocha) is a popular test framework.
* [Vows](http://vowsjs.org/) is a fantastic take on asynchronous testing, albeit somewhat stale.
* [Expresso](http://visionmedia.github.com/expresso/) is a more traditional unit testing framework.
* [node-unit](https://github.com/caolan/nodeunit) is another relatively traditional unit testing framework.

### Web Frameworks:

* [Express.js](http://expressjs.com/) is by far the most popular framework.
* [Koa](http://koajs.com/) is a new web framework designed by the team behind Express.js, which aims to be a smaller, more expressive, and more robust foundation for web applications and APIs.
* [sails.js](https://sailsjs.org/) the most popular MVC framework for node, and is based on express. It is designed to emulate the familiar MVC pattern of frameworks like Ruby on Rails, but with support for the requirements of modern apps: data-driven APIs with a scalable, service-oriented architecture.
* [Meteor](http://www.meteor.com/) bundles together jQuery, Handlebars, Node.js, WebSocket, MongoDB, and DDP and promotes convention over configuration without being a Ruby on Rails clone.
* [Geddy](http://geddyjs.org/) is another take on web frameworks.
* [RailwayJS](https://npmjs.org/package/railway) is a Ruby on Rails inspired MVC web framework.
* [Sleek.js](https://sleekjs.com/) is a simple web framework, built upon Express.js.
* [Hapi](http://hapijs.com/) is a configuration-centric framework with built-in support for input validation, caching, authentication, etc.
* [Danf](https://github.com/gnodi/danf) is a full-stack OOP framework providing many features in order to produce a scalable, maintainable, testable and performant applications and allowing to code the same way on both the server (node.js) and client (browser) sides.
* [Derbyjs](http://derbyjs.com/) is a reactive full-stack javascript framework. They are using patterns like reactive programming and isomorphic JS for a long time.
* [Loopback.io](http://loopback.io/) is a powerful Node.js framework for creating APIs and easily connecting to backend data sources. It has a Angular.js SDK and provides SDKs for iOS and Android.

### Web Framework Tools:

* [Jade](https://github.com/visionmedia/jade) is the HAML/Slim of the Node.js world
* [EJS](https://github.com/visionmedia/ejs) is a more traditional templating language.
Don't forget about Underscore's template method!

### Networking:

* [Connect](http://www.senchalabs.org/connect/) is the Rack or WSGI of the Node.js world.
* [Request](https://github.com/mikeal/request) is a very popular HTTP request library.
* [socket.io](https://github.com/LearnBoost/socket.io) is handy for building WebSocket servers.

### Command Line Interaction:

* [Optimist](https://github.com/substack/node-optimist) makes argument parsing a joy.
* [Commander](https://github.com/visionmedia/commander.js) is another popular argument parser.
* [Colors](https://github.com/Marak/colors.js) makes your CLI output pretty.