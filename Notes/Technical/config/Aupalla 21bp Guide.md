# Aupalla 21BP User Guide

Thank you for choosing Aupalla.

To familiar with **21BP** fitness tracker and get a better user experience please check the video and tips, and if you bought for friends or family, please let them know the tips too. 

1. [How to use the tracker](https://www.youtube.com/watch?v=-w4qM_LyKDw)
2. [How to charge the tracker](https://www.facebook.com/milenabressan/videos/10212211915483658/?l=4796030308830982749)
3. [How to turn on the tracker](https://www.facebook.com/Aupalla/videos/1205839812832145)
4. [How to take off the strap from the tracker easily](https://www.youtube.com/watch?v=Ft9IZozOJp4)

If you find the green light works but no display on screen, it is because of a failure upgrading automatically; it can be solved easily.

1. Please charge the tracker for about half an hour to make sure it has enough power for upgrading
2. Please delete the app **H Band**
3. Download the latest app from **App Store** (version 4. 0) or **Google Play** ( version 2. 1.4) 
4. Place the tracker next to your phone. 
5. Connect the tracker to your smartphone by **H Band** app like you did the first time 
6. There will be a push notification about new firmware updating. 
7. Choose yes and click "*start updating*" to start the process. 
8. Wait for the upgrading to reach 100%. Then the tracker will work again.

If this process doesn't work, please use another smart phone and download **H Band** app to upgrade again. It is better to use **iPhone**.

To get to know **21BP** better, please refer to tips below.

1. To switch on or off the tracker,please long press the home button/power button for about 10 seconds. The home button/power button is at the bottom of screen, which is very close to the junction of watch and band. Please check this video at our [Facebook](https://www.facebook.com/Aupalla).
2. To make the language and time of your tracker correct, download and install **H Band 2. 0** app from **Google Play** or **H Band** from **iPhone** app store to your smart phone. Connect your smart phone and your tracker via **H Band** app.
3. For the sleep monitor function, it takes about 24 hours for the tracker to fit you. During this period, sleep monitor may be inaccurate. It will go well the next day. Sleep data shows on the tracker after 30-40 minutes when you wake up and leave bed. 
4. To make the blood pressure more accurate,please set the personal mode of your blood pressure in **H Band** app.
5. **21BP** is designed to charge without cable. Just pull off the strap and plug it into any USB charger or USB port on computer or laptop. Make sure you plug the **blue** USB port to charge, not the black one. For the **blue** USB plug, ensure the gold Pin side is compatible with the USB charger. There are two directions for the blue USB plug. Please try both sides. *Important*: Don't charge it with Power bank. Power bank considers the tracker fully charged and stops charging it.
6. To change the screen display from vertical to horizontal:
    * Find the icon "*Screen Display Switch*" on the screen after the icon "*Blood pressure*".
    * Long press the home button for several seconds to activate this function.
7. To find more functions:
    * Open the **H Band** app
    * Enter the **Setting** page 
    * Click **V07**
8. Connect your smart phone and our product via **H Band** app, not the bluetooth setting of your smart phone.
9. For more support and product news, please like [Aupalla Facebook homepage](www.facebook.com/Aupalla).

If you buy this item for your family or friends, please let them know these tips too and share your experience . 
