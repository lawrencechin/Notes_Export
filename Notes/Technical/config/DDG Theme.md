# [DuckDuckGo Themes](https://duckduckgo.com/settings)
A selection of themes created for **DDG**. Use the references here to change themes. Lamentablemente one cannot quickly change themes so one must laboriously type in the colour codes. I tried using the *bookmarklet* to go directly to the settings page and save from there but no dice. It's worth pointing out that one doesn't change themes all too often so it should be much of a bother. 

Please note that iPhone search settings need to be updated with what ever theme one wishes to use.

## Cereza Silvestre Dark
* Font : SF Mono
* Background Colour : #2a1f33
* Header Colour : #2a1f33
* Result Title Colour : #efca79
* Result Visited Title Colour : #fff9e4
* Result Description Colour : #e0fbff
* Result URL Colour : #2dbc63
* Result Highlight Colour : #22192c

#### Bookmarklet
`https://duckduckgo.com/?kae=t&k5=1&k7=2a1f33&k8=e0fbff&k9=efca79&ka=SF+Mono&kaa=FFF9E4&kah=uk-en&kak=-1&kd=1&kj=2a1f33&kl=uk-en&ks=m&kt=SF+Mono&kw=n&kx=2dbc63&ky=22192c`

#### JSON Settings
```javascript
{"kae":"t","k5":"1","k7":"2a1f33","k8":"e0fbff","k9":"efca79","ka":"SF Mono","kaa":"FFF9E4","kah":"uk-en","kak":"-1","kd":"1","kj":"2a1f33","kl":"uk-en","ks":"m","kt":"SF Mono","kw":"n","kx":"2dbc63","ky":"22192c"}
```
![](./@imgs/DDG_Settings_Cereza_Silvestre_Dark.jpg)

## Cereza Silvestre Light
* Font : SF Mono
* Background Colour : #eeeeee
* Header Colour : #ffd882
* Result Title Colour : #e35a97
* Result Visited Title Colour : #6172a6
* Result Description Colour : #2a1f33
* Result URL Colour : #2dbc63
* Result Highlight Colour : #efca79

#### Bookmarklet
`https://duckduckgo.com/?kae=t&k5=1&k7=eeeeee&k8=2A1F33&k9=e35a97&ka=SF+Mono&kaa=6172a6&kah=uk-en&kak=-1&kd=1&kj=ffd882&kl=uk-en&ks=m&kt=SF+Mono&kw=n&kx=2dbc63&ky=efca79`

#### JSON Settings
```javascript
{"kae":"t","k5":"1","k7":"eeeeee","k8":"2A1F33","k9":"e35a97","ka":"SF Mono","kaa":"6172a6","kah":"uk-en","kak":-1,"kd":"1","kj":"ffd882","kl":"uk-en","ks":"m","kt":"SF Mono","kw":"n","kx":"2dbc63","ky":"efca79"}
```
![](./@imgs/DDG_Settings_Cereza_Silvestre_Light.jpg)

## Monokai Soda Dark
* Font : SF Mono
* Background Colour : #1a1a1a
* Header Colour : #1a1a1a
* Result Title Colour : #f4005f
* Result Visited Title Colour : #9d65ff
* Result Description Colour : #f9f9f9
* Result URL Colour : #98e024
* Result Highlight Colour : #000000

#### Bookmarklet
`https://duckduckgo.com/?kae=t&k5=1&k7=1a1a1a&k8=f9f9f9&k9=f4005f&ka=SF+Mono&kaa=9d65ff&kah=uk-en&kak=-1&kd=1&kj=1a1a1a&kl=uk-en&ks=m&kt=SF+Mono&kw=n&kx=98e024&ky=000000`

#### JSON Settings
```javascript
{"kae":"t","k5":"1","k7":"1a1a1a","k8":"f9f9f9","k9":"f4005f","ka":"SF Mono","kaa":"9d65ff","kah":"uk-en","kak":-1,"kd":"1","kj":"1a1a1a","kl":"uk-en","ks":"m","kt":"SF Mono","kw":"n","kx":"98e024","ky":"000000"}
```
![](./@imgs/DDG_Settings_Monokai_Dark.jpg)

## Monochrome Light
* Font : -apple-system
* Background Colour : #ffffff
* Header Colour : #ffffff
* Result Title Colour : #1a1a1a
* Result Visited Title Colour : #393939
* Result Description Colour : #2f2e2e
* Result URL Colour : #2a1f33
* Result Highlight Colour : #ffffff

#### Bookmarklet
`https://duckduckgo.com/?kae=t&kt=-apple-system&ka=-apple-system&k5=1&k7=ffffff&k8=2f2e2e&k9=1A1A1A&kaa=393939&kah=uk-en&kak=-1&kaq=-1&kd=1&kj=ffffff&kl=uk-en&ks=l&kw=n&kx=2a1f33&ky=ffffff`

#### JSON Settings
```javascript
{"kae":"t","kt":"-apple-system","ka":"-apple-system","k5":"1","k7":"ffffff","k8":"2f2e2e","k9":"1A1A1A","kaa":"393939","kah":"uk-en","kak":"-1","kaq":"-1","kd":"1","kj":"ffffff","kl":"uk-en","ks":"l","kw":"n","kx":"2a1f33","ky":"ffffff"}
```
![](./@imgs/DDG_Settings_Monochrome_Light.jpg)
