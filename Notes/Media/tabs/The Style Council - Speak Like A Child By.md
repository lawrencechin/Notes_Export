# Speak Like A Child By The Style Council
Written by Paul Weller

Chords used:

``` text
A:   x02220    Dmaj7:  xx0222
F#m: 244222    C#aug:  xx3221
Bm:  x24432
F:   133211
E:   022100
```

``` text
A F#m (x2)

A                   F#m
 Your hair hangs in golden steps
       A             F#m
You're a bonafide in every respect
    A                                F#m
You are walking through streets that mean nothing to you
A                                F#m
 You believe you're above it and I don't really blame you


A                     F#m
 Maybe that's why you speak like a child;
A                               F#m
 The things you're saying like "I'm so free and so wild"
Bm                         F#m
 And I believe it when you look in my eyes;
Bm                        F#m
 You offer me a life that never lies
      F                E
Least only the kind to make me smile


A                                F#m
 Your clothes are clean and your mind is productive
A                        F#m
 It shops in store where only the best buy
A                               F#m
 You're cool and hard, and if I sound like a lecher
A                           F#m
 It's probably true, but at least there's no lecture


A                          F#m
 I really like it when you speak like a child
A                        F#m
 The crazy sayings like "I'm so free and so wild"
Bm                          F#m
 You have to make a bargain with me now
Bm                        F#m
 A promise that you won't change somehow
   F           E
No way, no how


Dmaj7         E
Spent all day thinking about you
Dmaj7           E
Spent all night coming to terms with it
Dmaj7       C#aug
Time and conditions are built to tame
F#m                        F
Nothing lasts with age, so people say
    E                             A
But I will always try to feel the same


F#m

A F#m (x3)

Bm F#m (x2) F E


Dmaj7         E
Spent all day thinking about you
Dmaj7           E
Spent all night coming to terms with it
Dmaj7       C#aug
Time and conditions are built to tame
F#m                        F
Nothing lasts with age, so people say
    E                             A
But I will always try to feel the same


                          F#m
I really like it when you speak like a child
A                          F#m
 I really like it when you speak like a child
A                            F#m
 The way you hate the homely rank and the file
A                              F#m
 The way you're so proud to be oh, so free and so wild


A                          F#m
 I really like it when you speak like a child
A                          F#m
 I really like it when you speak like a child
A                          F#m
 I really like it when you speak like a child
A                              F#m                A
 The way you're so proud to be oh, so free and so wild


F#m

A F#m (to fade)
```
