# Such Unlikely Lovers
## Elvis Costello & Burt Bacharach

Author: [link](https://www.elviscostello.info/guitar/pfm.html)

``` text
Gsus    G       Fsus    F
Gsus    G       Fsus

     Cm7      F7
On a hot city day
          Cm7                  F#m7
When your white shirt turns to grey
Bm9
That's when she'll arrive
         Cm7          F7
When you look how you feel
        Cm7             F#m7
Someone steps upon your heel
Bm9
That's when she will come
       Gmaj7
Listen now
                                  Em7
I'm not saying that there will be violins
A                              F#m7     D#m7
But don't be surprised if they appear
                Gmaj7
Playing in some doorway
                                   Em7
Still I can't believe that this is happening
      Cmaj7#11      (D/C)
We're such unlikely lovers
Gmaj7                         F#m7sus4
Though no one seems to notice as they hurry by
Gmaj7                     F#m7sus4              D/E
Ask me what I'm thinking, and I won't deny it
C/D                  D/E
Can you believe it's happening?
C/D 
Can you believe it's happening?

There were no magic spells
You can keep the flowers and bells
They just don't seem right
Can it actually be?
Me and you and you and me
Though we're like day and night

Listen now
I'm not saying that there will be violins
But don't be surprised if they appear
Playing in some doorway

Still I can't believe that this is happening
We're such unlikely lovers
Though no one seems to notice as they hurry by
Ask me what I'm feeling, and I won't deny it

Can you believe it's happening?
I am bewildered
Can you believe it's happening?
Somebody help me
Can you believe it's happening?

```
