# ...DON'T TALK (PUT YOUR HEAD ON MY SHOULDER)... by The Beach Boys
*from 'Pet Sounds' (1966)*
(modified from Andrew Roger's original tab)

*CAPO 6th FRET*
(Original Key: D#m)

``` text
[Verse 1]

Am     Am7      Am6          B7
 I can hear, so much in your sighs,
Bm7b5      Eaug    Am7          F#m7b5
 And I can see, so much in your eyes.
Fm6        Cadd9               Dm7
 There are words we both could say.


[Chorus 1]

G9                    Dm7
 Don't talk; put your head on my shoulder.
G9                      Dm7
 Come close, close your eyes and be still.
A#7                  Fm9
 Don't talk, take my hand,
    Fm/maj           Fm6     C
And let me hear your heart...beat.


[Verse 2]

Am     Am7       Am6          B7
 Being here with you feels so right,
Bm7b5     Eaug     Am7     F#m7b5
 We could live for-ever to-night.
Fm6        Cadd9          Dm7
 Let's not think about to-morrow.


[Chorus 2]

G9                    Dm7
 Don't talk; put your head on my shoulder.
G9                      Dm7
 Come close, close your eyes and be still.
A#7                  Fm9
 Don't talk, take my hand,
    Fm/maj           Fm6     C
And let me hear your heart...beat.
               (Am7)
Listen, listen, listen.

Break: (Strings)

Am7  D7  G/A  F7/D#,  A#/D  Gdim, C/E  Fm6  Gm

Coda:

G9                    Dm7
 Don't talk; put your head on my shoulder.
G9                      Dm7
 Don't talk; close your eyes and be still.
G9                    Dm7
 Don't talk; put your head on my shoulder.
G9                      Dm7
 Don't talk; close your eyes and be still.

(Repeat to Fade)
```

``` text
CHORD DIAGRAMS:
---------------

   Am      Am7     Am6     B7     Bm7b5   Eaug   F#m7b5    Fm6

 EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
 x02210  x02013  x02212  021202  x2323x  032110  20221x  1x0110

  Cadd9    Dm7     G9      A#7     Fm9    Fm/maj   C       D7

 EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
 x3203x  xx0211  3232xx  x13131  131113  xx3221  x32010  xx0212

   D7      G/A    F7/D#   A#/D     C/E    Gdim     Gm

 EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
 xx0212  000003  xx1211  xx0331  xx2010  xx3434  355333
```

Tabbed by Joel from cLuMsY, Bristol, England, 2004 (clumsyband@hotmail.com)