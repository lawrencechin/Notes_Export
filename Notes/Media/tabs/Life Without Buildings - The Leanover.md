# The Leanover
## Life Without Buildings

Tuning: Standard

``` text
if i lose you

Verse.  It is played around with so listen to the song for timing.
e|----------------------------|----------0------------0-----|
B|----------------------------|------------7-7-7--2/3---3---|
G|--7-7-6--6-/11-11-11-4-2--2-|--7-7-6/7---x-x-x------------|
D|--0-0-0--0--0--0--0--0-0--0-|--0-0-0-----7-7-7------------|
A|----x-x--x--x--x-----x------|----x-x-----5-5-5------------|
E|-7--7-5--5-/10-10----3---3--|-7--7-5------------3---------|


chorus?
The notes in brackets may be strummed along for the duration of that chord
it sounds okay and adds thickness to the texture.

e|-0--0--0--0--0--0--0-0-0-0-0-0-0--0--0-0--------------------|
B|-0--0--0--0--0--0--0-0-0-0-0-0-0--0--0-0--------------------|
G|-12-12-12-11-11-11-6-6-7-7-9-9-11-11-7-7--2-2-2-2-2-2-2-2---|
D|-x--x--x--x--x--x--x-x-x-x-x-x-x--x--x-x--0-0-0-0-0-0-0-0---|
A|-10-10-10-9--9--9--4-4-5-5-7-7-9--9--5-5--x-x-x-x-x-x-x-x---|
E|(0)------(10)---------(7)-(9)-(10)--(7)---3-3-3-3-3-3-3-3---|


contact contact just sweet remember contact boom contact contact thats the way your first
your last your only contact uh oh uh oh contact the first the last the only
BOUNCE TWIRL

e|----------------------------------------------------|
B|----------------------------------------------------|
G|-2--2-2-2--6--6-6-6-6-7--7-7-7-9-6-7-2--2-2-2-2-2---|
D|-0--0-0-0--0--0-0-0-0-0--0-0-0-0-0-0-0--0-0-0-0-0---|
A|-0--0-0-0--x--x-x-x-x-x--x-x-x-x-x-x-x--x-x-x-x-x---|
E|-----------5--5-5-5-5-7--7-7-7-7-7-7-3--3-3-3-3-3---|

je suis
je dance
d d d d
with a bit of
FREEstyle
```
