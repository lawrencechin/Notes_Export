# Ragged Wooded

Time Signature: 4/4

**************************INTRO**************************

``` text
Robin's acoustic bit:

Hold a regular E chord and use your pinkie to get the notes on the B string:

   Q  E E E E Q    Q  E E E E Q    Q  E E E E Q    Q  E E E E Q
e|---------------|---------------|---------------|---------------|
B|-0-------------|-2-------------|-4-------------|-2-------------|
G|------1--------|------1--------|------1--------|------1--------|
D|----------2----|----------2----|----------2----|----------2----|
A|---------------|---------------|---------------|---------------|
E|-0--0---0---0--|-0--0---0---0--|-0--0---0---0--|-0--0---0---0--|


**************************VERSE**************************

Play the intro riff 3 times

Then D7 Major:

   Q.  Q  Q.     Q.  Q  Q.    Q.  Q  Q.    Q.  Q.  Q
e|-------------|------------|------------|------------|
B|--------7----|--------7---|--------7---|---------x--|
G|-----6-------|-----7------|-6---7------|-----7------|
D|-7-----------|-9----------|------------|-9----------|
A|-------------|------------|------------|------------|
E|-------------|------------|------------|------------|

Followed by an A chord:

   Q  E E E E Q    Q  E E E E Q    Q  E E E E Q   Q  Q  Q  Q
e|---------------|---------------|--------------|-------------|
B|-5-------------|-7--------7----|-9----5-------|-7-----5--4--|
G|------6--------|------6--------|----------6---|----6--------|
D|----------7----|---------------|--------------|-------------|
A|-0--0---0---0--|-0--0---0---0--|-0--0---0---0-|-0--0--0--0--|
E|---------------|---------------|--------------|-------------|

Then the intro riff 1 more time

Skyler's electric riff:

e|----------------------|
B|----------------------|
G|----------------------|
D|--------------1-2-----|
A|------------------4-2-|
E|-0--0-0--0-0----------| (repeat)

Then ("You should come back home, back on your own now...")

    B        A
e|-------------------|
B|-------------------|
G|--8--------6-------|
D|--9--------7-------|
A|--9--------7-------|
E|--7--------5-------|

Again, repeat the intro riff 2 more times.


**************************VERSE 2**************************

Repeat as previous verse.


**************************BRIDGE**************************

(When the drums go tacet)

Alternate this riff....

   Q  E E E E Q    Q  E E E E Q    Q  E E E E Q    Q  E E E E Q
e|---------------|---------------|---------------|---------------|
B|-0-------------|-2-------------|-4-------------|-2-------------|
G|------1--------|------1--------|------1--------|------1--------|
D|----------2----|----------2----|----------2----|----------2----|
A|---------------|---------------|---------------|---------------|
E|-0--0---0---0--|-0--0---0---0--|-0--0---0---0--|-0--0---0---0--|

With this riff...

   Q  E E E E Q    Q  E E E E Q    Q  E E E E Q   Q  E E E E Q
e|---------------|---------------|--------------|--------------|
B|-4--------4----|-4-------------|-4------------|-4------------|
G|------2--------|------2--------|------2-------|------2-------|
D|---------------|----------2----|----------2---|----------2---|
A|-0--0---0---0--|-0--0---0---0--|-0--0---0---0-|-0--0---0---0-|
E|---------------|---------------|--------------|--------------|


Ensuing riff (play twice):

e|------10--7------7-10-7-----------------------------------|
B|-9-10--------10---------10-9----10-9-7--------------------|
G|---------------------------------------9-7-----7-9-9------|
D|--------------------------------------------9-------------|
A|----------------------------------------------------------|
E|----------------------------------------------------------|


**************************PART 2**************************

New time signature: 6/8

Skyler's electric riff (the double circles mean to repeat the bar):

e|-------|--------------------|
B|-------|--9-----------------|
G|-------|o---9h11------9-11-o|
D|-9--11-|o--------9h11------o|
A|-------|--------------------|
E|-------|--------------------|

Alternate the previous riff with this, same position but different fret:

e|-----------------|
B|--4--------------|
G|o---4h6-----4-6-o|
D|o-------4h6-----o|
A|-----------------|
E|-----------------|

When he says "Tell me anything you want, any old lie will do, call me..."
Play this:

   B     A    E          B      A   C#m     D       A      B
e|---------------------|------------------|---------------------|
B|-12----10---9---10-9-|-12----10----9----|-3-------2------4----|
G|-11-----9---9----9-9-|-11-----9----9----|-2-------2------4----|
D|-13----11---9----9-9-|-13----11---11----|-0-------2------4----|
A|---------------------|------------------|---------0------2----|
E|---------------------|------------------|---------------------|


Here is "Part 2" with chords implied for the acoustic guitar:

CHORDS USED:

    E  G#m  A   B  C#m  D
e|--0---4---0-------4---2--|
B|--0---4---2---4---5---3--|
G|--1---4---2---4---6---2--|
D|--2---6---2---4---6---0--|
A|--2---6---0---2---4------|
E|--0---4------------------|

E                G#m
Lie to me if you will!

E                      A
At the top of Baringer Hill!

B          A         E
Tell me anything you want!

B       A        C#m
Any old lie will do!

D    A       B
Call me back to...

            E
Back to you!
```

====================================================================


Duration Legend
---------------
W - whole
H.- half + quarter
H - half
Q.- quarter + 8th
Q - quarter
E - 8th
S - 16th

4 quarter notes (or 8 8th notes) make up one bar in 4/4 time



Tablature Legend
----------------
x - dead note
h - hammer on


----------------

LYRICS:

Woahhhhhhhhh!

Come down from the mountain, you have been gone too long
The spring is upon us, follow my only song
Settle down with me by the fire of my yearning

You should come back home!
Back on your own, now!

The world is alive now, in and outside our home
You run through the forest, settle before the soul
Darling I can barely remember you beside me

You should come back home!
Back on your own, now!

In evening light, when the woman of the woods came by
To give to you the word of the old man
In the morning time, when the sparrow and the seagull fly
And Jonathan and Evelyn get tired

Lie to me if you will!
At the top of Baringer Hill!
Tell me anything you want!
Any old lie will do!
Call me back to...back to you!
