# She’s Electric

Chords:

E:    (022100)
G\#:   (4665xx)
C\#m:  (04665x)
A:    (x03330)
C:    (032010)
D:    (xx0232)
Amaj7:(x02120)
E7:   (020130)
F\#m7add4:(x44200)
B:    (x2444x)


``` text
Intro: | E  G\# | C\#m A|  (x3)
      
       | C | D | E |


Verse 1:

E    G\#   C\#m                  E      G\#               A
She's____electric, she's in a family full of eccentrics,

      E                 G\#      C\#m   A     C    D    E
She's done things i've never expected and I need more time.

E    G\#        C\#m  A      E        G\#             C\#m       A
She's____got a sister, and God only knows how i've missed her,

           E           G\#         C\#m    A      C    D    E
And on the palm of her hand is a blister, and I need more time.| A


Chorus 1:

      Amaj7       A                Amaj7             A
And I want you to know I've got my mind made up now,

      Amaj7     E   | E | A |
but I need more time

      Amaj7       A                Amaj7            A
And I want you to say, do you know what i'm saying?

      Amaj7
But I need more,

      E
Cause I'll be you and you'll be me,

        E7
There's lots and lots for us to see,

F\#m7add4
Lots and lots for us to do,

B
She's electric, can I be electric too? 


Instumental: | E | G\# | C\#m | A | (X3)
            
             | C | D | E |



Verse 2: (Chords as verse 1)


She's got a brother, we don't get on well with one another,
But I quite fancy her mother, and I think that she likes me
She's got a cousinin fact she's got bout a dozen
She's got one in the oven, but it's nothing to do with me.


Chorus 2: Repeat chorus 1


Outro:

| C | D | E |  C | D | E


    E                 C  | E | C | D | E |  (X3)
Can I be electric too?

C      | D | E | E
Ah________________________
```
