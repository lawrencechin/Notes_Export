# Teenage Riot - Sonic Youth

``` text
Intro

E|----------------------------------|
B|----------------------------------|
G|--7--7-7-7-5-5------0---0--0-5-5--|
D|--7--7-7-7-7-7---------------7-7--|
A|------------------0---0-----------|
E|----------------------------------|      repeat

Bridge 1

E|-----------------------------------------------------|
B|-----------------------------------------------------|
G|------7--7--7--5-------------------------------------|
D|------7--7--7--7-----7-7-7-7-7-5-0-0-0-0-0-7-7-7-5-0-|
A|--5------------------7-7-7-7-7-7-0-0-0-0-0-7-7-7-7-0-|
E|--7--------------------------------------------------|

E|----------------------------------------------|
B|----------------------------------------------|
G|------7—7---5---------------------------44444-|
D|------7—7---7------------5--4----4--555555555-|
A|--5---------------5--7---------7----0000------|
E|--7-------------------------------------------|

Verse

E|---------------------------------------------|
B|---------------------------------------------|
G|-------4----4--------2--------2----2-------4-|
D|-------0----0--------0--------0----0-------0-|
A|-------2----2--------2--------2----2-------2-|
E|--3-3----3----3-3-------3-3-----3------3-----|   repeat

I'm not sure about how many times do you play those 3's but listen to the song and 
eventually figure out the rhythm. Then play:

E|-----------------------------------------|
B|-----------------------------------------|
G|--2---2-2---2-2---12-12-12---9--9-9------|
D|--2---2-2---2-2---12-12-12---7--7-7------|
A|--0---0-0---0-0---12-12-12---7--7-7------|
E|--3---3-3---3-3--------------------------|

Chorus (chords, listen to the song, strumming is very simple to figure out)

E|---------------------------------------|
B|---------------------------------------|
G|----------------------------------0----|
D|----------------------------------5----|
A|--12---14---15---14---15---12-----0----|
E|--15---15---15---14---15---15-----5----|


Bridge 2 (repeat a few times,then go back to verse)

Strum this chord first:

E|------|
B|------|
G|--0---|
D|--5---|
A|--0---|
E|--5---|

then:

E|-----------------------------------------------------|
B|-----------------------------------------------------|
G|------7--7--7--5-------------------------------------|
D|------7--7--7--7-----7-7-7-7-7-5-0-0-0-0-0-7-7-7-5-0-|
A|--5------------------7-7-7-7-7-7-0-0-0-0-0-7-7-7-7-0-|
E|--7--------------------------------------------------|

E|---------------------------------------------------|
B|---------------------------------------------------|
G|------7—7---5---------------------------1616161616-|
D|------7—7---7------------5--4----4--55551717171717-|
A|--5---------------5--7---------7----0000-----------|
E|--7------------------------------------------------|

End with

E|------|
B|------|
G|--0---|
D|--5---|
A|--0---|
E|--5---|
```

This is just a STANDARD TUNING interpretation. If you want to play like Thurston, use 
tuning. And a different tab.

This is my first tab, so don't yell at me if you find a mistake.

-drocan
