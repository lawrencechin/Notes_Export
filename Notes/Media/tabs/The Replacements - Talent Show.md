# [Talent Show](http://paulwesterberg.proboards.com/thread/8674/talent-show)

Someone on the [colormeimpressed](colormeimpressed.com) tab pages cracked code for the lead figure. I'm just adding the rhythm guitar chords. Because you can't really play a B behind the lead on one guitar, this takes two guitarists to play right. 

``` 
Standard tuning

Lead 

Aadd9                   A6/9

|x|-|-|•|-|-| 9 fret    |x|-|•|-|-|-| 9 fret
|x|-|-|-|-|-|           |x|-|-|-|-|-|
|x|-|•|-|-|-|           |x|-|-|•|-|-|

Asus2                   Amaj9

|x|-|•|-|-|-| 7 fret    |x|-|•|-|-|-| 11 fret
|x|-|-|-|-|-|           |x|-|-|-|-|-|
|x|-|-|•|-|-|           |x|-|-|•|-|-|


A/9(2)

|x|-|•|-|-|-| 4 fret
|x|-|-|-|-|-|
|x|-|-|•|-|-|

Sequence: Aadd9 — A6/9 — Asus2 — A6/9 — Amaj9 — A6/9(2) — Asus2

This seems to be played on acoustic throughout the entire song. There’s an electric guitar that comes in playing the lead after some of the choruses. It seems to substitute Asus2 for Aadd9. 

It’s possible that, when the rhythm guitar goes to Dsus2, the electric lead lifts the finger that’s on the D string and plays the same figure with only the G string notes fretted 

Rhythm chords

Asus2
|x|-|•|•|-|-| 2 fret

or

|x|-|•|-|-|-| 2 fret
|x|-|-|-|-|-|
|x|-|-|•|-|-|

A
|x|-|•|•|-|-| 2 fret

(alternate with the following)

|x|-|-|•|-|-| 2 fret
|x|-|-|-|-|-|
|x|-|•|-|-|-|

B
|x|•|-|-|-|-| 2 fret
|x|-|-|-|-|-|
|x|-|•|•|-|-|

D
|x|-|-|•|-|-| 2 fret
|x|-|-|-|•|-|

Over the verses, when the lead is playing, it’s either Asus2 or A. 

Chorus

We ain’t much to look at so…
D

We’re playing at the talent show…
A or Asus2

Come on along, here we go
B

Playing at the talent show
A

Check us out, here we go
B

Playing at the talent show
A


It goes back to the D coming out of the second and third chorus, and the "It's too late to turn back" part just starts alternating between A and D. 
```
