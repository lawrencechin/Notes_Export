# No Poetry

(Chords/tab transcribed by Matthew Zelman)

Chords used:

* G    - 320003
* C    - X32010
* B7/C - X31200
* A11  - 5400XX
* Em   - 022000
* G7   - 323XXX
* G/E  - 0X5007
* G9/D - X53005

```
Intro:

e------------------------------------------3-
b------------------------------------------0-
g-----------------------------0-------2-0--0-
d-0-0-----------------------2---2---1------0-
a-----0-1-2-------0-1-2-3-3-------3--------2-
e-----------3---3--------------------------3-

(G)gonna take a meteor shower
(C)to lay me (B7/C)out on the lawn (G)
(G)with one detective's daughter
(A11)nth (B7/C)power, shine on (G)

(Em)and maybe my (G7)verses
(C)ain't that (B7/C)free
(G)cause there will be no more (C)poetry (G)

(G)taurus is a great big bull
(C)he ate my (B7/C)heart out in the woods (G)
(G)the heavens move so slow
(A11)but i'm (B7/C)quick and good (G)

(Em)and you don't have to (G7)be poor
(C)to hang with (B7/C)me
(G)cause there will be no more (C)poetry (G)

interlude:
G    C    B7/C  G
G    A11  B7/C  G
G/E  G9/D C     B7/C
G    C    G

(G)there's a god-shaped hole
(C)bleeding (B7/C)love up above(G) 
(G)and in my heart full of soul
(A11)i just can't (B7/C)seem to get enough

(Em)and you don't have to (G7)be poor
(C)to hang with (B7/C)me
(G)cause there will be no more (C)poetry (G)

(G)cut it up, cut it up (C) (B7/C) (G)
(G)fill it up, fill it up (A11) (B7/C) (G)

(Em)and you don't have to (G7)see me
(C)to be(B7/C)free
(G)cause there will be no more (C)poetry (G)

outro is same as intro
```
