# Surfs Up
The Beach Boys

`Fm7` at the end could also be played with `Am7` shape barre chord on the 8th fret. I like that voicing better. It also helps with the singing.        

``` text
[Verse]

Gm7/D
A diamond necklace played the pawn
Hand in hand some drummed along, oh
Dm7/G
To a handsome man and baton
Gm7/D
A blind class aristocracy
Back through the opera glass you see
Dm7/G
The pit and the pendulum drawn


[Chorus]

Bb      Gm7         F/C  C7  D/C
   Columnated ruins  do  mi  no
 G/D                               A/G
   Canvass the town and brush the backdrop
         D/A
Are you sleeping?


[Verse 2]

Gm7/D
Hung velvet overtaken me
Dim chandelier awaken me
Dm7/G
To a song dissolved in the dawn
Gm7/D
The music hall a costly bow
The music all is lost for now
Dm7/G
To a muted trumperter swan


[Chorus]

Bb      Gm7         F/C  C7  D/C
   Columnated ruins  do  mi  no
 G/D                               A/G
   Canvass the town and brush the backdrop
        D/A               D  Em7 Gm/F  D/F#
Are you sleeping, Brother  John?


[Verse]

Ab6
   Dove nested towers the hour was
Eb/Bb
Strike the street quicksilver moon
Ab6
   Carriage across the fog
    Eb/Bb
Two-Step to lamp lights cellar tune

    Dm7                 Gm7/C  C7   F
The laughs come hard in Auld Lang Syne
    Dm7
The glass was raised, the fired rose
    Gm7/C            C7                Edim7   F
The fullness of the wine, the dim last toast- ing
Dm7           Gm7/C  C7 F 
While at port adieu or die
  Dm7
A choke of grief heart hardened I
Gm7/C            C7             Edim7  F
Beyond belief a broken man too tough to cry


[Coda]

Ab6
   Surf's Up
Eb/Bb
   Aboard a tidal wave
Ab6
   Come about hard and join
    Eb/Bb
The young and often spring you gave
  Dbmaj7
I heard the word
   Cm7
Wonderful thing
  Fm7               Cm7  Bb  Cm7
A children's song  

Fm7  Cm7  Bb  Cm7 
```
