# Range Life

Pavement
Crooked Rain, Crooked Rain

``` text
intro
e-----5-------5---5^7-7---7-5---------5-----5---------|
B-------5---5-----------------5---5^7---7-------5---5-|
G-5^6-----6-----6-------6-------6---------6---6---6---|
D-----------------------------------------------------|
A-----------------------------------------------------|
E-----------------------------------------------------|

e-5---5-5-9---7---5-------5-9---7---5-------5-10---9---|
B---5-----------------5^7---------------5^7------5---5-|
G-----------6---6---6---------6---6---6----------------|
D------------------------------------------------------|
A------------------------------------------------------|
E------------------------------------------------------|

                            "after. . glow. .scene stage
e-7-9---5-----5-0-------5---------5---5------(5)^7-7---|
B-----------5-----5-5^7---7---------5------5-----------|
G-----5---5-----------------5-5^6--------6-----------6-|
D------------------------------------------------------|
A------------------------------------------------------|
E------------------------------------------------------|

e---7-5---------5-----5-----5------------------------|
B-------5---5^7---7-----7-------5--5^7-7-7-------5-5-|
G-6-------6---------6-----6---6---6--------0-5^6-----|
D----------------------------------------------------|
A----------------------------------------------------|
E----------------------------------------------------|

    gotta   pay   dues      pay     rent     over
e---------------------------2-0------------5-----5-5-|
B-0-0^1-1-0-0^1-1-0--------------3--2-2----5---------|
G----------------------2--2------------------5^6-----|
D----------------------------------------------------|
A----------------------------------------------------|
E----------------------------------------------------|

e-----5---5^7-7---7-5-----5-----5---------------------|
B---5-----------------5^7---7-------5---5^7-7-7-5-----|
G-6-----6-------6-------------6---6---6-----------5^6-|
D-----------------------------------------------------|
A-----------------------------------------------------|
E-----------------------------------------------------|


e-------------------------2-0-------|
B-5---5-7---------------------3-2-2-|
G-6-6-----5-5^6-6-4-2-4-2-----------|
D-----------------------------------|
A-----------------------------------|
E-----------------------------------|

Chorous
"I wanna range life. . . "
e-4-4-----4-----4-----4---------2-2-2-------2-----2-----|
B-5---5-----5-----5-----5-------2-----2---2---2-----2---|
G-6-----6-----6-----6-----6-6-6-2-------2-------2-----2-|
D-6-----------------------------4-----------------------|
A-4-----------------------------4-----------------------|
E-------------------------------2-----------------------|

e---2`-2-----2-----2-------2`-2-----2-----2-----|
B-2-3`---3-----3-------3---2`---2-----2-----2---|
G---4`-----4-----4-------4-2`-----2-----2-----2-|
D---4`---------------------4`-------------------|
A---2`---------------------4`-------------------|
E--------------------------2`-------------------|

e-4`-4-----4-----4-----4---------2`-2-----2-----2-----2---|
B-5`---5-----5-----5-----5-------2`---2-----2-----2-----2-|
G-6`-----6-----6-----6-----6-6-6-2`-----2-----2-----2-----|
D-6`-----------------------------4`-----------------------|
A-4`-----------------------------4`-----------------------|
E--------------------------------2`-----------------------|

                                            dive out
e-2`-2-----2-----2----------2---------------4`---|
B-3`---3-----3-------3----2-----2---2-------5`---|
G-4`-----4-----4-------4----------2---2-----6`---|
D-4`--------------------------4---------4--6`----|
A-2`----------------------------------------4`---|
E------------------------------------------------|
here's where the acoustic guitar slow strums an E chord but
the electric plays nothing.  Then, when it fires back up you play:

e-----5---5---5^7-7-9---9-0-10-0-9-0-7-9---10-0-9-0-|
B-------5-------------------------------------------|
G-5^6-------6---------6------------------6----------|
D---------------------------------------------------|
A---------------------------------------------------|
E---------------------------------------------------|

e-7---9-5-------5-----X-5---9---7---5-----7---9---7---|
B---5-------5^7---7---------------7---7-----7---7---7-|
G-----6---6---------6-----6---6---------6-------------|
D-----------------------------------------------------|
A-----------------------------------------------------|
E-----------------------------------------------------|

                        run from  pigs         cops
e-9---7---5---5-5-9---------5--------5-5---5^7-7---7-5-|
B---7-----------5-----5-------5--5^7-------------------|
G-------6---6-------6---5^6--------------6-------6-----|
D------------------------------------------------------|
A------------------------------------------------------|
E------------------------------------------------------|

                gloves
e-----------5---5---5-----------------5-------0---0---0-|
B-5-----5^7---7---7---7-5^7-7-5-5-5^7---7-------0---0---|
G---5^6-----------------------------------0^1-----1-----|
D-------------------------------------------------------|
A-------------------------------------------------------|
E-------------------------------------------------------|

                                              out on my
e-----------------------2-0---0^2-------------5-/5---7-|
B---0^2-0---0---0-0-3-------3-----2-----2---0----------|
G-1-------1---1-------1-------------2-2---2--------6---|
D------------------------------------------------------|
A------------------------------------------------------|
E------------------------------------------------------|

                        gumsmacks
e-5---4-------5-4-------------------5-----5-------------|
B---5-----7-5-------7-7---5---5-7-5---5^7---7-5---------|
G-------6---------6-----6---6-------------------0^1-0^1-|
D-------------------------------------------------------|
A-------------------------------------------------------|
E-------------------------------------------------------|


e-0-----0-------0---------------0---2-2-----------|
B---0---------------0-2-0---0-------------0^2-2-0-|
G-----1---1-0^1---4-------2---2---2-----2---------|
D-------------------------------------------------|
A-------------------------------------------------|
E-------------------------------------------------|

E              D           A       Bm           D
  "Don't worry, were in no hurry.  School's out, what
          A
did you expect."

then repeat the chorous followed by:
e-----5---5---9---7-----5-----5---10---9---7-5-0-7-0-9-|
B-------5-----5---5--------------------5-7-------------|
G-5^6-------6---6---5^6---5^6---6----5-----------------|
D------------------------------------------------------|
A------------------------------------------------------|
E------------------------------------------------------|

e-0-10-----9-0-7-0-9-0-10-0-9-0-12-0-10-0-9-0-10-0-9-|
B------(7)-------------------------------------------|
G----------------------------------------------------|
D----------------------------------------------------|
A----------------------------------------------------|
E----------------------------------------------------|

         "out on tour. . ."
                   NH
e-0-7-0-5---------<12>-12`---0-10---5-12-12-1215-14-14---|
B----------------------14`-------------------------------|
G--------------------------6------7----------------------|
D--------------------------------------------------------|
A--------------------------------------------------------|
E--------------------------------------------------------|

                    "don't..what they. . . could. . ."
                     NH
e-------12-10-9---7-<12>-----7---10---10---7-9-14-------|
B-13-13---------0-------------------0----0--------15----|
G----11----------------------------------------------11-|
D-------------------------------------------------------|
A-------------------------------------------------------|
E-------------------------------------------------------|

 "give.....fuck"              "temple
e-14----12-12------12------12--1012-9-7---7-5-7----|
B-------------15-0----15-0---------------7---------|
G----11--------------------------------------------|
D--------------------------------------------------|
A--------------------------------------------------|
E--------------------------------------------------|

 "foxy to me...foxy to you..."              "absolutely"
e-2----2-------2-------2-----2-----0-----0--------|
B-2-------2--2-2---------------2-------0---0------|
G-2------------2-----------------2---1-------2----|
D-4------------4----------------------------------|
A-4------------4----------------------------------|
E-2------------2----------------------------------|

                         "dream and dream...."
e-0---2-------------------------------5---5---5-----|
B---0-------3-0^2-2-----------------------------5---|
G-------2-2---------2`--------------6---6---7-----7-|
D---------------------------------------------------|
A---------------------------------------------------|
E---------------------------------------------------|

e-5-------------------------------------------------|
B-----7-7-----7-7---7-7-7-7-----7---7---7---7---7---|
G---------6-------6---------6---------------6-----7-|
D---5-------7-----------------6---6---6---6---7-----|
A---------------------------------------------------|
E---------------------------------------------------|

e---------------|
B-7-----5-------|
G---7-6-----6---|
D---------7---7-|
A---------------|
E---------------|

e-0-0-0-0-0-0-0-0-|
B-2-2-2-2-2-2-0-0-|eight times and then
G-2-2-2-2-2-2-2-2-|end on
D-2-2-2-2-2-2-2-2-|
A-0-0-0-0-0-0-0-0-|
E-----------------|

                         "so alive..."
e-0---0-0---0-0-0-0-0-0-|
B-2---0-2---0-2-2-2-0-2-|
G-2---2-2---2-2-2-2-2-2-|
D-2---2-2---2-2-2-2-2-2-|
A-0---0-0---0-0-0-0-0-0-|
E-----------------------|
```
