# I Just Wasnt Made For These Times
The Beach Boys

## Chords

``` text

Gmaj9    G6      F9    Bm7/E    E7b9    Cmaj7    C/D     C6

EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
320202  322000  xx3243  x20202  002101  x32000  x00010  x32210

Gadd9    Em7     E       D6      D7      Am7     D      G#aug

EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
320033  022030  022100  xx0202  xx0212  x02013  xx0232  xxx110

D/E     G/A    Em7/A   Bm7b5    E7     Am7b5

EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
000232  000003  x00030  x23230  020100  xx1213
```

*from 'Pet Sounds' (1966)*

(modified from Andrew Roger's original tab)


*CAPO 3rd FRET*

(Original Key: Bb)

``` text
Verse 1:

Gmaj9                 G6
 I keep looking for a place to fit in,
      F9             Bm7/E
Where I can speak my mind.
Gmaj9                     G6
 I've been trying hard to find the people,
     F9               Bm7/E  E7b9
That I won't leave be-hind.

Bridge 1:

     Cmaj7     C/D     C6       Gadd9       Em7   Bm7/E
They say I got brains, but they ain't doin' me no good.
            E     E7b9
I wish they could.

C6         D6              C6       D6
 Each time things start to happen a-gain,
  C6          D6              C6          D6                  D7
I think I got something good goin' for my-self, but what goes wrong?

Chorus 1:

Am7               D
 Sometimes I feel very sad,
Am7               D
 Sometimes I feel very sad,
(Something put my heart and soul into).
Am7               D
 Sometimes I feel very sad,
(Something put my heart and soul into).

Break: (*Harpsichord and Flute)

Cmaj7  G#aug, D/E  E7b9

Interlude:

Am7      D6     Am7         C/D       G/A   Em7
 I guess I just wasn't made for these times.

Verse 2:

Gmaj9                 G6
 Every time I get the inspiration,
   F9                 Bm7/E
To go change things a-round.
Gmaj9                    G6
 No one wants to help me look for places,
      F9                  Bm7/E  E7b9
Where new things might be found.

Bridge 2:

Cmaj7        C/D   C6      Gadd9        Em7         Bm7/E
 Where can I turn, when my fair-weather friends cop out?
                E     E7b9
What's it all a-bout?

C6         D6              C6       D6
 Each time things start to happen a-gain,
  C6          D6              C6          D6                  D7
I think I got something good goin' for my-self, but what goes wrong?

Chorus 2:

Am7               D
 Sometimes I feel very sad,
Am7               D
 Sometimes I feel very sad,
(Something put my heart and soul into).
Am7               D
 Sometimes I feel very sad,
(Something put my heart and soul into).

Break: (*Harpsichord and Flute)

Cmaj7  G#aug, D/E  E7b9

Interlude:

Am7      D6     Am7         C/D       G/A   Em7
 I guess I just wasn't made for these times.

Instrumental: (*Theremin)

Gmaj9  G6  F9, Bm7/E  E7b9

Coda:

Am7      D6     Am7         D6        Am7
 I guess I just wasn't made for these times.
Am7      D6     Am7         D6        Am7
 I guess I just wasn't made for these times.

Am7      D6     Bm7b5       E7        Am7b5
 I guess I just wasn't made for these times,
                D6
(I guess I just wasn't made for these times).

Am7      D6     Am7         D6        Am7
 I guess I just wasn't made for these times.
Am7      D6     Am7         D6        Am7
 I guess I just wasn't made for these times.

(Fade)
```
