# I Loves You, Porgy
## George & Ira Gershwin & Dubose Hayward

``` text
[Intro]
 
Emaj7  flourish

[Verse]

(B7)         Emaj7     F#m7                G#m7      C#m7
I loves you, Porgy,                don't let him take me
            G#m7     C#9                        F#9   Am7
Don't let him handle me,          and drive me mad
              E/B  C#m7           F#m7                 Am7
If you can keep me,        I wanna stay here with you forever,
E/B   F#m7/B  Emaj7   B7
and I'll be glad

Yes I loves you, Porgy, don't let him take me
Don't let him handle me, with his hot hands
If you can keep me,  I wants to stay here with you forever
I've got my man

I loves you, Porgy, don't let him take me
Don't let him handle me, and drive me mad
If you can keep me, I wanna stay here with you forever
I've got my man


[Bridge]

Am6                  F7            E7              
Someday I know he's coming to call me
Am6                   G7sus       G7
He's going to handle me and hold me.
Cm                              Cm/Bb                Ab7
So, it' going to be like dying, Porgy, when he calls me.
Ab7        G7sus      G7        Dm7     B7
But when he comes I know I'll have to go

I loves you, Porgy, don't let him take me
Honey, don't let him handle me, and drive me mad
If you can keep me, I wanna stay here with you forever
I've got my man.
```
