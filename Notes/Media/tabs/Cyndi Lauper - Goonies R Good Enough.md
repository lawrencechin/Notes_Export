# Goonies R Good Enough

Intro: Dm

``` text
F
Here we are
                        Dm    F
Hanging onto strings of green and blues
                        Dm    F
Break the chain then we break down
            Dm
Oh it's not real if you don't feel it 
Bb        C
Unspoken expectations
F          G       A
Ideals you used to play with
Bb              C
They've finally taken shape for us.


F
What's good enough for you
   Dm         Bb
Is good enough for me
     C
It's good enough
                     F
It's good enough for me
Bb             C    Dm
Yeah yeah yeah yeah yeah


F
Now you'll say
                            Dm       F
You're startin' to feel the push and pull
F
Of what could be and never can
    Dm
You mirror me stumblin' through those 
Bb            C
Old fashioned superstitions
F          G       A
I find too hard to break
Bb              C
Oh maybe you're out of place


F
What's good enough for you
   Dm         Bb
Is good enough for me
     C
It's good enough
                     F
It's good enough for me
Bb             C    Dm
Yeah yeah yeah yeah yeah
F
(Good Enough) for you
   

  Dm    Bb
Is good enough for me
     C                       
It's good enough, 
                      F
It's good enough for me
Bb             C    Dm
Yeah yeah yeah yeah yeah


Bb            C
Old fashioned superstitions
F           G      A
I find too hard to break
Bb              C 
Oh maybe you're out of place


F
What's good enough for you
   Dm        Bb
Is good enough for me
      C
It's good enough
                     F
It's good enough for me
Bb             C    Dm
Yeah yeah yeah yeah yeah
F
(Good Enough) for you


   Dm       Bb
Is good enough for me
      C
It's good enough
                     F
It's good enough for me
Bb             C    Dm
Yeah yeah yeah yeah yeah
```
