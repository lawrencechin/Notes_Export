# God Give Me Strength
Tabbed by John Lewitt <emplex@istar.ca>

Notes: This song is ideally suited to the piano, but a strong voice can carry it on the guitar. The 'Asus' chords in the bridge are more of a counter point, they are used in the song but not as the base of the song.
 
``` text
Intro: Gm9 D (4x)

Bm                  G                      D
Now I have nothing, so God give me strength
C#m7            C#m              F#m7
'Cause I'm weak in her [his] wake
C#m                          F#m   F#7
And if I'm strong I might still break
Bm                  A           G 
And I don't have anything to share
Bm            E                      A 
That I won't throw away into the air
  
G                 A
That song is sung out
G                 A 
This bell is rung out
Em               F#m             A 
[He] She was the light that I'd bless
Em               F#m                  A
[He] She took my last chance at happiness
Gm7                  Bm       Gm9 Em       D G Em A 
So God give me strength, God give me strength

(second verse same as first)

I can't hold onto [him] her, God give me strength
When the phone doesn't ring
And I'm lost in imagining
Everything that kind of love is worth
As I tumble back down to the earth
That song is sung out
This bell is rung out
[He] She was the light that I'd bless
[He] She took my last chance at happiness
So God give me strength,

Gm9      Em   D                  G              Em
God if [he'd] she'd grant me [his] her indulgence and decline
(e, f#, g#) Asus4      A                Asus2
I might as well wipe [him] her from my memory
(e, f#, g#) Asus4      A                Asus2
Fracture the spell as [he] she becomes my enemy
Bm               E              A
Maybe I was washed out like a lip-print on his shirt
Bm             E        A 
See, I'm only human, I want him to hurt
Asus4   A      A7
I want him, I want him to hurt

[instrumental - repeat structure of first verse, these last two lines are 
picked up as you reach the end of the verse)]

Since I lost the power to pretend
That there could ever be a happy ending

That song is sung out
This bell is rung out
[He] She was the light that I'd bless
[He] She took my last chance at happiness
So God give me strength, God give me strength

Gm    D

Wipe her from my life...
I might as well...
God give me strength...
God give me strength...
I might as well
```
