# God Only Knows (The Stereo Mix)[1997 Digital Remaster]
The Beach Boys
The Pet Sounds Sessions (1997)
Tab annotated by Richard Grosser (grosserr@gmail.com)        
Tempo: 4|4  
Tuning: Standard


Capo: 2

``` text
[Chords]
            EADGBe
G           320003     
D/F#        200232 
Em7         020000 
G/D         XX0003 
A/E         002220 
G#dim7      4X443X
C/G         332010              
Am          002210
Em          022000                 
A7/G        302020        
Bbdim7      X1202X
D/A         X00232             
A           X02220
D           XX0232
Abm7b5      202210
F           133211
Dm6         XX0201
G/B         X20003
Am7         002010 
D7          XX0212 
Ebdim       X6787X
C#m7b5      X42000
```

``` text
[INTRO]
|G  D/F#  Em7  G/D  A/E  Bb/F|| C/G | 


[VERSE 1]
C/G              Am
I may not always love you,
Em      Em7           A
But long as there are stars above you,
D/A          Bbdim7
You’ll never need to doubt it,
D/A              Abm7b5
I'll make you so sure about it.


[REFRAIN 1]
G        D/F#           Em7        D
God only knows what I'd be without you. 


[VERSE 2]
C/G                Am
If you should ever leave me,
Em      Em7             A
Though life would still go on believe me,
D/A                  Bbdim7     
The world could show nothing to me,
D/A                Abm7b5
So what good would living do me.


[REFRAIN 2]
G        D/F#              Em7     D
God only knows what I'd be without you.


[INSTRUMENTAL l](Keyboard, Bass & drums, then Scat vocals over)
|F    Dm6    Am   Am7 D7|(Aah-aah-aah, Doo-doo-doo)[dual-tracked]
|G/D  Ebdim   G/D   C#m7b5|(Bah-buppa-bah, Bah-buppa-bah)


[REFRAIN 3]
C/G      G/B                    Am7
God only knows what I'd be with-out you.


[VERSE 3]
C/G                Am
If you should ever leave me,
Em      Em7             A
Though life would still go on believe me,
D/A                  Bbdim7
The world could show nothing to me,
D/A                Abm7b5
So what good would living do me.


[REFRAIN 4] (Repeat x6)
G        D/F#              Em7     D
God only knows what I'd be without you-oo,
G        D/F#              Em7     D
God only knows what I'd be without you,
   (God only knows…)


[CODA](Fade)
G        D/F#              Em7     D
God only knows what I'd be without you.


[End]
```

## BACKGROUND:
“Mind of Brian 1: God Only Knows”  http://www.surfermoon.com/essays/mob1.html

## NOTES:
1. Since this was TAB'ed I learnt of the existence of 'The Wrecking Crew'.  Their musical contribution is 
unmistakable.  https://www.youtube.com/watch?v=DVUBpzlELOg; https://www.youtube.com/watch?v=QCTVcNsJGX0
2. My apologies to the UG players, as this TAB had been re-edited without my knowledge.  I have 
returned it to a corrected and playable format.  I trust that, you may continue to enjoy it? Ditto #2.
3. "...AllMusic calls the compilation "a fascinating, educational listen, even if it's not 
necessarily indispensable."[8] Q gave the "enlightening" box set a perfect score and wrote 
that "the backing music tracks sans vocals opens your ears to a bevy of awe-inspiring nuances 
previously obscured by singing.  At the same time, the isolated vocal tracks are 
nothing less than spiritual in their emotive wallop. ..."   
https://en.wikipedia.org/wiki/The_Pet_Sounds_Sessions
4.  "...The new mixes created for the box set were made in January–February 1996.  
Linett wrote: "In mixing Pet Sounds in stereo, every attempt was made to duplicate the feel and 
sound of the original mono mixes. Vocal and instrumental parts that Brian left off the record 
in 1966 were noted and duplicated, as were the fades.  To this end, a Scully model 280 4-track 
was used to transfer the analog reels to digital multi-tracks – the same model used for 
'Pet Sounds'.  The mix was then processed through an original tube console from United Western 
Recorders in the 1960's. ..."  https://en.wikipedia.org/wiki/The_Pet_Sounds_Sessions
Suggest correction