# Elevate me later 
from Pavement (Crooked rain crooked rain album), by Jar0


intro:

``` text
e-|-x-x-x-----------x-x----------x-x-x---x-x-x---x-x-x-x---x--x--|
B-|-x-x-x-----------x-x----------x-x-x---x-x-x---x-x-x-x---x--x--|
G-|-2-4-4---4-4---4-5-5----------4-5-5---4-5-5---5-7-7-7---10-10-|x2
D-|-0-0-0-----------x-x----------x-x-x---x-x-x---x-x-x-x---x--x--|
A-|-x-x-x-----------0-0----------3-3-3---3-3-3---3-5-5-5---8--8--|
E-|-3-3-3-----------x-x----------x-x-x---x-x-x---x-x-x-x---x--x--|
```

1st verse:

``` text
G             Am                   C
Well you greet the tokens and stamps
      D                      G
underneath the fake-oil burnin' lamps
    Am                    C
in the city we forgot to name
   D                        C
the concourse is four-wheeled shame
       D                   G
and the courthouse's double-breast
     Am                 C
i'd like to check out your public protests
D
why you're complaining Ta!
```
2nd verse:

``` text
G                Am                   C
Those who sleep with electric guitars
    D                     G
range rovin' with the cinema stars
   Am                            C
and i wouldn't want to shake their hands
            D                     C
'cause they're in such a high-protein land
             D                       G
because there's 40 different shades of black
     Am                        C
so many fortresses and ways to attack
D
so why you complaining? Ta!
```
