# JIMMY MACK by Martha and the Vandellas
*from 'Watchout!' (1966)*
*CAPO 1st FRET*

(Original Key: C#)

``` text
Chorus 1:

F6      Dm         Cmaj7                    F
 Jimmy, Jimmy, oh, Jimmy Mack; when are you coming back?
F6      Dm         Cmaj7                    F            G7
 Jimmy, Jimmy, oh, Jimmy Mack; when are you coming back?

Verse 1:

C   F/C      C           F/C
 My arms are missing you;
C   F/C           Dm7       G7
 My lips feel the same way, too.
C  F/C      C               F/C
 I tried so hard to be true,
C   F/C   C               F/C
 Li-ike I promised I'd do.
C         F/C       C             F/C
 But this boy keeps coming around,
C               F/C        C             F/C
 He's trying to wear my re-sistance down.

Chorus 2:

F6      Dm         Cmaj7                    F
 Jimmy, Jimmy, oh, Jimmy Mack; when are you coming back?
F       Dm         Cmaj7                  F          G7
 Jimmy, Jimmy, oh, Jimmy Mack; you better hurry back.

Verse 2:

C   F/C      C            F/C
 He calls me on the phone;
C      F/C   Dm7     G7
 About three times a day.
C       F/C          C            F/C
 Now my heart's just listening to,
C        F/C    C   F
 What he has to say.
         C          F/C     C           F/C
But this loneliness, that I have within;
      C            F/C C             F/C
Keeps reaching out, to be his friend.

Chorus 3:

F6      Dm         Cmaj7                    F
 Jimmy, Jimmy, oh, Jimmy Mack; when are you coming back?
F       Dm         Cmaj7                  F          G7
 Jimmy, Jimmy, oh, Jimmy Mack; you better hurry back.

Break:

          C
Need your loving,
             F/C       C
(Jimmy Mack; won't you hurry back?)
F/C        C
 Need your loving,                 F/C         G7
           (Jimmy Mack; you better hurry back).

Interlude:

C  F/C (x4), 

Dm7  G7, C  F/C  G7
                  I wanna say...

Verse 3:

C        F/C     C            F/C
 I'm not getting any stronger;
C        F/C       C         F/C
 I can't hold out, very much longer.
C            F/C C           F/C
 Trying hard,     to be true,
    C               F/C     C            F/C
But Jimmy; he talks just as sweet as you.

Chorus 4:

F6      Dm         Cmaj7                    F
 Jimmy, Jimmy, oh, Jimmy Mack; when are you coming back?
F       Dm         Cmaj7                  F          G7
 Jimmy, Jimmy, oh, Jimmy Mack; you better hurry back.

Coda:

          C                   F/C  C           F/C
Need your loving, (hurry back,     Jimmy Mack).
          Dm7
Need your loving, (hurry... hurry, hurry).
     C          F/C         C          F/C
Hey, Jimmy Mack; you better hurry back.
     C          F/C           C           F/C
Hey, Jimmy Mack; when are you coming back?

C        F/C     C            F/C
 I'm not getting any stronger;
C        F/C       C         F/C
 I can't hold out, very much longer.
C           F/C           C             F/C
 Jimmy Mack; when are you coming back?...

(Fade)
```

CHORD DIAGRAMS:
---------------

``` text
F6      Dm     Cmaj7    F       G7      C       F/C     Dm7

EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
xx3231  xx0231  x32000  133211  323000  x32010  x33211  xx0211
```
