# ...CAROLINE, NO... by The Beach Boys
*from 'Pet Sounds' (1966)*
(modified from Andrew Roger's original tab)

*CAPO 4th FRET*

``` text
(Original Key: Ab)


[Intro]

(n.c)
 (Drums)


[Verse 1]

E6              D6
 Where did your long hair go?
E6                   D6
 Where is the girl I used to know?
E6             Em7       G/A   Dmaj7
 How could you lose that happy glow?
             Dmaj9
Oh Caroline, no.


[Verse 2]

E6             D6
 Who took that look away,
E6                  D6
 I remember how you used to say.
E6           Em7         A7         Dmaj7
 You'd never change, but that's not true.
             Dmaj9
Oh Caroline, you...


[Bridge 1]

         Bm7      E7      Amaj7
Break my heart; I want to go and cry.
        G#m7b5 C#7b9   F#m
It's so sad to watch a sweet thing die.
             Bm7b5
Oh Caroline, why?


[Verse 3]

E6                    D6
 Could I ever find in you again;
E6                            D6
 Things that made me love you so much then?
E6                        Em7        A7       Dmaj7
 Could we ever bring them back, once they had gone?
             Dmaj9
Oh Caroline, no.


[Coda:]

E6  D6, E6  D6,
E6  Em7  A7, Dmaj7  Dmaj9

(Repeat to Fade)
```

``` text
CHORD DIAGRAMS:
---------------

   E6      D6      Em7     A7    Dmaj7   Dmaj9     Bm7

 EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
 022120  xx0202  022030  x02223  xx0222  x5465x  x24232

   E7     Amaj7  G#m7b5   C#7b9    F#m    Bm7b5

 EADGBE  EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
 020100  x02120  4544xx  x4545x  244222  x2323x
```


Tabbed by Joel from cLuMsY, Bristol, England, 2004 (clumsyband@hotmail.com)