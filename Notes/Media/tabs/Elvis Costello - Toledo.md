# Toledo
## Elvis Costello & Burt Bacharach

Tuning: E A D G B E
Key: A
Capo: no capo
Author samithecoral.:) [pro] 687. Last edit on Aug 13, 2021

``` text
Chord Shapes:
 
Em/G      = xx5450 | 3x5450
F#m7      = 244220
Bm7       = x24232
Cadd9     = x32030
Aadd9     = x02420 | x07600
E5        = x79xxx
C#m7      = x46454
Emaj7/G#  = 476444
B/A       = x04442
G#m7      = 464444
Emaj9/G#  = 47447x
Fdim      = xx3434
D         = xx0232
Dm(maj7)  = xx0221
Dm6       = xx0201
 
 
[Intro]
 
  Em/G         F#m7         Em/G         F#m7
| /  /  /  / | /  /  /  / | /  /  /  / | /  /  /  / |
 
 
[Verse 1]
 
Em/G             F#m7          Bm7
 All through the night you telephoned
           Cadd9
 I saw the light blinking red
           Aadd9
 Beside the cradle
Em/G           F#m7              Bm7
 But you don't know how far I've gone
            Cadd9
 Now I must live with the lie
       Aadd9
 That I made
 
 
[Pre-Chorus]
 
 E5  C#m7   Emaj7/G#  F#m7   B/A                   G#m7
 But if     I          call, I know I won't have to say it
 E5     C#m7     Emaj9/G#  F#m7     B/A                    G#m7      Fdim
 You'll hear     my         voice - something is bound to betray it
 
 
[Chorus]
 
Aadd9
 But do people living in Toledo
F#m7
 Know that their name hasn't travelled very well?
D
 And does anybody in Ohio
Bm7
 Dream of that Spanish citadel?
Aadd9
 But it's no use saying that I love you
F#m7
 And how that girl really didn't mean a thing to me
D
 For if anyone should look into your eyes
Bm7
 It's not forgiveness that they're going to see. 
 
 
[Post-Chorus]
 
Aadd9    F#m7         Dm(maj7) Dm           Dm6
 You hear... her voice - "How  could you do that?"
Aadd9    F#m7         Dm(maj7) Dm6
 You hear... her voice - "How  could you do that?"
 
 
[Instrumental]
 
  Em/G         F#m7         Em/G         F#m7
| /  /  /  / | /  /  /  / | /  /  /  / | /  /  /  / |
 
 
[Verse 2]
 
Em/G          F#m7           Bm7
 So I walked outside in the bright
              Cadd9
 Sunshine and lovers pass by
            Aadd9
 Smiling and joking
Em/G            F#m7            Bm7
 But they don't know the fool I was
                 Cadd9
 Why should they care what was lost
         Aadd9
 What was broken?
 
 
[Pre-Chorus]
 
 E5  C#m7   Emaj7/G#  F#m7   B/A                   G#m7
 But if     I          call, I know I won't have to say it
 E5     C#m7     Emaj9/G#  F#m7     B/A                    G#m7      Fdim
 You'll hear     my         voice - something is bound to betray it 
 
 
[Chorus]
 
Aadd9
 But do people living in Toledo
F#m7
 Know that their name hasn't travelled very well?
D
 And does anybody in Ohio
Bm7
 Dream of that Spanish citadel?
Aadd9
 But it's no use saying that I love you
F#m7
 And how that girl really didn't mean a thing to me
D
 For if anyone should look into your eyes
Bm7
 It's not forgiveness that they're going to see.
 
 
[Outro]
 
Aadd9
 But do people living in Toledo
F#m7
 Know that their name hasn't travelled very well?
D
 And does anybody in Ohio
Bm7
 Dream of that Spanish citadel?
Aadd9
 But we still have Florence, Alabama
F#m7
 We don't have Paris, and we don't have Rome 
D
 Or New York, or even Amsterdam
Bm7                                         Aadd9
 None of those lonely towns will be my home... 

```
