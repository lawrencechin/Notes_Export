# Another Man's Done Gone

Words: Woody Guthrie
Music: Jeff Tweedy/Jay Bennett

Chord-figuring: Judd Bolger <iamjudd@hotmail.com>

This is an all piano song, so I doesn't do it justice on guitar but it's 
still such a good song it doesn't matter that much. Here you are.

```
G         G/F                C      G
Sometimes I think I'm gonna lose my mind
Em7                      C/D    D
But it don't look like I ever do
G             G/F         C             G
I've loved so many people everywhere I went
Em7            D
Some too much, other's not enough
Cmaj7              B7        Em               A7
Well I don't know, I may go, Down or up or anywhere
Cmaj7    Bmin                          Amin7 C/D D
but I feel, like this scribbling might stay
G          G/F            C         G
Maybe if I hadn't seen so much hard feelings
Emin7               C/D                D
might not, could've felt other people's
G         G/F          C               G
So if you think of me, if and when you do
Emin7                   C/D    D   G
just say "well, another man's done gone."
Emin7        D          G
well another man's done gone
```


