# Back For Good

NORMAL TUNING, with capo III.

D      x00232
G      320033
Em7    022033
Bm     x24432
Bm/A   x04432
A7     x02020
A7sus4 x04030
Em/G   x05050

Introduction:
D / Em7 / G /{D A} (OR Asus4 and then resolve to A)

To really get the right 'feel' for the strumming pattern, try delaying the down-strum 
which gives an arpeggio effect - like done in the original.

VERSE 1

``` text
D     Em7           G D                D  
Em7
I guess now it's time for me to give up
             G    A7 > A7sus4 > Em/G
I feel it's time
      D 		   Em7
Got a picture of you beside me
      G                  A
Got a lipstick mark still on
            D    Em7
Your coffee cup
   G     A
Oh yeah
      D             Em7
Got a fist of pure emotion
      G                  A
Got a head of shattered dreams
      Bm              Bm/A
Gotta leave it, gotta leave it
      G         A
All behind now
```

CHORUS

``` text
        D                Em7
Whatever I said, whatever I did I
    G
Didn't mean it
        A                 D
I just want you back for good
                  Em7             G                  A
Want you back, want you back, want you back for good
    D                   Em7
Whatever I'm wrong just tell me
                  G
The song and I'll sing it
           A               D
You'll be right and understood
              Em7            G                     A
Want you back, want you back, I want you back for good
```

VERSE 2

``` text
D      Em7         G     A
Unaware but underlined
                    D         Em7
I figured out the story (no, no)
            G          A7 > A7sus4 > Em/G
It wasn't good (no, no)
           D            Em7     G
But in the corner of my mind
A                D    Em7
I celebrated glory
         G           A
But that was not to be
        D           Em7
In the twist of separation
     G                  A
You excelled at being free
          Bm
Can't you find, (can't you find)
         Bm/A           G     A
A little room inside for me
```

CHORUS

BRIDGE

``` text
G7               D/F#   G7                D/F#
And we'll be together, this time is forever
Em                        D/F#
We'll be fighting and forever we will be
       Bm         Bm/A
So complete in our love
        G7        D/F#        A     A7 > 
> Em/G
We will never be uncovered again
```
