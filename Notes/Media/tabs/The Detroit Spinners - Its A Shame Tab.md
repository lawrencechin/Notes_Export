# Its A Shame Tab
(Garrett - Wonder - Wright)

``` text
Intro (two guitars):

G#m7        G#m         F#maj7        D#m            [3X]
xx647x     xx644x        xx436x      xx434x
v     v     v     v       v     v     v     v
------4s6----4-----------|-------------------------|
--------------------2-4-5-s6----5s6--2-4-----------|
-------------------------|-------------------------|
-------------------------|-------------------------|
-------------------------|-------------------------|
-------------------------|-------------------------|

(bass drum, high-hat enter)

Bmaj7        B6           Bm7         Bm6
xxx446      xxx444       xxx435      xxx434
v     v     v     v       v     v     v     v
------4s6----4-----------|4s5---4s5--4-------------|
-------------------------|-------------------------|
-------------------------|-------------------------|
-------------------------|-------------------------|
-------------------------|-------------------------|
-------------------------|-------------------------|

G#m7         G#m7/C# (bass, tambourine enter)
/  /  /  /    /  /  /  /
Ooh-do-do-do  Ooh-do-do-do

Verse 1 (guitars continue as above; chords indicated are composites of
all parts):

   G#m9     G#m7/C#               F#maj7                    D#m
It's a shame (sha-ame), the way you mess around with your man
   G#m9     G#m7/C#            F#maj7    D#m
It's a shame (sha-ame), the way you hurt me
   G#m9     G#m7/C#               F#maj7                    D#m
It's a shame (sha-ame), the way you mess around with your man
       Bmaj7   B6        Bm7   Bm6
I'm sitting all alone  by the telephone
   G#m7                          G#m7/C#
Waiting for your call  when you don't call at all

Verse 2:

It's a shame (sha-ame), the way you mess around with your man
It's a shame (sha-ame), the way you play with my emotions
It's a shame (sha-ame), the way you mess around with your man
You're like a child at play  on a sunny day
But you play with love  and then you throw it away

Bridge:

      G#m9    G#m7/C#          F#maj7   D#m
Why do you use me         try to confuse me
        G#m9  G#m7/C#          F#maj7   D#m
How can you stand         to be so cruel
          Amaj7   Dmaj7
Why don't you free me       from this prison
        Emaj7                 G#m7   G#m7/C#
Where I serve my time as your fool

Verse 3:

It's a shame (sha-ame), the way you mess around with your man
It's a shame (sha-ame), the way you hurt me
It's a shame (sha-ame), the way you mess around with your man
I try to stay with you  show you love so true
But you won't appreciate  the love we try to make

[repeat first two lines of intro (two guitars w/drums)]

Oh, it's-a got to be a shame

[repeat bridge]

Verse 4:

Got to be a shame (sha-ame), the way you mess around with your man
Ohhh, it's a shame (sha-ame), the way you hurt me
It's a shame (sha-ame), the way you mess around with your man
You've got my heart in chains  and I must complain
I just can't be content  oh, look at (unintelligible)

Coda:

[vocal improvisation over verse chords; fade on 4th line]
```

Another ace 70's tab from Andrew Rogers
