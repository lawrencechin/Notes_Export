# Hounds of love

Artist: Kate Bush
Composer: Kate Bush
Album: Hounds of love

Tabbed by DJ ('DiddyD' at ultimateGuitar.com)
11th Sept 2014

N.B.
Please IGNORE ANY POP-UP CHORD BOXES the website
might automatically suggest and use the typed shapes
shown below:

``` text
F      = xx3211    Dm    = xx0231    Fsus2/D = xx0031
C      = x32010    Am    = x02210    Bb6     = x13333 or xx3333
Bbmaj7 = x13231    Fsus2 = xx3011    Dsus2   = xx0230

```

``` text
[Intro]
( It's in the trees! It's coming!)


[Verse 1]
F                 C                   Bbmaj7
   When I was a child, running in the night,
                     F                    Dm                  Bbmaj7
Afraid of what might be ...hiding in the dark, hiding in the street,
                          Dm              C                   Bbmaj7
And of what was following me, (Doo - doo, doo - doo, doo, d'- doo!)
               Am                Dm             Bbmaj7               Am
Now hounds of love are hunting,     (Urh - urh, urh - urh, urh, urh, urh!)
             C                   Dm             Bbmaj7               Am
I've always been a coward,          (Urh - urh, urh - urh, urh, urh, urh!)
             C                   Dm
And I don't know what's good for me!


[Chorus]
            F  Fsus2  Dm      Fsus2/D  Bbmaj7   Bb6
Oh, here I go,           it's coming    for me through the trees,
F      Fsus2    Dm       Fsus2/D    Bbmaj7  Bb6
   Oh, help me, someone! Help me,   please!
F      Fsus2    Dm       Fsus2/D    Bbmaj7  Bb6
   You take my shoes off,       and throw------- them in the lake,
             F   Fsus2   Dm      Fsus2/D   Bbmaj7  Bb6
And I'll be-----     ...two steps   on the water!


[Verse 2]
F             C            Bbmaj7                          F
   I found a fox caught by dogs, he let me take him in my hands,
             C                  Bbmaj7
His little heart ...it beats so fast,
                                F                  Dm
And I'm ashamed of running a - way ...from nothing real,
             Bbmaj7
I just can't deal with this,
                    Dm                       C                 Bbmaj7
I'm still afraid to be there... (Doo - doo, doo - doo, doo, d'- doo!)
               Am               Dm             Bbmaj7               Am
...Among your hounds of love,      (Urh - urh, urh - urh, urh, urh, urh!)
               C                Dm             Bbmaj7               Am
And feel your arms surround me,    (Urh - urh, urh - urh, urh, urh, urh!)
              C                 Dm             Bbmaj7               Am
I've always been a coward,         (Urh - urh, urh - urh, urh, urh, urh!)
         C                   Dm    Dsus2
I never know what's good for me!


[Chorus]
            F   Fsus2  Dm        Fsus2/D Bbmaj7  Bb6
Oh, here I go,          ...don't let me  go,
            F   Fsus2        Dm   Fsus2/D  Bbmaj7      Bb6
Hold--- me down,    ...it's coming for me through the trees,
F      Fsus2     Dm       Fsus2/D    Bbmaj7   Bb6
   Oh, help me, darling,   help me,  please,
F      Fsus2     Dm       Fsus2/D    Bbmaj7   Bb6
   And take my shoes off,        and throw--------- them in the lake,
             F   Fsus2   Dm     Fsus2/D    Bbmaj7  Bb6
And I'll be-----     ...two steps  on  the water!
                                 F
(Doo - doo, doo - doo, doo, d'- doo!)


[Coda]
F    Fsus2              Dm            Fsus2/D           Bbmaj7
   I don't know what's good for me, I don't know what's good for me,
Bb6
     I need love -love -love,  love -love -love,
 F    Fsus2   Dm   Fsus2/D   Bbmaj7 Bb6                    F
Yeah, your... yeah, your...   your love,  (Doo - doo, d'- doo!)
    Fsus2      Dm        Fsus2/D     Bbmaj7                     Bb6
And take your shoes off,      ...and throw--- them in the lake!
F          Fsus2         Dm                Fsus2/D      Bbmaj7
    Do you know what I really need?  Do you know what I really need?
Bb6                                        F
    Love -love -love,  love -love -love,  yeah-----!
```