# Stay Young

Notes:
- This is based on the acoustic version Noel performed for a Munich radio station on
July 18, 2005. You can download the song here: http://www.savefile.com/files/6158470
- In regards to the D chords intro and break, listen to the mp3 and you will understand
how to play it.

``` text
Chords:
D     = xx0232
Dsus2 = xx0230
Dsus4 = xx0233
Em7   = 022033
G     = 320033
F#    = 244322
G/F#  = 2x0033
```

``` text
Intro:
D  Dsus2  D  Dsus4  D  Dsus2  D  
Em7  G
D  Dsus2  D  Dsus4  D  Dsus2  D
Em7  G
```

``` text
Verse 1:
D              Em7             G
One way out is all  you're ever gonna get from
D                      Em7
Those who'll hand them out
           G
Don't ever let it upset you
    D                   Em7
Cos they'll put words into  your mouth


Pre-Chorus:
        G                   Em7
They're making you feel so ashamed
        G                     Em7
They're making you taking the blame
G                      Em7
Making you cold in the night
        G
They're making you question
     A
Your heart and your soul

And I think that it's not quite right


Chorus:
D        F#          G        G/F#
Hey stay young and invincible
       Em7               A
Cos we know just what we are
    D         F#          G         G/F#
And come what may we're unstoppable
       Em7               A   G  G/F#
Cos we know just what we are
       Em7                A   G  G/F#
Yeah we know just what we are
       Em7
Yeah we know just what we are


Dsus4  D  Dsus2  D  Dsus4  D  Dsus2  D
Em7  G
D  Dsus2  D  Dsus4  D  Dsus2  D
Em7  G


Verse 2:
D                   Em7             
Feed your head with all the things 
    G
You need When you're hungry
D               Em7  
Stay in bed and sleep 
           G
All day as long as it's Sunday
    D                   Em7 
Cos they'll put words into  my mouth


Pre-Chorus:
        G                  Em7
They're making us feel so ashamed
        G                    Em7
They're making me taking the blame
G                     Em7
Making me cold in the night
        G
They're making me question
   A
My heart and my soul

And I think that it's not quite right


Chorus:
D        F#          G        G/F#
Hey stay young and invincible
       Em7               A
Cos we know just what we are
    D         F#          G         G/F#
And come what may we're unstoppable
       Em7               A
Cos we know just what we are
D        F#          G        G/F#
Hey stay young and invincible
       Em7               A
Cos we know just what we are
    D         F#               G        G/F#
And come what may my faith's unshakable
       Em7               A   G  G/F#
Cos we know just what we are
       Em7                A   G  G/F#
Yeah we know just what we are
       Em7                A   G  G/F#
Yeah we know just what we are
       Em7               
Yeah we know just what we are


Dsus4  D
```
