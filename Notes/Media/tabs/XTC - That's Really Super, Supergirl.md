# [That's Really Super, Supergirl](http://chalkhills.org/reelbyreal/s_ThatsReallySuperSupergirl.html) 

## Chords with Lyrics

``` text
                 Bb6
I can't hold you down If you want to fly
                  Eb               Cm
Can't you see I'm all broke up inside
     Fsus              F7        Bb6         Faug
Well just you use your two X Ray eyes

               Bb6
Hurt like Kryptonite Put me on my knees
                    Eb                   Cm
Now that I've found out just what you're doing
Fsus             F7     Bb6             C
With your secret identities

              Ab6
That's really super Supergirl
Bb
How you saved yourself in seconds flat And your friends are going to say
              Ab6
That's really super Supergirl
Bb
How you're changing all the world's weather
But you couldn't put us back together
Now I feel like I'm tethered deep
  Cm9                   Bb/D
Inside your Fortress of Solitude
      Ebmaj7
Don't mean to be rude
                 F6   F
But I don't feel super
     Bb6
Supergirl

I won't call again Even in a jam
Now I realise you could be on a mission
Saving some other man

That's really super Supergirl
How you saved yourself in seconds flat
And your friends are going to say
That's really super Supergirl
How you stopped the universe from dying
But you're never going to stop me crying
And I feel like you're trying hard
To sweep me like dirt underneath your cape
Well I might be an ape
But I used to feel super
Supergirl

That's really super Supergirl
How you saved yourself in seconds flat
And your friends are going to say
That's really super Supergirl
How you're changing the world's weather
But you couldn't put us back together
Super Supergirl
How you stopped the universe from dying
But you're never going to stop me crying
Super Supergirl
I'm here in your Fortress of Solitude
Don't mean to be rude
But I don't feel super

```

## Solo Tab

``` text

           Bb
           ^       ^       ^       ^
e:------|----------------------------------|
B:--8---|-(8)--8-8-6-------------------8-8-|
G:------|--------------7---5-/-7-----------|
D:------|----------------------------------|
A:------|----------------------------------|
E:------|----------------------------------|



    ^       ^       ^       ^          ^       ^       ^       ^
e:----------------------------------|------------------------8-5---------|
B:-(8)8-8-8-6-------------------8---|-(8)8-8-8-6-----------8-------------|
G:--------------7---5-/-7-----------|--------------7---5-7---------10-12-|
D:----------------------------------|------------------------------------|
A:----------------------------------|------------------------------------|
E:----------------------------------|------------------------------------|



     Bb
     ^          ^           ^          ^
e:-----13-10------------15-12------------20-17----|
B:-------------------15---------------20-------18-|
G:-(12)--------12-14------------17-19-------------|
D:------------------------------------------------|
A:------------------------------------------------|
E:------------------------------------------------|



    Eb
    ^             ^         ^        ^
e:--------------------------------------------|
B:-16---16h16p15-13---------------13-----11---|
G:--------------------15---13-----------------|
D:--------------------------------------------|
A:--------------------------------------------|
E:--------------------------------------------|



     F7
     ^          ^           ^             ^
e:----------10-11-10------------------------------|
B:-(11)--13----------13-11-10h11p10------10-------|
G:----------------------------------12------------|
D:------------------------------------------------|
A:------------------------------------------------|
E:------------------------------------------------|



   Bb                                         C
   ^       ^       ^          ^               ^
e:----------------------------------------|----------||
B:-8---------------------------------13---|-(13~)----||
G:-7---------------7------10-12b(14)------|----------||
D:-7-----------7h8---9/12-----------------|----------||
A:----------------------------------------|----------||
E:----------------------------------------|----------||

```

## Bass Tab

``` text
Q=111  4/4   Tuning=EADG

                        [Verse]
   W          W          Q.   E Q  E E   +Q   Q  H
G|----------|----------|---------------|--------------|
D|----------|----------|---------------|--------------|
A|----------|----------|-1-----------1-|-(1)----------|
E|----------|----------|------1-0--1---|--------------|


   Q.   E Q  E E   +Q   Q  H      E E E E E E E E   +E  E E E E E Q
G|---------------|--------------|-----------------|------------------|
D|---------------|--------------|-1-0-------------|-------------3----|
A|-1-----------1-|-(1)----------|-----3-1---------|-------0-1-3---3--|
E|------1-0--1---|--------------|---------4---3-1-|-(1)-3------------|


   Q.   E Q  E E   +W           Q.   E Q  E E   +Q   Q  E E E E
G|---------------|------------|---------------|-----------------|
D|-------------3-|-(3)--------|---------------|-----------------|
A|-1-------------|------------|-1-----------1-|-(1)-------------|
E|------1-0--1---|------------|------1-0--1---|-----------3-1---|


   Q.   E Q  E E   +Q   Q  H      E E E E E E E E   +E  E E E E E Q
G|---------------|--------------|-----------------|------------------|
D|---------------|--------------|-1-0-------------|-------------3----|
A|-1-----------1-|-(1)----------|-----3-1---------|-------0-1-3---3--|
E|------1-0--1---|--------------|---------4---3-1-|-(1)-3------------|

                                  [Chorus]
   Q.   S S Q  S S E   +W           E E E E E E E E   E E E E E E E E
G|-------------------|------------|-----------------|-----------------|
D|-------------------|------------|-----------------|-6-5-3-----------|
A|-1---------------3-|-(3)--------|-----------3---3-|-------6-5-3---1-|
E|------1---0--1-----|------------|-4-3-1-3-4---4---|-------------4---|


   +Q   E E Q  E E   +Q   E E Q  E E   E E E E E E E E   E E E E E E E E
G|-----------------|-----------------|-----------------|-----------------|
D|------3----------|------3----------|-----------------|-----------------|
A|-(1)----1------1-|-(1)----1--------|-----------------|---------------1-|
E|----------1--3---|----------1--3---|-4-4---4-3-3---1-|---1---1-1-1-----|


   +Q   E E Q  E E   +Q   E E Q  E E   Q  Q  E E Q    Q  E Q.   Q
G|-----------------|-----------------|--------------|--------------|
D|------3----------|------3----------|--------------|--------------|
A|-(1)----1------1-|-(1)----1--------|--------------|--------------|
E|----------1--3---|----------1--3---|-3-----3-3----|-5----5-------|

                                                [Interlude]
   Q  Q  E E Q    E E E E E E E E   Q  Q  H      Q.   E Q  E E
G|--------------|-----------------|------------|---------------|
D|-1-----1-1----|-3-3-3-3-1-0-----|------------|---------------|
A|--------------|-------------3-1-|------------|-1-----------1-|
E|--------------|-----------------|-1----------|------1-0--1---|

                                                  [Verse]
   +Q   Q  E E E E   Q.   E Q  E E   +Q   Q  H      Q.   E Q  E E
G|-----------------|---------------|--------------|---------------|
D|-----------------|---------------|--------------|---------------|
A|-(1)-------------|-1-----------1-|-(1)----------|-1-----------1-|
E|-----------3-1---|------1-0--1---|--------------|------1-0--1---|


   +Q   Q  H      Q.   E Q  E E   +Q   Q  H      E E E E E E E E
G|--------------|---------------|--------------|-----------------|
D|--------------|---------------|--------------|-1-0-------------|
A|-(1)----------|-1-----------1-|-(1)----------|-----3-1---------|
E|--------------|------1-0--1---|--------------|---------4---3-1-|

                                                 [Chorus]
   +E  E E E E E Q    Q.   E Q  E E   +W           E E E E E E E E
G|------------------|---------------|------------|-----------------|
D|-------------3----|---------------|------------|-----------------|
A|-------0-1-3---3--|-1-----------3-|-(3)--------|-----------3---3-|
E|-(1)-3------------|--------1--3---|------------|-4-3-1-3-4---4---|


   E E E E E E E E   +Q   E E Q  E E   +Q   E E Q  E E   E E E E E E E E
G|-----------------|-----------------|-----------------|-----------------|
D|-6-5-3-----------|------3----------|------3----------|-----------------|
A|-------6-5-3---1-|-(1)----1------1-|-(1)----1--------|-----------------|
E|-------------4---|----------1--3---|----------1--3---|-4-4---4-3-3---1-|


   E E E E E E E E   +Q   E E Q  E E   +Q   E E Q  E E   Q  Q  E E Q
G|-----------------|-----------------|-----------------|--------------|
D|-----------------|------3----------|------3----------|--------------|
A|---------------1-|-(1)----1------1-|-(1)----1--------|--------------|
E|---1---1-1-1-----|----------1--3---|----------1--3---|-3-----3-3----|


   Q  E Q.   Q    Q  Q  E E Q    E E E E E E E E   Q  Q  H
G|--------------|--------------|-----------------|------------|
D|--------------|-1-----1-1----|-3-3-3-3-1-0-----|------------|
A|--------------|--------------|-------------3-1-|------------|
E|-5----5-------|--------------|-----------------|-1----------|

 [Guitar solo]
   Q.   E Q  E E   +Q   Q  E E E E   Q.   E Q  E E   +Q   Q  H
G|---------------|-----------------|---------------|--------------|
D|---------------|-----------------|---------------|--------------|
A|-1-----------1-|-(1)-------------|-1-----------1-|-(1)----------|
E|------1-0--1---|-----------3-1---|------1-0--1---|--------------|

   E E E E E E E E   +E  E E E E E Q    Q.   E Q  E E   +W
G|-----------------|------------------|---------------|--------|
D|-1-0-------------|-------------3----|---------------|--------|
A|-----3-1---------|-------0-1-3---3--|-1-----------3-|-(3)----|
E|---------4---3-1-|-(1)-3------------|------3-1--3---|--------|

 [Chorus]
   E E E E E E E E   E E E E E E E E   +Q   E E Q  E E   +Q   E E E E E E
G|-----------------|-----------------|-----------------|------------------|
D|-----------------|-----------------|------3----------|------3-----------|
A|-----------------|---------------1-|-(1)----1------1-|-(1)----1---------|
E|-4-4---4-3-3---1-|---1---1-1-1-----|----------1--3---|----------1---3---|


   E E E E E E E E   E E E E E E E E   +Q   E E Q  E E   +Q   E E Q  E E
G|-----------------|-----------------|-----------------|-----------------|
D|-----------------|-----------------|------3----------|------3----------|
A|-----------------|---------------1-|-(1)----1------1-|-(1)----1--------|
E|-4-4---4-3-3---1-|---1---1-1-1-----|----------1--3---|----------1--3---|


   E E E E E E E E   E E E E E E E E   +Q   E E Q  E E   +Q   E E Q  E E
G|-----------------|-----------------|-----------------|-----------------|
D|-----------------|-----------------|------3----------|------3----------|
A|-----------------|---------------1-|-(1)----1------1-|-(1)----1--------|
E|-4-4---4-3-3---1-|---1---1-1-1-----|----------1--3---|----------1--3---|


   E E E E E E E E   E E E E E E Q    Q  Q  E E Q    Q  E Q.   Q
G|-----------------|----------------|--------------|--------------|
D|-----------------|-6-5-3----------|--------------|--------------|
A|-----------3---3-|-------6-5-3----|--------------|--------------|
E|-4-3-1-3-4---4---|-------------6--|-3-----3-3----|-5----5-------|


   Q  Q  E E Q    E E E E E E E E   W
G|--------------|-----------------|------*|
D|-1-----1-1----|-----------1-----|------*|
A|--------------|-----0-1-3-------|------*|
E|--------------|-1-3---------4-3-|-1----*|



Duration Legend
---------------
W - whole
H - half
Q - quarter
E - 8th
S - 16th
T - 32nd
X - 64th
a - acciaccatura
+ - note tied to previous
. - note dotted
.. - note double dotted
Uncapitalized letters represent notes that are staccato (1/2 duration)
Irregular groupings are notated above the duration line  
Duration letters will always appear directly above the note/fret  
number it represents the duration for.
Duration letters with no fret number below them represent rests.
Multi-bar rests are notated in the form Wxn, where n is the number of bars to rest for.
Low melody durations appear below the staff

```

## Lyrics

``` text
I can't hold you down
If you want to fly
Can't you see I'm all broke up inside
Well just you use your two X Ray eyes

Hurt like Kryptonite
Put me on my knees
Now that I've found out just what you're doing
With your secret identities

That's really super Supergirl
How you saved yourself in seconds flat
And your friends are going to say
That's really super Supergirl
How you're changing all the world's weather
But you couldn't put us back together
Now I feel like I'm tethered deep
Inside your Fortress of Solitude
Don't mean to be rude
But I don't feel super
Supergirl

I won't call again
Even in a jam
Now I realise you could be on a mission
Saving some other man

That's really super Supergirl
How you saved yourself in seconds flat
And your friends are going to say
That's really super Supergirl
How you stopped the universe from dying
But you're never going to stop me crying
And I feel like you're trying hard
To sweep me like dirt underneath your cape
Well I might be an ape
But I used to feel super
Supergirl

That's really super Supergirl
How you saved yourself in seconds flat
And your friends are going to say
That's really super Supergirl
How you're changing the world's weather
But you couldn't put us back together
Super Supergirl
How you stopped the universe from dying
But you're never going to stop me crying
Super Supergirl
I'm here in your Fortress of Solitude
Don't mean to be rude
But I don't feel super

© 1987 Virgin Music (Publishers) Ltd.

```
