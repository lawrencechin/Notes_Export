# You've Changed by Ella Fitzgerald

## Chords

  |  |  |  
 -- | -- | -- | --
A7b9 | A9 | Am7 | Am9
6-x-5-6-5-x | x-4-5-4-5-x | 5-x-5-5-5-x | 5-x-5-5-5-7
B7 | Bb13 | Bbdim | Bbm7
7-x-7-8-7-x | 6-x-6-7-8-x | 6-x-5-6-5-x | 6-x-6-6-6-x
Bm | Bm7 | Bm7b5 | C
7-x-7-7-7-x | 7-x-7-7-7-x | 7-x-7-7-6-x | 3-3-5-5-5-3
C6add9 | D | D#7 | D#9
3-x-2-2-3-3 | 5-5-7-7-7-5 | 6-x-5-6-4-x | 6-x-5-6-6-6
D13 | D7 | D7#5 | D7b5
x-5-4-5-5-7 | 5-x-4-5-3-x | 5-x-4-5-6-x | 5-x-4-5-4-x
Dm7 | E7 | E7b5 | E9
10x101010-x | 7-x-6-7-5-x | 7-x-6-7-6-x | 0-x-6-7-7-7
Em7 | F#7#5 | F#9/B | F6
7-x-5-7-5-x | 2-x-2—3-3-x | x-2-3-2-3-x | 5-x-3-5-3-x
G | G6 | G7 | G9
3-5-5-4-3-3 | 3-x-2-4-3-x | 3-5-3-4-3-3 | 10x-9101010
Gm6 | Gmaj7 | Gmaj7/B | 
3-x-2-3-3-3 | 3-x-4-4-3-x | 7-x-5-7-7-x | 

## Chords with Lyrics

``` text
        G                 Am7           Bm
I've an awfully funny feeling that this thought
              C                   Gmaj7                  B7
That's been a-stealin' through my brain is not to be ignored
E9     Bm              Gm6               D                Gm6
But to really tell the truth, though I'm not a well-known sleuth
  D        Em7          A7b9     Am9  Am7 D7b5
I honestly believe that you are bored
 
[Refrain]
D7     Gmaj7/B
You've changed
      F#9/Bb         F#7#5   Bm7b5
That sparkle in your eyes is gone
     E7              E7b5     A9
Your smile is just a careless yawn
       D#7         D13
You're breaking my heart
D13     Em7       Bbdim Am7 D7b5
You've changed
 
[Refrain]
D7     Gmaj7/B
You've changed
     F#9/Bb         F#7#5 Bm7b5
Your kisses now are so    blase
       E7               E7b5  A9
You're bored with me in every way
  D#7        D13
I can't understand
D13      Dm7    G9
You've changed
 
[Bridge]
G      C6add9                 F9
You've forgotten the words "I love you"
Bm7                    F6     G7
Each memory that we've shared
    C6add9            F9
You ignore every star above you
  Bm7               Bbm7 D#9    Am7 D7
I can't realize you ever cared
 
[Refrain]
D7     Gmaj7
You've changed
       F#9/B         F#7#5  Bm7b5
You're not the angel I once knew
   E7              E7b5       A9
No need to tell me that we're through
     D#7      D13
It's all over now
D13
You've changed
G6 Bb13 Am7 D7#5
 
[Outro]
D7b5   Gmaj7
You've changed
       F#9/Bb        F#7#5  Bm7b5
You're not the angel I once knew
   E7              E7b5       A9
No need to tell me that we're through
     D#7            D13
It's all over now
          Dm7      G13
Yes, it’s all over now
D13    G6
You've changed
G6 Bb13 Am7 D7#5 D7b5 G6

```
