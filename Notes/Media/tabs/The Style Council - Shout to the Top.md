# Shout to the Top - The Style Council

---------------------------------------------------------------------------
Tabbed by: maguri
Tuning: Standard

The Style Council
Shout to the Top (1985)
(Paul Weller)

---------------------------------------------------------------------------

``` text
Basic Riff (verse / chorus / coda)

  Am7(add9)
  1 + 2 + 3 + 4 +  1 + 2 + 3 + 4 +  1 + 2 + 3 + 4 +  1 + 2 + 3 + 4 +
e|7---5---7-----7-|--5---7---------|7---5---7-----7-|--5-------------|
B|5---5---5-----5-|--5---5---------|5---5---5-----5-|--5---7---------|
G|5---5---5-----5-|--5---5---------|5---5---5-----5-|--5---5---------|
D|5-------------5-|----------------|5-------------5-|------5---------|
A|0-------------0-|----------------|0-------------0-|----------------|
E|----------------|----------------|----------------|----------------|
  Gmaj7
  1 + 2 + 3 + 4 +  1 + 2 + 3 + 4 +  1 + 2 + 3 + 4 +  1 + 2 + 3 + 4 +
e|2---0-----0-2-2-|--0-------0-2---|2-2-0-----0-2-2-|--0-------0-2---|
B|0---0---3-0-0-0-|--0---3---0-0---|0-0-0---3-0-0-0-|--0---3---0-0---|
G|0---0---0-0-0-0-|--0---0---0-0---|0---0---0-0-0-0-|--0---0---0-0---|
D|0-------0-----0-|------0---0-----|0-------0-----0-|------0---0-----|
A|----------------|----------------|----------------|----------------|
E|3-------------3-|----------------|3-------------3-|----------------|

```

---------------------------------------------------------------------------
CHORDS

``` text
        E-A-D-G-B-E
Am7     x-0-2-0-1-0
Gmaj7   3-x-0-0-0-2
Dsus4   x-x-0-2-3-3
Am7add9 x-0-5-5-5-7
```
---------------------------------------------------------------------------

``` text
INTRO
(breaks)
| Am7(add9) | % | % | % | Gmaj7 | % |
| Am7(add9) | % | % | % | Gmaj7 | % |
| Am7(add9) | % | % | % | Gmaj7 | % |
| Am7(add9) | % | % | % | Gmaj7 | % |
```

``` text
VERSE
            Am7
Mmh, I was half in mind - I was half in need
           Am7                                        | Gmaj7 | % |
And as the rain came down - I dropped to my knees and I prayed
            Am7
I said "Oh, Heavenly thing - please cleanse my soul
          Am7                                | Gmaj7 | % | % | % |
I've seen all on offer and I'm not impressed at all"
          Am7
Mmh I was halfway home - I was half insane
           Am7                                  | Gmaj7 | % |
And every shop window I looked in just looked the same
       Am7
I said send me a sign to save my life
               Am7 
'Cause at this moment in time 
                                 | Gmaj7 | % | % | % |
There is nothing certain in these days of mine


BRIDGE
             Cmaj7
Y'see it's a frightening thing when it dawns upon you
     Dsus4
That I know as much as the day I was born
           Cmaj7
And though I wasn't asked (I might as well stay)
    Dsus4
And promise myself each and every day - that -


VERSE
            Am7
When you're knocked on your back - an' your life's a flop
                Am7
And when you're down on the bottom there's nothing else
       Gmaj7
But to shout to the top


CHORUS
                  Am7
Well, we're gonna shout to the top
            Am7
We're gonna shout to the top
                       Gmaj7
Mmh mh mh, we're gonna shout to the top
                  Gmaj7
Yeah, we're gonna shout to the top


BRIDGE
             Cmaj7
Y'see it's a frightening thing when it dawns upon you
     Dsus4
That I know as much as the day I was born
           Cmaj7
And though I wasn't asked I might as well stay
    Dsus4
And promise myself each and every day that ...


CHORUS (breaks]
          Am7add9
I'm gonna shout to the top – shout!
            Am7add9
We're gonna shout to the top – shout!
                       Gmaj7
Mmh mh mh, we're gonna shout to the top - shout!
            Am7add9
We're gonna shout to the top – shout!
            Am7add9
We're gonna shout to the top – shout!
                   Gmaj7
Ooooh, we're gonna shout to the top - shout!


CODA
               Am7
So when you're knocked on your back - an' your life's a flop
                Am7
And when you're down on the bottom there's nothing else
       Gmaj7
But to shout to the top – shout!

                   Am7
Ooooh, we're gonna shout to the top - shout!
            Am7
We're gonna shout to the top – shout!
                       Gmaj7
Mmh mh mh, we're gonna shout to the top - shout!

                Am7
And when you're knocked on your back - an' your life's a flop
                Am7
And when you're down on the bottom there's nothing else
       Gmaj7
But to shout to the top – shout!

            Am7
We're gonna shout to the top - shout!
            Am7
We're gonna shout to the top – shout!
                       Gmaj7
Mmh mh mh, we're gonna shout to the top - shout!

Am7
Shout to the top - shout!
            Am7
We're gonna shout to the top – shout!
                       Gmaj7
Mmh mh mh, we're gonna shout to the top - shout!


(repeat and fade, vocals ad lib)
```
