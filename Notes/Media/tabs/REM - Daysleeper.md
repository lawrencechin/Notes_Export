# Daysleeper

Standard Tuning - EADGBE

``` text
{--------------------!------------------!--------------------!--------------------||}
|-----------8--------|------------8-----|-----------8--------|------------8-------||
|--------9--------0--|-------0----------|--------9--------0--|-------0------------||
|--10---------10-----|--9---------------|--10---------10-----|--9-----------------||
|--------------------|------------------|--------------------|--------------------||
[--------------------@------------------@--------------------@--------------------||]
```

Intro:
4x      C   Cmaj7

VERSE:

``` text
C     Cmaj7      C    Cmaj7
Receiving department, 3 AM
C     Cmaj7      C    Cmaj7
Staff cuts have socked up the overage
C     Cmaj7      C    Cmaj7
Directives are posted No call backs, complaints
C     Cmaj7      C    Cmaj7
Everywhere is calm
```

VERSE TO CHORUS:

``` text
D#                    Dm
Hong Kong is present, Taipei wakes up
F                G
Talk of circadian rhythm
```

CHORUS:

``` text
C         Dm          Am      Em
I see today with a newsprint frame
C         D          G
My night is colored headache-grey
C         Dm          Am       Em
Daysleeper, daysleeper
C         D           G
daysleeper
```

{Verse Chords}
The bull and the bear are marking their territories
They're leading the blind with their international glories

{Verse to Chorus Chords}
I('m the) screen, the blinding light
I('m the) screen, I work at night

{Chorus Chords}
I see today with a newsprint frame
My night is colored headache-grey
Don't wake me
(You're) so much
Daysleeper

2x Dsus4

BRIDGE:

``` text
C      Cmaj7      C
I cried the other night
C      Cmaj7      C
I can't even say why
C      Cmaj7      C
flourescent, flat, caffeine lights
C      Cmaj7      C
It's furious balancing
```

{Verse To Chorus Chords}
I('m the) screen, the blinding light
I('m the) screen, I work at night

{Chorus Chords}
I see today with a newsprint frame
My night is colored headache grey
Don't wake me (you're) so much
The ocean machine is set to night
I'll squeeze into heaven and Valentine
My bed is pulling me, gravity, daysleeper
Daysleeper, daysleeper, daysleeper, daysleeper

End on C

-----

Daysleeper by R.E.M

From Anders Kjeldsen 29.10.98 (AndersK@gangstah.net)

Bb       113331
Bbmaj7   113231
F        133211
G        355411
C        032010
D        000232
Am       002210
E        022100

``` text
e-|---3---3---3------------------3----------------------------------|
B-|---1---1------0-h-1r--0------------------------------------------|
G-|---0---0------------------0--------------------------------------|
D-|---2---2---------------------------------------------------------|
A-|---3---3---------------------------------------------------------|
E-|---0---0---------------------------------------------------------|
```

Use this riff as the fill.
