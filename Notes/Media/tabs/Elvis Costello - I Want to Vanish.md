# I Want to Vanish
## Elvis Costello

``` text

[Intro]
F Bbm6 F Bbm6
 
[Verse 1]
F         Bbm6        F             Am
I want to vanish this is my fondest wish
    Dm         Gm7                Bb         C        F
To go where I cannot be captured laid on a decorated dish
         Bbm6      F            Am
Even in splendour this curious fate
    Dm              Gm7          Bb        C   F
Is more than I care to surrender now it's too late
 
[Chorus]
F           Bdim      Bbm       F
Whether in wonder or indecent haste
Eb              Ab               Eb
You arrange the mirrors and the spoons
              Ab                 C
To snare the rare and precious jewels
           F            C    Caug
That were only made of paste
 
[Verse 2]
F              Bbm6    F             Am
If you should stumble upon my last remark
     Dm            Gm7            Bb        C               F
I'm crying in the wilderness I'm trying my best to make it dark
          Bbm6          F          Am
How can I tell you I'm rarer than most
     Dm           Gm7      Bb          C   F
I'm certain as a lost dog pondering a signpost
 
[Chorus]
F           Bdim      Bbm       F
Whether in wonder or indecent haste
Eb              Ab               Eb
You arrange the mirrors and the spoons
              Ab                 C
To snare the rare and precious jewels
           F            C    Caug
That were only made of paste
 
[Outro]
F         Bbm6        F             Am
I want to vanish this is my last request
      Dm            Gm7             Bb     C   F    Bbm6  F
I've given you the awful truth now give me my rest

```
