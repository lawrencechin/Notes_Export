# Tears at the Birthday Party
## Elvis Costello & Burt Bacharach

Author: [link](https://www.elviscostello.info/guitar/pfm.html)

```text
G       Fmaj7/G         G       Adim7

Am7                         Fmaj7/A
Think back now when we were young
Em7/A
There were always tears at the birthday party
Dm7          Dm6
You know how children can be
C6
So cruel
       G
That's how it starts, but
C       Bb/C
What if we never learn how to behave?
Fmaj7     Em                              Dm
I did something, and you never forgave me
F/G
I never thought that it could be like this
    C       Bb/C
But now I see
          F/C                  Fm6/C
I see you share your cake with him
G                   Fmaj7/G
Unwrapping presents that I should have sent
G
What can I do?
Adim7
Must I watch you?
Bbmaj7                                       Fmaj7
Close the door, dim the lights, blow out the candles
         Dm9 
So Happy Birthday again
G#dim7  Am7                     Fmaj7/A
        And it's the same every year
Em7/A
Seems that I remember it as something more, but
Dm9                       Dm6
You know how children can grow
Cmaj7
So strange
  G/B
I still adore you

What if we never learn from our mistakes?
But then, you'll never know how my heart aches

I never thought that it would be like this
But now I see
I see you share your cake with him
Unwrapping presents that I should have sent
What can I do?
Must I watch you?

Close the door, dim the lights, blow out the candles
So Happy Birthday again
Close the door, dim the lights, blow out the candles
So Happy Birthday again

One day I know he'll forget
To pay you the compliments you're after
You'll hang your sad, aching head
Behind a brittle smile or a shrill of laughter

What if we only get what we deserve?
Somehow I couldn't quite summon the nerve

Upon each anniversary
Then do you ever think of me?
Unwrapping presents that I should have sent
What can I do?
Must I watch you?

Close the door, dim the lights, blow out the candles
So Happy Birthday again
Close the door, dim the lights, blow out the candles
So Happy Birthday again
Close the door, dim the lights, blow out the candles
So Happy Birthday again
Close the door, dim the lights, blow out the candles
So Happy Birthday again

```
