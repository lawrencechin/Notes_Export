# Bon Iver - Skinny Love Tab

``` text 

-------------------------------------------------------------------------------
                                                       SKINNY LOVE - Bon Iver
-------------------------------------------------------------------------------
 
This is tabbed for standard tuning, only a capo is needed. Based on the live versions.
 
CAPO 5. [Though leave high E open/"un-capoed"] i.e.
 
e|-0-|
B|-5-|
G|-5-|
D|-5-|
A|-5-|
E|-5-|
 
 
[Intro]
 
Tab related to Capo. Strumming not indicated.
 
Be free when playing the little riffs, dont be afraid htting the open strings it adds to
sound.
 
e|-0-0-0-------------------0-0-0-----------------0-0-0--0-0----0-0-0-|
e|-0-0-0-------------------0-0-0--3-5-3----------0-0-0--1-0----0-0-0-|
C|-0-0-0--2-4-2---2-4-2----0-0-0-------4-2-4-2---0-0-0--0-0-/2-0-0-0-|
G|-2-5-0--------5-------5--2-5-0---------------5-2-5-0-------0-2-5-0-|
D|-2-2-5-------------------2-2-5-----------------2-2-5-------0-2-2-5-|
A|-0-3-3-------------------0-3-3-----------------0-3-3------/2-0-3-3-|
 
 
[Verse]
 
Chords used:
 
e|-0-|  |-0-|  |-0-|  |-0-|
e|-0-|  |-0-|  |-0-|  |-0-|
C|-0-|  |-0-|  |-0-|  |-2-|
G|-2-|  |-5-|  |-0-|  |-0-|
D|-2-|  |-2-|  |-5-|  |-0-|
A|-0-|  |-3-|  |-3-|  |-2-|
  Am      C     C/G    D6
 
Am             C                  C/G
Come on skinny love just last the year
Am             C                  C/G
Pour a little salt we were never here
        Am          C       C/G
My, my, my, my, my, my, my, my
               D6                        Am      C
Staring at the sink of blood and crushed veneer
 
 
e|--------------|                 |----------------|
e|--------------|  Bass cant      |---5------------|
C|--------------| Go low enough.  |--7-7-4--4------|
G|--2-----------| So alternative: |--------7-7-5-0-|
D|-5-5-2-2------|                 |----------------|
A|------5-5-3-0-|                 |----------------|
 
Am        C                C/G
I tell my love to wreck it all
Am              C                C/G
Cut out all the ropes and let me fall
        Am          C       C/G
My, my, my, my, my, my, my, my
             D6                  Am   C
Right in the moment this order's tall
 
[Chorus]
 
Chords used:
 
e|---|  |---|  |-0-|    |(3)|
e|---|  |---|  |-0-|    |-1-|
C|-0-|  |-0-|  |-0-|    |-0-|
G|-5-|  |-4-|  |-2-| OR |-2-| Down to preference.
D|-0-|  |-0-|  |-3-|    |-3-|
A|-3-|  |-2-|  |-0-|    |-0-|
   C.    Cmaj7  Fmaj7      F
 
  C.
I told you to be patient
  Cmaj7          F
I told you to be fine
  C.
I told you to be balanced
  Cmaj7          F
I told you to be kind
       C.
In the morning I'll be with you
            Cmaj7           F
But it will be a different "kind"
        C
I'll be holding all the tickets
              Cmaj7          F
And you'll be owning all the fines
 
 
 
************************************
 
| (n)  Ghost note
| p    Pull-off
| /    Slide up
 
************************************

```