# Tenth Avenue Freeze Out
Bruce Springsteen
Born To Run, 1975
Posted By Baba O'reilly

In This I'll Include The Trumpet part, Piano Part, and chords. There isn't much leads 
Yea.

``` text
Intro For Horns and Post-Chorus
--0---1--1----0---0--0------1--1-1-----------------------------------------------------|
--3---3--3----3---1--1------3--3-3-----------------------------------------------------|
--2---2--2----0---0--0------3--3-3-----------------------------------------------------|
--0---0--0----2---2--2------1--3-3-----------------------------------------------------|
--------------3---3--3------1--1-1-----------------------------------------------------|
---------------------------------------------------------------------------------------|


Piano Part For Most of Song
        F               Dm
E--1-1-1----1-1-1-1-1----------|
B--1-1-1----3-3-3-3-3----------|
G--2-2-2----2-2-2-2-2----------|
D--3-3-3----0-0-0-0-0----------|
A------------------------------|
E------------------------------|

Verse:
F                     Dm
Tear drops on the city
Bad Scooter searching for his groove
                          Bb                                 Gm
Seem like the whole world walking pretty
Bb                                 Gm             F       Dm
And you can't find the room to move
  C
Well everybody better move over, that's all
Bb
I'm running on the bad side
C
And I got my back to the wall

Chorus:
F       Dm       F         Dm              
        Dm       F     Dm
Tenth Avenue freeze-out, Tenth Avenue freeze-out

Verse:
F              Dm
I'm stranded in the jungle
Taking all the heat they was giving
Bb                Gm
The night is dark but the sidewalk's bright
Bb                     Gm                       F           Dm
And lined with the light of the living
C
From a tenement window a transistor blasts
                              Bb
Turn around the corner things got real quiet real fast
C
She hit me with a

Chorus:
F       Dm                                          F       Dm
Tenth Avenue freeze-out           Tenth Avenue freeze-out

Post Chorus:
D  Dm    C   C     Bb    Bb(9)
And I'm all alone, I'm all alone                                          (Horns Part)

(And kid you better get the picture)

D Dm C C   Bb     Bb(9)
And I'm on my own, I'm on my own
                                  F     Dm
And I can't go home

Verse:
F                Dm
When the change was made uptown
And the Big Man joined the band
Bb                             Gm
From the coastline to the city
Bb                Gm                                F        Dm
All the little pretties raise their hands
C
I'm gonna sit back right easy and laugh
Bb                                                                       C
When Scooter and the Big Man bust this city in half

Chorus:
F     Dm
With a Tenth Avenue freeze-out,
Tenth Avenue freeze-out
```
