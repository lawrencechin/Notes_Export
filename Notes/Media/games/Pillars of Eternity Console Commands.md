# Pillars of Eternity Console Commands

If you're using the IE Mod, you can just use these commands without any prerequisites. If you're not using the IE Mod, some of these commands will not be available at all, others will not be usable by default, you need to enable cheats by typing in iroll20s. This will activate cheats for this game, but will disable achievements. Achievements will be reactivated and cheats disabled when you reload the game (it's not necessary to exit the game entirely). But like I said, you can just use the mod and not worry about anything else, achievements will still work.

## How to add an NPC to party?

> (It's been reported that a creature that joined your party this way may be bugged after you reload a save in which you have it already joined your party or after you transition to another area, so exercise caution and only use this command to experiment)

You need an NPC's id first. The way you do it is simple, you just need to be on the same map with the npc.

Type `FindCharacter NPCname` - for example you want to add Medreth to your party, you type `FindCharacter Medreth`. You will see a few lines like `NPC_Medreth_Guard_Man`, etc, and one of them will be `NPC_Medreth`. This is the id that you need.

Now type `AddToParty NPC_Medreth`
And voila.

## Change zoom range

`SetZoomRange minValue maxValue` - will not change your current zoom, but will allow you to zoom really close and really far with the mouse wheel… try typing `SetZoomRange 0.1 200`

## Force an NPC who is not a party member to walk somewhere

`AIPathToPoint id1 id2 movementType`

- `id1` = it's the guy who you want moved. For example `NPC_Medreth_Guard_Dwarf`.
- `id2` = it's where you want the guy to go. You can give him anything you can find in `FindObject` or `FindCharacter`… You can give him `TurningWheel(Clone)` or `NPC_Villager_Female_03` and he'll go there. He won't go if he can't reach that place.
- `MovementType` = it's a number 2, 3 or 4... 2 = walk, 3 = run, 4 = sprint

> Example: `AIPathToPoint NPC_Medreth_Guard_Dwarf NPC_Villager_Female_03 2`

## Make NPC (who are not party members) fight someone

Same principle as with above, the command is `AIForceAttack id1 id2`
> Example: `AIForceAttack NPC_Medreth_Guard_Dwarf NPC_Medreth_Guard_Man`

## Jump to another area

`AreaTransition MapName PointLocation`
> Example: `AreaTransition AR_0002_Dyrford_Tavern_01 North1`

Or instead of using a pointlocation, just type its number, like 1.
By the way, instead of long names, you can just use the corresponding number of the area from the file.
Most of them don't seem to be in the game as of beta.

## Unlock an area on the world map or all areas

`WorldMapSetVisibility mapname visibilitytype` 

visibilitytypes are: 

- 0 = locked
- 1 = unlocked
- 2 = hidden
- 3 = developerOnly - unlocks a certain area
- UnlockAllMaps - unlocks them all

Of course it's useless to try unlocking areas that aren't supposed to be drawn on the map, like some caves.
If the name of the area indicates it to be outdoors, you can try it.> For example: `WorldMapSetVisibility AR_0301_Ondras_Gift_Exterior 1`

## Deal damage to anyone

Same deal as before: `DealDamage ID value`
> Example: `DealDamage NPC_Medreth_Guard_Man 200`

## Kill anyone

> Example: `Kill NPC_Medreth_Guard_Man`

## HealParty

Doesn't require any parameters.

## Unlock container (ignoring lock)

- `Open chest_01 true`
- `UnlockAll` - unlocks all containers in the area.

## Open container (ignoring lock) (This console commands requires the IE Mod to be installed.)

`OpenContainer chest_01`

## Jump to mouse cursor (This console commands requires the IE Mod to be installed.)

Usage: `Jump` (no parameters required)

Teleports selected characters under the mouse cursor, but only if the cursor is on the navmesh.

## Hire an NPC

`OpenCharacterCreationNewCompanion` cost endingLevel

The first parameter represents how much it will cost if the player does hire this companion. The second one stands for what level the companion will be.

> Example: `OpenCharacterCreationNewCompanion 0 8`

## Rest

Forces rest no matter where you are and no matter what your supplies are.

## Make someone hostile

> Example: `SetIsHostile CRE_Boar_Animal_Companion(Clone) true`
> Example 2: `SetIsHostile CRE_Boar_Animal_Companion(Clone) false`

## Various teleports

- `TeleportObejctToLocation guid guid`
- `TeleportPartyToLocation guid`
- `TeleportPlayerToLocation guid`

> Example: `TeleportPartyToLocation CRE_Boar_Animal_Companion(Clone)`

## Spawn an encounter

`EncounterSpawn id` -- the IDs can be found using the GameObject Browser (*console command:* `tt` ). You go up the heirarchy until you're at the top, then go into `2_Design_Area_Encounters` and there you will have your IDs. Sometimes the devs don't put the encounters into `2_Design_Area_Encounters` and they just hang at the top of the heirarchy along with other gameobjects, but can easily be identified, because they start with "`ENC_`".

## Spawn a creature  (This console commands requires the IE Mod to be installed.)

`BSC creaturename bool` (0 - for friendly, 1 - for hostile)

For now, the only names known are the file names starting with `cre_` in `assetbundles\prefabs\objectbundle`

> Example: to spawn a friendly "Obsidian Wurm" (it's an animal companion), you type: `BSC cre_wurm_obsidian_pet 0`. For a hostile druid cat: `BSC cre_druid_cat01 1`

## Advance Quest (This console commands requires the IE Mod to be installed.)

First, you need to get the internal names of your active quests using `ListActiveQuests`. This will yield a bunch of lines such as `data/quests/00_dyrford_ulterior_motives.quest` -- sometimes you'll have to guess which quest is which, for instance this name is assigned to Dyrford's quest known to players as "Cat and Mouse". Once you know the exact internal name, use `ForceAdvanceQuest name`

> Example: `ForceAdvanceQuest data/quests/00_dyrford_ulterior_motives.quest`

## Respec your party members (This console commands requires the IE Mod to be installed.)

This console command allows you to respec yourself or your party members (drops them to level 0 and allows you to relevel them up), or you can do the same thing, but also change their class. Changing the class is optional.

The console command is: `ChangeClass <name> <Class>`

As always, you must find the ingame name of your characters using the `FindCharacter name`.
So let's say you want to respec Aloth.
Type in `FindCharacter Aloth`. You'll get something like `Companion_Aloth(Clone)_1` in the console. This is the name you should use. Now do `ChangeClass Companion_Aloth(Clone)_1 Wizard` - your companion will fall down to level 0 and you'll be able to level him up differently from how he was. Or type `ChangeClass Companion_Aloth(Clone)_1 Fighter` - and he'll turn into a level 0 fighter instead.

> Warning: do not attempt to transition/quit/save game while your character is level 0. You need to level him up first. Do not attempt to open your grimoire, etc. Just immediately proceed to leveling up. It removes all talents and abilities, except racial ones and except Watcher's abilities, but that last part is not entirely true. I haven't progressed deep enough into the story to find all Watcher abilities, so it only makes an exception for `Crucible_of_the_Soul` and 3 more. I will include others as I discover them, but for now you will lose them if you're going to respec your main character. You can get them back through the `AddAbility` console command, so it's not a problem. Note that you will also lose your abilities gained through equipment, but you will regain them as soon as you reload your save.
In any case, exercise caution with this console command, consider it experimental.

> Update: Sagani will now keep her Itumaak even if you respec her into another class. If you respec Sagani into any other class, no further actions are necessary, you're all good. However, if you respec her into Ranger, you will be promoted to pick an animal companion by the game. It's just a formality, so pick any. When you're done, Sagani will still keep her Itumaak. However, another action is required now. You need to type `FixSagani id`. It's the same id you used in the `ChangeClass` command. So if you did `ChangeClass Companion_Sagani(Clone)_2 Ranger`, now you have to do `FixSagani Companion_Sagani(Clone)_2` Hivarias will lose his special shapeshift if you respec him, I've been told, but I'll fix it when I encounter him in game.

## Rename character

New a console command to allow you to rename creatures. This is important for when you respec into a Ranger, because your companion will be called something like `BearCompanion`. So again, find your companion's ingame name, then use `RenameCreature ingamename newname`.

> Example: `RenameCreature Animal_Companion_Wolf(Clone) Wolfie`

## Disable fog of war

`NoFog`

## GodMode

`God`

## Party becomes invisible for everyone

`Invisible`

## Nobody takes any damage

- `NoDamage 1` (to turn it on)
- `NoDamage 0` (to turn it off)

## Camera Speed

`CameraMoveDelta float`
Default value is 1. You can use values like 0.5 or 2 or 1.5, etc…

## Achievements (This console commands requires the IE Mod to be installed.)

- `CheckAchievements` - this checks whether you have accidentally disabled achievements for this playthrough or not. The only way you could've done it is by typing iroll20s, by the way.
- `ReenableAchievements` - for people who disabled them accidentally.

## Path of the Damned

`SwitchPOTD` - changes your game difficulty to Path of the Damned. Or if it's already at POTD, changes it back to Hard.

## Helmet Visibility

`HelmetVisibility true/false`
Affects only currently selected party members.

## Default Zoom

`SetDefaultZoom value`. Default is 1.

## Utility

- `ToggleSpellLimit` - makes you ignore spell limits.
- `FreeRecipesToggle` - allows crafting without having the ingredients.
- `CraftingDebug` - gives you all kinds of crafting supplies, a great amount of them.
- `AddItem itemname number` - example: `AddItem misc_troll_head 1` (the names of the items are the names of the files located in `PillarsOfEternity_Data\assetbundles\prefabs\objectbundle)`
- `GivePlayerMoney number`
- `AddExperience number`
- `AddExperienceToLevel number` -- gives you enough experience to reach level "number"
- `AddAbility charname abilityname` - Use `FindCharacter Name` to get the charname that you can use. `FindCharacter Aloth` will yield you something like `Companion_Aloth(Clone)_1`, so this is the name that you have to use. Abilities are found in `PillarsOfEternity_Data\assetbundles\prefabs\objectbundle`. So the end result should looks like `AddAbility Companion_Aloth(Clone)_1 necrotic_lance`.
- `AddTalent charname talentname` - (same here, but talents are prefaced with a "tln_"… do not try adding without it)
- `Skill charname skillname value` - example: `Skill player stealth 10` (player is what you type in when you mean your own character). Example2: `Skill Companion_Aloth(Clone)_1 stealth 10`
- `AttributeScore charname attribute value` - example: `AttributeScore player might 20` (this is base attribute score, so sometimes you'll end up with a slightly higher score because of racial bonuses)
- `RemoveTalent charname ability/talent`. Works both for talents and abilities. Works like `AddTalent` or `AddAbility`. Preface talents with "tln_".
- `IERemove SimpleName Ability/Talent` (requires IEMod) An easier to use variation of the previous command. You can use simple names and partial names for abilities/talents. Example1: `IERemove Eder Rapid_Recov or IERemove Kana The_Thunder`

## Stronghold Related

- `ActivateStronghold` - if you don't have a stronghold yet, you can activate it like this
- `StrongholdBuildAll`
- `StrongholdBuild type`
- `StrongholdDestroy type`
- `StrongholdForceAdventure type`
- `StrongholdForceBadVisitor`
- `StrongholdForceKidnapping`
- `StrongholdForcePayHirelings`
- `StrongholdForcePrisonBreak`
- `StrongholdForceSpawnIngredients`
- `StrongholdForceVisitor index` - a number ranging from 0 to I don't know
- `StrongholdForceAttack index` - same
- `AdjustSecurity int` - can be a positive or a negative number
- `AdjustPrestige int` - can be a positive or a negative number
- `AdvanceTimeByHour int`

## Misc

- `ManageParty`
- `Screenshake durationValue strengthValue` - example `Screenshake 1 1`
- `SoulMemoryCameraEnable true` (or false) - Gives you some kind of tunnel vision and everything becomes somewhat purple
- `UnlockBestiary` - unlocks all cyclopedia entries in `Journal -> Cyclopedia -> Bestiary`
- `DispositionAddPoints axis strength` - axis, strength
- `ReputationAddPoints Faction Axis Strength` - faction, axis, strength
- `SetGlobalValue name value`
- `SetWantsToTalk guid true` - force a party member to want to talk
- `StartQuest questName`

## Memorials

Memorial entries were exposed for you to rewrite them. If an enthusiast decides to edit them in order to make them more lore-friendly, all he has to do is the following: Type `ExtractMemorials` in the console. A new file will appear at `Managed/iemod/MemorialEntries.xml`. You can then edit it. Once you're done, you can then share it with other people and the IEMod will automatically use this file if it's placed in the `/iemod/` folder. If you want to remove certain memorial entries, you can just leave their title and/or text empty, the mod will automatically hide the entry in game.

## The ones I haven't figured out:

- `ChangeWaterLevel` (id, single, single) - probably didn't find the right id for water… didn't look very hard, either
- `AddPrisoner id` - It probably puts people in your prison, but only works on those that can be emprisoned in the first place?
