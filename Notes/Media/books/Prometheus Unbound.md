# Prometheus Unbound:

## A Lyrical Drama in Four Acts | By Percy Bysshe Shelley

### Edited by [Jack Lynch](http://andromeda.rutgers.edu/~jlynch/)

The text — not a critical text, just a reading text for in-class use — is drawn from the old Cambridge edition.

-----

Audisne haec amphiarae, sub terram abdite?

-----

## Preface

The Greek tragic writers, in selecting as their subject any portion of their national history or mythology, employed in their treatment of it a certain arbitrary discretion. They by no means conceived themselves bound to adhere to the common interpretation or to imitate in story as in title their rivals and predecessors. Such a system would have amounted to a resignation of those claims to preference over their competitors which incited the composition. The Agamemnonian story was exhibited on the Athenian theatre with as many variations as dramas.

I have presumed to employ a similar license. The Prometheus Unbound of Æschylus supposed the reconciliation of Jupiter with his victim as the price of the disclosure of the danger threatened to his empire by the consummation of his marriage with Thetis. Thetis, according to this view of the subject, was given in marriage to Peleus, and Prometheus, by the permission of Jupiter, delivered from his captivity by Hercules. Had I framed my story on this model, I should have done no more than have attempted to restore the lost drama of Æschylus; an ambition which, if my preference to this mode of treating the subject had incited me to cherish, the recollection of the high comparison such an attempt would challenge might well abate. But, in truth, I was averse from a catastrophe so feeble as that of reconciling the Champion with the Oppressor of mankind. The moral interest of the fable, which is so powerfully sustained by the sufferings and endurance of Prometheus, would be annihilated if we could conceive of him as unsaying his high language and quailing before his successful and perfidious adversary. The only imaginary being, resembling in any degree Prometheus, is Satan; and Prometheus is, in my judgment, a more poetical character than Satan, because, in addition to courage, and majesty, and firm and patient opposition to omnipotent force, he is susceptible of being described as exempt from the taints of ambition, envy, revenge, and a desire for personal aggrandizement, which, in the hero of Paradise Lost, interfere with the interest. The character of Satan engenders in the mind a pernicious casuistry which leads us to weigh his faults with his wrongs, and to excuse the former because the latter exceed all measure. In the minds of those who consider that magnificent fiction with a religious feeling it engenders something worse. But Prometheus is, as it were, the type of the highest perfection of moral and intellectual nature impelled by the purest and the truest motives to the best and noblest ends.

This Poem was chiefly written upon the mountainous ruins of the Baths of Caracalla, among the flowery glades and thickets of odoriferous blossoming trees, which are extended in ever winding labyrinths upon its immense platforms and dizzy arches suspended in the air. The bright blue sky of Rome, and the effect of the vigorous awakening spring in that divinest climate, and the new life with which it drenches the spirits even to intoxication, were the inspiration of this drama.

The imagery which I have employed will be found, in many instances, to have been drawn from the operations of the human mind, or from those external actions by which they are expressed. This is unusual in modern poetry, although Dante and Shakespeare are full of instances of the same kind; Dante indeed more than any other poet, and with greater success. But the Greek poets, as writers to whom no resource of awakening the sympathy of their contemporaries was unknown, were in the habitual use of this power; and it is the study of their works (since a higher merit would probably be denied me) to which I am willing that my readers should impute this singularity.

One word is due in candor to the degree in which the study of contemporary writings may have tinged my composition, for such has been a topic of censure with regard to poems far more popular, and indeed more deservedly popular, than mine. It is impossible that any one, who inhabits the same age with such writers as those who stand in the foremost ranks of our own, can conscientiously assure himself that his language and tone of thought may not have been modified by the study of the productions of those extraordinary intellects. It is true that, not the spirit of their genius, but the forms in which it has manifested itself, are due less to the peculiarities of their own minds than to the peculiarity of the moral and intellectual condition of the minds among which they have been produced. Thus a number of writers possess the form, whilst they want the spirit of those whom, it is alleged, they imitate; because the former is the endowment of the age in which they live, and the latter must be the uncommunicated lightning of their own mind.

The peculiar style of intense and comprehensive imagery which distinguishes the modern literature of England has not been, as a general power, the product of the imitation of any particular writer. The mass of capabilities remains at every period materially the same; the circumstances which awaken it to action perpetually change. If England were divided into forty republics, each equal in population and extent to Athens, there is no reason to suppose but that, under institutions not more perfect than those of Athens, each would produce philosophers and poets equal to those who (if we except Shakespeare) have never been surpassed. We owe the great writers of the golden age of our literature to that fervid awakening of the public mind which shook to dust the oldest and most oppressive form of the Christian religion. We owe Milton to the progress and development of the same spirit: the sacred Milton was, let it ever be remembered, a republican and a bold inquirer into morals and religion. The great writers of our own age are, we have reason to suppose, the companions and forerunners of some unimagined change in our social condition or the opinions which cement it. The cloud of mind is discharging its collected lightning, and the equilibrium between institutions and opinions is now restoring or is about to be restored.

As to imitation, poetry is a mimetic art. It creates, but it creates by combination and representation. Poetical abstractions are beautiful and new, not because the portions of which they are composed had no previous existence in the mind of man or in Nature, but because the whole produced by their combination has some intelligible and beautiful analogy with those sources of emotion and thought and with the contemporary condition of them. One great poet is a masterpiece of Nature which another not only ought to study but must study. He might as wisely and as easily determine that his mind should no longer be the mirror of all that is lovely in the visible universe as exclude from his contemplation the beautiful which exists in the writings of a great contemporary. The pretence of doing it would be a presumption in any but the greatest; the effect, even in him, would be strained, unnatural and ineffectual. A poet is the combined product of such internal powers as modify the nature of others, and of such external influences as excite and sustain these powers; he is not one, but both. Every man's mind is, in this respect, modified by all the objects of Nature and art; by every word and every suggestion which he ever admitted to act upon his consciousness; it is the mirror upon which all forms are reflected and in which they compose one form. Poets, not otherwise than philosophers, painters, sculptors and musicians, are, in one sense, the creators, and, in another, the creations, of their age. From this subjection the loftiest do not escape. There is a similarity between Homer and Hesiod, between Æschylus and Euripides, between Virgil and Horace, between Dante and Petrarch, between Shakespeare and Fletcher, between Dryden and Pope; each has a generic resemblance under which their specific distinctions are arranged. If this similarity be the result of imitation, I am willing to confess that I have imitated.

Let this opportunity be conceded to me of acknowledging that I have what a Scotch philosopher characteristically terms a 'passion for reforming the world:' what passion incited him to write and publish his book he omits to explain. For my part I had rather be damned with Plato and Lord Bacon than go to Heaven with Paley and Malthus. But it is a mistake to suppose that I dedicate my poetical compositions solely to the direct enforcement of reform, or that I consider them in any degree as containing a reasoned system on the theory of human life. Didactic poetry is my abhorrence; nothing can be equally well expressed in prose that is not tedious and supererogatory in verse. My purpose has hitherto been simply to familiarize the highly refined imagination of the more select classes of poetical readers with beautiful idealisms of moral excellence; aware that, until the mind can love, and admire, and trust, and hope, and endure, reasoned principles of moral conduct are seeds cast upon the highway of life which the unconscious passenger tramples into dust, although they would bear the harvest of his happiness. Should I live to accomplish what I purpose, that is, produce a systematical history of what appear to me to be the genuine elements of human society, let not the advocates of injustice and superstition flatter themselves that I should take Æschylus rather than Plato as my model.

The having spoken of myself with unaffected freedom will need little apology with the candid; and let the uncandid consider that they injure me less than their own hearts and minds by misrepresentation. Whatever talents a person may possess to amuse and instruct others, be they ever so inconsiderable, he is yet bound to exert them: if his attempt be ineffectual, let the punishment of an unaccomplished purpose have been sufficient; let none trouble themselves to heap the dust of oblivion upon his efforts; the pile they raise will betray his grave which might otherwise have been unknown.

-----

### Dramatis Personæ

*Prometheus.*   Asia }  
*Demogorgon.*   Panthea } Oceanides.  
*Jupiter.*      Ione }  
*The Earth.*    The Phantasm of Jupiter.  
*Ocean.*        The Spirit of the Earth.  
*Apollo.*       The Spirit of the Moon.  
*Mercury.*      Spirits of the Hours.  
*Hercules.*     Spirits. Echoes. Fauns. Furies.

-----

## ACT I

*Scene.* — A Ravine of Icy Rocks in the Indian Caucasus. *Prometheus* is discovered bound to the Precipice. *Panthea* and *Ione* are seated at his feet. Time, night. During the Scene, morning slowly breaks.


**Prometheus.**

Monarch of Gods and Dæmons, and all Spirits  
But One, who throng those bright and rolling worlds  
Which Thou and I alone of living things  
Behold with sleepless eyes! regard this Earth  
Made multitudinous with thy slaves, whom thou *[1.5]*  
Requitest for knee-worship, prayer, and praise,  
And toil, and hecatombs of broken hearts,  
With fear and self-contempt and barren hope.  
Whilst me, who am thy foe, eyeless in hate,  
Hast thou made reign and triumph, to thy scorn, *[1.10]*  
O'er mine own misery and thy vain revenge.  
Three thousand years of sleep-unsheltered hours,  
And moments aye divided by keen pangs  
Till they seemed years, torture and solitude,  
Scorn and despair, — these are mine empire: — *[1.15]*  
More glorious far than that which thou surveyest  
From thine unenvied throne, O Mighty God!  
Almighty, had I deigned to share the shame  
Of thine ill tyranny, and hung not here  
Nailed to this wall of eagle-baffling mountain, *[1.20]*  
Black, wintry, dead, unmeasured; without herb,  
Insect, or beast, or shape or sound of life.  
Ah me! alas, pain, pain ever, for ever!  
  
No change, no pause, no hope! Yet I endure.  
I ask the Earth, have not the mountains felt? *[1.25]*  
I ask yon Heaven, the all-beholding Sun,  
Has it not seen? The Sea, in storm or calm,  
Heaven's ever-changing Shadow, spread below,  
Have its deaf waves not heard my agony?  
Ah me! alas, pain, pain ever, for ever! *[1.30]*  
  
The crawling glaciers pierce me with the spears  
Of their moon-freezing crystals, the bright chains  
Eat with their burning cold into my bones.  
Heaven's wingèd hound, polluting from thy lips  
His beak in poison not his own, tears up *[1.35]*  
My heart; and shapeless sights come wandering by,  
The ghastly people of the realm of dream,  
Mocking me: and the Earthquake-fiends are charged  
To wrench the rivets from my quivering wounds  
When the rocks split and close again behind: *[1.40]*  
While from their loud abysses howling throng  
The genii of the storm, urging the rage  
Of whirlwind, and afflict me with keen hail.  
And yet to me welcome is day and night,  
Whether one breaks the hoar frost of the morn, *[1.45]*  
Or starry, dim, and slow, the other climbs  
The leaden-coloured east; for then they lead  
The wingless, crawling hours, one among whom  
— As some dark Priest hales the reluctant victim —  
Shall drag thee, cruel King, to kiss the blood *[1.50]*  
From these pale feet, which then might trample thee  
If they disdained not such a prostrate slave.  
Disdain! Ah no! I pity thee. What ruin  
Will hunt thee undefended through wide Heaven!  
How will thy soul, cloven to its depth with terror, *[1.55]*  
Gape like a hell within! I speak in grief,  
Not exultation, for I hate no more,  
As then ere misery made me wise. The curse  
Once breathed on thee I would recall. Ye Mountains,  
Whose many-voicèd Echoes, through the mist *[1.60]*  
Of cataracts, flung the thunder of that spell!  
Ye icy Springs, stagnant with wrinkling frost,  
Which vibrated to hear me, and then crept  
Shuddering through India! Thou serenest Air,  
Through which the Sun walks burning without beams! *[1.65]*  
And ye swift Whirlwinds, who on poisèd wings  
Hung mute and moveless o'er yon hushed abyss,  
As thunder, louder than your own, made rock  
The orbèd world! If then my words had power,  
Though I am changed so that aught evil wish *[1.70]*  
Is dead within; although no memory be  
Of what is hate, let them not lose it now!  
What was that curse? for ye all heard me speak.

**First Voice <em>(from the Mountains).</em>**
  
Thrice three hundred thousand years  
   O'er the Earthquake's couch we stood: *[1.75]*  
Oft, as men convulsed with fears,  
   We trembled in our multitude.

**Second Voice <em>(from the Springs).</em>**
  
Thunderbolts had parched our water,  
   We had been stained with bitter blood,  
And had run mute, 'mid shrieks of slaughter, *[1.80]*  
   Thro' a city and a solitude.

**Third Voice <em>(from the Air).</em>**
  
I had clothed, since Earth uprose,  
   Its wastes in colours not their own,  
And oft had my serene repose  
   Been cloven by many a rending groan. *[1.85]*

**Fourth Voice <em>(from the Whirlwinds).</em>**
  
We had soared beneath these mountains  
   Unresting ages; nor had thunder,  
Nor yon volcano's flaming fountains,  
   Nor any power above or under  
   Ever made us mute with wonder. *[1.90]*

**First Voice.**
  
But never bowed our snowy crest  
As at the voice of thine unrest.

**Second Voice.**
  
Never such a sound before  
To the Indian waves we bore.  
A pilot asleep on the howling sea *[1.95]*  
Leaped up from the deck in agony,  
And heard, and cried, &quot;Ah, woe is me!&quot;  
And died as mad as the wild waves be.

**Third Voice.**
  
By such dread words from Earth to Heaven  
My still realm was never riven: *[1.100]*  
When its wound was closed, there stood  
Darkness o'er the day like blood.

**Fourth Voice.**
  
And we shrank back: for dreams of ruin  
To frozen caves our flight pursuing  
Made us keep silence — thus — and thus — *[1.105]*  
Though silence is as hell to us.

**The Earth.**
  
The tongueless Caverns of the craggy hills  
Cried, &quot;Misery!&quot; then; the hollow Heaven replied,  
&quot;Misery!&quot; And the Ocean's purple waves,  
Climbing the land, howled to the lashing winds, *[1.110]*  
And the pale nations heard it, &quot;Misery!&quot;

**Prometheus.**
  
I heard a sound of voices: not the voice  
Which I gave forth. Mother, thy sons and thou  
Scorn him, without whose all-enduring will  
Beneath the fierce omnipotence of Jove, *[1.115]*  
Both they and thou had vanished, like thin mist  
Unrolled on the morning wind. Know ye not me,  
The Titan? He who made his agony  
The barrier to your else all-conquering foe?  
Oh, rock-embosomed lawns, and snow-fed streams, *[1.120]*  
Now seen athwart frore vapours, deep below,  
Through whose o'ershadowing woods I wandered once  
With Asia, drinking life from her loved eyes;  
Why scorns the spirit which informs ye, now  
To commune with me? me alone, who checked, *[1.125]*  
As one who checks a fiend-drawn charioteer,  
The falsehood and the force of him who reigns  
Supreme, and with the groans of pining slaves  
Fills your dim glens and liquid wildernesses:  
Why answer ye not, still? Brethren!

**The Earth.**
  
                They dare not. *[1.130]*

**Prometheus.**
  
Who dares? for I would hear that curse again.  
Ha, what an awful whisper rises up!  
'Tis scarce like sound: it tingles through the frame  
As lightning tingles, hovering ere it strike.  
Speak, Spirit! from thine inorganic voice *[1.135]*  
I only know that thou art moving near  
And love. How cursed I him?

**The Earth.**
  
                How canst thou hear  
Who knowest not the language of the dead?

**Prometheus.**
  
Thou art a living spirit; speak as they.

**The Earth.**
  
I dare not speak like life, lest Heaven's fell King *[1.140]*  
Should hear, and link me to some wheel of pain  
More torturing than the one whereon I roll.  
Subtle thou art and good, and though the Gods  
Hear not this voice, yet thou art more than God,  
Being wise and kind: earnestly hearken now. *[1.145]*

**Prometheus.**
  
Obscurely through my brain, like shadows dim,  
Sweep awful thoughts, rapid and thick. I feel  
Faint, like one mingled in entwining love;  
Yet 'tis not pleasure.

**The Earth.**
  
                No, thou canst not hear:  
Thou art immortal, and this tongue is known *[1.150]*  
Only to those who die.

**Prometheus.**
  
                And what art thou,  
O, melancholy Voice?

**The Earth.**
  
                I am the Earth,  
Thy mother; she within whose stony veins,  
To the last fibre of the loftiest tree  
Whose thin leaves trembled in the frozen air, *[1.155]*  
Joy ran, as blood within a living frame,  
When thou didst from her bosom, like a cloud  
Of glory, arise, a spirit of keen joy!  
And at thy voice her pining sons uplifted  
Their prostrate brows from the polluting dust, *[1.160]*  
And our almighty Tyrant with fierce dread  
Grew pale, until his thunder chained thee here.  
Then, see those million worlds which burn and roll  
Around us: their inhabitants beheld  
My spherèd light wane in wide Heaven; the sea *[1.165]*  
Was lifted by strange tempest, and new fire  
From earthquake-rifted mountains of bright snow  
Shook its portentous hair beneath Heaven's frown;  
Lightning and Inundation vexed the plains;  
Blue thistles bloomed in cities; foodless toads *[1.170]*  
Within voluptuous chambers panting crawled:  
When Plague had fallen on man, and beast, and worm,  
And Famine; and black blight on herb and tree;  
And in the corn, and vines, and meadow-grass,  
Teemed ineradicable poisonous weeds *[1.175]*  
Draining their growth, for my wan breast was dry  
With grief; and the thin air, my breath, was stained  
With the contagion of a mother's hate  
Breathed on her child's destroyer; ay, I heard  
Thy curse, the which, if thou rememberest not, *[1.180]*  
Yet my innumerable seas and streams,  
Mountains, and caves, and winds, and yon wide air,  
And the inarticulate people of the dead,  
Preserve, a treasured spell. We meditate  
In secret joy and hope those dreadful words, *[1.185]*  
But dare not speak them.

**Prometheus.**
  
                Venerable mother!  
All else who live and suffer take from thee  
Some comfort; flowers, and fruits, and happy sounds,  
And love, though fleeting; these may not be mine.  
But mine own words, I pray, deny me not. *[1.190]*

**The Earth.**
  
They shall be told. Ere Babylon was dust,  
The Magus Zoroaster, my dead child,  
Met his own image walking in the garden.  
That apparition, sole of men, he saw.  
For know there are two worlds of life and death: *[1.195]*  
One that which thou beholdest; but the other  
Is underneath the grave, where do inhabit  
The shadows of all forms that think and live  
Till death unite them and they part no more;  
Dreams and the light imaginings of men, *[1.200]*  
And all that faith creates or love desires,  
Terrible, strange, sublime and beauteous shapes.  
There thou art, and dost hang, a writhing shade,  
'Mid whirlwind-peopled mountains; all the gods  
Are there, and all the powers of nameless worlds, *[1.205]*  
Vast, sceptred phantoms; heroes, men, and beasts;  
And Demogorgon, a tremendous gloom;  
And he, the supreme Tyrant, on his throne  
Of burning gold. Son, one of these shall utter  
The curse which all remember. Call at will *[1.210]*  
Thine own ghost, or the ghost of Jupiter,  
Hades or Typhon, or what mightier Gods  
From all-prolific Evil, since thy ruin  
Have sprung, and trampled on my prostrate sons.  
Ask, and they must reply: so the revenge *[1.215]*  
Of the Supreme may sweep through vacant shades,  
As rainy wind through the abandoned gate  
Of a fallen palace.

**Prometheus.**
  
                Mother, let not aught  
Of that which may be evil, pass again  
My lips, or those of aught resembling me. *[1.220]*  
Phantasm of Jupiter, arise, appear!

**Ione.**
  
   My wings are folded o'er mine ears:  
      My wings are crossèd o'er mine eyes:  
   Yet through their silver shade appears,  
      And through their lulling plumes arise, *[1.225]*  
   A Shape, a throng of sounds;  
      May it be no ill to thee  
   O thou of many wounds!  
Near whom, for our sweet sister's sake,  
Ever thus we watch and wake. *[1.230]*

**Panthea.**
  
   The sound is of whirlwind underground,  
      Earthquake, and fire, and mountains cloven;  
   The shape is awful like the sound,  
      Clothed in dark purple, star-inwoven.  
   A sceptre of pale gold *[1.235]*  
      To stay steps proud, o'er the slow cloud  
   His veinèd hand doth hold.  
Cruel he looks, but calm and strong,  
Like one who does, not suffers wrong.

**Phantasm of Jupiter.**
  
Why have the secret powers of this strange world *[1.240]*  
Driven me, a frail and empty phantom, hither  
On direst storms? What unaccustomed sounds  
Are hovering on my lips, unlike the voice  
With which our pallid race hold ghastly talk  
In darkness? And, proud sufferer, who art thou? *[1.245]*

**Prometheus.**
  
Tremendous Image, as thou art must be  
He whom thou shadowest forth. I am his foe,  
The Titan. Speak the words which I would hear,  
Although no thought inform thine empty voice.

**The Earth.**
  
Listen! And though your echoes must be mute, *[1.250]*  
Gray mountains, and old woods, and haunted springs,  
Prophetic caves, and isle-surrounding streams,  
Rejoice to hear what yet ye cannot speak.

**Phantasm.**
  
A spirit seizes me and speaks within:  
It tears me as fire tears a thunder-cloud. *[1.255]*

**Panthea.**
  
See, how he lifts his mighty looks, the Heaven  
Darkens above.

**Ione.**
  
        He speaks! O shelter me!

**Prometheus.**
  
I see the curse on gestures proud and cold,  
And looks of firm defiance, and calm hate,  
And such despair as mocks itself with smiles, *[1.260]*  
Written as on a scroll: yet speak: Oh, speak!

**Phantasm.**
  
   Fiend, I defy thee! with a calm, fixed mind,  
      All that thou canst inflict I bid thee do;  
   Foul Tyrant both of Gods and Human-kind,  
      One only being shalt thou not subdue. *[1.265]*  
   Rain then thy plagues upon me here,  
   Ghastly disease, and frenzying fear;  
   And let alternate frost and fire  
   Eat into me, and be thine ire  
Lightning, and cutting hail, and legioned forms *[1.270]*  
Of furies, driving by upon the wounding storms.  
  
   Ay, do thy worst. Thou art omnipotent.  
      O'er all things but thyself I gave thee power,  
   And my own will. Be thy swift mischiefs sent  
      To blast mankind, from yon ethereal tower. *[1.275]*  
   Let thy malignant spirit move  
   In darkness over those I love:  
   On me and mine I imprecate  
   The utmost torture of thy hate;  
And thus devote to sleepless agony, *[1.280]*  
This undeclining head while thou must reign on high.  
  
   But thou, who art the God and Lord: O, thou,  
      Who fillest with thy soul this world of woe,  
   To whom all things of Earth and Heaven do bow  
      In fear and worship: all-prevailing foe! *[1.285]*  
   I curse thee! let a sufferer's curse  
   Clasp thee, his torturer, like remorse;  
   Till thine Infinity shall be  
   A robe of envenomed agony;  
And thine Omnipotence a crown of pain, *[1.290]*  
To cling like burning gold round thy dissolving brain.  
  
   Heap on thy soul, by virtue of this Curse,  
      Ill deeds, then be thou damned, beholding good;  
   Both infinite as is the universe,  
      And thou, and thy self-torturing solitude. *[1.295]*  
   An awful image of calm power  
   Though now thou sittest, let the hour  
   Come, when thou must appear to be  
   That which thou art internally;  
And after many a false and fruitless crime *[1.300]*  
Scorn track thy lagging fall through boundless space and time.

**Prometheus.**
  
Were these my words, O Parent?

**The Earth.**
  
                They were thine.

**Prometheus.**
  
It doth repent me: words are quick and vain;  
Grief for awhile is blind, and so was mine.  
I wish no living thing to suffer pain. *[1.305]*

**The Earth.**
  
   Misery, Oh misery to me,  
   That Jove at length should vanquish thee.  
   Wail, howl aloud, Land and Sea,  
   The Earth's rent heart shall answer ye.  
Howl, Spirits of the living and the dead, *[1.310]*  
Your refuge, your defence lies fallen and vanquishèd.

**First Echo.**
  
Lies fallen and vanquishèd!

**Second Echo.**
  
        Fallen and vanquishèd!

**Ione.**
  
Fear not: 'tis but some passing spasm,  
   The Titan is unvanquished still. *[1.315]*  
But see, where through the azure chasm  
   Of yon forked and snowy hill  
Trampling the slant winds on high  
   With golden-sandalled feet, that glow  
Under plumes of purple dye, *[1.320]*  
Like rose-ensanguined ivory,  
   A Shape comes now,  
Stretching on high from his right hand  
A serpent-cinctured wand.

**Panthea.**
  
'Tis Jove's world-wandering herald, Mercury. *[1.325]*

**Ione.**
  
And who are those with hydra tresses  
   And iron wings that climb the wind,  
Whom the frowning God represses  
   Like vapours steaming up behind,  
Clanging loud, an endless crowd — *[1.330]*

**Panthea.**
  
   These are Jove's tempest-walking hounds,  
Whom he gluts with groans and blood,  
When charioted on sulphurous cloud  
   He bursts Heaven's bounds.

**Ione.**
  
Are they now led, from the thin dead *[1.335]*  
On new pangs to be fed?

**Panthea.**
  
The Titan looks as ever, firm, not proud.

**First Fury.**
  
Ha! I scent life!

**Second Fury.**
  
        Let me but look into his eyes!

**Third Fury.**
  
The hope of torturing him smells like a heap  
Of corpses, to a death-bird after battle. *[1.340]*

**First Fury.**
  
Darest thou delay, O Herald! take cheer, Hounds  
Of Hell: what if the Son of Maia soon  
Should make us food and sport — who can please long  
The Omnipotent?

**Mercury.**
  
        Back to your towers of iron,  
And gnash, beside the streams of fire and wail, *[1.345]*  
Your foodless teeth. Geryon, arise! and Gorgon,  
Chimæra, and thou Sphinx, subtlest of fiends  
Who ministered to Thebes Heaven's poisoned wine,  
Unnatural love, and more unnatural hate:  
These shall perform your task.

**First Fury.**
  
                Oh, mercy! mercy! *[1.350]*  
We die with our desire: drive us not back!

**Mercury.**
  
Crouch then in silence.         Awful Sufferer!  
To thee unwilling, most unwillingly  
I come, by the great Father's will driven down,  
To execute a doom of new revenge. *[1.355]*  
Alas! I pity thee, and hate myself  
That I can do no more: aye from thy sight  
Returning, for a season, Heaven seems Hell,  
So thy worn form pursues me night and day,  
Smiling reproach. Wise art thou, firm and good, *[1.360]*  
But vainly wouldst stand forth alone in strife  
Against the Omnipotent; as yon clear lamps  
That measure and divide the weary years  
From which there is no refuge, long have taught  
And long must teach. Even now thy Torturer arms *[1.365]*  
With the strange might of unimagined pains  
The powers who scheme slow agonies in Hell,  
And my commission is to lead them here,  
Or what more subtle, foul, or savage fiends  
People the abyss, and leave them to their task. *[1.370]*  
Be it not so! there is a secret known  
To thee, and to none else of living things,  
Which may transfer the sceptre of wide Heaven,  
The fear of which perplexes the Supreme:  
Clothe it in words, and bid it clasp his throne *[1.375]*  
In intercession; bend thy soul in prayer,  
And like a suppliant in some gorgeous fane,  
Let the will kneel within thy haughty heart:  
For benefits and meek submission tame  
The fiercest and the mightiest.

**Prometheus.**
  
                Evil minds *[1.380]*  
Change good to their own nature. I gave all  
He has; and in return he chains me here  
Years, ages, night and day: whether the Sun  
Split my parched skin, or in the moony night  
The crystal-wingèd snow cling round my hair: *[1.385]*  
Whilst my belovèd race is trampled down  
By his thought-executing ministers.  
Such is the tyrant's recompense: 'tis just:  
He who is evil can receive no good;  
And for a world bestowed, or a friend lost, *[1.390]*  
He can feel hate, fear, shame; not gratitude:  
He but requites me for his own misdeed.  
Kindness to such is keen reproach, which breaks  
With bitter stings the light sleep of Revenge.  
Submission, thou dost know I cannot try: *[1.395]*  
For what submission but that fatal word,  
The death-seal of mankind's captivity,  
Like the Sicilian's hair-suspended sword,  
Which trembles o'er his crown, would he accept,  
Or could I yield? Which yet I will not yield. *[1.400]*  
Let others flatter Crime, where it sits throned  
In brief Omnipotence: secure are they:  
For Justice, when triumphant, will weep down  
Pity, not punishment, on her own wrongs,  
Too much avenged by those who err. I wait, *[1.405]*  
Enduring thus, the retributive hour  
Which since we spake is even nearer now.  
But hark, the hell-hounds clamour: fear delay:  
Behold! Heaven lowers under thy Father's frown.

**Mercury.**
  
Oh, that we might be spared: I to inflict *[1.410]*  
And thou to suffer! Once more answer me:  
Thou knowest not the period of Jove's power?

**Prometheus.**
  
I know but this, that it must come.

**Mercury.**
  
                Alas!  
Thou canst not count thy years to come of pain?

**Prometheus.**
  
They last while Jove must reign: nor more, nor less *[1.415]*  
Do I desire or fear.

**Mercury.**
  
                Yet pause, and plunge  
Into Eternity, where recorded time,  
Even all that we imagine, age on age,  
Seems but a point, and the reluctant mind  
Flags wearily in its unending flight, *[1.420]*  
Till it sink, dizzy, blind, lost, shelterless;  
Perchance it has not numbered the slow years  
Which thou must spend in torture, unreprieved?

**Prometheus.**
  
Perchance no thought can count them, yet they pass.

**Mercury.**
  
If thou might'st dwell among the Gods the while *[1.425]*  
Lapped in voluptuous joy?

**Prometheus.**
  
                I would not quit  
This bleak ravine, these unrepentant pains.

**Mercury.**
  
Alas! I wonder at, yet pity thee.

**Prometheus.**
  
Pity the self-despising slaves of Heaven,  
Not me, within whose mind sits peace serene, *[1.430]*  
As light in the sun, throned: how vain is talk!  
Call up the fiends.

**Ione.**
  
                O, sister, look! White fire  
Has cloven to the roots yon huge snow-loaded cedar;  
How fearfully God's thunder howls behind!

**Mercury.**
  
I must obey his words and thine: alas! *[1.435]*  
Most heavily remorse hangs at my heart!

**Panthea.**
  
See where the child of Heaven, with wingèd feet,  
Runs down the slanted sunlight of the dawn.

**Ione.**
  
Dear sister, close thy plumes over thine eyes  
Lest thou behold and die: they come: they come *[1.440]*  
Blackening the birth of day with countless wings,  
And hollow underneath, like death.

**First Fury.**
  
                Prometheus!

**Second Fury.**
  
Immortal Titan!

**Third Fury.**
  
                Champion of Heaven's slaves!

**Prometheus.**
  
He whom some dreadful voice invokes is here,  
Prometheus, the chained Titan. Horrible forms, *[1.445]*  
What and who are ye? Never yet there came  
Phantasms so foul through monster-teeming Hell  
From the all-miscreative brain of Jove;  
Whilst I behold such execrable shapes,  
Methinks I grow like what I contemplate, *[1.450]*  
And laugh and stare in loathsome sympathy.

**First Fury.**
  
We are the ministers of pain, and fear,  
And disappointment, and mistrust, and hate,  
And clinging crime; and as lean dogs pursue  
Through wood and lake some struck and sobbing fawn, *[1.455]*  
We track all things that weep, and bleed, and live,  
When the great King betrays them to our will.

**Prometheus.**
  
Oh! many fearful natures in one name,  
I know ye; and these lakes and echoes know  
The darkness and the clangour of your wings. *[1.460]*  
But why more hideous than your loathèd selves  
Gather ye up in legions from the deep?

**Second Fury.**
  
We knew not that: Sisters, rejoice, rejoice!

**Prometheus.**
  
Can aught exult in its deformity?

**Second Fury.**
  
The beauty of delight makes lovers glad, *[1.465]*  
Gazing on one another: so are we.  
As from the rose which the pale priestess kneels  
To gather for her festal crown of flowers  
The aëreal crimson falls, flushing her cheek,  
So from our victim's destined agony *[1.470]*  
The shade which is our form invests us round,  
Else we are shapeless as our mother Night.

**Prometheus.**
  
I laugh your power, and his who sent you here,  
To lowest scorn. Pour forth the cup of pain.

**First Fury.**
  
Thou thinkest we will rend thee bone from bone, *[1.475]*  
And nerve from nerve, working like fire within?

**Prometheus.**
  
Pain is my element, as hate is thine;  
Ye rend me now: I care not.

**Second Fury.**
  
                Dost imagine  
We will but laugh into thy lidless eyes?

**Prometheus.**
  
I weigh not what ye do, but what ye suffer, *[1.480]*  
Being evil. Cruel was the power which called  
You, or aught else so wretched, into light.

**Third Fury.**
  
Thou think'st we will live through thee, one by one,  
Like animal life, and though we can obscure not  
The soul which burns within, that we will dwell *[1.485]*  
Beside it, like a vain loud multitude  
Vexing the self-content of wisest men:  
That we will be dread thought beneath thy brain,  
And foul desire round thine astonished heart,  
And blood within thy labyrinthine veins *[1.490]*  
Crawling like agony?

**Prometheus.**
  
                Why, ye are thus now;  
Yet am I king over myself, and rule  
The torturing and conflicting throngs within,  
As Jove rules you when Hell grows mutinous.

**Chorus of Furies.**
  
From the ends of the earth, from the ends of the earth, *[1.495]*  
Where the night has its grave and the morning its birth,  
            Come, come, come!  
Oh, ye who shake hills with the scream of your mirth,  
When cities sink howling in ruin; and ye  
Who with wingless footsteps trample the sea, *[1.500]*  
And close upon Shipwreck and Famine's track,  
Sit chattering with joy on the foodless wreck;  
            Come, come, come!  
   Leave the bed, low, cold, and red,  
   Strewed beneath a nation dead; *[1.505]*  
   Leave the hatred, as in ashes  
      Fire is left for future burning:  
   It will burst in bloodier flashes  
      When ye stir it, soon returning:  
   Leave the self-contempt implanted *[1.510]*  
   In young spirits, sense-enchanted,  
      Misery's yet unkindled fuel:  
      Leave Hell's secrets half unchanted  
         To the maniac dreamer; cruel  
      More than ye can be with hate *[1.515]*  
            Is he with fear.  
            Come, come, come!  
We are steaming up from Hell's wide gate  
   And we burthen the blast of the atmosphere,  
   But vainly we toil till ye come here. *[1.520]*

**Ione.**
  
Sister, I hear the thunder of new wings.

**Panthea.**
  
These solid mountains quiver with the sound  
Even as the tremulous air: their shadows make  
The space within my plumes more black than night.

**First Fury.**
  
Your call was as a wingèd car *[1.525]*  
Driven on whirlwinds fast and far;  
It rapped us from red gulfs of war.

**Second Fury.**
  
From wide cities, famine-wasted;

**Third Fury.**
  
Groans half heard, and blood untasted;

**Fourth Fury.**
  
Kingly conclaves stern and cold, *[1.530]*  
Where blood with gold is bought and sold;

**Fifth Fury.**
  
From the furnace, white and hot,  
In which —

**A Fury.**
  
                Speak not: whisper not:  
I know all that ye would tell,  
But to speak might break the spell *[1.535]*  
Which must bend the Invincible,  
   The stern of thought;  
He yet defies the deepest power of Hell.

**A Fury.**
  
Tear the veil!

**Another Fury.**
  
        It is torn.

**Chorus.**
  
                The pale stars of the morn  
Shine on a misery, dire to be borne. *[1.540]*  
Dost thou faint, mighty Titan? We laugh thee to scorn.  
Dost thou boast the clear knowledge thou waken'dst for man?  
Then was kindled within him a thirst which outran  
Those perishing waters; a thirst of fierce fever,  
Hope, love, doubt, desire, which consume him for ever. *[1.545]*  
      One came forth of gentle worth  
      Smiling on the sanguine earth;  
      His words outlived him, like swift poison  
         Withering up truth, peace, and pity.  
      Look! where round the wide horizon *[1.550]*  
         Many a million-peopled city  
      Vomits smoke in the bright air.  
      Hark that outcry of despair!  
      'Tis his mild and gentle ghost  
         Wailing for the faith he kindled: *[1.555]*  
      Look again, the flames almost  
         To a glow-worm's lamp have dwindled:  
The survivors round the embers  
   Gather in dread.  
            Joy, joy, joy! *[1.560]*  
Past ages crowd on thee, but each one remembers,  
And the future is dark, and the present is spread  
Like a pillow of thorns for thy slumberless head.

**Semichorus I.**
  
Drops of bloody agony flow  
From his white and quivering brow. *[1.565]*  
Grant a little respite now:  
See a disenchanted nation  
Springs like day from desolation;  
To Truth its state is dedicate,  
And Freedom leads it forth, her mate; *[1.570]*  
A legioned band of linkèd brothers  
Whom Love calls children —

**Semichorus II.**
  
                'Tis another's:  
   See how kindred murder kin:  
   'Tis the vintage-time for death and sin:  
   Blood, like new wine, bubbles within: *[1.575]*  
      Till Despair smothers  
The struggling world, which slaves and tyrants win.
<p><em>[All the Furies vanish, except one.</em></p>

**Ione.**
  
Hark, sister! what a low yet dreadful groan  
Quite unsuppressed is tearing up the heart  
Of the good Titan, as storms tear the deep, *[1.580]*  
And beasts hear the sea moan in inland caves.  
Darest thou observe how the fiends torture him?

**Panthea.**
  
Alas! I looked forth twice, but will no more.

**Ione.**
  
What didst thou see?

**Panthea.**
  
                A woful sight: a youth  
With patient looks nailed to a crucifix. *[1.585]*

**Ione.**
  
What next?

**Panthea.**
  
        The heaven around, the earth below  
Was peopled with thick shapes of human death,  
All horrible, and wrought by human hands,  
And some appeared the work of human hearts,  
For men were slowly killed by frowns and smiles: *[1.590]*  
And other sights too foul to speak and live  
Were wandering by. Let us not tempt worse fear  
By looking forth: those groans are grief enough.

**Fury.**
  
Behold an emblem: those who do endure  
Deep wrongs for man, and scorn, and chains, but heap *[1.595]*  
Thousandfold torment on themselves and him.

**Prometheus.**
  
Remit the anguish of that lighted stare;  
Close those wan lips; let that thorn-wounded brow  
Stream not with blood; it mingles with thy tears!  
Fix, fix those tortured orbs in peace and death, *[1.600]*  
So thy sick throes shake not that crucifix,  
So those pale fingers play not with thy gore.  
O, horrible! Thy name I will not speak,  
It hath become a curse. I see, I see,  
The wise, the mild, the lofty, and the just, *[1.605]*  
Whom thy slaves hate for being like to thee,  
Some hunted by foul lies from their heart's home,  
An early-chosen, late-lamented home;  
As hooded ounces cling to the driven hind;  
Some linked to corpses in unwholesome cells: *[1.610]*  
Some — Hear I not the multitude laugh loud? —  
Impaled in lingering fire: and mighty realms  
Float by my feet, like sea-uprooted isles,  
Whose sons are kneaded down in common blood  
By the red light of their own burning homes. *[1.615]*

**Fury.**
  
Blood thou canst see, and fire; and canst hear groans;  
Worse things, unheard, unseen, remain behind.

**Prometheus.**
  
Worse?

**Fury.**
  
        In each human heart terror survives  
The ravin it has gorged: the loftiest fear  
All that they would disdain to think were true: *[1.620]*  
Hypocrisy and custom make their minds  
The fanes of many a worship, now outworn.  
They dare not devise good for man's estate,  
And yet they know not that they do not dare.  
The good want power, but to weep barren tears. *[1.625]*  
The powerful goodness want: worse need for them.  
The wise want love; and those who love want wisdom;  
And all best things are thus confused to ill.  
Many are strong and rich, and would be just,  
But live among their suffering fellow-men *[1.630]*  
As if none felt: they know not what they do.

**Prometheus.**
  
Thy words are like a cloud of wingèd snakes;  
And yet I pity those they torture not.

**Fury.**
  
Thou pitiest them? I speak no more!
<p><em>[Vanishes.</em></p>

**Prometheus.**
  
                Ah woe!  
Ah woe! Alas! pain, pain ever, for ever! *[1.635]*  
I close my tearless eyes, but see more clear  
Thy works within my woe-illumèd mind,  
Thou subtle tyrant! Peace is in the grave.  
The grave hides all things beautiful and good:  
I am a God and cannot find it there, *[1.640]*  
Nor would I seek it: for, though dread revenge,  
This is defeat, fierce king, not victory.  
The sights with which thou torturest gird my soul  
With new endurance, till the hour arrives  
When they shall be no types of things which are. *[1.645]*

**Panthea.**
  
Alas! what sawest thou more?

**Prometheus.**
  
        There are two woes:  
To speak, and to behold; thou spare me one.  
Names are there, Nature's sacred watchwords, they  
Were borne aloft in bright emblazonry;  
The nations thronged around, and cried aloud, *[1.650]*  
As with one voice, Truth, liberty, and love!  
Suddenly fierce confusion fell from heaven  
Among them: there was strife, deceit, and fear:  
Tyrants rushed in, and did divide the spoil.  
This was the shadow of the truth I saw. *[1.655]*

**The Earth.**
  
I felt thy torture, son; with such mixed joy  
As pain and virtue give. To cheer thy state  
I bid ascend those subtle and fair spirits,  
Whose homes are the dim caves of human thought,  
And who inhabit, as birds wing the wind, *[1.660]*  
Its world-surrounding aether: they behold  
Beyond that twilight realm, as in a glass,  
The future: may they speak comfort to thee!

**Panthea.**
  
Look, sister, where a troop of spirits gather,  
Like flocks of clouds in spring's delightful weather, *[1.665]*  
Thronging in the blue air!

**Ione.**
  
        And see! more come,  
Like fountain-vapours when the winds are dumb,  
That climb up the ravine in scattered lines.  
And, hark! is it the music of the pines?  
Is it the lake? Is it the waterfall? *[1.670]*

**Panthea.**
  
'Tis something sadder, sweeter far than all.

**Chorus of Spirits.**
  
From unremembered ages we  
Gentle guides and guardians be  
Of heaven-oppressed mortality;  
And we breathe, and sicken not, *[1.675]*  
The atmosphere of human thought:  
Be it dim, and dank, and gray,  
Like a storm-extinguished day,  
Travelled o'er by dying gleams;  
   Be it bright as all between *[1.680]*  
Cloudless skies and windless streams,  
   Silent, liquid, and serene;  
As the birds within the wind,  
   As the fish within the wave,  
As the thoughts of man's own mind *[1.685]*  
   Float through all above the grave;  
We make there our liquid lair,  
Voyaging cloudlike and unpent  
Through the boundless element:  
Thence we bear the prophecy *[1.690]*  
Which begins and ends in thee!

**Ione.**
  
More yet come, one by one: the air around them  
Looks radiant as the air around a star.

**First Spirit.**
  
On a battle-trumpet's blast  
I fled hither, fast, fast, fast, *[1.695]*  
'Mid the darkness upward cast.  
From the dust of creeds outworn,  
From the tyrant's banner torn,  
Gathering 'round me, onward borne,  
There was mingled many a cry — *[1.700]*  
Freedom! Hope! Death! Victory!  
Till they faded through the sky;  
And one sound, above, around,  
One sound beneath, around, above,  
Was moving; 'twas the soul of Love; *[1.705]*  
'Twas the hope, the prophecy,  
Which begins and ends in thee.

**Second Spirit.**
  
A rainbow's arch stood on the sea,  
Which rocked beneath, immovably;  
And the triumphant storm did flee, *[1.710]*  
Like a conqueror, swift and proud,  
Between, with many a captive cloud,  
A shapeless, dark and rapid crowd,  
Each by lightning riven in half:  
I heard the thunder hoarsely laugh: *[1.715]*  
Mighty fleets were strewn like chaff  
And spread beneath a hell of death  
O'er the white waters. I alit  
On a great ship lightning-split,  
And speeded hither on the sigh *[1.720]*  
Of one who gave an enemy  
His plank, then plunged aside to die.

**Third Spirit.**
  
I sate beside a sage's bed,  
And the lamp was burning red  
Near the book where he had fed, *[1.725]*  
When a Dream with plumes of flame,  
To his pillow hovering came,  
And I knew it was the same  
Which had kindled long ago  
Pity, eloquence, and woe; *[1.730]*  
And the world awhile below  
Wore the shade, its lustre made.  
It has borne me here as fleet  
As Desire's lightning feet:  
I must ride it back ere morrow, *[1.735]*  
Or the sage will wake in sorrow.

**Fourth Spirit.**
  
On a poet's lips I slept  
Dreaming like a love-adept  
In the sound his breathing kept;  
Nor seeks nor finds he mortal blisses, *[1.740]*  
But feeds on the aëreal kisses  
Of shapes that haunt thought's wildernesses.  
He will watch from dawn to gloom  
The lake-reflected sun illume  
The yellow bees in the ivy-bloom, *[1.745]*  
Nor heed nor see, what things they be;  
But from these create he can  
Forms more real than living man,  
Nurslings of immortality!  
One of these awakened me, *[1.750]*  
And I sped to succour thee.

**Ione.**
  
Behold'st thou not two shapes from the east and west  
Come, as two doves to one belovèd nest,  
Twin nurslings of the all-sustaining air  
On swift still wings glide down the atmosphere? *[1.755]*  
And, hark! their sweet, sad voices! 'tis despair  
Mingled with love and then dissolved in sound.

**Panthea.**
  
Canst thou speak, sister? all my words are drowned.

**Ione.**
  
Their beauty gives me voice. See how they float  
On their sustaining wings of skiey grain, *[1.760]*  
Orange and azure deepening into gold:  
Their soft smiles light the air like a star's fire.

**Chorus of Spirits.**
  
Hast thou beheld the form of Love?

**Fifth Spirit.**
  
                As over wide dominions  
   I sped, like some swift cloud that wings the wide air's wildernesses,  
That planet-crested shape swept by on lightning-braided pinions, *[1.765]*  
   Scattering the liquid joy of life from his ambrosial tresses:  
His footsteps paved the world with light; but as I passed 'twas fading,  
   And hollow Ruin yawned behind: great sages bound in madness,  
And headless patriots, and pale youths who perished, unupbraiding,  
   Gleamed in the night. I wandered o'er, till thou, O King of sadness, *[1.770]*  
   Turned by thy smile the worst I saw to recollected gladness.

**Sixth Spirit.**
  
Ah, sister! Desolation is a delicate thing:  
   It walks not on the earth, it floats not on the air,  
But treads with lulling footstep, and fans with silent wing  
   The tender hopes which in their hearts the best and gentlest bear; *[1.775]*  
Who, soothed to false repose by the fanning plumes above  
   And the music-stirring motion of its soft and busy feet,  
Dream visions of aëreal joy, and call the monster, Love,  
   And wake, and find the shadow Pain, as he whom now we greet.

**Chorus.**
  
Though Ruin now Love's shadow be, *[1.780]*  
Following him, destroyingly,  
   On Death's white and wingèd steed,  
Which the fleetest cannot flee,  
   Trampling down both flower and weed,  
Man and beast, and foul and fair, *[1.785]*  
Like a tempest through the air;  
Thou shalt quell this horseman grim,  
Woundless though in heart or limb.

**Prometheus.**
  
Spirits! how know ye this shall be?

**Chorus.**
  
   In the atmosphere we breathe, *[1.790]*  
As buds grow red when the snow-storms flee,  
   From Spring gathering up beneath,  
Whose mild winds shake the elder brake,  
And the wandering herdsmen know  
That the white-thorn soon will blow: *[1.795]*  
   Wisdom, Justice, Love, and Peace,  
   When they struggle to increase,  
      Are to us as soft winds be  
      To shepherd boys, the prophecy  
      Which begins and ends in thee. *[1.800]*

**Ione.**
  
Where are the Spirits fled?

**Panthea.**
  
                Only a sense  
Remains of them, like the omnipotence  
Of music, when the inspired voice and lute  
Languish, ere yet the responses are mute,  
Which through the deep and labyrinthine soul, *[1.805]*  
Like echoes through long caverns, wind and roll.

**Prometheus.**
  
How fair these airborn shapes! and yet I feel  
Most vain all hope but love; and thou art far,  
Asia! who, when my being overflowed,  
Wert like a golden chalice to bright wine *[1.810]*  
Which else had sunk into the thirsty dust.  
All things are still: alas! how heavily  
This quiet morning weighs upon my heart;  
Though I should dream I could even sleep with grief  
If slumber were denied not. I would fain *[1.815]*  
Be what it is my destiny to be,  
The saviour and the strength of suffering man,  
Or sink into the original gulf of things:  
There is no agony, and no solace left;  
Earth can console, Heaven can torment no more. *[1.820]*

**Panthea.**
  
Hast thou forgotten one who watches thee  
The cold dark night, and never sleeps but when  
The shadow of thy spirit falls on her?

**Prometheus.**
  
I said all hope was vain but love: thou lovest.

**Panthea.**
  
Deeply in truth; but the eastern star looks white, *[1.825]*  
And Asia waits in that far Indian vale,  
The scene of her sad exile; rugged once  
And desolate and frozen, like this ravine;  
But now invested with fair flowers and herbs,  
And haunted by sweet airs and sounds, which flow *[1.830]*  
Among the woods and waters, from the aether  
Of her transforming presence, which would fade  
If it were mingled not with thine. Farewell!
### END OF THE FIRST ACT.

-----

## ACT II

### Scene I.

— Morning. A lovely Vale in the Indian Caucasus. *Asia* alone.


**Asia.**
  
From all the blasts of heaven thou hast descended:  
Yes, like a spirit, like a thought, which makes  
Unwonted tears throng to the horny eyes,  
And beatings haunt the desolated heart,  
Which should have learnt repose: thou hast descended *[2.1.5]*  
Cradled in tempests; thou dost wake, O Spring!  
O child of many winds! As suddenly  
Thou comest as the memory of a dream,  
Which now is sad because it hath been sweet;  
Like genius, or like joy which riseth up *[2.1.10]*  
As from the earth, clothing with golden clouds  
The desert of our life.  
This is the season, this the day, the hour;  
At sunrise thou shouldst come, sweet sister mine,  
Too long desired, too long delaying, come! *[2.1.15]*  
How like death-worms the wingless moments crawl!  
The point of one white star is quivering still  
Deep in the orange light of widening morn  
Beyond the purple mountains. through a chasm  
Of wind-divided mist the darker lake *[2.1.20]*  
Reflects it: now it wanes: it gleams again  
As the waves fade, and as the burning threads  
Of woven cloud unravel in pale air:  
'Tis lost! and through yon peaks of cloud-like snow  
The roseate sunlight quivers: hear I not *[2.1.25]*  
The Æolian music of her sea-green plumes  
Winnowing the crimson dawn?
<p><em>[Panthea enters.</em></p>
                I feel, I see  
Those eyes which burn through smiles that fade in tears,  
Like stars half quenched in mists of silver dew.  
Belovèd and most beautiful, who wearest *[2.1.30]*  
The shadow of that soul by which I live,  
How late thou art! the spherèd sun had climbed  
The sea; my heart was sick with hope, before  
The printless air felt thy belated plumes.

**Panthea.**
  
Pardon, great Sister! but my wings were faint *[2.1.35]*  
With the delight of a remembered dream,  
As are the noontide plumes of summer winds  
Satiate with sweet flowers. I was wont to sleep  
Peacefully, and awake refreshed and calm  
Before the sacred Titan's fall, and thy *[2.1.40]*  
Unhappy love, had made, through use and pity,  
Both love and woe familiar to my heart  
As they had grown to thine: erewhile I slept  
Under the glaucous caverns of old Ocean  
Within dim bowers of green and purple moss, *[2.1.45]*  
Our young Ione's soft and milky arms  
Locked then, as now, behind my dark, moist hair,  
While my shut eyes and cheek were pressed within  
The folded depth of her life-breathing bosom:  
But not as now, since I am made the wind *[2.1.50]*  
Which fails beneath the music that I bear  
Of thy most wordless converse; since dissolved  
Into the sense with which love talks, my rest  
Was troubled and yet sweet; my waking hours  
Too full of care and pain.

**Asia.**
  
                Lift up thine eyes, *[2.1.55]*  
And let me read thy dream.

**Panthea.**
  
                As I have said  
With our sea-sister at his feet I slept.  
The mountain mists, condensing at our voice  
Under the moon, had spread their snowy flakes,  
From the keen ice shielding our linkèd sleep. *[2.1.60]*  
Then two dreams came. One, I remember not.  
But in the other his pale wound-worn limbs  
Fell from Prometheus, and the azure night  
Grew radiant with the glory of that form  
Which lives unchanged within, and his voice fell *[2.1.65]*  
Like music which makes giddy the dim brain,  
Faint with intoxication of keen joy:  
&quot;Sister of her whose footsteps pave the world  
With loveliness — more fair than aught but her,  
Whose shadow thou art — lift thine eyes on me.&quot; *[2.1.70]*  
I lifted them: the overpowering light  
Of that immortal shape was shadowed o'er  
By love; which, from his soft and flowing limbs,  
And passion-parted lips, and keen, faint eyes,  
Steamed forth like vaporous fire; an atmosphere *[2.1.75]*  
Which wrapped me in its all-dissolving power,  
As the warm aether of the morning sun  
Wraps ere it drinks some cloud of wandering dew.  
I saw not, heard not, moved not, only felt  
His presence flow and mingle through my blood *[2.1.80]*  
Till it became his life, and his grew mine,  
And I was thus absorbed, until it passed,  
And like the vapours when the sun sinks down,  
Gathering again in drops upon the pines,  
And tremulous as they, in the deep night *[2.1.85]*  
My being was condensed; and as the rays  
Of thought were slowly gathered, I could hear  
His voice, whose accents lingered ere they died  
Like footsteps of weak melody: thy name  
Among the many sounds alone I heard *[2.1.90]*  
Of what might be articulate; though still  
I listened through the night when sound was none.  
Ione wakened then, and said to me:  
&quot;Canst thou divine what troubles me to-night?  
I always knew what I desired before, *[2.1.95]*  
Nor ever found delight to wish in vain.  
But now I cannot tell thee what I seek;  
I know not; something sweet, since it is sweet  
Even to desire; it is thy sport, false sister;  
Thou hast discovered some enchantment old, *[2.1.100]*  
Whose spells have stolen my spirit as I slept  
And mingled it with thine: for when just now  
We kissed, I felt within thy parted lips  
The sweet air that sustained me, and the warmth  
Of the life-blood, for loss of which I faint, *[2.1.105]*  
Quivered between our intertwining arms.&quot;  
I answered not, for the Eastern star grew pale,  
But fled to thee.

**Asia.**
  
        Thou speakest, but thy words  
Are as the air: I feel them not: Oh, lift  
Thine eyes, that I may read his written soul! *[2.1.110]*

**Panthea.**
  
I lift them though they droop beneath the load  
Of that they would express: what canst thou see  
But thine own fairest shadow imaged there?

**Asia.**
  
Thine eyes are like the deep, blue, boundless heaven  
Contracted to two circles underneath *[2.1.115]*  
Their long, fine lashes; dark, far, measureless,  
Orb within orb, and line through line inwoven.

**Panthea.**
  
Why lookest thou as if a spirit passed?

**Asia.**
  
There is a change: beyond their inmost depth  
I see a shade, a shape: 'tis He, arrayed *[2.1.120]*  
In the soft light of his own smiles, which spread  
Like radiance from the cloud-surrounded moon.  
Prometheus, it is thine! depart not yet!  
Say not those smiles that we shall meet again  
Within that bright pavilion which their beams *[2.1.125]*  
Shall build o'er the waste world? The dream is told.  
What shape is that between us? Its rude hair  
Roughens the wind that lifts it, its regard  
Is wild and quick, yet 'tis a thing of air,  
For through its gray robe gleams the golden dew *[2.1.130]*  
Whose stars the noon has quenched not.

**Dream.**
  
                Follow! Follow!

**Panthea.**
  
It is mine other dream.

**Asia.**
  
                It disappears.

**Panthea.**
  
It passes now into my mind. Methought  
As we sate here, the flower-infolding buds  
Burst on yon lightning-blasted almond-tree, *[2.1.135]*  
When swift from the white Scythian wilderness  
A wind swept forth wrinkling the Earth with frost:  
I looked, and all the blossoms were blown down;  
But on each leaf was stamped, as the blue bells  
Of Hyacinth tell Apollo's written grief, *[2.1.140]*  
O, follow, follow!

**Asia.**
  
        As you speak, your words  
Fill, pause by pause, my own forgotten sleep  
With shapes. Methought among these lawns together  
We wandered, underneath the young gray dawn,  
And multitudes of dense white fleecy clouds *[2.1.145]*  
Were wandering in thick flocks along the mountains  
Shepherded by the slow, unwilling wind;  
And the white dew on the new-bladed grass,  
Just piercing the dark earth, hung silently;  
And there was more which I remember not: *[2.1.150]*  
But on the shadows of the morning clouds,  
Athwart the purple mountain slope, was written  
Follow, O, follow! as they vanished by;  
And on each herb, from which Heaven's dew had fallen,  
The like was stamped, as with a withering fire; *[2.1.155]*  
A wind arose among the pines; it shook  
The clinging music from their boughs, and then  
Low, sweet, faint sounds, like the farewell of ghosts,  
Were heard: O, follow, follow, follow me!  
And then I said: &quot;Panthea, look on me.&quot; *[2.1.160]*  
But in the depth of those belovèd eyes  
Still I saw, follow, follow!

**Echo.**
  
                Follow, follow!

**Panthea.**
  
The crags, this clear spring morning, mock our voices  
As they were spirit-tongued.

**Asia.**
  
                It is some being  
Around the crags. What fine clear sounds! O, list! *[2.1.165]*

**Echoes <em>(unseen).</em>**
  
Echoes we: listen!  
   We cannot stay:  
As dew-stars glisten  
   Then fade away —  
      Child of Ocean! *[2.1.170]*

**Asia.**
  
Hark! Spirits speak. The liquid responses  
Of their aëreal tongues yet sound.

**Panthea.**
  
                I hear.

**Echoes.**
  
O, follow, follow,  
   As our voice recedeth  
Through the caverns hollow, *[2.1.175]*  
   Where the forest spreadeth;
<p><em>(More distant.)</em></p>
   O, follow, follow!  
   Through the caverns hollow,  
As the song floats thou pursue,  
Where the wild bee never flew, *[2.1.180]*  
Through the noontide darkness deep,  
By the odour-breathing sleep  
Of faint night flowers, and the waves  
At the fountain-lighted caves,  
While our music, wild and sweet, *[2.1.185]*  
Mocks thy gently falling feet,  
      Child of Ocean!

**Asia.**
  
Shall we pursue the sound? It grows more faint And distant.

**Panthea.**
  
List! the strain floats nearer now.

**Echoes.**
  
In the world unknown *[2.1.190]*  
   Sleeps a voice unspoken;  
By thy step alone  
   Can its rest be broken;  
      Child of Ocean!

**Asia.**
  
How the notes sink upon the ebbing wind! *[2.1.195]*

**Echoes.**
  
   O, follow, follow!  
   Through the caverns hollow,  
As the song floats thou pursue,  
By the woodland noontide dew;  
By the forest, lakes, and fountains, *[2.1.200]*  
Through the many-folded mountains;  
To the rents, and gulfs, and chasms,  
Where the Earth reposed from spasms,  
On the day when He and thou  
Parted, to commingle now; *[2.1.205]*  
      Child of Ocean!

**Asia.**
  
Come, sweet Panthea, link thy hand in mine,  
And follow, ere the voices fade away.

### Scene II.

— A Forest, intermingled with Rocks and Caverns. *Panthea* pass into it. Two young Fauns are sitting on a Rock listening.


**Semichorus I. of Spirits.**
  
  The path through which that lovely twain  
     Have passed, by cedar, pine, and yew,  
     And each dark tree that ever grew,  
     Is curtained out from Heaven's wide blue;  
  Nor sun, nor moon, nor wind, nor rain, *[2.2.5]*  
        Can pierce its interwoven bowers,  
     Nor aught, save where some cloud of dew,  
  Drifted along the earth-creeping breeze,  
  Between the trunks of the hoar trees,  
      Hangs each a pearl in the pale flowers *[2.2.10]*  
   Of the green laurel, blown anew;  
And bends, and then fades silently,  
One frail and fair anemone:  
Or when some star of many a one  
That climbs and wanders through steep night, *[2.2.15]*  
Has found the cleft through which alone  
Beams fall from high those depths upon  
Ere it is borne away, away,  
By the swift Heavens that cannot stay,  
It scatters drops of golden light, *[2.2.20]*  
Like lines of rain that ne'er unite:  
And the gloom divine is all around,  
And underneath is the mossy ground.

**Semichorus II.**
  
There the voluptuous nightingales,  
   Are awake through all the broad noonday. *[2.2.25]*  
When one with bliss or sadness fails,  
      And through the windless ivy-boughs,  
   Sick with sweet love, droops dying away  
On its mate's music-panting bosom;  
Another from the swinging blossom, *[2.2.30]*  
      Watching to catch the languid close  
   Of the last strain, then lifts on high  
   The wings of the weak melody,  
'Till some new strain of feeling bear  
   The song, and all the woods are mute; *[2.2.35]*  
When there is heard through the dim air  
The rush of wings, and rising there  
   Like many a lake-surrounded flute,  
Sounds overflow the listener's brain  
So sweet, that joy is almost pain. *[2.2.40]*

**Semichorus I.**
  
There those enchanted eddies play  
   Of echoes, music-tongued, which draw,  
   By Demogorgon's mighty law,  
   With melting rapture, or sweet awe,  
All spirits on that secret way; *[2.2.45]*  
   As inland boats are driven to Ocean  
Down streams made strong with mountain-thaw:  
      And first there comes a gentle sound  
      To those in talk or slumber bound,  
   And wakes the destined soft emotion, — *[2.2.50]*  
Attracts, impels them; those who saw  
   Say from the breathing earth behind  
   There steams a plume-uplifting wind  
Which drives them on their path, while they  
   Believe their own swift wings and feet *[2.2.55]*  
The sweet desires within obey:  
And so they float upon their way,  
Until, still sweet, but loud and strong,  
The storm of sound is driven along,  
   Sucked up and hurrying: as they fleet *[2.2.60]*  
   Behind, its gathering billows meet  
And to the fatal mountain bear  
Like clouds amid the yielding air.

**First Faun.**
  
Canst thou imagine where those spirits live  
Which make such delicate music in the woods? *[2.2.65]*  
We haunt within the least frequented caves  
And closest coverts, and we know these wilds,  
Yet never meet them, though we hear them oft:  
Where may they hide themselves?

**Second Faun.**
  
                'Tis hard to tell:  
I have heard those more skilled in spirits say, *[2.2.70]*  
The bubbles, which the enchantment of the sun  
Sucks from the pale faint water-flowers that pave  
The oozy bottom of clear lakes and pools,  
Are the pavilions where such dwell and float  
Under the green and golden atmosphere *[2.2.75]*  
Which noontide kindles through the woven leaves;  
And when these burst, and the thin fiery air,  
The which they breathed within those lucent domes,  
Ascends to flow like meteors through the night,  
They ride on them, and rein their headlong speed, *[2.2.80]*  
And bow their burning crests, and glide in fire  
Under the waters of the earth again.

**First Faun.**
  
If such live thus, have others other lives,  
Under pink blossoms or within the bells  
Of meadow flowers, or folded violets deep, *[2.2.85]*  
Or on their dying odours, when they die,  
Or in the sunlight of the spherèd dew?

**Second Faun.**
  
Ay, many more which we may well divine.  
But, should we stay to speak, noontide would come,  
And thwart Silenus find his goats undrawn, *[2.2.90]*  
And grudge to sing those wise and lovely songs  
Of Fate, and Chance, and God, and Chaos old,  
And Love, and the chained Titan's woful doom,  
And how he shall be loosed, and make the earth  
One brotherhood: delightful strains which cheer *[2.2.95]*  
Our solitary twilights, and which charm  
To silence the unenvying nightingales.

### Scene III.

— A Pinnacle of Rock among Mountains.
*Asia* and *Panthea*.


**Panthea.**
  
Hither the sound has borne us — to the realm  
Of Demogorgon, and the mighty portal,  
Like a volcano's meteor-breathing chasm,  
Whence the oracular vapour is hurled up  
Which lonely men drink wandering in their youth, *[2.3.5]*  
And call truth, virtue, love, genius, or joy,  
That maddening wine of life, whose dregs they drain  
To deep intoxication; and uplift,  
Like Mænads who cry loud, Evoe! Evoe!  
The voice which is contagion to the world. *[2.3.10]*

**Asia.**
  
Fit throne for such a Power! Magnificent!  
How glorious art thou, Earth! And if thou be  
The shadow of some spirit lovelier still,  
Though evil stain its work, and it should be  
Like its creation, weak yet beautiful, *[2.3.15]*  
I could fall down and worship that and thee.  
Even now my heart adoreth: Wonderful!  
Look, sister, ere the vapour dim thy brain:  
Beneath is a wide plain of billowy mist,  
As a lake, paving in the morning sky, *[2.3.20]*  
With azure waves which burst in silver light,  
Some Indian vale. Behold it, rolling on  
Under the curdling winds, and islanding  
The peak whereon we stand, midway, around,  
Encinctured by the dark and blooming forests, *[2.3.25]*  
Dim twilight-lawns, and stream-illumèd caves,  
And wind-enchanted shapes of wandering mist;  
And far on high the keen sky-cleaving mountains  
From icy spires of sun-like radiance fling  
The dawn, as lifted Ocean's dazzling spray, *[2.3.30]*  
From some Atlantic islet scattered up,  
Spangles the wind with lamp-like water-drops.  
The vale is girdled with their walls, a howl  
Of cataracts from their thaw-cloven ravines,  
Satiates the listening wind, continuous, vast, *[2.3.35]*  
Awful as silence. Hark! the rushing snow!  
The sun-awakened avalanche! whose mass,  
Thrice sifted by the storm, had gathered there  
Flake after flake, in heaven-defying minds  
As thought by thought is piled, till some great truth *[2.3.40]*  
Is loosened, and the nations echo round,  
Shaken to their roots, as do the mountains now.

**Panthea.**
  
Look how the gusty sea of mist is breaking  
In crimson foam, even at our feet! it rises  
As Ocean at the enchantment of the moon *[2.3.45]*  
Round foodless men wrecked on some oozy isle.

**Asia.**
  
The fragments of the cloud are scattered up;  
The wind that lifts them disentwines my hair;  
Its billows now sweep o'er mine eyes; my brain  
Grows dizzy; see'st thou shapes within the mist? *[2.3.50]*

**Panthea.**
  
A countenance with beckoning smiles: there burns  
An azure fire within its golden locks!  
Another and another: hark! they speak!

**Song of Spirits.**
  
   To the deep, to the deep,  
      Down, down! *[2.3.55]*  
   Through the shade of sleep,  
   Through the cloudy strife  
   Of Death and of Life;  
   Through the veil and the bar  
   Of things which seem and are *[2.3.60]*  
Even to the steps of the remotest throne,  
      Down, down!  
  
   While the sound whirls around,  
      Down, down!  
   As the fawn draws the hound, *[2.3.65]*  
   As the lightning the vapour,  
   As a weak moth the taper;  
   Death, despair; love, sorrow;  
   Time both; to-day, to-morrow;  
As steel obeys the spirit of the stone, *[2.3.70]*  
      Down, down!  
  
   Through the gray, void abysm,  
      Down, down!  
   Where the air is no prism,  
   And the moon and stars are not, *[2.3.75]*  
   And the cavern-crags wear not  
   The radiance of Heaven,  
   Nor the gloom to Earth given,  
Where there is One pervading, One alone,  
      Down, down! *[2.3.80]*  
   In the depth of the deep,  
      Down, down!  
   Like veiled lightning asleep,  
   Like the spark nursed in embers,  
   The last look Love remembers, *[2.3.85]*  
   Like a diamond, which shines  
   On the dark wealth of mines,  
A spell is treasured but for thee alone.  
      Down, down!  
  
   We have bound thee, we guide thee; *[2.3.90]*  
      Down, down!  
   With the bright form beside thee;  
   Resist not the weakness,  
   Such strength is in meekness  
   That the Eternal, the Immortal, *[2.3.95]*  
   Most unloose through life's portal  
The snake-like Doom coiled underneath his throne  
      By that alone.

### Scene IV.

— The Cave of Demogorgon.
*Asia* and *Panthea*.


**Panthea.**
  
What vèiled form sits on that ebon throne?

**Asia.**
  
The veil has fallen.

**Panthea.**
  
        I see a mighty darkness  
Filling the seat of power, and rays of gloom  
Dart round, as light from the meridian sun.  
— Ungazed upon and shapeless; neither limb, *[2.4.5]*  
Nor form, nor outline; yet we feel it is  
A living Spirit.

**Demogorgon.**
  
        Ask what thou wouldst know.

**Asia.**
  
What canst thou tell?

**Demogorgon.**
  
        All things thou dar'st demand.

**Asia.**
  
Who made the living world?

**Demogorgon.**
  
        God.

**Asia.**
  
                Who made all  
That it contains? thought, passion, reason, will, Imagination? *[2.4.10]*

**Demogorgon.**
  
God: Almighty God.

**Asia.**
  
Who made that sense which, when the winds of Spring  
In rarest visitation, or the voice  
Of one belovèd heard in youth alone,  
Fills the faint eyes with falling tears which dim *[2.4.15]*  
The radiant looks of unbewailing flowers,  
And leaves this peopled earth a solitude  
When it returns no more?

**Demogorgon.**
  
                  Merciful God.

**Asia.**
  
And who made terror, madness, crime, remorse,  
Which from the links of the great chain of things, *[2.4.20]*  
To every thought within the mind of man  
Sway and drag heavily, and each one reels  
Under the load towards the pit of death;  
Abandoned hope, and love that turns to hate;  
And self-contempt, bitterer to drink than blood; *[2.4.25]*  
Pain, whose unheeded and familiar speech  
Is howling, and keen shrieks, day after day;  
And Hell, or the sharp fear of Hell?

**Demogorgon.**
  
                He reigns.

**Asia.**
  
Utter his name: a world pining in pain  
Asks but his name: curses shall drag him down. *[2.4.30]*

**Demogorgon.**
  
He reigns.

**Asia.**
  
        I feel, I know it: who?

**Demogorgon.**
  
                He reigns.

**Asia.**
  
Who reigns? There was the Heaven and Earth at first,  
And Light and Love; then Saturn, from whose throne  
Time fell, an envious shadow: such the state  
Of the earth's primal spirits beneath his sway, *[2.4.35]*  
As the calm joy of flowers and living leaves  
Before the wind or sun has withered them  
And semivital worms; but he refused  
The birthright of their being, knowledge, power,  
The skill which wields the elements, the thought *[2.4.40]*  
Which pierces this dim universe like light,  
Self-empire, and the majesty of love;  
For thirst of which they fainted. Then Prometheus  
Gave wisdom, which is strength, to Jupiter,  
And with this law alone, &quot;Let man be free,&quot; *[2.4.45]*  
Clothed him with the dominion of wide Heaven.  
To know nor faith, nor love, nor law; to be  
Omnipotent but friendless is to reign;  
And Jove now reigned; for on the race of man  
First famine, and then toil, and then disease, *[2.4.50]*  
Strife, wounds, and ghastly death unseen before,  
Fell; and the unseasonable seasons drove  
With alternating shafts of frost and fire,  
Their shelterless, pale tribes to mountain caves:  
And in their desert hearts fierce wants he sent, *[2.4.55]*  
And mad disquietudes, and shadows idle  
Of unreal good, which levied mutual war,  
So ruining the lair wherein they raged.  
Prometheus saw, and waked the legioned hopes  
Which sleep within folded Elysian flowers, *[2.4.60]*  
Nepenthe, Moly, Amaranth, fadeless blooms,  
That they might hide with thin and rainbow wings  
The shape of Death; and Love he sent to bind  
The disunited tendrils of that vine  
Which bears the wine of life, the human heart; *[2.4.65]*  
And he tamed fire which, like some beast of prey,  
Most terrible, but lovely, played beneath  
The frown of man; and tortured to his will  
Iron and gold, the slaves and signs of power,  
And gems and poisons, and all subtlest forms *[2.4.70]*  
Hidden beneath the mountains and the waves.  
He gave man speech, and speech created thought,  
Which is the measure of the universe;  
And Science struck the thrones of earth and heaven,  
Which shook, but fell not; and the harmonious mind *[2.4.75]*  
Poured itself forth in all-prophetic song;  
And music lifted up the listening spirit  
Until it walked, exempt from mortal care,  
Godlike, o'er the clear billows of sweet sound;  
And human hands first mimicked and then mocked, *[2.4.80]*  
With moulded limbs more lovely than its own,  
The human form, till marble grew divine;  
And mothers, gazing, drank the love men see  
Reflected in their race, behold, and perish.  
He told the hidden power of herbs and springs, *[2.4.85]*  
And Disease drank and slept. Death grew like sleep.  
He taught the implicated orbits woven  
Of the wide-wandering stars; and how the sun  
Changes his lair, and by what secret spell  
The pale moon is transformed, when her broad eye *[2.4.90]*  
Gazes not on the interlunar sea:  
He taught to rule, as life directs the limbs,  
The tempest-wingèd chariots of the Ocean,  
And the Celt knew the Indian. Cities then  
Were built, and through their snow-like columns flowed *[2.4.95]*  
The warm winds, and the azure aether shone,  
And the blue sea and shadowy hills were seen.  
Such, the alleviations of his state,  
Prometheus gave to man, for which he hangs  
Withering in destined pain: but who rains down *[2.4.100]*  
Evil, the immedicable plague, which, while  
Man looks on his creation like a God  
And sees that it is glorious, drives him on,  
The wreck of his own will, the scorn of earth,  
The outcast, the abandoned, the alone? *[2.4.105]*  
Not Jove: while yet his frown shook Heaven, ay, when  
His adversary from adamantine chains  
Cursed him, he trembled like a slave. Declare  
Who is his master? Is he too a slave?

**Demogorgon.**
  
All spirits are enslaved which serve things evil: *[2.4.110]*  
Thou knowest if Jupiter be such or no.

**Asia.**
  
Whom calledst thou God?

**Demogorgon.**
  
                I spoke but as ye speak,  
For Jove is the supreme of living things.

**Asia.**
  
Who is the master of the slave?

**Demogorgon.**
  
                If the abysm  
Could vomit forth its secrets . . . But a voice *[2.4.115]*  
Is wanting, the deep truth is imageless;  
For what would it avail to bid thee gaze  
On the revolving world? What to bid speak  
Fate, Time, Occasion, Chance, and Change? To these  
All things are subject but eternal Love. *[2.4.120]*

**Asia.**
  
So much I asked before, and my heart gave  
The response thou hast given; and of such truths  
Each to itself must be the oracle.  
One more demand; and do thou answer me  
As mine own soul would answer, did it know *[2.4.125]*  
That which I ask. Prometheus shall arise  
Henceforth the sun of this rejoicing world:  
When shall the destined hour arrive?

**Demogorgon.**
  
                Behold!

**Asia.**
  
The rocks are cloven, and through the purple night  
I see cars drawn by rainbow-wingèd steeds *[2.4.130]*  
Which trample the dim winds: in each there stands  
A wild-eyed charioteer urging their flight.  
Some look behind, as fiends pursued them there,  
And yet I see no shapes but the keen stars:  
Others, with burning eyes, lean forth, and drink *[2.4.135]*  
With eager lips the wind of their own speed,  
As if the thing they loved fled on before,  
And now, even now, they clasped it. Their bright locks  
Stream like a comet's flashing hair: they all  
Sweep onward.

**Demogorgon.**
  
        These are the immortal Hours, *[2.4.140]*  
Of whom thou didst demand. One waits for thee.

**Asia.**
  
A spirit with a dreadful countenance  
Checks its dark chariot by the craggy gulf.  
Unlike thy brethren, ghastly charioteer,  
Who art thou? Whither wouldst thou bear me? Speak! *[2.4.145]*

**Spirit.**
  
I am the shadow of a destiny  
More dread than is my aspect: ere yon planet  
Has set, the darkness which ascends with me  
Shall wrap in lasting night heaven's kingless throne.

**Asia.**
  
What meanest thou?

**Panthea.**
  
        That terrible shadow floats *[2.4.150]*  
Up from its throne, as may the lurid smoke  
Of earthquake-ruined cities o'er the sea.  
Lo! it ascends the car; the coursers fly  
Terrified: watch its path among the stars  
Blackening the night!

**Asia.**
  
        Thus I am answered: strange! *[2.4.155]*

**Panthea.**
  
See, near the verge, another chariot stays;  
An ivory shell inlaid with crimson fire,  
Which comes and goes within its sculptured rim  
Of delicate strange tracery; the young spirit  
That guides it has the dove-like eyes of hope; *[2.4.160]*  
How its soft smiles attract the soul! as light  
Lures wingèd insects through the lampless air.

**Spirit.**
  
My coursers are fed with the lightning,  
   They drink of the whirlwind's stream,  
And when the red morning is bright'ning *[2.4.165]*  
   They bathe in the fresh sunbeam;  
   They have strength for their swiftness I deem,  
Then ascend with me, daughter of Ocean.  
  
I desire: and their speed makes night kindle;  
   I fear: they outstrip the Typhoon; *[2.4.170]*  
Ere the cloud piled on Atlas can dwindle  
   We encircle the earth and the moon:  
   We shall rest from long labours at noon:  
Then ascend with me, daughter of Ocean.

### Scene V.

— The Car pauses within a Cloud on the top of a snowy Mountain.
*Asia*, *Panthea* and the *Spirit of the Hour.*


**Spirit.**
  
    On the brink of the night and the morning  
   My coursers are wont to respire;  
But the Earth has just whispered a warning  
   That their flight must be swifter than fire:  
   They shall drink the hot speed of desire! *[2.5.5]*

**Asia.**
  
Thou breathest on their nostrils, but my breath  
Would give them swifter speed.

**Spirit.**
  
                Alas! it could not.

**Panthea.**
  
Oh Spirit! pause, and tell whence is the light  
Which fills this cloud? the sun is yet unrisen.

**Spirit.**
  
The sun will rise not until noon. Apollo *[2.5.10]*  
Is held in heaven by wonder; and the light  
Which fills this vapour, as the aëreal hue  
Of fountain-gazing roses fills the water,  
Flows from thy mighty sister.

**Panthea.**
  
                Yes, I feel —

**Asia.**
  
What is it with thee, sister? Thou art pale. *[2.5.15]*

**Panthea.**
  
How thou art changed! I dare not look on thee;  
I feel but see thee not. I scarce endure  
The radiance of thy beauty. Some good change  
Is working in the elements, which suffer  
Thy presence thus unveiled. The Nereids tell *[2.5.20]*  
That on the day when the clear hyaline  
Was cloven at thine uprise, and thou didst stand  
Within a veinèd shell, which floated on  
Over the calm floor of the crystal sea,  
Among the Ægean isles, and by the shores *[2.5.25]*  
Which bear thy name; love, like the atmosphere  
Of the sun's fire filling the living world,  
Burst from thee, and illumined earth and heaven  
And the deep ocean and the sunless caves  
And all that dwells within them; till grief cast *[2.5.30]*  
Eclipse upon the soul from which it came:  
Such art thou now; nor is it I alone,  
Thy sister, thy companion, thine own chosen one,  
But the whole world which seeks thy sympathy.  
Hearest thou not sounds i' the air which speak the love *[2.5.35]*  
Of all articulate beings? Feelest thou not  
The inanimate winds enamoured of thee? List!
<p><em>[Music.</em></p>

**Asia.**
  
Thy words are sweeter than aught else but his  
Whose echoes they are: yet all love is sweet,  
Given or returned. Common as light is love, *[2.5.40]*  
And its familiar voice wearies not ever.  
Like the wide heaven, the all-sustaining air,  
It makes the reptile equal to the God:  
They who inspire it most are fortunate,  
As I am now; but those who feel it most *[2.5.45]*  
Are happier still, after long sufferings,  
As I shall soon become.

**Panthea.**
  
        List! Spirits speak.

**Voice in the Air, singing.**
  
Life of Life! thy lips enkindle  
   With their love the breath between them;  
And thy smiles before they dwindle *[2.5.50]*  
   Make the cold air fire; then screen them  
In those looks, where whoso gazes  
Faints, entangled in their mazes.  
  
Child of Light! thy limbs are burning  
   Through the vest which seems to hide them; *[2.5.55]*  
As the radiant lines of morning  
   Through the clouds ere they divide them;  
And this atmosphere divinest  
Shrouds thee wheresoe'er thou shinest.  
  
Fair are others; none beholds thee, *[2.5.60]*  
   But thy voice sounds low and tender  
Like the fairest, for it folds thee  
   From the sight, that liquid splendour,  
And all feel, yet see thee never,  
As I feel now, lost for ever! *[2.5.65]*  
  
Lamp of Earth! where'er thou movest  
   Its dim shapes are clad with brightness,  
And the souls of whom thou lovest  
   Walk upon the winds with lightness,  
Till they fail, as I am failing, *[2.5.70]*  
Dizzy, lost, yet unbewailing!

**Asia.**
  
   My soul is an enchanted boat,  
   Which, like a sleeping swan, doth float  
Upon the silver waves of thy sweet singing;  
   And thine doth like an angel sit *[2.5.75]*  
   Beside a helm conducting it,  
Whilst all the winds with melody are ringing.  
   It seems to float ever, for ever,  
   Upon that many-winding river,  
   Between mountains, woods, abysses, *[2.5.80]*  
   A paradise of wildernesses!  
Till, like one in slumber bound,  
Borne to the ocean, I float down, around,  
Into a sea profound, of ever-spreading sound:  
  
   Meanwhile thy spirit lifts its pinions *[2.5.85]*  
   In music's most serene dominions;  
Catching the winds that fan that happy heaven.  
   And we sail on, away, afar,  
   Without a course, without a star,  
But, by the instinct of sweet music driven; *[2.5.90]*  
   Till through Elysian garden islets  
   By thee, most beautiful of pilots,  
   Where never mortal pinnace glided,  
   The boat of my desire is guided:  
Realms where the air we breathe is love, *[2.5.95]*  
Which in the winds and on the waves doth move,  
Harmonizing this earth with what we feel above.  
  
     We have passed Age's icy caves,  
     And Manhood's dark and tossing waves,  
And Youth's smooth ocean, smiling to betray: *[2.5.100]*  
   Beyond the glassy gulfs we flee  
   Of shadow-peopled Infancy,  
Through Death and Birth, to a diviner day;  
   A paradise of vaulted bowers,  
   Lit by downward-gazing flowers, *[2.5.105]*  
   And watery paths that wind between  
   Wildernesses calm and green,  
Peopled by shapes too bright to see,  
And rest, having beheld; somewhat like thee;  
Which walk upon the sea, and chant melodiously! *[2.5.110]*
### END OF THE SECOND ACT.
-----

## ACT III

### Scene I.

— Heaven. *Jupiter* on his Throne; *Thetis* and the other Deities assembled.


**Jupiter.**
  
Ye congregated powers of heaven, who share  
The glory and the strength of him ye serve,  
Rejoice! henceforth I am omnipotent.  
All else had been subdued to me; alone  
The soul of man, like unextinguished fire, *[3.1.5]*  
Yet burns towards heaven with fierce reproach, and doubt,  
And lamentation, and reluctant prayer,  
Hurling up insurrection, which might make  
Our antique empire insecure, though built  
On eldest faith, and hell's coeval, fear; *[3.1.10]*  
And though my curses through the pendulous air,  
Like snow on herbless peaks, fall flake by flake,  
And cling to it; though under my wrath's night  
It climbs the crags of life, step after step,  
Which wound it, as ice wounds unsandalled feet, *[3.1.15]*  
It yet remains supreme o'er misery,  
Aspiring, unrepressed, yet soon to fall:  
Even now have I begotten a strange wonder,  
That fatal child, the terror of the earth,  
Who waits but till the destined hour arrive, *[3.1.20]*  
Bearing from Demogorgon's vacant throne  
The dreadful might of ever-living limbs  
Which clothed that awful spirit unbeheld,  
To redescend, and trample out the spark.  
Pour forth heaven's wine, Idæan Ganymede, *[3.1.25]*  
And let it fill the Dædal cups like fire,  
And from the flower-inwoven soil divine  
Ye all-triumphant harmonies arise,  
As dew from earth under the twilight stars:  
Drink! be the nectar circling through your veins *[3.1.30]*  
The soul of joy, ye ever-living Gods,  
Till exultation burst in one wide voice  
Like music from Elysian winds.  
  
                And thou  
Ascend beside me, veilèd in the light  
Of the desire which makes thee one with me, *[3.1.35]*  
Thetis, bright image of eternity!  
When thou didst cry, &quot;Insufferable might!  
God! Spare me! I sustain not the quick flames,  
The penetrating presence; all my being,  
Like him whom the Numidian seps did thaw *[3.1.40]*  
Into a dew with poison, is dissolved,  
Sinking through its foundations&quot;: even then  
Two mighty spirits, mingling, made a third  
Mightier than either, which, unbodied now,  
Between us floats, felt, although unbeheld, *[3.1.45]*  
Waiting the incarnation, which ascends,  
(Hear ye the thunder of the fiery wheels  
Griding the winds?) from Demogorgon's throne.  
Victory! victory! Feel'st thou not, O world,  
The earthquake of his chariot thundering up *[3.1.50]*  
Olympus?
<p><em>[The Car of the Hour arrives.  
Demogorgon descends, and moves  
towards the Throne of Jupiter.</em></p>
        Awful shape, what art thou? Speak!

**Demogorgon.**
  
Eternity. Demand no direr name.  
Descend, and follow me down the abyss.  
I am thy child, as thou wert Saturn's child;  
Mightier than thee: and we must dwell together *[3.1.55]*  
Henceforth in darkness. Lift thy lightnings not.  
The tyranny of heaven none may retain,  
Or reassume, or hold, succeeding thee:  
Yet if thou wilt, as 'tis the destiny  
Of trodden worms to writhe till they are dead, *[3.1.60]*  
Put forth thy might.

**Jupiter.**
  
        Detested prodigy!  
Even thus beneath the deep Titanian prisons  
I trample thee! thou lingerest?  
  
                Mercy! mercy!  
No pity, no release, no respite! Oh,  
That thou wouldst make mine enemy my judge, *[3.1.65]*  
Even where he hangs, seared by my long revenge,  
On Caucasus! he would not doom me thus.  
Gentle, and just, and dreadless, is he not  
The monarch of the world? What then art thou?  
No refuge! no appeal!  
  
                Sink with me then, *[3.1.70]*  
We two will sink on the wide waves of ruin,  
Even as a vulture and a snake outspent  
Drop, twisted in inextricable fight,  
Into a shoreless sea. Let hell unlock  
Its mounded oceans of tempestuous fire, *[3.1.75]*  
And whelm on them into the bottomless void  
This desolated world, and thee, and me,  
The conqueror and the conquered, and the wreck  
Of that for which they combated.  
  
                Ai! Ai!  
The elements obey me not. I sink *[3.1.80]*  
Dizzily down, ever, for ever, down.  
And, like a cloud, mine enemy above  
Darkens my fall with victory! Ai, Ai!

### Scene II.

— The Mouth of a great River in the Island Atlantis. *Ocean* is discovered reclining near the Shore; *Apollo* stands beside him.


**Ocean.**
  
He fell, thou sayest, beneath his conqueror's frown?

**Apollo.**
  
Ay, when the strife was ended which made dim  
The orb I rule, and shook the solid stars,  
The terrors of his eye illumined heaven  
With sanguine light, through the thick ragged skirts *[3.2.5]*  
Of the victorious darkness, as he fell:  
Like the last glare of day's red agony,  
Which, from a rent among the fiery clouds,  
Burns far along the tempest-wrinkled deep.

**Ocean.**
  
He sunk to the abyss? To the dark void? *[3.2.10]*

**Apollo.**
  
An eagle so caught in some bursting cloud  
On Caucasus, his thunder-baffled wings  
Entangled in the whirlwind, and his eyes  
Which gazed on the undazzling sun, now blinded  
By the white lightning, while the ponderous hail *[3.2.15]*  
Beats on his struggling form, which sinks at length  
Prone, and the aëreal ice clings over it.

**Ocean.**
  
Henceforth the fields of heaven-reflecting sea  
Which are my realm, will heave, unstained with blood,  
Beneath the uplifting winds, like plains of corn *[3.2.20]*  
Swayed by the summer air; my streams will flow  
Round many-peopled continents, and round  
Fortunate isles; and from their glassy thrones  
Blue Proteus and his humid nymphs shall mark  
The shadow of fair ships, as mortals see *[3.2.25]*  
The floating bark of the light-laden moon  
With that white star, its sightless pilot's crest,  
Borne down the rapid sunset's ebbing sea;  
Tracking their path no more by blood and groans,  
And desolation, and the mingled voice *[3.2.30]*  
Of slavery and command; but by the light  
Of wave-reflected flowers, and floating odours,  
And music soft, and mild, free, gentle voices,  
And sweetest music, such as spirits love.

**Apollo.**
  
And I shall gaze not on the deeds which make *[3.2.35]*  
My mind obscure with sorrow, as eclipse  
Darkens the sphere I guide; but list, I hear  
The small, clear, silver lute of the young Spirit  
That sits i' the morning star.

**Ocean.**
  
                Thou must away;  
Thy steeds will pause at even, till when farewell: *[3.2.40]*  
The loud deep calls me home even now to feed it  
With azure calm out of the emerald urns  
Which stand for ever full beside my throne.  
Behold the Nereids under the green sea,  
Their wavering limbs borne on the wind-like stream, *[3.2.45]*  
Their white arms lifted o'er their streaming hair  
With garlands pied and starry sea-flower crowns,  
Hastening to grace their mighty sister's joy.
<p><em>[A sound of waves is heard.</em></p>
It is the unpastured sea hungering for calm.  
Peace, monster; I come now. Farewell.

**Apollo.**
  
                Farewell. *[3.2.50]*

### Scene III.

> — Caucasus. *Prometheus*, *Hercules*, *Ione*, the *Earth*, *Asia* and *Panthea*, bourne in the Car with the *Spirit of the Hour*. *Hercules* unbinds *Prometheus*, who descends.


**Hercules.**
  
Most glorious among Spirits, thus doth strength  
To wisdom, courage, and long-suffering love,  
And thee, who art the form they animate,  
Minister like a slave.

**Prometheus.**
  
        Thy gentle words  
Are sweeter even than freedom long desired *[3.3.5]*  
And long delayed.  
  
        Asia, thou light of life,  
Shadow of beauty unbeheld: and ye,  
Fair sister nymphs, who made long years of pain  
Sweet to remember, through your love and care:  
Henceforth we will not part. There is a cave, *[3.3.10]*  
All overgrown with trailing odorous plants,  
Which curtain out the day with leaves and flowers,  
And paved with veinèd emerald, and a fountain  
Leaps in the midst with an awakening sound.  
From its curved roof the mountain's frozen tears *[3.3.15]*  
Like snow, or silver, or long diamond spires,  
Hang downward, raining forth a doubtful light:  
And there is heard the ever-moving air,  
Whispering without from tree to tree, and birds,  
And bees; and all around are mossy seats, *[3.3.20]*  
And the rough walls are clothed with long soft grass;  
A simple dwelling, which shall be our own;  
Where we will sit and talk of time and change,  
As the world ebbs and flows, ourselves unchanged.  
What can hide man from mutability? *[3.3.25]*  
And if ye sigh, then I will smile; and thou,  
Ione, shalt chant fragments of sea-music,  
Until I weep, when ye shal smile away  
The tears she brought, which yet were sweet to shed.  
We will entangle buds and flowers and beams *[3.3.30]*  
Which twinkle on the fountain's brim, and make  
Strange combinations out of common things,  
Like human babes in their brief innocence;  
And we will search, with looks and words of love,  
For hidden thoughts, each lovelier than the last, *[3.3.35]*  
Our unexhausted spirits; and like lutes  
Touched by the skill of the enamoured wind,  
Weave harmonies divine, yet ever new,  
From difference sweet where discord cannot be;  
And hither come, sped on the charmèd winds, *[3.3.40]*  
Which meet from all the points of heaven, as bees  
From every flower aëreal Enna feeds,  
At their known island-homes in Himera,  
The echoes of the human world, which tell  
Of the low voice of love, almost unheard, *[3.3.45]*  
And dove-eyed pity's murmured pain, and music,  
Itself the echo of the heart, and all  
That tempers or improves man's life, now free;  
And lovely apparitions, — dim at first,  
Then radiant, as the mind, arising bright *[3.3.50]*  
From the embrace of beauty (whence the forms  
Of which these are the phantoms) casts on them  
The gathered rays which are reality —  
Shall visit us, the progeny immortal  
Of Painting, Sculpture, and rapt Poesy, *[3.3.55]*  
And arts, though unimagined, yet to be.  
The wandering voices and the shadows these  
Of all that man becomes, the mediators  
Of that best worship love, by him and us  
Given and returned; swift shapes and sounds, which grow *[3.3.60]*  
More fair and soft as man grows wise and kind,  
And, veil by veil, evil and error fall:  
Such virtue has the cave and place around.
<p><em>[Turning to the Spirit of the Hour.</em></p>
For thee, fair Spirit, one toil remains. Ione,  
Give her that curvèd shell, which Proteus old *[3.3.65]*  
Made Asia's nuptial boon, breathing within it  
A voice to be accomplished, and which thou  
Didst hide in grass under the hollow rock.

**Ione.**
  
Thou most desired Hour, more loved and lovely  
Than all thy sisters, this is the mystic shell; *[3.3.70]*  
See the pale azure fading into silver  
Lining it with a soft yet glowing light:  
Looks it not like lulled music sleeping there?

**Spirit.**
  
It seems in truth the fairest shell of Ocean:  
Its sound must be at once both sweet and strange. *[3.3.75]*

**Prometheus.**
  
Go, borne over the cities of mankind  
On whirlwind-footed coursers: once again  
Outspeed the sun around the orbèd world;  
And as thy chariot cleaves the kindling air,  
Thou breathe into the many-folded shell, *[3.3.80]*  
Loosening its mighty music; it shall be  
As thunder mingled with clear echoes: then  
Return; and thou shalt dwell beside our cave.  
And thou, O, Mother Earth! —

**The Earth.**
  
                I hear, I feel;  
Thy lips are on me, and their touch runs down *[3.3.85]*  
Even to the adamantine central gloom  
Along these marble nerves; 'tis life, 'tis joy,  
And through my withered, old, and icy frame  
The warmth of an immortal youth shoots down  
Circling. Henceforth the many children fair *[3.3.90]*  
Folded in my sustaining arms; all plants,  
And creeping forms, and insects rainbow-winged,  
And birds, and beasts, and fish, and human shapes,  
Which drew disease and pain from my wan bosom,  
Draining the poison of despair, shall take *[3.3.95]*  
And interchange sweet nutriment; to me  
Shall they become like sister-antelopes  
By one fair dam, snow-white and swift as wind,  
Nursed among lilies near a brimming stream.  
The dew-mists of my sunless sleep shall float *[3.3.100]*  
Under the stars like balm: night-folded flowers  
Shall suck unwithering hues in their repose:  
And men and beasts in happy dreams shall gather  
Strength for the coming day, and all its joy:  
And death shall be the last embrace of her *[3.3.105]*  
Who takes the life she gave, even as a mother  
Folding her child, says, &quot;Leave me not again.&quot;

**Asia.**
  
Oh, mother! wherefore speak the name of death?  
Cease they to love, and move, and breathe, and speak,  
Who die?

**The Earth.**
  
        It would avail not to reply: *[3.3.110]*  
Thou art immortal, and this tongue is known  
But to the uncommunicating dead.  
Death is the veil which those who live call life:  
They sleep, and it is lifted: and meanwhile  
In mild variety the seasons mild *[3.3.115]*  
With rainbow-skirted showers, and odorous winds,  
And long blue meteors cleansing the dull night,  
And the life-kindling shafts of the keen sun's  
All-piercing bow, and the dew-mingled rain  
Of the calm moonbeams, a soft influence mild, *[3.3.120]*  
Shall clothe the forests and the fields, ay, even  
The crag-built deserts of the barren deep,  
With ever-living leaves, and fruits, and flowers.  
And thou! There is a cavern where my spirit  
Was panted forth in anguish whilst thy pain *[3.3.125]*  
Made my heart mad, and those who did inhale it  
Became mad too, and built a temple there,  
And spoke, and were oracular, and lured  
The erring nations round to mutual war,  
And faithless faith, such as Jove kept with thee; *[3.3.130]*  
Which breath now rises, as amongst tall weeds  
A violet's exhalation, and it fills  
With a serener light and crimson air  
Intense, yet soft, the rocks and woods around;  
It feeds the quick growth of the serpent vine, *[3.3.135]*  
And the dark linkèd ivy tangling wild,  
And budding, blown, or odour-faded blooms  
Which star the winds with points of coloured light,  
As they rain through them, and bright golden globes  
Of fruit, suspended in their own green heaven, *[3.3.140]*  
And through their veinèd leaves and amber stems  
The flowers whose purple and translucid bowls  
Stand ever mantling with aëreal dew,  
The drink of spirits: and it circles round,  
Like the soft waving wings of noonday dreams, *[3.3.145]*  
Inspiring calm and happy thoughts, like mine,  
Now thou art thus restored. This cave is thine.  
Arise! Appear!
<p><em>[A Spirit rises in the likeness of a winged child.</em></p>
        This is my torch-bearer;  
Who let his lamp out in old time with gazing  
On eyes from which he kindled it anew *[3.3.150]*  
With love, which is as fire, sweet daughter mine,  
For such is that within thine own. Run, wayward,  
And guide this company beyond the peak  
Of Bacchic Nysa, Mænad-haunted mountain,  
And beyond Indus and its tribute rivers, *[3.3.155]*  
Trampling the torrent streams and glassy lakes  
With feet unwet, unwearied, undelaying,  
And up the green ravine, across the vale,  
Beside the windless and crystalline pool,  
Where ever lies, on unerasing waves, *[3.3.160]*  
The image of a temple, built above,  
Distinct with column, arch, and architrave,  
And palm-like capital, and over-wrought,  
And populous with most living imagery,  
Praxitelean shapes, whose marble smiles *[3.3.165]*  
Fill the hushed air with everlasting love.  
It is deserted now, but once it bore  
Thy name, Prometheus; there the emulous youths  
Bore to thy honour through the divine gloom  
The lamp which was thine emblem; even as those *[3.3.170]*  
Who bear the untransmitted torch of hope  
Into the grave, across the night of life,  
As thou hast borne it most triumphantly  
To this far goal of Time. Depart, farewell.  
Beside that temple is the destined cave. *[3.3.175]*

### Scene IV.

— A Forest. In the Background a Cave. *Prometheus*, *Asia*, *Panthera*, *Ione*, and the *Spirit of the Earth*.


**Ione.**
  
Sister, it is not earthly: how it glides  
Under the leaves! how on its head there burns  
A light, like a green star, whose emerald beams  
Are twined with its fair hair! how, as it moves,  
The splendour drops in flakes upon the grass! *[3.4.5]*  
Knowest thou it?

**Panthea.**
  
        It is the delicate spirit  
That guides the earth through heaven. From afar  
The populous constellations call that light  
The loveliest of the planets; and sometimes  
It floats along the spray of the salt sea, *[3.4.10]*  
Or makes its chariot of a foggy cloud,  
Or walks through fields or cities while men sleep,  
Or o'er the mountain tops, or down the rivers,  
Or through the green waste wilderness, as now,  
Wondering at all it sees. Before Jove reigned *[3.4.15]*  
It loved our sister Asia, and it came  
Each leisure hour to drink the liquid light  
Out of her eyes, for which it said it thirsted  
As one bit by a dipsas, and with her  
It made its childish confidence, and told her *[3.4.20]*  
All it had known or seen, for it saw much,  
Yet idly reasoned what it saw; and called her —  
For whence it sprung it knew not, nor do I —  
Mother, dear mother.

**The Spirit of the Earth <em>(running to Asia).</em>**
  
        Mother, dearest mother;  
May I then talk with thee as I was wont? *[3.4.25]*  
May I then hide my eyes in thy soft arms,  
After thy looks have made them tired of joy?  
May I then play beside thee the long noons,  
When work is none in the bright silent air?

**Asia.**
  
I love thee, gentlest being, and henceforth *[3.4.30]*  
Can cherish thee unenvied: speak, I pray:  
Thy simple talk once solaced, now delights.

**Spirit of the Earth.**
  
Mother, I am grown wiser, though a child  
Cannot be wise like thee, within this day;  
And happier too; happier and wiser both. *[3.4.35]*  
Thou knowest that toads, and snakes, and loathly worms,  
And venomous and malicious beasts, and boughs  
That bore ill berries in the woods, were ever  
An hindrance to my walks o'er the green world:  
And that, among the haunts of humankind, *[3.4.40]*  
Hard-featured men, or with proud, angry looks,  
Or cold, staid gait, or false and hollow smiles,  
Or the dull sneer of self-loved ignorance,  
Or other such foul masks, with which ill thoughts  
Hide that fair being whom we spirits call man; *[3.4.45]*  
And women too, ugliest of all things evil,  
(Though fair, even in a world where thou art fair,  
When good and kind, free and sincere like thee),  
When false or frowning made me sick at heart  
To pass them, though they slept, and I unseen. *[3.4.50]*  
Well, my path lately lay through a great city  
Into the woody hills surrounding it:  
A sentinel was sleeping at the gate:  
When there was heard a sound, so loud, it shook  
The towers amid the moonlight, yet more sweet *[3.4.55]*  
Than any voice but thine, sweetest of all;  
A long, long sound, as it would never end:  
And all the inhabitants leaped suddenly  
Out of their rest, and gathered in the streets,  
Looking in wonder up to Heaven, while yet *[3.4.60]*  
The music pealed along. I hid myself  
Within a fountain in the public square,  
Where I lay like the reflex of the moon  
Seen in a wave under green leaves; and soon  
Those ugly human shapes and visages *[3.4.65]*  
Of which I spoke as having wrought me pain,  
Passed floating through the air, and fading still  
Into the winds that scattered them; and those  
From whom they passed seemed mild and lovely forms  
After some foul disguise had fallen, and all *[3.4.70]*  
Were somewhat changed, and after brief surprise  
And greetings of delighted wonder, all  
Went to their sleep again: and when the dawn  
Came, wouldst thou think that toads, and snakes, and efts,  
Could e'er be beautiful? yet so they were, *[3.4.75]*  
And that with little change of shape or hue:  
All things had put their evil nature off:  
I cannot tell my joy, when o'er a lake  
Upon a drooping bough with nightshade twined,  
I saw two azure halcyons clinging downward *[3.4.80]*  
And thinning one bright bunch of amber berries,  
With quick long beaks, and in the deep there lay  
Those lovely forms imaged as in a sky;  
So, with my thoughts full of these happy changes,  
We meet again, the happiest change of all. *[3.4.85]*

**Asia.**
  
And never will we part, till thy chaste sister  
Who guides the frozen and inconstant moon  
Will look on thy more warm and equal light  
Till her heart thaw like flakes of April snow  
And love thee.

**Spirit of the Earth.**
  
        What; as Asia loves Prometheus? *[3.4.90]*

**Asia.**
  
Peace, wanton, thou art yet not old enough.  
Think ye by gazing on each other's eyes  
To multiply your lovely selves, and fill  
With spherèd fires the interlunar air?

**Spirit of the Earth.**
  
Nay, mother, while my sister trims her lamp *[3.4.95]*  
'Tis hard I should go darkling.

**Asia.**
  
                Listen; look!
<p><em>[The Spirit of the Hour enters.</em></p>

**Prometheus.**
  
We feel what thou hast heard and seen: yet speak.

**Spirit of the Hour.**
  
Soon as the sound had ceased whose thunder filled  
The abysses of the sky and the wide earth,  
There was a change: the impalpable thin air *[3.4.100]*  
And the all-circling sunlight were transformed,  
As if the sense of love dissolved in them  
Had folded itself round the spherèd world.  
My vision then grew clear, and I could see  
Into the mysteries of the universe: *[3.4.105]*  
Dizzy as with delight I floated down,  
Winnowing the lightsome air with languid plumes,  
My coursers sought their birthplace in the sun,  
Where they henceforth will live exempt from toil,  
Pasturing flowers of vegetable fire; *[3.4.110]*  
And where my moonlike car will stand within  
A temple, gazed upon by Phidian forms  
Of thee, and Asia, and the Earth, and me,  
And you fair nymphs looking the love we feel, —  
In memory of the tidings it has borne, — *[3.4.115]*  
Beneath a dome fretted with graven flowers,  
Poised on twelve columns of resplendent stone,  
And open to the bright and liquid sky.  
Yoked to it by an amphisbaenic snake  
The likeness of those wingèd steeds will mock *[3.4.120]*  
The flight from which they find repose. Alas,  
Whither has wandered now my partial tongue  
When all remains untold which ye would hear?  
As I have said, I floated to the earth:  
It was, as it is still, the pain of bliss *[3.4.125]*  
To move, to breathe, to be; I wandering went  
Among the haunts and dwellings of mankind,  
And first was disappointed not to see  
Such mighty change as I had felt within  
Expressed in outward things; but soon I looked, *[3.4.130]*  
And behold, thrones were kingless, and men walked  
One with the other even as spirits do,  
None fawned, none trampled; hate, disdain, or fear,  
Self-love or self-contempt, on human brows  
No more inscribed, as o'er the gate of hell, *[3.4.135]*  
&quot;All hope abandon ye who enter here&quot;;  
None frowned, none trembled, none with eager fear  
Gazed on another's eye of cold command,  
Until the subject of a tyrant's will  
Became, worse fate, the abject of his own, *[3.4.140]*  
Which spurred him, like an outspent horse, to death.  
None wrought his lips in truth-entangling lines  
Which smiled the lie his tongue disdained to speak;  
None, with firm sneer, trod out in his own heart  
The sparks of love and hope till there remained *[3.4.145]*  
Those bitter ashes, a soul self-consumed,  
And the wretch crept a vampire among men,  
Infecting all with his own hideous ill;  
None talked that common, false, cold, hollow talk  
Which makes the heart deny the <em>yes</em> it breathes, *[3.4.150]*  
Yet question that unmeant hypocrisy  
With such a self-mistrust as has no name.  
And women, too, frank, beautiful, and kind  
As the free heaven which rains fresh light and dew  
On the wide earth, past; gentle radiant forms, *[3.4.155]*  
From custom's evil taint exempt and pure;  
Speaking the wisdom once they could not think,  
Looking emotions once they feared to feel,  
And changed to all which once they dared not be,  
Yet being now, made earth like heaven; nor pride, *[3.4.160]*  
Nor jealousy, nor envy, nor ill shame,  
The bitterest of those drops of treasured gall,  
Spoilt the sweet taste of the nepenthe, love.  
  
Thrones, altars, judgement-seats, and prisons; wherein,  
And beside which, by wretched men were borne *[3.4.165]*  
Sceptres, tiaras, swords, and chains, and tomes  
Of reasoned wrong, glozed on by ignorance,  
Were like those monstrous and barbaric shapes,  
The ghosts of a no-more-remembered fame,  
Which, from their unworn obelisks, look forth *[3.4.170]*  
In triumph o'er the palaces and tombs  
Of those who were their conquerors: mouldering round,  
These imaged to the pride of kings and priests  
A dark yet mighty faith, a power as wide  
As is the world it wasted, and are now *[3.4.175]*  
But an astonishment; even so the tools  
And emblems of its last captivity,  
Amid the dwellings of the peopled earth,  
Stand, not o'erthrown, but unregarded now.  
And those foul shapes, abhorred by god and man, — *[3.4.180]*  
Which, under many a name and many a form  
Strange, savage, ghastly, dark and execrable,  
Were Jupiter, the tyrant of the world;  
And which the nations, panic-stricken, served  
With blood, and hearts broken by long hope, and love *[3.4.185]*  
Dragged to his altars soiled and garlandless,  
And slain amid men's unreclaiming tears,  
Flattering the thing they feared, which fear was hate, —  
Frown, mouldering fast, o'er their abandoned shrines:  
The painted veil, by those who were, called life, *[3.4.190]*  
Which mimicked, as with colours idly spread,  
All men believed or hoped, is torn aside;  
The loathsome mask has fallen, the man remains  
Sceptreless, free, uncircumscribed, but man  
Equal, unclassed, tribeless, and nationless, *[3.4.195]*  
Exempt from awe, worship, degree, the king  
Over himself; just, gentle, wise: but man  
Passionless? — no, yet free from guilt or pain,  
Which were, for his will made or suffered them,  
Nor yet exempt, though ruling them like slaves, *[3.4.200]*  
From chance, and death, and mutability,  
The clogs of that which else might oversoar  
The loftiest star of unascended heaven,  
Pinnacled dim in the intense inane.
### END OF THE THIRD ACT.

-----

## ACT IV.

Scene. – A Part of the Forest near the Cave of *Prometheus*. *Pantera* and *Ione* are sleeping: they awaken gradually during the first Song.


**Voice of unseen Spirits.**
  
   The pale stars are gone!  
   For the sun, their swift shepherd,  
   To their folds them compelling,  
   In the depths of the dawn,  
Hastes, in meteor-eclipsing array, and they flee *[4.5]*  
   Beyond his blue dwelling,  
   As fawns flee the leopard.  
      But where are ye?
<p><em>A Train of dark Forms and Shadows passes by confusedly, singing.</em></p>
   Here, oh, here:  
   We bear the bier *[4.10]*  
Of the Father of many a cancelled year!  
   Spectres we  
   Of the dead Hours be,  
We bear Time to his tomb in eternity.  
  
   Strew, oh, strew *[4.15]*  
   Hair, not yew!  
Wet the dusty pall with tears, not dew!  
   Be the faded flowers  
   Of Death's bare bowers  
Spread on the corpse of the King of Hours! *[4.20]*  
   Haste, oh, haste!  
   As shades are chased,  
Trembling, by day, from heaven's blue waste.  
   We melt away,  
   Like dissolving spray, *[4.25]*  
From the children of a diviner day,  
   With the lullaby  
   Of winds that die  
On the bosom of their own harmony!

**Ione.**
  
What dark forms were they? *[4.30]*

**Panthea.**
  
The past Hours weak and gray,  
With the spoil which their toil  
   Raked together  
From the conquest but One could foil.

**Ione.**
  
Have they passed?

**Panthea.**
  
        They have passed; *[4.35]*  
They outspeeded the blast,  
While 'tis said, they are fled:

**Ione.**
  
Whither, oh, whither?

**Panthea.**
  
To the dark, to the past, to the dead.

**Voice of unseen Spirits.**
  
   Bright clouds float in heaven, *[4.40]*  
   Dew-stars gleam on earth,  
   Waves assemble on ocean,  
   They are gathered and driven  
By the storm of delight, by the panic of glee!  
   They shake with emotion, *[4.45]*  
   They dance in their mirth.  
      But where are ye?  
  
   The pine boughs are singing  
   Old songs with new gladness,  
   The billows and fountains *[4.50]*  
   Fresh music are flinging,  
Like the notes of a spirit from land and from sea;  
   The storms mock the mountains  
   With the thunder of gladness.  
      But where are ye? *[4.55]*

**Ione.**
  
What charioteers are these?

**Panthea.**
  
        Where are their chariots?

**Semichorus of Hours.**
  
The voice of the Spirits of Air and of Earth  
   Have drawn back the figured curtain of sleep  
Which covered our being and darkened our birth  
In the deep.

**A Voice.**
  
        In the deep?

**Semichorus II.**
  
                Oh, below the deep. *[4.60]*

**Semichorus I.**
  
An hundred ages we had been kept  
   Cradled in visions of hate and care,  
And each one who waked as his brother slept,  
   Found the truth —

**Semichorus II.**
  
                Worse than his visions were!

**Semichorus I.**
  
We have heard the lute of Hope in sleep; *[4.65]*  
   We have known the voice of Love in dreams;  
We have felt the wand of Power, and leap —

**Semichorus II.**
  
As the billows leap in the morning beams!

**Chorus.**
  
Weave the dance on the floor of the breeze,  
   Pierce with song heaven's silent light, *[4.70]*  
Enchant the day that too swiftly flees,  
   To check its flight ere the cave of Night.  
  
Once the hungry Hours were hounds  
   Which chased the day like a bleeding deer,  
And it limped and stumbled with many wounds *[4.75]*  
   Through the nightly dells of the desert year.  
  
But now, oh weave the mystic measure  
   Of music, and dance, and shapes of light,  
Let the Hours, and the spirits of might and pleasure,  
Like the clouds and sunbeams, unite.

**A Voice.**
  
                Unite! *[4.80]*

**Panthea.**
  
See, where the Spirits of the human mind  
Wrapped in sweet sounds, as in bright veils, approach.

**Chorus of Spirits.**
  
   We join the throng  
   Of the dance and the song,  
By the whirlwind of gladness borne along; *[4.85]*  
   As the flying-fish leap  
   From the Indian deep,  
And mix with the sea-birds, half asleep.

**Chorus of Hours.**
  
Whence come ye, so wild and so fleet,  
For sandals of lightning are on your feet, *[4.90]*  
And your wings are soft and swift as thought,  
And your eyes are as love which is veilèd not?

**Chorus of Spirits.**
  
   We come from the mind  
   Of human kind  
Which was late so dusk, and obscene, and blind, *[4.95]*  
   Now 'tis an ocean  
   Of clear emotion,  
A heaven of serene and mighty motion  
  
   From that deep abyss  
   Of wonder and bliss, *[4.100]*  
Whose caverns are crystal palaces;  
   From those skiey towers  
   Where Thought's crowned powers  
Sit watching your dance, ye happy Hours!  
  
   From the dim recesses *[4.105]*  
   Of woven caresses,  
Where lovers catch ye by your loose tresses  
   From the azure isles,  
   Where sweet Wisdom smiles,  
Delaying your ships with her siren wiles. *[4.110]*  
  
   From the temples high  
   Of Man's ear and eye,  
Roofed over Sculpture and Poesy;  
   From the murmurings  
   Of the unsealed springs *[4.115]*  
Where Science bedews her Dædal wings.  
  
   Years after years,  
   Through blood, and tears,  
And a thick hell of hatreds, and hopes, and fears;  
   We waded and flew, *[4.120]*  
   And the islets were few  
Where the bud-blighted flowers of happiness grew.  
  
   Our feet now, every palm,  
   Are sandalled with calm,  
And the dew of our wings is a rain of balm; *[4.125]*  
   And, beyond our eyes,  
   The human love lies  
Which makes all it gazes on Paradise.

**Chorus of Spirits and Hours.**
  
   Then weave the web of the mystic measure;  
From the depths of the sky and the ends of the earth, *[4.130]*  
   Come, swift Spirits of might and of pleasure,  
Fill the dance and the music of mirth,  
   As the waves of a thousand streams rush by  
   To an ocean of splendour and harmony!

**Chorus of Spirits.**
  
   Our spoil is won, *[4.135]*  
   Our task is done,  
We are free to dive, or soar, or run;  
   Beyond and around,  
   Or within the bound  
Which clips the world with darkness round. *[4.140]*  
  
   We'll pass the eyes  
   Of the starry skies  
Into the hoar deep to colonize:  
   Death, Chaos, and Night,  
   From the sound of our flight, *[4.145]*  
Shall flee, like mist from a tempest's might.  
  
   And Earth, Air, and Light,  
   And the Spirit of Might,  
Which drives round the stars in their fiery flight;  
   And Love, Thought, and Breath, *[4.150]*  
   The powers that quell Death,  
Wherever we soar shall assemble beneath.  
  
   And our singing shall build  
   In the void's loose field  
A world for the Spirit of Wisdom to wield; *[4.155]*  
   We will take our plan  
   From the new world of man,  
And our work shall be called the Promethean.

**Chorus of Hours.**
  
Break the dance, and scatter the song;  
   Let some depart, and some remain. *[4.160]*

**Semichorus I.**
  
We, beyond heaven, are driven along:

**Semichorus II.**
  
Us the enchantments of earth retain:

**Semichorus I.**
  
Ceaseless, and rapid, and fierce, and free,  
With the Spirits which build a new earth and sea,  
And a heaven where yet heaven could never be. *[4.165]*

**Semichorus II.**
  
Solemn, and slow, and serene, and bright,  
Leading the Day and outspeeding the Night,  
With the powers of a world of perfect light.

**Semichorus I.**
  
We whirl, singing loud, round the gathering sphere,  
Till the trees, and the beasts, and the clouds appear *[4.170]*  
From its chaos made calm by love, not fear.

**Semichorus II.**
  
We encircle the ocean and mountains of earth,  
And the happy forms of its death and birth  
Change to the music of our sweet mirth.

**Chorus of Hours and Spirits.**
  
Break the dance, and scatter the song, *[4.175]*  
   Let some depart, and some remain,  
Wherever we fly we lead along  
In leashes, like starbeams, soft yet strong,  
   The clouds that are heavy with love's sweet rain.

**Panthea.**
  
Ha! they are gone!

**Ione.**
  
        Yet feel you no delight *[4.180]*  
From the past sweetness?

**Panthea.**
  
        As the bare green hill  
When some soft cloud vanishes into rain,  
Laughs with a thousand drops of sunny water  
To the unpavilioned sky!

**Ione.**
  
        Even whilst we speak  
New notes arise. What is that awful sound? *[4.185]*

**Panthea.**
  
'Tis the deep music of the rolling world  
Kindling within the strings of the waved air  
Æolian modulations.

**Ione.**
  
        Listen too,  
How every pause is filled with under-notes,  
Clear, silver, icy, keen, awakening tones, *[4.190]*  
Which pierce the sense, and live within the soul,  
As the sharp stars pierce winter's crystal air  
And gaze upon themselves within the sea.

**Panthea.**
  
But see where through two openings in the forest  
Which hanging branches overcanopy, *[4.195]*  
And where two runnels of a rivulet,  
Between the close moss violet-inwoven,  
Have made their path of melody, like sisters  
Who part with sighs that they may meet in smiles,  
Turning their dear disunion to an isle *[4.200]*  
Of lovely grief, a wood of sweet sad thoughts;  
Two visions of strange radiance float upon  
The ocean-like enchantment of strong sound,  
Which flows intenser, keener, deeper yet  
Under the ground and through the windless air. *[4.205]*

**Ione.**
  
I see a chariot like that thinnest boat,  
In which the Mother of the Months is borne  
By ebbing light into her western cave,  
When she upsprings from interlunar dreams;  
O'er which is curved an orblike canopy *[4.210]*  
Of gentle darkness, and the hills and woods,  
Distinctly seen through that dusk aery veil,  
Regard like shapes in an enchanter's glass;  
Its wheels are solid clouds, azure and gold,  
Such as the genii of the thunderstorm *[4.215]*  
Pile on the floor of the illumined sea  
When the sun rushes under it; they roll  
And move and grow as with an inward wind;  
Within it sits a wingèd infant, white  
Its countenance, like the whiteness of bright snow, *[4.220]*  
Its plumes are as feathers of sunny frost,  
Its limbs gleam white, through the wind-flowing folds  
Of its white robe, woof of ethereal pearl.  
Its hair is white, the brightness of white light  
Scattered in strings; yet its two eyes are heavens *[4.225]*  
Of liquid darkness, which the Deity  
Within seems pouring, as a storm is poured  
From jaggèd clouds, out of their arrowy lashes,  
Tempering the cold and radiant air around,  
With fire that is not brightness; in its hand *[4.230]*  
It sways a quivering moonbeam, from whose point  
A guiding power directs the chariot's prow  
Over its wheelèd clouds, which as they roll  
Over the grass, and flowers, and waves, wake sounds,  
Sweet as a singing rain of silver dew. *[4.235]*

**Panthea.**
  
And from the other opening in the wood  
Rushes, with loud and whirlwind harmony,  
A sphere, which is as many thousand spheres,  
Solid as crystal, yet through all its mass  
Flow, as through empty space, music and light: *[4.240]*  
Ten thousand orbs involving and involved,  
Purple and azure, white, and green, and golden,  
Sphere within sphere; and every space between  
Peopled with unimaginable shapes,  
Such as ghosts dream dwell in the lampless deep, *[4.245]*  
Yet each inter-transpicuous, and they whirl  
Over each other with a thousand motions,  
Upon a thousand sightless axles spinning,  
And with the force of self-destroying swiftness,  
Intensely, slowly, solemnly roll on, *[4.250]*  
Kindling with mingled sounds, and many tones,  
Intelligible words and music wild.  
With mighty whirl the multitudinous orb  
Grinds the bright brook into an azure mist  
Of elemental subtlety, like light; *[4.255]*  
And the wild odour of the forest flowers,  
The music of the living grass and air,  
The emerald light of leaf-entangled beams  
Round its intense yet self-conflicting speed,  
Seem kneaded into one aëreal mass *[4.260]*  
Which drowns the sense. Within the orb itself,  
Pillowed upon its alabaster arms,  
Like to a child o'erwearied with sweet toil,  
On its own folded wings, and wavy hair,  
The Spirit of the Earth is laid asleep, *[4.265]*  
And you can see its little lips are moving,  
Amid the changing light of their own smiles,  
Like one who talks of what he loves in dream.

**Ione.**
  
'Tis only mocking the orb's harmony.

**Panthea.**
  
And from a star upon its forehead, shoot, *[4.270]*  
Like swords of azure fire, or golden spears  
With tyrant-quelling myrtle overtwined,  
Embleming heaven and earth united now,  
Vast beams like spokes of some invisible wheel  
Which whirl as the orb whirls, swifter than thought, *[4.275]*  
Filling the abyss with sun-like lightenings,  
And perpendicular now, and now transverse,  
Pierce the dark soil, and as they pierce and pass,  
Make bare the secrets of the earth's deep heart;  
Infinite mines of adamant and gold, *[4.280]*  
Valueless stones, and unimagined gems,  
And caverns on crystalline columns poised  
With vegetable silver overspread;  
Wells of unfathomed fire, and water springs  
Whence the great sea, even as a child is fed, *[4.285]*  
Whose vapours clothe earth's monarch mountain-tops  
With kingly, ermine snow. The beams flash on  
And make appear the melancholy ruins  
Of cancelled cycles; anchors, beaks of ships;  
Planks turned to marble; quivers, helms, and spears, *[4.290]*  
And gorgon-headed targes, and the wheels  
Of scythèd chariots, and the emblazonry  
Of trophies, standards, and armorial beasts,  
Round which death laughed, sepulchred emblems  
Of dead destruction, ruin within ruin! *[4.295]*  
The wrecks beside of many a city vast,  
Whose population which the earth grew over  
Was mortal, but not human; see, they lie,  
Their monstrous works, and uncouth skeletons,  
Their statues, homes and fanes; prodigious shapes *[4.300]*  
Huddled in gray annihilation, split,  
Jammed in the hard, black deep; and over these,  
The anatomies of unknown wingèd things,  
And fishes which were isles of living scale,  
And serpents, bony chains, twisted around *[4.305]*  
The iron crags, or within heaps of dust  
To which the tortuous strength of their last pangs  
Had crushed the iron crags; and over these  
The jaggèd alligator, and the might  
Of earth-convulsing behemoth, which once *[4.310]*  
Were monarch beasts, and on the slimy shores,  
And weed-overgrown continents of earth,  
Increased and multiplied like summer worms  
On an abandoned corpse, till the blue globe  
Wrapped deluge round it like a cloak, and they *[4.315]*  
Yelled, gasped, and were abolished; or some God  
Whose throne was in a comet, passed, and cried,  
&quot;Be not!&quot; And like my words they were no more.

**The Earth.**
  
   The joy, the triumph, the delight, the madness!  
   The boundless, overflowing, bursting gladness, *[4.320]*  
The vaporous exultation not to be confined!  
   Ha! ha! the animation of delight  
   Which wraps me, like an atmosphere of light,  
And bears me as a cloud is borne by its own wind.

**The Moon.**
  
   Brother mine, calm wanderer, *[4.325]*  
   Happy globe of land and air,  
Some Spirit is darted like a beam from thee,  
   Which penetrates my frozen frame,  
   And passes with the warmth of flame,  
With love, and odour, and deep melody *[4.330]*  
   Through me, through me!

**The Earth.**
  
   Ha! ha! the caverns of my hollow mountains,  
   My cloven fire-crags, sound-exulting fountains  
Laugh with a vast and inextinguishable laughter.  
   The oceans, and the deserts, and the abysses, *[4.335]*  
   And the deep air's unmeasured wildernesses,  
Answer from all their clouds and billows, echoing after.  
  
   They cry aloud as I do. Sceptred curse,  
   Who all our green and azure universe  
Threatenedst to muffle round with black destruction, sending *[4.340]*  
   A solid cloud to rain hot thunderstones,  
   And splinter and knead down my children's bones,  
All I bring forth, to one void mass battering and blending, —  
  
   Until each crag-like tower, and storied column,  
   Palace, and obelisk, and temple solemn, *[4.345]*  
My imperial mountains crowned with cloud, and snow, and fire;  
   My sea-like forests, every blade and blossom  
   Which finds a grave or cradle in my bosom,  
Were stamped by thy strong hate into a lifeless mire:  
  
   How art thou sunk, withdrawn, covered, drunk up *[4.350]*  
   By thirsty nothing, as the brackish cup  
Drained by a desert-troop, a little drop for all;  
   And from beneath, around, within, above,  
   Filling thy void annihilation, love  
Burst in like light on caves cloven by the thunder-ball. *[4.355]*

**The Moon.**
  
   The snow upon my lifeless mountains  
   Is loosened into living fountains,  
My solid oceans flow, and sing, and shine:  
   A spirit from my heart bursts forth,  
   It clothes with unexpected birth *[4.360]*  
My cold bare bosom: Oh! it must be thine  
   On mine, on mine!  
  
   Gazing on thee I feel, I know  
   Green stalks burst forth, and bright flowers grow,  
And living shapes upon my bosom move: *[4.365]*  
   Music is in the sea and air,  
   Wingèd clouds soar here and there,  
Dark with the rain new buds are dreaming of:  
   'Tis love, all love!

**The Earth.**
  
   It interpenetrates my granite mass, *[4.370]*  
   Through tangled roots and trodden clay doth pass  
Into the utmost leaves and delicatest flowers;  
   Upon the winds, among the clouds 'tis spread,  
   It wakes a life in the forgotten dead,  
They breathe a spirit up from their obscurest bowers. *[4.375]*  
  
   And like a storm bursting its cloudy prison  
   With thunder, and with whirlwind, has arisen  
Out of the lampless caves of unimagined being:  
   With earthquake shock and swiftness making shiver  
   Thought's stagnant chaos, unremoved for ever, *[4.380]*  
Till hate, and fear, and pain, light-vanquished shadows, fleeing,  
   Leave Man, who was a many-sided mirror,  
   Which could distort to many a shape of error,  
This true fair world of things, a sea reflecting love;  
   Which over all his kind, as the sun's heaven *[4.385]*  
   Gliding o'er ocean, smooth, serene, and even,  
Darting from starry depths radiance and life, doth move:  
  
   Leave Man, even as a leprous child is left,  
   Who follows a sick beast to some warm cleft  
Of rocks, through which the might of healing springs is poured; *[4.390]*  
   Then when it wanders home with rosy smile,  
   Unconscious, and its mother fears awhile  
It is a spirit, then, weeps on her child restored.  
  
   Man, oh, not men! a chain of linkèd thought,  
   Of love and might to be divided not, *[4.395]*  
Compelling the elements with adamantine stress;  
   As the sun rules, even with a tyrant's gaze,  
   The unquiet republic of the maze  
Of planets, struggling fierce towards heaven's free wilderness.  
  
   Man, one harmonious soul of many a soul, *[4.400]*  
   Whose nature is its own divine control,  
Where all things flow to all, as rivers to the sea;  
   Familiar acts are beautiful through love;  
   Labour, and pain, and grief, in life's green grove  
Sport like tame beasts, none knew how gentle they could be! *[4.405]*  
  
   His will, with all mean passions, bad delights,  
   And selfish cares, its trembling satellites,  
A spirit ill to guide, but mighty to obey,  
   Is as a tempest-wingèd ship, whose helm  
   Love rules, through waves which dare not overwhelm, *[4.410]*  
Forcing life's wildest shores to own its sovereign sway.  
  
   All things confess his strength. Through the cold mass  
   Of marble and of colour his dreams pass;  
Bright threads whence mothers weave the robes their children wear;  
   Language is a perpetual Orphic song, *[4.415]*  
   Which rules with Dædal harmony a throng  
Of thoughts and forms, which else senseless and shapeless were.  
  
   The lightning is his slave; heaven's utmost deep  
   Gives up her stars, and like a flock of sheep  
They pass before his eye, are numbered, and roll on! *[4.420]*  
   The tempest is his steed, he strides the air;  
   And the abyss shouts from her depth laid bare,  
Heaven, hast thou secrets? Man unveils me; I have none.

**The Moon.**
  
   The shadow of white death has passed  
   From my path in heaven at last, *[4.425]*  
A clinging shroud of solid frost and sleep;  
   And through my newly-woven bowers,  
   Wander happy paramours,  
Less mighty, but as mild as those who keep  
   Thy vales more deep. *[4.430]*

**The Earth.**
  
   As the dissolving warmth of dawn may fold  
   A half unfrozen dew-globe, green, and gold,  
And crystalline, till it becomes a wingèd mist,  
   And wanders up the vault of the blue day,  
   Outlives the moon, and on the sun's last ray *[4.435]*  
Hangs o'er the sea, a fleece of fire and amethyst.

**The Moon.**
  
   Thou art folded, thou art lying  
   In the light which is undying  
Of thine own joy, and heaven's smile divine;  
   All suns and constellations shower *[4.440]*  
   On thee a light, a life, a power  
Which doth array thy sphere; thou pourest thine  
   On mine, on mine!

**The Earth.**
  
   I spin beneath my pyramid of night,  
   Which points into the heavens dreaming delight, *[4.445]*  
Murmuring victorious joy in my enchanted sleep;  
   As a youth lulled in love-dreams faintly sighing,  
   Under the shadow of his beauty lying,  
Which round his rest a watch of light and warmth doth keep.

**The Moon.**
  
   As in the soft and sweet eclipse, *[4.450]*  
   When soul meets soul on lovers' lips,  
High hearts are calm, and brightest eyes are dull;  
   So when thy shadow falls on me,  
   Then am I mute and still, by thee  
Covered; of thy love, Orb most beautiful, *[4.455]*  
   Full, oh, too full!  
  
Thou art speeding round the sun  
Brightest world of many a one;  
Green and azure sphere which shinest  
With a light which is divinest *[4.460]*  
Among all the lamps of Heaven  
To whom life and light is given;  
I, thy crystal paramour  
Borne beside thee by a power  
Like the polar Paradise, *[4.465]*  
Magnet-like of lovers' eyes;  
I, a most enamoured maiden  
Whose weak brain is overladen  
With the pleasure of her love,  
Maniac-like around thee move *[4.470]*  
Gazing, an insatiate bride,  
On thy form from every side  
Like a Mænad, round the cup  
Which Agave lifted up  
In the weird Cadmæan forest. *[4.475]*  
Brother, wheresoe'er thou soarest  
I must hurry, whirl and follow  
Through the heavens wide and hollow,  
Sheltered by the warm embrace  
Of thy soul from hungry space, *[4.480]*  
Drinking from thy sense and sight  
Beauty, majesty, and might,  
As a lover or a chameleon  
Grows like what it looks upon,  
As a violet's gentle eye *[4.485]*  
Gazes on the azure sky  
Until its hue grows like what it beholds,  
As a gray and watery mist  
Glows like solid amethyst  
Athwart the western mountain it enfolds, *[4.490]*  
When the sunset sleeps  
   Upon its snow —

**The Earth.**
  
    And the weak day weeps  
       That it should be so.  
Oh, gentle Moon, the voice of thy delight *[4.495]*  
Falls on me like thy clear and tender light  
Soothing the seaman, borne the summer night,  
   Through isles for ever calm;  
Oh, gentle Moon, thy crystal accents pierce  
  The caverns of my pride's deep universe, *[4.500]*  
Charming the tiger joy, whose tramplings fierce  
   Made wounds which need thy balm.

**Panthea.**
  
I rise as from a bath of sparkling water,  
A bath of azure light, among dark rocks,  
Out of the stream of sound.

**Ione.**
  
                Ah me! sweet sister, *[4.505]*  
The stream of sound has ebbed away from us,  
And you pretend to rise out of its wave,  
Because your words fall like the clear, soft dew  
Shaken from a bathing wood-nymph's limbs and hair.

**Panthea.**
  
Peace! peace! A mighty Power, which is as darkness, *[4.510]*  
Is rising out of Earth, and from the sky  
Is showered like night, and from within the air  
Bursts, like eclipse which had been gathered up  
Into the pores of sunlight: the bright visions,  
Wherein the singing spirits rode and shone, *[4.515]*  
Gleam like pale meteors through a watery night.

**Ione.**
  
There is a sense of words upon mine ear.

**Panthea.**
  
An universal sound like words: Oh, list!

**Demogorgon.**
  
Thou, Earth, calm empire of a happy soul,  
  Sphere of divinest shapes and harmonies, *[4.520]*  
Beautiful orb! gathering as thou dost roll  
   The love which paves thy path along the skies:

**The Earth.**
  
I hear: I am as a drop of dew that dies.

**Demogorgon.**
  
Thou, Moon, which gazest on the nightly Earth  
   With wonder, as it gazes upon thee; *[4.525]*  
Whilst each to men, and beasts, and the swift birth  
   Of birds, is beauty, love, calm, harmony:

**The Moon.**
  
I hear: I am a leaf shaken by thee!

**Demogorgon.**
  
Ye Kings of suns and stars, Dæmons and Gods,  
   Aetherial Dominations, who possess *[4.530]*  
Elysian, windless, fortunate abodes  
   Beyond Heaven's constellated wilderness:

**A Voice from above.**
  
Our great Republic hears, we are blest, and bless.

**Demogorgon.**
  
Ye happy Dead, whom beams of brightest verse  
   Are clouds to hide, not colours to portray, *[4.535]*  
Whether your nature is that universe  
Which once ye saw and suffered —

**A Voice from beneath.**
  
                Or as they  
Whom we have left, we change and pass away.

**Demogorgon.**
  
Ye elemental Genii, who have homes  
   From man's high mind even to the central stone *[4.540]*  
Of sullen lead; from heaven's star-fretted domes  
   To the dull weed some sea-worm battens on:

**A confused Voice.**
  
We hear: thy words waken Oblivion.

**Demogorgon.**
  
Spirits, whose homes are flesh: ye beasts and birds,  
   Ye worms, and fish; ye living leaves and buds; *[4.545]*  
Lightning and wind; and ye untameable herds,  
   Meteors and mists, which throng air's solitudes: —

**A Voice.**
  
Thy voice to us is wind among still woods.

**Demogorgon.**
  
Man, who wert once a despot and a slave;  
   A dupe and a deceiver; a decay; *[4.550]*  
A traveller from the cradle to the grave  
   Through the dim night of this immortal day:

**All.**
  
Speak: thy strong words may never pass away.

**Demogorgon.**
  
This is the day, which down the void abysm  
At the Earth-born's spell yawns for Heaven's despotism, *[4.555]*  
   And Conquest is dragged captive through the deep:  
Love, from its awful throne of patient power  
In the wise heart, from the last giddy hour  
   Of dread endurance, from the slippery, steep,  
And narrow verge of crag-like agony, springs *[4.560]*  
And folds over the world its healing wings.  
  
Gentleness, Virtue, Wisdom, and Endurance,  
These are the seals of that most firm assurance  
   Which bars the pit over Destruction's strength;  
And if, with infirm hand, Eternity, *[4.565]*  
Mother of many acts and hours, should free  
   The serpent that would clasp her with his length;  
These are the spells by which to reassume  
An empire o'er the disentangled doom.  
  
To suffer woes which Hope thinks infinite; *[4.570]*  
To forgive wrongs darker than death or night;  
   To defy Power, which seems omnipotent;  
To love, and bear; to hope till Hope creates  
From its own wreck the thing it contemplates;  
   Neither to change, nor falter, nor repent; *[4.575]*  
This, like thy glory, Titan, is to be  
Good, great and joyous, beautiful and free;  
This is alone Life, Joy, Empire, and Victory.
