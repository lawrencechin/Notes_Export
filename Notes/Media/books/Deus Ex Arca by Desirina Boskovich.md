# Deus Ex Arca
Desirina Boskovich

It was a crystalline morning in early June, and the sky was wide as a saucer.
It was a beautiful day for the arrival of the box.

Ferocious rains had come the night before, leaving the air fresh with dew. The Greater Springfield Farmers’ Market was swinging into gear outside the mall. At the far end of the blacktop parking lot, an array of tents and tables had been erected, staffed by vendors from across southwest Missouri.

Mr. and Mrs. Yamamoto were there, with radishes, cucumbers, yellow squash, and salad greens. Their teenage son helped bag the vegetables, and Mrs. Yamamoto’s mother made change. Jeff Finley, of Finley Farms, sold grass-fed beef and unpasteurized milk from a freezer in the back of his Chevy pickup. Miss Amelia offered hand-poured candles, and herbs in plastic pots. A Mennonite family sold fresh produce from their sixteen-acre farm. Their sons wore suspenders and broad-brimmed hats; their daughters wore ankle-length dresses and bonnets sewn from stiff white mesh. There were red potatoes, green tomatoes, fist-sized strawberries, and fresh baked bread.

Jackson Smith, aged seven, also happened to be at the farmers’ market that day. He was there with his parents, and his little sister Emily, aged two. Their father pushed Emily in the stroller; their mother held Jackson’s hand, as they wandered from table to table.

Jackson was in the second grade. His bowl-cut hair was straight and fine, so blond it was almost white. His eyes were round and blue. There was a gap between his two front teeth; his parents assumed the gap would disappear as he grew up, but in fact it never did. He played Little League and collected rocks. He had a special bond with the family cat, Scottie—a fat lazy tom who purred and slobbered when you picked him up.

Jackson waited patiently while his mother talked to Miss Amelia, who loved to talk about her candles.
Then he saw it: the box.
The box was sitting on the asphalt, just to the left of Miss Amelia’s folding table.
Jackson couldn’t remember ever seeing a box like this before. He broke away from his mother’s grasp, and, wiping his palm on his jeans, he went over to inspect the box.
It was about the size of a shoebox. It was matte charcoal in hue, a name-evading shade that hovered indistinctly between black and gray. Jackson squatted in front of the box. He poked it, then laid his palm flat on top. Nothing happened, although his fingers left faintly visible prints of moisture. These quickly evaporated.

He picked it up. It was heavy, but not unexpectedly so.
“Mom, look what I found,” he said, and hoisted the box onto Miss Amelia’s table.
What happened next is hard to describe. Except that, in the instant it was happening, it felt like the most natural thing imaginable.
The table, along with all the candles and herbs carefully arranged upon it, simply disappeared. It didn’t fade. It didn’t crumble. It just popped out of existence. Where there had been a table, there was now something else, and that something else was air.
When the table disappeared, the box sat on the ground. It didn’t fall to the ground. There was no slam, no thud, no clunk. The box sat on the ground as if it had been sitting there all along.

Jackson’s mother screamed. Jackson’s father rushed over, still pushing Emily, who cackled with glee at the bumpy ride.
Miss Amelia gazed in shock at where her table had been. “Well, I never,” she said. She bent over to pick up the box . . .
. . . and turned into a giant celery stalk.
Where there had been Miss Amelia, there was now something else, and that something else was a column of celery, measuring approximately five feet and five inches, its limpid green fronds rustling gently in the breeze.
The box sat beside it.

The Yamamoto’s teenage son, who’d seen the whole thing, rushed over. He joined Jackson and Jackson’s parents, who were staring down at the box. Then, before anyone could stop him, he nudged the box with his foot.
Nothing happened.
He touched it with the tips of his fingers.
Nothing happened.

Mr. Yamamoto ran over, yelling. He grabbed his reckless teenage son by the shoulder and hauled him back toward the safety of their tent, lecturing him in Japanese about the importance of thinking before one acts.
They were almost there when Mr. Yamamoto simply disappeared. His son turned into a toaster.
The box didn’t move.

Mrs. Yamamoto and her mother tottered over to the spot where Mr. Yamamoto and his courageous son had been standing just a moment before. Mrs. Yamamoto began to wail. Her mother shouted warningly at everyone who tried to come near them. The toaster just sat there.
Jackson’s parents backed away a few steps—and then a few steps further. When Jackson finally noticed, he backed away, too.

Jeff Finley came over to see if he could lend a hand. He’d pulled hapless cars out of the mud in his Chevy pickup and he’d helped countless cows through labor. And—though no one knew about this but his wife and his two teenage stepdaughters, certainly not Jackson—he’d even built a survival shelter in his own backyard and stocked it with bottled water, canned tuna, and guns. He was so rightwing he was liberal, and so leftwing he was conservative. Ever since his first wife and only son had died in a car accident eleven years ago, he’d considered himself immune to pain. In short, he assumed he was prepared to deal with any eventuality.
But he’d never imagined anything like the box.
He picked it up, of course; it was impossible to believe that the box had anything to do with the things that were happening at the farmers’ market. In fact, it was impossible to believe those things were happening at all.
The moment he picked it up, he winked out of existence. The box remained on the ground, as it always had.

Jeff appeared a moment later, standing on top of the mall. According to observer measurement, approximately 1.7 seconds had passed since he’d disappeared from the parking lot. But according to Jeff’s measurement, he’d been gone much longer. He’d seen things no human should ever see, perhaps things no human had ever seen. He stumbled over to the edge of the roof.
“Look! On the roof!” someone shouted, from down in the parking lot. Just then, Jeff jumped.
It was not a very big mall. The fall broke his bones, but didn’t kill him—at least not right away.

Meanwhile, pandemonium ruled the parking lot. Shoppers rushed around, screaming and crying. A Mennonite girl sprinted toward the sidewalk, white tennis shoes flashing beneath her dress. A loose dog ran among the cars, barking frantically, trailing a useless leash. Vendors leaped into their cars and sped away, leaving behind their tables and tents without a second thought. Already, a major car accident had jammed the nearest intersection. Something, somewhere, was on fire; smoke billowed toward the sky. The intertwined wail of sirens rose and fell in the distance, and a fire truck’s horn blared like an oncoming train.
Jackson picked up the box and cradled it close to his chest. His father comforted his mother, and his mother comforted Emily, who’d begun to cry.

They didn’t run to the car. They walked. Jackson’s father, holding the stroller. Jackson’s mother, holding Emily. Jackson, walking five paces behind them, holding the box.

They climbed into their station wagon, exercising the utmost calm. Jackson’s father navigated carefully through the chaos of the parking lot, then out into the traffic jam of the street. Jackson’s mother sat beside him, reciting a nursery rhyme that Emily loved. Her breath was jagged, but her voice was soft.
In the backseat, Emily sat in her car seat and cried. Jackson sat next to her, the box on his lap. He wore his seatbelt, even though no one had reminded him. He watched out the window as telephone poles and brick buildings and gas stations flew by.

By the time they had returned home and pulled into their driveway, Jackson’s mother had stopped saying the nursery rhyme, and Emily had stopped crying.
As they got out of the car, Jackson realized something.

Neither of his parents had touched the box. And neither of his parents had touched him.
He walked carefully up the stairs, still holding the box, and placed it gently beneath the bed.
***
They came for the box the next day, as Jackson assumed they would. (He’d seen movies, after all.) A procession of unmarked black SUVs squealed into the cul-de-sac and screeched to a halt outside Jackson’s house. Before the vehicles even stopped, soldiers in black body armor piled out. They’d been briefed to expect the worst.

Jackson’s mother and father opened the door for the soldiers, so they wouldn’t have to kick it down. They stood aside as the soldiers rushed up the stairs.
Jackson stood open-mouthed in the middle of his bedroom, as the shouting soldiers piled in. He pointed silently: underneath the bed.
The first soldier got on his knees and reached underneath. He turned into a bobby pin the size of a trumpet. The second soldier followed his lead. He turned into a yellow toy pickup truck. The third soldier disappeared. The fourth dissolved into a puddle of gray goo. The fifth also disappeared. The sixth became a tuna sandwich.

The seventh fished out the box.
He stood, transfixed, holding the box at arm’s length. His mask obscured his face, making it hard to be sure, but he seemed shocked.
Jackson sat on the bed and crossed his arms.
The seventh soldier handed the box to an eighth soldier, who promptly exploded: a fountain of blood, guts, and brain matter, misting across the room like hairspray. The seventh soldier let out a strangled scream, and picked up the box again. He carried the box down the stairs, flanked by the soldiers who’d survived.
After they left, the house was deadly quiet. In fact, the entire neighborhood fell into a soundless stupor, a stillness it hadn’t known in years.

Jackson sat alone in his room and played with the yellow truck, pushing it across the rug. A few hours passed. He curled up on the bed and fell into a dreamless sleep until he woke to the sounds of his family eating dinner downstairs without him.
He took a bite of the tuna sandwich, but it tasted bland, and the crust was stale.
He missed the box.
***
The black SUVs returned a week later. This time, they were coming for Jackson.
They brought a man with them. The man was reedy and balding; he wore a pink collared shirt and a turquoise striped tie. He looked like a math teacher, but he was actually a psychologist. His job was to explain things to Jackson’s parents.

In soothing tones, he clarified that this was nothing less than a matter of national security. The box was incredibly powerful, with untold applications. The box could wipe out entire foreign armies, but it was impossible to control. And so far, Jackson was the only one who’d been able to reliably interact with the box without suffering any ill effects.
The phenomenon was astonishing and inexplicable, but their scientists would get to the bottom of it. In a way, it was for Jackson’s own good.

Of course, if Jackson’s parents weren’t amenable to reason, there was always the army.
Jackson’s mother cried hysterically, clawing her face and dragging her fingers through her hair. But Jackson’s father appeared surprisingly stoic. He said, “That’s just the way it is. That’s just the way it has to be.” He said it again and again.
The man who looked like a math teacher climbed the stairs to collect Jackson, followed by the soldiers in body armor. Jackson’s father restrained Jackson’s mother. He held onto her even when Jackson emerged at the bottom of the stairs, escorted by his entourage. Jackson’s mother fought Jackson’s father, biting him and scratching him, calling him awful names that Jackson had never heard before, not even on television.

Jackson stood awkwardly to the side, next to the man who looked like a math teacher, flanked by the soldiers in body armor.
His mother broke away from his father’s grasp, and dashed over to Jackson.
“No!” his father shouted.
So Jackson knew for sure what he’d suspected ever since the day in the station wagon.
His mother threw her arms around him, touched his face, mussed his hair, kissed his forehead, and rubbed her snotty tears all over his neck. “I’m sorry for being such a terrible mommy,” she hiccupped.

The soldiers yanked her away, and the man who looked like a math teacher pulled Jackson through the house, toward the front door.
Jackson looked over his shoulder all the way to the car. They stuffed him into a black SUV that looked like all the others. As they pulled away from the curb, his mother stood in the front yard, sobbing. Other than the hysterical crying, she seemed fine.
Later that week, the man who looked like a math teacher was transformed into a bottle of hydrogen peroxide.

***

They took Jackson to a strange place full of cramped rooms with stainless steel tables and glaring white walls. They tried to run tests on the box, but it was dangerous, so they ran tests on Jackson instead. They pricked his finger. They took samples from his skin and hair and stools. They gave him shots, with long glistening needles thin as thread, liquid glistening at their tips. They strapped him down so he couldn’t move, and put him inside an MRI machine for hours at a time. It was terrible inside the machine; he closed his eyes and imagined that he was inside a spaceship instead, on his way to a distant star. They gave him new shots and did it again. They attached electrodes to his skull; they wrote down number after number. No one talked to him except the psychologist. (Not the same one who’d become a bottle of hydrogen peroxide.) The psychologist interrogated him for hours, trying to make him recount everything he could remember since birth.

They watched Jackson from the other side of mirrored glass and made him touch the box.
He lost weight. He couldn’t sleep. His head hurt all the time. Sometimes at night, he would lay awake and cry. He hated this place so much, and he missed his old house, where the stairs creaked in just the right places, the closets were cozy and smelled like cedar, and the wall-mounted gas heater provided the perfect place to curl up in the winter, especially in the mornings before school, especially while eating cereal. Most of all, Jackson missed his mother. He missed his father. He missed Emily. He missed Scottie, especially the way he used to jump in bed with Jackson and purr like an engine while he licked his paws, making sure to get in between the toes.

He knew the researchers were watching him all the time. He’d seen movies, after all. But no one ever came when he cried.
All he had was the box.
He told the box about Emily, who he’d hated at first because she was a baby and everyone loved her more, but whom he now loved with all his heart. He was her older brother, and she depended on him. That’s what their parents said, anyways. He told the box about his rock collection, which already embarrassed him; he was ready to move on to a more exciting collection, like beetles or bottle caps.
The box never said anything, of course. Jackson didn’t expect it to. But talking to it still made him feel better.

In a way, he began to feel like he understood it.
He made the mistake of saying this to the latest psychologist. (They were on iteration number four by this time.) She pounced like a hawk. “What do you mean? When you say you understand the box, are you saying you feel like you might be able to explain it? Unlock it? What exactly have you come to understand about the box?”
She brought in others. They crowded around Jackson, in front of the mirror that he knew was actually a window. There were more psychologists on the other side, taking notes. They questioned him every day for weeks. Jackson was older now, but he still couldn’t find the words for what he wanted to say. He didn’t understand what the box did, or how it did it, or why. He didn’t know how to control it, or how to make it stop. He didn’t even know how to open it; as far as he could tell, it was perfectly seamless.

It was more like he understood how the box felt.
They kept Jackson in that place for five years. Then they let him go.
But they kept the box.

***

During his years at the research facility, Jackson had changed. He’d gotten taller, and his hair had grown darker, and he was becoming strong. But he still had that gap between his two front teeth.

His family had changed, too. His mother’s strawberry blonde hair had gone white. His father had gone gray. His parents rarely spoke to each other, or anyone else. They spent long minutes staring into space. They fought strange fights, like chess games played by mail; move and countermove took hours. The house would be filled with silent tension, while one of them worked up the energy to make a cruel remark. Sometime later, the other would rouse themselves and angrily respond. Their arguments took days.

Jackson’s sister Emily was seven now, and as pretty as an angel. She talked constantly and laughed at everything. She didn’t seem to notice their parents’ fights. The made-up Emily that Jackson kept in his head while he was at the facility turned into the actual Emily, and he loved her even more than he’d loved the pretend one. She’d forgotten Jackson while he was away, but now she was crazy about him. She followed him everywhere, up the stairs, down the stairs, into the kitchen, out to the backyard, chattering the whole time. She demanded he carry his bowl of cereal out to the bus stop with her in the morning, and wait with her until the school bus came; he waved with his right hand and held his Wheaties with his left, until the bus disappeared around the corner. Every night, Emily made him sit next to her bed until she fell asleep.

Scottie was old now, but he hadn’t changed much, except that he was fatter than ever.
Jackson didn’t go to school. He was too far behind. He asked his parents once if he should try to catch up; they just shrugged. So he stayed at home all day, left to his own devices.

He decided to build a tree house for Emily in the backyard. He made his mother drive him to Home Depot and help him buy the lumber. He asked his father if he could use power tools; his father said, “Maybe.” That was good enough. He got to work sawing, sanding and hammering; Emily helped in the afternoons, and no one stopped them. The tree house went up in a week. After that, Jackson lay in the tree house all day, reading dusty novels and waiting for Emily to get home from school.
One day, the box showed up instead.
Jackson was stretched out on the knotty pine floor of the tree house, surrounded by old pillows and scratchy blankets, when he felt: something. A tickle, a shadow, a foreboding.
It was the box. It sat there as if it had sat there all along.
Jackson sat up, folded his knees beneath his interlocked arms, and waited for something to happen.
Nothing did.

“Hello,” he said experimentally, remembering all those conversations they’d had—he and the box—back when he was in that room surrounded by mirrored glass. Until this moment, those awful years had seemed like a bad dream.
The box didn’t answer, as it never had.
Jackson tucked the box beneath the crook of his left arm and climbed down the ladder. He let himself in the back door without making a sound, and walked past his mother, who was sitting at the table staring into a cold cup of coffee. He tiptoed up the stairs and tucked the box beneath his bed. Then he went back outside, walking past his mother for the second time. She never looked up.
Back in the tree house, he sat cross-legged and waited patiently for the groan and fart of Emily’s school bus.

***

The soldiers came for the box, as he’d expected they would. They took it away, and he sighed as he watched them go, not knowing if the feeling in his chest was relief or dread. His parents never even asked why the special forces were back. But Emily did.
He told her it was too complicated to explain.
“Mom and Dad told everyone that you were dead,” she said.
“Maybe I was.”
“I don’t like that,” she said. “Don’t say it.”
“Okay.”
The box returned. This time it popped into being on his dresser. The soldiers came and retrieved it, but the box returned again. This time it appeared on top of the refrigerator. Jackson’s mother had a panic attack, and had to be sedated; his father dialed a phone number, and the soldiers came, but the box returned yet again. This time it materialized on the breakfast table. Emily was sitting there, eating Nutella on toast. She laughed.
“Don’t ever touch that box,” Jackson told her. He took both her hands into his own, and gazed into her blue eyes with all the force he could muster. “Do you understand? Never touch the box.”
“Jeez, I know,” she said. “I’m not going to touch the box.”
Then the soldiers came, but the box kept coming. One time it arrived in the bottom of Jackson’s closet. He didn’t even know it was there until the black SUVs arrived and the soldiers rushed up the stairs, the robots bumbling slowly behind them. (By this time, they’d developed a mechanized transportation strategy, to minimize carnage).

One day, Jackson noticed that every other house on their block was empty. The roofs were caving in, the windows were broken, the doors were boarded up, and the grass was knee high. He didn’t know if the people who lived in those houses had died, or if they’d all just moved away. It was no longer a very desirable neighborhood.
Later, he looked more closely and realized that two of the houses across the street had become the skeletons of dead dinosaurs. A third was made out of gingerbread. A fourth hovered two feet above the ground. A fifth was only there on Wednesdays.

***

One night, Emily came to Jackson’s room. She was nine now, and she wasn’t afraid of the dark anymore. In fact, she wasn’t afraid of anything. But tonight, she seemed worried.
Jackson lay under the comforter, reading a novel. Emily sat on the bed, her back to the wall, and stretched her long skinny legs across the blanket, draping them over Jackson’s legs so he couldn’t move.
The box sat on top of his dresser; it sat there all the time now. They’d given up on coming back for it. Or maybe there was no one left tocome back.
“Jackson?” Emily said. She was picking her cuticles.
“Yes.”
“Will you always protect me from your box?”
“It’s not my box,” he said. But he glanced guiltily toward it.
“Then why does it always come back to you?”
“I don’t know. It doesn’t belong to me. It just found me.”
“Then it’s yours.”
“No. More like I’m its.”
“But you can keep me safe.”
“Of course I can,” he said.
“And what about everyone else?”
“What about them?”
“Why didn’t you keep them safe, too?”
“Because I don’t have that power.”
“So what powers do you have?”
Jackson gazed at Emily, wondering if he could ever know what she really felt about him. Was he a failed superhero, or a sympathetic villain? Maybe it didn’t matter. “I don’t know my powers yet, Emily,” he said. “But I will always keep you safe. I promise.”
“Okay,” she said. She stopped chewing her bottom lip. She fell asleep on the floor next to his bed. Her white-blonde hair was turning brown, like his.

***

The next week, Scottie disappeared. No one saw him go.
Maybe he dematerialized. Maybe he ran away. Maybe he just crawled under the shed and died of old age.
Emily cried and cried, but Jackson was the saddest; he didn’t tell Emily that Scottie had always loved him more, even though it was true.
The next week, part of their mother disappeared.
The other part remained at the kitchen table, where she’d been sitting for weeks now. This part was translucent and two-dimensional, and shimmered into nothing when Jackson and Emily tried to talk to her. Sometimes she looked almost solid, but she no longer spoke.
“I’m sorry,” Jackson told the ghost that was part of his mother. “This is all my fault. I should never have touched that box.”
For a moment, he thought she was looking at him, but then she blinked and flickered into nothingness. He waited for a while, but she didn’t come back.

After that, Jackson and Emily began spending all their time in the tree house. It felt safer there. They talked about everything; both felt old beyond their years.
“Time doesn’t really exist,” Jackson said. “I figured this out when I was in the place. They said I was there for five years, but I know it wasn’t five years. It might have been a month, and it might have been twenty years, but I know it wasn’t five.”
“But I was five years older when you came back.”
“That stuff is just on the outside.”
Another time, Emily asked him: “What do you think everything is actually made out of?”
“What do you mean?”
“Well, none of this is real, right? It’s made out of something else, like Legos, or a dream.”
“I guess it could be real.” It didn’t seem likely.

Some time after that, they heard their father screaming. They peeked out the door of the tree house. Their father was standing in the backyard, and he was howling. His voice proclaimed a level of anguish that most people never feel in their lives. His noiseless syllables spoke of mental agony deeper than he’d ever imagined possible. Finally unconstrained, he released the pain in its raging totality.

One of his arms was missing. Blood spurted from the empty socket. It shot out pressurized, like water from a fire hose. He ran in frenzied circles, spraying the backyard with blood. He kept on screaming. Then his other arm disappeared. Then a leg. He hopped around the backyard on his one remaining limb, blood spewing from three orifices, still screaming. Then his head disappeared. A moment later, the rest followed.
Some time after that, the house went away too.
The box went with it. But it showed up in the tree house two days later.

***

It was crowded in the tree house, especially because Emily took care not to get too close to the box. But at least they had a place.
Emily didn’t go to school anymore. The school had become something else.
Mostly they just sat in the tree house and talked.
“Remember when you asked me what everything is made of?” Jackson said.
“Yeah. Did you figure it out?”
“I think so. Everything is everything. Like, everything is made out of everything else. So it doesn’t matter what things are. Because they’re all the same thing.”
“So the things that disappeared are still here, really?”
“Yeah. They just became part of something else.” Jackson knew that when she said “things,” she didn’t really mean “things.”
The neighborhood had changed. Everything in it had turned into something else. But there was a store two miles away that still worked. It was a long walk, but it was too dangerous to drive: The streets were full of strange creatures and oversized objects and vast crevices where things had disappeared. And the car could always turn into something else, too. A lot of cars had.

So Jackson set out for supplies on foot, carrying a backpack. No one used money anymore; it was too weird. They just asked for things. Sometimes they appeared. Sometimes they didn’t.
This time, they did. Jackson filled the backpack with chips and cheese and candy bars, and hiked back home. Lately he’d been feeling these overwhelming waves of déjà vu. He felt it now. Which was odd, because he was sure he had never encountered a three-legged bear that hopped down the street like a frog, or a box fan the size of a house.

As he climbed the ladder to the tree house, he called out: “Hello? I’m back. Emily? I’m back.”
But there was no one in the tree house. Except the box, of course.
Then he noticed the doll. It lay on the stack of ratty blankets that Jackson and Emily had made into a makeshift bed. He picked up the doll. He’d never seen it before.
The doll had blue eyes and light brown hair. It looked like Emily.
Jackson flung the doll out of the tree house. It landed in the backyard with a thud. He climbed down after it and kicked it across the yard. He ran around in crazed circles, crying in agony the way his father had done once before.
“I told you not to touch the box!” he screamed. “I told you not to touch it! You promised!”
He threw his face to the sky and howled. He beat the trunk of the tree that held the tree house until his knuckles bled. He screamed all the curse words he could remember, the way his mother had done when they first took him away.
But he knew he was being unfair. Maybe Emily hadn’t touched the box. Maybe touching the box didn’t really matter; lots of things in the world had never touched the box, and the box didn’t seem to care.
Maybe she’d just run away.

***

He waited in the tree house for a long time. It might have been a month. It might have been five years. It might have been twenty.
But Emily never came back.
There wasn’t anyone else anymore. And time seemed to be turning itself inside out.
He antagonized the box, trying to make something happen. He kicked it. He hit it. He dropped it. He slammed it against concrete. He rested his palm flat against the top and meditated for what felt like hours.
Nothing happened.
Nothing at all.

***

Then one day the tree house became the MRI machine. Jackson was lying inside it. He was tied down, and he couldn’t move.
He closed his eyes as tightly as he could, counted to ten, and opened them again. Still here.

Had he been in the machine the whole time?
They pulled him out.
But they weren’t the same they. They’d transcended themselves. And so had he, he discovered as he looked down at what was now himself.
They were un-things in un-time. They existed on the razor sharp edge of the present, which is just as difficult as it sounds. But it wasn’t like that, either, really.
They released him. They hovered around him, trapped in the mirrored glass. The panel of psychologists all over again, as if like they’d always been.
“We must admit, we’re confused,” they said.
“You’re confused?” (Was Jackson speaking? He was, but it wasn’t like that either, really.)
“Why didn’t your world like our gift?”
“Because it killed everyone?”
“That’s not our fault. We put the secrets to the universe in that thing. It was all in there! Everything any advanced civilization has ever wanted to know. Time travel. Teleportation. Immortality. Alchemy. ESP. This was the greatest gift of all time! You always talk about wanting to meet that guy, God? Well, here we are. And you don’t just get to meet us. You could be us. All you had to do was reverse-engineer one little box.”
“Why couldn’t you just tell us about what was in the box? You know, like, explain it to us in words?”
“We can’t use words,” they said, glaring at him with pity and scorn.
“You’re using them right now.”
“Not in the least,” they said, and he saw that they were right. Whatever words they’d used were the ones he’d given them to tell a story he already knew.
“The box was so simple it transcended language. The box was as elegant as we could possibly make it. The box—well, the box was perfect.”
“You ruined my life,” Jackson said. “And the entire world. But I guess I don’t take that quite as personally.”
They talked amongst their un-selves in their not-words.
“We’re going to try again,” they said. “That last time really must have been a fluke. We mean, come on! It was just one little box. Someone on your planet has to be able to figure it out. Or maybe we just need to test it further.”
“Please, whatever you do, do not test it further.”
“We’ll get it right! We mean, you’ll get it right.”
“Am I dreaming?” he asked. He’d begun to think he was.
“Of course,” they said. And they gave him a shot, strapped him down, and put him back into the machine.

He closed his eyes and dreamed he was a box. A box about the size of a shoebox, with a matte charcoal hue that hovered indistinctly between black and gray; perfectly seamless despite the fact that it contained multitudes.
A tow-headed little boy peered down at him.
It was a crystalline morning in early June, and the sky was wide as a saucer.
[end]