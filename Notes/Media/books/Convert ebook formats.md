# Convert ebook formats

You will need **Calibre** for the command-line tools. You cannot simply take the *exec* files out of the app as there is a dependancy issue. No matter. Store **Calibre** somewhere - perhaps on the external drive if you so wish. 

To convert a single ebook one would navigate to the **Calibre** app, explore the package and find *ebook-convert* in the *MacOs* folder. Drag this into the terminal followed by the ebook. Specify the output including the file extension which will determine the format. Done.

To convert multiple ebooks at once you will find it’s a tad bit more involved but really quite simple. Use the following code-snippet as a guide:

`for book in \*.epub; do echo "Converting $book"; ebook-convert "$book" "$(basename "$book" .epub).mobi"; done`

Navigate to the folder containing the ebooks. In place of ‘*ebook-convert*’ drag in the file from the **Calibre** app and change the file extensions to whatever makes sense. The program will proceed to convert all books specified.

