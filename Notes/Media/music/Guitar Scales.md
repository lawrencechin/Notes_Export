# Guitar Scales
## The Minor Pentatonic Scale
### Root on 6th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  •  |  _  |  _  |  °  |  _  |
|  °  |  _  |  _  |  °  |  _  |
|  °  |  _  |  °  |  _  |  _  |
|  °  |  _  |  •  |  _  |  _  |
|  °  |  _  |  °  |  _  |  _  |
|  •  |  _  |  _  |  °  |  _  |

### Root on 5th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  °  |  _  |  _  |  °  |  _  |
|  _  |  °  |  _  |  °  |  _  |
|  °  |  _  |  •  |  _  |  _  |
|  °  |  _  |  °  |  _  |  _  |
|  •  |  _  |  _  |  °  |  _  |
|  °  |  _  |  _  |  °  |  _  |

### Intervals

Tone & Half - Tone - Tone - Tone & Half

### Chords From the Scale
 
## The Blues Scale
### Root on 6th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  •  |  _  |  _  |  °  |  _  |
|  °  |  _  |  _  |  °  |  _  |
|  °  |  _  |  °  |  _  |  _  |
|  °  |  _  |  •  |  _  |  _  |
|  °  |  °  |  °  |  _  |  _  |
|  •  |  _  |  _  |  °  |  _  |

### Root on 5th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  °  |  _  |  _  |  °  |  _  |
|  _  |  °  |  _  |  °  |  °  |
|  °  |  _  |  •  |  _  |  _  |
|  °  |  °  |  °  |  _  |  _  |
|  •  |  _  |  _  |  °  |  _  |
|  °  |  _  |  _  |  °  |  _  |

### Intervals

Tone & Half - Tone - Semi - Semi - Tone - Tone & Half

### Chords From the Scale

## The Natural Minor Scale (*Aeolian Mode*)
### Root on 6th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  _  |  •  |  _  |  °  |  °  |
|  _  |  °  |  °  |  _  |  °  |
|  °  |  °  |  _  |  °  |  _  |
|  _  |  °  |  _  |  •  |  _  |
|  _  |  °  |  _  |  °  |  °  |
|  _  |  •  |  _  |  °  |  °  |

### Root on 5th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  °  |  °  |  _  |  °  |  _  |
|  °  |  °  |  _  |  °  |  _  |
|  °  |  _  |  •  |  _  |  _  |
|  °  |  _  |  °  |  °  |  _  |
|  •  |  _  |  °  |  °  |  _  |
|  °  |  °  |  _  |  °  |  _  |

### Intervals

Tone - Semi - Semi - Tone - Semi - Semi - Tone

### Chords From the Scale

## The Major Scale
### Root on 6th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  °  |  •  |  _  |  °  |  _  |
|  _  |  °  |  _  |  °  |  _  |
|  °  |  _  |  °  |  °  |  _  |
|  °  |  _  |  °  |  •  |  _  |
|  °  |  °  |  _  |  °  |  _  |
|  °  |  •  |  _  |  °  |  _  |

### Root on 5th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  _  |  °  |  _  |  °  |  _  |
|  _  |  °  |  _  |  °  |  °  |
|  °  |  _  |  °  |  •  |  _  |
|  °  |  °  |  _  |  °  |  _  |
|  °  |  •  |  _  |  °  |  _  |
|  _  |  °  |  _  |  °  |  _  |

### Intervals

Tone - Tone - Semi - Tone - Tone - Tone

### Chords From the Scale

## The Dorian Mode
### Root on 6th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  _  |  •  |  _  |  °  |  °  |
|  _  |  °  |  _  |  °  |  °  |
|  °  |  °  |  _  |  °  |  _  |
|  °  |  °  |  _  |  •  |  _  |
|  _  |  °  |  _  |  °  |  _  |
|  _  |  •  |  _  |  °  |  °  |

### Root on 5th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  _  |  °  |  _  |  °  |  °  |
|  _  |  °  |  °  |  _  |  °  |
|  °  |  °  |  _  |  •  |  _  |
|  _  |  °  |  _  |  °  |  _  |
|  _  |  •  |  _  |  °  |  °  |
|  _  |  °  |  _  |  °  |  °  |

### Intervals

Tone - Semi - Tone - Tone - Semi - Tone

### Chords From the Scale

## The Mixolydian Mode
### Root on 6th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  _  |  •  |  _  |  °  |  _  |
|  _  |  °  |  _  |  °  |  °  |
|  °  |  _  |  °  |  °  |  _  |
|  °  |  °  |  _  |  •  |  _  |
|  °  |  °  |  _  |  °  |  _  |
|  _  |  •  |  _  |  °  |  _  |

### Root on 5th String

|     |     |     |     |     |
| :-: | :-: | :-: | :-: | :-: |
|  _  |  °  |  _  |  °  |  °  |
|  _  |  °  |  _  |  °  |  °  |
|  °  |  °  |  _  |  •  |  _  |
|  °  |  °  |  _  |  °  |  _  |
|  _  |  •  |  _  |  °  |  _  |
|  _  |  °  |  _  |  °  |  °  |

### Intervals

Tone - Tone - Semi - Tone - Tone - Semi 

### Chords From the Scale

<script>
	const notes = [ 
    "c", "c#", "d", "d#", "e", "f", "f#", "g", "g#", "a", "a#", "b"
];

// 2 = tone, 1 = semi tone, 3 = tone & semi
const scales = {
    blues : [ 3, 2, 1, 1, 3, 2 ], 
    minor_pentatonic : [ 3, 2, 2, 3, 2 ],
    aeolian : [ 2, 1 ],
    major : [ 2, 2, 1, 2, 2, 2, 1 ],
    mixolydian : [],
    dorian : []
};

function generateScale( scale, root ){
	let start = notes.indexOf( root );
	if( scales.hasOwnProperty( scale ) && start > -1 ){
		// root = "d"
		// scale = "blues"
		// start = 2
		// scale = ( 0 ), 3, 2, 1, 1, 3, 2
		// scale distance = 2, 5, 7, 8, 9, 12, 14 
		// scale = d, f, g, g#, a, c, d
		// everything after the "a" is at the start of the array
		// array length = 12
		// anything thing greater than 11 needs have 12 deducted from it
		// 12 - 12 = 0 ("c"), 14 - 12 = 2 ("d")
		// use reduce or map to produce a new array of notes
	} else {
		// error
	}	
}
</script>