# Things That May Interest

For **Hype Machines** look back of 2017 see [here](https://hypem.com/zeitgeist/2017/artists). Explore the wonders…

Or perhaps **10x17** features the earthly delights you [seek](http://10x17.co).

Add any album or track the lists below or do whatever you want with them.

## Albums of Interest

<div style="column-count : 3">
* Bonnie "Prince" Billy - I Made a Place
* Black Marble - Bigger Than Life
* Teebs - Anicca
* White Reaper - You Deserve Love
* Sorry Girls - Deborah
* Floating Points - Crush
* Vagabon - Vagbon
* Screaming Females - Singles Too
* Ernest Hood - Neighbourhoods
* Kim Gordan - No Home Record
* The Hecks - My Star
* Chromatics - Closer to Grey
* Pelada - Movimiento Para Cambio
* Lightning Bolt - Sonic Citadel
* Gong Gong Gong - Phantom Rhythm
* Wilco - Ode to Joy
* Micheal Vincent Waller - Moments
* The New Pornographers - In the Morse Code of Brake Lights
* Chastity Belt - Chastity Belt
* Temples - Hot Motion
* Vivian Girls - Memory
* Dylan Moon - Only the Blues
* Surf Curse - Heaven Surrounds You
* M83 - DSVII
* Darkthrone -  A Blaze in the Northern Sky
* Thurston Moore - Spirit Counsel
* Blacks' Myths - Blacks' Myths II II 
* Bonnie Prince Billy - When We Are Inhuman
* Frankie Cosmos - Close It Quietly
* Ezra Furman - Twelve Nudes
* Squid - Town Centre
* Black Belt Eagle Scout - At the Party with My Brown Friends
* Bat for Lashes - Lost Girls
* De La Noche - Blue Days, Black Nights
* Sleater-Kinney - The Centre Won't Hold
* Friendly Fires - Inflorescent
* Loscil - Equivalents
* Dry Cleaning - Sweet Princess EP
* Oso Oso - Basking in the Glow
* Jade Heart - Melt Away
* Purple Pilgrims - Perfumed Earth
* The Soft Cavalry - The Soft Calvary
* Bon Iver - I,I
* Strange Ranger - Remembering the Rockets
* De Lorians - De Lorians
* Lisel - Angels on the Slope
* Lingue Ignota - Caligula
* Jeff Mills - Moon: The Area of Influence
* The Flaming Lips - King's Mouth
* Mal Blum - Pity Boy
* Ada Lea - What we say in private
* Lolina - Who is Experimental Music?
* Prince Daddy & The Hyene - Cosmic Thrill Seekers
* Boody Krlic - Midsommar
* Daughter of Swords - Dawnbreaker
* Felicia Atkinson - The Flower and the Vessel
* Jesca Hoop - Stonechild
* Oren Ambarchi - Simian Angel
* Sofia Bolt - Waves
* Nathan Bajar - Playroom
* Bedouine - Bird Songs of a Killjoy
* Keiji Haino & Charles Hayward - A Loss Permitted
* 75 Dollar Bill - I was Real
* Black Midi - Schlagenheim 
* Holy Ghost! - Work
* Mannequin Pussy - Patience
* French Vanilla - How am I not Myself?
* Bad Books - III
* Titus Andronicus - An Obelisk
* Claire Cronin - Big Dread Moon
* Ellen Arkbro - CHORDS
* Calexico and Iron & Wine - Years to Burn
* Leif - Dream
* Bill Calahan - Shepard in a Sheepskin Vest
* Earthen Sea - Grass and Trees
* Stef Chura - Midnight
* Palehound - Black Friday
* Erin Durant - Islands
* Ryuichi Sakamoto - Black Mirror Smithereens
* Sarah Davavhi - Pale Bloom
* Lust for Youth - Lust for Youth
* Sacred Paws - Run Around the Sun
* Eluvium - Pianoworks
* Damon Locks / Blacks Monument Ensemble - Where Future Unfolds
* Pip Bloom - Boat
* Kyle Bobby Dunn - From Here to Eternity
* Emily A.Sprague -  Water Memory / Mount Wilson
* ionnalee - REMEMBER THE FUTURE
* Earth - Full Upon Her Burning Lips
* Faye Webster - Atlanta Millionaires Club
* Pronoun - I'll Show You Stronger
* Sebadoh - Act Surprised
* Cate le Bon - Reward
* Matias Aguayo - Support Alien Invasion
* Nadia Tehran - Dozakh All Lovers Hell
* Hayden Thorpe - Diviner
* Mavis Staples - We Get By
* Amyl and the Sniffers - Amyl and the Sniffers
* Josephine Wiggs - We Fall
* Caterina Barbieri - Ecstatic Computation
* Operators - Radiant Dawn
* M. Geddes Gengras - I am the Last of that Green and Warm-Hued World
* Olden Yoke - Living Treasure
* Pacific Breeze - Japanese City Pop, AOR & Boogie 1976-1986
* Kelly Moran - Origin EP
* I Love Your Lifestyle - The Movie
* Rhye - Spirit
* Benjamin Lew - Le personnage principal est un peuple isolé
* Ryan Pollie - Ryan Pollie
* Boogarins - Sombrou Dúvida
* The Ballet - Matchy Matchy
* The National - I am Easy to Find
* Radiator Hospital - Sings Music for Daydreaming 
* Dehd - Water
* Holly Herndon - PROTO
* Lydia Ainsworth - Phantom Forest
* Clinic - Wheeltappers and Shunters
* Young Enough
* Guitar Wolf - Love & Jett
* Efrim Manuel Menuck & Kevin Doria - are SING SNACK, SING
* Mac DeMarco - Here Comes the Cowboy
* Filthy Friends - Emerald Valley
* Tim Hecker - Anoyo
* ALASKALASKA - The Dots
* Rhiannon Giddens - there is no Other
* Empath - Active Listening, Night on Earth
* Kedr Livanskiy - Your Need
* Drahla - Useless Coordinates
* Big Thief - U.F.O.F
* Tacocat - This Mess is a Place
* Pure Bathing Culture - Night Pass
* Joel Ross - KingMaker
* Local Natives - Violet Street
* Judy and the Jerks - Music for Donuts EP
* Rodrigo y Gabriela - Mettavolution
* Jackie Mendoza - LuvHz
* Club Night - What Life
* DJ Nate - Take Off Mode
* The Mountain Goats - In League with Dragons
* Marina - Love + Fear
* Otoboke Beaver - Itekoma Hits
* Picastro - Exit 
* ShunGu - A Black Market Album
* Intellexual - Intellexual
* SOAK - Grim Town
* Wand - Laughing Matter
* Possible Humans - Everybody Split
* Joshua Abrams & Natural Information Society - Mandatory Reality
* Haunted Items
* The Mekons - Deserted
* USA/Mexico - Matamoros
* ATAXIA
* RAP - EXPORT
* Hand Habits - Wildly Idle (*Humble Buore The Void*)
* Nite Jewel -  Real High
* DillanPonders - No Mans Land
* Sonder - Into
* White Reaper - The Worlds Best American Band
* Daphni - Joli Mai
* Hoops - Tapes #1-3
* Juana Molina - Halo
* Diet Cig - Swear I'm Good at This
* Francobollo - Long Live Life
* Swet Shop Boys - Sufi La
* Sampha - Process
* Trouble - Snake Eyes
* RF Shannon - Jaguar Palace
* Kamaiyah - Before I Wake
* Cornelius - Mellow Waves
* Walter Martin - My Kinda Music
</div>

## Songs

* Frankie Rose - Red Museum
* Pains of Being Pure at Heart - Kelly
* Lemonheads - Can't Forget
* Big Thief - Strange
* Foxwarren - To Be
* Caged Animals - These Dark Times
* Tristen - Dream Within a Dream

