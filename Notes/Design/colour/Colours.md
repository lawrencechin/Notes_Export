<style>
    img{    
        float : left;
        margin-right : 2em; 
        margin-bottom : 1em;
    }
    h2{ clear : left }
</style>

# Terminal Colour Schemes
A list of **Terminal** colour schemes. Consider this a backup of *hex* codes that makeup the schemes and will allow you to recreate schemes that might get lost in one way or another.

## Wild Cherry - Dark
![](./@imgs/Wild_Cherry_Dark.jpg)
I really like this purple/cherry theme but the magenta colour it currently uses is too close to the text colour. A quick bash listing the files of the home directory shows that symlinks are displayed in magenta and this is a problem. Consider an alternative and save the file separately as *Wild Cherry - Edited* : always leave the original file as it is.
Hmm. It would seem there are two *Wild Cherry* themes in the theme folder. Let's explore that one too.

* Foreground : #E0FBFF
* Background : #2A1F33
* Bold : #93A1A1
* Links : #005CBB 
* Selection : #003541
* Selected Text : #E9FFFF
* Black : #000405
* Red : #E35A97
* Green : #2DBC63
* Yellow : #FFD882
* Blue : #9B5AE3
* Magenta : #F0F0F0
* Cyan : #CCC5C4
* White : #FFF9E4
* Bright Black : #00ACD4
* Bright Red : #E384BA
* Bright Green : #F7E2B4
* Bright Yellow : #EFCA79
* Bright Blue : #3A9EC6
* Bright Magenta : #BD797E
* Bright Cyan : #FFA5AD
* Bright White : #EB989E
* Cursor : #E73EFF
* Cursor Text : #FF40FE

## Wild Cherry Alternate - Dark

![](./@imgs/Wild_Cherry_Alternate_Dark.jpg)
This alternative version, I think the most up-to-date, has some strange changes compared to the other. The *black* colour is blue in both normal and bright. This leads to some strange looking interfaces in other **terminal** applications. The scheme eschews a separate bright palette which seems to be fairly common amongst other colour schemes but I have misgivings. The text colour has been changed from a light blue to straight *white* (*FFF*) and lastly the cursor is now *blue* rather than *magenta*. 

Some of the bolder choices are appealing but the *black* colouring and the harsh *white* text don't work as well as the original version. We can use both variations to create an *ultimate* version along with a light background version.

* Foreground : #FFFFFF
* Background : #2A1F33
* Bold : #93A1A1
* Links : #005CBB 
* Selection : #E05894
* Selected Text : #FDFFFF
* Black : #00ADDF
* Red : #E35A97
* Green : #2DBC63
* Yellow : #FFD882
* Blue : #9B5AE3
* Magenta : #00ACDF
* Cyan : #6172A6
* White : #FFF9E4
* Bright Black : #00ADDF
* Bright Red : #E35A97
* Bright Green : #2DBC63
* Bright Yellow : #FFD882
* Bright Blue : #9B5AE3
* Bright Magenta : #00ACDF
* Bright Cyan : #6172A6
* Bright White : #FFF9E4
* Cursor : #00A3DD
* Cursor Text : #00ACDF

## Cerveza Silvestre - Dark
![](./@imgs/Cereza_Silvestre_Dark.jpg)
Cereza Silvestre (*wild cherry!*) with some minor changes. The terribly light 'bright' colours have been replaced with the similar colours from the alternate theme. 

* Foreground : #E0FBFF
* Background : #2A1F33
* Bold : #FFF9E4
* Links : #005CBB 
* Selection : #E9FFFF
* Selected Text : #003541
* Black : #000405
* Red : #E35A97
* Green : #2DBC63
* Yellow : #FFD882
* Blue : #9B5AE3
* Magenta : #6172A6
* Cyan : #00ACDF
* White : #FFF9E4
* Bright Black : #93A1A1
* Bright Red : #E384BA
* Bright Green : #2DBC63
* Bright Yellow : #EFCA79
* Bright Blue : #B07CE9
* Bright Magenta : #7D87A6
* Bright Cyan : #54BFDF
* Bright White : #FFFCF1
* Cursor : #E73EFF
* Cursor Text : #00ACDF

![](./@imgs/Cereza_Silvestre_Dark_Hex.jpg)

## Cerveza Silvestre - Light
![](./@imgs/Cereza_Silvestre_Light.jpg)
The light version of this theme simply reverses the background and foreground resulting in a somewhat pleasant backdrop. This somewhat pleasant backdrop might not always be pleasant so let's fiddle.

* Foreground : #2A1F33
* Background : #E0FBFF
* Bold : #491672
* Links : #005CBB 
* Selection : #003541
* Selected Text : #E9FFFF
* Black : #000405
* Red : #E35A97
* Green : #2DBC63
* Yellow : #FFD882
* Blue : #9B5AE3
* Magenta : #6172A6
* Cyan : #00ACDF
* White : #FFF9E4
* Bright Black : #454C4C
* Bright Red : #E384BA
* Bright Green : #2DBC63
* Bright Yellow : #EFCA79
* Bright Blue : #B07CE9
* Bright Magenta : #7D87A6
* Bright Cyan : #54BFDF
* Bright White : #FFFCF1
* Cursor : #E73EFF
* Cursor Text : #00ACDF

![](./@imgs/Cereza_Silvestre_Light_Hex.jpg)

## Cerveza Silvestre - Light Bright
![](./@imgs/Cereza_Silvestre_Light_FFF.jpg)
The same as above but with a stark **FFF** background.

* Foreground : #2A1F33
* Background : #FFFFFF
* Bold : #491672
* Links : #005CBB 
* Selection : #FFFFFF
* Selected Text : #E73EFF
* Black : #000405
* Red : #E35A97
* Green : #2DBC63
* Yellow : #FFD882
* Blue : #9B5AE3
* Magenta : #6172A6
* Cyan : #00ACDF
* White : #FFF9E4
* Bright Black : #454C4C
* Bright Red : #E384BA
* Bright Green : #2DBC63
* Bright Yellow : #EFCA79
* Bright Blue : #B07CE9
* Bright Magenta : #7D87A6
* Bright Cyan : #54BFDF
* Bright White : #FFFCF1
* Cursor : #E73EFF
* Cursor Text : #00ACDF

![](./@imgs/Cereza_Silvestre_Light_FFF_Hex.jpg)

## Cerveza Silvestre - Light Dim
![](./@imgs/Cereza_Silvestre_Light_EEE.jpg)
The same as the bright version but with a *dimmed* background taken from **Paper Colour**.

* Foreground : #2A1F33
* Background : #EEEEEE
* Bold : #491672
* Links : #005CBB 
* Selection : #003541
* Selected Text : #E9FFFF
* Black : #000405
* Red : #E35A97
* Green : #2DBC63
* Yellow : #FFD882
* Blue : #9B5AE3
* Magenta : #6172A6
* Cyan : #00ACDF
* White : #FFF9E4
* Bright Black : #454C4C
* Bright Red : #E384BA
* Bright Green : #2DBC63
* Bright Yellow : #EFCA79
* Bright Blue : #B07CE9
* Bright Magenta : #7D87A6
* Bright Cyan : #54BFDF
* Bright White : #FFFCF1
* Cursor : #E73EFF
* Cursor Text : #00ACDF

## Monokai Dark
![](./@imgs/Monokai_Dark.jpg)
Look specifically at the *blue*, *magenta* and *cyan* colours and compare them to **Monokai Soda**. I would like a different colour for *magenta* as at this moment it is the same as *red*.

* Foreground : #F8F8F2
* Background : #272822
* Bold : #F8F8F2
* Links : #005CBB
* Selection : #49483E
* Selected Text : #F8F8F2
* Black : #272822
* Red : #F92A72
* Green : #A6E22E
* Yellow : #F4BF75
* Blue : #66D9EF
* Magenta : #AE81FF
* Cyan : #A1EFE4
* White : #F8F8F2
* Bright Black : #75715E
* Bright Red : #F92A72
* Bright Green : #A6E22E
* Bright Yellow : #F4BF75
* Bright Blue : #66D9EF
* Bright Magenta : #AE81FF
* Bright Cyan : #A1EFE4
* Bright White : #F9F8F5
* Cursor : #F8F8F2 
* Cursor Text : #272822

## Monokai Light
![](./@imgs/Monokai_Light.jpg)
Always difficult to get a theme that works either light or dark using the same colours. With **Monokai** the colours, like *green*, really pop against a dark background but are exceedingly difficult to read on a light backdrop. See what works and what can be taken from this theme.

* Foreground : #49483E
* Background : #F9F8F5
* Bold : #49483E
* Links : #005CBB
* Selection : #F8F8F2
* Selected Text :#49483E
* Black : #272822
* Red : #F92A72
* Green : #A6E22E
* Yellow : #F4BF75
* Blue : #66D9EF
* Magenta : #AE81FF
* Cyan : #A1EFE4
* White : #F8F8F2
* Bright Black : #75715E
* Bright Red : #F92A72
* Bright Green : #A6E22E
* Bright Yellow : #F4BF75
* Bright Blue : #66D9EF
* Bright Magenta : #AE81FF
* Bright Cyan : #A1EFE4
* Bright White : #F9F8F5
* Cursor : #49483E
* Cursor Text : #F9F8F5

## Monokai Soda - Original
![](./@imgs/Monokai_Soda_Dark.jpg)
This theme has only been slightly modified to create the edited version. The main difference is the swap of *blue* and *cyan*. Call me old fashioned but *blue* really should be blue rather than *purple*.

* Foreground : #CFCFC2
* Background : #222222
* Bold : #CFCFC2
* Links : #005CBB
* Selection : #444444
* Selected Text :#CFCFC2
* Black : #222222
* Red : #F92A72
* Green : #A6E22E
* Yellow : #FD971F
* Blue : #AE81FF
* Magenta : #F92A72
* Cyan : #66D9EF
* White : #CFCFC2
* Bright Black : #75715E
* Bright Red : #F92A72
* Bright Green : #A6E22E
* Bright Yellow : #E6DB74
* Bright Blue : #AE81FF
* Bright Magenta : #F92A72
* Bright Cyan : #66D9EF
* Bright White : #F8F8F2
* Cursor : #F8F8F0
* Cursor Text : #CFCFC2

## Monokai Soda - Edited Dark
![](./@imgs/Monokai_Soda_Edited_Dark.jpg)
The first thing to note is the change between the foreground/background colours from the original theme. Compare and contrast.

* Foreground : #C4C5B5
* Background : #1A1A1A
* Bold : #F9F9F9
* Links : #005CBB
* Selection : #343434
* Selected Text :#CFCFC2
* Black : #1A1A1A
* Red : #F4005F
* Green : #98E024
* Yellow : #FA8419
* Blue : #58D1EB
* Magenta : #F4005F
* Cyan : #9D65FF
* White : #C4C5B5
* Bright Black : #625E4C
* Bright Red : #F4005F
* Bright Green : #98E024
* Bright Yellow : #E0D561
* Bright Blue : #58D1EB
* Bright Magenta : #F4005F
* Bright Cyan : #9D65FF
* Bright White : #F6F6EF
* Cursor : #F6F7EC
* Cursor Text : #CFCFC2

## Monokai Soda - Edited Light
![](./@imgs/Monokai_Soda_Edited_Light.jpg)
Look at **Paper Colour** to see some bolder colours against a light background. 

* Foreground : #44445B
* Background : #EEEEEE
* Bold : #1D1D27
* Links : #005CBB
* Selection : #C2C2C2
* Selected Text :#343434
* Black : #44445B
* Red : #F4005F
* Green : #80BE1F
* Yellow : #F1801A
* Blue : #419EB2
* Magenta : #F4005F
* Cyan : #9D65FF
* White : #C4C5B5
* Bright Black : #445040
* Bright Red : #F4005F
* Bright Green : #98E024
* Bright Yellow : #DDD462
* Bright Blue : #58D1EB
* Bright Magenta : #F4005F
* Bright Cyan : #9D65FF
* Bright White : #F6F6EF
* Cursor : #DDD462
* Cursor Text : #343434

## PaperColour
![](./@imgs/Paper_Colour_Light.jpg)
The light terminal theme to accompany the **vim** theme. I don't massively like the scheme but it does show how one can use colours against a light background.

* Foreground : #4D4D4C
* Background : #EEEEEE
* Bold : #4D4D4C
* Links : #005CBB
* Selection : #4D4D4C
* Selected Text : #EEEEEE
* Black : #4D4D4C
* Red : #D7235F
* Green : #718C00
* Yellow : #D75F00
* Blue : #4271AE
* Magenta : #8959A8
* Cyan : #3E999F
* White : #EEEEEE
* Bright Black : #4D4D4C
* Bright Red : #D7235F
* Bright Green : #718C00
* Bright Yellow : #D75F00
* Bright Blue : #4271AE
* Bright Magenta : #8959A8
* Bright Cyan : #3E999F
* Bright White : #EEEEEE
* Cursor : #000000
* Cursor Text : #FFFFFF

## Monokai Soda Final - Dark
![](./@imgs/Monokai_Soda_Final_Dark.jpg)
The final colour scheme for Monokai Soda Dark as noted by the title.

* Foreground : #C4C5B5
* Background : #1A1A1A
* Bold : #E3E986
* Links : #005CBB
* Selection : #343434
* Selected Text :#CFCFC2
* Black : #1A1A1A
* Red : #F4005F
* Green : #98E024
* Yellow : #FA8419
* Blue : #58D1EB
* Magenta : #9D65FF
* Cyan : #6172A6
* White : #C4C5B5
* Bright Black : #545454
* Bright Red : #F44086
* Bright Green : #ABE056
* Bright Yellow : #E0D561
* Bright Blue : #84D8EB
* Bright Magenta : #AC7CFF
* Bright Cyan : #768ACA
* Bright White : #CFD0BF
* Cursor : #C4C5B5
* Cursor Text : #1A1A1A

![](./@imgs/Monokai_Soda_Final_Dark_Hex.jpg)

## Monokai Soda Final - Light
![](./@imgs/Monokai_Soda_Final_Light.jpg)
Final Monokai Soda Light theme with an additional dimmed background version.

* Foreground : #F9F8F5
* Background : #44445B
* Bold : #3B3BA2
* Links : #005CBB
* Selection : #343434
* Selected Text :#F9F8F5
* Black : #1A1A1A
* Red : #F4005F
* Green : #80BE1F
* Yellow : #F1801A
* Blue : #419EB2
* Magenta : #8757DC
* Cyan : #4D62A2
* White : #C4C5B5
* Bright Black : #545454
* Bright Red : #F44086
* Bright Green : #93D035
* Bright Yellow : #DDD462
* Bright Blue : #2FB7D5
* Bright Magenta : #9967F1
* Bright Cyan : #5475DA
* Bright White : #CFD0BF
* Cursor : #343434
* Cursor Text : #F9F8F5

![](./@imgs/Monokai_Soda_Final_Light_Hex.jpg)

## Monokai Soda Final - Light Dimmed
![](./@imgs/Monokai_Soda_Light_Paper.jpg)
A dimmed background for when you want a light theme but not an eye-piercing one.

* Foreground : #44445B
* Background : #EEEEEE
* Bold : #0a0a5b
* Links : #005CBB
* Selection : #343434
* Selected Text :#F9F8F5
* Black : #1A1A1A
* Red : #F4005F
* Green : #80BE1F
* Yellow : #F1801A
* Blue : #419EB2
* Magenta : #8757DC
* Cyan : #4D62A2
* White : #C4C5B5
* Bright Black : #545454
* Bright Red : #F44086
* Bright Green : #93D035
* Bright Yellow : #DDD462
* Bright Blue : #2FB7D5
* Bright Magenta : #9967F1
* Bright Cyan : #5475DA
* Bright White : #CFD0BF
* Cursor : #343434
* Cursor Text : #F9F8F5

## Wild Monokai Soda - Dark
![](./@imgs/Wild_Monokai_Soda_Dark.jpg)
As the name suggests this is a combination of Monokai Soda & Wild Cherry (*perhaps more pertinetly Cereza Silvestre*). The theme takes the background and foreground of Monokai Soda and the colours of Wild Cherry. A light theme is provided.

* Background : #1A1A1A
* Foreground : #C4C5B5
* Bold : #FAFFB2
* Selection : #343434
* Selected Text : #C4C5B5
* Black : #000405
* Red : #E35A97
* Green : #2DBC63
* Yellow : #FFD882
* Blue : #00ACDF
* Magenta : #9B5AE3
* Cyan : #6172A6
* White : #FFF9E4
* Bright Black : #454C4C
* Bright Red : #E384BA
* Bright Green : #2DBC63
* Bright Yellow : #EFCA79
* Bright Blue : #54BFDF
* Bright Magenta : #B07CE9
* Bright Cyan : #7D87A6
* Bright White : #FFFCF1
* Cursor : #E73EFF
* Cursor Text : #C4C5B5

![](./@imgs/Wild_Monokai_Soda_Dark_Hex.jpg)

## Wild Monokai Soda - Light
![](./@imgs/Wild_Monokai_Light.jpg)
The light theme with a modicum of tinkering to try and balance the colours against the backdrop.

* Background : #EEEEEE
* Foreground : #44445B
* Bold : #09095B
* Selection : #44445B
* Selected Text : #FFF9E4
* Black : #000405
* Red : #E35A97
* Green : #2DBC63
* Yellow : #FFD882
* Blue : #00ACDF
* Magenta : #9B5AE3
* Cyan : #6172A6
* White : #FFF9E4
* Bright Black : #454C4C
* Bright Red : #E384BA
* Bright Green : #2DBC63
* Bright Yellow : #EFCA79
* Bright Blue : #54BFDF
* Bright Magenta : #B07CE9
* Bright Cyan : #7D87A6
* Bright White : #FFFCF1
* Cursor : #343434
* Cursor Text : #C4C5B5

![](./@imgs/Wild_Monokai_Light_Hex.jpg)
