# [Can Meaningful Design Survive in the Age of Shitposting](http://www.surfacemag.com/articles/can-design-survive-shitposting-culture/)

## The internet's culture of nihilistic memes and viral absurdity have become a way to speak not truth, but angry nonsense to both power and the powerless—the angrier and more nonsensical, the better.

By Gustavo Turner    
November 20, 2017

![Illustration by Nicolas Ortega](https://d2611yx6t84boa.cloudfront.net/app/uploads/2017/11/19135734/meme-board-illustration.jpg)

In 2005, Milton Glaser published *The Design of Dissent: Socially and Politically Driven Graphics*, an anthology of graphic and poster art covering the period between the fall of the Berlin Wall and the U.S. 2004 election. Co-edited with Bosnian-born designer Mirko Ilić, *The Design of Dissent* benefitted from both Glaser's visionary eye for graphic design (he's undoubtedly one of the greatest masters of that craft) and his liberal passion for a righteous cause.

Glaser was (and still is) a living link with the cultural history of the mid-to-late 20th century. Born at the onset of the Great Depression, he was 76 when the book was published. His poster and logo art is iconic for its wit and deceptive simplicity. Think of the ubiquitous "I ♥ NY" design, or the silhouetted profile of Bob Dylan with an electric rainbow issuing from his hair, just two examples in Glaser's peerless portfolio. But Glaser, who dedicated the collection of political art to Martin Luther King Jr., sees himself first as a citizen-artist, a craftsman with a clear social conscience and a heartfelt obligation to depict truth to power.

The kind of political poster that attracts Glaser's impeccable taste—a rare combination of surprising intelligence and technical skill—has always been a kind of mirror image to the highest end of official propaganda and advertising. (Unsurprisingly, many of the best political posters are produced, after-hours and pro bono, by the very same people—including Glaser—who make their living with commercial assignments.)

Looking at the 12-year-old *Design of Dissent* from the vantage point of 2017, however, is like looking at a long-vanished world.

Something like the stunning poster remixing the then popular iPod ad with the iconic image of the hooded man being tortured at Abu Ghraib seems like an artefact of a different era. In the age when crudely cut-and-pasted memes, amateur graphics, and "shitposting" (defined by an Urban Dictionary user as "to make utterly worthless and inane posts on an internet message board," usually to confuse, derail meaning, and entertain) can be weaponised to sway a presidential election, a witty poster or billboard devised by a graphic design pro seems like a relic of the 20^th century.

![An anti-Iraq War poster (2004)](https://d2611yx6t84boa.cloudfront.net/app/uploads/2017/11/19140501/iraq-ipod-abu-ghraib.jpg)

In the book, Glaser is incredibly canny about what he saw coming. In the interview that closes it, speaking of an anti-Bush campaign he had designed during the 2004 campaign, Glaser mentions that he already couldn't afford a billboard or poster service in gentrified New York City.

"Now of course, what you hope for is that these ideas will travel, as they say, virally. That people will catch on, and that the message will quickly circulate. The internet provides the opportunity, and perhaps, the idea of posting printed objects has become less relevant."

But even a genius like Glaser could not have foreseen our brave new world of fake-news content farms, data-driven audience manipulation, Russian meddling, and targeted shitposting campaigns with deep pockets. In terms of debating ideas via design, all the forces that came into play during the absurdly long American primaries and presidential cycle of 2015 and 2016 turned the meme into the new political poster. Homemade (or homemade-looking, in the case of stealth propaganda operations) internet graphics destroyed that much admired tradition, dating back to the Soviet Revolution and the art nouveau era, of dissent by clever, professionally designed printed posters. Turns out Shepard Fairey's "Hope" design for the 2008 Obama campaign was the last artefact of a dying era.

**The Design of Dissent has just been reissued** by Quarto Publishing Group in an expanded edition subtitled *Greed, Nationalism, Alternative Facts, and the Resistance*. The new section, crucially, includes only three images specifically designed for the internet, and does not include Facebook posts or memes or viral images. It immediately dates the whole project: It is unthinkable to imagine a survey of political art and design leading up to the 2016 election that does not include digital examples.

By now we know that the emergence of meme culture was a slow process, best exemplified by the evolution (some would say devolution) of the forum 4chan: from its creation in 2003 as a place to share user-generated anime-related content where all the participants went by the common name "Anonymous," to 2008, when some of these "Anonymous" became a loose collective advocating direct action, appropriating the Guy Fawkes mask from the 2005 *V for Vendetta* film and appearing in real-world demonstrations. During the Obama administration, 4chan started veering from a kind of prank-based anarchism into becoming a more reactionary force. Memes and shitposting became a way to speak not truth, but angry nonsense to both power and the powerless—the angrier and more nonsensical, the better.

![From Left: Shepard Fairey's "Hope" Poster (2008). Donald Trump-Pepe the Frog meme (2015-2016)](https://d2611yx6t84boa.cloudfront.net/app/uploads/2017/11/19141647/hope-obama-pepe-trump.jpg)

The 2015/2016 campaign shocked many sophisticates into the realisation that design power had been taken away from the liberal elites who found the iPod/Abu Ghraib remix clever, and handed over to these shadowy meme-makers. This reaction started brewing from the darkest crevices of the internet and was also driven by resentment and suspicion of the corporate takeover of private life. Glossy posters might advertise questionably ethical diamond rings or Vegas vacations, but the power to dismantle all the claims and promises of the slickest advertisers now resides on anonymous, frustrated individuals who hit back by messing around with Photoshop and Microsoft Paint.

This revolt of the angry and powerless underlies huge segments of the culture, from the meme-driven resurgence of White Supremacy, to abusive Facebook users ranting about poor service or perceived slights, to the intemperate ravings of online commenters. All these "rogue designers" present themselves as the victims and rejects of corporate culture and they are repurposing the internet as a reaction to where real power lies (the post–Citizens United corporations on steroids, the super-mega-rich).

Anonymous, WikiLeaks, MAGA, et al., are presenting themselves as grassroots versions of real power. This taps into a kind of folk sovereignty and "rebellion" that is very important in American mythology (cowboys vs. Davos ?). We have gone, design-wise, from "power" as a positive, desirable, or aspirational brand, to "power" as something "regular folks" undermine via the internet with their own kind of reactive energy.

Internet users (abusers, some would say) exercise their power to confuse and dismantle the claims of design and advertising from the vantage point of anonymity. If Hollywood, for example, serves them a winky, ironic, self-knowing billboard for the movie *The Expendables*, they will crudely remix it as "The Deplorables," adding the heads of Donald Trump, Milo Yiannopoulos, and the ultimate shitpost mascot, Pepe the Frog, himself randomly repurposed from Matt Furie's charming comic strip *Boy's Club*. Is this an effective ad for Trump? Is this a parody? Is this laughing *with* or laughing *at*? And ultimately, does anyone care?

![One of several examples of a poster for The Expendables altered with alt-right icons (2015–2016)](https://d2611yx6t84boa.cloudfront.net/app/uploads/2017/11/19142551/deplorables-trump-meme.jpg)

One of several examples of a poster for The Expendables altered with alt-right icons (2015–2016).

**It is not a stretch to talk about** internet-fueled isolation and alienation, a general degeneration of meaning in shitposting, and thus, the futility of "good" or "professional" design as a tool of dissent in the current world. The current rock star of academic philosophers writing about power in the digital age, the Korean-German scholar Byung-Chul Han, has made this point in several works over the last decade.

Han talks about "the swarm," the kind of collective entity produced by digital media (the descendant of the 19th- and 20th-century "masses"), and the "shitstorm," the type of aggression-driven scandal that is typical of an internet campaign to destroy a person's livelihood and reputation—our current version of a lynching, or the proverbial angry villagers with torches.

For the prolific Han, everyone is now voluntarily exposed. People have been convinced to turn themselves into self-exploiters: Everyone is a brand and a one-person company that demands constant growth in order to survive. (Just look to the bursting industry of Instagram "influencers.") The individual is thus both exploiter and exploited, lives in isolation, only connected to others via the internet, and has been encouraged to be completely transparent—to the state and to the corporations that own the actual power.

Real power can be bought and sold as it always has been. Access to capital and weapons (and impunity) are still the true measures of power, both in Machiavelli's Italy (Robert Greene's neo-Machiavellian *The 48 Laws of Power* is not going out of print anytime soon) and in Blackwater's America. You can sarcastically post nonsensical inside jokes about the Kardashians on Facebook's shitpost groups with names like @TheKardashianMemes all you want, but you are still promoting Kim et al.'s brands—and ultimately making money for Mark Zuckerberg.

When all bodies are tethered, *Matrix*-like, into online social networks 24/7, power controls the mind, and individuals have subjected themselves voluntarily. For Han, the only possible—albeit ineffective—reaction to this is the "shitstorm" in the form of a public attack against a perceived wrongdoer. The recent fall of Hollywood mogul Harvey Weinstein was largely perceived as a "righteous" version of Han's shitstorm. But the same tactics can also be applied against someone less obviously guilty, if only enough people get whipped up out of their digital stupor.

![Popular shitpost image](https://d2611yx6t84boa.cloudfront.net/app/uploads/2017/11/20104308/shitpost-meme-furry-animals.001.jpeg)

**Crude is a word that turns up a lot** in discussions of meme and shitpost aesthetics. This is not the 1990s version of suspicion and resentment, with Dilbert (always a hit as cubicle decoration) normalising the exploitation of the white-collar worker. This is something that looks amateur and raw, and doesn't really make sense. All the tools of design were put in the hands of the swarm, and what came up on top was Doge memes, or a picture of Donald Trump with a frog painted on his chin.

And what is a shitpost if not a pointless, carnivalesque attack against meaning itself? Like every aesthetic that draws from the absurd (Dada, surrealism, situationism, punk), shitposting is ultimately a cry of despair against impending disaster.

A popular shitpost image has a cute animal (talking animals, cartoons, and infantilism in general play a huge role in meme-making) asking his friend, "Why are you shitposting?" "I shitpost when I am depressed," the other furry creature replies, staring at the computer screen. "But you're always shitposting," protests the first animal. The last panel is of the shitposter creature just grinning.

As the original Pepe the Frog said (while gleefully peeing with his pants around his ankles), "Feels good, man."
