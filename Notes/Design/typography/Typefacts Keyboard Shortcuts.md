# Typefacts

Written on 7. March 2009 by Christoph Koeberlin

Edited in January 2018 by Norman Posselt

On Typefacts we impart knowledge on type and typography through comprehensive language and graphic description.

## [Keyboard Shortcuts](https://typefacts.com/en/articles/keyboard-shortcuts)

Here you find all special characters that are accessible via the [British/American keyboard](http://en.wikipedia.org/wiki/British_and_American_keyboards). I compiled them in groups that I find quite sensible, most important ones on the top.

### Windows Input Method

On Windows, many of the characters have to be inserted by entering an alt code; press and hold alt key and enter the code with the numeric keypad – e.g. en dash = `Alt + 0 1 5 0`.

![](./@imgs/typefacts/1651abdcc60812cb201958f05136969341fc1460.png)

### Quotation

Character | Name | macOS | Windows | Linux
-- | -- | -- | -- | --
“ | Opening | ⌥ + [ | Alt + 0147 | —
” | Closing | ⇧ + ⌥ + [ | Alt + 0148 | —
‘ |Single opening | ⌥ + ] | Alt + 0145 | —
’ | Apostrophe | ⇧ + ⌥ + ] | Alt + 0146 | —
« | Opening Guillemet (angle quote) | ⌥ + \ | Alt + 0171 | —
» | Closing Guillemet (angle quote) | ⌥ + ⇧ + \ | Alt + 0187 | —
‹ | Single opening Guillemet | ⇧ + ⌥ + 3 | Alt + 0139 | —
› | Single closing Guillemet | ⇧ + ⌥ + 4 | Alt + 0155 | —
„ | German opening | ⇧ + ⌥ + W | Alt + 0132 | —
‚ | German single opening | ⇧ + ⌥ + 0 | Alt + 0130 | —

### Punctuation

Character | Name | macOS | Windows | Linux
-- | -- | -- | -- | --
\- | Ascii hyphen | - | - | -
– | En Dash | ⌥ + - | Alt + 0150 | —
— | Em Dash | ⇧ + ⌥ + - | Alt + 0151 | —
… | Ellipsis | ⌥ + ; | Alt + 0133 | —
’ | Apostrophe | ⇧ + ⌥ + ] | Alt + 0146 | —
· | Interpunct (interpoint, Centred point, Middle dot) | ⇧ + ⌥ + 9 | Alt + 0183 | —
• | Bullet | ⌥ + 8 | Alt + 0149 | —
¡ | Inverted exclamation mark | ⌥ + 1 | Alt + 0161 | —
! | Exclamation mark | ⇧ + 1 | ⇧ + 1 | —
¿ | Inverted question mark | ⇧ + ⌥ + / | Alt + 0191 | —
? | Question mark | ⇧ + / | ⇧ + / | —
( | Opening parenthesis (round bracket) | ⇧ + 9 | ⇧ + 9 | —
) | Closing parenthesis (round bracket) | ⇧ + 0 | ⇧ + 0 | —
[ | Opening square bracket | [ | [ | —
] | Closing square bracket | ] | ] | —
{ | Opening curly bracket (brace) | ⇧ + \[ | ⇧ + \[ | —
} | Closing curly bracket (brace) | ⇧ + \] | ⇧ + \] | —
/ | Slash | / | / | —
\ | Backslash | \ | \ | —
| | Vertical bar | ⇧ + \\ | ⇧ + \\ | —

### Currencies

Character | Name | macOS | Windows | Linux
-- | -- | -- | -- | --
€ | Euro | ⇧ + ⌥ + 2 | Alt + 0128 | —
$ | Dollar | ⇧ + 4 | ⇧ + 4 | —
¥ | Yen | ⌥ + Y | Alt + 0165 | —
£ | Pound | ⌥ + 3 | Alt + 0163 | —
¢ | Cent | ⌥ + 4 | Alt + 0162 | —
ƒ | Florin | ⌥ + F | Alt + 0131 | —
¤ | Currencie Sign | — | Alt + 0164 | —

### Miscellaneous Symbols

Character | Name | macOS | Windows | Linux
-- | -- | -- | -- | --
@ | At sign | ⇧ + 2 | ⇧ + 2 | —
& | Ampersand | ⇧ + 7 | ⇧ + 7 | —
ﬁ | ﬁ ligature | ⇧ + ⌥ + 5 | — | —
ﬂ | ﬂ ligature | ⇧ + ⌥ + 6 | — | —
§ | Section sign | § | Alt + 21 | —
% | Percent | ⇧ + 5 | ⇧ + 5 | —
‰ | Permille sign | ⇧ + ⌥ + R | Alt + 0137 | —
¶ | Pilcrow (paragraph mark, alinea) | ⌥ + 7 | Alt + 0182 | —
™ | Trademark | ⌥ + 2 | Alt + 0153 | —
© | Copyright | ⌥ + G | Alt + 0169 | —
® | Registered Trademark | ⌥ + R | Alt + 0174 | —
\* | Asterisk | ⇧ + 8 | ⇧ + 8 | —
† | Dagger (obelisk) | ⌥ + T | Alt + 0134 | —
‡ | Double dagger (diesis) | ⇧ + ⌥ + 7 | Alt + 0135 | —
° | Degree | ⇧ + ⌥ + 8 | Alt + 248 | —
' | Single code mark (Feet, minutes) | ' | ' | —
" | Double code mark (Inches, seconds) | ⇧ + ' | ⇧ + ' | —
ª | Feminine ordinal indicator | ⌥ + 9 | Alt + 0170 | —
º | Masculine ordinal indicator | ⌥ + 0 | Alt + 0186 | —

### Diacritics / Accented letters

These are all accented letters available via keyboard shortcuts! You can copy/paste the most important Latin characters at our [tweeting symbols](https://typefacts.com/en/articles/nicer-tweets) article.

![iOS (left) and macOS (right): by pushing and holding keys, you can access a corresponding glyph palette; e.g. in iA Writer](./@imgs/typefacts/91ed81b301c632e865a66d81bc13c82c1e78d6a5.jpg)

Character | Name | macOS | Windows | Linux
-- | -- | -- | -- | --
À | A with grave | ⌥ + \` + ⇧ + A | Alt + 0192 | —
à | a with grave | ⌥ + \` + A | Alt + 0224 | —
Á | A with acute | ⇧ + ⌥ + Y | Alt + 0193 | —
á | a with acute | ⌥ + E + A | Alt + 0225 | —
Â | A with circumflex | ⌥ + ⇧ + M | Alt + 0194 | —
â | a with circumflex | ⌥ + I + A | Alt + 0226 | —
Ã | A with tilde | ⌥ + N + ⇧ + A | Alt + 0195 | —
ã | a with tilde | ⌥ + N + A | Alt + 0227 | —
Ä | A with diaeresis | ⌥ + U + A | Alt + 0196 | —
ä | a with diaeresis | ⌥ + U + A | Alt + 0228 | —
Å | A with ring | ⇧ + ⌥ + A | Alt + 0197 | —
å | a with ring | ⌥ + A | Alt + 0229 | —
Æ | AE ligature | ⇧ + ⌥ + ‘ | Alt + 0198 | —
æ | ae ligature | ⌥ + ‘ | Alt + 0230 | —
Ç | C with cedilla | ⇧ + ⌥ + C | Alt + 0199 | —
ç | c with cedilla | ⌥ + C | Alt + 0231 | —
È | E with grave | ⌥ + \` + ⇧ + E | Alt + 0200 | —
è | e with grave | ⌥ + \` + E | Alt + 0232 | —
É | E with acute | ⌥ + E + ⇧ + E | Alt + 0201 | —
é | e with acute | ⌥ + E + E | Alt + 0233 | —
Ê | E with circumflex | ⌥ + I + ⇧ + E | Alt + 0202 | —
ê | e with circumflex | ⌥ + I + E | Alt + 0234 | —
Ë | E with diaeresis | ⌥ + U + ⇧ + E | Alt + 0203 | —
ë | e with diaeresis | ⌥ + U + E | Alt + 0235 | —
Ì | I with grave | ⌥ + \` + ⇧ + I | Alt + 0204 | —
ì | i with grave | ⌥ + \` + I | Alt + 0236 | —
Í | I with acute | ⇧ + ⌥ + S | Alt + 0205 | —
í | i with acute | ⌥ + E + I | Alt + 0237 | —
Î | I with circumflex | ⌥ + ⇧ + D | Alt + 0206 | —
î | i with circumflex | ⌥ + I + I | Alt + 0238 | —
Ï | I with diaeresis | ⇧ + ⌥ + F | Alt + 0207 | —
ï | i with diaeresis | ⌥ + U + I | Alt + 0239 | —
ı | Dotless i | ⇧ + ⌥ + B | — | —
Ñ | N with tilde | ⌥ + N + ⇧ + N | Alt + 0209 | —
ñ | n with tilde | ⌥ + N +N | Alt + 0241 | —
Ò | O with grave | ⌥ + ⇧ + L | Alt + 0210 | —
ò | o with grave | ⌥ + \` + O | Alt + 0242 | —
Ó | O with acute | ⇧ + ⌥ + H | Alt + 0211 | —
ó | o with acute | ⌥ + E + O | Alt + 0243 | —
Ô | O with circumflex | ⌥ + ⇧ + J | Alt + 0212 | —
ô | o with circumflex | ⌥ + I + O | Alt + 0244 | —
Õ | O with tilde | ⌥ + N + ⇧ + O | Alt + 0213 | —
õ | o with tilde | ⌥ + N + O | Alt + 0245 | —
Ö | O with diaeresis | ⌥ + U + ⇧ + O | Alt + 0214 | —
ö | o with diaeresis | ⌥ + U + O | Alt + 0246 | —
Ø | O with stroke (slashed O) | ⇧ + ⌥ + O | Alt + 0216 | —
ø | o with stroke (slashed o) | ⌥ + O | Alt + 0248 | —
Œ | OE ligature | ⇧ + ⌥ + Q | Alt + 0140 | —
œ | oe ligature | ⌥ + Q | Alt + 0156 | —
Š | S with caron | — | Alt + 0138 | —
š | s with caron | — | Alt + 0154 | —
ẞ | Capital eszett | — | — | —
ß | Eszett | ⌥ + S | Alt + 0223 | —
Ù | U with grave | ⌥ + \` + ⇧ + U | Alt + 0217 | —
ù | u with grave | ⌥ + \` + U | Alt + 0249 | —
Ú | U with acute | ⌥ + ⇧ + ; | Alt + 0218 | —
ú | u with acute | ⌥ + E + U | Alt + 0250 | —
Û | U with circumflex | ⌥ + I + ⇧ + U | Alt + 0219 | —
û | u with circumflex | ⌥ + I + U | Alt + 0251 | —
Ü | U with diaeresis | ⌥ + U + ⇧ + U | Alt + 0220 | —
ü | u with diaeresis | ⌥ + U + U | Alt + 0252 | —
Ý | Y with acute | — | Alt + 0221 | —
ý | y with acute | — | Alt + 0253 | —
Ÿ | Y with diaeresis | ⌥ + U + ⇧ + Y | Alt + 0159 | —
ÿ | y with diaeresis | ⌥ + U + Y | Alt + 0255 | —
Ž | Z with caron | — | Alt + 0142 | —
ž | z with caron | — | Alt + 0158 | —
Ð | Eth (Icelandic) | — | Alt + 0208 | —
ð | eth (Icelandic) | — | Alt + 0240 | —
Þ | Thorn (Icelandic) | — | Alt + 0222 | —
þ | thorn (Icelandic) | — | Alt + 0254 | —
\` | Grave | \` | \` | —
´ | Acute | ⇧ + ⌥ + e | Alt + 0180 | —
ˆ | Circumflex | ⇧ + ⌥ + I | Alt + 0192 | —
˜ | Tilde (non-mathematical) | ⇧ + ⌥ + N | Alt + 0152 | —
¨ | Diaeresis (Umlaut) | ⇧ + ⌥ + U | Alt + 0168 | —
˚ | Ring accent (≠ degree °) | ⌥ + K | Alt + 0000 | —
ˇ | Caron | ⇧ + ⌥ + T | — | —
˝ | Double acute (hungarumlaut) | ⇧ + ⌥ + G | — | —
˙ | Dot accent | ⌥ + H | — | —
¯ | Macron | ⇧ + ⌥ + , | Alt + 0175 | —
˘ | Breve | ⇧ + ⌥ + . | — | —
¸ | Cedilla | ⇧ + ⌥ + Z | — | —
˛ | Ogonek | ⇧ + ⌥ + X | — | —

### Mathematical etc.

Character | Name | macOS | Windows | Linux
-- | -- | -- | -- | --
\+ | Plus | ⇧ + = | ⇧ + = | —
× | Multiplication | — | Alt + 0215 | —
÷ | Division | ⌥ + / | Alt + 0247 | —
= | Equals | = | = | —
≈ | Almost equal to | ⌥ + X | — | —
≠ | Not equal | ⌥ + = | — | —
≙ | Estimates | — | Alt + 2259 | Strg + ⇧ + U + 2259
< | Less-than | ⇧ + , | ⇧ + , | —
\> | Greater-than | ⇧ + . | ⇧ + . | —
≤ | Greater-than or equal | ⌥ + . | — | —
≥ | Greater-than or equal | ⌥ + . | — | —
± | Plus-minus | ⇧ + § | Alt + 0177 | —
¦ | Broken bar | — | Alt + 0166 | —
^ | Caret (Ascii Circumflex) | ⇧ + 6 | Alt + 94 | —
~ | tilde (mathematical) | ⇧ + \` | ⇧ + \` | —
¬ | Logical Not | ⌥ + L | Alt + 0172 | —
µ | Micro sign (mu) | ⌥ + M | Alt Gr + M | —
Ω | Ohm sign (Omega) | ⌥ + Z | — | —
∆ | Symmetric Difference | ⌥ + J | — | —
π | pi | ⌥ + P | — | —
∂ | Partial derivative | ⌥ + D | — | —
∏ | Product | ⇧ + ⌥ + P | — | —
∑ | Summation | ⌥ + W | — | —
√ | Square root | ⌥ + V | — | —
∞ | Infinity | ⌥ + 5 | — | —
∫ | Integral | ⌥ + B | — | —
◊ | Lozenge (diamond) | ⇧ + ⌥ + V | — | —
⁄ | Fraction slash | ⇧ + ⌥ + 1 | — | —
¹ | Superscript 1 | — | Alt + 0185 | —
² | Superscript 2 | — | Alt + 253 | —
³ | Superscript 3 | — | Alt + 0179 | —
¼ | One quarter | — | Alt + 0188 | —
½ | One half | — | Alt + 0189 | —
¾ | Three quarters | — | Alt + 0190 | —

### Windows-only

Character | Name | macOS | Windows | Linux
-- | -- | -- | -- | --
☺ | Smiley | — | Alt + 1 | Strg + ⇧ + U + 263A
☻ | Black smiley | — | Alt + 2 | Strg + ⇧ + U + 263B
♥ | Hearts (suit) | — | Alt + 3 | Strg + ⇧ + U + 2665
♦ | Diamonds (suit) | — | Alt + 4 | Strg + ⇧ + U + 2666
♣ | Clubs (suit) | — | Alt + 5 | Strg + ⇧ + U + 2663
♠ | Spades (suit) | — | Alt + 6 | Strg + ⇧ + U + 2660
◘ | Inverse bullet | — | Alt + 8 | Strg + ⇧ + U + 25D8
○ | White circle | — | Alt + 9 | Strg + ⇧ + U + 25CB
◙ | Inverse white circle | — | Alt + 10 | Strg + ⇧ + U + 25D9
♂ | Male sign | — | Alt + 11 | Strg + ⇧ + U + 2642
♀ | Female sign | — | Alt + 12 | Strg + ⇧ + U + 2640
♪ | Eighth note | — | Alt + 13 | Strg + ⇧ + U + 266A
♫ | Beamed eighth notes | — | Alt + 14 | Strg + ⇧ + U + 266B
☼ | Sun | — | Alt + 15 | Strg + ⇧ + U + 263C
► | Black right-pointing pointer | — | Alt + 16 | Strg + ⇧ + U + 25BA
◄ | Black left-pointing pointer | — | Alt + 17 | Strg + ⇧ + U + 25C4
↕ | Up down arrow | — | Alt + 18 | Strg + ⇧ + U + 2195
‼ | Double exclamation mark | — | Alt + 19 | Strg + ⇧ + U + 203C
▬ | Black rectangle | — | Alt + 22 | Strg + ⇧ + U + 25AC
↨ | Up down arrow with base | — | Alt + 23 | Strg + ⇧ + U + 21A8
↑ | Upwards arrow | — | Alt + 24 | Strg + ⇧ + U + 2191
↓ | Downwards arrow | — | Alt + 25 | Strg + ⇧ + U + 2193
→ | Rightwards arrow | — | Alt + 26 | Strg + ⇧ + U + 2192
← | Leftwards arrow | — | Alt + 27 | Strg + ⇧ + U + 2190
∟ | Right angle | — | Alt + 28 | Strg + ⇧ + U + 221F
▲ | Black up-pointing triangle | — | Alt + 30 | Strg + ⇧ + U + 25B2
▼ | Black down-pointing triangle | — | Alt + 31 | Strg + ⇧ + U + 25BC
⌂ | House | — | Alt + 127 | Strg + ⇧ + U + 2302
₧ | Pesetas sign | — | Alt + 158 | Strg + ⇧ + U + 20A7
░ | Light shade | — | Alt + 176 | Strg + ⇧ + U + 2591
▒ | Medium shade | — | Alt + 177 | Strg + ⇧ + U + 2592
▓ | Dark shade | — | Alt + 178 | Strg + ⇧ + U + 2593
│ | Box drawing element | — | Alt + 179 | Strg + ⇧ + U + 2502
┤ | Box drawing element | — | Alt + 180 | Strg + ⇧ + U + 2524
╡ | Box drawing element | — | Alt + 181 | Strg + ⇧ + U + 2561
╢ | Box drawing element | — | Alt + 182 | Strg + ⇧ + U + 2562
╖ | Box drawing element | — | Alt + 183 | Strg + ⇧ + U + 2556
╕ | Box drawing element | — | Alt + 184 | Strg + ⇧ + U + 2555
╣ | Box drawing element | — | Alt + 185 | Strg + ⇧ + U + 2563
║ | Box drawing element | — | Alt + 186 | Strg + ⇧ + U + 2551
╗ | Box drawing element | — | Alt + 187 | Strg + ⇧ + U + 2557
╝ | Box drawing element | — | Alt + 188 | Strg + ⇧ + U + 255D
╜ | Box drawing element | — | Alt + 189 | Strg + ⇧ + U + 255C
╛ | Box drawing element | — | Alt + 190 | Strg + ⇧ + U + 255B
┐ | Box drawing element | — | Alt + 191 | Strg + ⇧ + U + 2510
└ | Box drawing element | — | Alt + 192 | Strg + ⇧ + U + 2514
┴ | Box drawing element | — | Alt + 193 | Strg + ⇧ + U + 2534
┬ | Box drawing element | — | Alt + 194 | Strg + ⇧ + U + 252C
├ | Box drawing element | — | Alt + 195 | Strg + ⇧ + U + 251C
─ | Box drawing element | — | Alt + 196 | Strg + ⇧ + U + 2500
┼ | Box drawing element | — | Alt + 197 | Strg + ⇧ + U + 253C
╞ | Box drawing element | — | Alt + 198 | Strg + ⇧ + U + 255E
╟ | Box drawing element | — | Alt + 199 | Strg + ⇧ + U + 255F
╚ | Box drawing element | — | Alt + 200 | Strg + ⇧ + U + 255A
╔ | Box drawing element | — | Alt + 201 | Strg + ⇧ + U + 2554
╩ | Box drawing element | — | Alt + 202 | Strg + ⇧ + U + 2569
╦ | Box drawing element | — | Alt + 203 | Strg + ⇧ + U + 2566
╠ | Box drawing element | — | Alt + 204 | Strg + ⇧ + U + 2560
═ | Box drawing element | — | Alt + 205 | Strg + ⇧ + U + 2550
╬ | Box drawing element | — | Alt + 206 | Strg + ⇧ + U + 256C
╧ | Box drawing element | — | Alt + 207 | Strg + ⇧ + U + 2567
╨ | Box drawing element | — | Alt + 208 | Strg + ⇧ + U + 2568
╤ | Box drawing element | — | Alt + 209 | Strg + ⇧ + U + 2564
╥ | Box drawing element | — | Alt + 210 | Strg + ⇧ + U + 2565
╙ | Box drawing element | — | Alt + 211 | Strg + ⇧ + U + 2559
╘ | Box drawing element | — | Alt + 212 | Strg + ⇧ + U + 2558
╒ | Box drawing element | — | Alt + 213 | Strg + ⇧ + U + 2552
╓ | Box drawing element | — | Alt + 214 | Strg + ⇧ + U + 2553
╫ | Box drawing element | — | Alt + 215 | Strg + ⇧ + U + 256
╪ | Box drawing element | — | Alt + 216 | Strg + ⇧ + U + 256A
┘ | Box drawing element | — | Alt + 217 | Strg + ⇧ + U + 2518
┌ | Box drawing element | — | Alt + 218 | Strg + ⇧ + U + 250C
█ | Full block | — | Alt + 219 | Strg + ⇧ + U + 2588
▄ | Lower half block | — | Alt + 220 | Strg + ⇧ + U + 2584
▌ | Left half block | — | Alt + 221 | Strg + ⇧ + U + 258C
▐ | Rigth half block | — | Alt + 222 | Strg + ⇧ + U + 2590
▀ | Upper half block | — | Alt + 223 | Strg + ⇧ + U + 2580
α | Greek letter alpha | — | Alt + 224 | Strg + ⇧ + U + 03B1
Γ | Greek letter Gamma | — | Alt + 226 | Strg + ⇧ + U + 0393
Σ | Greek letter Sigma | — | Alt + 228 | Strg + ⇧ + U + 03A3
σ | Greek letter sigma | — | Alt + 229 | Strg + ⇧ + U + 03C3
τ | Greek letter tau | — | Alt + 231 | Strg + ⇧ + U + 03C4
Φ | Greek letter Phi | — | Alt + 232 | Strg + ⇧ + U + 03A6
Θ | Greek letter Theta | — | Alt + 233 | Strg + ⇧ + U + 0398
δ | Greek letter delta | — | Alt + 235 | Strg + ⇧ + U + 03B4
φ | Greek letter phi | — | Alt + 237 | Strg + ⇧ + U + 03C6
ε | Greek letter epsilon | — | Alt + 238 | Strg + ⇧ + U + 03B5
∩ | Intersection | — | Alt + 239 | Strg + ⇧ + U + 2229
≡ | Identical to | — | Alt + 240 | Strg + ⇧ + U + 2261
⌠ | Top half integral | — | Alt + 244 | Strg + ⇧ + U + 2320
⌡ | Bottom half integral | — | Alt + 245 | Strg + ⇧ + U + 2321
∙ | Bullet operator | — | Alt + 249 | Strg + ⇧ + U + 2219
ⁿ | Superscript n | — | Alt + 252 | Strg + ⇧ + U + 207F
■ | Black square | — | Alt + 254 | Strg + ⇧ + U + 25A0

Hint: You can copy & paste all characters shown, even if there's no keyboard shortcut available, like Ω on Windows.

### Further Reading

  - [Alles über Unicode](http://unicode.e-workers.de)
  - [InDesign Shortcuts: Special Characters](https://www.fonts.com/content/learning/fyti/using-type-tools/indesign-shortcuts)
  - [Typo-Spickzettel für wichtige Sonderzeichen](http://www.fontblog.de/neuer-typo-spickzettel-fuer-wichtige-sonderzeichen/)
  - [Wikipedia: Diakritisches Zeichen](https://de.wikipedia.org/wiki/Diakritisches_Zeichen)
  - [Wikipedia: Sonderzeichen](https://de.wikipedia.org/wiki/Sonderzeichen)
