# The Mystery Font That Took Over New York

By RUMSEY TAYLOR, NOV. 21, 2018

## How did Choc, a quirky calligraphic typeface drawn by a French graphic designer in the 1950s, end up on storefronts everywhere?

![](./@imgs/NY_Font/1.jpg)

**Stand just about anywhere** on Broadway, or on Canal Street with its sprightly neon and overstuffed souvenir shops, or the long stretch of restaurants, hardware stores, pharmacies, bars, realtors, barber shops, groceries and auto shops that extends through Fifth Avenue in South Brooklyn, and you'll find a surplus of vibrant and overstated signage — a cacophony of typography.

Steven Heller, a co-chairman at the School of Visual Arts' M.F.A. program, sees it somewhat differently. "You say 'cacophony,'" he said. "I call it chaos."

But amid all of this chaos there is the occasional beacon. Choc, for instance.

It's a typeface that draws the eye with its inherent contradictions. It seems to have been drawn improvisationally with a brush, and yet it's so hefty it looks like it could slip off a wall. It's both delicate and emphatic, a casual paradox, like a Nerf weapon.

Choc is far from the most popular typeface on the storefronts of New York, but it can still be found everywhere and in every borough. It's strewn on fabric awnings and etched in frosted glass. It gleams in bright magenta or platinum lighting. It's used for beauty salons, Mexican restaurants, laundromats, bagel shops, numerous sushi bars. It may be distorted, stacked vertically, or shoehorned into a cluster of other typefaces. But even here Choc remains clear and articulate, its voice deep and friendly, its accent foreign, perhaps, yet endearing.

You've already seen it, probably repeatedly, like a stranger you recognize from your morning commute.

![](./@imgs/NY_Font/2.jpg)

### France, 1955

Like so many New Yorkers, Choc is an immigrant.
![Choc specimen, cover, 1955](./@imgs/NY_Font/3.jpg)
![Advertisement for the Choc typeface, in *La France graphique*, No 107, Nov. 1955](./@imgs/NY_Font/4.jpg)

It was designed by Roger Excoffon, a French typographer and graphic designer whose work departed from the Modernist trends that characterized midcentury type design. Based in Marseilles, Excoffon created a diverse array of typefaces during the 1940s and '50s, but his script typefaces have become his most enduring work.

"There's an unmistakable energy in all the designs that he does," said Tobias Frere-Jones, [a Brooklyn-based typeface designer](https://frerejones.com/). "They project their personality so clearly. And it's very obviously not Helvetica or not Times or any other generic thing that might be on the awning next door."

Excoffon's most popular typeface is probably Mistral, which was modeled after his own handwriting. (Mistral has been widely used for years, from the title credits in "Night Court" to Connecticut Muffin or N.W.A.'s logos, not to mention many storefronts in New York and elsewhere.) There's also Banco, which can justly be described as "shouty," composed entirely of letters that resemble exclamation marks; Calypso, with halftone-patterned letterforms that look like miniature optical illusions; Diane, an ornate script that's just about impossible to use outside of wedding invitations; and there is Choc, something of a synthesis of all of these.

![Amy's Restaurant at 586 West 207th Street in Manhattan. Vincent Tullo for The New York Times](./@imgs/NY_Font/5.jpg)
![Sushi Q at 1610 Crosby Avenue in the Bronx. Vincent Tullo for The New York Times](./@imgs/NY_Font/6.jpg)
![Fuji at 1115 Hylan Boulevard on Staten Island. Vincent Tullo for The New York Times](./@imgs/NY_Font/7.jpg)
![Fukuyama Sushi and Ramen at 622 Metropolitan Avenue in Brooklyn. Vincent Tullo for The New York Times](./@imgs/NY_Font/8.jpg)

Typefaces are characterized by redundancies. Notice how often the "d" and "b" can be rotated to make a "p" or a "q," or how a capital "Z" is so plainly a grown-up version of its lowercase counterpart. But Choc is full of irregularity. Its lowercase "r" resembles a "z." Its "g" looks like a capital "S." And its "h" crouches forward as though in starting position for a race, whereas its more heavyset uppercase incarnation is on the verge of rolling backward.

"Choc expresses a certain violence," said Sandra Chamaret, a French designer and publisher, and a co-author of [a 2010 monograph on Excoffon](http://ypsilonediteur.com/fiche.php?id=95). "It seems spattered on the page, the letters going in all directions." ("Choc" — the '*ch*' is soft, as in '*sh*' — is French for "shock" or "crash.")

Choc remains distinctive for these reasons, and it lacks datedness, as evidenced in its continued use throughout the world, most visibly on storefronts.

![](./@imgs/NY_Font/9.jpg)

### A Sign Painter's Heaven

Choc wouldn't appear on storefronts until well after its 1955 conception, however.

As an industry, type design was at this time slow-going, and its applications more limited than it is now. It wasn't until the spread of phototypesetting in the 1960s that Choc could be used efficiently for large-scale applications. Before then, the most practical way to get your name on your business was to have it painted by hand.

"New York was just a sign painter's heaven until designers started getting involved," Mr. Heller said. "There were one or two or three sign producers that did all the signs, and they worked from a fairly consistent stylebook, like going into a stationery store and looking for a wedding sample."

![Hana Sushi at 211 7th Avenue in Manhattan. Vincent Tullo for The New York Times](./@imgs/NY_Font/10.jpg)
![Pad Thai at 409 8th Avenue in Manhattan. Vincent Tullo for The New York Times](./@imgs/NY_Font/11.jpg)
![Nha Trang Centre at 148 Centre Street in Manhattan. Vincent Tullo for The New York Times](./@imgs/NY_Font/12.jpg)
![Ilan Shoes at 80-01 Roosevelt Avenue in Queens. Vincent Tullo for The New York Times](./@imgs/NY_Font/13.jpg)

Paul Boegemann was trained as one such sign painter — he is the proprietor of [Paul Signs](https://paulsignsny.com/) [Inc](https://paulsignsny.com/)., in Greenwood Heights, Brooklyn — and has been in business for over 35 years.

"I learned how to hand letter signs and trucks," Mr. Boegemann said. "We started with a simple pencil and paper. We designed what we wanted and used the styles we wanted and knew how to apply."

Right around the time Mr. Boegemann got into the business, the first computerized vinyl printing machines became efficient alternatives to hand-lettered signage. One was the Gerber IVB, also known as the Signmaker.

"I bought the second Signmaker in N.Y.C. back in 1984, $10,000 with three fonts and 10 colors of vinyl," Mr. Boegeman said. "The computer makes everything faster and cheaper.

"Over the years, more computer-generated advances made more fonts and designs and sizes," he added. "Everyone got into the sign and printing industry. You did not have to have any talent or skills."

Asked if he's ever made a sign in Choc, Mr. Boegemann said: "This style is a 'Bastard' letter style."

"I hate these styles and do not consider them as sign advertising. It is all graffiti."

![](./@imgs/NY_Font/15.jpg)

### How Did Choc Get Here?

And yet, Choc is everywhere. But the reasons for its prevalence are something of a mystery.

There's an abundance of Arial (Spin City Cleaners in the East Village), as well as more kitschy options like Brush Script (Economy Candy in the Lower East Side) or Impact (5th Ave. Gourmet Delight, on a corner in Park Slope). These typefaces have long been available in computer operating systems, which would account in part for their proliferation.

This wasn't precisely the case with Choc. In the early 1990s it was packaged with CorelDraw, a vector graphics editor, but under a different name: Staccato 555. CorelDraw, according to Mr. Frere-Jones, "would have been more common at a sign-making shop, being cheaper and Windows-based."

At any rate, Choc is more common than its limited availability may indicate. It's used consistently as well, and with evident purpose.

This could have to do with what Choc evokes. For some it bears a resemblance to the calligraphic forms of Asian writing systems.

However, it's disputed as to whether Choc was a direct homage to these styles. According to Ms. Chamaret, José Mendoza, Excoffon's assistant at the time of Choc's production, said that the letterforms were drawn in outlines. "Never with a brush\!" she said.

Even so, and just as pizzerias favor color schemes that recall the Italian flag, or how the names of Irish pubs rely on Gaelic-looking letters, Choc has come to signify Asia.

John Chen, a New York-based restaurateur, claims to be among the first to have used Choc on signs in New York. (The front of Mr. Chen's 25-year-old J.J. Garden, a Chinese restaurant in Jackson Heights, is emblazoned in Choc.) "When I wanted to use these letters, everyone thought I was crazy," he said, speaking in Mandarin.

![Aji Sushi at 201 5th Avenue in Brooklyn. Vincent Tullo for The New York Times](./@imgs/NY_Font/16.jpg)
![Kumo II at 512 Court Street in Brooklyn. Vincent Tullo for The New York Times](./@imgs/NY_Font/17.jpg)
![Rizo's Beauty Salon at 955 4th Avenue in Brooklyn Vincent Tullo for The New York Times](./@imgs/NY_Font/18.jpg)
![](./@imgs/NY_Font/19.jpg)

"It's abstract. It's modern. It's artistic," Mr. Chen continued. "It just gives you that certain feeling."

There's no denying that Choc has become a typographical shorthand for Asian-themed restaurants. Imagine a sushi bar adorned in Helvetica, and it may not seem as authentic, or as appetizing.

"Having been used for a particular purpose," said Mr. Frere-Jones, referring to the contemporary applications of Excoffon's typefaces, "it starts to take on a bit of that association, which encourages that association to be repeated, which just makes it stronger."

It's true. Excoffon's expressive, 60-year-old heavyweight script endures throughout all five boroughs: Ginger Root, on the Upper East Side; Sushi Q, in the Bronx; Sakura 6, in Greenpoint (and Natural Cleaners right across the street); Amy's Restaurant, in Inwood; Sumo Teriyaki & Sushi, in Williamsburg (which is set in an excess of Choc); A Sushi in Forest Hills; Genki Sushi in Staten Island; and, in the heart of Chinatown, Deluxe Food Market.

Here and in countless other instances Choc can be a welcome familiarity. Consider Kami Asian, on Flatbush Avenue near Grand Army Plaza. It's nestled between a laundromat and a Mexican restaurant, and it's the more striking of the three storefronts: on one side is the Mexican restaurant's folksy aesthetic (its name is ornamented with a sombrero), and on the other the laundromat's staid sans-serif. In between is "Kami," set in gracefully spaced Choc capitals, which at night cast an ethereal glow.

![](./@imgs/NY_Font/20.jpg)

Jeffrey E. Singer contributed reporting.
Produced by Jeffrey Furticella, Meghan Louttit and Rumsey Taylor.
