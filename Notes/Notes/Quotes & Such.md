# Quotes & Such

“As a rule, impeccable taste springs partly from inborn sensitivity: from feeling. But feelings remain rather unproductive unless they can inspire a secure judgment. Feelings have to mature into knowledge about the consequences of formal decisions. For this reason, there are no born masters of typography, but self-education may lead in time to mastery.” - **Jan Tschichold**, *The Form of the Book: Essays on Morality of Good Design*

More things I found after some spring cleaning that I had no other place for. 

![Quotes & Such](./@imgs/quotes.jpg)
