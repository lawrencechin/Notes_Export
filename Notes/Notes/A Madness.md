# A Madness

Jadon Sancho, of *Manchester City*, details a new form of mental illness:

![A madness](./@imgs/a madness.jpg)

If you have it, flaunt it!
