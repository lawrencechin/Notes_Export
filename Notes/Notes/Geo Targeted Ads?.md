# Geo Targeted Ads?
![](./@imgs/geo_target.jpg)

Noticed this is the **Guardian**. The comment doesn't seem to be in relation to the rest of the thread whilst the comment is a little to close to my geographic location. One wonders if any company generates comments for people who don't actually contribute, *lurkers*, introducing a subtler form of advertising.