# [The Lewis Model – Dimensions of Behaviour](https://www.crossculture.com/latest-news/the-lewis-model-dimensions-of-behaviour/)

*Posted on:* June 22nd, 2015 *by* cuco

Up to the middle of the 20th century, the scrutiny, analysis and comparative studies of the world's cultures were largely matters for academicians. Some knowledge of the subject was helpful in our travels abroad or when welcoming foreign guests to our shores.

The globalisation of world business in the last 5 decades has heralded in an era when cultural differences have become vitally important to leaders, managers and executives in the world's international and multinational companies. The complexities of merging corporate cultures, issues of leadership, planning, decision-making, recruitment and task assignment are all compromised by the nation-traits of the people involved. What allowances must be made when outlining organisational culture? Where can one look for guidelines?

One of the great dilemmas in analysing a person's cultural profile and deciding where to fit him or her into an existing organisation is how to choose cultural dimensions to create an understandable assessment.

Several dozen cross-cultural experts have proposed such dimensions. None has yet succeeded in capturing the whole field. The best-known models are:

**Edward Hall,** who classified groups as mono-chronic or poly-chronic, high or low context and past- or future-oriented.

**Kluckholn** saw 5 dimensions – attitude to problems, time, Nature, nature of man, form of activity and reaction to compatriots.

**Hofstede's** 4-D model looked at power distance, collectivism vs. individualism, femininity vs. masculinity and uncertainty avoidance. Later he added long-term vs. short-term orientation.

**Trompenaars'** dimensions came out as universalist vs. particularist, individualist vs. collectivist, specific vs. diffuse, achievement-oriented vs. ascriptive and neutral vs. emotional or affective.

**Tönnies** dwelt on *Gemeinschaft* vs. *Geselleschaft* cultures.

**The Lewis Model** is the latest to gain world-wide recognition, being developed in the 1990s and articulated in Richard Lewis's blockbuster, *When Cultures Collide* (1996), which won the US Book of the Month Award in 1997. Lewis, after visiting 135 countries and working in more than 20 of them, came to the conclusion that humans can be divided into 3 clear categories, based not on nationality or religion but on BEHAVIOUR. He named his typologies *Linear-active, Multi-active* and *Reactive*.

Lewis considered that previous cross-culturalists, in accumulating the multiplicity of dimensions listed in the preceding paragraph, ran the risk of creating confusion for those who sought clarity and succinctness. Moreover, he pointed out that the experts' preoccupation with north/south, mono-chronic/poly-chronic dichotomies, had caused them to overlook or ignore the powerful Asian mindset (comprising, in fact, half of humanity). He named this behavioural category *Reactive*, thereby creating a model that is essentially tripartite and cites the following characteristics:

![characteristics](./@imgs/lewis_model/9ee1c5f1bd96ac71bf18e4f30e25e0331224ff24.jpg)

The **Linear-active** group is easily identified. It comprises: the English-speaking world – North America, Britain, Australia and New Zealand, and Northern Europe, including Scandinavia and Germanic countries.

The **Reactive** group is located in all major countries in Asia, except the Indian sub-continent, which is hybrid.

The **Multi-actives** are more scattered: Southern Europe, Mediterranean countries, South America, sub-Saharan Africa, Arab and other cultures in the Middle East, India and Pakistan and most of the Slavs. Though these cultures are wildly diverse, geographically and in their religions, beliefs and values, they can be categorised as a group, as **behaviourally** they follow the same pattern with the following traits and commonalities: emotion, talkativeness, rhetoric, drama, eloquence, persuasion, expressive body language, importance of religion or creed, primacy of family bonds, low trust societies, unpunctuality, variable work ethic, volatility, inadequate planning, capacity for compassion, collectivism, relationship-orientation, situational truth, dislike of officialdom, tactility, sociability, nepotism, excitability, changeability, sense of history, unease with strict discipline

**NB** While the three types are distinctive, each possesses behavioural elements from the other two categories. It is a question of which one is **dominant**. Many individuals deviate from the national type in a work situation e.g. engineers and accountants tend to be Linear, sales people Multi-active, lawyers and doctors Reactive.

The Lewis Model is based on data drawn from 50,000 executives taking residential courses and more than 150,000 online questionnaires to 68 different nationalities and has produced the following tripartite comparison according to country.

![Screen Shot 2015-06-08 at 12.48.56](./@imgs/lewis_model/a0b505a3201bc83c61af4ff8c47fa95bc78fd165.jpg)

The questionnaire provides us with individual cultural profiles, which are displayed in the following manner.

![Fig. 2 Lewis Model triangle pinpointing individual cultural profiles](./@imgs/lewis_model/df5e1ad6170f89eb7f02ba406ad0f42f8152e534.jpg)

The location of each individual shows how close he or she is in behaviour or affinity to different cultures.

How does this information help training officers, headhunters or others engaged in the placement of new recruits in the company structure? After assessment, the individual's cultural profile is pinpointed inside the triangle, showing how close or how far it is to the world's major cultural groups. It indicates not only how much affinity their behaviour has to that of other countries but also shows their similarity to or deviation from their own national norm, as well as their compatibility with other people tested. This is particularly useful if members of a proposed team are tested simultaneously.

![Fig. 3 Lewis Model pinpointing group of engineers](./@imgs/lewis_model/3285ac50968cf6f058522ea9b2440049b53438fe.jpg)

Figure 3 shows the analysis of a compact group of engineers in a technical company, where two people show an affinity with Multi-active cultures, but no one likely to function comfortably in an Asian environment.

![Fig. 4 Cultural profiles of a group of HR officers](./@imgs/lewis_model/750040125da654e5083974ad4087e0531554a046.jpg)

Figure 4 shows a group of HR officers with a concentration of profiles in a central position (good for HR work and mediation roles).

![Fig. 5 Lewis Model pinpointing search for ice-cream sales manager](./@imgs/lewis_model/08235c7a53d9e020de103e371a1d3e0c9771730a.jpg)

Figure 5 shows an analysis conducted in the search for a person leading Unilever's ice cream sales in South America (a huge job). The successful candidate (in red) was in fact an Indian national.

-----

The Lewis Model, born in an era of rampant globalisation of business, is particularly appropriate for assessing an individual's likely performance in a commercial role. The design of the questionnaire is based on business situations. The nomenclature of the typologies is succinct: Linear-active, Multi-active, Reactive.

A Training Officer, on being told that "Candidate A is basically monochronic and low-context but high on uncertainty avoidance, has a tendency towards collectivism and femininity and is past-oriented," may well ask, "What shall I do with him?"

If the description is Linear-active, Multi-active or Reactive, the answer is clear and succinct:

**Linear-Actives** are task-oriented, highly-organized planners, who complete action chains by doing one thing at ta time, preferably in accordance with a linear agenda.

**Multi-Actives** are emotional, loquacious and impulsive people who attach great importance to family, feelings, relationships, people in general. They like to do many things at the same time and are poor followers of agendas.

**Reactives** are good listeners, who rarely initiate action or discussion, preferring first to listen to and establish the other's position, then react to it and form their own opinion.

Where and when do we need these types of people?

### We Need Each Other\!

![Screen Shot 2015-06-22 at 11.13.55](./@imgs/lewis_model/aed0618cc4e697077343c7727bced0d08db8bfca.jpg)
