# [Guardian of the GPL: Online advertising is becoming “a perfect despotism”](http://arstechnica.co.uk/information-technology/2016/05/eben-moglen-gpl-online-advertising-is-becoming-a-perfect-despotism/)

![Facebook Stalin Poster](http://cdn.arstechnica.net/wp-content/uploads/sites/3/2016/05/facebook-stalin-poster-1.jpg)

Time is running out to prevent complete totalitarian dictatorship until the end of human civilisation, Eben Moglen, the guardian of the GPL, told Ars in an interview.
But let's rewind a bit. Earlier this month, Moglen and Mishi Choudhary, both of the Software Freedom Law Center, told a packed crowd at the [Re:publica](https://re-publica.de/en) conference in Germany about the worrying outlook for *Homo sapiens*.
"This is the last generation in which the human race gets a choice," Moglen said during the duo's [opening keynote](https://re-publica.de/en/16/session/opening-keynote-last-kilometer-last-chance) for the media and technology conference. "Most of the human race doesn't know what the choice is, and if we here, who do know, do not help them understand," he said, "if we don't give them proof of concept plus running code, the revolution becomes impossible."
Moglen is a Columbia law professor and a well-known stalwart of the free software movement. As general counsel to the Free Software Foundation for many years, he helped Richard M. Stallman draft the GPLv3. He received the EFF's Pioneer Award in 2003, and is the author of The [dotCommunist Manifesto](http://moglen.law.columbia.edu/publications/dcm.html), among many other works. Choudhary is the SFLC’s legal director and previously practised as litigator before the High Court and Supreme Court in India.

![Eben Moglen](http://cdn.arstechnica.net/wp-content/uploads/sites/3/2016/05/eben-moglen.jpg)

"Our speech in Berlin was designed to explain the two primary forms of threat to human autonomy," Moglen told Ars later in an interview. "One, the ceaseless behaviourist calculation of the attention economy, and two, the piggybacking of despotism, or at least state-supported, state-activated social control, on top of that network."
The problem, he said, begins with advertising.

## "Totalitarianism on the cheap"

Moglen argued in Berlin that online advertising enables totalitarian control: "We are building a perfect despotism, and we think we're only improving the efficiency of advertising."
The problem, he explained to Ars, is that the Internet today undermines human autonomy, and seeks to control our behaviour. "The idea that advertising is still primarily about displaying an advertisement is part of the culprit here."
Major platforms like Facebook, Google, and Twitter, which Moglen collectively refers to as "the machine," are experimenting with readers billions of times a day in order to capture our attention and to change our behaviour, without our understanding and rarely with our informed consent.
"20th-century totalitarianism didn't scale very well," he told the audience. "It needed lots and lots of people, it needed lots and lots of fear which could only be produced by episodes of serious violence. People had to disappear, people had to be broken, they had to be afraid."
"21st-century totalitarianism solves those problems," he said. "You don't need so many people any more. The platforms do the work for you."
The business incentives keep the surveillance ticking over, Choudhary explained. "Surveilling and predicting human behaviour is the new economy," she said. "It also means more effective tyranny, an increasingly inescapable prison for the human race."
The machine does not treat us as human beings with minds and free will, Moglen continuted, but as "stimulus and response correlations" to be sorted and sold as "mineable human attention."
So valuable are we as mineable attention, he said, that the platforms want to wire up the rest of humanity just to get access to their data too.

## Here, let us transport these packets for you...

Zero-rating targeted by regulators overseas while it remains legal in US. Moglen: "It is worth transporting people's packets in order to get their attention and experiment with their behaviour. Self-driving cars are worth building and deploying so people can be spending time with Facebook while commuting. It used to be in the 20th century you put a radio in the car. That won't work any more, because you need a two-way connection... you need human attention and response to stimuli for these media to work."
Choudhary agreed, applauding [India's decision to reject Facebook's "free basics" program](http://arstechnica.co.uk/tech-policy/2016/02/facebooks-free-internet-app-banned-by-indias-new-net-neutrality-rule/), and encouraged other countries to do likewise. Facebook's attitude, she said, is "Let's get all of these people online because their data is what works for us."
In 10 years time most of the human race will be connected to the internet. But, Moglen warned, if we do nothing, we will become nothing more than objects to be controlled by powerful, opaque corporations.
"This should be the greatest moment of liberation, of social justice, the greatest economic opportunity in the history of mankind," he said. "But it won't be," because the governments of the world "can't resist riding the pig."

## Government piggybacking

"Ridiculously optimistic" machine learning algorithm is "completely bullshit," says expert.As Edward Snowden's revelations made clear, the US government demands access to user data from Google, Facebook, Twitter, and many others, with or without the companies' permission, and then shares that data with other governments around the world.
"We accept the commercial surveillance because it is fundamentally only being carried out for profit," Moglen said in Berlin. "Such a thin motive, so lacking in malevolence, that we can accept that we should go along with it, after all they're like us, they're only trying to make a living."
But, he pointed out, what if the government itself had proposed building just such a surveillance apparatus?
"We would have said, 'it is without any question the evil that we fought in the 20th century, that for which we died, or suffered millions of others to die, to prevent that from happening'."
The audience reaction to the keynote address was, by and large, one of depression and deep introspection. Moglen acknowledged as much to Ars, saying "It was a difficult speech for people because it didn't end on an up note."
"Unfortunately," he added, "we don't have too much longer, and the people who care about it must not get too depressed by an accurate description of the problem."
Instead, he argued, we must fight to regain our privacy in order to regain our lost freedom, and solving that problem begins with changing how we discuss privacy.
