# Quote Of The Day

"When I was in **Monaco** I thought it would be good to have a family of footballers. So I made sure my brother **Rotimi** got into a football academy in **France**. Within a few months; *out of 27 players*, he stole **21 phones**" 
- *Emmanuel Adebayor* takes to *Facebook* to rant about his family.

----

"That was in the past – we're in the future now."
- *David Beckham*

---- 

"And Seaman, just like a falling oak, manages to change direction."
- *John Motson*

---- 

"I never comment on referees and I'm not going to break the habit of a lifetime for that prat."

"Well, Clive, it's all about the two Ms - movement and positioning."
- *Ron Atkinson*

---- 

 "If history repeats itself, I should think we can expect the same thing"
- *Terry Venables*

---- 

"I always used to put my right boot on first, and then obviously my right sock."
- *Barry Venison*

---- 

"I'm as happy as I can be - but I have been happier."
- *Ugo Ehiogu*

---- 

"Playing with wingers is more effective against European sides like Brazil, than English sides like Wales."
- *Ron Greenwood*

---- 

With Joey Barton, you know what to expect. He's going to come strong in the tackle and come in your face" 
- *Philippe Senderos*

---- 

"He's carrying his left leg, which to be honest is his only leg" 
- *Steve Coppell*

---- 

"Look at those olive trees! They're 200 years old - from before the time of Christ." 

"What can I say about Peter Shilton? Peter Shilton is Peter Shilton, and he has been Peter Shilton since the year dot"

"We're flying on the Concorde. That'll shorten the distance — that's self-explanatory."

Alan Brazil: "I'm delighted to say we've got Sir Bobby Robson on the
end of the phone, fresh from getting his knighthood at Buckingham Palace.
Bobby, terrific news."
Sir Bobby Robson: "What is?"
Brazil: "You know, getting the old sword on the shoulder from Prince
Charlie."
Sir Bob: Eh? *…Long pause…* "Oh yeah… well, it was a day I'll never
forget."

Sir Bobby to Bryan Robson: "Good morning, Bobby."
Bryan: "You're Bobby, I'm Bryan!"
- *Bobby Robson*

---- 

"They compare Steve McManaman to Steve Highway and he's nothing like him, but I can see why – because he's a bit different."

"They're the second best team in the world, and there's no higher praise than that."

"I came to Nantes two years ago and it's much the same today, except that it's totally different."

"I'm not disappointed – just disappointed"

"I don't think there's anyone bigger or smaller than Maradona."

"He's using his strength. And that is his strength – his strength."
- *Kevin Keegan*

---- 
