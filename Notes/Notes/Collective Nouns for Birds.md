# [Collective Nouns For Birds](https://www.britishbirdlovers.co.uk/articles/collective-nouns-for-birds)

One of the most remarkable things about the animal kingdom and one of the many crazy things about the English language is the variety of collective nouns that all mean 'group'.  
  
![Twack Of Ducks](./@imgs/Collective_Birds/0713437559cc19a6f75eef2a7b9c01a6f1172c4d.jpg)

Below is a list of collective nouns for birds - if you know of any more then please get in touch using the [contact form](https://www.britishbirdlovers.co.uk/contact).  
  
-----

## A

albatrosses - a rookery of albatrosses
albatrosses - a weight of albatrosses
auks - a raft of auks  
avocets - a colony of avocets

![Avocet](./@imgs/Collective_Birds/c1b805f3014240706c924e5512c8c248febde22b.jpg)  
  
*A colony of avocets*

-----

## B

bitterns - a pretence of bitterns  
bitterns - a sedge of bitternsbitterns - a siege of bitterns  
bobolinks - a chain of bobolinks  
bullfinches - a bellowing of bullfinches  
buzzards - a wake of buzzards  

![Bullfinch](./@imgs/Collective_Birds/8005e5e5e2c7b727ff04f84405eb8da0a5ebf840.jpg)  
*A bellowing of bullfinches*

-----

## C

capercaillie - a tok of capercaillie  
capons - a mews of capons  
chicks - a clutch of chicks  
chickens - a peep of chickens  
choughs - a clattering of choughs  
coots - a covert of coots  
coots - a raft of coots  
cormorants - a flight of cormorants  
cranes - a herd of cranes  
cranes - a sedge of cranes  
crows - a horde of crows  
crows - a hover of crows  
crows - a mob of crows  
crows - a murder of crows  
crows - a muster of crows  
crows - a parcel of crows  
crows - a parliament of crows  
crows - a storytelling of crows  
curlews - a herd of curlews  

![A Murder Of Crows](./@imgs/Collective_Birds/88be89866abb6a6b1fac8c89adea1e3b8b0b8087.jpg)  
*A murder of crows*

-----

## D

dotterel - a trip of dotterel  
doves - a bevy of doves  
doves - a dole of doves  
doves - a flight of doves  
doves - a piteousness of doves  
doves - a pitying of doves  
ducks (diving) - a dopping of ducks  
ducks (flying) - a plump of ducks  
ducks (on water) - a paddling of ducks  
ducks - a badling of ducks  
ducks - a flush of ducks  
ducks - a raft of ducks  
ducks - a sord of ducks  
ducks - a team of ducks  
ducks - a twack of ducks  
dunlin - a fling of dunlins  

![Mallard Duck](./@imgs/Collective_Birds/3c1aa91c2eab512fcdcac2c85b0503a28a121c6a.jpg)  
*A twack of ducks*

-----

## E

eagles - a congress of eagles  
eagles - a convocation of eagles  
emus - a mob of emus  

![Golden Eagle](./@imgs/Collective_Birds/012d44fe155ac7baece0a56bcf2c92f3ff957051.jpg)  
*A congress of eagles*

-----

## F 

falcons - a cast of falcons  
finches - a charm of finches  
finches - a trembing of finches  
finches - a trimming of finches  
flamingoes - a flamboyance of flamingoes  
flamingoes - a stand of flamingoes  

![Peregrine Falcon](./@imgs/Collective_Birds/1b5318a04f06afb04abb56caccc95ffb04edfd5f.jpg)  
*A cast of falcons*

-----

## G
godwits - an omniscience of godwits  
godwits - a prayer of godwits  
godwits - a pantheon of godwits  
goldfinches - a drum of goldfinches  
goldfinches - a troubling of goldfinches  
goldfinches - a charm of goldfinches  
goldfinches - a chirm of goldfinches  
geese - a gaggle of geese  
geese (flying) - a wedge of geese  
geese - a nide of geese  
geese - a skein of geese  
geese (on water) - a plump of geese  
goshawks - a flight of goshawks  
grouse - a covey of grouse  
grouse - a lek of grouse  
grouse - a pack of grouse  
guillemots - a bazaar of guillemots  
guinea fowl - a confusion of guinea fowl  
gulls - a colony of gulls  
gulls - a screech of gulls  

![Goldfinch](./@imgs/Collective_Birds/afeb079a53b300080fc99fcc796a3407c3acd4af.jpg)  
*A charm of goldfinches*

-----

## H

hawks (tame) - a cast of hawks  
hawks (tame) -a lease of hawks  
hawks - a kettle of hawks  
hens - a brood of hens  
herons - a siege of herons  
hummingbirds - a charm of hummingbirds  

![Grey Heron](./@imgs/Collective_Birds/8e5b4ba2ed51e0b611399be2fabfd9d66e2b50c1.jpg)  
*A siege of herons*

-----

## J

jackdaws - a clattering of jackdaws  
jackdaws - a train of jackdaws  
jays - a band of jays  
jays - a party of jays  
jays - a scold of jays  

![Jackdaw](./@imgs/Collective_Birds/15326414f55369792ea71ed6f5341d79c4c3b684.jpg)  
*A clattering of jackdaws*  
  
-----

## L

lapwings - a desert of lapwings  
larks - a bevy of larks  
larks - an exaltation of larks  
larks - an exalting of larks  

![Lapwing](./@imgs/Collective_Birds/e9c63143779c2917cf68b4ba9e51d290493e95e1.jpg)  
*A desert of lapwings*

-----

## M

magpies - a conventicle of magpies  
magpies - a gulp of magpies  
magpies - a mischief of magpies  
magpies - a tidings of magpies  
magpies - a tittering of magpies  
mallards - a sute of mallards  
mallards - a sord of mallards  
martins - a richness of martins  
mudhen- a fleet of mudhen  

![Magpie](./@imgs/Collective_Birds/c7f638222d8210e2452fed281d934d5d3888e315.jpg)  
*A tidings of magpies*

-----

## N

nightingales - a watch of nightingales  

![Nightingale](./@imgs/Collective_Birds/3112904779f122ab28ebebdff6149c7ec0b66287.jpg)  
*A watch of nightingales*

-----

## O

owls - a parliament of owls  
owls - a stare of owls  
owls - a study of owls  
owls - a wisdom of owls  
oystercatchers - a parcel of oystercatchers  

![Tawny Owl](./@imgs/Collective_Birds/11d027af3d4fb47a7ec5de29795d4c37c9b2e7a5.jpg)  
*A parliament of owls*

-----

## P

parrots - a company of parrots  
parrots - a prattle of parrots  
parrots - a pandemonium of parrots  
partridges - a covey of partridges  
peacocks - a muster of peacocks  
peacocks - a pride of peacocks  
peacocks - an ostentation of peacocks  
pelicans - a pod of pelicans  
pelicans - a scoop of pelicans  
pelicans - a squadron of pelicans  
penguins - a colony of penguins  
penguins - a creche of penguins  
penguins - a huddle of penguins  
penguins - a parcel of penguins  
penguins - a rookery of penguins  
pheasants - a bouquet of pheasants  
pheasants - a covey of pheasants  
pheasants - a nide of pheasants  
pheasants -a nye of pheasants  
pigeons - a kit of pigeons  
pigeons - a loft of pigeons  
plovers - a congregation of plovers  
ptarmigan - a covey of ptarmigans  

![Grey Partridge](./@imgs/Collective_Birds/fbcb8e0a50d89fb7267994e75633f3d861291054.jpg)  
*A covey of partridges*

-----

## Q

quail - a bevy of quail  
quail - a covey of quail  

![Quail](./@imgs/Collective_Birds/ba52633e3cf3a27e1d6ba9a047f1a033b73d2ae3.jpg)  
*A bevy of quail*

-----

## R

ravens - an unkindness of ravens  
robins - a blush of robins  
robins - a bobbin of robins  
robins - a breast of robins  
robins - a carol of robins  
robins - a gift of robins  
robins - a reliant of robins  
robins - a riot of robins  
robins - a rouge of robins  
robins - a round of robins  
robins - a ruby of robins  
rooks - a building of rooks  
rooks - a clamour of rooks  
rooks - a parliament of rooks  
ruffs - a hill of ruffs  

![Raven](./@imgs/Collective_Birds/fe443bc978f8ba24e70e31cc391190361d5b897e.jpg)  
*An unkindness of ravens*

-----

## S

sandpipers - a bind of sandpipers  
sandpipers - a cluster of sandpipers  
sandpipers - a contradiction of sandpipers  
sandpipers - a fling of sandpipers  
sandpipers - a hill of sandpipers  
sandpipers - a time-step of sandpipers  
sea fowl - a cloud of sea fowl  
seagulls - a colony of seagulls  
seagulls - a flock of seagulls  
sheldrakes - a doading of sheldrakes  
skylarks - an exultation of skylarks  
snipe - a walk of snipe  
snipe - a wisp of snipe  
sparrows - a host of sparrows  
sparrows - a quarrel of sparrows  
sparrows - a ubiquity of sparrows  
starlings - a murmuration of starlings  
storks - a muster of storks  
storks - a phalanx of storks  
swallows - a flight of swallows  
swallows - a gulp of swallows  
swans - a gaggle of swans  
swans (flying) - a wedge of swans  
swans - a bank of swans  
swans - a bevy of swans  
swans - a whiteness of swans  
swans - a herd of swans  
swans - an eyrar of swans  
swans - a gargle of swans  

![House Sparrow](./@imgs/Collective_Birds/b5a1799d5be33a8bc24837db94bfed1c6d0790b5.jpg)  
*A ubiquity of sparrows*

-----

## T

teal - a diving of teal  
teal - a spring of teal  
thrushes - a mutation of thrushes  
turkeys - a raffle of turkeys  
turkeys - a rafter of turkeys  
turtle doves - a dole of turtle doves  
turtle doves - a pitying of turtle doves  

![Turtle Dove](./@imgs/Collective_Birds/5b83676fce201289fc49af2bdd5b68b8efccabbf.jpg)  
*A dole of turtle doves*

-----

## W

waterfowl - a knob of waterfowl  
waterfowl - a plump of waterfowl  
wigeon - a coil of wigeon  
woodpeckers - a descent of woodpeckers  
woodcocks - a fall of woodcocks  
wrens - a herd of wrens  
Attract bees, butterflies, insects and moths to your garden as well as larger mammals such as bats and hedgehogs.

![Wigeon](./@imgs/Collective_Birds/4fb90c30c4af4dcf10eb1b0f64bfd1cdbf3130fd.jpg)  
*A coil of wigeon*

-----
