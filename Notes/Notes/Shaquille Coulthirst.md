# Shaquille Coulthirst

![Shaquille](./@imgs/shaquille.jpg)

Having moved to the club from Spurs in January 2016, Coulthirst made 19 appearances and scored twice, one of which came in the FA Cup at West Brom. With a wealth of football league experience under his belt with Wigan and Southend, he is an important player for the club and is able to play as both a central striker and as a winger with pace to burn and an ability to hold the ball up for his team.
[Read more](http://www.theposh.com/team/player-profile/shaquile-coulthirst/17#hIWrXdAASovIHTBK.99)