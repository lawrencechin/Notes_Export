# We have an APB on an MC killer, looks like the work of a master…

When several people, runners, named Lawrence turn up at the same time and the same place it piques the interest of the authorities.

![Congregation of Lawrencí](./@imgs/Google_Maps_July_2018/A_Congregation_of_Law.jpg)

Thoughts quickly turn to surveillance to see what these guys are involved with…

There he is! He's casing the Science Academy.

![Science Academy](./@imgs/Google_Maps_July_2018/Running_Man_2.jpg)

I think we've been spotted…

![Looking this way](./@imgs/Google_Maps_July_2018/Running_Man_3.jpg)

![Strange posture](./@imgs/Google_Maps_July_2018/Running_Man_4.jpg)

He's making a break for it.

![Follow the arrow](./@imgs/Google_Maps_July_2018/Running_Man_5.jpg)

And calling his handler (whose name probably is Lawrence).

![Three similar images](./@imgs/Google_Maps_July_2018/Running_Man_1.jpg)

![The second](./@imgs/Google_Maps_July_2018/Running_Man_6.jpg)

![And the last](./@imgs/Google_Maps_July_2018/Running_Man_7.jpg)

Get him! Watling Street, everyone to King Harry sharpish.

![Bye Jack](./@imgs/Google_Maps_July_2018/Running_Man_8.jpg)

![We really shook the pillars of hell…](./@imgs/Google_Maps_July_2018/Running_Man_9.jpg)

![Bye Jack](./@imgs/Google_Maps_July_2018/Running_Man_10.jpg)
