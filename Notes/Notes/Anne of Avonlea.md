# [Anne of Avonlea](http://www.amazon.co.uk/exec/obidos/redirect?link_code=ur2&tag=histoficti-21&camp=1634&creative=6738&path=ASIN%2F0140367985%2Fqid%3D1133397784%2Fsr%3D1-2%2Fref%3Dsr_1_3_2)

Lucy Maud Montgomery - excerpt

A tall, slim girl, "half-past sixteen," with serious gray eyes and hair which her friends called auburn, had sat down on the broad red sandstone doorstep of a Prince Edward Island farmhouse one ripe afternoon in August, firmly resolved to construe so many lines of Virgil. 
But an August afternoon, with blue hazes scarfing the harvest slopes, little winds whispering elfishly in the poplars, and a dancing slendor of red poppies outflaming against the dark coppice of young firs in a corner of the cherry orchard, was fitter for dreams than dead languages.
