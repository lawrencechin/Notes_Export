# Isaac Asimov spotted this coming many years ago:


“*Anti-intellectualism has been a constant thread winding its way through our political and cultural life, nurtured by the false notion that democracy means that 'my ignorance is just as good as your knowledge.*”

And it's gotten far worse since then.