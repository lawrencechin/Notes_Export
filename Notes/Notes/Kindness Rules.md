# Kindness Rules.

“Nothing can make our life, or the lives of other people, more beautiful than perpetual kindness.”
  - Leo Tolstoy
