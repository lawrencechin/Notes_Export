# Would you solve it? Would you get into Oxbridge?
[Original Link](https://www.theguardian.com/science/2016/nov/07/can-you-solve-it-would-you-get-into-oxbridge)

<pre style="color : red">

   /\        /\        /\
  /  \      /  \      /  \
 ------    ------    ------
 |[ * \    |{ > \    |( ~ \
 |    -    |    -    |    -
 ------    ------    ------
 Alice     Bob       Charlie
 
 </pre>

Alice, Bob and Charlie are well-known expert logicians; they always tell the truth. They are sat in a row, as illustrated above. In each of the scenarios below, their father puts a red or blue hat on each of their heads. Alice can see Bob's and Charlie's hats, but not her own; Bob can see only Charlie's hat; Charlie can see none of the hats. All three of them are aware of this arrangement.

## Problems

1. Their father puts a hat on each of their heads and says: “Each of your hats is either red or blue. At least one of you has a red hat.” Alice then says “I know the colour of my hat.” What colour is each person's hat?
2. Their father puts a new hat on each of their heads and again says: “Each of your hats is either red or blue. At least one of you has a red hat.” Alice then says “I don't know the colour of my hat.” Bob then says “I don't know the colour of my hat.” What colour is Charlie's hat?
3. Their father puts a new hat on each of their heads and says: “Each of your hats is either red or blue. At least one of you has a red hat, and at least one of you has a blue hat.” Alice says “I know the colour of my hat.” Bob then says “Mine is red.” What colour is each person's hat?
4. Their father puts a new hat on each of their heads and says: “Each of your hats is either red or blue. At least one of you has a red hat, and at least one of you has a blue hat.” Alice then says “I don't know the colour of my hat.” Bob then says “My hat is red”. What colour is Charlie's hat?
5. Their father puts a new hat on each of their heads and says: “Each of your hats is either red or blue. Two of you who are seated adjacently both have red hats.” Alice then says “I don't know the colour of my hat.” What colour is Charlie's hat?

If you want to find the answers now - please submit your answers below. In each case write down only the colour of Charlie's hat.

## Answers
**Alice** > *Bob* > __*Charlie*__

### Problem 1
**Alice** must be wearing the red hat as that's the only hat that can be determined from the information given. The other two must be wearing blue hats allowing **Alice** to know for certain she is wearing the red hat that someone *must* have on.

*Answer* : Blue

### Problem 2
**Alice** doesn't know her hat colour so one of the other two must be wearing a red hat. *Bob* also doesn't know his hat colour so __*Charlie*__ must be wearing a red hat.

*Answers* : Red

### Problem 3
**Alice** sees two red hats so she knows hers is blue. *Bob* understands that **Alice** can only determine her hat colour in the event that his and __*Charlie's*__ hat are the same colour. He can see that __*Charlie's*__ hat is red so his must also be.

*Answer* : Red

### Problem 4
*Answer* : Blue

### Problem 5
*Bob* must have a red hat as he is in the middle of three. **Alice** can only be sure of her hat colour if __*Charlie*__ has a blue hat so he therefore must have a red hat. There is no stipulation that a blue hat must exist.
*Answer* : Red

Correct! 100% Let's be honest that was remarkably easy and the five problems were really just three: two of the questions are just inversions.
