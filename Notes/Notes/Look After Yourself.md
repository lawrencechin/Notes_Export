# Look After Yourself

![look after yourself](./@imgs/look_after_yourself.png)

1. Celebrate small things
1. Celebrate big things
1. Don't compare
1. Make time for exercise
1. Eat well
1. Look for the good
1. Stop being so hard on yourself
1. Have some rest. Don't feel guilty about it
1. Give yourself a pat on the back
1. Bed, early

*via Do Lectures*
