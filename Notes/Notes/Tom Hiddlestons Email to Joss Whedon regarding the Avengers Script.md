# Tom Hiddleston’s Email to Joss Whedon regarding the Avengers Script

Joss,
I am so excited I can hardly speak.
The first time I read it I grabbed at it like_ Charlie Bucket_ snatching for a golden ticket somewhere behind the chocolate in the wrapper of a **Wonka Bar**. I didn’t know where to start. Like a classic actor I jumped in looking for **LOKI **on every page, jumping back and forth, reading words in no particular order, utterances imprinting themselves like flash-cuts of newspaper headlines in my mind: “_real menace_”; “_field of obeisance_”; “_discontented, nothing is enough_”; “_his smile is nothing but a glimpse of his skull_”; “_Puny god_” …
… Thank you for writing me my _Hans Gruber_. But a _Hans Gruber_ with super-magic powers. As played by _James Mason_ … It’s high operatic villainy alongside detached throwaway tongue-in-cheek; plus the “_real menace_” and his closely guarded suitcase of pain. It’s grand and epic and majestic and poetic and lyrical and wicked and rich and badass and might possibly be the most gloriously fun part I’ve ever stared down the barrel of playing. It is just so juicy.
I love how throughout you continue to put _Loki_ on some kind of pedestal of regal magnificence and then consistently tear him down. He gets battered, punched, blasted, side-swiped, roared at, sent tumbling on his back, and every time he gets back up smiling, wickedly, never for a second losing his eloquence, style, wit, self-aggrandisement or grandeur, and you never send him up or deny him his real intelligence…. That he loves to make an entrance; that he has a taste for the grand gesture, the big speech, the spectacle. I might be biased, but I do feel as though you have written me the coolest part.
… But really I’m just sending you a transatlantic shout-out and fist-bump, things that traditionally British actors probably don’t do. It’s epic.

## Joss’s reply:

Tom,
this is one of those emails you keep forever. Thanks so much. It’s more articulate (_and possibly longer_) than the script. I couldn’t be more pleased at your reaction, but I’ll also tell you I’m still working on it … Thank you again. I’m so glad you’re pleased. Absurd fun to ensue.
Best, (_including uncharacteristic fist bump_), 
Joss. 