# Frankly Just A Bit Fucking Weird Dept.

Stuart Jeffries to Kevin Keegan – Selected Extracts (Guardian)

* “Surely footballing success is measured in silverware?", “Is it?” asks Keegan, brushing something from one of the thighs that once wreaked havoc on weaker defences. He looks over the double bed in ruminative silence.
* We’re sitting in two narrow tub chairs, one of which can scarcely contain the still firm musculature of the 60-year-old, 5ft 8in footballing superstar, whom adoring Hamburg fans nicknamed, in his late-70s pomp, “Mighty Mouse”.
* Keegan takes the apple, I neck the mineral water. He pulls down the shades, zips up the jacket, as if ready for the hit. The shades are unnecessary: outside it’s midsummer Manchester – fierce rain from leaden skies. He scampers off with an age-defying turn of pace. The dog? It is, like Kevin Keegan, a Yorkshire terrier.
