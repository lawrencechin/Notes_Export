# Epic Fails

I was sorting through a bunch of research images and found some that I couldn't find a place for. They tickled me though. I present them here for your pleasure.

![Epic Fails](./@imgs/epicfail.jpg)

And you should always blame any kind of *epic fail* on wizards…

![A Wizard wot done it](./@imgs/A_Wizard_Wot_Done_it.jpg)