# The Moon Does Not Fight.

“The moon does not fight. It attacks no one. It does not worry. It does not try to crush others. It keeps to its course, but by its very nature, it gently influences. What other body could pull an entire ocean from shore to shore? The moon is faithful to its nature and its power is never diminished.”
  - Deng Ming-Dao
