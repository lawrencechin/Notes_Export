#!/bin/bash

# Variables
TEMPLATE_DIR="templates"
MD_FILES="Notes"
STATIC_FILES="static"
OUTPUT="public"
TEMP="public/temp"

# receives a filename and directory as arguments
createHTML() {
    # Syntax styles :  kate, haddock, tango
    pandoc -s \
        --toc \
        --toc-depth=4 \
        --highlight-style=kate \
        --template="$TEMPLATE_DIR"/note_template.html \
        "$1" \
        -o "$OUTPUT/$2"
}

make_public() {
    if test -d ./public; then
        rm -rf ./public
    fi

    mkdir -p "$TEMP"
}

#recieves category name
make_menu_list() { 
    menu="$TEMP"/menu.txt
    list_item="<li class=\"menu_item\"><a href=\"#$1\">$1</a></li>"
    echo "$list_item" >> "$menu"
}

#recieves tag name and tag counter
make_tag_list() { 
    tag_list="$TEMP"/tags.txt
    tag_name=$( echo "$1" | sed -E 's/ /_/g' )
    list_item="<li class=\"tag_item $tag_name\"><a class=\"tag_$2\" href=\"$tag_name\">$1</a></li>"
    echo "$list_item" >> "$tag_list"
}

#recieves category
make_category_list() { 
    category_list="$TEMP"/"$1".txt
    cat_list_header="<h2 id=\"$1\">$1</h2>"
    echo "$cat_list_header" > "$category_list"
}

#recieves full path to note, cat file, note name, tag name and tag counter
fill_category_list() { 
    cat_list_item_1="<li class=\"category_list_item\"><a href=\""
    cat_list_item_2="\">"
    cat_list_item_3="</a>"
    cat_list_item_4="<a href=\""
    cat_list_item_5="\" class=\"cat_list_tag tag_$5\">"
    cat_list_item_6="</a>"
    cat_list_item_7="</li>"

    note_name=$( echo "$3" | sed -E -e 's/ /_/g' -e 's/\?//g' ) 
    tag_name=$( echo "$4" | sed -E 's/ /_/g' )

    link_partial="$cat_list_item_1$note_name.html$cat_list_item_2$3$cat_list_item_3"

    if test -n "$4"; then
        link_partial="$link_partial$cat_list_item_4$tag_name$cat_list_item_5$4$cat_list_item_6"
    fi

    echo "$link_partial$cat_list_item_7" >> $2

    createHTML "$1" "$note_name".html
}

process_external_links() { 
    cat_list_item_1="<li class=\"category_list_item\"><a href=\""
    cat_list_item_2="\" class=\"external\" target=\"_blank\" data-link=\""
    cat_list_item_2_1="\">"
    cat_list_item_3="</a>"
    cat_list_item_4="<a href=\""
    cat_list_item_5="\" class=\"cat_list_tag"
    cat_list_item_6="\">"
    cat_list_item_7="</a>"
    cat_list_item_8="</li>"
    link_partial="$cat_list_item_1"

    while IFS="|" read -r category name address tag; do
        note_name=$( echo "$name" | sed -E -e 's/ /_/g' -e 's/\?//g' ) 
        
        link_partial="$link_partial$note_name$cat_list_item_2$address$cat_list_item_2_1$name$cat_list_item_3"
        if test -n "$tag"; then
            tag_name=$( echo "$tag" | sed -E 's/ /_/g' )
            tag_number=$( grep "$tag_name" "$TEMP"/tags.txt | sed -E 's/.*(tag_[0-9]).*/\1/' )
            link_partial="$link_partial$cat_list_item_4$tag_name$cat_list_item_5 $tag_number$cat_list_item_6$tag$cat_list_item_7"
        fi

        echo "$link_partial$cat_list_item_8" >> "$TEMP"/"$category".txt
        link_partial="$cat_list_item_1"
    done < "$MD_FILES"/external_links.txt
}

run_through_files() {
    counter=1
    for category in "$MD_FILES"/F*/; do
        cat=$( basename "$category" )
        make_menu_list "$cat"
        make_category_list "$cat"

        for tag in "$category"*; do
            if test -d "$tag"; then
                tg=$( basename "$tag" )
                if test $counter -gt 4; then
                    counter=1
                fi
                make_tag_list "$tg" "$counter"

                for note in "$tag"/*; do
                    tag_note=$( basename "$note" .md )
                    fill_category_list "$note" "$TEMP/$cat.txt" "$tag_note" "$tg" "$counter"
                done
                counter=$(( counter + 1 ))
            else
                nt=$( basename "$tag" .md )
                    fill_category_list "$tag" "$TEMP/$cat.txt" "$nt"
            fi
        done
    done
}

make_index() {
    index="$OUTPUT"/index.html
    cat "$TEMPLATE_DIR"/header.html > "$index"
    cat "$TEMP"/menu.txt >> "$index"
    cat "$TEMPLATE_DIR"/tags.html >> "$index"
    cat "$TEMP"/tags.txt | sort >> "$index"
    echo "</ul></header>" >> "$index"
    echo "<div id=\"note_lists\">" >> "$index"

    rm "$TEMP"/menu.txt "$TEMP"/tags.txt

    for txt in "$TEMP"/*.txt; do
        echo "<ul class=\"category_list\">" >> "$index"
        cat "$txt" | sort >> "$index"
        echo "</ul>" >> "$index"
    done

    cat "$TEMPLATE_DIR"/footer.html >> "$index"
}

build() {
    make_public
    cp -r "$STATIC_FILES"/* "$OUTPUT"
    run_through_files
    process_external_links
    make_index
    rm -r "$TEMP"
}

build
