#!/bin/bash

# Variables
TEMPLATE_DIR="templates"
MD_FILES="Notes"
STATIC_FILES="static"
OUTPUT="public"

make_public() {
    if test -d ./public; then
        rm -rf ./public
    fi

    mkdir -p "$OUTPUT"
}

get_note() { 
    while true
        echo "Search for note… (single results are selectable | q to exit)"
        echo -n "> "
        read REPLY; do
        case "$REPLY" in
            [qQ] ) break ;;
            * ) find_note "$REPLY" ;;
        esac
    done
}

find_note(){ #args: user typed input
    result=$( fd -t f -e "md" --max-results 10 "$1" "$MD_FILES" )
    if test -n "$result"; then
        awk 'BEGIN {FS="\n"}{n=split($1,a,"/"); print a[n]}' <<< "$result"
        if [[ "$result" != *$'\n'* ]]; then
            while true
                echo -n "Create html preview of note [y or n]? "
                read -n 1 REPLY; do
                case "$REPLY" in
                    [yY] ) createHTML "${result}"; break ;;
                    * ) echo -e "\n"; break ;;
                esac
            done
        fi
    else
        echo "No results"
    fi
}

createHTML() { # receives a filename and directory as arguments
    # Syntax styles :  kate, haddock, tango
    note_name="${1##*/}"
    note_name_no_ext="${note_name%\.md}"
    make_public
    pandoc -s \
        --toc \
        --toc-depth=4 \
        --highlight-style=kate \
        --template="$TEMPLATE_DIR"/preview.html \
        "$1" \
        -o "$OUTPUT/$note_name_no_ext.html"

    cp -r "$STATIC_FILES"/* "$OUTPUT"
    if test -f "$OUTPUT"/"$note_name_no_ext.html"; then
        open "$OUTPUT"/"$note_name_no_ext".html
    fi
}

get_note
