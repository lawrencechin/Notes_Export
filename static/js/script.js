let search_timeout;
let resize_timeout;
const body = document.body;
const header = body.querySelector( "header" );
const search_bar =  header.querySelector( "#searchbar" );
const sidebar_btn = body.querySelector( "#sidebar_btn" );
const note_list_btn = body.querySelector( "#note_list_btn" );
const toc_btn = body.querySelector( "#toc_btn" );
const top_btn = body.querySelector( "#top_btn" );
const tag_container = header.querySelector( "#tag_container" );
const clear_tag_btn = tag_container.querySelector( "#clear_tag_selection" );
const note_lists = body.querySelector( "#note_lists" );
const note_list_items = note_lists.querySelectorAll( "li" );
const toc_container = body.querySelector( "#toc" ).children[ 1 ];
const content_pane = body.querySelector( "#content_pane" );
const loading_alert = body.querySelector( ".loading" );
const lightbox = body.querySelector( "#lightbox" );
const lightbox_img = lightbox.querySelector( "img" );
const lightbox_close_btn = lightbox.querySelector( "#close_lightbox_btn" );
let is_toc_visible = false;
const single_pane_media_query = window.matchMedia( "(max-width: 950px)" );
const single_pane_small_media_query = window.matchMedia( "(max-width: 560px)" );

const media_query_match = () => {
    return single_pane_media_query.matches || single_pane_small_media_query.matches;
};

const initial_media_match = () => {
    if( media_query_match())
        body.classList.add( "single_pane" );
    else
        body.classList.add( "multiple_pane" );
};

const auto_scroll = elem_to_scroll_to => {
    if( media_query_match()){
        window.scrollTo({
            top: elem_to_scroll_to.offsetTop,
            left: 0,
            behaviour: "scroll"
        });

        return true;
    }
    return false;
};

const top_handler = () => {
    auto_scroll( body );
};

const toggle_sidebar = () => {
    if( body.matches( ".collapse_sidebars" )){
        body.classList.remove( "collapse_sidebars" );
        if( is_toc_visible ) body.classList.add( "reveal_toc" );
    } else {
        body.classList.add( "collapse_sidebars" );
        body.classList.remove( "reveal_toc", "reveal_note_lists" );
    }
};

const single_pane_note_lists_scroll = () => {
    const active_list_item = body.querySelector( ".category_list_item.active" );

    return auto_scroll( active_list_item || note_lists );
};

const toggle_note_lists = () => {
    const media_scroll = single_pane_note_lists_scroll();
    if( !media_scroll ){
        body.classList.remove( "reveal_toc" );
        body.classList.toggle( "reveal_note_lists" );
        is_toc_visible = false;
    }
};

const single_pane_toc_scroll = () => {
    return auto_scroll( toc );
};

const toggle_toc = () => {
    const media_scroll = single_pane_toc_scroll();
    if( !media_scroll ){
        body.classList.remove( "reveal_note_lists" );
        body.classList.toggle( "reveal_toc" );
        is_toc_visible = body.matches( ".reveal_toc" );
    }
};

const button_enable = () => {
    const toc_depth = toc_container.querySelector( "li > ul" );    
    toc_btn.disabled = toc_depth ? false : true;
    note_list_btn.disabled = toc_depth ? false : true;
    sidebar_btn.disabled = false;
};

const close_lightbox = () => {
    lightbox.classList.remove( "show" );
};

const tag_handler = e => {
    e.preventDefault();
    if( body.matches( ".reveal_toc" )) return;
    search_bar.value = "";
    const target = e.target;
    const node_type = target.nodeName;
    const is_button = node_type === "BUTTON";

    if( node_type !== "A" && !is_button ) return;

    const href = !is_button
        ? target.href.split( "/" ).splice( -1, 1 )[ 0 ]
        : null;
    let is_active = target.parentElement.matches( ".active" );
    const active_tag = is_active ? null : tag_container.querySelector( ".active" );

    if( is_active ){
        target.parentElement.classList.remove( "active" );
        clear_tag_btn.disabled = true;
    } else {
        if( !is_button ){
            target.parentElement.classList.add( "active" );
            clear_tag_btn.disabled = false;
        } else
            clear_tag_btn.disabled = true;
    }

    if( active_tag ) active_tag.classList.remove( "active" );

    if( is_button && active_tag )
        is_active = true;

    note_list_items.forEach( l => {
        if( is_active )
            l.classList.remove( "tag_sort", "hide" );
        else {
            const tagged_list_item = l.children[ 1 ];
            const tag = tagged_list_item
                ? tagged_list_item.href.split( "/" ).splice( -1, 1 )[ 0 ]
                : null;
            if( tag === href ){
                l.classList.add( "tag_sort" );
                l.classList.remove( "hide" );
            } else {
                l.classList.add( "hide" )
                l.classList.remove( "tag_sort" );
            }
        }
    });

    const is_tag_sorted = note_lists.querySelector( ".tag_sort" );
    if( is_tag_sorted ) 
        note_lists.scrollTo( 0, is_tag_sorted.offsetTop );
    else 
        note_lists.scrollTo( 0, 0 );
};

const make_active_note = elem => {
    const current_note = note_lists.querySelector( ".active" );
    if( current_note ) current_note.classList.remove( "active" );
    elem.parentElement.classList.add( "active" );
};

// Make long tables scrollable but only on mobile devices
const wrap_tables = tables_list => {
    if( tables_list && media_query_match()){
        tables_list.forEach( t => {
            const wrapper = document.createElement( "div" );
            const table_parent = t.parentElement;
            const table_clone = t.cloneNode( true );
            wrapper.appendChild( table_clone );
            wrapper.classList.add( "table_container" );
            table_parent.replaceChild( wrapper, t );
        });
    }
};

const parse_html_data = data => {
    content_pane.classList.add( "hide" );
    [ ...content_pane.children ].forEach( cp => {
        cp.remove();
    });
    [ ...toc_container.children ].forEach( tc => {
        tc.remove();
    });
    toc_container.parentElement.scrollTo( 0, 0 );
    content_pane.scrollTo({ 
        top: 0,
        left: 0,
        behaviour: "smooth"
    });

    const html = data.slice( 
        data.indexOf( "</nav" ) + 6,
    );
    const toc = data.slice( 
        data.indexOf( "<ul" ) + 4,
        data.indexOf( "</nav" ) + 6
    );

    parse_toc( toc );
    button_enable();
    content_pane.innerHTML = html;
    content_pane.classList.remove( "hide" );
    loading_alert.classList.remove( "show" );

    wrap_tables( content_pane.querySelectorAll( "table" ));
};

const expand_toc_btn = e => {
    e.preventDefault();
    e.target.classList.toggle( "open" );
    if( e.target.classList.contains( "open" ))
        e.target.innerText = "-";
    else
        e.target.innerText = "+";
};

const create_expand_btn = () => {
    const expand_btn = document.createElement( "button" );
    expand_btn.innerText = "+";
    expand_btn.title = "Expand TOC";
    expand_btn.classList.add( "expand_toc_btn" );
    expand_btn.addEventListener( "click", expand_toc_btn );
    return expand_btn;
};

const parse_toc = toc_data => {
    toc_container.innerHTML = toc_data;
    toc_container.children[ 0 ].querySelectorAll( "li > ul > li:has( ul )" ).forEach( li => {
        li.children[ 0 ].after( create_expand_btn());
    });
};

const note_list_handler = e => {
    e.preventDefault();
    const elem = e.target;
    const node_type = elem.nodeName;
    if( node_type !== "A" ) return;

    if( elem.matches( ".cat_list_tag" )){
        const tag = elem.href.split( "/" ).splice( -1, 1 )[ 0 ];
        tag_container.querySelector( "." + tag ).children[ 0 ].click();
    } else if( elem.matches( ".external" ))
        window.open( elem.dataset.link, "_blank" );
    else {
        loading_alert.classList.add( "show" );
        fetch( elem.href )
            .then( res =>
                res.text()
            ).then( res => {
                parse_html_data( res );
                auto_scroll( content_pane );
                make_active_note( elem );
            });
    }
};

const search_handler = e => {
    const target = e.target;
    clearTimeout( search_timeout );
    const is_tag_active = tag_container.querySelector( ".active" );
    search_timeout = setTimeout(() => {
        const searchTerm = new RegExp( target.value.trim(), "i" );
        const search_container = is_tag_active
            ? note_lists.querySelectorAll( ".tag_sort" )
            : note_list_items;
        note_lists.classList.add( "hide" );

        search_container.forEach( l => {
            const text = l.children[ 0 ].innerText;
            if( searchTerm.test( text )){ 
                l.classList.remove( "hide" );
            } else
                l.classList.add( "hide" );
        });
        note_lists.classList.remove( "hide" );
    }, 500 );
};

content_pane.addEventListener( "error", e => {
    if( e.target.nodeName === "IMG" ){
        e.target.src = "./icons/_no_image.png";
        e.target.classList.add( "img_not_found" );
    }
}, true );

content_pane.addEventListener( "click", e => {
    const target = e.target;
    const node_type = target.nodeName;

    // TODO: what do we do with images that are links?

    if( node_type === "IMG" ){
        lightbox.classList.add( "show" );
        lightbox_img.src = target.src;
    }
});

lightbox.addEventListener( "click", e => {
    const node_type = e.target.nodeName;
    if( node_type !== "IMG" )
        lightbox_close_btn.click();
});

note_lists.addEventListener( "click", note_list_handler );
tag_container.addEventListener( "click", tag_handler );
sidebar_btn.addEventListener( "click", toggle_sidebar );
note_list_btn.addEventListener( "click", toggle_note_lists );
toc_btn.addEventListener( "click", toggle_toc );
top_btn.addEventListener( "click", top_handler );
lightbox_close_btn.addEventListener( "click", close_lightbox );
search_bar.addEventListener( "keydown", search_handler );

initial_media_match();

document.addEventListener( "DOMContentLoaded", () => {
    // only act on width changes
    // stops resize firing when iOS address bar
    // hides or shows
    let windowWidth = window.innerWidth;
    const resize_handler = () => {
        clearTimeout( resize_timeout );
        resize_timeout = setTimeout(() => {
            if( window.innerWidth != windowWidth && media_query_match()){
                windowWidth = window.innerWidth;
                // show everything in single pane mode
                body.classList.remove( "reveal_note_lists", "reveal_toc", "collapse_sidebars" );
                if( body.matches( ".multiple_pane" )){
                    // jump back to previous scroll position
                    const pageYOffset = content_pane.children.length;
                    const combined_elem_height = header.offsetHeight + note_lists.offsetHeight + toc.offsetHeight;
                    if( pageYOffset > 2 )
                        window.scrollTo( 
                            0,
                            window.pageYOffset + combined_elem_height
                        );
                    body.classList.remove( "multiple_pane" );
                    body.classList.add( "single_pane" );
                }
            } else {
                body.classList.remove( "single_pane" );
                body.classList.add( "multiple_pane" );
            }
        }, 250 );
    };
    
    window.addEventListener( "resize", resize_handler );
});
