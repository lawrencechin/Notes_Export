Boolean = { fg = scheme.Orange }, -- a boolean constant: TRUE, false
Character =	{ fg = scheme.Orange }, -- any character constant: 'c', '\n'
Comment = { fg = scheme.Grey },
Conceal = { fg = scheme.Blue }, -- placeholder characters substituted for concealed text (see 'conceallevel')
Conditional = { fg = scheme.Orange, style = "bold" },
Constant =	{ fg = scheme.Blue }, -- any constant
Debug = { fg = scheme.Red, bg = scheme.none, scheme.none }, -- debugging statements
Define = { fg = scheme.Purple }, -- preprocessor #define
Delimiter =	{ fg = scheme.Green }, -- character that needs attention like , or .
Directory = { fg = scheme.Purple }, -- directory names (and other special names in listings)
Error = { fg = scheme.Red, style = "bold" }, -- any erroneous construct
ErrorMsg = { style = "bold" }, -- error messages
Exception =	{ fg = scheme.Red }, -- try, catch, throw
Float = { fg = scheme.Blue }, -- a floating point constant: 2.3e10
Function = { fg = scheme.Magenta },
Identifier = { fg = scheme.Blue },
Ignore = { fg = scheme.none }, -- left blank, hidden
IncSearch = { fg = scheme.none, bg = scheme.none, style = 'reverse' }, -- 'incsearch' highlighting; also used for the text replaced with ":s///c"
Include = { fg = scheme.Purple }, -- preprocessor #include
Keyword = { fg = scheme.Purple, style = "bold" },
Label = { fg = scheme.Yellow }, -- case, default, etc.
MatchParen = { sp = scheme.Pink, style = "underline" }, -- The character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
NonText = { fg = scheme.Pink }, -- '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line). See also |hl-EndOfBuffer|.
Number = { fg = scheme.Orange }, -- a number constant: 5
Operator =	{ fg = scheme.Slate_Grey }, -- sizeof", "+", "*", etc.
PreCondit =	{ fg = scheme.Yellow }, -- preprocessor #if, #else, #endif, etc.
PreProc =	{ fg = scheme.Magenta }, -- generic Preprocessor
Question = { fg = scheme.Green, style = "bold" }, -- |hit-enter| prompt and yes/no questions
Repeat = { fg = scheme.Purple, bg = scheme.none, scheme.none },
Special = { fg = scheme.Orange }, -- any special symbol
SpecialChar = { fg = scheme.Blue }, -- special character in a constant
SpecialComment = { fg = scheme.Grey }, -- special things inside a comment
SpecialKey = { fg = scheme.Purple }, -- Unprintable characters: text displayed differently from what it really is.But not 'listchars' whitespace. |hl-Whitespace|
Statement =	{ fg = scheme.Purple, style = "bold" }, -- any statement
StorageClass = { fg = scheme.Bold_Italic }, -- static, register, volatile, etc.
String = { fg = scheme.Green, bg = scheme.none },
Structure =	{ fg = scheme.Purple }, -- struct, union, enum, etc.
Tag = { fg = scheme.Purple }, -- you can use CTRL-] on this
Title = { fg = scheme.Magenta, style = "bold" }, -- titles for output from ":set all", ":autocmd" etc.
Todo = { fg = scheme.Yellow, style = "bold,italic" }, -- anything that needs extra attention; mostly the keywords TODO FIXME and XXX
Type = { fg = scheme.Purple }, -- int, long, char, etc.
Typedef = { fg = scheme.Blue }, -- A typedef
WarningMsg = { fg = scheme.Orange }, -- warning messages
Warnings = { fg = scheme.Yellow, bg = scheme.none, style = "reverse" },
Whitespace =	{ fg = scheme.Text_Colour }, -- "nbsp", "space", "tab" and "trail" in 'listchars'

TSEmphasis = { fg = scheme.Bold_Italic, style = "italic" }, -- For text to be represented with emphasis.
TSField = { fg = scheme.Text_Colour }, -- For fields.
TSFuncMacro = { fg = scheme.Bold_Italic }, -- For macro defined fuctions (calls and definitions): each `macro_rules` in Rust.
TSFunction = { fg = scheme.Blue }, -- For function (calls and definitions).
TSLiteral = { fg = scheme.Text_Colour }, -- Literal text.
TSMethod = { fg = scheme.Blue }, -- For method calls and definitions.
TSNamespace = { fg = scheme.Yellow }, -- For identifiers referring to modules and namespaces.
TSParameter = { fg = scheme.Slate_Grey }, -- For parameters of a function.
TSParameterReference= { fg = scheme.Slate_Grey }, -- For references to parameters of a function.
TSProperty = { fg = scheme.Text_Colour }, -- Same as `TSField`,accesing for struct members in C.
TSPunctBracket = { link = "PreProc" }, -- For brackets and parens.
TSPunctDelimiter = { fg = scheme.Slate_Grey }, -- For delimiters ie: `.`
TSPunctSpecial = { fg = scheme.Slate_Grey }, -- For special punctutation that does not fall in the catagories before.
TSString = { fg = scheme.Green }, -- For strings.
TSStringEscape = { fg = scheme.Slate_Grey }, -- For escape characters within a string.
TSStringRegex = { fg = scheme.Blue }, -- For regexes.
TSStrong = { fg = scheme.Bold_Italic, style = "bold" },
TSSymbol = { fg = scheme.Yellow }, -- For identifiers referring to symbols or atoms.
TSTagDelimiter = { fg = scheme.Slate_Grey }, -- Tag delimiter like `<` `>` `/`
TSText = { fg = scheme.Text_Colour }, -- For strings considered text in a markup language.
TSTextReference = { fg = scheme.Bold_Italic }, -- Currently used in MD for link text
TSTitle = { fg = scheme.Magenta, style = 'bold' }, -- Text that is part of a title.
TSTypeBuiltin = { fg = scheme.Yellow }, -- For builtin types.
TSURI = { fg = scheme.Blue }, -- Any URI like a link or email.
TSVariable = { fg = scheme.Text_Colour }, -- Any variable name that does not have another highlight.
TSVariableBuiltin = { fg = scheme.Text_Colour }, -- Variable names that are defined by the languages, like `this` or `self`.